---
title:  "Love and Sexuality: 1. Introduction"
author: Andreas Matthias, Lingnan University
date: September 1, 2019
...

# Introduction to the course

## Welcome!

Some of my names are:

- Andreas Matthias
- “Mr. Ma”
- “Andy”

(use which one you like!)

## Talk about

>- Moodle
>- Course outline in Moodle
>- Class calendar
>- Lecture notes in Moodle
>- Presentation assignments
>- Term papers, final exam
>- Deadlines

## Notes about this class

>- Not hard to understand, but very little to memorize.
>- Little lecturing. Class is built around class discussion.
>- Participate in the discussions and explore the topics by thinking about them critically and engaging with them.
>- Read the reading materials!
>- If you expect to just listen, memorize, and do the final exam, then you can be pretty sure to end up with a C.
>- You need to spend about 6 hours (!) every week reading and preparing for class (2 hours for every hour of class instruction according to student handbook).
>- This is a difficult class, because it covers a lot of ground. Love is a big topic, and we’ll follow its history and theory through the centuries and across disciplines, so there will be a lot to learn and remember.

## Any questions?
