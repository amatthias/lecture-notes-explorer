love00-template.md&0.1&---
love00-template.md&0.1&title:  "Love and Sexuality: 1. Introduction"
love00-template.md&0.1&author: Andreas Matthias, Lingnan University
love00-template.md&0.1&date: September 1, 2019
love00-template.md&0.1&...
love00-template.md&0.1&
love00-template.md&1.0&# Introduction to the course
love00-template.md&1.0&
love00-template.md&1.1&## Welcome!
love00-template.md&1.1&
love00-template.md&1.1&Some of my names are:
love00-template.md&1.1&
love00-template.md&1.1&- Andreas Matthias
love00-template.md&1.1&- “Mr. Ma”
love00-template.md&1.1&- “Andy”
love00-template.md&1.1&
love00-template.md&1.1&(use which one you like!)
love00-template.md&1.1&
love00-template.md&1.2&## Presentation of course outline
love00-template.md&1.2&
love00-template.md&1.2&(see outline in Moodle)
love00-template.md&1.2&
love00-template.md&1.2&![](graphics/01-chess.jpg) \
love00-template.md&1.2&
love01-Intro.md&0.1&---
love01-Intro.md&0.1&title:  "Love and Sexuality: 1. Introduction"
love01-Intro.md&0.1&author: Andreas Matthias, Lingnan University
love01-Intro.md&0.1&date: September 1, 2019
love01-Intro.md&0.1&...
love01-Intro.md&0.1&
love01-Intro.md&1.0&# Introduction to the course
love01-Intro.md&1.0&
love01-Intro.md&1.1&## Welcome!
love01-Intro.md&1.1&
love01-Intro.md&1.1&Some of my names are:
love01-Intro.md&1.1&
love01-Intro.md&1.1&- Andreas Matthias
love01-Intro.md&1.1&- “Mr. Ma”
love01-Intro.md&1.1&- “Andy”
love01-Intro.md&1.1&
love01-Intro.md&1.1&(use which one you like!)
love01-Intro.md&1.1&
love01-Intro.md&1.2&## Talk about
love01-Intro.md&1.2&
love01-Intro.md&1.2&>- Moodle
love01-Intro.md&1.2&>- Course outline in Moodle
love01-Intro.md&1.2&>- Class calendar
love01-Intro.md&1.2&>- Lecture notes in Moodle
love01-Intro.md&1.2&>- Presentation assignments
love01-Intro.md&1.2&>- Term papers, final exam
love01-Intro.md&1.2&>- Deadlines
love01-Intro.md&1.2&
love01-Intro.md&1.3&## Notes about this class
love01-Intro.md&1.3&
love01-Intro.md&1.3&>- Not hard to understand, but very little to memorize.
love01-Intro.md&1.3&>- Little lecturing. Class is built around class discussion.
love01-Intro.md&1.3&>- Participate in the discussions and explore the topics by thinking about them critically and engaging with them.
love01-Intro.md&1.3&>- Read the reading materials!
love01-Intro.md&1.3&>- If you expect to just listen, memorize, and do the final exam, then you can be pretty sure to end up with a C.
love01-Intro.md&1.3&>- You need to spend about 6 hours (!) every week reading and preparing for class (2 hours for every hour of class instruction according to student handbook).
love01-Intro.md&1.3&>- This is a difficult class, because it covers a lot of ground. Love is a big topic, and we’ll follow its history and theory through the centuries and across disciplines, so there will be a lot to learn and remember.
love01-Intro.md&1.3&
love01-Intro.md&1.4&## Any questions?
love02-phenomenon.md&0.1&---
love02-phenomenon.md&0.1&title:  "Love and Sexuality: 2. The phenomenon of love"
love02-phenomenon.md&0.1&author: Andreas Matthias, Lingnan University
love02-phenomenon.md&0.1&date: September 1, 2019
love02-phenomenon.md&0.1&...
love02-phenomenon.md&0.1&
love02-phenomenon.md&1.0&# What is love?
love02-phenomenon.md&1.0&
love02-phenomenon.md&1.1&## Your ideas?
love02-phenomenon.md&1.1&
love02-phenomenon.md&1.1&>- What is love? How can we define it for someone who’s just arrived from Mars?
love02-phenomenon.md&1.1&>- What distinguishes love from other social phenomena (e.g. friendship)?
love02-phenomenon.md&1.1&>- What is romantic love? What distinguishes that from other forms of love (e.g. Christian love towards one’s neighbour)?
love02-phenomenon.md&1.1&>- Is romantic love the same as a sexual relationship? Can I have sex without love and love without sex?
love02-phenomenon.md&1.1&>- What is the difference between loving and liking?
love02-phenomenon.md&1.1&>- Can one believe one is in love but be wrong about it? 
love02-phenomenon.md&1.1&>- Can one live a worthwhile life without love?
love02-phenomenon.md&1.1&
love02-phenomenon.md&1.2&## Does love depend on the properties of the beloved? (1)
love02-phenomenon.md&1.2&
love02-phenomenon.md&1.2&>- Appraisal or bestowal?
love02-phenomenon.md&1.2&>- What if the beloved’s properties change (for example, due to ageing)? Does our love then diminish or disappear? 
love02-phenomenon.md&1.2&>- What if I am mistaken about the beloved’s properties? (e.g. he turns out to be a secret agent, a spy, a criminal, or to be a woman instead of a man, like in some Shakespearean plays). Does my love suddenly become unjustifiable?
love02-phenomenon.md&1.2&>- Are there different kinds of properties that we must distinguish (e.g. inner/outer)?
love02-phenomenon.md&1.2&
love02-phenomenon.md&1.3&## Does love depend on the properties of the beloved? (2)
love02-phenomenon.md&1.3&
love02-phenomenon.md&1.3&>- What if someone else comes along who has the same properties to an even higher degree? Do we then switch the object of our love? Would it be right to switch?
love02-phenomenon.md&1.3&>- If two people have the same properties, is it a random choice whom I fall in love with?
love02-phenomenon.md&1.3&>- If we don’t think that love is based on properties, can we then just love random people?
love02-phenomenon.md&1.3&>- Is the *history* of a love relationship relevant to the actual love experienced at a particular moment?
love02-phenomenon.md&1.3&
love02-phenomenon.md&1.4&## Appraisal and bestowal
love02-phenomenon.md&1.4&
love02-phenomenon.md&1.4&![](graphics/02-appraisal-bestowal.png)\ 
love02-phenomenon.md&1.4&
love02-phenomenon.md&1.4&
love02-phenomenon.md&1.4&
love02-phenomenon.md&1.5&## Greek and Latin words for love
love02-phenomenon.md&1.5&
love02-phenomenon.md&1.5&>- Eros
love02-phenomenon.md&1.5&>- Philia: Friendship.
love02-phenomenon.md&1.5&>- Agape: The unconditional love of God towards his creation, or the unconditional love required of Christians towards their neighbour.
love02-phenomenon.md&1.5&>- Caritas: Charity. Is charity the same as love?
love02-phenomenon.md&1.5&>- Storge: Care, for example for one’s old parents or children.
love02-phenomenon.md&1.5&
love02-phenomenon.md&1.6&## The moral side of loving
love02-phenomenon.md&1.6&
love02-phenomenon.md&1.6&>- Is love a virtue or a passion?
love02-phenomenon.md&1.6&>- If it is a virtue, is then someone who does not love a bad person? Do we need to love in order to be morally good? Do we have a duty to love?
love02-phenomenon.md&1.6&>- When I benefit from a friendship, is this then a “worse” friendship? Does friendship have to be entirely selfless? Does such a thing as selfless friendship even exist?
love02-phenomenon.md&1.6&>- Is selfless love possible?
love02-phenomenon.md&1.6&>- What if my love contradicts my moral duties? (e.g. I love a killer). What should be stronger? My love or my moral duty?
love02-phenomenon.md&1.6&>- Is self-love morally right, wrong, necessary, meaningless?
love02-phenomenon.md&1.6&>- Is it permissible to be blinded by love, or must love be completely clear-eyed? Does love depend on correct and complete knowledge about the beloved?
love02-phenomenon.md&1.6&>- Is love compatible with obsession?
love02-phenomenon.md&1.6&
love02-phenomenon.md&1.7&## The properties of love
love02-phenomenon.md&1.7&
love02-phenomenon.md&1.7&>- How would I recognise a love relationship between two people?
love02-phenomenon.md&1.7&>- Is concern for the other person necessary for love?
love02-phenomenon.md&1.7&>- Is concern for the other person sufficient for love?
love02-phenomenon.md&1.7&>- Does love need to be exclusive? Can I love multiple people at the same time?
love02-phenomenon.md&1.7&>- Does love need to be constant? Can I love different people every month but still honestly love them?
love02-phenomenon.md&1.7&>- Is it necessary that I think of the beloved person as irreplaceable? 
love02-phenomenon.md&1.7&>- What if the beloved dies and I find another person to love? Does this mean that I didn’t love the first one? Does it mean that I don’t love the second one? What if they are both alive at the same time?
love02-phenomenon.md&1.7&
love02-phenomenon.md&1.8&## Objects of love
love02-phenomenon.md&1.8&
love02-phenomenon.md&1.8&>- Can non-human animals love? Can they be objects of love?
love02-phenomenon.md&1.8&>- Can I love someone who is in a coma in a hospital bed? Would this be different from loving someone from a distance who doesn’t know about me? In what way?
love02-phenomenon.md&1.8&>- Is love for inanimate objects (e.g. one’s country) possible? Is it really “love”? In what sense?
love02-phenomenon.md&1.8&>- Is one-sided love possible? Is it still love? Is it worse than reciprocated love?
love02-phenomenon.md&1.8&>- What if I am mistaken about the feelings of the beloved? If what looks like reciprocated love is fake?
love02-phenomenon.md&1.8&>- Is it possible to love and hate the same person at the same time, or are these feelings mutually exclusive?
love02-phenomenon.md&1.8&
love02-phenomenon.md&1.9&## Love and sex
love02-phenomenon.md&1.9&
love02-phenomenon.md&1.9&>- What is the relationship between love and sex?  (Is totally asexual love deficient in any way?)
love02-phenomenon.md&1.9&>- Is a life without sex a worse life than a life without chocolate? Why?
love02-phenomenon.md&1.9&
love02-phenomenon.md&1.10&## The End. Thank you for your attention!
love02-phenomenon.md&1.10&
love02-phenomenon.md&1.10&Questions?
love02-phenomenon.md&1.10&
love02-phenomenon.md&1.10&<!--
love02-phenomenon.md&1.10&
love02-phenomenon.md&1.10&%% Local Variables: ***
love02-phenomenon.md&1.10&%% mode: markdown ***
love02-phenomenon.md&1.10&%% mode: visual-line ***
love02-phenomenon.md&1.10&%% mode: cua ***
love02-phenomenon.md&1.10&%% mode: flyspell ***
love02-phenomenon.md&1.10&%% End: ***
love02-phenomenon.md&1.10&
love02-phenomenon.md&1.10&-->
love03-plato1.md&0.1&---
love03-plato1.md&0.1&title:  "Love and Sexuality: 3. Plato’s Symposion"
love03-plato1.md&0.1&author: Andreas Matthias, Lingnan University
love03-plato1.md&0.1&date: September 1, 2019
love03-plato1.md&0.1&...
love03-plato1.md&0.1&
love03-plato1.md&1.0&# Plato and the Symposion
love03-plato1.md&1.0&
love03-plato1.md&1.0&
love03-plato1.md&1.1&## Why read the Symposion at all?
love03-plato1.md&1.1&
love03-plato1.md&1.1&>- A 2400 years old text?
love03-plato1.md&1.1&>- Somebody said that all of philosophy is just “footnotes to Plato.”
love03-plato1.md&1.1&>- This is a little exaggerated, but in some areas of philosophy it is true.
love03-plato1.md&1.1&>- The philosophy of love, and our everyday understanding of what love is, have not changed radically since Plato’s time.
love03-plato1.md&1.1&>- The text is also surprisingly modern, very well constructed and works on multiple levels. You will see how nicely it’s done in the end, when we have read it.
love03-plato1.md&1.1&
love03-plato1.md&1.2&## Symposia
love03-plato1.md&1.2&
love03-plato1.md&1.2&>- “Syn-”: plus, together.
love03-plato1.md&1.2&>- “pino,” “pos-”: to drink.
love03-plato1.md&1.2&>- “Symposion”: the drinking-together; a party.
love03-plato1.md&1.2&>- Greek: symposion, Latin: symposium. Often, books refer to it as “symposium” (Latin), but this is misleading. Plato spoke and wrote in Greek.
love03-plato1.md&1.2&>- At symposia, only men were normally drinking together, singing songs, telling jokes, and having a good time. They reclined on long benches and the host could dictate how strong the wine should be. In this way, the host could control how things developed.
love03-plato1.md&1.2&>- Women were only present as dancers or musicians, not as participants. 
love03-plato1.md&1.2&>- In Plato’s Symposion, the wisest character, the one who even teaches Socrates about love, is a woman, Diotima!
love03-plato1.md&1.2&
love03-plato1.md&1.2&
love03-plato1.md&1.3&## Plato
love03-plato1.md&1.3&
love03-plato1.md&1.3&>- Who was Plato?
love03-plato1.md&1.3&>     - An ancient Greek philosopher.
love03-plato1.md&1.3&>- Where and when did he live?
love03-plato1.md&1.3&>     - Athens, 427-347 BCE.
love03-plato1.md&1.3&>- How is he related to other ancient Greek philosophers you know?
love03-plato1.md&1.3&>     - Student of Socrates (470-399 BCE), teacher of Aristotle.
love03-plato1.md&1.3&
love03-plato1.md&1.4&## Socrates
love03-plato1.md&1.4&
love03-plato1.md&1.4&>- What happened to Socrates?
love03-plato1.md&1.4&>     - He was accused of corrupting the youth with his teachings. His way of publicly disputing with influential people and humiliating them surely didn’t make him more popular.
love03-plato1.md&1.4&>     - In a famous trial, Socrates gave a long speech (“Apology”), in which he refused to apologise and even made fun of the judges.
love03-plato1.md&1.4&>     - He was sentenced to death in 399 BCE. 
love03-plato1.md&1.4&>     - Plato and other friends and students offered to get him out of prison, but he refused and was executed.
love03-plato1.md&1.4&>     - By not escaping, he wanted to make the point that obeying the laws and being morally upright is more important than even one’s life.
love03-plato1.md&1.4&>     - Socrates is generally described as an ugly, old man, which is important in the context of the Symposion.
love03-plato1.md&1.4&
love03-plato1.md&1.5&## After Socrates’ death
love03-plato1.md&1.5&
love03-plato1.md&1.5&>- Plato travelled around the Mediterranean.
love03-plato1.md&1.5&>- He tried to convince the leader of Syracuse to try out Plato’s political theory and create an ideal state that would be ruled by philosophers.
love03-plato1.md&1.5&>- He failed, returned back to Athens, and founded a philosophy school, in which Aristotle later studied.
love03-plato1.md&1.5&
love03-plato1.md&1.5&
love03-plato1.md&1.6&## The Symposion
love03-plato1.md&1.6&
love03-plato1.md&1.6&>- The particular symposion took place around 35 years before it was re-told (or imagined) by Plato.
love03-plato1.md&1.6&>- Is is probably not a faithful retelling of a real symposion, but to some extent made up to tell the story that Plato wanted to tell.
love03-plato1.md&1.6&>- The people were still known to Plato’s readers, but all of them, except for Aristophanes, were already dead. Aristophanes might have been in the last year of his life.
love03-plato1.md&1.6&>- Imagine a book about a meeting of famous people in the mid-1980s: politicians of that time, generals, an actor or two, a writer, and a philosopher; now all dead or very old. This is how the Athenians of Plato’s time would have read the Symposion, recognising the names in the text.
love03-plato1.md&1.6&
love03-plato1.md&1.6&
love03-plato1.md&1.7&## People in the text (only the main ones) (1)
love03-plato1.md&1.7&
love03-plato1.md&1.7&>- Agathon (about 31 years old at the Symposion): A poet who wrote tragedies (none survived).
love03-plato1.md&1.7&>- Alcibiades (about 35 years old): A famous aristocrat and rich party boy, later politician and general of the Athenian fleet. He was known for switching sides as it served him and only looking at his own benefit. Socrates saved his life in battle on two different occasions, and Alcibiades always looked up at Socrates as his teacher.
love03-plato1.md&1.7&>- Aristophanes (about 34 years old): One of the “big four” writers in classical Athens (the others are Aeschylus, Euripides, Sophocles). Aristophanes wrote comedies, some of which made fun of Socrates.
love03-plato1.md&1.7&>- Eryximachus (about 30 years old): A physician, friend of the others.
love03-plato1.md&1.7&
love03-plato1.md&1.8&## People in the text (only the main ones) (2)
love03-plato1.md&1.8&
love03-plato1.md&1.8&>- Pausanias: Has a relationship with Agathon. Legal expert. There are many people in ancient history with that name.
love03-plato1.md&1.8&>- Phaedrus (about 28 years old): Friend of Eryximachus, also appears in other works of Plato. Educated young man who likes literature.
love03-plato1.md&1.8&>- Socrates (about 54 years. old): one of the most famous philosophers of ancient times. He didn’t write anything down himself, so his thoughts are only known through the works of others (mainly Plato). 
love03-plato1.md&1.8&
love03-plato1.md&1.8&
love03-plato1.md&2.0&# Structure of the text
love03-plato1.md&2.0&
love03-plato1.md&2.1&## Narrative structure (1)
love03-plato1.md&2.1&
love03-plato1.md&2.1&>- Many layers of “recollections,” perhaps to indicate that the Symposion is not really a transcript of the party as it happened, but a text that reflects the values and preferences of the people that retell it.
love03-plato1.md&2.1&>- The Symposion is the recollection of someone called Apollodoros, of what he was told long ago by one Aristodemos, who himself was reconstructing the events from memory.
love03-plato1.md&2.1&
love03-plato1.md&2.2&## Narrative structure (2)
love03-plato1.md&2.2&
love03-plato1.md&2.2&>- Seven people decide to have a party in which they’ll give speeches about love.
love03-plato1.md&2.2&>- Each person talks from their own perspective. Some are more romantic, some funny, some more oriented towards medicine or the natural sciences.
love03-plato1.md&2.2&>- Through these seven people, Plato manages to talk about love from many different perspectives before finally giving the word to Socrates.
love03-plato1.md&2.2&>- Socrates provides the crowning contribution of the evening. Interestingly, he attributes his ideas to a woman, Diotima, an unusual move for the time.
love03-plato1.md&2.2&
love03-plato1.md&2.2&
love03-plato1.md&2.3&## The seven speeches
love03-plato1.md&2.3&
love03-plato1.md&2.3&>- Introduction. First glimpse of Socrates.
love03-plato1.md&2.3&>- 1. Phaedrus (literature): the moral side of love.
love03-plato1.md&2.3&>- 2. Pausanias (law): “higher” (heavenly) and “lower” (popular) love.
love03-plato1.md&2.3&>- 3. Eryximachos (medicine): love as an expression of universal harmony.
love03-plato1.md&2.3&>- 4. Aristophanes (comedy): love as union between separated parts of an individual.
love03-plato1.md&2.3&>- 5. Agathon (tragedy): the connection between beauty and virtue.
love03-plato1.md&2.3&>- 6. Socrates (philosophy): the ladder of love, attributed to Diotima.
love03-plato1.md&2.3&>- 7. Alcibiades (lover of Socrates): application of Diotima’s ladder.
love03-plato1.md&2.3&>- End of the party.
love03-plato1.md&2.3&
love03-plato1.md&3.0&# The speeches
love03-plato1.md&3.0&
love03-plato1.md&3.1&## How to read the text
love03-plato1.md&3.1&
love03-plato1.md&3.1&>- Ignore the references to mythological and literary figures.
love03-plato1.md&3.1&>- Back then, they would have been known to everyone, like when we today talk of Superman or any Disney movie character.
love03-plato1.md&3.1&>- Also, remember that this is a game to pass the time. The speakers don’t *want* to speak directly and make their points. They want to take their time, show their wit, and entertain their audience. You’ll have to look over all that to find the core message of each speaker.
love03-plato1.md&3.1&
love03-plato1.md&3.2&## Phaedrus (literary man)
love03-plato1.md&3.2&
love03-plato1.md&3.2&>- Love brings moral improvement.
love03-plato1.md&3.2&>- Lovers try to “look good” to their beloved, so they become better people.
love03-plato1.md&3.2&>- Only lovers will die for others.
love03-plato1.md&3.2&>- Therefore, love is the source of all virtue and happiness for men.
love03-plato1.md&3.2&
love03-plato1.md&3.3&## Pausanias (lawyer)
love03-plato1.md&3.3&
love03-plato1.md&3.3&>- Lawyers make all kinds of distinctions in law. Pausanias applies this method to his speech about love.
love03-plato1.md&3.3&>- Love is not one thing. It’s two different things: Heavenly and popular love.
love03-plato1.md&3.3&>- Generally, things are not in themselves good or bad. The context and the way we do them makes them good or bad.
love03-plato1.md&3.3&>- “Good” love is the love of older men towards boys that educates and improves the beloved. All other (selfish, pleasure-focused) kinds of love are vulgar (popular) instead of virtuous and refined (heavenly).
love03-plato1.md&3.3&>- Pausanias here already foreshadows Socrates’ later point that love is a progression from the love of the body to the love of the mind.
love03-plato1.md&3.3&
love03-plato1.md&3.4&## Eryximachus (doctor)
love03-plato1.md&3.4&
love03-plato1.md&3.4&>- Love is not only about morality and virtue, but also about health and well-being.
love03-plato1.md&3.4&>- Love is not restricted to humans. All things strive towards harmony and order.
love03-plato1.md&3.4&>- This is according to ancient theories of health that see illness as an imbalance of elements in the body (both in ancient Greece and China!). The physician diagnoses this imbalance and prescribes what is necessary to restore the balance = heal the patient.
love03-plato1.md&3.4&>- Opposites (or dissimilar things) attract each other. Medicine, music and other arts are nothing but the attempt to order and harmonise different aspects (heat and cold, musical notes, elements in the body), and so are kinds of love.
love03-plato1.md&3.4&>- If the opposites come together in the wrong way, they cause illness and destruction. 
love03-plato1.md&3.4&
love03-plato1.md&3.5&## Aristophanes (comic writer)
love03-plato1.md&3.5&
love03-plato1.md&3.5&>- Aristophanes tells a funny story about how love was created.
love03-plato1.md&3.5&>- Originally, there were three kinds of men: Male-Male, Female-Female and Female-Male. These were double beings (Imagine two of today’s humans standing together back-to-back as one being.) Each had four arms and legs.
love03-plato1.md&3.5&>- At one point, humans became too powerful. So the gods decided to split them in half. The two halves would eternally be searching for their other half, trying to reach the original unity of their person.
love03-plato1.md&3.5&>- The best kind of love is the love from man to man, since the original beings were Male-Male (and thus had the best properties).
love03-plato1.md&3.5&>- The absurd picture of half people looking for their other halves leads to the first definition of love in the Symposion:
love03-plato1.md&3.5&>     - *Love is the craving and pursuit of wholeness. [193a]*
love03-plato1.md&3.5&
love03-plato1.md&3.6&## Agathon (tragic poet)
love03-plato1.md&3.6&
love03-plato1.md&3.6&>- Love is always the love of beauty.
love03-plato1.md&3.6&>- It “flees from old age... [Love] hates old age by nature and refuses to come within any distance of it.” [195b]. Love “seeks its food among flowers” [196b].
love03-plato1.md&3.6&>- Love is desire, but it also is the desire for “beautiful things” [197c] and for the arts that create these things. Therefore, love is also the basis for arts, crafts and all kinds of skills.
love03-plato1.md&3.6&
love03-plato1.md&3.7&## Socrates (also representing Plato’s views)
love03-plato1.md&3.7&
love03-plato1.md&3.7&>- On p.39 of our reading (around 200a) you can see the “Socratic method” at work. Socrates asks someone (who considers himself an expert) questions that reveal that the expert must be mistaken (and that Socrates’ views are right).
love03-plato1.md&3.7&>- Love is not in the *possession* of the good and beautiful, but the *desire* for it.
love03-plato1.md&3.7&>- It doesn’t make sense to love health or riches if one is already healthy and rich (except as a with to continue to have these things in the future).
love03-plato1.md&3.7&>- Love is the *desire* to have something *now and for ever* in the future.
love03-plato1.md&3.7&>- But if love is desire, and we cannot desire something we already have, then love cannot be beautiful or good. Since only the lack of goodness or beauty would inspire love for these things.
love03-plato1.md&3.7&>- Now we reach a point at which it is unclear how to proceed. And here Socrates tells the tale of Diotima.
love03-plato1.md&3.7&
love03-plato1.md&3.8&## Diotima’s ladder (1): Preliminary clarifications
love03-plato1.md&3.8&
love03-plato1.md&3.8&>- Things get strange here. After all have agreed that proper love can only be between men, now Socrates, the wisest, presents a *woman* as his teacher in love! Socrates here makes fun of himself, by taking on the role of the receiving, dumb end of a typical Socratic dialogue.
love03-plato1.md&3.8&>- Diotima is, in every way, an exception: Woman among men, non-Athenian among Athenians, and absent among present guests.
love03-plato1.md&3.8&>- Love desires good things, because the possession of good things leads to happiness as the ultimate good [p.49, 205a].
love03-plato1.md&3.8&>- As opposed to Aristophanes, Diotima thinks that we don’t always have a desire to unite with our other halves, but only if we perceive them as something *good.* [p.50, 205e].
love03-plato1.md&3.8&>- *“Love loves the good to be one’s own for ever.”* [p.51,206a]
love03-plato1.md&3.8&
love03-plato1.md&3.9&## Diotima’s ladder (2): Immortality
love03-plato1.md&3.9&
love03-plato1.md&3.9&>- Since love loves the good to be one’s own *for ever,* but people die, love necessarily desires immortality. [p.53, 207a]
love03-plato1.md&3.9&>- “For here, too, on the same principle as before, [207d] the mortal nature ever seeks, as best it can, to be immortal. In one way only can it succeed, and that is by generation; since so it can always leave behind it a new creature in place of the old.” (p.54)
love03-plato1.md&3.9&>- Remember that Plato sees himself as a student of Socrates, who was dead by the time this was written; Plato continued his work and made his teacher immortal.
love03-plato1.md&3.9&>- Every being tries to have children in order to have part in this immortality. But children themselves are mortal.
love03-plato1.md&3.9&>- Only the love between learned men is ideal, because only their “children” (their ideas) are really immortal.
love03-plato1.md&3.9&
love03-plato1.md&3.10&## Diotima’s ladder (3): The progression of love
love03-plato1.md&3.10&
love03-plato1.md&3.10&>- In the first stage, we are in love with a particular body [p.57]
love03-plato1.md&3.10&>- But then we realise that many bodies are equally beautiful. So we love all beautiful bodies.
love03-plato1.md&3.10&>- From that, we go on to love the beauty of the souls that are inside these bodies.
love03-plato1.md&3.10&>- More practice leads us to love the beauty of our society, its rituals, its laws and all branches of knowledge that have their own beauty (mathematics, physics, art, ...)
love03-plato1.md&3.10&>- We look away from the fragile beauty of the individual and “turn towards the main ocean of the beautiful,” to be found in discourse, meditation and philosophy.
love03-plato1.md&3.10&>- Finally, we look for eternal and perfect beauty. This is only to be found beyond the realm of material things, in the world of the *ideas* behind the things.
love03-plato1.md&3.10&
love03-plato1.md&3.11&## Diotima’s ladder (4): Recap [211d, p.59]
love03-plato1.md&3.11&
love03-plato1.md&3.11&>- “Beginning from obvious beauties he must for the sake of that highest beauty be ever climbing aloft, as on the rungs of a ladder, from one to two, and from two to all beautiful bodies; from personal beauty he proceeds to beautiful observances, from observance to beautiful learning, and from learning at last to that particular study which is concerned with the beautiful itself and that alone; so that in the end he comes to know [211d] the very essence of beauty.”
love03-plato1.md&3.11&
love03-plato1.md&3.12&## Alcibiades (Socrates’ lover)
love03-plato1.md&3.12&
love03-plato1.md&3.12&>- Socrates has finished presenting Diotima’s theory. Suddenly, a drunk Alcibiades crashes in.
love03-plato1.md&3.12&>- Alcibiades is jealous that Socrates got a seat beside handsome Agathon [213c, p.62].
love03-plato1.md&3.12&>- He goes on to praise Socrates: Socrates, he says, is like one of these ugly statues that one can open, and inside they are hollow and beautifully painted and decorated [p.65].
love03-plato1.md&3.12&>- In this way, he illustrates Diotima’s point: that although Socrates looks like an ugly old man from the outside, if one looks at his inner qualities he is more beautiful than everybody else. And loving this inner beauty is a step up on Diotima’s ladder from just loving Socrates’ body.
love03-plato1.md&3.12&>- But Alcibiades has not yet reached the higher rungs of the ladder. He is still in love with Socrates’ (inner) beauty, unable to transcend it and see the absolute beauty of ideas (which is what Socrates does).
love03-plato1.md&3.12&
love03-plato1.md&4.0&# The text itself
love03-plato1.md&4.0&
love03-plato1.md&4.1&## Now let’s look at the text!
love03-plato1.md&4.1&
love03-plato1.md&4.1&Read the Symposion (highlighted passages).
love03-plato1.md&4.1&
love03-plato1.md&4.1&
love03-plato1.md&4.2&## The End. Thank you for your attention!
love03-plato1.md&4.2&
love03-plato1.md&4.2&Questions?
love03-plato1.md&4.2&
love03-plato1.md&4.2&
love03-plato1.md&4.2&<!--
love03-plato1.md&4.2&
love03-plato1.md&4.2&%% Local Variables: ***
love03-plato1.md&4.2&%% mode: markdown ***
love03-plato1.md&4.2&%% mode: visual-line ***
love03-plato1.md&4.2&%% mode: cua ***
love03-plato1.md&4.2&%% mode: flyspell ***
love03-plato1.md&4.2&%% End: ***
love03-plato1.md&4.2&
love03-plato1.md&4.2&-->
love04-plato2.md&0.1&---
love04-plato2.md&0.1&title:  "Love and Sexuality: 4. Plato’s theory of erotic love and Irving Singer’s criticism"
love04-plato2.md&0.1&author: Andreas Matthias, Lingnan University
love04-plato2.md&0.1&date: September 1, 2019
love04-plato2.md&0.1&...
love04-plato2.md&0.1&
love04-plato2.md&1.0&# Plato’s views on love
love04-plato2.md&1.0&
love04-plato2.md&1.1&## Diotima’s ladder
love04-plato2.md&1.1&
love04-plato2.md&1.1&>- Love of beautiful bodies, to beautiful works, to beautiful institutions, to beautiful knowledge, and finally to a glimpse of Beauty itself.
love04-plato2.md&1.1&>- The text of the Symposion itself illustrates Diotima’s ladder:
love04-plato2.md&1.1&>     - It begins with the bodily aspects of love (Phaedrus, Pausanias, Eryximachus).
love04-plato2.md&1.1&>     - It goes into philosophy and the search for truth (Aristophanes: union, Agathon: beauty and virtue).
love04-plato2.md&1.1&>     - It ends with Socrates, who is himself ugly (and therefore not a suitable object of cheap, bodily love), but he is a symbol of the much more important love of wisdom, which is true eros.
love04-plato2.md&1.1&>     - Alcibiades, at the end, illustrates the point by declaring his love of Socrates. He, the attractive young man, is himself attracted by Socrates’ inner qualities rather than the philosopher’s appearance.
love04-plato2.md&1.1&
love04-plato2.md&1.2&## Erotic love in ancient Greece
love04-plato2.md&1.2&
love04-plato2.md&1.2&>- “Platonic” love is not what we today think when we say that word.
love04-plato2.md&1.2&>- Homoeroticism was common and considered the highest kind of love.
love04-plato2.md&1.2&>- But it co-existed with other kinds of love: Socrates was married and Alcibiades had many affairs with women.
love04-plato2.md&1.2&
love04-plato2.md&1.3&## Sexual identity (from: Cooksey, Plato’s Symposium: A Reader’s Guide)
love04-plato2.md&1.3&
love04-plato2.md&1.3&>- “Sexual identity was understood in terms of the role in the sexual act, whether the partner was male or female.”
love04-plato2.md&1.3&>- “The masculine, as distinct from the male, was defined as the active part in the sexual relationship, and the partner as the passive part, independently of biological sex.”
love04-plato2.md&1.3&>- “The non-masculine partner was anyone ‘inferior’ to the masculine, whether in age, gender, or social status.”
love04-plato2.md&1.3&>- “Thus normal masculine behavior might focus on a younger man, a woman, or a slave. A boy might assume the passive role with an older man, but the active role with a younger boy.”
love04-plato2.md&1.3&>- The older man, the erastes (lover), was subject to desire (eros), while the boy, the eromenos (beloved) was the object of that desire, supposedly motivated by a mixture of admiration, gratitude, and affection.” (Cooksey, Plato’s Symposium, 12)
love04-plato2.md&1.3&
love04-plato2.md&1.4&## Ancient Greek eros is not symmetrical
love04-plato2.md&1.4&
love04-plato2.md&1.4&>- An erotic relationship is not symmetrical.
love04-plato2.md&1.4&>- There is the erastes (lover), the older, wiser man, who leads the relationship. 
love04-plato2.md&1.4&>- And the eromenos (beloved), who is usually a boy, and receives both the older man’s erotic attention and his protection and guidance in society. 
love04-plato2.md&1.4&>- A more (but not perfectly) symmetric relationship would be philia (friendship). See Aristotle for details (later in this class).
love04-plato2.md&1.4&
love04-plato2.md&1.5&## The word ‘erosic’
love04-plato2.md&1.5&
love04-plato2.md&1.5&>- Alan Soble uses the word “erosic” (from Greek ‘eros’), to denote Plato’s version of love.
love04-plato2.md&1.5&>- In this way, it is not confused with the modern usage of the word “erotic.”
love04-plato2.md&1.5&
love04-plato2.md&2.0&# Plato: Universals and ideas
love04-plato2.md&2.0&
love04-plato2.md&2.1&## Love as desire (1)
love04-plato2.md&2.1&
love04-plato2.md&2.1&>- Plato: “Love is desire for the perpetual possession of the good.”
love04-plato2.md&2.1&>- In principle, all desires are desires for something considered “good.” Otherwise why would we desire these things that are not good?
love04-plato2.md&2.1&>- But we might be mistaken about what we really want.
love04-plato2.md&2.1&>- In Diotima’s ladder, the lover who desires the beauty of the body is simply mistaken about his desire.
love04-plato2.md&2.1&>- Similarly, when we chase after money, what we want is not the money itself, but the happiness that (we hope) will result from being wealthy.
love04-plato2.md&2.1&>- But this might be a mistake. If material wealth didn’t promote happiness, then chasing after it would be counter-productive.
love04-plato2.md&2.1&>- The role of philosophy is to help us clarify what our ultimate desires are, so that we can direct our efforts towards our ultimate good.  (Irving Singer I,53 ff.)
love04-plato2.md&2.1&
love04-plato2.md&2.2&## Love as desire (2)
love04-plato2.md&2.2&
love04-plato2.md&2.2&>- It is important to see that, for Plato, love for a person is just a special case of a more general “love”: admiration for natural beauty, fascination with social order and ritual, love for wisdom.
love04-plato2.md&2.2&>- Some loves are “lower” (physical attraction to one’s boy- or girlfriend) and some are of “higher” quality (the love of the good, the love of perfect ideas). But both are variations of the same basic phenomenon.
love04-plato2.md&2.2&
love04-plato2.md&2.3&## Perfect circles
love04-plato2.md&2.3&
love04-plato2.md&2.3&>- Consider a circle drawn onto a piece of paper.
love04-plato2.md&2.3&>- This certainly will not be a *true* circle.
love04-plato2.md&2.3&>- It will be irregular at places, perhaps it will not even close properly.
love04-plato2.md&2.3&>- But that doesn’t matter. We can easily recognise that it is “supposed to be a circle.”
love04-plato2.md&2.3&>- Even mechanically drawn circles, or circles on a computer screen are never absolutely true and perfect circles.
love04-plato2.md&2.3&>- So how do we know what a circle is if we have never really seen one?
love04-plato2.md&2.3&>- Plato: all those specific examples of (imperfect) circles partake in the Idea (capital I) or Form (capital F) of a perfect circle.
love04-plato2.md&2.3&
love04-plato2.md&2.4&## Forms (Ideas)
love04-plato2.md&2.4&
love04-plato2.md&2.4&>- These perfect Forms of things are outside the realm of physical things.
love04-plato2.md&2.4&>- No physical circle can be perfect. No physical, existing tree is a perfect example of a tree. Existing things are always deficient in some way.
love04-plato2.md&2.4&>- Forms are perfect and they exist in a world of their own. The world of perfect things.
love04-plato2.md&2.4&>- The world of perfect things (Forms) is not the physical world, and it is also not just a world of the mind. For Plato, Forms are real, but *somewhere else.*
love04-plato2.md&2.4&
love04-plato2.md&2.4&
love04-plato2.md&2.5&## Forms and reality
love04-plato2.md&2.5&
love04-plato2.md&2.5&>- The physical world is a bad, imperfect picture of the world of Forms, in the same way as the shadow of a horse is an imperfect image of that horse.
love04-plato2.md&2.5&>- We live like people who are sitting in a cave, looking only at shadows on the cave wall. We know the real world (the world of Forms) only through its shadows (the physical things in our world).
love04-plato2.md&2.5&>- As an example of the cleverness of the Symposion’s construction, you could see the five speeches at the beginning as imperfect images or shadows of Diotima’s perfect explanation of the ladder of love.
love04-plato2.md&2.5&
love04-plato2.md&2.6&## The Symposion itself as a metaphor
love04-plato2.md&2.6&
love04-plato2.md&2.6&>- Thus the whole text provides an illustration of Plato’s theory of Forms: 
love04-plato2.md&2.6&>     - Diotima is the absent, not physically present participant. She is in another than the physical realm and she has the perfect knowledge of love.
love04-plato2.md&2.6&>     - The other speakers are physically (!) present and they have only faint images of the whole picture.
love04-plato2.md&2.6&>     - It is only Socrates, the wisest of them, who can approach Diotima’s wisdom and explain it to the others, make it accessible. 
love04-plato2.md&2.6&>     - But the eternal Form, Diotima herself, stays out of reach and out of the physical world of the Symposion.
love04-plato2.md&2.6&
love04-plato2.md&2.7&## Forms/Ideas (Singer)
love04-plato2.md&2.7&
love04-plato2.md&2.7&>- “Existence is merely the actualization of a form; the form, however, is real whether or not anything does exist.” (Singer, I,58)
love04-plato2.md&2.7&>- Properties of forms (Singer, I, 58):
love04-plato2.md&2.7&>     - Forms are “eternal” (that is, independent of time and space).
love04-plato2.md&2.7&>     - They neither come into being nor pass away (for they do not [materially] exist and cannot therefore cease to exist).
love04-plato2.md&2.7&>     - They don’t vary with the beholder (because they simply are what they are, the abstract green-ness or tree-ness).
love04-plato2.md&2.7&>     - They are unique (there can be only a single essence for each type of entity).
love04-plato2.md&2.7&>     - They are such that actual things partake of them (for all things that exist must be instances of some universal).
love04-plato2.md&2.7&
love04-plato2.md&2.7&
love04-plato2.md&3.0&#  Plato’s influence and other notes
love04-plato2.md&3.0&
love04-plato2.md&3.1&## Plato and Christianity
love04-plato2.md&3.1&
love04-plato2.md&3.1&There are obvious parallels between Plato and basic Christian beliefs. Consider Plato:
love04-plato2.md&3.1&
love04-plato2.md&3.1&. . . 
love04-plato2.md&3.1&
love04-plato2.md&3.1&>1. Love is desire.
love04-plato2.md&3.1&>2. All desires for lesser things are really part of a great “ladder” that leads to the ultimate desire for the (morally) good and beautiful.
love04-plato2.md&3.1&>3. This love is not limited to men, but part of the plan of nature.
love04-plato2.md&3.1&>4. Men might not always recognize this, but it is the truth, and this truth can be revealed by the study of philosophy.
love04-plato2.md&3.1&>5. Love for the idea of perfect beauty and goodness is the highest form of love (all other forms of love are parts of it and lead towards it.)
love04-plato2.md&3.1&> 6. Love strives for perpetual possession of the good. Since we are mortal, procreation, creativity, and the study of eternal things are the closest ways we can come to the perpetual possession of the good.
love04-plato2.md&3.1&
love04-plato2.md&3.2&## Christian version
love04-plato2.md&3.2&
love04-plato2.md&3.2&>1. Love is desire (for God).
love04-plato2.md&3.2&>2. All desires for lesser things are really part of a great “ladder” that leads to the ultimate desire for the moral perfection and beauty of God.
love04-plato2.md&3.2&>3. This love is not limited to men, but part of the whole plan of nature that strives towards God. (...)
love04-plato2.md&3.2&>4. Men might not always recognize this, but it is the truth, and this truth can be revealed by the study of philosophy (or God’s revelation in the Bible).
love04-plato2.md&3.2&>5. Love for God as the embodiment of perfect beauty and goodness is the highest form of love (all other forms of love are parts of it and lead towards it.)
love04-plato2.md&3.2&>6. Love strives for perpetual possession of the good. Since we are mortal, the perpetual possession of the good (contemplation of God in eternity) will occur in the afterlife.^[This comes from somewhere, but I’ve lost the reference.]
love04-plato2.md&3.2&
love04-plato2.md&3.3&## Singer’s criticism of merging and wedding unions
love04-plato2.md&3.3&
love04-plato2.md&3.3&>- Plato says that love is based on the desire to be “unified” with the good (compare Aristophanes’ contribution: the halves looking for their other half). 
love04-plato2.md&3.3&>- The fulfilment of love is union (with the other half, or, ultimately, with the idea of the good).
love04-plato2.md&3.3&>- Singer (p.65ff) distinguishes between two kinds of union:
love04-plato2.md&3.3&>     - Wedding: the “union” of meat and rice in a dish.
love04-plato2.md&3.3&>     - Merging: the “union” of tea and sugar in a hot tea.
love04-plato2.md&3.3&>- A “wedding” union retains the characteristics of the parts.
love04-plato2.md&3.3&>- A “merging” union dissolves the individual parts into a new whole.
love04-plato2.md&3.3&>- Merging; Mixing of paint into a new colour. Wedding: the union of members of a family, or colleagues in a company.
love04-plato2.md&3.3&
love04-plato2.md&3.4&## Wedding and merging
love04-plato2.md&3.4&
love04-plato2.md&3.4&>- Is Platonic eros an instance of merging or wedding?
love04-plato2.md&3.4&>- For physical love, it must be a wedding. Alcibiades and Socrates don’t physically merge. They don’t lose their individual identities.
love04-plato2.md&3.4&>- But for the love of perfect beauty, it must be a kind of merging. 
love04-plato2.md&3.4&>- Like in the Aristophanes myth, the philosopher remains “in constant union” (Plato) with the good. The true lover becomes god-like and immortal himself, Plato says. This is so, because he *unites* with the absolute good.
love04-plato2.md&3.4&>- But if this is a rational, philosophical process, is this likely? Thinking requires a distance, a rational agent. We would imagine a mystic or a shaman to *merge* with God, but not a philosopher.
love04-plato2.md&3.4&>- In Plato’s system, it is the *rational* recognition of the good that leads the philosopher up Diotima’s ladder. It is not a mystical process. 
love04-plato2.md&3.4&
love04-plato2.md&3.5&## Love and emotion
love04-plato2.md&3.5&
love04-plato2.md&3.5&>- How would Plato judge a love that is based on feelings (emotions) towards the loved person?
love04-plato2.md&3.5&>- He would say that this is essentially a mistaken form of love. It is an early stage of love, before one has ascended Diotima’s ladder. 
love04-plato2.md&3.5&>- When one matures as a lover of beauty, feelings towards particular persons have to be left behind, to make the contemplation of absolute beauty possible.
love04-plato2.md&3.5&>- This makes Platonic eros look quite strange as a kind of love.
love04-plato2.md&3.5&
love04-plato2.md&3.6&## Platonic exclusivity?
love04-plato2.md&3.6&
love04-plato2.md&3.6&>- What would Plato think of the idea of monogamy (marrying one partner only) or the romantic notion that there is the one perfect partner for everyone? (As in Aristophanes’ myth: the original other half.)
love04-plato2.md&3.6&>- He would say that this is nonsense.
love04-plato2.md&3.6&>- Erosic love is not limited to any one object of love.
love04-plato2.md&3.6&>- Instead, all individual objects of love are just steps in a ladder that leads up the final, single, *one* object of love. The steps don’t matter in themselves.
love04-plato2.md&3.6&
love04-plato2.md&3.7&## Appraisal and bestowal
love04-plato2.md&3.7&
love04-plato2.md&3.7&>- Is Platonic love “appraisal” or “bestowal” love?
love04-plato2.md&3.7&>- Platonic love is not “bestowal” love. The Platonic lover seeks the absolute perfection that is there in eternity. It is not created or altered in any way by the act of loving itself! 
love04-plato2.md&3.7&>- Platonic love is purely an act of appraisal of the eternal beauty as it shows itself in material things first, and in ideas later, and finally in the contemplation of the eternal beauty itself.
love04-plato2.md&3.7&>- Any problems with this idea?
love04-plato2.md&3.7&>- This means that we can never really love any actual human.
love04-plato2.md&3.7&>- *All humans are only ‘apparent’ objects of love* (Singer I,69).
love04-plato2.md&3.7&>- They *seem* or *appear* to be what we love, but this appearance is mistaken. We love the Idea instead (even if we don’t know it).
love04-plato2.md&3.7&
love04-plato2.md&3.8&## Devaluing of human love
love04-plato2.md&3.8&
love04-plato2.md&3.8&>- This position seems to devalue actual human love.
love04-plato2.md&3.8&>- If we never love any actual human being, then human beings become replaceable, irrelevant in themselves.
love04-plato2.md&3.8&>- Which other ancient school of philosophy has a similar understanding of love, and sees such an impersonal love also as the “proper” kind of love?
love04-plato2.md&3.8&>- The Stoics.
love04-plato2.md&3.8&>     - They have a whole theory about how it is wise to love only “kinds of things” instead of “individual things,” and how this is considered to be the best type of love (see Epictetus, Enchiridion).
love04-plato2.md&3.8&>     - The Stoics emphasize the possible loss of the loved thing or person. 
love04-plato2.md&3.8&>     - The only object of love that can’t be lost is the idea of the loved thing, or a whole class of things, of which each can replace every other, like beautiful bodies replace one another at the lower stages of Plato’s ladder of love.
love04-plato2.md&3.8&
love04-plato2.md&3.9&## Plato today
love04-plato2.md&3.9&
love04-plato2.md&3.9&>- What would Plato’s say about erotic love in today’s sense (that means, bodily, sexual love)?
love04-plato2.md&3.9&>- Sexual love is at the low stages of Diotima’s ladder.
love04-plato2.md&3.9&>- It is a passion that should be overcome in order for the lover to reach the higher forms of love.
love04-plato2.md&3.9&>- This is strange if we consider the modern use of “erotic.” We could say that Plato is ultimately anti-erotic or non-erotic.
love04-plato2.md&3.9&
love04-plato2.md&3.10&## Summary of Singer’s criticism
love04-plato2.md&3.10&
love04-plato2.md&3.10&Singer I,73:
love04-plato2.md&3.10&
love04-plato2.md&3.10&> “Platonism [...] ignores so much that is important to love: not only the bestowal of value, which is not reducible to reason, but also such feelings as tenderness, warmth, and that caring about in which bestowing manifests itself. Moreover, the emphasis upon sight tends to eliminate physical intimacy. The whole question of sexual relations seems to be pushed aside.”
love04-plato2.md&3.10&
love04-plato2.md&3.11&## The End. Thank you for your attention!
love04-plato2.md&3.11&
love04-plato2.md&3.11&Questions?
love04-plato2.md&3.11&
love04-plato2.md&3.11&
love04-plato2.md&3.11&
love04-plato2.md&3.11&<!--
love04-plato2.md&3.11&
love04-plato2.md&3.11&%% Local Variables: ***
love04-plato2.md&3.11&%% mode: markdown ***
love04-plato2.md&3.11&%% mode: visual-line ***
love04-plato2.md&3.11&%% mode: cua ***
love04-plato2.md&3.11&%% mode: flyspell ***
love04-plato2.md&3.11&%% End: ***
love04-plato2.md&3.11&
love04-plato2.md&3.11&-->
love05-aristotle1.md&0.1&---
love05-aristotle1.md&0.1&title:  "Love and Sexuality: 5. Aristotle overview"
love05-aristotle1.md&0.1&author: Andreas Matthias, Lingnan University
love05-aristotle1.md&0.1&date: September 1, 2019
love05-aristotle1.md&0.1&theme: lisbonsmall
love05-aristotle1.md&0.1&transition: fade
love05-aristotle1.md&0.1&slidenumber: 'c/t'
love05-aristotle1.md&0.1&autoslide: 0
love05-aristotle1.md&0.1&...
love05-aristotle1.md&0.1&
love05-aristotle1.md&1.0&# Intro
love05-aristotle1.md&1.0&
love05-aristotle1.md&1.1&## Aristotle (384-322 BC)
love05-aristotle1.md&1.1&
love05-aristotle1.md&1.1&>- How do Socrates, Plato and Aristotle relate to each other?
love05-aristotle1.md&1.1&>- Socrates was teacher of Plato. Plato was teacher of Aristotle.
love05-aristotle1.md&1.1&
love05-aristotle1.md&2.0&# Virtues
love05-aristotle1.md&2.0&
love05-aristotle1.md&2.1&## What is a virtue?
love05-aristotle1.md&2.1&
love05-aristotle1.md&2.1&Ethical virtues:^[Aristotle distinguishes between intellectual virtues (virtues of the mind), and the ethical virtues (virtues of character). For a first understanding of his theory, it is sufficient to look at the ethical virtues (like the three above), and to ignore the intellectual virtues.]
love05-aristotle1.md&2.1&
love05-aristotle1.md&2.1&- Courage
love05-aristotle1.md&2.1&- Kindness
love05-aristotle1.md&2.1&- Honesty
love05-aristotle1.md&2.1&
love05-aristotle1.md&2.1&... and many more.
love05-aristotle1.md&2.1&
love05-aristotle1.md&2.2&### Virtue (Greek: *arete*):
love05-aristotle1.md&2.2&A property of one's character that is beneficial to oneself and to others.
love05-aristotle1.md&2.2&
love05-aristotle1.md&2.3&## Virtues are good only in the right amount
love05-aristotle1.md&2.3&
love05-aristotle1.md&2.3&Too little | Virtue | Too much
love05-aristotle1.md&2.3&------------|----------|------------
love05-aristotle1.md&2.3&cowardice | courage | recklessness
love05-aristotle1.md&2.3&to be mean, selfish | kindness | to be a "doormat"
love05-aristotle1.md&2.3&liar, to deceive others | honesty | harsh, hurting others 
love05-aristotle1.md&2.3&
love05-aristotle1.md&2.3&Conclusion: Virtues are only good if the agent has them *in the right amount.*
love05-aristotle1.md&2.3&
love05-aristotle1.md&2.4&## What is the right amount of a virtue? (1)
love05-aristotle1.md&2.4&
love05-aristotle1.md&2.4&This poses the next question: "What *is* the right amount of virtue?"
love05-aristotle1.md&2.4&
love05-aristotle1.md&2.4&A common misunderstanding would be:
love05-aristotle1.md&2.4&
love05-aristotle1.md&2.4&- The right amount is the *middle* amount.
love05-aristotle1.md&2.4&- If cowardice is 0% courage, and recklessness is 100% courage, then the ideal would be 50% courage.
love05-aristotle1.md&2.4&
love05-aristotle1.md&2.4&Is this true? Or are there situations where it would be perfectly right to have 0% or 100% courage, instead of 50%?
love05-aristotle1.md&2.4&
love05-aristotle1.md&2.5&## What is the right amount of a virtue? (2)
love05-aristotle1.md&2.5&
love05-aristotle1.md&2.5&Situations where **0%** courage would be appropriate:
love05-aristotle1.md&2.5&
love05-aristotle1.md&2.5&- A lion attacks you and you have no weapon, only a pen. The best here would be to run, and not try to be courageous at all.
love05-aristotle1.md&2.5&- You see someone drowning at the beach, but you cannot swim yourself. It would be pointless to try to be brave and jump into the water. This would actually make it more difficult for the rescuers to rescue the other person, because now they'll have to rescue two people.
love05-aristotle1.md&2.5&
love05-aristotle1.md&2.6&## What is the right amount of a virtue? (3)
love05-aristotle1.md&2.6&
love05-aristotle1.md&2.6&Situations where **100%** courage would be appropriate:
love05-aristotle1.md&2.6&
love05-aristotle1.md&2.6&- You are a life saver at the beach, and now you see someone drown. In this situation, you *are* supposed to jump into the water and save them.
love05-aristotle1.md&2.6&- A lion attacks someone, and you are a policeman, trained in shooting, and you have your gun ready. Now you should be courageous and kill that lion, not run away.
love05-aristotle1.md&2.6&
love05-aristotle1.md&2.7&## What is the right amount of a virtue? (4)
love05-aristotle1.md&2.7&
love05-aristotle1.md&2.7&What do we learn from this?
love05-aristotle1.md&2.7&
love05-aristotle1.md&2.7&>- You cannot say that the ideal amount of a virtue is 50% virtue. Not only would this be hard to measure, but it is obviously not true.
love05-aristotle1.md&2.7&>- Sometimes the right amount of a virtue is 0%, sometimes 100%.
love05-aristotle1.md&2.7&>- It depends entirely on the situation.
love05-aristotle1.md&2.7&>- Even the same situation can call for different amounts of a virtue, depending on *who you are* in that situation (see above, life saver example).
love05-aristotle1.md&2.7&>- Conclusion: *The amount of virtue is not constant, but changes with every situation and with your role in it.*
love05-aristotle1.md&2.7&
love05-aristotle1.md&2.8&## What is the right amount of a virtue? (5)
love05-aristotle1.md&2.8&
love05-aristotle1.md&2.8&If it is true that every situation has its own "right amount" of virtue, the next question obviously is:
love05-aristotle1.md&2.8&
love05-aristotle1.md&2.8&"Then how do I know what the *right amount* of virtue is in every particular situation?"
love05-aristotle1.md&2.8&
love05-aristotle1.md&3.0&# Phronesis ("practical wisdom")
love05-aristotle1.md&3.0&
love05-aristotle1.md&3.1&## Phronesis (practical wisdom)
love05-aristotle1.md&3.1&
love05-aristotle1.md&3.2&### Definition:
love05-aristotle1.md&3.2&Phronesis (practical wisdom) is the ability to judge what is the right amount of every virtue in every particular situation.
love05-aristotle1.md&3.2&
love05-aristotle1.md&3.2&Having practical wisdom means two things:
love05-aristotle1.md&3.2&
love05-aristotle1.md&3.2&1. The wise person knows the *means* to certain *good ends* 
love05-aristotle1.md&3.2&2. The wise person knows *how much particular ends are worth* 
love05-aristotle1.md&3.2&
love05-aristotle1.md&3.3&## Do children have phronesis?
love05-aristotle1.md&3.3&
love05-aristotle1.md&3.3&Think of a 5-year old child. Does it have **virtues?**
love05-aristotle1.md&3.3&
love05-aristotle1.md&3.3&- Yes. Even a small child can be courageous, or honest.
love05-aristotle1.md&3.3&
love05-aristotle1.md&3.3&But can a 5-year old **judge the right amount** of courage or honesty in every situation?
love05-aristotle1.md&3.3&
love05-aristotle1.md&3.3&- No. Although the child possesses the virtues themselves, it needs to learn how much is the right amount. Otherwise he will hurt people by being too honest, or fall out of the window because he is too courageous and balances on the windowsill.
love05-aristotle1.md&3.3&
love05-aristotle1.md&3.3&For Aristotle, therefore, children do not have phronesis. They cannot act morally right by themselves.
love05-aristotle1.md&3.3&
love05-aristotle1.md&3.3&
love05-aristotle1.md&3.4&## How do people acquire phronesis?
love05-aristotle1.md&3.4&
love05-aristotle1.md&3.4&>- Own experience
love05-aristotle1.md&3.4&>- Education
love05-aristotle1.md&3.4&>- Knowledge of own abilities and shortcomings
love05-aristotle1.md&3.4&>- Exchange with friends, observation of friends' lives
love05-aristotle1.md&3.4&
love05-aristotle1.md&3.4&. . . 
love05-aristotle1.md&3.4&
love05-aristotle1.md&3.4&Although the own experience is most direct, not everything can or should be experienced directly (wars, extreme poverty, being the victim of a crime).
love05-aristotle1.md&3.4&
love05-aristotle1.md&3.4&We can know that being robbed is bad without actually having experienced it (from the tales of others who have).
love05-aristotle1.md&3.4&
love05-aristotle1.md&3.5&## Do older people have *more* phronesis?
love05-aristotle1.md&3.5&
love05-aristotle1.md&3.5&If children don't have phronesis, and older people have it, and phronesis grows through experience, is it true that the older you get the *more* phronesis you have?
love05-aristotle1.md&3.5&
love05-aristotle1.md&3.5&Do older people always have more phronesis than younger people?
love05-aristotle1.md&3.5&
love05-aristotle1.md&3.5&>- No.
love05-aristotle1.md&3.5&>- Because it is not the length of one's life that counts, but the actual experiences one has learned from.
love05-aristotle1.md&3.5&>- A shorter life can contain more experiences than a longer life.
love05-aristotle1.md&3.5&>- Even if one has had many experiences, it is also important whether one can *learn* from these experiences.
love05-aristotle1.md&3.5&
love05-aristotle1.md&3.6&## Can all people acquire phronesis equally well? (1)
love05-aristotle1.md&3.6&
love05-aristotle1.md&3.6&No. Some people can have a harder time getting new, different kinds of experiences.
love05-aristotle1.md&3.6&
love05-aristotle1.md&3.6&Factors that make acquiring phronesis from experience easier:
love05-aristotle1.md&3.6&
love05-aristotle1.md&3.6&>- A healthy body. People with disabilities will be limited in the kinds of experiences that are accessible to them. Only a healthy body will have maximum access to all experiences available to humans.
love05-aristotle1.md&3.6&>- A healthy, intelligent mind.
love05-aristotle1.md&3.6&>- Some reasonable amount of wealth. Extremely poor people cannot afford to have some of the possible experiences (e.g. travelling the world).
love05-aristotle1.md&3.6&>- Perhaps not too much wealth. Extremely rich people tend to be busy with many obligations, so that they too don't have the time and leisure to go out and make new experiences.
love05-aristotle1.md&3.6&
love05-aristotle1.md&3.7&## Can all people acquire phronesis equally well? (2)
love05-aristotle1.md&3.7&
love05-aristotle1.md&3.7&>- Living in a free society affords more possibilities to make new experiences, as opposed to a rigid, oppressive social system.
love05-aristotle1.md&3.7&>- A supportive environment is also helpful (supportive family, easy leave-taking from job, easy access to travel, libraries, media, friends, teachers).
love05-aristotle1.md&3.7&>- All these external factors are *necessary but not sufficient* for acquiring phronesis easily.
love05-aristotle1.md&3.7&>- Even under ideal conditions, some people will not want to make experiences or learn from them.
love05-aristotle1.md&3.7&>- On the other hand, there are always examples of extremely disadvantaged people who manage to acquire phronesis and live good lives. But Aristotle would say that these are exceptional, not the rule.
love05-aristotle1.md&3.7&
love05-aristotle1.md&4.0&# Eudaimonia
love05-aristotle1.md&4.0&
love05-aristotle1.md&4.1&## Life with perfect phronesis (1)
love05-aristotle1.md&4.1&
love05-aristotle1.md&4.1&Assuming you have acquired a great amount of practical wisdom:
love05-aristotle1.md&4.1&
love05-aristotle1.md&4.1&- You have all the virtues to exactly the right amount.
love05-aristotle1.md&4.1&- You are always doing the right thing, never too little or too much.
love05-aristotle1.md&4.1&- Your actions are always beneficial to yourself and to others.
love05-aristotle1.md&4.1&
love05-aristotle1.md&4.1&How will your life be?
love05-aristotle1.md&4.1&
love05-aristotle1.md&4.2&## Life with perfect phronesis (2)
love05-aristotle1.md&4.2&
love05-aristotle1.md&4.2&How will your life be?
love05-aristotle1.md&4.2&
love05-aristotle1.md&4.2&>- **Successful** (because everyone will like you, you will have many friends, your employers will praise your good judgement and your virtues, and so on.)
love05-aristotle1.md&4.2&>- **Morally good** (because your virtues, developed and used correctly, will always be beneficial to oneself and to others. Moral badness is a lack of virtue, which, we assumed, you wouldn't have.)
love05-aristotle1.md&4.2&>- **Happy** (how can you fail to be happy if you are successful in everything, morally good, with lots of friends, perfectly virtuous, and admired by everybody?)
love05-aristotle1.md&4.2&
love05-aristotle1.md&4.2&. . . 
love05-aristotle1.md&4.2&
love05-aristotle1.md&4.2&This kind of life is the *ideal human life* for Aristotle.
love05-aristotle1.md&4.2&
love05-aristotle1.md&4.3&## Eudaimonia
love05-aristotle1.md&4.3&
love05-aristotle1.md&4.4&### Eudaimonia:
love05-aristotle1.md&4.4&The performance of a life that is, at the same time, successful, morally good, and happy.
love05-aristotle1.md&4.4&
love05-aristotle1.md&4.4&**Eudaimonia is the ultimate goal of human life.**
love05-aristotle1.md&4.4&
love05-aristotle1.md&4.5&## Eudaimonia is the highest good
love05-aristotle1.md&4.5&
love05-aristotle1.md&4.5&>- Difference to Plato here: the highest good is *not* the contemplation of abstract beauty and perfection, but a morally good, successful, and happy life!
love05-aristotle1.md&4.5&>- We already saw Aristotle’s argument to support that a happy life is the ultimate goal of human beings:
love05-aristotle1.md&4.5&>     - Everybody wants happiness.
love05-aristotle1.md&4.5&>     - We want other things *for the sake of* happiness. I want money in order to be happy. I want a family in order to be happy. And so on.
love05-aristotle1.md&4.5&>     - Nobody would say: I want to be happy in order to earn more money. It just doesn’t make sense.
love05-aristotle1.md&4.5&>     - The same applies to fame, learning, work and everything else.
love05-aristotle1.md&4.5&>     - Conclusion: Happiness must be the ultimate goal of human life. All other aims are subordinate to it (less valuable).
love05-aristotle1.md&4.5&
love05-aristotle1.md&4.5&
love05-aristotle1.md&4.6&## Eudaimonia (the word)
love05-aristotle1.md&4.6&
love05-aristotle1.md&4.6&The word has two parts:
love05-aristotle1.md&4.6&
love05-aristotle1.md&4.6&- "eu-" means "good" in Greek.
love05-aristotle1.md&4.6&- A "daimon" (daemon) is a spirit or ghost.
love05-aristotle1.md&4.6&
love05-aristotle1.md&4.6&If you are "eudaimon," you have a life that is so good and successful, that is looks *like* it is guided by a good spirit.
love05-aristotle1.md&4.6&
love05-aristotle1.md&4.6&This is a metaphor only. Aristotle doesn't mean that there is actually such a spirit!
love05-aristotle1.md&4.6&
love05-aristotle1.md&4.7&## Can a criminal be eudaimon? (1)
love05-aristotle1.md&4.7&
love05-aristotle1.md&4.7&Imagine that an assassin has a good day, made a lot of money killing people, the weather is good, and he is happily going for a walk with his girlfriend. Out of all this good mood, he is giving a beggar on the street 100 HKD.
love05-aristotle1.md&4.7&
love05-aristotle1.md&4.7&Is this a morally valuable action? Is the criminal now a good man?
love05-aristotle1.md&4.7&
love05-aristotle1.md&4.8&## Can a criminal be eudaimon? (2)
love05-aristotle1.md&4.8&
love05-aristotle1.md&4.8&- For Aristotle, not isolated *actions* count, but only whether a *life* is eudaimon or not.
love05-aristotle1.md&4.8&- In other words, whether an agent is exercising his virtues in such a way as to maximise his phronesis and, therefore, his future eudaimonia.
love05-aristotle1.md&4.8&- The question is: will the criminal's action eventually lead to an increase in phronesis and, in the long term, to eudaimonia?
love05-aristotle1.md&4.8&
love05-aristotle1.md&4.9&## Can a criminal be eudaimon? (3)
love05-aristotle1.md&4.9&
love05-aristotle1.md&4.9&- No.
love05-aristotle1.md&4.9&- First, because although the criminal has *some* virtues (courage, for example), he does not have others (honesty, compassion).
love05-aristotle1.md&4.9&- His virtues are not *balanced.*
love05-aristotle1.md&4.9&- Second, although he does give the beggar money, this charitable action is not really part of his character. If he was generally charitable, he wouldn't be a criminal in the first place. His charity is "out of character." It is not *deeply* rooted in his character, but is *superficial.*
love05-aristotle1.md&4.9&- Third, although he has some virtues, he does not apply them correctly. His virtues are not beneficial to himself and to others.
love05-aristotle1.md&4.9&
love05-aristotle1.md&4.9&**Virtues need to be (1) deeply rooted in one's character and (2) well-balanced in order to count as real virtues.**
love05-aristotle1.md&4.9&
love05-aristotle1.md&4.9&
love05-aristotle1.md&4.10&## How do virtues become "deep?"
love05-aristotle1.md&4.10&
love05-aristotle1.md&4.10&- How do virtues become "deeply rooted in one's character?"
love05-aristotle1.md&4.10&- Similarly: how do any character traits or skills become deeply rooted in a person's character?
love05-aristotle1.md&4.10&- Through constant *practice.*
love05-aristotle1.md&4.10&
love05-aristotle1.md&4.11&## Handwriting
love05-aristotle1.md&4.11&
love05-aristotle1.md&4.11&Consider the skill of handwriting:
love05-aristotle1.md&4.11&
love05-aristotle1.md&4.11&>- When you were a small child, you learned to write.
love05-aristotle1.md&4.11&>- At first, this was very hard. You had to concentrate completely on just writing the shapes of the letters correctly, and you couldn't concentrate on the content of what you wrote at all.
love05-aristotle1.md&4.11&>- After you learned the letters, you began to write words. Again, this was so difficult that you couldn't imagine writing whole sentences at once.
love05-aristotle1.md&4.11&>- Later you had so much mastery of the letters and the words, that you could attempt to write sentences. Slowly, after long time of practising, you managed to learn that, too.
love05-aristotle1.md&4.11&>- Today you can take notes while listening to a lecture, and you can concentrate completely on the content of the lecture, without wasting a single thought on the writing process.
love05-aristotle1.md&4.11&>- The writing has become *automatic.* It is not a conscious effort at all any more.
love05-aristotle1.md&4.11&
love05-aristotle1.md&4.12&## Mastering skills
love05-aristotle1.md&4.12&
love05-aristotle1.md&4.12&The same principle applies to all skills (drawing, driving a car, walking, playing football, taking good photographs).
love05-aristotle1.md&4.12&
love05-aristotle1.md&4.12&**When you have mastered a skill, the skill becomes something you do automatically, without any conscious thought or effort.**
love05-aristotle1.md&4.12&
love05-aristotle1.md&4.13&## Mastering moral behaviour
love05-aristotle1.md&4.13&
love05-aristotle1.md&4.13&- Things work similarly for moral behaviour.
love05-aristotle1.md&4.13&- Through your constant exercise of phronesis, you become proficient in acting morally good, so that acting morally right becomes automatic.
love05-aristotle1.md&4.13&- You do it without conscious effort.
love05-aristotle1.md&4.13&
love05-aristotle1.md&4.14&## Three types of people (1)
love05-aristotle1.md&4.14&
love05-aristotle1.md&4.14&Now consider that someone in front of you, on the street, accidentally dropped some money, and didn't notice it. As he walks away, you pick up the money. What happens next?
love05-aristotle1.md&4.14&
love05-aristotle1.md&4.14&Three possibilities:
love05-aristotle1.md&4.14&
love05-aristotle1.md&4.14&>1. You keep the money and walk the other way.
love05-aristotle1.md&4.14&>2. You think for a moment whether you should keep the money or not, but then decide that it wouldn't be morally good to keep it, and that you should perhaps return it. Reluctantly, you call to the other person, and hand over the money. He thanks you and praises you for being morally good.
love05-aristotle1.md&4.14&>3. You immediately run to catch up with the other person and give him back his money without even the thought that you might keep it for yourself.
love05-aristotle1.md&4.14&
love05-aristotle1.md&4.15&## Three types of people (2)
love05-aristotle1.md&4.15&
love05-aristotle1.md&4.15&These three types of people Aristotle thinks of as *stages in the moral development* of humans:
love05-aristotle1.md&4.15&
love05-aristotle1.md&4.15&>1. The first type (the *akrates* in Greek) is the one who is always tempted to do the morally bad thing, and unable to resist the temptation. As a result, he always does the bad thing.
love05-aristotle1.md&4.15&>2. The second type (the *enkrates*) is one who is always tempted, but manages to overcome the temptation. As a result, he acts morally right, but only after some inner fight against himself.
love05-aristotle1.md&4.15&>3. The third type (the *sophron*) is the one who naturally and without effort does the right thing. He is not even tempted to do something that is not morally right. Long practice has made phronesis *automatic* in him.
love05-aristotle1.md&4.15&
love05-aristotle1.md&4.15&The human life is (ideally) a process of advancing from akrates$\to$enkrates$\to$sophron.
love05-aristotle1.md&4.15&
love05-aristotle1.md&4.16&## Three types of people (3)
love05-aristotle1.md&4.16&
love05-aristotle1.md&4.16&Compare this with other skills:
love05-aristotle1.md&4.16&
love05-aristotle1.md&4.16&>- For an expert walker, walking becomes automatic. He is never tempted to fall over while walking. A half-trained walker (like a small child, for example) is tempted to fall over, and sometimes does.
love05-aristotle1.md&4.16&>- For an expert writer, writing correctly is automatic. He is never tempted to write the letters or words wrongly. If you are occasionally in doubt about how you should write a word, then you are not yet an expert in writing, but a learner (like a student in primary school).
love05-aristotle1.md&4.16&
love05-aristotle1.md&4.16&
love05-aristotle1.md&4.16&
love05-aristotle1.md&5.0&# The End. Thank you for your attention!
love05-aristotle1.md&5.0&
love05-aristotle1.md&5.0&<!--
love05-aristotle1.md&5.0&
love05-aristotle1.md&5.0&%% Local Variables: ***
love05-aristotle1.md&5.0&%% mode: markdown ***
love05-aristotle1.md&5.0&%% mode: visual-line ***
love05-aristotle1.md&5.0&%% mode: cua ***
love05-aristotle1.md&5.0&%% mode: flyspell ***
love05-aristotle1.md&5.0&%% End: ***
love05-aristotle1.md&5.0&
love05-aristotle1.md&5.0&-->
love05-aristotle1.md&5.0&
love05-aristotle1.md&5.0&
love05-aristotle1.md&5.0&
love05-aristotle1.md&5.0&
love06-aristotle2.md&0.1&---
love06-aristotle2.md&0.1&title:  "Love and Sexuality: 6. Aristotle: Discussion"
love06-aristotle2.md&0.1&author: Andreas Matthias, Lingnan University
love06-aristotle2.md&0.1&date: September 1, 2019
love06-aristotle2.md&0.1&theme: lisbonsmall
love06-aristotle2.md&0.1&transition: fade
love06-aristotle2.md&0.1&slidenumber: 'c/t'
love06-aristotle2.md&0.1&autoslide: 0
love06-aristotle2.md&0.1&...
love06-aristotle2.md&0.1&
love06-aristotle2.md&0.1&
love06-aristotle2.md&1.0&# Introductory remark
love06-aristotle2.md&1.0&
love06-aristotle2.md&1.1&## How to locate quotes
love06-aristotle2.md&1.1&
love06-aristotle2.md&1.1&>- If you want to locate a piece of text in Aristotle (or many other ancient sources), the easiest way is to use Perseus:
love06-aristotle2.md&1.1&>- <http://www.perseus.tufts.edu/hopper/text?doc=Aristot.+Nic.+Eth.+1157b&fromdoc=Perseus%3Atext%3A1999.01.0053> 
love06-aristotle2.md&1.1&>- If you click on that, you’ll get to Nicomachean Ethics, 1157b. There you can also enter other numbers and jump directly to the text.
love06-aristotle2.md&1.1&>- You can also switch between Greek and English, if you want to look at particular words in the original language (e.g. philia, eros, agape etc). This can be useful even if you don’t fluently speak ancient Greek (as long as you learn the alphabet).
love06-aristotle2.md&1.1&
love06-aristotle2.md&1.1&
love06-aristotle2.md&2.0&# Aristotle on friendship and love
love06-aristotle2.md&2.0&
love06-aristotle2.md&2.0&
love06-aristotle2.md&2.1&## Virtue and function in Aristotle
love06-aristotle2.md&2.1&
love06-aristotle2.md&2.1&>- The Greek word for virtue (*arete*) is associated with *aristos* (excellent, best) and means the goodness or excellence of a kind of thing.
love06-aristotle2.md&2.1&>- “A thing’s virtue [or excellence] is *relative to its own proper function* (ergon), that is, the characteristic activity peculiar to something or its distinctive mark.”^[Jiyuan Yu (1998): Virtue: Confucius and Aristotle. Philosophy East&West, 48(2)]
love06-aristotle2.md&2.1&>     - A good pen is a pen that writes well, that is, a pen that fulfils its distinctive function well. A good tree is a tree that performs its function of being a tree well.
love06-aristotle2.md&2.1&>     - “Any kind of thing can be said to possess its (specific) virtue by performing its function well.” (op.cit)
love06-aristotle2.md&2.1&>- Aristotle: "The virtue of a human being will likewise be the state that makes a human being good and makes him perform his function well." (1106a23-24).
love06-aristotle2.md&2.1&>- What is the distinctive “function” of humans? 
love06-aristotle2.md&2.1&>     - To be virtuous in a rational way. No other animals can rationally control their virtues (=have phronesis).
love06-aristotle2.md&2.1&
love06-aristotle2.md&2.2&## General characteristics of Aristotle’s theory of love (1)
love06-aristotle2.md&2.2&
love06-aristotle2.md&2.2&>- From what you know now about Aristotle, try to guess what his opinions on love might be!
love06-aristotle2.md&2.2&>- Every virtue results from moderation between the extremes of lack and excess. Love should exist in moderation.
love06-aristotle2.md&2.2&>     - The point of all virtues is that they are based on rational evaluation of the right amount. When rationality is affected or the virtue’s exercise becomes irrational, it is not a virtue any more.
love06-aristotle2.md&2.2&>     - Indifference to others is lack; sexual eros is excess (because it clouds the rational mind). So what is the optimal level of love?
love06-aristotle2.md&2.2&>     - Companionship, friendship. Clear-eyed, rational, serving the benefit of both the lover and the beloved.
love06-aristotle2.md&2.2&
love06-aristotle2.md&2.3&## General characteristics of Aristotle’s theory of love (2)
love06-aristotle2.md&2.3&
love06-aristotle2.md&2.3&>- The ultimate goal of human life is eudaimonia.
love06-aristotle2.md&2.3&>     - So love has to serve the eudaimonia (happiness) of all participants.
love06-aristotle2.md&2.3&>     - Overly emotional love that makes people unhappy is not the right thing. It is a (bad) passion rather than a (good) virtue, exercised with phronesis.
love06-aristotle2.md&2.3&>- We need friends in order to learn phronesis and progress towards eudaimonia.
love06-aristotle2.md&2.3&>     - So we need to select our lovers so that they help us improve.
love06-aristotle2.md&2.3&>     - This will likely be the case if our lovers are close to us in terms of phronesis. A criminal being friends with a saint won’t make for a mutually useful relationship. The same applies to old/young, educated/uneducated and so on.
love06-aristotle2.md&2.3&>     - Aristotle’s relationships are based on mutual usefulness and the lovers have to be roughly equal in phronesis, so that the relationship benefits both. Otherwise it’s a teacher/student relationship, but not *love*.
love06-aristotle2.md&2.3&
love06-aristotle2.md&2.4&## “Philia”
love06-aristotle2.md&2.4&
love06-aristotle2.md&2.4&>- The word “philia” for Aristotle can mean different things:
love06-aristotle2.md&2.4&>     - Friendship.
love06-aristotle2.md&2.4&>     - The feelings one has towards one’s family.
love06-aristotle2.md&2.4&>     - The feelings towards people we share interests or hobbies with.
love06-aristotle2.md&2.4&>- Aristotle gives examples of philia: Young lovers, lifelong friends, partner cities, business contacts, parents and children, fellow-voyagers, members of the same religious society, business owners and their customers.
love06-aristotle2.md&2.4&>- Also: “The friendship of children to parents, and of men to gods” (NE VIII, 12, p. 141).
love06-aristotle2.md&2.4&>- We see that this is quite a bit more than what English “friendship” means.
love06-aristotle2.md&2.4&
love06-aristotle2.md&2.5&## Definition of friendship (1)
love06-aristotle2.md&2.5&
love06-aristotle2.md&2.5&>- Friendship is a relationship in which persons **similarly** love each other, and in which they reciprocally wish good things to each other “in that very respect in which they love each other.” (Nicomachean Ethics 8.3).
love06-aristotle2.md&2.5&>- Remember that Eros is an asymmetric relationship. The older erastes and the younger eromenos don’t love each other “similarly.” This is how friendship differs from Eros.
love06-aristotle2.md&2.5&
love06-aristotle2.md&2.6&## Definition of friendship (2)
love06-aristotle2.md&2.6&
love06-aristotle2.md&2.6&>- In the Rhetoric: “[Philia is] wanting for someone what one thinks good, for his sake and not for one’s own, and being inclined, so far as one can, to do such things for him.” (1380b36–1381a2)”
love06-aristotle2.md&2.6&>- John M. Cooper (“Aristotle on the Forms of Friendship,” The Review of Metaphysics 30, 1976–1977, pp. 619–648): “the central idea of φιλíα is that of doing well by someone for his own sake, out of concern for him (and not, or not merely, out of concern for oneself).”
love06-aristotle2.md&2.6&>- Aristotle says that philia is necessary for happiness, since all men need friendship. 
love06-aristotle2.md&2.6&
love06-aristotle2.md&2.7&## Kinds of love and friendship
love06-aristotle2.md&2.7&
love06-aristotle2.md&2.7&>- Aristotle thinks that we value a person only because of their goodness or usefulness (NE 8.2): “For not everything seems to be loved but only the lovable, and this is good, pleasant, or useful.”
love06-aristotle2.md&2.7&>- There are two ways in which someone may be good or pleasant/useful: 
love06-aristotle2.md&2.7&>     - Someone may be good or pleasant/useful either “in his own right” 
love06-aristotle2.md&2.7&>     - or “in relation to you.” 
love06-aristotle2.md&2.7&
love06-aristotle2.md&2.8&## Complete friendship
love06-aristotle2.md&2.8&
love06-aristotle2.md&2.8&>- Someone is “good in his own right” if he is a good human being, virtuous, acting according to the virtues etc. That is, a person with phronesis.
love06-aristotle2.md&2.8&>- To love such a person is pleasant and valuable in itself.
love06-aristotle2.md&2.8&>- A complete friendship is a friendship in which this sort of love is reciprocated.
love06-aristotle2.md&2.8&>- So, in a complete friendship, both friends are good, virtuous, sophron human beings.
love06-aristotle2.md&2.8&
love06-aristotle2.md&2.9&## Usefulness and pleasure
love06-aristotle2.md&2.9&
love06-aristotle2.md&2.9&>- Some people are “good in relation to you,” that is, they are useful to you.
love06-aristotle2.md&2.9&>- Others are “pleasant in relation to you”: they are entertaining.
love06-aristotle2.md&2.9&>- Someone may be useful without being entertaining, or entertaining without being useful.
love06-aristotle2.md&2.9&>- These two relations define two further kinds of friendship:
love06-aristotle2.md&2.9&>     - Friendships based on usefulness; and
love06-aristotle2.md&2.9&>     - friendships based on shared entertainment and pleasure.
love06-aristotle2.md&2.9&
love06-aristotle2.md&2.10&## Are usefulness and pleasure really different?
love06-aristotle2.md&2.10&
love06-aristotle2.md&2.10&>- Can you give an example to show that these two types of friendship are really distinct?
love06-aristotle2.md&2.10&>- If you are involved in a traffic accident with others, you might help them and they might help you (usefulness), but you may not like them at the same time. Usefulness does not need liking. Or, a friend who tells good jokes might be pleasant to have around, but otherwise not useful to you.
love06-aristotle2.md&2.10&>- Same with shopping: the people who sell you something in a shop are useful to you, but they don’t become your friends.
love06-aristotle2.md&2.10&
love06-aristotle2.md&2.11&## Philia vs frienship
love06-aristotle2.md&2.11&
love06-aristotle2.md&2.11&>- How is Aristotle’s philia different from our modern English “friendship?”
love06-aristotle2.md&2.11&>     - We would call only “complete friendship” a real friendship.
love06-aristotle2.md&2.11&>     - We would not accept “friendship for usefulness” as genuine friendship at all.
love06-aristotle2.md&2.11&>     - “Friendship for pleasure” would be somewhere in between, a weak form of sympathy from amusement, but not “real” friendship either.
love06-aristotle2.md&2.11&
love06-aristotle2.md&2.12&## Similarity and friendship
love06-aristotle2.md&2.12&
love06-aristotle2.md&2.12&>- In Aristole’s view, do similar or dissimilar persons become friends?
love06-aristotle2.md&2.12&>- Remember that for Aristotle, friendship is symmetrical, that is, the love of one friend answers to the love of the other in roughly equal ways. Therefore, friends will tend to be similar in character and virtue.
love06-aristotle2.md&2.12&>- Ancient Greek “pederasty” (the sexual love between older and younger men) is not a symmetrical relationship. Therefore, the two parts will not be able to fully relate as equals to each other and their relationship will be unstable and not a true friendship. The older man will not be able to love the virtue in the younger, and the younger will not be able to love the body of the older.
love06-aristotle2.md&2.12&
love06-aristotle2.md&2.13&## Goodness and complete friendship
love06-aristotle2.md&2.13&
love06-aristotle2.md&2.13&>- What is the relationship between goodness and friendship? Can bad people be friends with each other?
love06-aristotle2.md&2.13&>- Complete friendship is based on good character, so only good persons can be friends in a complete friendship.
love06-aristotle2.md&2.13&>- But this does not matter as much for the other kinds of friendship: I can be entertained by someone or find him useful, despite the fact that he is not a good person.
love06-aristotle2.md&2.13&
love06-aristotle2.md&2.14&## Can we love things?
love06-aristotle2.md&2.14&
love06-aristotle2.md&2.14&>- Can we love things, for example: wine? What would Aristotle say?
love06-aristotle2.md&2.14&>- I can talk of “loving wine,” but I cannot be friends with wine.
love06-aristotle2.md&2.14&>- Since Aristotle’s love is reciprocal, I’d have to expect the wine to love me back. 
love06-aristotle2.md&2.14&>- Also, philia for Aristotle means to “wish the other well.” But we cannot wish the wine well. We can only wish that *it keeps well,* so that we can have it to ourselves.
love06-aristotle2.md&2.14&>- Therefore, no true love (friendship) is possible towards things. The talk of “loving” wine is wrong and not meant to be taken seriously.
love06-aristotle2.md&2.14&
love06-aristotle2.md&2.15&## Loving animals? (1)
love06-aristotle2.md&2.15&
love06-aristotle2.md&2.15&>- Can I love an animal? Can I have “philia” towards my cat or dog?
love06-aristotle2.md&2.15&>- “[Philia is] wanting for someone what one thinks good, for his sake and not for one’s own, and being inclined, so far as one can, to do such things for him.” (1380b36–1381a2)”
love06-aristotle2.md&2.15&>- John M. Cooper (“Aristotle on the Forms of Friendship,” The Review of Metaphysics 30, 1976–1977, pp. 619–648): “the central idea of φιλíα is that of doing well by someone for his own sake, out of concern for him (and not, or not merely, out of concern for oneself).”
love06-aristotle2.md&2.15&>- If we see it like this, yes, I can love an animal. I can wish my dog well and I can do good things for it. I can “do well by my dog” for its own sake.
love06-aristotle2.md&2.15&
love06-aristotle2.md&2.16&## Loving animals? (2)
love06-aristotle2.md&2.16&
love06-aristotle2.md&2.16&>- Also, things (or people) are lovable because they are “good, pleasant or useful.” (NE 8.2)
love06-aristotle2.md&2.16&>- Can a dog be good, pleasant and useful?
love06-aristotle2.md&2.16&>- A dog can be pleasant and useful. These are the two main reasons why people keep dogs. 
love06-aristotle2.md&2.16&>- A dog cannot be good in itself, because this would mean that the dog is virtuous and has phronesis. Only human beings can be “good” in a moral sense.
love06-aristotle2.md&2.16&>- Consequently, I can have a relationship based on usefulness or pleasure with an animal. But it cannot be real friendship.
love06-aristotle2.md&2.16&
love06-aristotle2.md&2.17&## Loving animals? (3)
love06-aristotle2.md&2.17&
love06-aristotle2.md&2.17&>- Also, real friendship is a relationship in which persons **similarly** love each other, and in which they reciprocally wish good things to each other “in that very respect in which they love each other.” (Nicomachean Ethics 8.3).
love06-aristotle2.md&2.17&>- It is not clear that a dog can do that, and much less a cat or a bird in a cage. A dog might try to help its owner in an emergency, but is this really “wishing good things” to the human?
love06-aristotle2.md&2.17&>- In order to “wish good things” I need to know what the other considers ‘good.’ I need to have some empathy. 
love06-aristotle2.md&2.17&>     - For example, most humans would value a good job, a good and secure income, health and a happy family.
love06-aristotle2.md&2.17&>     - Does a dog really wish these things to its owner?
love06-aristotle2.md&2.17&>- It seems that reciprocity is severely limited in the case of love towards animals.
love06-aristotle2.md&2.17&
love06-aristotle2.md&2.18&## Loving a country (1)
love06-aristotle2.md&2.18&
love06-aristotle2.md&2.18&>- Can we love a country? Can I be friends with Greece, for example?
love06-aristotle2.md&2.18&>- Again: “[Philia is] wanting for someone what one thinks good, for his sake and not for one’s own, and being inclined, so far as one can, to do such things for him.” (1380b36–1381a2)”
love06-aristotle2.md&2.18&>- And: John M. Cooper (“Aristotle on the Forms of Friendship,” The Review of Metaphysics 30, 1976–1977, pp. 619–648): “the central idea of φιλíα is that of doing well by someone for his own sake, out of concern for him (and not, or not merely, out of concern for oneself).”
love06-aristotle2.md&2.18&>- I can certainly wish my country well (as opposed to the wine). I can do good things to my country. I can be concerned about my country.
love06-aristotle2.md&2.18&
love06-aristotle2.md&2.18&
love06-aristotle2.md&2.19&## Loving a country (2)
love06-aristotle2.md&2.19&
love06-aristotle2.md&2.19&>- Also, things (or people) are lovable because they are “good, pleasant or useful.” (NE 8.2)
love06-aristotle2.md&2.19&>- Can a country be good, pleasant and useful?
love06-aristotle2.md&2.19&>- A country can be pleasant and useful. Greece is pleasant as a place where I spend my holidays. Being a citizen of Greece is useful to me, because I then become a citizen of the EU, I can get employment in the EU and so on.
love06-aristotle2.md&2.19&>- But can a country be good by itself? Can it, as a country, have virtues and phronesis?
love06-aristotle2.md&2.19&>- (See also next slide: functionalism)
love06-aristotle2.md&2.19&
love06-aristotle2.md&2.19&
love06-aristotle2.md&2.20&## Loving a country (3)
love06-aristotle2.md&2.20&
love06-aristotle2.md&2.20&>- Can a country reciprocate my feelings? Can it “wish me well”?
love06-aristotle2.md&2.20&>- This is difficult to answer. A social system can take care of me. A country’s hospitals and other infrastructure (roads, playgrounds, parks) may “do well by me” and help me live a good life.
love06-aristotle2.md&2.20&>- But does a country do this consciously? 
love06-aristotle2.md&2.20&>- If a forest is good to me (it relaxes me, it improves my health) can I say that the forest “wishes me well”?
love06-aristotle2.md&2.20&>- Probably not. 
love06-aristotle2.md&2.20&>- But a country is more complex than a forest. It acts, raises taxes, builds streets, protects the citizens. Does it also “wish”?
love06-aristotle2.md&2.20&>     - This depends on how we approach mental states like virtues or wishes.
love06-aristotle2.md&2.20&>     - If we are *functionalists* about mental states, we might say that a country has a mental state (e.g. makes a wish) if that “wish” relates to its other states in the same way as a human wish relates to other mental states inside a human brain. A country, in this view, *might* be able to wish, love, threaten etc.
love06-aristotle2.md&2.20&
love06-aristotle2.md&2.20&
love06-aristotle2.md&3.0&# Plato and Aristotle
love06-aristotle2.md&3.0&
love06-aristotle2.md&3.1&## Main differences between Plato’s Aristotle’s love (1)
love06-aristotle2.md&3.1&
love06-aristotle2.md&3.1&>- Try to summarise the main differences between the concepts of love in the Symposion and Aristotle’s writings!
love06-aristotle2.md&3.1&>- Plato’s love is asymmetrical. Eros is directed towards the good, either as it is personified in particular people, or the abstract idea of the good itself. Lovers have different power and get treated differently in a relationship, and the good itself does not love you back.
love06-aristotle2.md&3.1&>- Aristotle’s love is ideally symmetrical. Both parties wish each other well, they both enjoy spending time together etc. If friendships based on utility are asymmetrical, this can work for a while, but such a relationship is unstable.
love06-aristotle2.md&3.1&>- Plato’s love is at least passionate; Aristotle’s love is a kind of rational exchange of goods: utility, pleasure, or moral character.
love06-aristotle2.md&3.1&
love06-aristotle2.md&3.2&## Main differences between Plato’s Aristotle’s love (2)
love06-aristotle2.md&3.2&
love06-aristotle2.md&3.2&>- On the other hand, Aristotle does acknowledge the human who is on the other end of a relationship more than Plato does. 
love06-aristotle2.md&3.2&>     - For Plato, every human is just the carrier of some part of the idea of the good and beautiful. 
love06-aristotle2.md&3.2&>     - For Aristotle, humans are valuable as carriers of virtue, and their personalities actually are important for love. Not every human being will be an equally good friend.
love06-aristotle2.md&3.2&>     - Singer (I, 91): “The Aristotelian friends are related to one another as something more than vehicles to the highest form. If theirs is not the love of persons, at least it is the love of good character in other persons.”
love06-aristotle2.md&3.2&
love06-aristotle2.md&3.3&## Love as moderation between extremes
love06-aristotle2.md&3.3&
love06-aristotle2.md&3.3&>- You remember that, for Aristotle, virtue is the moderation between extremes of character.
love06-aristotle2.md&3.3&>- Aristotle describes *philia* as the highest kind of love. *Eros* for him has a more specific, sexual meaning (different from Plato!)
love06-aristotle2.md&3.3&>- Eros, for Aristotle, is a “desire for pleasure” and a “sort of excess in feeling.”
love06-aristotle2.md&3.3&>- Generally, sexual love (eros) is an extreme and true friendship (philia) is the (good) mean (Singer I, 92).
love06-aristotle2.md&3.3&
love06-aristotle2.md&3.3&
love06-aristotle2.md&4.0&# Is Aristotle’s love a bestowal or appraisal love?
love06-aristotle2.md&4.0&
love06-aristotle2.md&4.1&## Appraisal?
love06-aristotle2.md&4.1&
love06-aristotle2.md&4.1&>- Could we classify Aristotle’s love as an “appraisal” love? Why?
love06-aristotle2.md&4.1&>- Irving Singer believes that Aristotle’s love is purely an “appraisal” kind of love.
love06-aristotle2.md&4.1&>- When we love our friends, we appraise their qualities of character and we directly respond to these qualities.
love06-aristotle2.md&4.1&>- If our friends didn’t have the required qualities, they would not be suitable as our friends and we wouldn’t love them.
love06-aristotle2.md&4.1&
love06-aristotle2.md&4.2&## Bestowal (1)
love06-aristotle2.md&4.2&
love06-aristotle2.md&4.2&>- Are there any “bestowal” elements in Aristotle’s philia?
love06-aristotle2.md&4.2&>- But Richard Kraut (“The Importance of Love in Aristotle’s Ethics”, 1975) makes another point.
love06-aristotle2.md&4.2&>- “Those who love have an intense desire to benefit someone for his own sake. If one wants to help another solely as a way of benefiting or pleasing oneself, then one “loves” that individual only by an extension of the term (1156al4-19, 1157a30-32). Commercial partnerships (1158a21) and purely sexual relationships (1156bl-3) typify such “incidental” forms of love; one person aids or pleases the other only to increase his own wealth or to enhance his own physical pleasure.”
love06-aristotle2.md&4.2&>- “When love is real, on the other hand, the advantages and pleasures one might receive will be valued, but one does not help the person one loves merely as a means to such goods.”
love06-aristotle2.md&4.2&
love06-aristotle2.md&4.3&## Bestowal (2)
love06-aristotle2.md&4.3&
love06-aristotle2.md&4.3&>- “Aristotle recognizes two very different types of genuine love. In one case, we respond to an individual's character and love him for the kind of person he is. This love begins with good will, a mild well-wishing caused by the recognition of some apparent virtue in a person. As one discovers through greater acquaintance that the object of one's good will really is virtuous and therefore worthy of one’s love (1156b25-32), and as one continues to act on one's desire to benefit him for his own sake, this desire, at first weak, strengthens until it becomes love.”
love06-aristotle2.md&4.3&>- “One comes to love the object of one's benevolence just as a craftsman loves the artifact he has created (1167b31-1168a9). Both have gone to considerable lengths and have exercised their skills, and they see their efforts embodied in the person benefited or product created.”
love06-aristotle2.md&4.3&
love06-aristotle2.md&4.4&## Bestowal (3)
love06-aristotle2.md&4.4&
love06-aristotle2.md&4.4&>- This now sounds much more like bestowal. 
love06-aristotle2.md&4.4&>- It is not only the immediate appreciation of another’s character that causes me to love them.
love06-aristotle2.md&4.4&>- Instead, the emerging love is the product of my virtuous interaction with the other person, my being benevolent towards them and benefiting them.
love06-aristotle2.md&4.4&>- In this sense, we could imagine that almost any other person (within limits) could be the object of our virtuous interaction, our “love bestowal.”
love06-aristotle2.md&4.4&>- Love, in this sense, is *created* by the lover, rather than just a response to features of the beloved.
love06-aristotle2.md&4.4&
love06-aristotle2.md&4.5&## Bestowal (4)
love06-aristotle2.md&4.5&
love06-aristotle2.md&4.5&>- Kraut: “There is a second form of love, however, which has very different features. This is the love of one's child, which is natural to all parents (1155a16-18) and arises immediately at birth (1161b24-25).”
love06-aristotle2.md&4.5&>- “Rather than developing gradually with increased acquaintance, it arises suddenly with full intensity, and it is not caused by the perception of some good quality in the person loved. We love our children for their own sake (ekeinon heneka), but not for themselves (di'hautous, kath'hautous); that is, not for their character.”
love06-aristotle2.md&4.5&>- This can be disputed. Erich Fromm, for example, in “The Art of Loving” (1956) distinguishes between “father’s love,” which is conditional on the qualities and behaviour of the child; and “mother’s love” which is supposed to be unconditional.
love06-aristotle2.md&4.5&>- In any case, love for one’s children would be a pure “bestowal” love for Aristotle.
love06-aristotle2.md&4.5&
love06-aristotle2.md&5.0&# Is love an emotion?
love06-aristotle2.md&5.0&
love06-aristotle2.md&5.1&## Is love an emotion for Aristotle? (1)
love06-aristotle2.md&5.1&
love06-aristotle2.md&5.1&>- Is love an emotion for Aristotle?
love06-aristotle2.md&5.1&>- No. The feeling/emotion he calls *philesis,* and he distinguishes it from the character trait *philia:*
love06-aristotle2.md&5.1&>     - “Liking seems to be an emotion, friendship a fixed disposition, for liking can be felt even for inanimate things, but reciprocal liking involves deliberate choice, and this springs from a fixed disposition.” (1157b)
love06-aristotle2.md&5.1&>- Kraut: “Loving someone, in other words, is not a matter of being struck by an urge, but is rather a fixed attitude that reflects one's values. That love also involves experiencing an emotion if not denied (1126b11).”
love06-aristotle2.md&5.1&
love06-aristotle2.md&5.1&
love06-aristotle2.md&5.2&## Is love an emotion for Aristotle? (2)
love06-aristotle2.md&5.2&
love06-aristotle2.md&5.2&>- This is different from modern understanding:
love06-aristotle2.md&5.2&>     - Love: “a feeling of strong personal attachment” and “ardent affection” (Webster's New International Dictionary, 2nd ed., 1959)
love06-aristotle2.md&5.2&>     - “Companionate love... combines feelings of deep attachment, commitment, and intimacy” (Hatfield and Rapson, Handbook of Emotions (2000: 655, both cited after Konstan (2008): Aristotle on Love and Friendship, Schole, II(2))
love06-aristotle2.md&5.2&>- Aristotle says nothing about feelings and focuses exclusively on the intention to benefit the other person who is one’s friend (Konstan).
love06-aristotle2.md&5.2&
love06-aristotle2.md&5.2&
love06-aristotle2.md&6.0&# Is (self-) love a virtue?
love06-aristotle2.md&6.0&
love06-aristotle2.md&6.1&## Is Aristotle’s love essentially egoistic? (1)
love06-aristotle2.md&6.1&
love06-aristotle2.md&6.1&>- You could argue that Aristotle’s virtues are egoistic, because what we want from exercising them is, ultimately, to promote our own *eudaimonia.* Is this a correct reading of Aristotle?
love06-aristotle2.md&6.1&>- Probably not. Aristotle wants us to exercise our virtues not *because* we will benefit ourselves, but *because* we want to help others, be good, honest, caring and so on.
love06-aristotle2.md&6.1&>- The benefit to us comes as a *consequence* of these actions, but should not be their driving force.
love06-aristotle2.md&6.1&
love06-aristotle2.md&6.2&## Is Aristotle’s love essentially egoistic? (2)
love06-aristotle2.md&6.2&
love06-aristotle2.md&6.2&>- Example: You find something valuable on the street and give it back. As a consequence, you get a reward. What would Aristotle say? Are you an egoist because you wanted the reward?
love06-aristotle2.md&6.2&>- No. If you are *sophron,* then you returned the thing *because* you have, through long practice, achieved the state of acting virtuously automatically.
love06-aristotle2.md&6.2&>- You don’t choose your action consciously, and you have no choice. You couldn’t have kept the thing. It’s in your character to return it.
love06-aristotle2.md&6.2&>- The reward might come, and it might be pleasant, but it’s not the *reason* you returned the thing.
love06-aristotle2.md&6.2&
love06-aristotle2.md&6.2&
love06-aristotle2.md&6.3&## Should we love ourselves? (1)
love06-aristotle2.md&6.3&
love06-aristotle2.md&6.3&>- What would Aristotle say about self-love?
love06-aristotle2.md&6.3&>- Aristotle has a discussion on p.155/156 (Book IX, 8) of our edition of the Nicomachean Ethics (see Moodle).
love06-aristotle2.md&6.3&>- “The question is also debated, whether a man should love himself most, or some one else. People criticize those who love themselves most, and call them self-lovers, using this as an epithet of disgrace, and a bad man seems to do everything for his own sake, and the more so the more wicked he is—and so men reproach him, for instance, with doing nothing of his own accord—while the good man acts for honour’s sake, and the more so the better he is, and acts for his friend’s sake, and sacrifices his own interest.”
love06-aristotle2.md&6.3&
love06-aristotle2.md&6.4&## Should we love ourselves? (2)
love06-aristotle2.md&6.4&
love06-aristotle2.md&6.4&>- But, Aristotle says, all these properties apply first to the relation one has with oneself.
love06-aristotle2.md&6.4&>- “For men say that one ought to love best one’s best friend, and man’s best friend is one who wishes well to the object of his wish for his sake, even if no one is to know of it; and these attributes are found most of all in a man’s attitude towards himself, and so are all the other attributes by which a friend is defined...”
love06-aristotle2.md&6.4&
love06-aristotle2.md&6.5&## Should we love ourselves? (3)
love06-aristotle2.md&6.5&
love06-aristotle2.md&6.5&>- “Living according to a rational principle is [different] from living as passion dictates, and desiring what is noble from desiring what seems advantageous. Those, then, who busy themselves in an exceptional degree with noble actions all men approve and praise; and if all were to strive towards what is noble and strain every nerve to do the noblest deeds, everything would be as it should be ... Therefore the good man should be a lover of self (for he will both himself profit by doing noble acts, and will benefit his fellows), but the wicked man should not; for he will hurt both himself and his neighbours, following as he does evil passions.”
love06-aristotle2.md&6.5&>- So we *are* allowed to love ourselves, but only if this self-love is the basis for a virtuous character that seeks to benefit others as one loves oneself.
love06-aristotle2.md&6.5&>- It is *not* good to be selfish from a lack of virtue.
love06-aristotle2.md&6.5&>- “In this sense, then, as has been said, a man should be a lover of self; but in the sense in which most men are so, he ought not.”
love06-aristotle2.md&6.5&
love06-aristotle2.md&7.0&# Criticism of Aristotle’s concept of friendship
love06-aristotle2.md&7.0&
love06-aristotle2.md&7.1&## Reason and emotion
love06-aristotle2.md&7.1&
love06-aristotle2.md&7.1&>- How can we criticise Aristotle’s concept of friendship?
love06-aristotle2.md&7.1&>- Aristotle’s love comes at the high price: “Even more than Plato, Aristotle associates love with reason and against emotion.” (Singer, I, 91). 
love06-aristotle2.md&7.1&>- Plato’s lovers are “enthusiasts yearning for the good” (Singer). – “The Aristotelian friends are businessmen who share a partnership in virtue.” (Singer, I, 92). 
love06-aristotle2.md&7.1&
love06-aristotle2.md&7.2&## Eros
love06-aristotle2.md&7.2&
love06-aristotle2.md&7.2&>- Is eros a virtue for Aristotle?
love06-aristotle2.md&7.2&>- No. Aristotle calls it “a sort of excess of feeling.” This does not seem to do it justice.
love06-aristotle2.md&7.2&
love06-aristotle2.md&7.3&## Philia and choice
love06-aristotle2.md&7.3&
love06-aristotle2.md&7.3&>- Do we have a choice about whom we love, according to Aristotle?
love06-aristotle2.md&7.3&>- Aristotelian philia is based on choice. Love is bestowed by choice, and can be withheld by choice, because it is a rational act, not a feeling.
love06-aristotle2.md&7.3&>- This neglects the emotional character of love as we experience it.
love06-aristotle2.md&7.3&>- For example, an Aristotelian could justify an arranged marriage with a suitably virtuous partner one has never met.
love06-aristotle2.md&7.3&
love06-aristotle2.md&7.4&## Philia and persons
love06-aristotle2.md&7.4&
love06-aristotle2.md&7.4&>- Do Aristotelian lovers really relate to the person they love?
love06-aristotle2.md&7.4&>- Aristotelian lovers don’t love the person itself, but some attributes of their character. In principle, every human with those characteristics would do as well. Love objects become interchangeable.
love06-aristotle2.md&7.4&>- This is a problem of every “appraisal”-type concept of love.
love06-aristotle2.md&7.4&
love06-aristotle2.md&7.5&## Love and justice
love06-aristotle2.md&7.5&
love06-aristotle2.md&7.5&>- Do you see a relationship between love and justice in Aristotle’s idea of love?
love06-aristotle2.md&7.5&>- For Aristotle, love is linked to justice. 
love06-aristotle2.md&7.5&>     - One *deserves* love because of one’s character attributes (Singer I, 95). 
love06-aristotle2.md&7.5&>     - One has a justifiable claim to be loved because of one’s character. 
love06-aristotle2.md&7.5&>     - In a sense, this is crazy: one cannot force love, arguing that one deserves it. One can *honour* deserving people, but not, on command, *love* them.
love06-aristotle2.md&7.5&>- This seems to be the opposite of agape or Christian love of the next man: Aristotle’s love is completely dependent on merit. While agape is, by its nature, unmerited and unearned, bestowed without reason.
love06-aristotle2.md&7.5&
love06-aristotle2.md&7.5&
love06-aristotle2.md&7.5&
love06-aristotle2.md&8.0&# The End. Thank you for your attention!
love06-aristotle2.md&8.0&
love06-aristotle2.md&8.0&<!--
love06-aristotle2.md&8.0&
love06-aristotle2.md&8.0&%% Local Variables: ***
love06-aristotle2.md&8.0&%% mode: markdown ***
love06-aristotle2.md&8.0&%% mode: visual-line ***
love06-aristotle2.md&8.0&%% mode: cua ***
love06-aristotle2.md&8.0&%% mode: flyspell ***
love06-aristotle2.md&8.0&%% End: ***
love06-aristotle2.md&8.0&
love06-aristotle2.md&8.0&-->
love06-aristotle2.md&8.0&
love06-aristotle2.md&8.0&
love06-aristotle2.md&8.0&
love06-aristotle2.md&8.0&
love07-christian.md&0.1&---
love07-christian.md&0.1&title:  "Love and Sexuality: 7. Christian Love: Agape"
love07-christian.md&0.1&author: Andreas Matthias, Lingnan University
love07-christian.md&0.1&date: September 1, 2019
love07-christian.md&0.1&...
love07-christian.md&0.1&
love07-christian.md&1.0&# Christian love
love07-christian.md&1.0&
love07-christian.md&1.1&## Christian love
love07-christian.md&1.1&
love07-christian.md&1.1&Common sources of Christian love concepts:
love07-christian.md&1.1&
love07-christian.md&1.1&>- The Bible, particularly the letters of St Paul.
love07-christian.md&1.1&>- St Augustine
love07-christian.md&1.1&>- The Desert Fathers
love07-christian.md&1.1&>- Thomas Aquinas
love07-christian.md&1.1&>- Anders Nygren
love07-christian.md&1.1&>- C. S. Lewis
love07-christian.md&1.1&
love07-christian.md&1.1&
love07-christian.md&1.2&## Subjects and objects of love
love07-christian.md&1.2&
love07-christian.md&1.2&Christian religion, like any other theory of love, has to provide descriptions and explanations for different kinds of love:
love07-christian.md&1.2&
love07-christian.md&1.2&>- Love of humans towards God
love07-christian.md&1.2&>- Love of God towards humans
love07-christian.md&1.2&>- Love of humans towards the next man 
love07-christian.md&1.2&>- Charitable love
love07-christian.md&1.2&>- Erotic love
love07-christian.md&1.2&
love07-christian.md&1.2&
love07-christian.md&1.3&## St Paul (4 BCE - ~63 CE)
love07-christian.md&1.3&
love07-christian.md&1.3&>- “Love is patient, love is kind. It does not envy, it does not boast, it is not proud. It is not rude, it is not self-seeking, it is not easily angered, it keeps no record of wrongs. Love does not delight in evil but rejoices with the truth. It always protects, always trusts, always hopes, and always perseveres. ... And now these three remain: faith, hope and love. But the greatest of these is love.” (1 Corinthians 13)
love07-christian.md&1.3&
love07-christian.md&1.3&
love07-christian.md&1.4&## Flesh and spirit (1)
love07-christian.md&1.4&
love07-christian.md&1.4&St Paul, Galatians 5:
love07-christian.md&1.4&
love07-christian.md&1.4&>- Here we already find the opposition between “flesh” and “spirit”:
love07-christian.md&1.4&>- “13 You, my brothers and sisters, were called to be free. But do not use your freedom to indulge the flesh; rather, serve one another humbly in love [agape]. 14 For the entire law is fulfilled in keeping this one command: “Love your neighbor as yourself.” ... 16 So I say, walk by the Spirit, and you will not gratify the desires of the flesh. 17 For the flesh desires what is contrary to the Spirit, and the Spirit what is contrary to the flesh. They are in conflict with each other, so that you are not to do whatever you want.
love07-christian.md&1.4&
love07-christian.md&1.5&## Flesh and spirit (2)
love07-christian.md&1.5&
love07-christian.md&1.5&St Paul, Galatians 5:
love07-christian.md&1.5&
love07-christian.md&1.5&>- 19 The acts of the flesh are obvious: sexual immorality, impurity and debauchery; 20 idolatry and witchcraft; hatred, discord, jealousy, fits of rage, selfish ambition, dissensions, factions 21 and envy; drunkenness, orgies, and the like. I warn you, as I did before, that those who live like this will not inherit the kingdom of God.
love07-christian.md&1.5&>- Clearly, St Paul has some idea like Plato’s in mind, where Eros should, ideally, be removed from the bodily desires and directed towards some eternal version of the good.
love07-christian.md&1.5&
love07-christian.md&1.5&
love07-christian.md&1.6&## Is Christian love exclusive? (1)
love07-christian.md&1.6&
love07-christian.md&1.6&>- The New Testament is not always clear: are Christians asked to love *everyone* or only other Christians?
love07-christian.md&1.6&>- John 13:34: “A new command I give you: Love one another. As I have loved you, so you must love *one another*. 35 By this everyone will know that you are my disciples, if you love one another.” (emph. added)
love07-christian.md&1.6&>- Galatians 3:28: “There is neither Jew nor Gentile, neither slave nor free, nor is there male and female, for you are all one in Christ Jesus.”
love07-christian.md&1.6&>- Colossians 3:11: “Here there is no Gentile or Jew, circumcised or uncircumcised, barbarian, Scythian, slave or free, but Christ is all, and is in all.”
love07-christian.md&1.6&
love07-christian.md&1.7&## Is Christian love exclusive? (2)
love07-christian.md&1.7&
love07-christian.md&1.7&>- Matthew 22:35-40:
love07-christian.md&1.7&>     - 36 “Teacher, which is the greatest commandment in the Law?”
love07-christian.md&1.7&>     - 37 Jesus replied: “‘Love the Lord your God with all your heart and with all your soul and with all your mind.’[c] 38 This is the first and greatest commandment. 39 And the second is like it: ‘Love your neighbor as yourself.’ 40 All the Law and the Prophets hang on these two commandments.”
love07-christian.md&1.7&>- Here is a conflict: what if the love for God contradicts our love to our neighbour? ^[Liu Qingping (2007): “On a Paradox of Christian Love”. Journal of Religious Ethics.]
love07-christian.md&1.7&
love07-christian.md&1.7&
love07-christian.md&1.8&## St Augustine (Augustine of Hippo, 354-430 AD)
love07-christian.md&1.8&
love07-christian.md&1.8&>- Difference between love and lust.
love07-christian.md&1.8&>- Remember, Aristotle distinguishes love from pleasure and love from goodness. Similarly, Augustine sees that a love relationship has both elements that are pleasurable and elements that are (morally) good.
love07-christian.md&1.8&>- Central to Augustine’s understanding of rightly ordered sexuality is his belief that *the pleasure of the act should not be separated from its good (procreation).* (Meilaender, G. (2001). Sweet necessities: food, sex, and saint Augustine. Journal of Religious Ethics, 29(1), 3-18.)
love07-christian.md&1.8&
love07-christian.md&1.8&
love07-christian.md&1.9&## St Augustine (2)
love07-christian.md&1.9&
love07-christian.md&1.9&>- Augustine distinguishes between the *good* of an activity (its appropriate end or purpose) and the *pleasure* the activity gives. 
love07-christian.md&1.9&>- “There is, in Augustine’s view, nothing wrong with taking pleasure in any permitted activity. What is wrong is trying to separate the pleasure from the good—trying to get the pleasure when one has no interest in or need of the good to which the activity tends or ought to tend.” (Meilaender)
love07-christian.md&1.9&>- (This is also at the basis of one of the Christian/Catholic arguments against contraception).
love07-christian.md&1.9&
love07-christian.md&1.9&
love07-christian.md&1.10&## St Augustine (3)
love07-christian.md&1.10&
love07-christian.md&1.10&>- Metaphor with food: 
love07-christian.md&1.10&>     - The good of food: nourishment.
love07-christian.md&1.10&>     - The good of sex: children.
love07-christian.md&1.10&>     - Both give pleasure, but: “As we should not grasp for the pleasure of eating apart from the good purpose to which it is divinely ordered, so, also, we should not seek the pleasure of the sexual act apart from the good to which it is divinely ordered. ... Its purpose is offspring.” (Meilaender)
love07-christian.md&1.10&
love07-christian.md&1.11&## St Augustine (4)
love07-christian.md&1.11&
love07-christian.md&1.11&>- “[A] man had to love his spouse, but preferably not her body, and both parties were permitted an ordinate sexual pleasure in conjugal relations as long as they were not motivated by desire. This is like saying it is acceptable to enjoy eating, but not to feel hungry.” (Power 1996, 229)
love07-christian.md&1.11&>- What do you think of that? Is this a good comparison?
love07-christian.md&1.11&>- Hunger is a signal of a life-threatening condition: starvation. Abstinence from sexual activities is not life-threatening.
love07-christian.md&1.11&>- Hunger directs us towards “the good” (eating as nourishment). Sexual appetite, in contrast, is directed towards the pleasure of sex. We don’t normally feel a sexual urge specifically directed towards making children.
love07-christian.md&1.11&>- Perhaps “appetite” or “lust for fancy food” should be substituted for “hunger” above: “It is acceptable to enjoy eating, but not to feel lust for fancy food or delicacies.” This would describe Augustine’s position better.
love07-christian.md&1.11&
love07-christian.md&1.11&
love07-christian.md&1.12&## Desert fathers (approx. 3rd-5th century AD)
love07-christian.md&1.12&
love07-christian.md&1.12&>- Anthony the Great (~AD 270) heard a sermon that said that one could achieve perfection by giving away all of one’s possessions to the poor and following Christ.
love07-christian.md&1.12&>- Anthony interpreted that to mean leaving society. He moved to the desert to be alone.
love07-christian.md&1.12&>- Over time, the movement grew. Life in the desert was intentionally hard, renouncing all usual comforts and pleasures of the senses.
love07-christian.md&1.12&>- The monks living in the desert were rumoured to be wise and people came to listen to them and get advice. When Anthony died, the dwellings in the desert were like a small city.
love07-christian.md&1.12&
love07-christian.md&1.12&
love07-christian.md&1.13&## Desert fathers (approx. 3rd-5th century AD)
love07-christian.md&1.13&
love07-christian.md&1.13&>- The teachings of the desert fathers often emphasise the need to act in a selfless way, ignoring one’s own wishes, and living a life in total service to others.
love07-christian.md&1.13&>- Multiple collections of texts describe the sayings of the desert fathers.
love07-christian.md&1.13&>- An easy to read, small collection is: Thomas Merton (1960). *The Wisdom of the Desert.*
love07-christian.md&1.13&
love07-christian.md&1.14&## Stories of the desert fathers (1)
love07-christian.md&1.14&
love07-christian.md&1.14&>- There was once a hermit who was attacked by robbers. His cries alerted the other hermits, and together they all managed to capture the robbers. The robbers were put in jail, but the hermits were ashamed and sad, because, on their account, the robbers had been put to jail. They had acted selfishly, and not with sufficient love for the robbers. So in the night they went into the city, broke into the jail, and freed the robbers.
love07-christian.md&1.14&
love07-christian.md&1.15&## Stories of the desert fathers (2)
love07-christian.md&1.15&
love07-christian.md&1.15&>- Abbot Anastasius, another hermit, had a very expensive book, a Bible, his only possession. One day, a visitor stole his book, but Anastasius did not pursue him, because he didn’t want to make the other man lie about the book. A few days later, a used books seller from the city came to Anastasius and said: a man wanted to sell me this book, but because it looks quite expensive, I wanted to hear your opinion. Is this really a valuable book? Anastasius said yes, and told the book seller the real value of the book, without mentioning that it was his own. When the thief heard that, he took the book back to Anastasius and begged him to take back the book. But Anastasius didn’t want the book, and he gave it to the thief as a present. The thief was so impressed by the whole episode that he became Anastasius’ student and lived with him in the desert for the rest of his life.
love07-christian.md&1.15&>- In these stories, you can see an extreme and uncompromising version of Christian love for one’s neighbour. 
love07-christian.md&1.15&
love07-christian.md&1.15&
love07-christian.md&1.16&## Thomas Aquinas (1225-1274) (1): Amor and caritas
love07-christian.md&1.16&
love07-christian.md&1.16&>- The greatest Christian philosopher of all time.
love07-christian.md&1.16&>- Influenced by Aristotle, he tried to synthesise and combine Platonic, Aristotelian and Christian ideas.
love07-christian.md&1.16&>- Aquinas uses different words for love.^[Much of the following discussion is from: Stump, E. (2006). Love, by All Accounts. Proceedings and Addresses of the American Philosophical Association, Vol. 80, No. 2 (Nov., 2006), pp. 25-43.]
love07-christian.md&1.16&>- “Amor” is the most general sense, which is included in the other types.
love07-christian.md&1.16&>- “Caritas” is love in the real and most perfect (Christian) sense.
love07-christian.md&1.16&
love07-christian.md&1.16&
love07-christian.md&1.17&## Thomas Aquinas (2): God
love07-christian.md&1.17&
love07-christian.md&1.17&>- According to Aquinas, the ultimate proper object of love is God.
love07-christian.md&1.17&>- But since every human being is made in the image of God, the divine goodness is also reflected in every human person. Consequently, the proper object of love also includes human beings.
love07-christian.md&1.17&>- (You can see the Platonic aspect of this: human beings have part in the perfection of the Form/Idea of beauty. Their physical beauty is an imperfect “image” of the eternal Form.)
love07-christian.md&1.17&>- Thus, love is primarily the love of persons.
love07-christian.md&1.17&>- The most general kind of love for persons is friendship. (You see Aristotle here!)
love07-christian.md&1.17&
love07-christian.md&1.18&## Thomas Aquinas (3): Desires in love
love07-christian.md&1.18&
love07-christian.md&1.18&>- Love consists of two desires:
love07-christian.md&1.18&>     - the desire for the good of the beloved; and
love07-christian.md&1.18&>     - the desire for union with the beloved.
love07-christian.md&1.18&>- Again, see how this synthesises Aristotle (first desire) and Plato (second desire).
love07-christian.md&1.18&>- Differently from Plato, Aquinas did not think that the desire ends when we have achieved what we desire. You can be in the presence or possession of the desired thing and still have a desire towards it. (Plato would put this differently: how?)
love07-christian.md&1.18&>     - Plato: the desire that we have for X after possessing X is the desire for the continued possession of X in eternity.
love07-christian.md&1.18&
love07-christian.md&1.19&## Thomas Aquinas (4): Goodness
love07-christian.md&1.19&
love07-christian.md&1.19&>- What is “goodness”?
love07-christian.md&1.19&>- Not only moral goodness, but also beauty, elegance, efficiency and other positive traits. 
love07-christian.md&1.19&>- Goodness, for Aquinas, is objective and measurable.
love07-christian.md&1.19&>- “So to desire the good of the beloved is to desire for the beloved those things which in fact contribute to the beloved's flourishing.” (Stump, op.cit.)
love07-christian.md&1.19&>- What counts is the actual flourishing of the beloved, not only the intentions of the lover. So one has to do for one’s friends what is actually good for *them,* not what one would like oneself (which may be something different).
love07-christian.md&1.19&>- “On this understanding, it is possible for a person Jerome to think that he loves another person Paula when he actually does not, in virtue of the fact that Jerome has an intrinsic desire for what is harmful for Paula. On Aquinas's views, therefore, a person can be mistaken in his beliefs about whom he loves.” (Stump)
love07-christian.md&1.19&
love07-christian.md&1.19&
love07-christian.md&1.20&## Thomas Aquinas (5): Union and offices
love07-christian.md&1.20&
love07-christian.md&1.20&>- Second, the desire for union is only a “loving” desire if the union is in the interests of the flourishing of the beloved.
love07-christian.md&1.20&>- Stump: “If a mother who wants her son with her tries to prevent him from ever leaving home, when leaving home is necessary for his flourishing, then she is not in fact desiring the good for her son. For that reason, her desire for union with him is also not a desire of love.”
love07-christian.md&1.20&
love07-christian.md&1.20&
love07-christian.md&1.21&## Thomas Aquinas (6): Union and offices
love07-christian.md&1.21&
love07-christian.md&1.21&>- What form that “union” takes will be different depending on whom we want to unite with. 
love07-christian.md&1.21&>- “The kind of union ... appropriate for the people who are [one’s] spouse, parent, child, colleague, and priest will be different, depending on the relationship in question.” (Stump)
love07-christian.md&1.21&>- Sometimes people might have multiple relationships to each other: a teacher might, for example, be teaching their own child. Different types of love will be appropriate at the different phases of this relationship.
love07-christian.md&1.21&>- The “office” of teacher (or parent, or husband) will require particular responses in terms of the kind of love shown and experienced.
love07-christian.md&1.21&
love07-christian.md&1.22&## Thomas Aquinas (7): Appraisal and office
love07-christian.md&1.22&
love07-christian.md&1.22&>- Some theories have problems explaining particular kinds of love. For example, an “appraisal” theory will not be able to explain parental love well. Can you see why?
love07-christian.md&1.22&>     - Because there is little to appraise in small babies. They are a lot of trouble and don’t have many features of excellence. 
love07-christian.md&1.22&>     - If one’s child is less good or beautiful than another’s child, the parent won’t love the other more. The “appraisal” approach cannot explain the constancy of parental love.
love07-christian.md&1.22&>- How would Aquinas solve this problem?
love07-christian.md&1.22&>- For Aquinas, “the love between a parent and her child derives from the office which connects them, and the office is a function of the relational characteristics of the lover and the beloved. It is not a response to the intrinsic chacteracteristics of the beloved. Consequently, the parent's love does not co-vary with the intrinsic characteristics of her child either.” (Stump)
love07-christian.md&1.22&
love07-christian.md&1.23&## Thomas Aquinas (8): Appraisal and office
love07-christian.md&1.23&
love07-christian.md&1.23&>- What would Aquinas say about meeting someone who has better characteristics than the beloved?
love07-christian.md&1.23&>- “Furthermore, on Aquinas's account, love will not be readily transferable to any other person just because the other person's valuable intrinsic characteristics are the same as those of the beloved. The mere possession of the beloved's valuable characteristics on the part of some person other than the beloved is not enough for establishing that other person in an *office of love with the lover*” (emphasis added). (Stump)
love07-christian.md&1.23&>- On the other hand, Aquinas does allow us to be responsive to valuable properties in the beloved.
love07-christian.md&1.23&
love07-christian.md&1.24&## Thomas Aquinas (9): Love from lack and love as friendship
love07-christian.md&1.24&
love07-christian.md&1.24&Thomas Aquinas also distinguishes between:
love07-christian.md&1.24&
love07-christian.md&1.24&>- “Amor concupiscentiae” the love that seeks from the beloved that which is lacking in oneself and wants that thing for oneself. We can again see the Platonic element in that (Aristophanes in the Symposion); and
love07-christian.md&1.24&>- “Amor benevolentiae” (the benevolent love) or amor amicitiae (the friendship love) which is the love for another person because of who that person is rather than what the lover is able to give to another person.
love07-christian.md&1.24&>- As a summary, we can see that Aquinas has a very complex and differentiated account of love that seems to be more Platonic and Aristotelian than actually Christian in the modern sense of “agape” love.
love07-christian.md&1.24&
love07-christian.md&1.24&
love07-christian.md&1.25&## Anders Nygren (1890-1978): Agape and Eros (1930/1936)
love07-christian.md&1.25&
love07-christian.md&1.25&>- Anders Nygren was a Swedish theologian and bishop. In his book “Agape and Eros” (1930/1936) he emphasised the famous distinction between (bad, self-interested, conditional) Eros and (Christian, selfless, unconditional) Agape.
love07-christian.md&1.25&>- Because Eros is selfish, it is not a proper kind of love at all.
love07-christian.md&1.25&
love07-christian.md&1.26&## Anders Nygren (1890-1978): Agape and Eros (1930/1936)
love07-christian.md&1.26&
love07-christian.md&1.26&>- Agape, for Nygren, is modelled on the self-sacrifice of Jesus on the cross. ^[Pope, S: Love in Contemporary Christian Ethics, Journal of religious ethics. , 1995, Vol.23(1), p.165-197.]
love07-christian.md&1.26&>- It is radically self-sacrificial, spontaneous, uncalculating, bestowing rather than appraising, and unmotivated by considerations of reciprocity (Pope, 167).
love07-christian.md&1.26&>- Agape “abandons all thought of the worthiness of the object.” (Nygren)
love07-christian.md&1.26&>- This contradicts Aquinas, who saw Agape as much more of a mutually beneficial union than a radical self-sacrifice. Therefore, Catholic thinkers tended to ignore Nygren, while Protestants accepted it more.
love07-christian.md&1.26&>- Since then, Nygren’s distinction has often been criticised.
love07-christian.md&1.26&
love07-christian.md&1.26&
love07-christian.md&1.27&## Criticism of Nygren
love07-christian.md&1.27&
love07-christian.md&1.27&>- First, it is possible that the change in the New Testament which speaks of “agape” instead of “eros” was a purely linguistic development of the Greek language.
love07-christian.md&1.27&>- The spoken language at the time the New Testament was written, was “koine,” the “common” Greek, as opposed to the classic Greek of Plato.
love07-christian.md&1.27&>- “The history of the classical word eros, so frequently found in the Socratic dialogues, is that in the connotation of “loving” it went more and more out of use from the fourth century B.C. onward, while agape was more and more used instead of it. ... As early as the 2nd cent. B.C. agape was the common word for ‘loving’, both in the spoken and in the written language of everybody.”^[C. J. de Vogel (1981). Greek Cosmic Love and the Christian Love of God. Vigiliae Christianae 35, 57-81]
love07-christian.md&1.27&>- Accordingly, it might just be an accident of language that the Bible speaks of Agape rather than Eros, and not a very deep difference in meaning.
love07-christian.md&1.27&
love07-christian.md&1.27&
love07-christian.md&1.28&## Outka’s concept of Agape (Pope 1995) (1)
love07-christian.md&1.28&
love07-christian.md&1.28&>- In 1972, Gene Outka published “Agape. An Ethical Analysis,” which is discussed in Pope (1995).
love07-christian.md&1.28&>- Agape acknowledges the human dignity of every person, independently of merit, attractiveness, or ability to reciprocate. Every human being possesses irreducible worth and cannot only be used as a “means” (Kant!) to one’s own interests.
love07-christian.md&1.28&>- Agape respects the worth of all human beings equally. Worth does not increase or diminish with social role or relationship to the agent (“office” in Aquinas’ terms). Exclusiveness, partiality and discrimination of any form are not allowed.
love07-christian.md&1.28&
love07-christian.md&1.29&## Outka’s concept of Agape (Pope 1995) (2)
love07-christian.md&1.29&
love07-christian.md&1.29&>- Agape admits reasonable self-regard. Agape does not exclude or overcome self-love (as Nygren said); but it also does not allow self-love to become stronger than neighbour-love. 
love07-christian.md&1.29&>- Agape commands genuine and responsible service to one’s neighbour, but it does not require indiscriminate service to the neighbour.
love07-christian.md&1.29&>- We can call this concept “Agape as equal regard” (Pope, 1995, 168)
love07-christian.md&1.29&
love07-christian.md&1.30&## Criticising “Agape as equal regard”
love07-christian.md&1.30&
love07-christian.md&1.30&>- Do you see how this concept of Agape could be criticised?
love07-christian.md&1.30&>- Pope (1995):
love07-christian.md&1.30&>     - The concept is excessively individualistic. It seems to talk about persons that are detached from their community, families and traditions.
love07-christian.md&1.30&>     - “Equal regard” does not sufficiently respect our feelings towards particular people.
love07-christian.md&1.30&>     - It does not sufficiently account for family, friendship, marriage and other institutions that create “special relationships” with special rights and obligations towards others.
love07-christian.md&1.30&
love07-christian.md&1.30&
love07-christian.md&1.31&## C. S. Lewis: The Four Loves (1)
love07-christian.md&1.31&
love07-christian.md&1.31&>- C.S. Lewis (1960): The Four Loves. Lewis (1898-1963) was a professor of English literature, close friend of Tolkien, and author of the Chronicles of Narnia and other books, among them Christian theory.
love07-christian.md&1.31&>- Similar to Nygren, he distinguishes:
love07-christian.md&1.31&>- Storge, an “empathy bond.” This means liking people through familiarity, like in family relationships. It is natural and does not require “worthiness” in the object of love. This is, for Lewis, the basis for most of human happiness.
love07-christian.md&1.31&>- Philia. This is the love of friends for each other. It is based on shared values, interests or activities. It is freely chosen and highly selective. In modern times, he thought, genuine friendship was not appreciated enough.
love07-christian.md&1.31&
love07-christian.md&1.31&
love07-christian.md&1.32&## C. S. Lewis: The Four Loves (2)
love07-christian.md&1.32&
love07-christian.md&1.32&>- Eros (romantic love). This is the romantic sense of “being in love” rather then the raw sexual desire. Eros is a strong motivational force for people, and it can lead to good or bad results.
love07-christian.md&1.32&>- Agape or Charity: unconditional love, based on God’s love. Selfless and the greatest of the four loves. The other kinds of love can turn bad or be used for bad ends, while only agape is always good.
love07-christian.md&1.32&
love07-christian.md&1.32&
love07-christian.md&2.0&# Peter Black: The Broken Wings of Eros
love07-christian.md&2.0&
love07-christian.md&2.1&## Five questions about Eros in Christianity
love07-christian.md&2.1&
love07-christian.md&2.1&>- What is it? 
love07-christian.md&2.1&>- Why has it been so difficult to incorporate it into Christian sexual ethics?
love07-christian.md&2.1&>- What is the importance of doing so?
love07-christian.md&2.1&>- Why has it been forgotten or denied?
love07-christian.md&2.1&>- Who is leading the rediscovery?
love07-christian.md&2.1&
love07-christian.md&2.2&## History of the tension between Agape and Eros (1)
love07-christian.md&2.2&
love07-christian.md&2.2&>- In Plato, Eros is the desire to unite with the loved.
love07-christian.md&2.2&>- But ultimately, “Lovers, with their passions, desires, and impulses, in search of the other half and longing for wholeness, are only true lovers and under the power of Eros, according to Plato, when what they seek is good.” (Black, 109)
love07-christian.md&2.2&>- Origen of Alexandria (184–253 AD) thought that the Bible “only substitutes agape for eros to prevent the weak and uninformed from thinking about carnal desire and passion.” (Black 110). Otherwise, he sees God’s Agape as a Platonic “Eros” relationship.
love07-christian.md&2.2&
love07-christian.md&2.2&
love07-christian.md&2.3&## History of the tension between Agape and Eros (2)
love07-christian.md&2.3&
love07-christian.md&2.3&>- In modern times, Nygren (as we saw above) saw Eros as opposed to (Christian) Agape.
love07-christian.md&2.3&>- Karl Barth (theologian, 1886-1968) “portrayed eros as a ravenous desire, a ... strengthening of natural self-assertion, to be contrasted with Christian love.” (Black 110)
love07-christian.md&2.3&>- Since Platonic eros can be seen as being “acquisitive, egocentric, and devaluing of persons” (Black 110), this also seems to suggest that it is not a proper basis for Christian love.
love07-christian.md&2.3&>- For C.S. Lewis (see above), Eros works more like in Diotima’s Ladder: it begins with the appreciation of another’s body, but develops towards a love of the whole person. But Eros is not constant and reliable. It comes and goes and there is a danger that, as a culture, we might idolise Eros itself too much (Black, 113).
love07-christian.md&2.3&
love07-christian.md&2.4&## Edward Vacek’s “three loves”
love07-christian.md&2.4&
love07-christian.md&2.4&>- Agape;
love07-christian.md&2.4&>- Agape for self;
love07-christian.md&2.4&>- Eros.
love07-christian.md&2.4&>- All three are vital for Christian life (Black, 116).
love07-christian.md&2.4&>- Eros is just another (indirect) form of self-love:
love07-christian.md&2.4&>     - “In the first [agape], the immediate object of love is our own self; in the second [eros] the immediate object is something other than ourselves, which we love as a way of loving ourselves. With the first, we love ourselves for our own sake: agapic self-love. With the second, we love another for our own sake: eros.” (Vacek, cited in Black, 116)
love07-christian.md&2.4&
love07-christian.md&2.5&## Self-love
love07-christian.md&2.5&
love07-christian.md&2.5&>- What is the importance of self-love?
love07-christian.md&2.5&>- Self-love is important because:
love07-christian.md&2.5&>     - It overcomes unhealthy self-loathing;
love07-christian.md&2.5&>     - It can give a positive direction to our lives;
love07-christian.md&2.5&>     - It gives us a positive self-identity;
love07-christian.md&2.5&>     - It leads us to preserve our lives and to actualise ourselves.
love07-christian.md&2.5&>- In the end, even our love for God is derived from our own needs: “If God did not fulfill our need then there would be no reason to love Him.” (Black, 116)
love07-christian.md&2.5&>- Eros, therefore, is real love for others, although it is compatible with (and based on) self-love: “It does not seem unreasonable to suggest that we love others partly for what they can do for us”
love07-christian.md&2.5&
love07-christian.md&2.6&## Eros
love07-christian.md&2.6&
love07-christian.md&2.6&>- Black distinguishes three forms of Eros:
love07-christian.md&2.6&>     - “There is the sensual *epithymic* form [Greek: epithymia = desire], where the loved one gives us sexual sense pleasure.”
love07-christian.md&2.6&>     - “In the *psychological* form eros seeks the other because they make us comfortable and cheerful.” 
love07-christian.md&2.6&>     - “With *spiritual* form of eros, we hope to share in the sublimity of the beautiful object and its religious form.
love07-christian.md&2.6&>     - “[E]ros wants God to be perfect because otherwise our quest for the perfectly fulfilling good would seem thwarted.” (Black, 117)
love07-christian.md&2.6&>- See how the latter one makes a bridge between Plato’s forms and God.
love07-christian.md&2.6&
love07-christian.md&2.7&## Eros has been suppressed in the Christian tradition
love07-christian.md&2.7&
love07-christian.md&2.7&>- Black makes the point that the erotic element has been suppressed in the Christian tradition, although it can often be found in art (p.119)
love07-christian.md&2.7&>- Two approaches to spirituality (p.121):
love07-christian.md&2.7&>     - One approach “places the stress on detachment, denial of desire, and the power of the intellect.”
love07-christian.md&2.7&>     - The other approach “emphasizes attachment, the release of desire, feeling, and the body.”
love07-christian.md&2.7&>- Joan Timmerman: “Those who deny their bodies and their feelings, thinking that the real self is the mental subject, are never wholly available. Some part, the vital, spontaneous part, is always under constraint. Touch is always feared.” (p.121)
love07-christian.md&2.7&
love07-christian.md&2.8&## Conclusion
love07-christian.md&2.8&
love07-christian.md&2.8&>- What is the conclusion of Black’s paper?
love07-christian.md&2.8&>- The tension between Christian Eros and Agape does not need to be as strong as seems to be in Nygren.
love07-christian.md&2.8&>- We can understand both Eros and Agape as results of proper (moral) self-love.
love07-christian.md&2.8&>- Self-love (and, therefore, “selfish” Eros) don’t need to be morally bad, but can be the basis for self-acceptance and positive Christian love.
love07-christian.md&2.8&>- Both art and modern feminist authors show how Eros can return to its proper place in Christian theology.
love07-christian.md&2.8&>- “The renewed flight of eros in certain quarters of ethics invites us to reconsider issues concerning power and knowledge, self-love and self-giving, the sexual and the sacred and the many guises of desire and pleasure. The experience of and reflection on eros connects humans to the mysterious, the perplexing, the vulnerable and attractive dimensions of the other.” (p.126)
love07-christian.md&2.8&
love07-christian.md&3.0&# Richardson on Christian Agape
love07-christian.md&3.0&
love07-christian.md&3.1&## C. Richardson: “Love, Greek and Christian” (1943)
love07-christian.md&3.1&
love07-christian.md&3.1&>- After discussing Plato and Aristotle, Richardson turns to Christian love (p.178)
love07-christian.md&3.1&>- “Thus Christian agape is something which is both realized and yet not fully achieved in mortal existence. It is realized in so far as men pass from self-centeredness to a truer selfhood (a state of growth which the Christian calls “sanctification”); but it is unrealized in the sense that they never fully overcome the barriers which separate them from their fellows.” (p.178)
love07-christian.md&3.1&>- Two senses of Agape: (1) Love towards God; and (2) Love towards one’s fellows (p.179). We cannot love both in the same way.
love07-christian.md&3.1&>- Agape towards others is (as in Aquinas and Aristotle) “eu-poiein,” “to do good” to others.
love07-christian.md&3.1&>- Agape towards God means serving God.
love07-christian.md&3.1&
love07-christian.md&3.2&## The historic evil of self-love
love07-christian.md&3.2&
love07-christian.md&3.2&>- “Aquinas can say: ‘The good look upon their rational nature ... as being the chief thing in them. On the other hand, the wicked reckon their sensitive and corporeal nature ... to hold the first place.” (p.181)
love07-christian.md&3.2&>- This led to the “repressive nature of medieval asceticism.” (We saw some of that in the discussion of the Desert Fathers, above).
love07-christian.md&3.2&>- Martin Luther (1483-1546) was opposed to the idea that humans can try to “earn” God’s love by having particular properties.
love07-christian.md&3.2&>- He saw God’s love as entirely “bestowal” love.
love07-christian.md&3.2&>- “God's love,” he wrote, “does not *find,* but *creates* its lovable object.” (p.182)
love07-christian.md&3.2&
love07-christian.md&3.3&## Meanings of Eros
love07-christian.md&3.3&
love07-christian.md&3.3&>- Sexual passion.
love07-christian.md&3.3&>- Passion for creativity (p.183).
love07-christian.md&3.3&>- The cosmic principle in mysticism. That is the meaning that it has when we contemplate the Platonic Form of ideal beauty. “Eros descends from above ... and turns all things toward the divine beauty” (Proclus Lycaeus, 412-485 AD).
love07-christian.md&3.3&>- Conclusion: Eros is a creative force: “To me eros is concerned with the experience of being enraptured in the moment of creativity.” (p.184)
love07-christian.md&3.3&>- It is fulfilled in the harmony of self-affirmation and self-abandon.
love07-christian.md&3.3&>- Christianity has, so far, failed to unite Eros and Agape. But these two must be balanced.
love07-christian.md&3.3&
love07-christian.md&3.3&
love07-christian.md&3.3&
love07-christian.md&3.3&
love07-christian.md&3.3&
love07-christian.md&3.3&
love07-christian.md&3.3&
love07-christian.md&3.3&
love07-christian.md&3.3&
love07-christian.md&3.3&
love07-christian.md&3.3&
love08-middleages-mystical.md&0.1&---
love08-middleages-mystical.md&0.1&title:  "Love and Sexuality: 8. Middle Ages (1): God’s love and the mystical experience"
love08-middleages-mystical.md&0.1&author: Andreas Matthias, Lingnan University
love08-middleages-mystical.md&0.1&date: September 1, 2019
love08-middleages-mystical.md&0.1&...
love08-middleages-mystical.md&0.1&
love08-middleages-mystical.md&1.0&# Background
love08-middleages-mystical.md&1.0&
love08-middleages-mystical.md&1.1&## Sources
love08-middleages-mystical.md&1.1&
love08-middleages-mystical.md&1.1&Much of this session is based on: Ursula King, “Christian Mystics.” Routledge 2001, 2004.
love08-middleages-mystical.md&1.1&
love08-middleages-mystical.md&1.1&Also on Irving Singer, “The Nature of Love,” vol.1: Plato to Luther.
love08-middleages-mystical.md&1.1&
love08-middleages-mystical.md&1.2&## Where we are
love08-middleages-mystical.md&1.2&
love08-middleages-mystical.md&1.2&>- Let us summarise what we know about the tradition of the concepts of love so far.
love08-middleages-mystical.md&1.2&>- Love in Plato: Eros, union with the eternal Form of the absolute good. Hierarchical, not symmetrical.
love08-middleages-mystical.md&1.2&>- Love for Aristotle: Philia, wanting to benefit like-minded friends for their own sake. Symmetrical, mutual.
love08-middleages-mystical.md&1.2&>- Christian love: A synthesis of:
love08-middleages-mystical.md&1.2&>     - Platonic ideas (the absolute good as God);
love08-middleages-mystical.md&1.2&>     - Aristotelian philia (to do good to others); and
love08-middleages-mystical.md&1.2&>     - Agape (the unconditional love of God that is the basis of our unconditional love towards others).
love08-middleages-mystical.md&1.2&>- Today we will see how the eros towards God turns, in mysticism, into a personal (and sometimes even sexual) kind of love.
love08-middleages-mystical.md&1.2&>- In the next session we will see how this forms the basis of medieval courtly and later “romantic” love, of which the essential elements survive until today.
love08-middleages-mystical.md&1.2&
love08-middleages-mystical.md&1.3&## Mysticism (1)
love08-middleages-mystical.md&1.3&
love08-middleages-mystical.md&1.3&>- “Mysticism” comes from the Greek μυω = “to conceal.” 
love08-middleages-mystical.md&1.3&>- “Mystical” referred to secret religious rituals. Such rituals existed in all ancient cultures, they are not specific to Christianity.
love08-middleages-mystical.md&1.3&>- A μύστης or μυστικός (mystes or mystikos), is an initiate, someone who has been introduced to a secret religious ritual.
love08-middleages-mystical.md&1.3&>- In the Middle Ages, Christian mystics were mostly monks who experienced a union with God and wrote about it.
love08-middleages-mystical.md&1.3&
love08-middleages-mystical.md&1.4&## Mysticism (2)
love08-middleages-mystical.md&1.4&
love08-middleages-mystical.md&1.4&>- Often the language to describe this experience is explicitly erotic or sexual, talking of a “merging” of the mystic with God in an act that is reminiscent of a sexual union.
love08-middleages-mystical.md&1.4&>- Since God is not a physical body but a spiritual force, that “merging” can be much more intense and complete than it could be in a physical sexual act with another human being.
love08-middleages-mystical.md&1.4&>- In this sense, it is the ultimate sexual union, the “Platonic Form” of a sexual union.
love08-middleages-mystical.md&1.4&>- Often the mystics were women, who otherwise are underrepresented in medieval philosophy.
love08-middleages-mystical.md&1.4&>     - It seems that the social conditions at that time made it hard for women to get an education, but that women’s monasteries were an exception, and that women could there cultivate their talents more freely, resulting in many famous women mystics that were famous and respected even in their own time.
love08-middleages-mystical.md&1.4&
love08-middleages-mystical.md&1.4&
love08-middleages-mystical.md&1.5&## The Bible: Song of Songs (1)
love08-middleages-mystical.md&1.5&
love08-middleages-mystical.md&1.5&>- The Bible contains, among other passages that are relevant to mysticism, the “Song of Songs.”
love08-middleages-mystical.md&1.5&
love08-middleages-mystical.md&1.5&. . . 
love08-middleages-mystical.md&1.5&
love08-middleages-mystical.md&1.5&> She: “Let him kiss me with the kisses of his mouth —  
love08-middleages-mystical.md&1.5&  for your love is more delightful than wine.  
love08-middleages-mystical.md&1.5&  Pleasing is the fragrance of your perfumes;  
love08-middleages-mystical.md&1.5&  your name is like perfume poured out.  
love08-middleages-mystical.md&1.5&  No wonder the young women love you!  
love08-middleages-mystical.md&1.5&  Take me away with you—let us hurry!  
love08-middleages-mystical.md&1.5&  Let the king bring me into his chambers.”
love08-middleages-mystical.md&1.5&
love08-middleages-mystical.md&1.5&. . . 
love08-middleages-mystical.md&1.5&
love08-middleages-mystical.md&1.5&>- Interestingly, it’s a “she” here, like in the mystical tradition of the Middle Ages, where many famous mystics were nuns.
love08-middleages-mystical.md&1.5&>- The “she” is probably meant to be the soul of man that enters into “marriage” with God’s spirit (and since God is traditionally perceived as male, the soul had to be seen as female).
love08-middleages-mystical.md&1.5&
love08-middleages-mystical.md&1.6&## The Bible: Song of Songs (2)
love08-middleages-mystical.md&1.6&
love08-middleages-mystical.md&1.6&> She: Like an apple tree among the trees of the forest  
love08-middleages-mystical.md&1.6&  is my beloved among the young men.  
love08-middleages-mystical.md&1.6&  I delight to sit in his shade,  
love08-middleages-mystical.md&1.6&  and his fruit is sweet to my taste.  
love08-middleages-mystical.md&1.6&  Let him lead me to the banquet hall,  
love08-middleages-mystical.md&1.6&  and let his banner over me be love.  
love08-middleages-mystical.md&1.6&  Strengthen me with raisins,  
love08-middleages-mystical.md&1.6&  refresh me with apples,  
love08-middleages-mystical.md&1.6&  for I am faint with love.  
love08-middleages-mystical.md&1.6&  His left arm is under my head,  
love08-middleages-mystical.md&1.6&  and his right arm embraces me.
love08-middleages-mystical.md&1.6&  
love08-middleages-mystical.md&1.6&. . .
love08-middleages-mystical.md&1.6&
love08-middleages-mystical.md&1.6&>- It’s hard to see this still as a metaphor for the soul’s journey to God. 
love08-middleages-mystical.md&1.6&>- The Song of Songs mixes the spiritual journey with the language of bodily love and desire. We will see this later also in the descriptions of the mystics.
love08-middleages-mystical.md&1.6&
love08-middleages-mystical.md&1.7&## The Bible: Song of Songs (3)
love08-middleages-mystical.md&1.7&
love08-middleages-mystical.md&1.7&> He: How beautiful your sandaled feet,  
love08-middleages-mystical.md&1.7&  O prince’s daughter!  
love08-middleages-mystical.md&1.7&  Your graceful legs are like jewels,  
love08-middleages-mystical.md&1.7&  the work of an artist’s hands.  
love08-middleages-mystical.md&1.7&  Your navel is a rounded goblet  
love08-middleages-mystical.md&1.7&  that never lacks blended wine.  
love08-middleages-mystical.md&1.7&  Your waist is a mound of wheat  
love08-middleages-mystical.md&1.7&  encircled by lilies.
love08-middleages-mystical.md&1.7&
love08-middleages-mystical.md&1.7&
love08-middleages-mystical.md&1.7&> Your neck is like an ivory tower  
love08-middleages-mystical.md&1.7&  Your eyes are the pools of Heshbon  
love08-middleages-mystical.md&1.7&  ...  
love08-middleages-mystical.md&1.7&  Your nose is like the tower of Lebanon  
love08-middleages-mystical.md&1.7&  looking toward Damascus.
love08-middleages-mystical.md&1.7&
love08-middleages-mystical.md&1.7&. . . 
love08-middleages-mystical.md&1.7&
love08-middleages-mystical.md&1.7&>- (That last one is a bit puzzling.)
love08-middleages-mystical.md&1.7&
love08-middleages-mystical.md&1.8&## The Last Supper
love08-middleages-mystical.md&1.8&
love08-middleages-mystical.md&1.8&>- At the Last Supper, Jesus explicitly sees the sharing of the bread as a kind of bodily union:
love08-middleages-mystical.md&1.8&
love08-middleages-mystical.md&1.8&. . . 
love08-middleages-mystical.md&1.8&
love08-middleages-mystical.md&1.8&> “I am that living bread which has come down from heaven: if anyone eats this bread he shall live for ever. Moreover, the bread which I will give is my own flesh; I give it for the life of the world.... Whoever eats my flesh and drinks my blood dwells continually in me and I dwell in him.” (King, Christian Mystics, p.6)
love08-middleages-mystical.md&1.8&
love08-middleages-mystical.md&1.8&
love08-middleages-mystical.md&1.9&## Asceticism
love08-middleages-mystical.md&1.9&
love08-middleages-mystical.md&1.9&>- We already saw how the Desert Fathers attempted to approach God by removing themselves from the temptations of the flesh through fasting, living in the desert, and enduring hardships.
love08-middleages-mystical.md&1.9&>- Later, out of this community of men came the first monks that organised themselves into monasteries (under Pachomius, 292–348 AD).
love08-middleages-mystical.md&1.9&>- “It was the aim of ascetic and monastic life to achieve the conquest of self through renunciation so that, once purified from all obstacles, the soul might live the perfect life face to face with God, in direct communion and union with him. In the silence of the desert, in the solitude of the cell, free from all worldly entanglements, the mystic could ascend to the contemplation and knowledge of God, and loving union with him.” (King, p.8)
love08-middleages-mystical.md&1.9&
love08-middleages-mystical.md&1.9&
love08-middleages-mystical.md&1.10&## The “ladder of perfection” (King, p.9)
love08-middleages-mystical.md&1.10&
love08-middleages-mystical.md&1.10&>- Three stages through which the mystic had to pass to achieve union with God:
love08-middleages-mystical.md&1.10&>- It ... begins with the lowest stage ..., the way of purification, understood as detachment, renunciation and asceticism, to move away from the world of the senses and ego to the higher, eternally abiding reality of God.
love08-middleages-mystical.md&1.10&>- [This] leads to the second stage, which is the illuminative life. At this stage the mystic draws nearer to divine unity, reaching the heights of loving contemplation. 
love08-middleages-mystical.md&1.10&>- Fully illumined, he or she realizes the ultimate mystery of all that exists and dwells with joy in a state of sublime ignorance, likened to utter darkness, to an abyss of nothingness.
love08-middleages-mystical.md&1.10&>- This is followed by the highest stage, the unitive life, the ultimate goal of loving union with God, an ecstatic experience of overwhelming joy. 
love08-middleages-mystical.md&1.10&
love08-middleages-mystical.md&2.0&# Medieval mysticism
love08-middleages-mystical.md&2.0&
love08-middleages-mystical.md&2.1&## God as a person
love08-middleages-mystical.md&2.1&
love08-middleages-mystical.md&2.1&>- Singer (p.174) sees St Augustine as the thinker who understood that normal people cannot be emotionally involved with a religion that worships an abstract, transcendent idea (or Platonic Form) of the good.
love08-middleages-mystical.md&2.1&>- Christianity had to offer the faithful a God that was a relatable *person*.
love08-middleages-mystical.md&2.1&>- To this contribute both the idea that we were made “in the image of God” and the life and suffering of Christ as a human being on Earth. God is seen as a “father” rather than an abstract idea.
love08-middleages-mystical.md&2.1&>- Thus, God could be loved even by those who weren’t Platonic philosophers:
love08-middleages-mystical.md&2.1&>- “In asserting the primacy of feeling, mysticism denies that the philosopher (whether Socrates or another) is worthy of being considered the true lover. The love of God does not require superior training [in philosophy]. ... It depends in part on faith, in part on the purification of erotic feeling.” (p.174)
love08-middleages-mystical.md&2.1&
love08-middleages-mystical.md&2.2&## The ladder/mountain metaphor (1)
love08-middleages-mystical.md&2.2&
love08-middleages-mystical.md&2.2&>- We saw that both Plato and Aristotle had their ladder/ascent metaphors:
love08-middleages-mystical.md&2.2&>     - Plato talks of Diotima’s ladder of Eros, from the bodily to the abstract and purely spiritual.
love08-middleages-mystical.md&2.2&>     - Aristotle talks of the ladder towards Eudaimonia, the way one develops from akrates to sophron. This corresponds to more and more refined ways of enjoying the philia of similarly good men.
love08-middleages-mystical.md&2.2&
love08-middleages-mystical.md&2.3&## The ladder/mountain metaphor (2)
love08-middleages-mystical.md&2.3&
love08-middleages-mystical.md&2.3&>- The medieval mystics often also use a ladder metaphor (Singer, p.179):
love08-middleages-mystical.md&2.3&>     - “At first, the mystic tries to overcome his sins through fasting, ... meditation, prayer,” and so on.
love08-middleages-mystical.md&2.3&>     - God then gives the mystic a vision of divinity, a glimpse of God’s world: illumination, enlightenment.
love08-middleages-mystical.md&2.3&>     - God shows himself to the mystic, and the mystic’s soul can enter into a first loving “union” with God.
love08-middleages-mystical.md&2.3&>     - Further exercises and purification remove the last bits of pride and attachment to the self (remember the Desert Fathers!) and makes the human worthy of “marrying” God.
love08-middleages-mystical.md&2.3&>     - The mystic then unites with God to the greatest extent possible for a human being. Human and God become “one spirit.”
love08-middleages-mystical.md&2.3&>     - In the moment of ecstasy (Greek: ek-stasis, to stand outside oneself) the mystic has lost his own self and is pure spirit, pure divine love. This is the top of the ladder in this world.
love08-middleages-mystical.md&2.3&>     - After death, the ladder continues upward into heaven.
love08-middleages-mystical.md&2.3&
love08-middleages-mystical.md&2.4&## Freud on ladders and eros (Singer p. 182)
love08-middleages-mystical.md&2.4&
love08-middleages-mystical.md&2.4&>- Can you see the connection of ladders to erotic imagery?
love08-middleages-mystical.md&2.4&>- In his dream analysis, the psychologist Sigmund Freud (1856-1939) likens ladders to the sexual act:
love08-middleages-mystical.md&2.4&>     - “Steps, ladders or staircases, or ... walking up or down them, are representations of the sexual act. ... We come to the top in a series of rhythmical movements and with increasing breathlessness and then, with a few rapid leaps, we can get to the bottom again. Thus the rhythmical pattern of copulation is reproduced in going upstairs.”
love08-middleages-mystical.md&2.4&
love08-middleages-mystical.md&2.5&## Ladders in love (Richard of St Victor)
love08-middleages-mystical.md&2.5&
love08-middleages-mystical.md&2.5&>- (As cited in Singer, p.184)
love08-middleages-mystical.md&2.5&>- First degree: lovers are stricken by the “bolt out of the blue,” an unexpected meeting that arouses the desires. The lover is “shot through the heart.”
love08-middleages-mystical.md&2.5&>- In the second stage, love overwhelms the senses and the imagination. It binds the lover to the object of his devotion (p.185). The lover constantly dreams of the beloved. In the love of God, the lover sees God in all his beauty.
love08-middleages-mystical.md&2.5&>- In the third stage, “the soul is captivated, all other interests driven out. ... The lover loses all autonomy.” In mystical love, this is the point where the soul passes over into God (ecstasy).
love08-middleages-mystical.md&2.5&>- In the fourth stage, love becomes unbearable and the lover finally rejects the control that the beloved has over him. In the spiritual love, the soul “demeans itself” to serve God in the same way as Christ “demeaned himself” when he became a man. The soul now returns to Earth, happy to serve God, loving all things and all mankind for the sake of God.
love08-middleages-mystical.md&2.5&
love08-middleages-mystical.md&2.5&
love08-middleages-mystical.md&3.0&# The experiences of Christian mystics
love08-middleages-mystical.md&3.0&
love08-middleages-mystical.md&3.1&## St. Bernard of Clairvaux (1090–1153)
love08-middleages-mystical.md&3.1&
love08-middleages-mystical.md&3.1&>- Bernard distinguishes carnal and social love, love of self and love of others, and divides love into four degrees. 
love08-middleages-mystical.md&3.1&>     - First we love ourselves for our own sake; 
love08-middleages-mystical.md&3.1&>     - then we love God, but for our own sake. 
love08-middleages-mystical.md&3.1&>     - Different again is when we love God for his sake and, 
love08-middleages-mystical.md&3.1&>     - the highest degree, our love of ourselves for God’s sake. The divine love is sincere because it does not seek its own advantage.^[King, p.32]
love08-middleages-mystical.md&3.1&
love08-middleages-mystical.md&3.2&## St. Bernard of Clairvaux (2)
love08-middleages-mystical.md&3.2&
love08-middleages-mystical.md&3.2&> “To lose yourself, as if you no longer existed, to cease completely to experience yourself, to reduce yourself to nothing is not a human sentiment but a divine experience...
love08-middleages-mystical.md&3.2&
love08-middleages-mystical.md&3.2&> “It is deifying to go through such an experience. As a drop of water seems to disappear completely in a big quantity of wine, even assuming the wine’s taste and color, just as red, molten iron becomes so much like fire it seems to lose its primary state; just as the air on a sunny day seems transformed into a sunshine instead of being lit up; so it is necessary for the saints that all human feelings melt in a mysterious way and flow into the will of God. Otherwise, how will God be all in all if something human survives in man?” ^[King, p.32]
love08-middleages-mystical.md&3.2&
love08-middleages-mystical.md&3.3&## Hildegard of Bingen (1098–1179)
love08-middleages-mystical.md&3.3&
love08-middleages-mystical.md&3.3&>- Hildegard von Bingen sees God’s presence everywhere in nature, as the force that gives life to all things (itself an erotic motive).
love08-middleages-mystical.md&3.3&>- “For her, wisdom is less about thinking than tasting. In Latin, wisdom (sapientia) and taste (sapere) are words stemming from the same root.” (King, p.39)
love08-middleages-mystical.md&3.3&
love08-middleages-mystical.md&3.3&. . . 
love08-middleages-mystical.md&3.3&
love08-middleages-mystical.md&3.3&> “I, the fiery life of divine essence, am aflame beyond the beauty of the meadows. I gleam in the waters. I burn in the sun, moon, and stars. With every breeze, as with invisible life that contains everything, I awaken everything to life.
love08-middleages-mystical.md&3.3&
love08-middleages-mystical.md&3.3&> “I am the breeze that nurtures all things green. I encourage blossoms to flourish with ripening fruits. I am the rain coming from the dew that causes the grasses to laugh with the joy of life.”
love08-middleages-mystical.md&3.3&
love08-middleages-mystical.md&3.3&
love08-middleages-mystical.md&3.4&## Jan van Ruusbroec (1293–1381)
love08-middleages-mystical.md&3.4&
love08-middleages-mystical.md&3.4&> “If we look deep within ourselves, there we shall feel God’s Spirit driving and urging us on in the impatience of love; and if we look high above ourselves, there we shall feel God’s Spirit drawing us out of ourselves and bringing us to nothing in the essence of God, that is, in the essential love in which we are one with Him, the love which we possess deeper and wider than every other thing.”
love08-middleages-mystical.md&3.4&
love08-middleages-mystical.md&3.4&
love08-middleages-mystical.md&3.5&## St. Catherine of Genoa (1447–1510)
love08-middleages-mystical.md&3.5&
love08-middleages-mystical.md&3.5&> “I am so...submerged in His immense love, that I seem as though immersed in the sea, and nowhere able to touch, see or feel [anything] but water. ... My being is God, not by simple participation but by a true transformation of my being.”
love08-middleages-mystical.md&3.5&
love08-middleages-mystical.md&3.5&
love08-middleages-mystical.md&4.0&# The erotic element in mystical union
love08-middleages-mystical.md&4.0&
love08-middleages-mystical.md&4.0&
love08-middleages-mystical.md&4.1&## St Gertrude (1256–1302)
love08-middleages-mystical.md&4.1&
love08-middleages-mystical.md&4.1&> “My heart craves the kiss of your love, my soul thirsts for the most intimate embrace joining me to you.”
love08-middleages-mystical.md&4.1&
love08-middleages-mystical.md&4.1&
love08-middleages-mystical.md&4.2&## Mechtild von Magdeburg (1241–1299)
love08-middleages-mystical.md&4.2&
love08-middleages-mystical.md&4.2&> “Lord, love me hard, love me long and often. I call you, burning with desire. Your burning love enflames me constantly. I am but a naked soul, and you, inside it, a richly adorned guest.”
love08-middleages-mystical.md&4.2&
love08-middleages-mystical.md&4.3&## Hadewijch of Antwerp (first half of thirteenth century)
love08-middleages-mystical.md&4.3&
love08-middleages-mystical.md&4.3&> “My heart and my arteries, and all my limbs quivered and trembled with desire. I felt myself so violently and dreadfully tested it appeared that if I did not give satisfaction to my lover entirely, to know him, to taste him in every part of his body and if he did not respond to my desire, I would die of rage...”
love08-middleages-mystical.md&4.3&
love08-middleages-mystical.md&4.3&
love08-middleages-mystical.md&4.4&## Hadewijch of Antwerp (2)
love08-middleages-mystical.md&4.4&
love08-middleages-mystical.md&4.4&> “He came, handsome and sweet, ... I approached him submissively ... And he gave himself to me as he usually does, in the form of the sacrament. Then he came to me in person and took me in his arms and locked me in his arms. All my limbs felt this contact with his with equal intensity, following my heart, as I had desired. Thus externally, I was satisfied and quenched. ... Following which, I remained merged with my lover until I had melted entirely within him in such a way as nothing was left of me.”
love08-middleages-mystical.md&4.4&
love08-middleages-mystical.md&4.5&## Bernini “The Ecstasy of Saint Teresa” (1652)
love08-middleages-mystical.md&4.5&
love08-middleages-mystical.md&4.5&![](graphics/bernini3.png)\
love08-middleages-mystical.md&4.5&
love08-middleages-mystical.md&4.5&
love08-middleages-mystical.md&4.6&## St Teresa of Avila (1515–1582)
love08-middleages-mystical.md&4.6&
love08-middleages-mystical.md&4.6&> “I saw in his hand a long spear of gold, and at the iron's point there seemed to be a little fire. He appeared to me to be thrusting it at times into my heart, and to pierce my very entrails; when he drew it out, he seemed to draw them out also, and to leave me all on fire with a great love of God. The pain was so great, that it made me moan; and yet so surpassing was the sweetness of this excessive pain, that I could not wish to be rid of it.”
love08-middleages-mystical.md&4.6&
love08-middleages-mystical.md&4.7&## St Teresa of Avila (1515–1582)
love08-middleages-mystical.md&4.7&
love08-middleages-mystical.md&4.7&> “The soul is satisfied now with nothing less than God. The pain is not bodily, but spiritual; though the body has its share in it. It is a caressing of love so sweet which now takes place between the soul and God, that I pray God of His goodness to make him experience it who may think that I am lying.”
love08-middleages-mystical.md&4.7&
love08-middleages-mystical.md&4.7&
love08-middleages-mystical.md&4.8&## Conclusion
love08-middleages-mystical.md&4.8&
love08-middleages-mystical.md&4.8&>- Christian love, beginning as a version of Platonic Eros, turns into actual human-like love in the mystical experience.
love08-middleages-mystical.md&4.8&>- God stops being only an abstract concept and becomes an *experienced* being, someone who can be touched and with whom one can undergo a kind of physical, even erotic, union.
love08-middleages-mystical.md&4.8&>- In the next session we will see how this move of God down from the level of abstraction and into the realm of physical experience became the basis for courtly love in the Middle Ages.
love08-middleages-mystical.md&4.8&>- From courtly love, in turn, we have inherited many ideas still present in modern, romantic love.
love08-middleages-mystical.md&4.8&
love08-middleages-mystical.md&4.8&
love08-middleages-mystical.md&5.0&# The End. Thank you for your attention!
love08-middleages-mystical.md&5.0&
love08-middleages-mystical.md&5.0&<!--
love08-middleages-mystical.md&5.0&
love08-middleages-mystical.md&5.0&%% Local Variables: ***
love08-middleages-mystical.md&5.0&%% mode: markdown ***
love08-middleages-mystical.md&5.0&%% mode: visual-line ***
love08-middleages-mystical.md&5.0&%% mode: cua ***
love08-middleages-mystical.md&5.0&%% mode: flyspell ***
love08-middleages-mystical.md&5.0&%% End: ***
love08-middleages-mystical.md&5.0&
love08-middleages-mystical.md&5.0&-->
love09-middleages-courtly.md&0.1&---
love09-middleages-courtly.md&0.1&title:  "Love and Sexuality: 9. Middle Ages (2): Courtly and Romantic Love"
love09-middleages-courtly.md&0.1&author: Andreas Matthias, Lingnan University
love09-middleages-courtly.md&0.1&date: September 1, 2019
love09-middleages-courtly.md&0.1&...
love09-middleages-courtly.md&0.1&
love09-middleages-courtly.md&1.0&# Courtly love
love09-middleages-courtly.md&1.0&
love09-middleages-courtly.md&1.1&## What is courtly love?
love09-middleages-courtly.md&1.1&
love09-middleages-courtly.md&1.1&>- A kind of love that first was only a literary device, but soon was accepted in the Middle Ages as the proper way for a knight to be in love with a lady.
love09-middleages-courtly.md&1.1&>- It was exclusively a phenomenon for the upper classes.
love09-middleages-courtly.md&1.1&>- Love songs were sung by travelling singers, called troubadours.
love09-middleages-courtly.md&1.1&>- Courtly love was not generally meant to lead to any kind of (sexual or otherwise) union.
love09-middleages-courtly.md&1.1&>- The lady would generally be married to someone else. Courtly love was a kind of a game, a fictional relationship, a dream.
love09-middleages-courtly.md&1.1&>- As such, it was accepted even by the husband of the lady.
love09-middleages-courtly.md&1.1&>- Marriages were forced for political and economic reasons and had little to do with love.
love09-middleages-courtly.md&1.1&>- Courtly love, in contrast, was free and voluntary.
love09-middleages-courtly.md&1.1&>- It was inspired by works of the Roman poet Ovid, and this is where our word “romantic” and “romance” comes from, meaning: the “Roman” style of love poetry (=Ovid’s style).
love09-middleages-courtly.md&1.1&
love09-middleages-courtly.md&1.1&
love09-middleages-courtly.md&1.2&## What is courtly love?^[https://www.ancient.eu/Courtly_Love]
love09-middleages-courtly.md&1.2&
love09-middleages-courtly.md&1.2&>- Andreas Capellanus (12th cent. CE): De Amore. He sets down some of the rules of the genre:
love09-middleages-courtly.md&1.2&>     - Marriage is no excuse for not loving
love09-middleages-courtly.md&1.2&>     - One who is not jealous, cannot love
love09-middleages-courtly.md&1.2&>     - No one can be bound by a double love
love09-middleages-courtly.md&1.2&>     - Love is always increasing or decreasing
love09-middleages-courtly.md&1.2&
love09-middleages-courtly.md&1.3&## What is courtly love?
love09-middleages-courtly.md&1.3&
love09-middleages-courtly.md&1.3&Central motifs of courtly love poetry:
love09-middleages-courtly.md&1.3&
love09-middleages-courtly.md&1.3&>- A beautiful woman who is inaccessible (either because she is married or imprisoned)
love09-middleages-courtly.md&1.3&>- A noble knight who has sworn to serve her
love09-middleages-courtly.md&1.3&>- A forbidden, passionate love shared by both
love09-middleages-courtly.md&1.3&>- The impossibility or danger of consummating that love
love09-middleages-courtly.md&1.3&
love09-middleages-courtly.md&1.3&
love09-middleages-courtly.md&1.4&## Stages of courtly love^[Barbara Tuchman (1978): “A Distant Mirror”]
love09-middleages-courtly.md&1.4&
love09-middleages-courtly.md&1.4&>- Attraction to the lady, usually via eye contact
love09-middleages-courtly.md&1.4&>- Worship of the lady from afar
love09-middleages-courtly.md&1.4&>- Declaration of passionate devotion
love09-middleages-courtly.md&1.4&>- Virtuous rejection by the lady
love09-middleages-courtly.md&1.4&>- Renewed wooing with oaths of virtue and eternal faithfulness
love09-middleages-courtly.md&1.4&>- Moans of approaching death from unsatisfied desire (and other physical manifestations of lovesickness)
love09-middleages-courtly.md&1.4&>- Heroic deeds which win the lady's heart
love09-middleages-courtly.md&1.4&>- (The last two are often not present:)
love09-middleages-courtly.md&1.4&>     - Consummation of the secret love
love09-middleages-courtly.md&1.4&>     - Endless adventures and deceptions to avoid detection
love09-middleages-courtly.md&1.4&
love09-middleages-courtly.md&1.4&
love09-middleages-courtly.md&2.0&# The first troubadour: William the ninth, Duke of Aquitaine
love09-middleages-courtly.md&2.0&
love09-middleages-courtly.md&2.1&## William’s life (1)
love09-middleages-courtly.md&2.1&
love09-middleages-courtly.md&2.1&>- William’s life is worth retelling, because it’s such a beautiful tale of adventure. One can hardly believe that this is a real life and not a Hollywood production! (From: William Reddy, The Making of Romantic Love)
love09-middleages-courtly.md&2.1&>- William, ninth duke of Aquitaine and seventh count of Poitou (1071-1126, ruled 1086-1126), is the earliest-known troubadour. 
love09-middleages-courtly.md&2.1&>- Troubadours were composers of songs that defined courtly love.
love09-middleages-courtly.md&2.1&>- Many others followed William’s example and copied the style of his poems and compositions, creating the whole genre of courtly love poetry.
love09-middleages-courtly.md&2.1&
love09-middleages-courtly.md&2.2&## William’s life (2)
love09-middleages-courtly.md&2.2&
love09-middleages-courtly.md&2.2&>- His father, William VIII (1024–86, ruled 1058–86), had terminated his first two marriages. You were not supposed to do that in the Middle Ages.
love09-middleages-courtly.md&2.2&>- The local bishop of Paris supported William’s father and did not object to him re-marrying, but the Popes (first Alexander II, then Gregory VII) questioned the third marriage.
love09-middleages-courtly.md&2.2&>- Since the third marriage finally gave William a son (William IX), his father was very eager to have his marriage legalised and his son fully recognised.
love09-middleages-courtly.md&2.2&>- The Pope sent delegates to discuss the situation.
love09-middleages-courtly.md&2.2&
love09-middleages-courtly.md&2.3&## William’s life (3)
love09-middleages-courtly.md&2.3&
love09-middleages-courtly.md&2.3&>- When William saw that they would decide against him, he sent his knights to break up the meeting.
love09-middleages-courtly.md&2.3&>- Threatened by the Pope, William (the father) went to Rome. The Pope did not recognise his marriage, but he accepted his son as legitimate. In return, William promised to found and finance a new monastery in his home town.
love09-middleages-courtly.md&2.3&>- When he came back, he forced three of his vassals (men who had lands but who depended on William) to sign away their lands to the new monastery.
love09-middleages-courtly.md&2.3&
love09-middleages-courtly.md&2.4&## William’s life (4)
love09-middleages-courtly.md&2.4&
love09-middleages-courtly.md&2.4&>- The young William IX soon started to participate in his father’s deals.
love09-middleages-courtly.md&2.4&>- In 1100, having become duke himself, he decided to join the crusade to Jerusalem.
love09-middleages-courtly.md&2.4&>- The crusade was a disaster. The majority of William’s soldiers were killed.
love09-middleages-courtly.md&2.4&>- William fought for a few months in Jerusalem, then returned home.
love09-middleages-courtly.md&2.4&>- His authority being questioned and without military success, he retreated from public life.
love09-middleages-courtly.md&2.4&>- He started drinking and writing love poems and became the first troubadour.
love09-middleages-courtly.md&2.4&
love09-middleages-courtly.md&3.0&# Troubadours and their love poems
love09-middleages-courtly.md&3.0&
love09-middleages-courtly.md&3.1&## Guillaume de Machaut: “Foy porter” (14th cent.)
love09-middleages-courtly.md&3.1&
love09-middleages-courtly.md&3.1&> I want to stay faithful, guard your honor,
love09-middleages-courtly.md&3.1&  Seek peace, obey
love09-middleages-courtly.md&3.1&  Fear, serve and honor you,
love09-middleages-courtly.md&3.1&  Until death,
love09-middleages-courtly.md&3.1&  Peerless Lady.
love09-middleages-courtly.md&3.1&
love09-middleages-courtly.md&3.2&## Guillaume de Machaut: “Foy porter” (14th cent.)
love09-middleages-courtly.md&3.2&
love09-middleages-courtly.md&3.2&> For I love you so much, truly,
love09-middleages-courtly.md&3.2&  that one could sooner dry up
love09-middleages-courtly.md&3.2&  the deep sea
love09-middleages-courtly.md&3.2&  and hold back its waves
love09-middleages-courtly.md&3.2&  than I could constrain myself
love09-middleages-courtly.md&3.2&  from loving you,
love09-middleages-courtly.md&3.2&
love09-middleages-courtly.md&3.2&> without falsehood; for my thoughts
love09-middleages-courtly.md&3.2&  my memories, my pleasures
love09-middleages-courtly.md&3.2&  and my desires are perpetually
love09-middleages-courtly.md&3.2&  of you, whom I cannot leave or even briefly forget.
love09-middleages-courtly.md&3.2&
love09-middleages-courtly.md&3.2&
love09-middleages-courtly.md&3.3&## Guillaume de Machaut: “Foy porter” (14th cent.)
love09-middleages-courtly.md&3.3&
love09-middleages-courtly.md&3.3&> There is no joy or pleasure
love09-middleages-courtly.md&3.3&  or any other good that one could feel
love09-middleages-courtly.md&3.3&  or imagine which does not seem to me worthless
love09-middleages-courtly.md&3.3&  whenever your sweetness wants to sweeten my
love09-middleages-courtly.md&3.3&  bitterness.
love09-middleages-courtly.md&3.3&
love09-middleages-courtly.md&3.4&## Guillaume de Machaut: “Foy porter” (14th cent.)
love09-middleages-courtly.md&3.4&
love09-middleages-courtly.md&3.4&> You are the true sapphire
love09-middleages-courtly.md&3.4&  that can heal and end all my sufferings,
love09-middleages-courtly.md&3.4&  the emerald which brings rejoicing,
love09-middleages-courtly.md&3.4&  the ruby to brighten and comfort the heart.
love09-middleages-courtly.md&3.4&  
love09-middleages-courtly.md&3.4&> Your speech, your looks,
love09-middleages-courtly.md&3.4&  Your bearing, make one flee and hate and detest
love09-middleages-courtly.md&3.4&  all vice and cherish and desire all that is good.
love09-middleages-courtly.md&3.4&
love09-middleages-courtly.md&3.5&## Features of the poem
love09-middleages-courtly.md&3.5&
love09-middleages-courtly.md&3.5&>- “Your looks, ... make one ... desire all that is good.”
love09-middleages-courtly.md&3.5&>     - Plato, but also the Christian ideal of God (“all that is good”) now projected onto the beloved.
love09-middleages-courtly.md&3.5&>     - As opposed to Platonic eros, the other person *is* all that is good, instead of just being a mere “shadow” of some transcendent, ideal good.
love09-middleages-courtly.md&3.5&
love09-middleages-courtly.md&3.5&
love09-middleages-courtly.md&3.6&## Another love song of Guillaume de Machaut (14th cent.) (1)
love09-middleages-courtly.md&3.6&
love09-middleages-courtly.md&3.6&Lady, I am one of those who willingly endures
love09-middleages-courtly.md&3.6&your wishes, so long as I can endure;
love09-middleages-courtly.md&3.6&but I do not think I can endure it for long
love09-middleages-courtly.md&3.6&without dying, since you are so hard on me
love09-middleages-courtly.md&3.6&as if you wanted to drive me away from you,
love09-middleages-courtly.md&3.6&so I should never again see the great and true beauty
love09-middleages-courtly.md&3.6&of your gentle body, which has such worth
love09-middleages-courtly.md&3.6&that you are of all good women the best.
love09-middleages-courtly.md&3.6&
love09-middleages-courtly.md&3.7&## Another love song of Guillaume de Machaut (14th cent.) (2)
love09-middleages-courtly.md&3.7&
love09-middleages-courtly.md&3.7&Alas! thus I imagine my death.
love09-middleages-courtly.md&3.7&But the pain I shall have to bear
love09-middleages-courtly.md&3.7&would be sweet, if I could only hope,
love09-middleages-courtly.md&3.7&that before my death, you let me see you again.
love09-middleages-courtly.md&3.7&Lady, if ever my heart undertakes anything
love09-middleages-courtly.md&3.7&which may honor or profit my heart,
love09-middleages-courtly.md&3.7&it will come from you, however far you may be,
love09-middleages-courtly.md&3.7&for never without you, whom I love very loyally,
love09-middleages-courtly.md&3.7&nor without Love, could I undertake it or know it.
love09-middleages-courtly.md&3.7&
love09-middleages-courtly.md&4.0&# Stories of romantic love
love09-middleages-courtly.md&4.0&
love09-middleages-courtly.md&4.1&## Ancient, pre-courtly: Ovid, The Metamorphoses, Book X
love09-middleages-courtly.md&4.1&
love09-middleages-courtly.md&4.1&>- Publius Ovidius Naso (43 BC – 17/18 AD).
love09-middleages-courtly.md&4.1&>- One of the greatest poets of Roman literature.
love09-middleages-courtly.md&4.1&>- The *Metamorphoses* is a very long poem, first published around 8 AD.
love09-middleages-courtly.md&4.1&>- Although it is a work of the ancient world, it was very influential in the Middle Ages. It inspired many writers, including the troubadours and later also William Shakespeare.
love09-middleages-courtly.md&4.1&>- Part of it is the ancient Greek story of Orpheus and Eurydice.
love09-middleages-courtly.md&4.1&
love09-middleages-courtly.md&4.2&## Orpheus and Eurydice
love09-middleages-courtly.md&4.2&
love09-middleages-courtly.md&4.2&> While the newly wedded bride, Eurydice, was walking through the grass, ... she was killed, by a bite on her ankle, from a snake, sheltering there. When the poet Orpheus had mourned for her, greatly, in the upper world, he dared to go down to [the Underworld] to see if he might not move the dead [to return her].
love09-middleages-courtly.md&4.2&
love09-middleages-courtly.md&4.3&## Orpheus and Eurydice
love09-middleages-courtly.md&4.3&
love09-middleages-courtly.md&4.3&> He came to ... the lord of the shadows, he who rules the joyless kingdom. Then striking the lyre-strings to accompany his words, he sang: ‘O gods of this world, placed below the earth, to which all, who are created mortal, descend; ... My wife is the cause of my journey. ... A [snake] ... robbed her of her best years. I longed to be able to accept it, and I do not say I have not tried: Love won.’
love09-middleages-courtly.md&4.3&
love09-middleages-courtly.md&4.4&## Orpheus and Eurydice
love09-middleages-courtly.md&4.4&
love09-middleages-courtly.md&4.4&> ‘Eurydice, too, will be yours [the gods’ of the underworld] to command, when she has lived out her fair span of years, to maturity. I ask this benefit as a gift; but, if the fates refuse my wife this kindness, I am determined not to return: you can delight in both our deaths.’
love09-middleages-courtly.md&4.4&
love09-middleages-courtly.md&4.5&## Orpheus and Eurydice
love09-middleages-courtly.md&4.5&
love09-middleages-courtly.md&4.5&> The bloodless spirits wept as he spoke, accompanying his words with the music. ... The king of the deep and his royal bride could not bear to refuse his prayer, and called for Eurydice.
love09-middleages-courtly.md&4.5&
love09-middleages-courtly.md&4.6&## Orpheus and Eurydice
love09-middleages-courtly.md&4.6&
love09-middleages-courtly.md&4.6&> She was among the recent ghosts, and walked haltingly from her wound. Orpheus received her, and, at the same time, accepted this condition, that he must not turn his eyes behind him, until he emerged [to the upper world], or the gift would be null and void.
love09-middleages-courtly.md&4.6&
love09-middleages-courtly.md&4.7&## Orpheus and Eurydice
love09-middleages-courtly.md&4.7&
love09-middleages-courtly.md&4.7&> They took the upward path, through the still silence, steep and dark, shadowy with dense fog, drawing near to the threshold of the upper world. Afraid she was no longer there, and eager to see her, the lover turned his eyes. In an instant she dropped back, and he, unhappy man, stretching out his arms to hold her and be held, clutched at nothing but the receding air. 
love09-middleages-courtly.md&4.7&
love09-middleages-courtly.md&4.8&## Orpheus and Eurydice
love09-middleages-courtly.md&4.8&
love09-middleages-courtly.md&4.8&> Dying a second time, now, there was no complaint to her husband (what, then, could she complain of, except that she had been loved?). She spoke a last ‘farewell’ that, now, scarcely reached his ears, and turned again towards that same place [the underworld].
love09-middleages-courtly.md&4.8&
love09-middleages-courtly.md&4.9&## Orpheus and Eurydice
love09-middleages-courtly.md&4.9&
love09-middleages-courtly.md&4.9&>- What does this tale say? In what sense does it illustrate romantic love?
love09-middleages-courtly.md&4.9&>     - Love (between man and woman) is stronger than death (originally, since Orpheus gets his wife back).
love09-middleages-courtly.md&4.9&>     - Love (between man and woman) is stronger than caution or rationality (since Orpheus cannot help but look at Eurydice!), thus losing her again.
love09-middleages-courtly.md&4.9&>- Compare this tale to Platonic eros and Aristotelian philia. How would Plato and Aristotle judge this story and Orpheus’ behaviour?
love09-middleages-courtly.md&4.9&>     - This would never have been a tale that Aristotle or Plato would have appreciated!
love09-middleages-courtly.md&4.9&>     - Wisdom?
love09-middleages-courtly.md&4.9&>     - Eudaimonia?
love09-middleages-courtly.md&4.9&>     - Phronesis?
love09-middleages-courtly.md&4.9&>     - Emotional detachment?
love09-middleages-courtly.md&4.9&
love09-middleages-courtly.md&5.0&#  The typical representative: Tristan and Isolde
love09-middleages-courtly.md&5.0&
love09-middleages-courtly.md&5.1&## Tristan and Isolde (Iseult)
love09-middleages-courtly.md&5.1&
love09-middleages-courtly.md&5.1&>- Gottfried of Strassburg’s (died approx. 1210) version was the most popular and well-known in the Middle Ages.
love09-middleages-courtly.md&5.1&>- It was made popular in the 12th century and had a great influence on how romantic love was (and still is) perceived.
love09-middleages-courtly.md&5.1&>- We may have forgotten the story, but we still embrace its vision of love.
love09-middleages-courtly.md&5.1&>- Story (as told by Joseph Bedier, transl. by Belloc, 1945), quoted from Wagoner: “The Meanings of Love.”
love09-middleages-courtly.md&5.1&
love09-middleages-courtly.md&5.2&## Tristan and Iseult: The plot (1)
love09-middleages-courtly.md&5.2&
love09-middleages-courtly.md&5.2&>- After defeating another knight, Tristan goes to Ireland to bring back the fair Iseult for his uncle King Mark to marry. 
love09-middleages-courtly.md&5.2&>- Along the way, they ingest a love potion (believing it is just wine), which causes the pair to fall madly in love.
love09-middleages-courtly.md&5.2&>- In the courtly version, the potion's effects last for a lifetime; in the common versions, the potion's effects wane after three years.
love09-middleages-courtly.md&5.2&>- In some versions, they ingest the potion accidentally; in others, the potion's maker instructs Iseult to share it with Mark, but Iseult deliberately gives it to Tristan instead. 
love09-middleages-courtly.md&5.2&
love09-middleages-courtly.md&5.3&## Tristan and Iseult: The plot (2)
love09-middleages-courtly.md&5.3&
love09-middleages-courtly.md&5.3&>- Although Iseult marries Mark, she and Tristan are forced by the potion to seek one another as lovers. 
love09-middleages-courtly.md&5.3&>- While the typical noble Arthurian character would be shamed from such an act, the love potion that controls them frees Tristan and Iseult from responsibility. 
love09-middleages-courtly.md&5.3&>- The king's advisors repeatedly try to have the pair tried for adultery, but again and again the couple use trickery to preserve their façade of innocence. They even replace Isolde by another women in her wedding night with the king.
love09-middleages-courtly.md&5.3&
love09-middleages-courtly.md&5.4&## Tristan and Iseult: The plot (3)
love09-middleages-courtly.md&5.4&
love09-middleages-courtly.md&5.4&>- At some point, King Mark finds them sleeping in the forest side by side, but *with a sword lying between them.*^[We will see later why this is important.] He concludes that they stayed honourably separated, and lets them return to his court.
love09-middleages-courtly.md&5.4&>- Various endings. In some versions the love potion wears off. In others, it persists. Some end tragically, as their love is discovered by the king. In others, Tristan meets another woman later.
love09-middleages-courtly.md&5.4&
love09-middleages-courtly.md&5.5&## Tristan and Iseult: The plot (4)
love09-middleages-courtly.md&5.5&
love09-middleages-courtly.md&5.5&>- In some versions, the story of Tristan and Iseult also ends in death. 
love09-middleages-courtly.md&5.5&>- Long separated from Iseult, Tristan dies from wounds suffered in battle before Iseult can reach him, despite her belief that they are fated to die together. 
love09-middleages-courtly.md&5.5&>- She then dies of grief beside his body. 
love09-middleages-courtly.md&5.5&>- King Mark has them buried in separate tombs, but a green and leafy wild rose springs from Tristan's tomb and falls to root at Iseult's tomb. It is only in death that the lovers are finally united. 
love09-middleages-courtly.md&5.5&
love09-middleages-courtly.md&5.5&
love09-middleages-courtly.md&6.0&# A real-life story: Abelard and Heloise
love09-middleages-courtly.md&6.0&
love09-middleages-courtly.md&6.1&## Abelard and Heloise (1)
love09-middleages-courtly.md&6.1&
love09-middleages-courtly.md&6.1&>- Another medieval love story that seemed to thrive on obstacles was that of Abelard and Heloise.
love09-middleages-courtly.md&6.1&>- Heloise (pronounced: E-louise)
love09-middleages-courtly.md&6.1&>- Story told in Wagoner (op cit), p.55.
love09-middleages-courtly.md&6.1&>- In contrast to the mythical romance of Tristan and Iseult, however, this affair actually happened and the locus was the church, not courtly society. 
love09-middleages-courtly.md&6.1&>- Abelard was a brilliant and attractive young philosopher in Paris. He quickly achieved fame and popularity as a teacher and skilled debater. 
love09-middleages-courtly.md&6.1&>- He was retained by [a high-ranking church official] to tutor his young niece, Heloise, who was also very bright and literate.
love09-middleages-courtly.md&6.1&
love09-middleages-courtly.md&6.2&## Abelard and Heloise (2)
love09-middleages-courtly.md&6.2&
love09-middleages-courtly.md&6.2&>- The two young people fell in love and their affair became an open scandal. 
love09-middleages-courtly.md&6.2&>- The poet-professor's love songs to Heloise were sung all over [Paris]. 
love09-middleages-courtly.md&6.2&>- “Heloise was Isolde, and would in a moment have done what Isolde did.” (Henry Adams)
love09-middleages-courtly.md&6.2&>- Everyone knew about it -- except, of course, Heloise's uncle.
love09-middleages-courtly.md&6.2&>- When Heloise became pregnant, Abelard offered to marry her ... but Heloise refused to have him give up his independence. 
love09-middleages-courtly.md&6.2&>- At length, she yielded, however , and they were married.
love09-middleages-courtly.md&6.2&>- So as not to compromise Abelard's career in the church, they struggled unsuccessfully to keep the marriage a secret. 
love09-middleages-courtly.md&6.2&
love09-middleages-courtly.md&6.2&
love09-middleages-courtly.md&6.3&## Abelard and Heloise (3)
love09-middleages-courtly.md&6.3&
love09-middleages-courtly.md&6.3&>- Finally, at Abelard's request, Heloise “took the veil” and became a nun, and the child was taken by Abelard's sister. 
love09-middleages-courtly.md&6.3&>- Thinking that Abelard had found a way to [get rid of] the marriage, Heloise's uncle was furious at Abelard's treachery and hired thugs to assault and to castrate him.
love09-middleages-courtly.md&6.3&>- Following this catastrophe, Abelard went into monastic seclusion and later established a convent where Heloise became the highly respected abbess. 
love09-middleages-courtly.md&6.3&
love09-middleages-courtly.md&6.4&## Abelard and Heloise (4)
love09-middleages-courtly.md&6.4&
love09-middleages-courtly.md&6.4&>- The two remained mostly out of touch for ten years. 
love09-middleages-courtly.md&6.4&>- Communication was restored when Heloise happened upon a letter written by Abelard to a friend in which he recited the history of his calamities. When she then wrote to him she revealed, astonishingly, that her love for Abelard had not diminished at all.
love09-middleages-courtly.md&6.4&
love09-middleages-courtly.md&6.4&
love09-middleages-courtly.md&6.5&## Abelard and Heloise (5)
love09-middleages-courtly.md&6.5&
love09-middleages-courtly.md&6.5&>- All their ... long separation and monastic vows had served only to heighten the intensity of her feeling for him -- and this was true despite the fact that she knew he was incapable of sexual response. 
love09-middleages-courtly.md&6.5&>- Six hundred years later, the French Empress Josephine Bonaparte ( 1763-1814) had their remains moved to a common tomb. 
love09-middleages-courtly.md&6.5&>- Their tomb has become a tourist attraction for lovers from all over the world.
love09-middleages-courtly.md&6.5&
love09-middleages-courtly.md&6.6&## Letters - Heloise to Abelard
love09-middleages-courtly.md&6.6&
love09-middleages-courtly.md&6.6&> “To her lord, or rather, father; to her husband, or rather, brother; from his servant, or rather daughter; his wife, or rather sister: To Abelard from Heloise.
love09-middleages-courtly.md&6.6&> “You know, beloved, as the whole world knows, how much I have lost in you ... You are the sole cause of my sorrow, and you alone can grant me the grace of consolation. You alone have the power to make me sad, to bring me happiness or comfort.
love09-middleages-courtly.md&6.6&> *“I have feared to offend you rather than God, and tried to please you more than him”*
love09-middleages-courtly.md&6.6&
love09-middleages-courtly.md&6.7&## Letters - Heloise to Abelard
love09-middleages-courtly.md&6.7&
love09-middleages-courtly.md&6.7&> “Solitude is insupportable to the uneasy mind; its troubles increase in the midst of silence, and retirement heightens them. Since I have been shut up in these walls I have done nothing but weep our misfortunes. This cloister has resounded with my cries, and, like a wretch condemned to eternal slavery, I have worn out my days with grief. 
love09-middleages-courtly.md&6.7&> “Instead of fulfilling God's merciful design towards me I have offended against Him; I have looked upon this sacred refuge as a frightful prison, and have borne with unwillingness the yoke of the Lord. Instead of purifying myself with a life of penitence I have confirmed my condemnation.”
love09-middleages-courtly.md&6.7&
love09-middleages-courtly.md&6.8&## Letters - Heloise to Abelard
love09-middleages-courtly.md&6.8&
love09-middleages-courtly.md&6.8&> “Among those who are wedded to God I am wedded to a man; among the heroic supporters of the Cross I am the slave of a human desire; at the head of a religious community I am devoted to Abelard alone. What a monster am I!”
love09-middleages-courtly.md&6.8&
love09-middleages-courtly.md&6.9&## Letters - Heloise to Abelard
love09-middleages-courtly.md&6.9&
love09-middleages-courtly.md&6.9&> “Enlighten me, O Lord, for I know not if my despair or Thy grace draws these words from me! I am, I confess, a sinner, but one who, far from weeping for her sins, weeps only for her lover; far from abhorring her crimes, longs only to add to them; and who, with a weakness unbecoming my state, please myself continually with the remembrance of past delights when it is impossible to renew them.”
love09-middleages-courtly.md&6.9&
love09-middleages-courtly.md&6.10&## Letters - Heloise to Abelard
love09-middleages-courtly.md&6.10&
love09-middleages-courtly.md&6.10&> “... When you please, anything seems lovely to me, and nothing is ugly when you are by. I am only weak when I am alone and unsupported by you, and therefore it depends on you alone to make me such as you desire.”
love09-middleages-courtly.md&6.10&
love09-middleages-courtly.md&6.11&## Letters - Heloise to Abelard
love09-middleages-courtly.md&6.11&
love09-middleages-courtly.md&6.11&> “God knows I never sought anything in you except yourself; I wanted simply you, nothing of yours. I looked for no marriage bond, no marriage portion. … The name of wife may seem more sacred or more binding, but sweeter for me will always be the word mistress, or, if you will permit me, that of concubine or whore.”
love09-middleages-courtly.md&6.11&
love09-middleages-courtly.md&6.12&## Letters - Heloise to Abelard
love09-middleages-courtly.md&6.12&
love09-middleages-courtly.md&6.12&> “When prayer should be purest, the obscene imagining of these pleasures so completely overwhelms my poor soul that I yield to their shameful delectation rather than to prayer. I who should tremble at what I have done, sigh over what I have lost.”
love09-middleages-courtly.md&6.12&
love09-middleages-courtly.md&6.13&## Abelard and Heloise
love09-middleages-courtly.md&6.13&
love09-middleages-courtly.md&6.13&>- If you look at the love story and the words of Abelard and Heloise, what kind of love comes to mind? Can you try to place it, by finding its Platonic, Aristotelian, Christian, and courtly components?
love09-middleages-courtly.md&6.13&>     - Platonic: Idealization of the other, to the extent that rightfully belongs only to absolute beauty or to God. Eros includes the love of the body, which is everywhere present in their letters.
love09-middleages-courtly.md&6.13&>     - Aristotelian: Companionship, common striving towards eudaimonia, even after their separation (through their letters)
love09-middleages-courtly.md&6.13&>     - Christian: Replacement of God by a human (Abelard for Heloise).
love09-middleages-courtly.md&6.13&
love09-middleages-courtly.md&6.14&## Abelard and Heloise
love09-middleages-courtly.md&6.14&
love09-middleages-courtly.md&6.14&>- Courtly elements:
love09-middleages-courtly.md&6.14&>     - (Hetero-)sexual love is more than bodily love. 
love09-middleages-courtly.md&6.14&>     - It is in itself noble, and not something to be ashamed of.
love09-middleages-courtly.md&6.14&>     - Only their status as monk and nun make things difficult. But in the eyes of the world, they don’t feel shame. Heloise is always pointing out how she still loves Abelard and how she refuses to see this as morally bad.
love09-middleages-courtly.md&6.14&>     - Love can be expressed in letters. It is about courtesy and courtship, but, although it is sexually motivated, it does not need any direct contact between the lovers. A life-long, passionate relationship can be had through letter-writing only!
love09-middleages-courtly.md&6.14&>     - Their love establishes a one-ness between them (that is mostly secret) and that unites the two of them against the world (that is the cause of their suffering) -  This is a common romantic love motive.
love09-middleages-courtly.md&6.14&
love09-middleages-courtly.md&7.0&# Characteristics of courtly love
love09-middleages-courtly.md&7.0&
love09-middleages-courtly.md&7.1&## Characteristics of courtly love (Wagoner)
love09-middleages-courtly.md&7.1&
love09-middleages-courtly.md&7.1&>- Courtly love is love between nobles.
love09-middleages-courtly.md&7.1&>- One does not only idolize the lady because she’s a woman, but also because she’s (usually) a high noblewoman.
love09-middleages-courtly.md&7.1&>- Peasants did not participate in this kind of love, and didn’t share its ideals.
love09-middleages-courtly.md&7.1&>- As a form of love, it compensated for the too practical aspects of marriage in the middle ages. 
love09-middleages-courtly.md&7.1&>     - Marriage between nobles was *only* a means of securing land and allies. 
love09-middleages-courtly.md&7.1&>     - There was no provision for love between husband and wife. 
love09-middleages-courtly.md&7.1&>     - So courtly love stepped in to address the emotional needs of the people in this situation.
love09-middleages-courtly.md&7.1&
love09-middleages-courtly.md&7.2&## Characteristics of courtly love (Wagoner)
love09-middleages-courtly.md&7.2&
love09-middleages-courtly.md&7.2&>- In contrast to marriage, courtly love was free. 
love09-middleages-courtly.md&7.2&>- Wagoner (53): 
love09-middleages-courtly.md&7.2&>     - “If at length she responded with a smile or with some more generous gesture, then [the lover] would know that he had won her heart because it was done voluntarily; it was outside the duties and constraints of marriage.”
love09-middleages-courtly.md&7.2&>     - “The very thing that certifies that it is in fact genuine love is that it is absolutely free.” (We have the same idea today!)
love09-middleages-courtly.md&7.2&
love09-middleages-courtly.md&7.3&## Love and church
love09-middleages-courtly.md&7.3&
love09-middleages-courtly.md&7.3&>- The church contributed (unwillingly) to the idea of courtly love: Wagoner (53):
love09-middleages-courtly.md&7.3&>     - First, against the inclination of the landowning barons to arrange marriages as a means of estate management, the church insisted that marriage had to be voluntary.
love09-middleages-courtly.md&7.3&>     - This had the ironic effect of making courtly love affairs appear to be more “moral” (because they were indeed voluntary) than most of the contracted marriages of the time. 
love09-middleages-courtly.md&7.3&>     - Moreover, the church taught that “God is love,” and while the priests and bishops may have meant something quite different by this, it had the effect of giving divine sanction to powerful feelings of desire. How could God not be on the side of lovers? 
love09-middleages-courtly.md&7.3&
love09-middleages-courtly.md&7.4&## Impossibility
love09-middleages-courtly.md&7.4&
love09-middleages-courtly.md&7.4&>- One of the main points is that romantic love has to be impossible love.
love09-middleages-courtly.md&7.4&>- The feeling between the lovers is so strong that it ignores all obstacles and the “impracticality, irrationality, and immorality of their relationship.” (Wagoner)
love09-middleages-courtly.md&7.4&
love09-middleages-courtly.md&7.5&## Otherworldly innocence (Wagoner)
love09-middleages-courtly.md&7.5&
love09-middleages-courtly.md&7.5&>- The difficulties and the impossibility of courtly love affairs were taken to suggest that somehow God must be on the side of the lovers.
love09-middleages-courtly.md&7.5&>- Iseult's handmaiden says to her after the lovers have just narrowly escaped being caught: “God has worked a miracle for you, Iseult, for he is compassionate and will not hurt the innocent in heart.”
love09-middleages-courtly.md&7.5&>- “Even though the lovers betray the most sacred vows of loyalty - Tristan to his lord and Iseult to her husband - the innocent totality of their love lifts them above ordinary moral constraints.” (Wagoner)
love09-middleages-courtly.md&7.5&>- Iseult: “For men see this and that outward thing, but God alone the heart, and in the heart alone is crime and the sole judge is God.”
love09-middleages-courtly.md&7.5&
love09-middleages-courtly.md&7.6&## Otherworldly innocence
love09-middleages-courtly.md&7.6&
love09-middleages-courtly.md&7.6&>- Neither Tristan nor Iseult will concede the slightest bit of guilt when they are called upon to repent: “I will not say one word of penance for my love.” (Iseult)
love09-middleages-courtly.md&7.6&>- The same is true of Heloise: “If I am hurting you exceedingly, I am, as you know, exceedingly innocent.” (And other quotes we saw above)
love09-middleages-courtly.md&7.6&>- Heloise displaces God and replaces him with Abelard. (See quotes above)
love09-middleages-courtly.md&7.6&
love09-middleages-courtly.md&7.7&## Transcendent value of the beloved
love09-middleages-courtly.md&7.7&
love09-middleages-courtly.md&7.7&>- We see that, in a way, the value that Plato bestowed on the transcendent form of the Good; and that Christians bestow on the transcendent God; in courtly love becomes value bestowed on the transcendent (because inaccessible) lady.
love09-middleages-courtly.md&7.7&>- Wagoner: “The ascription of transcendent value to one's beloved ... is the defining relation” of courtly love.
love09-middleages-courtly.md&7.7&
love09-middleages-courtly.md&7.8&## Obstacles
love09-middleages-courtly.md&7.8&
love09-middleages-courtly.md&7.8&>- Every obstacle, instead of diminishing the love, increases the lovers’ devotion.
love09-middleages-courtly.md&7.8&>- Even achieving the union with the beloved becomes secondary. The suffering is what counts.
love09-middleages-courtly.md&7.8&>- The yearning and passion is how the lover knows that this is “true love.” We will see the same idea in the discussion of modern romantic love (later in this course).
love09-middleages-courtly.md&7.8&
love09-middleages-courtly.md&7.9&## The absence of fulfilment
love09-middleages-courtly.md&7.9&
love09-middleages-courtly.md&7.9&>- Wagoner: “Like erotic love, romantic love thrives on what it does not have.”
love09-middleages-courtly.md&7.9&>- “As with erotic love, success could be the worst thing that could happen.”
love09-middleages-courtly.md&7.9&>- Romantic stories (for example, in movies) *end* with marriage! Marriage is the *end* of romantic love, because it cannot exist afterwards in its form as unfulfilled passion and suffering.
love09-middleages-courtly.md&7.9&>- See also what Heloise says about marriage above: she prefers to be Abelard’s whore than his wife! Why?
love09-middleages-courtly.md&7.9&
love09-middleages-courtly.md&7.10&## The role of reason and passion
love09-middleages-courtly.md&7.10&
love09-middleages-courtly.md&7.10&>- In ancient philosophy, loving is a rational activity that has the purpose of achieving some good (the vision of the eternal form of Good or a good life with phronesis and eudaimonia).
love09-middleages-courtly.md&7.10&>- In this way, ancient love is self-interested, rational behaviour.
love09-middleages-courtly.md&7.10&>- Under the influence of Christianity and its vision of *agape,* courtly love becomes selfless and not calculating.
love09-middleages-courtly.md&7.10&>- The lovers are not out to get any benefits from their love. They just want to experience its passion.
love09-middleages-courtly.md&7.10&>- The primacy of passion over reason is a Christian attitude. Religious faith itself is an act of passion that defies reason.
love09-middleages-courtly.md&7.10&
love09-middleages-courtly.md&7.11&## Self-destruction
love09-middleages-courtly.md&7.11&
love09-middleages-courtly.md&7.11&>- The lovers’ passion is so powerful that self-annihilation becomes desirable (as the only way of finally being united in death). The same motive that we saw in the Christian mystics (but there it was directed towards God).
love09-middleages-courtly.md&7.11&>- “Ah, what do you ask? That I come? No. Be still, death waits for us ... What does death matter? You call me, you want me, I come!” (Tristan)
love09-middleages-courtly.md&7.11&>- And Heloise: “I was powerless to oppose you in anything. I found strength at your command to destroy myself.”
love09-middleages-courtly.md&7.11&
love09-middleages-courtly.md&7.12&## The role of sexuality^[Wagoner, following W.T.H. Jackson, “The Anatomy of Love”]
love09-middleages-courtly.md&7.12&
love09-middleages-courtly.md&7.12&>- Courtly love points out a contradiction: “How can love be reconciled with a social order which recognizes sex only in marriage but regards marriage as a contract made without love?”
love09-middleages-courtly.md&7.12&>- The answer was to move away from the reproductive function of sex (which was a concern of marriage, not love!) towards an idealised, Platonic vision of sexual union.
love09-middleages-courtly.md&7.12&
love09-middleages-courtly.md&7.13&## Body and soul
love09-middleages-courtly.md&7.13&
love09-middleages-courtly.md&7.13&>- The body of the beloved is the means by which her or his freedom is given to the lover, but it is at the same time that which limits the expression of that love.
love09-middleages-courtly.md&7.13&>- It separates as well as unites. (Wagoner)
love09-middleages-courtly.md&7.13&>- The ingenuity of romantic lovers is thus endlessly tested to find new ways of overcoming their physical separation at the same time that their separateness is enhanced. 
love09-middleages-courtly.md&7.13&
love09-middleages-courtly.md&7.14&## Tristan’s sword
love09-middleages-courtly.md&7.14&
love09-middleages-courtly.md&7.14&>- What does the drawn sword signify that lies between Tristan and Iseult in the forest?
love09-middleages-courtly.md&7.14&>     - Wagoner 61: Remember that Tristan loves Iseult as a lady, a queen, and he is her servant.
love09-middleages-courtly.md&7.14&>     - But in the woods together she is not a queen and he is not a servant. 
love09-middleages-courtly.md&7.14&>     - Their love for the moment is somehow weaker. Tristan laments that Iseult was a queen at King Mark's side, “but in this wood she lives a slave, and I waste her youth.” (Bedier 78).
love09-middleages-courtly.md&7.14&>     - In other words, their love, while it is otherworldly in its aspirations, requires the separations of worldly roles for its intensity.
love09-middleages-courtly.md&7.14&>     - There has to be a sword between them in order for them to be in love.
love09-middleages-courtly.md&7.14&>     - In a perverse sort of way, romantic lovers actually need the world in order to continually transcend it and thereby keep their "heavenly love" alive.
love09-middleages-courtly.md&7.14&
love09-middleages-courtly.md&7.15&## Obstacles and love
love09-middleages-courtly.md&7.15&
love09-middleages-courtly.md&7.15&>- Meeting obstacles is precisely what makes courtly love possible and what keeps it alive.
love09-middleages-courtly.md&7.15&>- The lovers don’t want each other so much as they want the *longing towards each other.*
love09-middleages-courtly.md&7.15&>- In Abelard and Heloise’s story, Heloise’s sexual desire reaches its height when actually Abelard is not available any more as a sexual partner (due to him being both a monk now and castrated). Her longing is both sexual and dependent on the fact that this longing can not be satisfied.
love09-middleages-courtly.md&7.15&
love09-middleages-courtly.md&7.16&## The role of death
love09-middleages-courtly.md&7.16&
love09-middleages-courtly.md&7.16&>- Both Tristan/Iseult and Abelard/Heloise are only united in death.
love09-middleages-courtly.md&7.16&>- Wagoner, 65: 
love09-middleages-courtly.md&7.16&>     - If romantic love is indeed a passion that surpasses all worldly bounds, then it is not surprising that death, as a release from the world, is its logical outcome.
love09-middleages-courtly.md&7.16&>     - Denis de Rougemont (1906-1985, author of “Love in the Western World”) argues that the dark secret of romantic love is that it is really in love with death rather than life.
love09-middleages-courtly.md&7.16&>     - Lovers do not so much want death as defy it. 
love09-middleages-courtly.md&7.16&>- The message of romantic love is that death, too, can be overcome by love.
love09-middleages-courtly.md&7.16&
love09-middleages-courtly.md&7.16&
love09-middleages-courtly.md&8.0&# Five criteria for the definition of courtly love (Singer II, 23)
love09-middleages-courtly.md&8.0&
love09-middleages-courtly.md&8.1&## Sexual love between man and woman is something good
love09-middleages-courtly.md&8.1&
love09-middleages-courtly.md&8.1&>- This is quite revolutionary.
love09-middleages-courtly.md&8.1&>- Up to that time, there wasn’t such a notion.
love09-middleages-courtly.md&8.1&>- Both Plato’s eros and Aristotelian philia are:
love09-middleages-courtly.md&8.1&>     - mainly homosexual
love09-middleages-courtly.md&8.1&>     - directed towards a goal that lies outside and beyond the love experience itself!
love09-middleages-courtly.md&8.1&>- The Christian ideals of love in part follow Plato (union with God), in part they concentrate on agape. Neither involves sexual or erotic love between man and woman.
love09-middleages-courtly.md&8.1&>- Love between the sexes was for all three sources (Plato, Aristotle, Christianity) a means of reproduction, the basis of marriage, a basis for social stability and child raising. But love between the sexes was never a good in itself.
love09-middleages-courtly.md&8.1&
love09-middleages-courtly.md&8.2&## Love ennobles both the lover and the beloved
love09-middleages-courtly.md&8.2&
love09-middleages-courtly.md&8.2&>- Courtly love makes both partners better beings.
love09-middleages-courtly.md&8.2&>- Being noble in the middle ages meant to be faithful to one’s lord or husband.
love09-middleages-courtly.md&8.2&>- This, in courtly love, was translated into the devotion of the knight to his lady, and the faithfulness of the lady to her knight.
love09-middleages-courtly.md&8.2&>- Often the woman is described as a divine being, an embodiment of perfection (quite a Platonic description!)
love09-middleages-courtly.md&8.2&>- Most often, the lady would have sexual relations with her husband, while staying faithful to her knight. She just loved her knight differently and more truly than her husband, who often was someone she was married to for practical or political reasons.
love09-middleages-courtly.md&8.2&>- The lady’s behaviour thus was parallel to the knight’s, who also served his lord, and the church, and his lady, all at the same time (in different ways).
love09-middleages-courtly.md&8.2&
love09-middleages-courtly.md&8.3&## Jealousy
love09-middleages-courtly.md&8.3&
love09-middleages-courtly.md&8.3&>- The knight might also enjoy other sexual contacts (his wife, mistresses, peasant girls), without confusing such pleasures with the “special ecstasy” (Singer II, 27) he found in the company of the beloved.
love09-middleages-courtly.md&8.3&>- Since this loving relationship was exclusive, it was the basis of jealousy. A lady could only love a single knight in this special way, so this knight had the right to be jealous of all others who might take his lady’s love away from him!
love09-middleages-courtly.md&8.3&>- In a way, jealousy was a *proof* for that special relation between a knight and his lady.
love09-middleages-courtly.md&8.3&>- This is another new characteristic of courtly love that is not in Plato, Aristotle, or Christian love.
love09-middleages-courtly.md&8.3&
love09-middleages-courtly.md&8.4&##  Sexual love cannot be reduced to a mere bodily function
love09-middleages-courtly.md&8.4&
love09-middleages-courtly.md&8.4&>- From the ancient world, people perceived the tension between sexuality and high ideals of love.
love09-middleages-courtly.md&8.4&>- Plato solved the problem by assuming that “real” love is something higher and outside of sex.
love09-middleages-courtly.md&8.4&>- Aristotle does not need sexual relations at all in his concept of philia.
love09-middleages-courtly.md&8.4&>- Courtly love, although explicitly sexual, is not reducible to the sexual component.
love09-middleages-courtly.md&8.4&>- The main idea is to worship love itself, both in its sexual and asexual forms. Sex was a part of that, but the merging of the souls was the higher good.
love09-middleages-courtly.md&8.4&
love09-middleages-courtly.md&8.5&##  Love is not necessarily related to marriage
love09-middleages-courtly.md&8.5&
love09-middleages-courtly.md&8.5&>- Courtly love emphasises the rituals of courtship (compare mating rituals of birds or other animals).
love09-middleages-courtly.md&8.5&>- The partners in courtly love enjoy one another’s company in a context that is more or less sexual, but without being related to an institution such as marriage.
love09-middleages-courtly.md&8.5&>- Courtly love is an attempt to re-introduce sexual courtesy and individual choice in an area of life that had been controlled by economic, political, and largely impersonal considerations. (Singer II, 29).
love09-middleages-courtly.md&8.5&>- It provided a safe way of fulfilling the demands of one’s erotic imagination without actually endangering one’s marriage.
love09-middleages-courtly.md&8.5&>- On the other hand, sometimes the ideals of courtly love are indeed married people (as in Ovid’s story of Orpheus and Eurydice).
love09-middleages-courtly.md&8.5&
love09-middleages-courtly.md&8.6&##  Love is an intense, passionate relationship
love09-middleages-courtly.md&8.6&
love09-middleages-courtly.md&8.6&>- Nowadays it seems normal to connect love with passion.
love09-middleages-courtly.md&8.6&>- But it was not like that for Plato or Aristotle, or even Christian love concepts.
love09-middleages-courtly.md&8.6&>- Prior to courtly love, man and woman had a relationship that was expected to be practical, not passionate.
love09-middleages-courtly.md&8.6&>- For Plato and Aristotle, passion was itself not the basis of good conduct. The ancient ideal was one of *ataraxia* or *apatheia* (Stoics, Epicureans).
love09-middleages-courtly.md&8.6&>     - Ataraxia: to not be troubled by emotions.
love09-middleages-courtly.md&8.6&>     - Apatheia: to not be driven by passion.
love09-middleages-courtly.md&8.6&>- Passion enters Christianity with the passionate love of God by the mystics. But there, passion was reserved for the love of God. It would have been sinful to be passionate in the same way towards a human being. Courtly love helped change that perception.
love09-middleages-courtly.md&8.6&
love09-middleages-courtly.md&8.7&##  Francis Newman on Courtly Love
love09-middleages-courtly.md&8.7&
love09-middleages-courtly.md&8.7&>- Francis Newman (“The Meaning of Courtly Love,” 1968): 
love09-middleages-courtly.md&8.7&>- “A love at once illicit^[Illicit: not allowed by laws or rules, or strongly disapproved of by society.] and morally elevating, passionate and disciplined, humiliating and exalting^[To exalt: To raise something or someone to a higher level.], human and transcendent.”
love09-middleages-courtly.md&8.7&>- This characterization is (on the surface) contradictory. Try to explain it so that it makes sense!
love09-middleages-courtly.md&8.7&
love09-middleages-courtly.md&8.7&
love09-middleages-courtly.md&9.0&# Platonism in courtly love
love09-middleages-courtly.md&9.0&
love09-middleages-courtly.md&9.1&## Platonism in courtly love
love09-middleages-courtly.md&9.1&
love09-middleages-courtly.md&9.1&>- All Platonists claim that the function of human love is to ultimately transcend the human and unite with something beyond the individual (ultimate beauty, God etc)
love09-middleages-courtly.md&9.1&>- The troubadours are unconcerned about anything outside of the beloved person.
love09-middleages-courtly.md&9.1&>- Their beloved is indeed more than just another beautiful form in the external world: she is for them the supreme instance of beauty and that is why they love her. (Singer II, 47)
love09-middleages-courtly.md&9.1&>- The relation [the troubadour] has to his lady is modelled on the relation humans should have towards God, but the object of that relation has shifted and become fully human.
love09-middleages-courtly.md&9.1&
love09-middleages-courtly.md&9.2&## Idealization of the beloved (Singer II, 49)
love09-middleages-courtly.md&9.2&
love09-middleages-courtly.md&9.2&>- The idealization of the beloved is a Platonic element in courtly love.
love09-middleages-courtly.md&9.2&>- The troubadour loves his lady *because* she is perfect.
love09-middleages-courtly.md&9.2&>- So perfection here is the reason for the love. This is clearly an appraisal kind of love.
love09-middleages-courtly.md&9.2&>- If the beloved were not perfect, she would not have the troubadour’s love.
love09-middleages-courtly.md&9.2&>- William IX, the first troubadour: “For you are whiter than ivory. This is why I love no other”
love09-middleages-courtly.md&9.2&>- Pons de Capdeuil: “You are so worthy, courteous in true speaking, frank and gentle, gay with humility, beautiful and pleasing, that ... you do not lack any good quality one could wish in a lady.”
love09-middleages-courtly.md&9.2&
love09-middleages-courtly.md&9.2&
love09-middleages-courtly.md&9.3&## Idealization of love itself (Singer II, 50)
love09-middleages-courtly.md&9.3&
love09-middleages-courtly.md&9.3&>- The troubadours also idealized love itself, their own desire, their own condition as lovers. (Singer II, 50)
love09-middleages-courtly.md&9.3&>- Bernard de Ventadour: “No man is of value without love. ... By nothing is man made more excellent than by love and the service of women, for thence arises delight and song and all that pertains to excellence.” And: “I crave so noble a love that my longing is already a gain.”
love09-middleages-courtly.md&9.3&
love09-middleages-courtly.md&9.4&## The revolutionary and heretic character of courtly love (Singer II, 51)
love09-middleages-courtly.md&9.4&
love09-middleages-courtly.md&9.4&>- Troubadour love is revolutionary and heretical.
love09-middleages-courtly.md&9.4&>- It refuses to define itself as subordinate to God’s love.
love09-middleages-courtly.md&9.4&>- It encourages self-sufficiency among human beings, and makes them independent of God.
love09-middleages-courtly.md&9.4&>- This is what particularly disturbed the church.
love09-middleages-courtly.md&9.4&>     - Before there had been love stories that were focussed on sexuality and desire, but that desire had never been of the spiritual quality that courtly love had, and that threatened to compete with the desire for God itself!
love09-middleages-courtly.md&9.4&>     - (Compare Heloise’s letters to Abelard).
love09-middleages-courtly.md&9.4&
love09-middleages-courtly.md&9.5&## The love of nature (leading to romanticism) (Singer II, 54)
love09-middleages-courtly.md&9.5&
love09-middleages-courtly.md&9.5&>- Since romantic love cannot be satisfied in this world, it needs another outlet. Troubadours used poetry as a means to channel the creative powers that love inspired.
love09-middleages-courtly.md&9.5&>- With the troubadours begins the poetry of nature.
love09-middleages-courtly.md&9.5&>- In their writings we find “sensitivity to landscapes, the love of nature, ... which the Romantics [much later] revived.” (Singer)
love09-middleages-courtly.md&9.5&>- The troubadours were among the first to see the things of the world as objects to be enjoyed in the imagination.
love09-middleages-courtly.md&9.5&
love09-middleages-courtly.md&9.6&## The love of nature (leading to romanticism) (Singer II, 54)
love09-middleages-courtly.md&9.6&
love09-middleages-courtly.md&9.6&>- Bernard de Ventadour: “When tender leafage doth appear, When vernal meads grow gay with flowers, And aye with singing loud and clear, The nightingale fulfills the hours, I joy in him, and joy in every flower, and in myself, and in my lady more.”
love09-middleages-courtly.md&9.6&>- Later this will inspire St. Francis of Assisi (1182–1226), who transformed courtly love back to a love of all the creatures of God. He spoke with birds, called the wind, the fire and the sun his brothers and sisters, and generally displayed an unlimited love towards all humans, animals, and natural phenomena.
love09-middleages-courtly.md&9.6&>- Love of nature much later becomes the basis of European Romanticism.
love09-middleages-courtly.md&9.6&
love09-middleages-courtly.md&9.7&## The End. Thank you for your attention!
love09-middleages-courtly.md&9.7&
love09-middleages-courtly.md&9.7&Questions?
love09-middleages-courtly.md&9.7&
love09-middleages-courtly.md&9.7&<!--
love09-middleages-courtly.md&9.7&
love09-middleages-courtly.md&9.7&%% Local Variables: ***
love09-middleages-courtly.md&9.7&%% mode: markdown ***
love09-middleages-courtly.md&9.7&%% mode: visual-line ***
love09-middleages-courtly.md&9.7&%% mode: cua ***
love09-middleages-courtly.md&9.7&%% mode: flyspell ***
love09-middleages-courtly.md&9.7&%% End: ***
love09-middleages-courtly.md&9.7&
love09-middleages-courtly.md&9.7&-->
love10-romantic.md&0.1&---
love10-romantic.md&0.1&title:  "Love and Sexuality: 10. Romantic love"
love10-romantic.md&0.1&author: Andreas Matthias, Lingnan University
love10-romantic.md&0.1&date: October 7, 2019
love10-romantic.md&0.1&...
love10-romantic.md&0.1&
love10-romantic.md&1.0&# From courtly to romantic love
love10-romantic.md&1.0& 
love10-romantic.md&1.1&## Towards romantic love: Romeo and Juliet
love10-romantic.md&1.1&
love10-romantic.md&1.1&Simplified plot:
love10-romantic.md&1.1&
love10-romantic.md&1.1&>- Romeo and Juliet, children of enemy families, fall in love with each other.
love10-romantic.md&1.1&>- They are secretly married.
love10-romantic.md&1.1&>- In a series of fights and duels, Romeo is forced to kill a cousin of Juliet.
love10-romantic.md&1.1&>- As a consequence, he is exiled from his city, Verona.
love10-romantic.md&1.1&>- Juliet’s parents decide to marry her off to someone else.
love10-romantic.md&1.1&>- In order to avoid that, Juliet drinks a “poison” that will *appear* to kill her for 42 hours. Then she will wake up again and be fine.
love10-romantic.md&1.1&>- She sends a messenger to Romeo to tell him about that, but the messenger doesn’t reach him.
love10-romantic.md&1.1&>- Romeo hears of Juliet’s death (which he believes to be genuine) and commits suicide himself at her grave.
love10-romantic.md&1.1&>- Juliet awakens to find Romeo dead, so she kills herself, this time for real.
love10-romantic.md&1.1&
love10-romantic.md&1.2&## Differences between Romeo/Juliet and courtly love
love10-romantic.md&1.2&
love10-romantic.md&1.2&>- Q: How is this story different from courtly love?
love10-romantic.md&1.2&>     - The love is more equal, mutual, less one-sided and idealizing only Juliet.
love10-romantic.md&1.2&>     - It is a love that seeks sexual fulfilment, as opposed to the distance in courtly love.
love10-romantic.md&1.2&>     - Neither Romeo nor Juliet tolerate that the other might get married to another person. For a courtly couple, this would not be such a great problem.
love10-romantic.md&1.2&>     - But observe that in the Tristan/Iseult story they also don’t want to give up their sexual love and accept Iseult’s marriage to the king!
love10-romantic.md&1.2&>     - So there is an ambiguity here about the exact role of sexuality and abstinence from it in courtly love!
love10-romantic.md&1.2&>     - As opposed to courtly love, romantic love is clearly sexual with the intention of the lovers to unite in every way.
love10-romantic.md&1.2&
love10-romantic.md&1.3&## William Shakespeare: Sonnet 18
love10-romantic.md&1.3&
love10-romantic.md&1.3&> Shall I compare thee to a summer’s day?   
love10-romantic.md&1.3&  Thou art more lovely and more temperate:   
love10-romantic.md&1.3&  Rough winds do shake the darling buds of May,   
love10-romantic.md&1.3&  And summer’s lease hath all too short a date:   
love10-romantic.md&1.3&  Sometime too hot the eye of heaven shines,   
love10-romantic.md&1.3&  And often is his gold complexion dimm’d;   
love10-romantic.md&1.3&  And every fair from fair sometime declines,   
love10-romantic.md&1.3&  By chance or nature’s changing course untrimm’d;  
love10-romantic.md&1.3&  But thy eternal summer shall not fade   
love10-romantic.md&1.3&  Nor lose possession of that fair thou owest;   
love10-romantic.md&1.3&  Nor shall Death brag thou wander’st in his shade,   
love10-romantic.md&1.3&  When in eternal lines to time thou growest:   
love10-romantic.md&1.3&  So long as men can breathe or eyes can see,   
love10-romantic.md&1.3&  So long lives this and this gives life to thee.
love10-romantic.md&1.3&
love10-romantic.md&1.4&## Modern English
love10-romantic.md&1.4&
love10-romantic.md&1.4&> Shall I compare you to a summer’s day?  
love10-romantic.md&1.4&  You are more lovely and more moderate:  
love10-romantic.md&1.4&  Harsh winds disturb the delicate buds of May,   
love10-romantic.md&1.4&  and summer doesn’t last long enough.   
love10-romantic.md&1.4&  Sometimes the sun is too hot,   
love10-romantic.md&1.4&  and its golden face is often dimmed by clouds.   
love10-romantic.md&1.4&  All beautiful things eventually become less beautiful,  
love10-romantic.md&1.4&  either by the experiences of life or by the passing of time.  
love10-romantic.md&1.4&  But your eternal beauty won’t fade,  
love10-romantic.md&1.4&  nor lose any of its quality.   
love10-romantic.md&1.4&  And you will never die,  
love10-romantic.md&1.4&  as you will live on in my enduring poetry.   
love10-romantic.md&1.4&  As long as there are people still alive to read poems,  
love10-romantic.md&1.4&  this sonnet will live, and you will live in it.^[From: http://www.nosweatshakespeare.com/sonnets/18.]
love10-romantic.md&1.4&
love10-romantic.md&1.5&## Observations
love10-romantic.md&1.5&
love10-romantic.md&1.5&>- Observe:
love10-romantic.md&1.5&>     - The use of nature images
love10-romantic.md&1.5&>     - Comparison of the beloved with the beauty of nature
love10-romantic.md&1.5&>     - Overcoming death (the same motive as in Tristan/Iseult and Abelard/Heloise stories!). Interestingly, here the death is overcome through poetry, which is a Platonic concept.
love10-romantic.md&1.5&
love10-romantic.md&1.5&
love10-romantic.md&2.0&# Examples of romanticism in literature and art
love10-romantic.md&2.0&
love10-romantic.md&2.1&## Novalis (1772-1801)
love10-romantic.md&2.1&
love10-romantic.md&2.1&> ... through the cloud I saw the glorified face of my beloved. In her eyes eternity reposed -- I laid hold of her hands, and the tears became a sparkling bond that could not be broken. Into the distance swept by, like a tempest, thousands of years. On her neck I welcomed the new life with ecstatic tears. It was the first, the only dream -- and just since then I have held fast an eternal, unchangeable faith in the heaven of the Night, and its Light, the Beloved.
love10-romantic.md&2.1&
love10-romantic.md&2.2&## A typical representative: Love in “Wuthering Heights” (Emily Brontë, 1847)
love10-romantic.md&2.2&
love10-romantic.md&2.2&> “My love for Linton is like the foliage in the woods: time will change it, I'm well aware, as winter changes the trees. My love for Heathcliff resembles the eternal rocks beneath: a source of little visible delight, but necessary. Nelly, I am Healthcliff! He's always, always in my mind: not as a pleasure, any more than I am always a pleasure to myself, but as my own being.”
love10-romantic.md&2.2&
love10-romantic.md&2.3&## A typical representative: Love in “Wuthering Heights” (Emily Brontë, 1847)
love10-romantic.md&2.3&
love10-romantic.md&2.3&> “Be with me always - take any form - drive me mad! only do not leave me in this abyss, where I cannot find you! Oh, God! it is unutterable! I can not live without my life! I can not live without my soul!”
love10-romantic.md&2.3&
love10-romantic.md&2.3&. . . 
love10-romantic.md&2.3&
love10-romantic.md&2.3&>- You see the desire for union and also how the speaker identifies with the beloved as her “own being.”
love10-romantic.md&2.3&>- But, like in courtly romances, the lovers here can be united only after death.
love10-romantic.md&2.3&
love10-romantic.md&2.4&##  Schelling’s Naturphilosophie (Singer II, 387)
love10-romantic.md&2.4&
love10-romantic.md&2.4&>- The whole universe is created as an act of God’s love.
love10-romantic.md&2.4&>- By loving, human beings themselves can experience unity with the universe.
love10-romantic.md&2.4&>- Love is, therefore, the force that keeps the universe together.
love10-romantic.md&2.4&
love10-romantic.md&2.5&##  Shelley (Singer II, 412): Essay on love^[Percy Bysshe Shelley (1792 – 8 July 1822)]
love10-romantic.md&2.5&
love10-romantic.md&2.5&> This is Love. This is the bond and the sanction [=permission] which connects not only man with man, but with every thing which exists. ... We dimly see within our intellectual nature a miniature as it were of our entire self, yet deprived of all that we condemn or despise, the ideal prototype of every thing excellent and lovely that we are capable of conceiving as belonging to the nature of man. ... Hence in solitude, or in that deserted state when we are surrounded by human beings, and yet they sympathize not with us, we love the flowers, the grass, the waters, and the sky. In the motion of the very leaves of spring, in the blue air, there is then found a secret correspondence with our heart.
love10-romantic.md&2.5&
love10-romantic.md&2.6&## Shelley, observations
love10-romantic.md&2.6&
love10-romantic.md&2.6&>- The love of nature is a kind of our love of our ideal selves.
love10-romantic.md&2.6&>- Our ideal selves: “We dimly see within our intellectual nature a miniature as it were of our entire self, yet deprived of all that we condemn or despise, the ideal prototype of every thing excellent and lovely that we are capable of conceiving as belonging to the nature of man.” -- This is clearly Platonic, the Form of man.
love10-romantic.md&2.6&>- In nature we find this perfection that we cannot easily find in human company: “In that deserted state when we are surrounded by human beings, and yet they sympathize not with us.”
love10-romantic.md&2.6&>- Love connects us to everything that exists. It is a love of the whole universe, not a personal love.
love10-romantic.md&2.6&>- In this way, the romantic ideal goes back to Plato in a much stronger way than courtly love did.
love10-romantic.md&2.6&
love10-romantic.md&3.0&#  The Romantics
love10-romantic.md&3.0& 
love10-romantic.md&3.1&##  Feeling rather than reason (Singer II, 285)
love10-romantic.md&3.1&
love10-romantic.md&3.1&>- The Romantic movement (peak around 1800-1850) emphasised the importance of emotion over rationality.
love10-romantic.md&3.1&>- Coleridge (1772-1834): “Deep thinking is attainable only by a man of deep feeling.”
love10-romantic.md&3.1&>- William Blake (1757-1827): Human imagination is the faculty that reveals God’s own creativity. It is the means by which the world becomes a unity instead of a system of unrelated objects. Through the imagination ... we participate in God’s being as the creator of such unity. We thereby indentify with him, with nature, and with all men and women. (Singer II, 287) 
love10-romantic.md&3.1&
love10-romantic.md&3.2&##  Oneness rather than dualism (Singer II, 288)
love10-romantic.md&3.2&
love10-romantic.md&3.2&>- Blending and merging (Singer II, 290/291) instead of wedding!
love10-romantic.md&3.2&>- August Wilhelm Schlegel (1767-1845): Romantic poetry is literature that perceives the cosmos as “all in all at one and the same time.”
love10-romantic.md&3.2&>- Novalis idealizes the poet who “blends himself with all the creatures of nature, one might say: feels himself into them.”
love10-romantic.md&3.2&>- Shelley in a poem to Emilia Viviani: “I am not thine. I am part of thee!”
love10-romantic.md&3.2&>- Bronte, Wuthering heights: “He’s more myself than I am. Whatever our souls are made of, his and mine are the same.”
love10-romantic.md&3.2&>- This returns to Platonic merging, but now it is not the absolute beauty of Plato, not the God of Christians, not the lady of the troubadours, but all nature that is the object of merging.
love10-romantic.md&3.2&
love10-romantic.md&3.3&##  Is romantic love appraisal or bestowal love?
love10-romantic.md&3.3&
love10-romantic.md&3.3&>- Unclear.
love10-romantic.md&3.3&>- On the one hand, it is essentially Platonic, meaning that it responds to and idealizes properties of the beloved (beauty, kindness etc). This would make it appraisal love.
love10-romantic.md&3.3&>- But: Wuthering Heights: “Nelly, I am Healthcliff! He's always, always in my mind: not as a pleasure, any more than I am always a pleasure to myself, but as my own being.” -- Here she does not appreciate particular qualities of the beloved. He is not even “a pleasure” to her.
love10-romantic.md&3.3&>- Also, when romantic love is extended to all of nature, we cannot really speak of an appraisal.
love10-romantic.md&3.3&>- If I love everyone because they are part of God’s nature, then my love has essentially become unconditional *agape*.
love10-romantic.md&3.3&
love10-romantic.md&3.4&## Romance and revolt (1)
love10-romantic.md&3.4&
love10-romantic.md&3.4&>- Love qua bestowal is inherently revolutionary. 
love10-romantic.md&3.4&>- We can see that Catherine, in Wuthering Heights, decides to love someone who is totally unsuitable by the usual social standards.
love10-romantic.md&3.4&>- Romeo and Juliet are drawn to each other although they are unsuitable partners in the eyes of their society.
love10-romantic.md&3.4&>- The same with Abelard and Heloise.
love10-romantic.md&3.4&>- This is a difference to courtly love, which was, for the most part, well-behaved and within the limits of the social conventions of its time.
love10-romantic.md&3.4&
love10-romantic.md&3.5&## Romance and revolt (2)
love10-romantic.md&3.5&
love10-romantic.md&3.5&>- More modern examples: 
love10-romantic.md&3.5&>     - Rose and Jack in “Titanic”
love10-romantic.md&3.5&>     - The homosexual love in Brokeback Mountain
love10-romantic.md&3.5&>     - Beauty and the Beast
love10-romantic.md&3.5&>     - Maid in Manhattan
love10-romantic.md&3.5&>     - Pretty Woman
love10-romantic.md&3.5&>     - Notting Hill
love10-romantic.md&3.5&>     - (sorry if the list is a bit dated -- I’m 50)
love10-romantic.md&3.5&
love10-romantic.md&3.6&## Romance and revolt (Singer)
love10-romantic.md&3.6&
love10-romantic.md&3.6&>- While appraisal orients itself around given systems of values, bestowal negates or ignores these given values.
love10-romantic.md&3.6&>- Instead, it confers private value that is distinguished by not being identical (or even compatible) with given, accepted values. 
love10-romantic.md&3.6&>- Proper love is therefore romantic, in that its bestowal creates private value, possibly opposed to societal valuing norms. 
love10-romantic.md&3.6&>- And by being opposed in this way, love becomes conscious of itself and affirms itself.
love10-romantic.md&3.6&
love10-romantic.md&3.7&## The End. Thank you for your attention!
love10-romantic.md&3.7&
love10-romantic.md&3.7&Questions?
love10-romantic.md&3.7&
love10-romantic.md&3.7&<!--
love10-romantic.md&3.7&
love10-romantic.md&3.7&%% Local Variables: ***
love10-romantic.md&3.7&%% mode: markdown ***
love10-romantic.md&3.7&%% mode: visual-line ***
love10-romantic.md&3.7&%% mode: cua ***
love10-romantic.md&3.7&%% mode: flyspell ***
love10-romantic.md&3.7&%% End: ***
love10-romantic.md&3.7&
love10-romantic.md&3.7&-->
love11-universal.md&0.1&---
love11-universal.md&0.1&title:  "Love and Sexuality: 11. Is romantic love universal?"
love11-universal.md&0.1&author: Andreas Matthias, Lingnan University
love11-universal.md&0.1&date: October 3, 2019
love11-universal.md&0.1&...
love11-universal.md&0.1&
love11-universal.md&1.0&# Is romantic love universal?
love11-universal.md&1.0&
love11-universal.md&1.1&## The universality of romantic love
love11-universal.md&1.1&
love11-universal.md&1.1&>- Is romantic love universal or does it depend on particular historical events that happened in the West?
love11-universal.md&1.1&>- Is romantic love therefore a typically Western phenomenon, or does it exist in similar ways in other cultures? 
love11-universal.md&1.1&>- What do you think? How could we decide the question?
love11-universal.md&1.1&
love11-universal.md&1.2&## The argument for universal romantic love
love11-universal.md&1.2&
love11-universal.md&1.2&>- If romantic love was *universal* we should be able to find it practised in similar ways in different cultures that don’t share a common history.
love11-universal.md&1.2&>- But this is not easy to do. Can you see why?
love11-universal.md&1.2&>- Today it is difficult to find independent cultures. Most of the world’s cultures have been heavily influenced by Western ideas, often through mass media: comics, movies, books, music.
love11-universal.md&1.2&>- Therefore, if we want to find evidence for the universality of romantic love, we’d have to look at:
love11-universal.md&1.2&>     - Historical accounts of different cultures;
love11-universal.md&1.2&>     - Old myths, stories and literary works;
love11-universal.md&1.2&>     - Cultures that have had little contact with the West: indigenous, largely isolated cultures in remote areas Africa, South America and parts of Asia.
love11-universal.md&1.2&
love11-universal.md&1.2&
love11-universal.md&1.3&## Why would romantic love be universal?
love11-universal.md&1.3&
love11-universal.md&1.3&>- If we want to claim that romantic love is universal, we should also be able to provide an explanation for *why* this would be the case.
love11-universal.md&1.3&>- How could we do that?
love11-universal.md&1.3&>- If you want to claim that there is something that unites us, despite all our differences in culture, then this is likely some biological factor that is common to all human beings.
love11-universal.md&1.3&>- Therefore, we would look at a biological explanation for romantic love.
love11-universal.md&1.3&
love11-universal.md&1.4&## The evolutionary advantage argument
love11-universal.md&1.4&
love11-universal.md&1.4&>- In our (Western) conception of biology and evolution, we could try to find a reason why romantic love provides an evolutionary advantage over other concepts of love.
love11-universal.md&1.4&>- If we could do that, we would not only justify the universality of romantic love, but also leave some room for (less well-adapted) societies that have *not* developed the same concept.
love11-universal.md&1.4&
love11-universal.md&1.5&## Countering the evolutionary advantage argument (1)
love11-universal.md&1.5&
love11-universal.md&1.5&>- Evolutionary explanations of this type are hard to disprove with counter-examples, because one can always claim that the particular counter-example is just some less-fit society that will in future die out.
love11-universal.md&1.5&>     - Since Western culture continues to expand, this claim is very hard to disprove. Indeed, cultures that do things in a non-Western way are rare and often threatened by extinction.
love11-universal.md&1.5&>     - But is this because they are less adapted in terms of their love concepts, or is it because the world has other incentives to adopt Western culture (economic ones, for instance)? Or is it because we have the power to grab other cultures’ living spaces away from them and push them towards extinction? (Australia, Amazon and many others).
love11-universal.md&1.5&
love11-universal.md&1.6&## Countering the evolutionary advantage argument (2)
love11-universal.md&1.6&
love11-universal.md&1.6&>- So, rather than pointing towards the factual evolutionary success of a cultural practice (for which the reasons may be unclear) it is necessary for the proponent of an “evolutionary advantage” theory to actually make a plausible case *why precisely* a particular practice is advantageous.
love11-universal.md&1.6&>- The explanation itself has to be convincing. We cannot only make an argument from the historical success of cultures that have that practice.
love11-universal.md&1.6&
love11-universal.md&1.6&
love11-universal.md&2.0&# Thesis I: Romantic love is the result of historical developments
love11-universal.md&2.0&
love11-universal.md&2.1&## Beigel, H.G.: Romantic Love.^[American Sociological Review, Vol. 16, No. 3 (Jun., 1951), pp. 326-334]
love11-universal.md&2.1&
love11-universal.md&2.1&>- Three phases of romantic love:
love11-universal.md&2.1&>     - The origin of courtly love in the twelfth century;
love11-universal.md&2.1&>     - Its revival at the turn of the nineteenth century;
love11-universal.md&2.1&>     - Its present state and significance for marital selection.
love11-universal.md&2.1&
love11-universal.md&2.2&## Courtly love
love11-universal.md&2.2&
love11-universal.md&2.2&>- “Courtly love ... institutionalized certain aspects of the male-female relationship outside marriage.”
love11-universal.md&2.2&>- “In conformity with the Christian concept of and contempt for sex, the presupposition for [courtly love] was chastity. ... Such love was deemed to be impossible between husband and wife.”
love11-universal.md&2.2&
love11-universal.md&2.2&
love11-universal.md&2.3&## The loss of family ties
love11-universal.md&2.3&
love11-universal.md&2.3&>- “Starting in the fourteenth century, the dissolution of the broader family had progressed to the point where its economic, religious, and political functions were gone.”
love11-universal.md&2.3&>- “With increasing urbanization the impact of social isolation made itself felt upon the individual.”
love11-universal.md&2.3&
love11-universal.md&2.4&## The Romantic movement
love11-universal.md&2.4&
love11-universal.md&2.4&>- “The Romanticists rebelled against the progressing de-humanization, all-devouring materialism and rationalism, and sought escape from these dangers in the wonders of the emotions.”
love11-universal.md&2.4&>- In the basic feelings of humanity they hoped to find security and a substitute for eliminated cultural values.”
love11-universal.md&2.4&
love11-universal.md&2.5&## Beigel, summary
love11-universal.md&2.5&
love11-universal.md&2.5&>- “To summarize: courtly love, romantic love, and their modern derivative should be considered cultural phenomena evolved from basic human feelings that have gradually developed forms useful as replacements for discarded or decaying cultural concepts.”
love11-universal.md&2.5&>- “Love aims at and assists in the adjustment to frustrating experiences.”
love11-universal.md&2.5&>- You see here an attempt to justify the development of romantic love from the historical contingencies and necessities of:
love11-universal.md&2.5&>     - The courtly period’s values and family structure; and
love11-universal.md&2.5&>     - The loss of family ties and the commercialisation of society towards the 19th century (the high point of Romanticism).
love11-universal.md&2.5&
love11-universal.md&2.5&
love11-universal.md&2.6&## Lindholm: Anthropology vs sociobiology
love11-universal.md&2.6&
love11-universal.md&2.6&>- Two different approaches:
love11-universal.md&2.6&>- “Anthropological students of emotion are interested in discovering when and where romantic love occurs, and in correlating its emergence with particular social and psychological preconditions.” (Lindholm, RLA, 13)^[Lindholm, Charles (2006): Romantic Love and Anthropology. Etnofoor 10:1-12]
love11-universal.md&2.6&>- “Those influenced by sociobiology, in contrast, believe love must necessarily appear in all human societies, since it is genetically ingrained.”
love11-universal.md&2.6&>- So, the question is: Is romantic love a cultural practice dependent on society, or is it a biological phenomenon that appears in all societies in the same way?
love11-universal.md&2.6&
love11-universal.md&2.7&## Love and marriage
love11-universal.md&2.7&
love11-universal.md&2.7&>- One reason to assume that cultural practices determine our understanding of romantic love is that, in other cultures, love and marriage don’t always go together.
love11-universal.md&2.7&>- “In fact, in most of the complex societies for which we have records of romantic passion, conjugal love between husband and wife was considered both absurd and impossible.” (RLA 14)
love11-universal.md&2.7&>- Why would this be?
love11-universal.md&2.7&>     - The membership in one’s father’s clan determines claims to property, leadership or honour.
love11-universal.md&2.7&>     - Marriage created connections to other clans and their respective claims.
love11-universal.md&2.7&>     - “In this context matrimony was too important a matter to be decided by young people swept away by passion. Rather, marriage arrangements were negotiated by powerful elders whose job was to advance the interests of the clan -- much as royal marriages are still arranged today.” (RLA 14)
love11-universal.md&2.7&
love11-universal.md&2.7&
love11-universal.md&2.8&## Romantic feelings outside of marriage
love11-universal.md&2.8&
love11-universal.md&2.8&>- “Because love with one's spouse was next to impossible, romantic feelings were directed instead toward individuals one could not marry.” (RLA 15)
love11-universal.md&2.8&>- This could cause problems:
love11-universal.md&2.8&>     - In Tokugawa Japan (1603-1868), “love dramas always revolved around the conflicts caused by relationships between respectable men and their courtesans.” (RLA 15)
love11-universal.md&2.8&>     - In ancient Rome, powerful men would fall in love with slaves.
love11-universal.md&2.8&>     - “Roman poets idealized their beloved slave prostitutes as domina, literally reversing the role of master and slave.”
love11-universal.md&2.8&>- In order to avoid such problems, many societies explicitly stress the chastity of romantic relationships (like in the Middle Ages).
love11-universal.md&2.8&
love11-universal.md&2.9&## The Marri Baluch (1)
love11-universal.md&2.9&
love11-universal.md&2.9&>- Tribes of people living in Pakistan, Iran and Afghanistan.
love11-universal.md&2.9&>- “The Marri inhabit a harsh, isolated and unforgiving world. They are highly individualistic, self-interested and competitive, and expect opportunism and manipulation from all social transactions. Their personal lives are dominated by fear, mistrust, and hostility; secrecy and social masking are at a premium, while collective action and cooperation are minimal.” (RLA 18)
love11-universal.md&2.9&
love11-universal.md&2.10&## The Marri Baluch (2)
love11-universal.md&2.10&
love11-universal.md&2.10&>- But romantic relationships are idealised.
love11-universal.md&2.10&>- A love affair implies “absolute trust, mutuality, and loyalty; such a love is to be pursued at all costs. Romance is both the stuff of dreams, and of life. Frustrated lovers among the Marri may commit suicide, and become celebrated in the romantic poems and songs that are the mainstay of Marri art.”
love11-universal.md&2.10&>- Romance, for the Marri, is “absolutely opposed to marriage, which is never for love.”
love11-universal.md&2.10&>- While marriage is public and officially endorsed, romantic love has to be kept secret and is dangerous. It is also non-sexual (RLA 22)
love11-universal.md&2.10&
love11-universal.md&2.10&
love11-universal.md&2.11&## Different societies and their ideas of romantic love (1)
love11-universal.md&2.11&
love11-universal.md&2.11&>- In rigid, antagonistic (Marri) and complex societies (courtly, ancient Roman, old Japanese), “the idealization offered by romantic love offers a way of imagining a different and more fulfilling life. But because of the objective reality of the social environment, romance can never form the base for actually constructing the family ... It must instead stand against and outside of the central social formation.” (RLA 24)
love11-universal.md&2.11&
love11-universal.md&2.12&## Different societies and their ideas of romantic love (2)
love11-universal.md&2.12&
love11-universal.md&2.12&>- In societies with fluid social relations, that are individualistic and operating in insecure environments, people “may find meaning and emotional warmth in the mutuality of romantic relationships. Romance in these societies is associated with marriage, since the couple is idealized as the ultimate refuge against the hostile world, and functions as the necessary nucleus of the atomized social organization.” (RLA 25)
love11-universal.md&2.12&
love11-universal.md&2.12&
love11-universal.md&3.0&# Thesis II: Romantic love is universal
love11-universal.md&3.0&
love11-universal.md&3.1&## Schiefenhoevel: Symptoms of romantic love
love11-universal.md&3.1&
love11-universal.md&3.1&>- Romantic love can be identified by particular “symptoms” (that are similar to addiction):
love11-universal.md&3.1&>     - “Electrified” reactions, elated, joyous feelings, “butterflies in the stomach”, increased heartbeat and sweat production, changed body posture and facial expression ...
love11-universal.md&3.1&>     - when thinking of him/her, when her/his name is said by someone, when the other comes into view, when one is together with the other ... (Schiefenhoevel 40)^[Schiefenhoevel (2009): Romantic Love. Human Ontogenetics 3(2).]
love11-universal.md&3.1&>- For Schiefenhoevel, these are expressions of a deeper, biological mechanism.
love11-universal.md&3.1&>- The secrecy of romantic love, for example, “may well be another endowment of our evolutionary past when powerful alpha males were trying to prevent other males to copulate with females ...”
love11-universal.md&3.1&
love11-universal.md&3.2&## The universality of romantic love
love11-universal.md&3.2&
love11-universal.md&3.2&>- Ancient texts show that romantic love is universal, even in Western history, where it has been disputed (see previous reading!)
love11-universal.md&3.2&>     - Helena and Paris;
love11-universal.md&3.2&>     - Heracles and Omphale;
love11-universal.md&3.2&>     - Dido and Aeneas;
love11-universal.md&3.2&>     - Medea and Jason. (Schiefenhoevel, 41-42)
love11-universal.md&3.2&>- Middle Ages, Walther von der Vogelweide: “Under der Linden”;
love11-universal.md&3.2&>- Love song of an Eipo woman (New Guinea);
love11-universal.md&3.2&>- ... and other examples (Schiefenhoevel, 46-47)
love11-universal.md&3.2&
love11-universal.md&3.3&## The evolutionary role of romantic love (1)
love11-universal.md&3.3&
love11-universal.md&3.3&>- “The more we know about the underlying biology of our erotic, sexual and social feelings the more it becomes evident that the corresponding perceptions, feelings, and behaviours are common human heritage, basically part of all people, not likely to be dramatically altered by culture.”
love11-universal.md&3.3&>- “I am sure that the universality of romantic love will be increasingly demonstrated by future crosscultural and other research.” (Schiefenhoevel, 48-49)
love11-universal.md&3.3&
love11-universal.md&3.4&## The evolutionary role of romantic love (2)
love11-universal.md&3.4&
love11-universal.md&3.4&>- “Possibly then, this transformation of emotions to an artistically shaped metalevel is also a biologically efficient enhancement mechanism: One may live one’s highs and peaks again and repeatedly by creating a poem or song capturing them. One’s feelings will, thus, become even more powerful. This could have been a base for its becoming an evolved trait.” (Schiefenhoevel, 48-49)
love11-universal.md&3.4&
love11-universal.md&3.5&## The evolutionary role of romantic love (3)
love11-universal.md&3.5&
love11-universal.md&3.5&>- “My hypothesis is that the psychic condition of being involved in romantic love functions as honest signal.” (Schiefenhoevel, 48-49)
love11-universal.md&3.5&>- “Honest” signals are signals of interest that cannot be faked: erection, sweat patterns and others.
love11-universal.md&3.5&>- Such signals are valuable because they allow the other party to see that the lover’s interest is genuine.
love11-universal.md&3.5&>- And genuine interest is better for procreation and the stability of the family.
love11-universal.md&3.5&>- “To have a powerful, convincing love song made by somebody, to receive other signs of his/her deep involvement, must have always exercised a strong attraction. As women are the ones who choose partners for reproduction ... it is likely that expressing romantic love ... in poetry, song or otherwise, should be a male form of courtship behaviour.”
love11-universal.md&3.5&
love11-universal.md&3.5&
love11-universal.md&4.0&# Thesis III: Romantic love is universal, but expressed differently in different cultures
love11-universal.md&4.0&
love11-universal.md&4.1&## Source
love11-universal.md&4.1&
love11-universal.md&4.1&>- Victor Karandashev (2015): A Cultural Perspective on Romantic Love
love11-universal.md&4.1&
love11-universal.md&4.2&## China (1)
love11-universal.md&4.2&
love11-universal.md&4.2&>- In early Chinese history, attitudes toward passionate love and sexual desire were generally positive.
love11-universal.md&4.2&>- “In the Late Empire (approximately 1,000 years ago), when the Neo-Confucianists gained political and religious power, Chinese attitudes gradually altered and became more repressive concerning sexuality.”
love11-universal.md&4.2&>- “Erotic art and literature were often burned. Since neither spouse had chosen each other, it mattered little whether or not they were sexually attracted to each other. Their primary duty was to procreate.”
love11-universal.md&4.2&>- “The husband was assumed to be biologically destined to seek satisfaction with a variety of women. ... The woman was not  so destined; her chief function was to give birth to children, and she was expected to remain faithful to her husband (Murstein, 1974, p. 469).” (Karandashev, 6)
love11-universal.md&4.2&
love11-universal.md&4.3&## China (2)
love11-universal.md&4.3&
love11-universal.md&4.3&>- “Displays of love outside marriage were restricted. Even in the event that the partners were highly attracted to each other, it was contrary to custom to express the slightest degree of public affection.”
love11-universal.md&4.3&>- In the People’s Republic of China (after 1949):
love11-universal.md&4.3&>     - Party policy constructed an altruism which assumed that men and women work hard during the day, without being “deflected or confused” by love, sexual desire, or any strivings for private happiness (Gil, 1992, p. 571). 
love11-universal.md&4.3&>     - “True” happiness was based on spiritual rather than material enjoyment, on public rather than private interest, on collective welfare rather than on individual happiness” (Murstein, 1974, p. 482).^[Karandashev 6-7]
love11-universal.md&4.3&
love11-universal.md&4.3&
love11-universal.md&4.4&## Europe
love11-universal.md&4.4&
love11-universal.md&4.4&>- Medieval England (before 12th century): Love is self-sacrificing and unselfish, similar to friendship.
love11-universal.md&4.4&>- 12th-14th centuries: Growth of the courtly love ideal. “The key feature of courtly love was suffering and longing due to separation from the loved one.”
love11-universal.md&4.4&>- 16th-17th centuries: Shakespeare, “love was described as a consuming passion, strong illness, or powerful force that is impossible to resist.”
love11-universal.md&4.4&>- 20th century: “Relaxation of sexual morals in Europe and the “sexual revolution” of the 1960s to early 1970s. Furthermore, some expressions concerning love began to refer to sexual desire.” (Karandashev 7)
love11-universal.md&4.4&
love11-universal.md&4.5&## Anthropological studies
love11-universal.md&4.5&
love11-universal.md&4.5&>- Jankowiak and Fischer (1992) explored romantic love in 166 cultures around the world.
love11-universal.md&4.5&>- Indicators of love: 
love11-universal.md&4.5&>     - young lovers talk about passionate love, 
love11-universal.md&4.5&>     - they recount tales of love, 
love11-universal.md&4.5&>     - sing love songs, 
love11-universal.md&4.5&>     - and speak of the longings and anguish of infatuation.
love11-universal.md&4.5&>- The researchers found that romantic love was present in 147 out of 166 cultures (88.5%). 
love11-universal.md&4.5&>- For the remaining 19 cultures, there were no signs indicating that people experience romantic love (Karandashev 8).
love11-universal.md&4.5&>- But in the original paper, Jankowiak and Fischer guess that it is the lack of material and research that makes these cultures appear to not have romantic love (for example the absence of recorded songs), rather than a true absence of the phenomenon.
love11-universal.md&4.5&
love11-universal.md&4.5&
love11-universal.md&4.6&## Biological basis of romantic love (1)
love11-universal.md&4.6&
love11-universal.md&4.6&>- Bartels and Zeki (2000) interviewed young men and women from 11 countries ... who claimed to be “truly, deeply, and madly” in love and then used fMRI (brain imaging) techniques to identify the corresponding brain activities.
love11-universal.md&4.6&>- The authors concluded that passionate love suppresses the activity in the areas of the brain responsible for critical thought.
love11-universal.md&4.6&>- Passion also produced increased activity in the brain areas associated with euphoria and reward, and decreased levels of activity in the areas associated with distress and depression.
love11-universal.md&4.6&>- Brain scan using fMRI technique have found only small differences ... even if comparing the results of two very different cultures like America and China (Xu et al., 2010). The brain regions associated with romantic love seem to be very primitive ones leading scholars to conclude that love feelings were always present in hominid evolution (Fisher, 2004).^[(Karandashev 9-10).]
love11-universal.md&4.6&
love11-universal.md&4.7&## Biological basis of romantic love (2)
love11-universal.md&4.7&
love11-universal.md&4.7&>- People who are madly in love and asked to think about their beloved tend to show activity in the ... area associated with euphoria and addiction. 
love11-universal.md&4.7&>- These findings confirm that passionate love is rooted in a very basic evolutionary system. 
love11-universal.md&4.7&>- Passionate love served to guarantee mate selection, long-term romantic relationships, and the survival of our species. (Karandashev 11)
love11-universal.md&4.7&
love11-universal.md&4.8&## Individualism and collectivism (1)
love11-universal.md&4.8&
love11-universal.md&4.8&>- Karen and Kenneth Dion (1991) found that people who are more individualistic exhibit less likelihood of ever having been in love.
love11-universal.md&4.8&>- Greater individualism was associated with a perception of their relationships as less rewarding and less deep.
love11-universal.md&4.8&>- Generally, the more individualistic a person, the lower the quality of experience of love for his or her partner. (Karandashev 12)
love11-universal.md&4.8&>- Dion and Dion (2005) found that people who are high in individualism tend to report less happiness in their marriages as well as lower satisfaction with their family life and friends.
love11-universal.md&4.8&
love11-universal.md&4.9&## Individualism and collectivism (2)
love11-universal.md&4.9&
love11-universal.md&4.9&>- In collectivistic cultures, people experience the dependencies in their lives being embedded in multiple relationships with their family and close friends. 
love11-universal.md&4.9&>- Collectivism is related to the view of love as pragmatic, based on friendship, and having altruistic goals (Dion & Dion, 2005). 
love11-universal.md&4.9&>- Women in collectivistic cultures endorse an altruistic view of love more commonly than women in individualistic cultures; they consequently place greater emphasis on a broader network of close friendships. (Karandashev 12)
love11-universal.md&4.9&
love11-universal.md&4.9&
love11-universal.md&4.10&## Americans, Lithuanians and Russians: The core of romantic love
love11-universal.md&4.10&
love11-universal.md&4.10&>- Comparison between United States, Lithuania, and Russia (Karandashev 13)
love11-universal.md&4.10&>- People from all three countries agreed on the following characteristics as the “core” of romantic love: 
love11-universal.md&4.10&>     - (1) the eros component of love (physical attraction), 
love11-universal.md&4.10&>     - (2) the essence of altruistic love (agape), 
love11-universal.md&4.10&>     - (3) the tendency of lovers to engage in intrusive thinking about the beloved, 
love11-universal.md&4.10&>     - (4) a concept of transcendence: the feeling that the union of two lovers results in something more meaningful than just the two lovers.
love11-universal.md&4.10&>- There was agreement across all cultures that love is a strong feeling and that lovers ultimately want to be together. 
love11-universal.md&4.10&>- Only altruism was considered more important in the United States than in Lithuania and Russia.
love11-universal.md&4.10&
love11-universal.md&4.11&## Americans, Lithuanians and Russians: The core of romantic love
love11-universal.md&4.11&
love11-universal.md&4.11&>- Several differences among individualism (US) and collectivism (Lithuania and Russia): 
love11-universal.md&4.11&>     - Lithuanians and Russians resembled each other much more than either group resembles Americans. 
love11-universal.md&4.11&>     - Russians and Lithuanians perceived love as an unreal fairy tale and expect it at some point either to come to an end or to transfer to a more real and enduring relationship that lacks the initial excitement of romantic love. 
love11-universal.md&4.11&>     - Only then, they believed, “real” love and friendship set in. 
love11-universal.md&4.11&>     - In the United States, participants perceived romantic love as more realistic and less illusionary. 
love11-universal.md&4.11&
love11-universal.md&4.11&
love11-universal.md&4.12&## Americans, Lithuanians and Russians: The core of romantic love
love11-universal.md&4.12&
love11-universal.md&4.12&>- Americans (in contrast with Lithuanians and Russians) also included friendship in romantic love.
love11-universal.md&4.12&>- Lithuanians and Russians fall in love much more quickly than Americans. 
love11-universal.md&4.12&>     - The study found that 90% of Lithuanians reported that they fell in love within a month or less, whereas 58% of Americans fell in love within a time frame of two months to a year (de Munck, Korotayev, de Munck, & Khaltourina, 2011). 
love11-universal.md&4.12&>- When Sprecher and her colleagues (1994) asked people of different nationalities whether they were currently in love, Russians were the people who were found to be most in love (67%), Americans were in the middle (58%), and the Japanese were the least likely to be in love (52%). (Karandashev 13)
love11-universal.md&4.12&
love11-universal.md&4.12&
love11-universal.md&4.13&## Emotional investment across countries (1)
love11-universal.md&4.13&
love11-universal.md&4.13&>- Ingersoll-Dayton, Campbell, Kurokawa, and Saito (1996) compared how marriages develop over the long term in the United States versus Japan. 
love11-universal.md&4.13&>- In the United States marriages start out with a relatively high level of intimacy and the respective partners try to keep the intimacy of the relationship while maintaining a separate identity.
love11-universal.md&4.13&>- Japanese marriages, on the other hand, are at first characterized by many obligations that the married couple has to the other people in their social relationships. 
love11-universal.md&4.13&>- The intimacy develops later in life when other close family members, to whom the couple had obligations, die and when the husband becomes more willing to share affection with the wife. (Karandashev 14)
love11-universal.md&4.13&
love11-universal.md&4.14&## Emotional investment across countries (2)
love11-universal.md&4.14&
love11-universal.md&4.14&>- Schmitt (2006) and his colleagues explored cross-culturally emotional investment as a dimension of love: Loving, affectionate, cuddlesome, compassionate, and passionate.
love11-universal.md&4.14&>     - 15,234 participants from 48 countries.
love11-universal.md&4.14&>     - North American participants exhibited a higher level of emotional investment, significantly higher than those in all other regions of the world.
love11-universal.md&4.14&>     - East Asia had levels of emotional investment significantly lower than those of all other world regions.
love11-universal.md&4.14&>     - Tanzania, Hong Kong, and Japan were the countries with the lowest levels of emotional investment, whereas the United States, Slovenia, and Cyprus were the countries with the highest levels of emotional investment (Schmitt et al., 2009). (Karandashev 14)
love11-universal.md&4.14&
love11-universal.md&4.14&
love11-universal.md&4.15&## Karandashev’s conclusions
love11-universal.md&4.15&
love11-universal.md&4.15&>- Romantic love is universal but expressed in different cultural patterns.
love11-universal.md&4.15&>- Love emotions are experienced by many people, in various historical periods, and in most cultures of the world. 
love11-universal.md&4.15&>- Yet, these feelings display diversity - cultures influence how people feel, think, and behave being in romantic love.
love11-universal.md&4.15&>- Thus, love is universal, but still culturally specific.
love11-universal.md&4.15&>- The cultural perspective is as much powerful as evolutionary heritage in understanding of love.
love11-universal.md&4.15&>- Evolutionary psychology and neuroscience explain why passionate love is universal and equally intense in different cultures, while cultural influence demonstrates how romantic love is expressed in multiple cultural forms.
love11-universal.md&4.15&>- The research findings indicate that universal features primarily relate to love *experience,* while culturally influenced features are ones that pertain to the *expressions of love:* cultural rituals of love.
love11-universal.md&4.15&
love11-universal.md&4.15&
love11-universal.md&4.16&## The End. Thank you for your attention!
love11-universal.md&4.16&
love11-universal.md&4.16&Questions?
love11-universal.md&4.16&
love11-universal.md&4.16&<!--
love11-universal.md&4.16&
love11-universal.md&4.16&%% Local Variables: ***
love11-universal.md&4.16&%% mode: markdown ***
love11-universal.md&4.16&%% mode: visual-line ***
love11-universal.md&4.16&%% mode: cua ***
love11-universal.md&4.16&%% mode: flyspell ***
love11-universal.md&4.16&%% End: ***
love11-universal.md&4.16&
love11-universal.md&4.16&-->
love12-duty.md&0.1&---
love12-duty.md&0.1&title:  "Love and Sexuality: 12. Is there a duty to love?"
love12-duty.md&0.1&author: Andreas Matthias, Lingnan University
love12-duty.md&0.1&date: September 1, 2019
love12-duty.md&0.1&...
love12-duty.md&0.1&
love12-duty.md&1.0&# Kant’s view on duties and emotions
love12-duty.md&1.0&
love12-duty.md&1.1&## Sources
love12-duty.md&1.1&
love12-duty.md&1.1&- Arroyo, C. (2016). Kant on the Emotion of Love. European Journal of Philosophy, 24(3), 580-606.
love12-duty.md&1.1&- Driver, Julia. "Love and Duty." Philosophic Exchange 44.1 (2014): 1.
love12-duty.md&1.1&- Liao, S. Matthew. "The idea of a duty to love." The Journal of Value Inquiry 40.1 (2006): 1-22.
love12-duty.md&1.1&
love12-duty.md&1.1&
love12-duty.md&1.2&## Kant's good motivation
love12-duty.md&1.2&
love12-duty.md&1.3&### Kant:
love12-duty.md&1.3&A good motivation: “To do what is right *for no other reason* than that it is right.”
love12-duty.md&1.3&
love12-duty.md&1.3&**Note:** for no *other* reason (not: for no reason!)
love12-duty.md&1.3&
love12-duty.md&1.4&## Motivation and inclination
love12-duty.md&1.4&
love12-duty.md&1.4&>- What does “inclination” mean? How is it related to morality?
love12-duty.md&1.4&>- “Inclination” means that I like to do something. For example, I have an inclination to do gardening, to sing, to help my friends, to go to the cinema and so on.
love12-duty.md&1.4&>- What happens if the moral duty coincides with what I like to do? Can I then still distinguish the motives?
love12-duty.md&1.4&>- Say, I like to sing, and I fulfill my moral duty by singing for free at charity concerts. Is this morally praiseworthy?
love12-duty.md&1.4&>- Now say somebody else (a big star) hates singing for free, but also does it for charity. Is he more praiseworthy than me?
love12-duty.md&1.4&>- Kant: probably yes, because he acts against his inclination. He does what is right *for no other reason* than that it is right.
love12-duty.md&1.4&
love12-duty.md&1.5&## Benefiting friends
love12-duty.md&1.5&
love12-duty.md&1.5&>- For Kant, acting **against inclination is praiseworthy,** because if I have an inclination to act in a particular way, then my inclination is a *reason* to act in this way, and so I’m not acting without “any other reason than that my action is morally right.”
love12-duty.md&1.5&>- If two of your classmates ask you to copy your lecture notes from the previous session, and one is your friend, while the other is someone you hate. Which one would it be more praiseworthy to let copy your notes? 
love12-duty.md&1.5&>- (The one you hate!)
love12-duty.md&1.5&
love12-duty.md&1.5&
love12-duty.md&1.6&## Central ideas of Kant's system
love12-duty.md&1.6&
love12-duty.md&1.6&>- **The morally right action is the rational action.**
love12-duty.md&1.6&>- Acting against reason means acting in a morally wrong way.
love12-duty.md&1.6&>- We can use our reason to find out what the morally right action is.
love12-duty.md&1.6&
love12-duty.md&1.7&## Kant: Overview (graphics)
love12-duty.md&1.7&![](graphics/kant-01.png)
love12-duty.md&1.7&
love12-duty.md&1.8&## Notes on Kant (1)
love12-duty.md&1.8&*Numbers refer to the previous picture!*
love12-duty.md&1.8&
love12-duty.md&1.8&1. Good motivation is necessary but not sufficient for morally right action.
love12-duty.md&1.8&2. Categorical: must be obeyed, unconditional; Imperative: order, command.
love12-duty.md&1.8&3. Maxim: the principle behind an action.
love12-duty.md&1.8&4. You simply can not will a maxim that is immoral. Doing so would lead to a contradiction (for perfect duties), or to a world you don't want (imperfect duties). Morality is dictated by reason.
love12-duty.md&1.8&
love12-duty.md&1.9&## Notes on Kant (2)
love12-duty.md&1.9&
love12-duty.md&1.9&5. "Humanity:" All rational and autonomous beings (for example including rational and autonomous aliens, but not animals!)
love12-duty.md&1.9&6. You have to respect yourself in the same way as you respect others (sacrifice, suicide!).
love12-duty.md&1.9&7. You *can* treat others as means, but not *only* as means!
love12-duty.md&1.9&8. At the same time you treat them as means, you must *also* treat them as ends.
love12-duty.md&1.9&9. "End:" the target of an action. The ultimate reason for doing something.
love12-duty.md&1.9&
love12-duty.md&1.10&## Derived and original value (worth)
love12-duty.md&1.10&
love12-duty.md&1.10&>- We must distinguish things that have relative and derived value (like diamond rings, or cinema tickets) from things that have intrinsic worth, or dignity (like human beings).
love12-duty.md&1.10&>- Things that have dignity cannot be traded in the same way as things that have mere value.
love12-duty.md&1.10&>- I can trade a bicycle for a surfboard, but I can not trade one child for another.
love12-duty.md&1.10&>- Human beings are special.
love12-duty.md&1.10&>- Out of all things in nature, only they are able to be *autonomous,* that is, to make decisions for themselves. This is what gives them their intrinsic worth.
love12-duty.md&1.10&
love12-duty.md&1.10&
love12-duty.md&1.11&## Love as a “pathological” feeling
love12-duty.md&1.11&
love12-duty.md&1.11&>- What would Kant say about romantic or emotional love? Is it a good thing?
love12-duty.md&1.11&>- No, because such love is “pathological”: it is based on “pathos,” emotion.
love12-duty.md&1.11&>- Why is this bad?
love12-duty.md&1.11&>- First, emotions can be irrational and deceiving. We might not feel love for those we should love (for example, family members). Or we might love those that don’t deserve our love.
love12-duty.md&1.11&>- Second, every action that we perform in order to satisfy our own emotions is, in a way, selfish.
love12-duty.md&1.11&
love12-duty.md&1.11&
love12-duty.md&1.12&## Duty out of inclination is selfish and unreliable
love12-duty.md&1.12&
love12-duty.md&1.12&Kant:
love12-duty.md&1.12&
love12-duty.md&1.12&> “If we now, on the other hand, take well-doing (Wohltätigkeit) from love, and consider a man who loves from inclination, we find that such a man has a need of other folk, to whom he can show his kindness. He is not content if he does not find people to whom he can do good. A loving heart has an immediate pleasure and satisfaction in well-doing, and finds more pleasure in that than in its own enjoyment.”
love12-duty.md&1.12&
love12-duty.md&1.13&## Duty and inclination:
love12-duty.md&1.13&
love12-duty.md&1.13&Vigilantius’ notes of Kant’s lectures on the Metaphysics of Morals:
love12-duty.md&1.13&
love12-duty.md&1.13&> “No duty, and hence, not the duty of love either, can be founded on inclination ... This is the basis of love in all those cases where, in exercising the act of love, we have our own welfare in view ... We call this love from inclination ‘kindness’ ..., when it has the intention of laying upon the other an obligation towards us, and is thus coupled with an interest.”
love12-duty.md&1.13&
love12-duty.md&1.13&
love12-duty.md&1.14&## Love that delights and love that wishes well
love12-duty.md&1.14&
love12-duty.md&1.14&>- Kant distinguishes:
love12-duty.md&1.14&>     - The pathological love that wishes well (Liebe des Wohlwollens); and
love12-duty.md&1.14&>     - The love that delights (Liebe des Wohlgefallens).
love12-duty.md&1.14&>- “Love that delights” is appraisal love (or, in Aristotle’s terms, friendship for pleasure).
love12-duty.md&1.14&>- Love that wishes well (beneficence) is not as valuable when it is based on delight love.
love12-duty.md&1.14&>- Both are “pathological” forms of love, expressions of love that are not guided by rational moral principles but by one’s own self-love (i.e., a concern for one’s own good). (Arroyo p.588)
love12-duty.md&1.14&
love12-duty.md&1.15&## Love cannot be commanded
love12-duty.md&1.15&
love12-duty.md&1.15&>- For Kant, both kinds of love cannot be commanded:
love12-duty.md&1.15&>     - The love that delights is based on appraisal of qualities that cause us to be delighted. In the absence of such qualities, there is no way to command that sentiment to come into existence.
love12-duty.md&1.15&>     - The love that wishes well is dependent on the love that delights. It is well-wishing towards someone who delights us. Therefore we cannot love at will, but only if we have an urge to love.
love12-duty.md&1.15&
love12-duty.md&1.15&
love12-duty.md&1.16&## Beneficence can be commanded (1)
love12-duty.md&1.16&
love12-duty.md&1.16&Kant, The Metaphysics of Morals (transl. Mary J. Gregor):
love12-duty.md&1.16&
love12-duty.md&1.16&> “Beneficence is a duty. If someone practices it often and succeeds in realizing his beneficent intention, he eventually comes actually to love the person he has helped. So the saying ‘you ought to love your neighbor as yourself’ does not mean that you ought immediately (first) to love him and (afterwards) by means of this love do good to him. It means, rather, do good to your fellow human beings, and your beneficence will produce love of them in you (as an aptitude of the inclination to beneficence in general).”
love12-duty.md&1.16&
love12-duty.md&1.16&
love12-duty.md&1.17&## Beneficence can be commanded
love12-duty.md&1.17&
love12-duty.md&1.17&> “Love as an inclination cannot be commanded; but beneficence from duty, when no inclination impels us and even when a natural and unconquerable aversion opposes such beneficence, is practical, and not pathological, love. Such love resides in the will and not in the propensities of feeling, in principles of action and not in tender sympathy; and only this practical love can be commanded.” (Kant, Grounding of the Metaphysics of Morals, transl. James Ellington)
love12-duty.md&1.17&
love12-duty.md&1.17&
love12-duty.md&1.18&## Problems with Kant’s concept
love12-duty.md&1.18&
love12-duty.md&1.18&>- Remember that, according to Kant, we have to treat all humans as ends and not as means only. Human beings have an unlimited worth (dignity) and not merely a price (financial value).
love12-duty.md&1.18&>- But if the core of love is the appreciation of someone’s value as a person, and since we all have value as persons, we all ought to love each other. (Driver p.2)
love12-duty.md&1.18&
love12-duty.md&1.18&
love12-duty.md&1.19&## The un-loving love of Kant
love12-duty.md&1.19&
love12-duty.md&1.19&>- Do you see any problem with Kant’s account of love?
love12-duty.md&1.19&>- The problem is that “practical love” (beneficence from duty) does not account for the special relationships that we call love.
love12-duty.md&1.19&>- If I have to treat everyone equally with respect and as a being of unlimited value, then what is special in the love to my wife, as opposed to my attitude towards any random person on the street?
love12-duty.md&1.19&>- Obviously, Kant misses the point of romantic love here (or even of love in general).
love12-duty.md&1.19&
love12-duty.md&1.19&
love12-duty.md&2.0&# The no-reasons view
love12-duty.md&2.0&
love12-duty.md&2.1&## No reasons for love
love12-duty.md&2.1&
love12-duty.md&2.1&>- Some authors have proposed that we can never justify love with reasons (Frankfurt, Smuts). We will talk about this thesis in a later session.
love12-duty.md&2.1&>- A similar view is “bestowal” love. We bestow love on someone not because of any appraisal of his or her qualities. Why then? We cannot provide a reason (without returning to an appraisal view).
love12-duty.md&2.1&>- The no-reasons view is different from the bestowal view in that the bestowal view would see the bestowal itself as a reason to love the other person after that love has been bestowed.
love12-duty.md&2.1&>- The no-reasons view would never assume that there are *any* rational and justifiable reasons for love.
love12-duty.md&2.1&
love12-duty.md&2.1&
love12-duty.md&2.2&## No-reasons and hate
love12-duty.md&2.2&
love12-duty.md&2.2&>- A problem with the no-reasons view is the asymmetry between love and hate.
love12-duty.md&2.2&>- Since we have no reasons to love, what about reasons to hate?
love12-duty.md&2.2&>- It seems that we generally believe that there are good reasons to hate people, or that we can distinguish between justifiable hate and irrational, not justifiable hate.
love12-duty.md&2.2&>- So why is it that love does not have reasons but hate has?
love12-duty.md&2.2&>- Driver (p.5): “Advocates of the No Reasons View can simply hold that hate is just unlike love in this respect -- that one needs reasons to justify hatred, even though one doesn’t need them ... in the case of love. But a view that preserves a connection in terms of justification is to be preferred...”
love12-duty.md&2.2&
love12-duty.md&2.2&
love12-duty.md&2.3&## The Relationship view
love12-duty.md&2.3&
love12-duty.md&2.3&>- The relationship view, love is a kind of relationship between two people that involves a *central commitment.*
love12-duty.md&2.3&>- The relationship *creates* particular demands that the two people have to satisfy.
love12-duty.md&2.3&>- Reasons for love can be found in the relationship itself.
love12-duty.md&2.3&>- The relationship view does not make it clear why we *fall* in love, but it does explain why we *stay* in love. (Driver p.6)
love12-duty.md&2.3&>- What does this view remind you of?
love12-duty.md&2.3&>- Thomas Aquinas: “Office” of love. An “office” for Aquinas is a defining relationship that prescribes particular kinds of behaviours, including love.
love12-duty.md&2.3&
love12-duty.md&2.3&
love12-duty.md&3.0&# Loving Harry Lime
love12-duty.md&3.0&
love12-duty.md&3.1&## The trouble with Harry
love12-duty.md&3.1&
love12-duty.md&3.1&>- In Graham Greene’s novel “The Third Man,” the main character is Harry Lime.
love12-duty.md&3.1&>- He is someone who, during a war, was selling diluted black-market antibiotics that caused the deaths of many children.
love12-duty.md&3.1&>- His lover, Anna, did not know this when she fell in love with him.
love12-duty.md&3.1&>- How should she react now that she suddenly learns that the man she loves is, in reality, an immoral criminal?
love12-duty.md&3.1&>- More generally:
love12-duty.md&3.1&>     - Is it permissible to fall in love with Harry Lime?
love12-duty.md&3.1&>     - Is it permissible to stay in love with Harry Lime after one knows the truth?
love12-duty.md&3.1&>     - Is anything wrong about loving Harry Lime, and what exactly?
love12-duty.md&3.1&
love12-duty.md&3.1&
love12-duty.md&3.2&## What should Anna do?
love12-duty.md&3.2&
love12-duty.md&3.2&>- What should Anna do?
love12-duty.md&3.2&>- Options: 
love12-duty.md&3.2&>     - It is right to love Harry Lime and to act upon that love.
love12-duty.md&3.2&>     - It is okay to (privately) love Harry Lime, but not to act upon that love (that is, it is not okay to help him escape or to defend him).
love12-duty.md&3.2&>     - It is not okay to even privately love him. Harry Lime must be despised, not loved.
love12-duty.md&3.2&>- What if Harry Lime was one’s father or son? Would this change the situation? Do we have to love our family even if they are acting immorally?
love12-duty.md&3.2&
love12-duty.md&3.3&## Confucius on family ties
love12-duty.md&3.3&
love12-duty.md&3.3&>- What would Confucius say?
love12-duty.md&3.3&>- “One has stronger moral obligations toward, and should have stronger emotional attachment to, those who are bound to oneself by community, friendship, and especially kinship.” (van Norden)
love12-duty.md&3.3&>- Analects 13.18: “The Governor of She in conversation with Confucius said, ‘In our village there is someone called [Upright] Person.  When his father stole a sheep, he reported him to the authorities.’  Confucius replied:  ‘Those who are [upright] in my village conduct themselves differently.  A father covers for his son, and a son covers for his father. [Uprightness consists of this].’”
love12-duty.md&3.3&>- So, Confucius would say that fathers should help their sons cover up crimes. But does this also apply to lovers? To friends? To acquaintances? Does it cover Anna?
love12-duty.md&3.3&
love12-duty.md&3.3&
love12-duty.md&3.4&## Does Anna have a duty to not love Harry?
love12-duty.md&3.4&
love12-duty.md&3.4&>- Can one have a duty one is unable to fulfil?
love12-duty.md&3.4&>- Can Anna have a duty to not love Harry any more if she cannot control her love for him?
love12-duty.md&3.4&>- Possible answer: If I say that Anna has a duty to not love Harry, then I must make sure that she’s able to fulfil that duty. I must make sure that she *can* fall out of love with him.
love12-duty.md&3.4&>     - An appraisal theory would allow for that. Anna appraises the (newly discovered) quality of Harry to be a murderer and decides that this terminates her love for him.
love12-duty.md&3.4&>     - But what if Anna still has feelings for Harry? Is it possible to manipulate one’s feelings based on one’s reason? Can Anna convince herself not to love Harry?
love12-duty.md&3.4&
love12-duty.md&3.5&## Does Anna have a duty to not love Harry?
love12-duty.md&3.5&
love12-duty.md&3.5&>- Another possible answer: “Even though [Anna] is not blameworthy for loving Lime, she still has a *duty* to not love him.” (Driver, p.7)
love12-duty.md&3.5&>- A third possible answer: “The duty to not love does not involve a duty to eradicate one’s feelings for someone, but will involve a duty to *try* to eradicate those feelings, and subvert any dispositions that come along with those feelings -- dispositions to seek the well-being of the loved one when that conflicts with justice, and dispositions to make excuses, and ignore the evidence.” (Driver, emphasis added)
love12-duty.md&3.5&
love12-duty.md&3.5&
love12-duty.md&3.6&## Can we block our own emotions?
love12-duty.md&3.6&
love12-duty.md&3.6&>- Can we manipulate and block our own emotions?
love12-duty.md&3.6&>- Examples:
love12-duty.md&3.6&>     - A boxer before a fight?
love12-duty.md&3.6&>     - Someone who wants to feel more religious might go to church in order to achieve that.
love12-duty.md&3.6&>     - Animal rights campaigners might show us pictures of animal suffering in order to convince us to not eat meat. What if we show these pictures to ourselves? Would this work to convince us to not eat meat?
love12-duty.md&3.6&>     - Smokers who want to stop smoking might watch movies about the health effects of smoking in order to manipulate their own emotions towards smoking.
love12-duty.md&3.6&
love12-duty.md&3.6&
love12-duty.md&3.7&## The asymmetry of a duty to love
love12-duty.md&3.7&
love12-duty.md&3.7&Driver:
love12-duty.md&3.7&
love12-duty.md&3.7&> “The thesis of this paper is that there is an important asymmetry between a duty to love and a duty to not love: there is no duty to love as a fitting response to someone’s very good qualities, but there is a duty to not love as a fitting response to someone’s very bad qualities.” (p.1)
love12-duty.md&3.7&
love12-duty.md&3.8&## The reason for the asymmetry
love12-duty.md&3.8&
love12-duty.md&3.8&Two parts of love:
love12-duty.md&3.8&
love12-duty.md&3.8&1. the emotional part; and 
love12-duty.md&3.8&2. the evaluative commitment part.
love12-duty.md&3.8&
love12-duty.md&3.8&. . . 
love12-duty.md&3.8&
love12-duty.md&3.8&“One cannot directly, or ‘at will,’ control an emotional response, but one can undermine any commitment one would normally have under the circumstances.” (Driver p.2)
love12-duty.md&3.8&
love12-duty.md&3.8&
love12-duty.md&4.0&# Can love be commanded?
love12-duty.md&4.0&
love12-duty.md&4.0&
love12-duty.md&4.1&## Commandability
love12-duty.md&4.1&
love12-duty.md&4.1&>- The main problem with a “duty to love” is that one must be able to fulfil one’s duties, at least in principle.
love12-duty.md&4.1&>- “Ought implies can.”
love12-duty.md&4.1&>- We would perhaps agree that a hermit, who finds a child in a forest, ought to love that child in order to give it a chance to develop normally; although the hermit has not chosen to have this child. Still, it seems that we can have a duty to love even when we have not willingly committed to doing so. (Liao, 16)
love12-duty.md&4.1&
love12-duty.md&4.1&
love12-duty.md&4.2&## The attitudes of love
love12-duty.md&4.2&
love12-duty.md&4.2&>- One could argue that love is misunderstood. It is not an emotion, but a collection of *attitudes.*
love12-duty.md&4.2&>- Attitudes are things like:
love12-duty.md&4.2&>     - Valuing the person we love for the person’s sake.
love12-duty.md&4.2&>     - Wanting to promote another person’s well-being for the person’s sake.
love12-duty.md&4.2&>     - Wanting to be with the person.
love12-duty.md&4.2&>- Do you see how this solves the problem?
love12-duty.md&4.2&>- Although we cannot influence our emotions, we *can* control our attitudes.
love12-duty.md&4.2&>- Therefore, we *can* control our love; and therefore, we are responsible for it (and can fulfil it as a duty). (Liao)
love12-duty.md&4.2&
love12-duty.md&4.3&## Is love more than attitudes?
love12-duty.md&4.3&
love12-duty.md&4.3&>- But love does not seem to be only attitudes.
love12-duty.md&4.3&>- We can have strong attitudes to help others or be with them (for example, our friends or younger colleagues) without being in love with them in a romantic sense.
love12-duty.md&4.3&>- “Indeed, a friendly colleague may have all the attitudes typically associated with love such as valuing the other person and wanting to promote the other person’s well-being for the other person’s sake. But it does not follow that the colleague loves the other person.” (Liao)
love12-duty.md&4.3&
love12-duty.md&5.0&# Can we control our emotions?
love12-duty.md&5.0&
love12-duty.md&5.1&## Internal control and reasons for emotions
love12-duty.md&5.1&
love12-duty.md&5.1&>- How can we control our emotions?
love12-duty.md&5.1&>- Internal and external control.
love12-duty.md&5.1&>- Ways of internal control:
love12-duty.md&5.1&
love12-duty.md&5.1&. . . 
love12-duty.md&5.1&
love12-duty.md&5.1&>1. Creating and visualising reasons
love12-duty.md&5.1&>2. Reflecting on reasons
love12-duty.md&5.1&>3. Cultivating emotions (like Aristotle’s habits)
love12-duty.md&5.1&
love12-duty.md&5.2&## 1. Creating and visualising reasons
love12-duty.md&5.2&
love12-duty.md&5.2&>- “For example, let us suppose that we are not in a good mood on a particular day and on the particular day, we by chance are also invited to attend a close friend’s wedding.”
love12-duty.md&5.2&>- “Knowing that our close friend would not want us to be in a bad mood and would instead want us to be joyful on her wedding day, we might tell ourselves that because it is a special occasion, we should not be in a bad mood and that we should instead be joyful.”
love12-duty.md&5.2&>- “In giving ourselves a reason to be joyful and not to be in a bad mood, there is a good chance that we would not be in a bad mood, and would instead be joyful.” (Liao 5)
love12-duty.md&5.2&
love12-duty.md&5.2&
love12-duty.md&5.3&## 2. Reflecting on reasons
love12-duty.md&5.3&
love12-duty.md&5.3&>- “Another method of internal control calls on us to reflect on the reasons why we tend to experience particular emotions in particular circumstances or toward particular persons.”
love12-duty.md&5.3&>- “Through reflecting on these reasons, we might then decide to continue to have particular emotions, if the emotions are supported by good reasons, or to discontinue to have particular emotions, if the emotions are not supported by good reasons.”
love12-duty.md&5.3&
love12-duty.md&5.3&
love12-duty.md&5.4&## 3. Cultivating emotions
love12-duty.md&5.4&
love12-duty.md&5.4&>- “We can also cultivate our emotional capacities such that we would be more likely to have particular emotions in appropriate circumstances.”
love12-duty.md&5.4&>- “As Aristotle says, cultivation involves habituation as well as reflection.”
love12-duty.md&5.4&>- “One strategy for cultivating certain emotions is to behave as if we have particular emotions. After some time and effort, it is likely that we would cultivate the capacities for these emotions.”
love12-duty.md&5.4&>- “For example, if we wish to cultivate our capacity for joy, we might begin by behaving as if we are joyful. We might smile, whistle, sing ... Through engaging in these forms of behavior repeatedly over time, it is likely that we would cultivate the capacity for joy.”
love12-duty.md&5.4&>- “Indeed,  observes that through enacting the behavior associated with religious rituals, we seem to increase our capacity for religious feelings.” (Liao 7)
love12-duty.md&5.4&
love12-duty.md&5.5&## External control
love12-duty.md&5.5&
love12-duty.md&5.5&>- External control uses the outside world as a support in manipulating our emotions.
love12-duty.md&5.5&>- “Repeatedly placing ourselves in situations in which we know that we would probably experience particular emotions.”
love12-duty.md&5.5&>- “Using our previous examples, let us suppose that we know that attending church services and visiting homeless shelters often elicit emotions of piety and compassion in us. To cultivate our capacities for such emotions, we might repeatedly visit such places. In doing so, there is a good chance that we would cultivate these emotional capacities.”
love12-duty.md&5.5&
love12-duty.md&5.6&## Internal and external control
love12-duty.md&5.6&
love12-duty.md&5.6&>- What do you think of these methods of emotion control? 
love12-duty.md&5.6&>- Are they likely to be effective?
love12-duty.md&5.6&>- Are they likely to help Anna deal with her love of Harry Lime?
love12-duty.md&5.6&
love12-duty.md&5.7&## The difficulty of cultivating emotions
love12-duty.md&5.7&
love12-duty.md&5.7&>- One might argue that cultivating emotions in these ways is very difficult.
love12-duty.md&5.7&>- Is this a good argument for avoiding to cultivate the emotions and for accepting that we cannot have a duty to love?
love12-duty.md&5.7&>- “This suggests that to cultivate our emotional capacities, we may be required to evaluate critically some of our fundamental values, and to alter some of our ingrained character traits such as becoming more reflective than superficial, or becoming more altruistic than self-interested.”
love12-duty.md&5.7&>- “Such cultivation can be difficult. However, the cultivation of physical abilities such as learning how to play the piano or to sail a boat is often equally difficult. Yet people acquire these capacities nevertheless.”
love12-duty.md&5.7&>- Therefore, it is a too strong conclusion to say that love is *never* commandable.
love12-duty.md&5.7&
love12-duty.md&5.8&## Is romantic love special? (1)
love12-duty.md&5.8&
love12-duty.md&5.8&>- But even if we agree that some kinds of love might be commandable (for example, parental love), we might disagree that romantic love is.
love12-duty.md&5.8&>- This might be because romantic love involves passion, which is not under the control of our reason.
love12-duty.md&5.8&
love12-duty.md&5.9&## Is romantic love special? (2)
love12-duty.md&5.9&
love12-duty.md&5.9&>- “The idea that romantic love is not commandable may be a relic of the German Romanticism of the eighteenth and nineteen centuries, according to which there is a sharp boundary between reasons and emotions.” (Liao)
love12-duty.md&5.9&>- “It is therefore possible that we have inherited this possible bias from German romanticism, and that we have continued to perpetuate this idea in our media, literature, and music.”
love12-duty.md&5.9&>- “Indeed, there are alternative views of emotions, according to which there is not such a sharp boundary between reasons and emotions.”
love12-duty.md&5.9&>- An Aristotelian approach would not emphasise passion and uncontrollable emotions so much, but would be more accessible to reason. (Liao, 20)
love12-duty.md&5.9&
love12-duty.md&5.10&## Romantic love is also rational
love12-duty.md&5.10&
love12-duty.md&5.10&>- Is romantic love really completely irrational?
love12-duty.md&5.10&>- An argument that even romantic and passionate love is rational to some extent can help reduce the impression that romantic love is not controllable by reason at all.
love12-duty.md&5.10&>- We don’t love randomly. We have particular expectations of our beloved.
love12-duty.md&5.10&>- “For example, our expectations might have been built up from our childhood from when our parents and teachers read us stories about brave knights and beautiful princesses. They might have further been reinforced through our peers, society, and the media.”
love12-duty.md&5.10&
love12-duty.md&5.11&## Romantic love is also rational
love12-duty.md&5.11&
love12-duty.md&5.11&>- “Given this, when we meet a certain individual, we already have a fairly extensive checklist for deciding whether that individual is the right person for us. It is not surprising that we fall in love when someone seems to fit all the criteria, until we find out later that the person is not as perfect as we thought.”
love12-duty.md&5.11&>- “We fail to fall in love when someone lacks certain important criteria on our list, even though that person may seem to others perfect for us in all respects. ...”
love12-duty.md&5.11&>- “Finally, we fall out of love either when we realize that the other person does not fit our expectations or our expectations have changed as a result of certain life events such as midlife crisis.” (Liao 22)
love12-duty.md&5.11&
love12-duty.md&5.12&## Romantic love is also rational
love12-duty.md&5.12&
love12-duty.md&5.12&>- If romantic love is rational, then it’s also commandable.
love12-duty.md&5.12&>- “But, if we do have these expectations, then romantic love should be commandable, provided that there are good reasons to believe that we should revise our expectations.” (Liao 22)
love12-duty.md&5.12&>- As a conclusion, love, including romantic love, seems to be sufficiently controllable by rationality as to be commandable. 
love12-duty.md&5.12&>- Therefore, there *can* rationally be a duty to love (or not to love) in particular circumstances.
love12-duty.md&5.12&
love12-duty.md&5.12&
love12-duty.md&5.13&## The End. Thank you for your attention!
love12-duty.md&5.13&
love12-duty.md&5.13&Questions?
love12-duty.md&5.13&
love12-duty.md&5.13&<!--
love12-duty.md&5.13&
love12-duty.md&5.13&%% Local Variables: ***
love12-duty.md&5.13&%% mode: markdown ***
love12-duty.md&5.13&%% mode: visual-line ***
love12-duty.md&5.13&%% mode: cua ***
love12-duty.md&5.13&%% mode: flyspell ***
love12-duty.md&5.13&%% End: ***
love12-duty.md&5.13&
love12-duty.md&5.13&-->
love14-concepts.md&0.1&---
love14-concepts.md&0.1&title:  "Love and Sexuality: 14. Concepts and features of love"
love14-concepts.md&0.1&author: Andreas Matthias, Lingnan University
love14-concepts.md&0.1&date: October 21, 2019
love14-concepts.md&0.1&...
love14-concepts.md&0.1&
love14-concepts.md&0.1&
love14-concepts.md&1.0&# Features of love
love14-concepts.md&1.0&
love14-concepts.md&1.1&## Sources
love14-concepts.md&1.1&
love14-concepts.md&1.1&>- Helm, B. W. (2010): Love, friendship, and the self: Intimacy, identification, and the social nature of persons. Oxford University Press.
love14-concepts.md&1.1&>- An older version of the Stanford Encyclopedia of Philosophy article: “Love.”
love14-concepts.md&1.1&>- Alan Soble (1990): The Structure of Love, Yale University Press.
love14-concepts.md&1.1&
love14-concepts.md&1.1&
love14-concepts.md&1.2&## Characteristics of love
love14-concepts.md&1.2&
love14-concepts.md&1.2&>- *How is loving different from liking?* I can love my girlfriend, but I can like a chocolate cookie. Are these relationships different and in what way?
love14-concepts.md&1.2&>- Love generally is assumed to be distinguished from other relationships or emotions by having particular characteristics:
love14-concepts.md&1.2&>     - Exclusivity
love14-concepts.md&1.2&>     - Constancy
love14-concepts.md&1.2&>     - Reciprocity
love14-concepts.md&1.2&>     - Uniqueness
love14-concepts.md&1.2&>     - Irrepleaceability of the beloved^[Soble (1990): The Structure of Love.]
love14-concepts.md&1.2&>- Try to apply these to the cookie example to see how it works out.
love14-concepts.md&1.2&
love14-concepts.md&1.3&## Loving and liking
love14-concepts.md&1.3&
love14-concepts.md&1.3&>- How is loving different from liking?
love14-concepts.md&1.3&>- Other possible answers:
love14-concepts.md&1.3&>     - Love acknowledges that the other person has an intrinsic value, a value as a person. 
love14-concepts.md&1.3&>     - Accordingly, we can “wish her well for her own sake” as Aristotle and Aquinas would say.
love14-concepts.md&1.3&>     - Liking does not seem to require that. 
love14-concepts.md&1.3&>     - I can like a chocolate cookie by seeing the cookie as only instrumental to my own happiness, not as something that has a value in itself.
love14-concepts.md&1.3&
love14-concepts.md&1.3&
love14-concepts.md&1.4&## Is liking instrumental?
love14-concepts.md&1.4&
love14-concepts.md&1.4&>- Singer: Liking is a matter of desiring something for *my* own good (not for the desired thing’s or person’s own good).
love14-concepts.md&1.4&>- Is this convincing?
love14-concepts.md&1.4&>     - It seems that there might be relationships in between the two extremes. 
love14-concepts.md&1.4&>     - I might like a colleague at work for their intrinsic value and wish for their own good without actually *loving* them.
love14-concepts.md&1.4&>     - The same is true for agape love towards a beggar on the street. I might wish to do good to them for their own sake, but we wouldn’t call that proper, personal love.
love14-concepts.md&1.4&
love14-concepts.md&2.0&# Love as union
love14-concepts.md&2.0&
love14-concepts.md&2.1&## Love as union
love14-concepts.md&2.1&
love14-concepts.md&2.1&>- Another attempt would be to see love as a *union* between two people.
love14-concepts.md&2.1&>- Liking, in contrast, does not establish a union.
love14-concepts.md&2.1&>- *But what makes a union a union?*
love14-concepts.md&2.1&>- Some kind of serious commitment, as a result of which the interests of the lover fuse with the interests of the beloved.
love14-concepts.md&2.1&>- A union of two people is created, in which both partners share common interests as a union, rather than only adding up their individual interests.
love14-concepts.md&2.1&
love14-concepts.md&2.2&## Love as a union of interests
love14-concepts.md&2.2&
love14-concepts.md&2.2&>- The “union of interests” can take different forms:
love14-concepts.md&2.2&>- Scruton (1986): Love exists “just so soon as reciprocity becomes community: that is, just so soon as all distinction between my interests and your interests is overcome.”^[Scruton, R., 1986, Sexual Desire: A Moral Philosophy of the Erotic, Free Press.] (p.230)
love14-concepts.md&2.2&>- Solomon (1988)^[1988, About Love: Reinventing Romance for Our Times, Simon & Schuster.]: “Love is the concentration and the intensive focus of mutual definition on a single individual, subjecting virtually every personal aspect of one's self to this process.” (p.197)
love14-concepts.md&2.2&
love14-concepts.md&2.3&## Nozick on love as a union (1)
love14-concepts.md&2.3&
love14-concepts.md&2.3&>- Nozick (1989)^[Nozick, R., 1989, “Love's Bond”, in The Examined Life: Philosophical Meditations, Simon \& Schuster, 68–86.]:
love14-concepts.md&2.3&>     - What is necessary for love is not so much the actual union, but the *desire* to form a “we,” together with the desire that your beloved reciprocates.
love14-concepts.md&2.3&>- That new “we” creates a new web of relationships between the lovers which makes them be no longer separate individuals.
love14-concepts.md&2.3&>- Lovers “pool” their well-beings together: The well-being of each is tied up with that of the other.
love14-concepts.md&2.3&>- They also pool together their autonomy: “Each transfers some previous rights to make certain decisions unilaterally into a joint pool” (p.71).
love14-concepts.md&2.3&
love14-concepts.md&2.4&## Nozick on love as a union (2)
love14-concepts.md&2.4&
love14-concepts.md&2.4&Nozick: The lovers:
love14-concepts.md&2.4&
love14-concepts.md&2.4&>1. want to be perceived publicly as a couple;
love14-concepts.md&2.4&>2. attend to their pooled well-being; and
love14-concepts.md&2.4&>3. accept a “certain kind of division of labor” in their relationship. For example, one might find something interesting to read, but leave it for the other person to read instead.
love14-concepts.md&2.4&
love14-concepts.md&2.4&. . . 
love14-concepts.md&2.4&
love14-concepts.md&2.4&In this sense, the lovers form a new being, a “we,” that is separate from their two individual beings.
love14-concepts.md&2.4&
love14-concepts.md&2.4&
love14-concepts.md&2.5&## Love as union: Criticism (1)
love14-concepts.md&2.5&
love14-concepts.md&2.5&>- Is this a good description of love? Criticism?
love14-concepts.md&2.5&>     - It would explain why charity towards a beggar is not proper love.
love14-concepts.md&2.5&>     - It would explain why liking a cookie is not love.
love14-concepts.md&2.5&>     - But what about arranged (or medieval) marriages? They create unions but they don’t necessarily require (or encourage) love.
love14-concepts.md&2.5&>     - Courtly love happens largely outside of the sphere of common interests, as does unfulfilled romantic love.
love14-concepts.md&2.5&>     - Other kinds of relationships also create common interest unions (sometimes only for a short time) without being romantic love.
love14-concepts.md&2.5&>     - For example, work relationships, boss and employee, teacher and student, taxi driver and passenger, kidnapper and kidnapped.
love14-concepts.md&2.5&
love14-concepts.md&2.6&## Love as union: Criticism (2)
love14-concepts.md&2.6&
love14-concepts.md&2.6&>- One could criticise the union theory as taking away the autonomy of the lovers.
love14-concepts.md&2.6&>- Generally, we tend to see autonomy as something good and morally valuable.
love14-concepts.md&2.6&>- For example, Kant sees moral autonomy as the basis of all moral behaviour and human value (“dignity”).
love14-concepts.md&2.6&>- If we give our autonomy away in love, does this make love bad? Do we become worse human beings when we are in love, because we are less autonomous?
love14-concepts.md&2.6&>- Singer: a necessary part of loving is respect for your beloved as the particular person he or she is, and this requires respecting their autonomy.
love14-concepts.md&2.6&>- Taking the lover’s autonomy away would therefore diminish love itself.
love14-concepts.md&2.6&
love14-concepts.md&2.7&## Love as union: Criticism (3)
love14-concepts.md&2.7&
love14-concepts.md&2.7&>- Finally, love as union seems to exclude love where union is not possible due to the circumstances (medieval courtly love, loving the dead and so on).
love14-concepts.md&2.7&>- What could a “love as union” theorist answer to these criticisms?
love14-concepts.md&2.7&>     - This loss of autonomy is actually desirable and it is what makes love special.
love14-concepts.md&2.7&>     - Or we could say that it is a loss, but an acceptable loss, considering the benefits from such a union.
love14-concepts.md&2.7&>     - Solomon calls the loss of autonomy in love “the paradox of love.”
love14-concepts.md&2.7&
love14-concepts.md&2.8&## Love as union: Criticism (4)
love14-concepts.md&2.8&
love14-concepts.md&2.8&>- Aristotle and Aquinas both think that love is defined as a wish to benefit the other person, to act so that one benefits them for their own sake.
love14-concepts.md&2.8&>- Do you see how this creates a problem for the union view?
love14-concepts.md&2.8&>- If, in the union view, the two lovers unite and their interests merge, then how can one benefit the other?
love14-concepts.md&2.8&>- Benefiting the other would always be selfish: one would be benefiting (in part) oneself.
love14-concepts.md&2.8&
love14-concepts.md&2.9&## Love as union: Criticism (5)
love14-concepts.md&2.9&
love14-concepts.md&2.9&>- What do you think of this? What could the “love as union” supporter answer?
love14-concepts.md&2.9&>     - This is actually a good feature of the theory.
love14-concepts.md&2.9&>     - The “love as union” theory explains how individuals (who are essentially egoistic) can overcome their egoism and act so that they benefit others (because they are, at the same time, also benefiting themselves).
love14-concepts.md&2.9&>     - What does this remind you of? Who other philosopher had a similar concept of the interdependence of human interests?
love14-concepts.md&2.9&>     - This is similar to Aristotle’s idea that the sophron individual benefits others and himself at the same time. 
love14-concepts.md&2.9&>     - Virtues are properties of character that benefit oneself and others.
love14-concepts.md&2.9&
love14-concepts.md&2.9&
love14-concepts.md&2.10&## Love as federation of selves
love14-concepts.md&2.10&
love14-concepts.md&2.10&>- In order to avoid the problems of a union view, Friedman (1998)^[Friedman (1998): “Romantic Love and Personal Autonomy”, Midwest Studies in Philosophy, 22: 162–81.] proposed a “federation” model for love:
love14-concepts.md&2.10&>- “On the federation model, a third unified entity is constituted by the interaction of the lovers, one which involves the lovers acting in concert across a range of conditions and for a range of purposes. This concerted action, however, does not erase the existence of the two lovers as separable and separate agents with continuing possibilities for the exercise of their own respective agencies.” (p.165)
love14-concepts.md&2.10&>- This would allow for altruism in love. Since the lovers are only “acting” together, but not really merging into one, they can still benefit each other and care for each other’s interests.
love14-concepts.md&2.10&
love14-concepts.md&2.10&
love14-concepts.md&3.0&# Love as life choice
love14-concepts.md&3.0&
love14-concepts.md&3.1&## Love as a life choice (1)
love14-concepts.md&3.1&
love14-concepts.md&3.1&>- The union view also emphasises that who we are is, in part, defined by whom we love.
love14-concepts.md&3.1&>- Nussbaum (1990)^[Nussbaum, M., 1990, “Love and the Individual: Romantic Rightness and Platonic Aspiration”, in Love's Knowledge: Essays on Philosophy and Literature, Oxford: Oxford University Press, 314–34.]: “The choice between one potential love and another can feel, and be, like a choice of a way of life, a decision to dedicate oneself to these values rather than these.”
love14-concepts.md&3.1&>- Liking is not so exclusive. I can like chocolate cookies, but I can also like a cheese cake. I don’t need to “dedicate myself” to either.
love14-concepts.md&3.1&
love14-concepts.md&3.2&## Love as a life choice (2)
love14-concepts.md&3.2&
love14-concepts.md&3.2&>- Criticism?
love14-concepts.md&3.2&>- This might be a good *additional* criterion, but it’s not enough to alone define love.
love14-concepts.md&3.2&>- For example, the choice of a job requires identification of this kind. By choosing a job, I am dedicating my life to one kind of activity and values. But this is not love.
love14-concepts.md&3.2&>- People who like a football club, pop group, health or political cause, are sometimes changed by that liking, in that “being a fan/follower/promoter of X” becomes an important part of their self-description. Still, we wouldn’t call such associations “love.”
love14-concepts.md&3.2&
love14-concepts.md&3.2&
love14-concepts.md&4.0&# Love as robust concern
love14-concepts.md&4.0&
love14-concepts.md&4.1&## Love as robust concern
love14-concepts.md&4.1&
love14-concepts.md&4.1&>- A second view would put the *concern for the beloved* into the centre of a definition of love.
love14-concepts.md&4.1&>- What makes a relationship “love” is a “robust concern” for the other person. (Robust: strong and enduring over time.)
love14-concepts.md&4.1&>- Frankfurt: “That a person cares about or that he loves something has less to do with how things make him feel, or with his opinions about them, than with the more or less *stable motivational structures* that shape his preferences and that guide and limit his conduct.”
love14-concepts.md&4.1&>- What counts are the “stable motivational structures,” that is, the lover’s enduring concern for the welfare of the beloved.
love14-concepts.md&4.1&
love14-concepts.md&4.2&## Criticism of the “robust concern” theory (1)
love14-concepts.md&4.2&
love14-concepts.md&4.2&>- How can we criticise this view of love as robust concern?
love14-concepts.md&4.2&>- One problem is that, in this view, love could be one-sided and missing the interaction between the lovers.
love14-concepts.md&4.2&>- I can have a concern for another person, although they don’t love me back (or are not concerned about me). Is this then “proper” love? Wouldn’t it be better if the concern was reciprocal?
love14-concepts.md&4.2&>- What if my concern makes me act in a way that *I* think will benefit my beloved, although the beloved does not want me to act in this way?
love14-concepts.md&4.2&>     - For example, I see my beloved smoke. 
love14-concepts.md&4.2&>     - Robust concern for their health might lead me to take away their cigarettes.
love14-concepts.md&4.2&>     - Is my action an expression of love? Or would love require me to respect my beloved’s autonomy instead?
love14-concepts.md&4.2&
love14-concepts.md&4.3&## Criticism of the “robust concern” theory (2)
love14-concepts.md&4.3&
love14-concepts.md&4.3&>- Can there be love without a robust concern?
love14-concepts.md&4.3&>- For example, can I “love” a troublesome relative of mine whom I don’t actually want to meet or benefit?
love14-concepts.md&4.3&>- Can we love the dead? (Badhwar 2003)^[“Love”, in H. LaFollette (ed.), Practical Ethics, Oxford: Oxford University Press, 42–69.] If love is robust concern, and the dead are beyond any concern (because it is impossible to benefit or harm them), how can I say that I love a dead person?
love14-concepts.md&4.3&
love14-concepts.md&4.4&## Criticism of the “robust concern” theory (3)
love14-concepts.md&4.4&
love14-concepts.md&4.4&>- Conversely, can I be concerned about the well-being of people or things without being in love with them?
love14-concepts.md&4.4&>- Doctors, psychologists, politicians, teachers might be genuinely concerned about the welfare of others without being in love with them.
love14-concepts.md&4.4&>- A dog keeper might be robustly concerned about the welfare of their dog. Still, this is not “romantic love”.
love14-concepts.md&4.4&>- It seems like “robust concern” is not quite enough to distinguish romantic love from weaker forms of benefiting, caring and liking.
love14-concepts.md&4.4&
love14-concepts.md&5.0&# Kant, dignity, and love as valuing
love14-concepts.md&5.0&
love14-concepts.md&5.1&## Love as valuing
love14-concepts.md&5.1&
love14-concepts.md&5.1&>- We already talked about the two different forms of valuing something: 
love14-concepts.md&5.1&>     - Valuing its qualities (appraisal); or 
love14-concepts.md&5.1&>     - Giving value to something as the result of a decision that is not dependent on the properties of the valued thing (or person) (bestowal).
love14-concepts.md&5.1&>- In the bestowal view, the beloved *comes to be valuable* to the lover as a result of the lover loving the beloved.
love14-concepts.md&5.1&>- For the appraisal theory, it is not enough to appraise qualities *as part* of a love relationship. Even bestowal probably involves some initial appraisal.
love14-concepts.md&5.1&>- The point of an appraisal theory must be that *love consists entirely in that appraisal.*
love14-concepts.md&5.1&
love14-concepts.md&5.2&## Kant, dignity and love
love14-concepts.md&5.2&
love14-concepts.md&5.2&>- Remember Kant. What is the relation between dignity and price?
love14-concepts.md&5.2&>- Things have a value that can be compared to the values of other things.
love14-concepts.md&5.2&>- We can exchange things of similar value with each other.
love14-concepts.md&5.2&>- But human beings cannot be replaced by other humans “of similar value.” There is no such thing as a measurable, finite “value” for humans.
love14-concepts.md&5.2&>- Instead, human beings have a unique kind of absolute value that Kant calls “dignity.” Dignity comes from the realisation of the moral autonomy of human beings.
love14-concepts.md&5.2&>- As a result of this unique, infinite value, we can never treat human beings as mere means to other ends, but must treat them as “ends in themselves.”
love14-concepts.md&5.2&>- Examples?
love14-concepts.md&5.2&>- Taxi driver, waiter in restaurant. Furthering *their ability to pursue their own ends,* instead of my own.
love14-concepts.md&5.2&
love14-concepts.md&5.3&## Velleman: Love is the appreciation of the dignity of others (1)
love14-concepts.md&5.3&
love14-concepts.md&5.3&>- Love is a response to the dignity of persons. It is the dignity of the object of our love that justifies that love.
love14-concepts.md&5.3&>- As a result, I cannot treat my lover as a means to my ends only, but have to treat them as an end in themselves.
love14-concepts.md&5.3&>- Therefore, it is impossible to love a chocolate cookie, a dog, or a country. These things don’t have moral autonomy and therefore they don’t have dignity.
love14-concepts.md&5.3&>- But not all appreciation of human dignity is love.
love14-concepts.md&5.3&>- Just paying the taxi driver or restaurant waiter is not an act of love.
love14-concepts.md&5.3&>- For Velleman^[Velleman, J. D., 1999, “Love as a Moral Emotion”, Ethics, 109: 338–74.], we can have different responses to other people’s dignity.
love14-concepts.md&5.3&>- Love is *the maximal response* to others’ dignity.
love14-concepts.md&5.3&
love14-concepts.md&5.4&## Velleman: Love is the appreciation of the dignity of others (2)
love14-concepts.md&5.4&
love14-concepts.md&5.4&>- What makes me have this maximal response to particular people and not others? 
love14-concepts.md&5.4&>     - For Kant, all people have the same value (dignity). 
love14-concepts.md&5.4&>     - Why don’t I respond to all in the same way?
love14-concepts.md&5.4&>- Helm^[Helm, B. W. (2010). Love, friendship, and the self: Intimacy, identification, and the social nature of persons. Oxford University Press.]: “The answer ... lies in the contingent fit between the way some people behaviorally express their dignity as persons and the way I happen to respond to those expressions by becoming emotionally vulnerable to them. ... The right sort of fit makes someone ‘lovable’ by me.”
love14-concepts.md&5.4&
love14-concepts.md&5.4&
love14-concepts.md&5.5&## Velleman: Criticism
love14-concepts.md&5.5&
love14-concepts.md&5.5&>- Do you see any problems with this account?
love14-concepts.md&5.5&>- First, what exactly is meant by “the contingent fit” that causes me to respond in a particular way to others’ behavioural expressions of their dignity as persons?
love14-concepts.md&5.5&>- Why would only some people be lovable to me and others not?
love14-concepts.md&5.5&>- If the reason are particular attributes of the beloved, then we have a common “appraisal” theory of love. Then the whole thing about Kant and dignity is not needed and does not contribute anything.
love14-concepts.md&5.5&>- If we speak with Kant of appraising human dignity, then we must extend this appraisal to all humans equally. This was a very important point to Kant: all humans (and only humans) have dignity, and all are equal in dignity. But then, why is love selective and not universal?
love14-concepts.md&5.5&
love14-concepts.md&6.0&# Criticism of ‘appraisal’ theories of love
love14-concepts.md&6.0&
love14-concepts.md&6.1&## Criticism of appraisal accounts in general (1)
love14-concepts.md&6.1&
love14-concepts.md&6.1&>- Some of the problems with Velleman’s account are problems of appraisal theories of love in general:
love14-concepts.md&6.1&>     - Appraisal does not only lead to love. We appraise things and people all the time.
love14-concepts.md&6.1&>     - I appraise a bike that I want to buy. I appraise a basketball player whom I admire. I appraise a good student’s performance in class.
love14-concepts.md&6.1&>     - Why do these appraisals not constitute love?
love14-concepts.md&6.1&>     - You might say, because they don’t have that particular strong effect on my emotions and my motivations that love has.
love14-concepts.md&6.1&>     - But this seems to be begging the question. *Why* do these other appraisals not have the same effects? If there is a difference between love and other kinds of appraisal, then love is more than appraisal, rather than identical with it.
love14-concepts.md&6.1&>     - An appraisal theory of love must explain what exactly makes appraisal-love different from appraisal-admiration, appraisal of students’ performance, appraisal of consumer goods an so on.
love14-concepts.md&6.1&>     - Common appraisal theories don’t do that.
love14-concepts.md&6.1&
love14-concepts.md&6.2&## Criticism (2): Love of types rather than individuals
love14-concepts.md&6.2&
love14-concepts.md&6.2&>- Vlastos (1981)^[Vlastos, G., 1981, “The Individual as Object of Love in Plato”, in Platonic Studies, Princeton, NJ: Princeton University Press, 3–42, 2nd edn.]: Appraisal accounts are really a love of properties rather than a love of persons. This is also true of Plato and Aristotle.
love14-concepts.md&6.2&>- By loving a person with properties (P1...Pn), I really love a *type of person* who has such properties, rather than an *individual* person.
love14-concepts.md&6.2&
love14-concepts.md&6.3&## Criticism (3): Fungibility
love14-concepts.md&6.3&
love14-concepts.md&6.3&>- A second problem is *fungibility.* 
love14-concepts.md&6.3&>- To be fungible: to be replaceable by another relevantly similar object without any loss of value.
love14-concepts.md&6.3&>- Money is fungible: I can change a twenty dollar bill into two ten dollar bills and nothing is lost.
love14-concepts.md&6.3&>- But can I do the same with people? Can I exchange my girlfriend with another with similar properties without any loss?
love14-concepts.md&6.3&
love14-concepts.md&6.4&## Criticism (4): Substituting Nancy (1)
love14-concepts.md&6.4&
love14-concepts.md&6.4&>- An extreme example of fungibility is the substitution argument^[Soble (1990): The Structure of Love, Yale University Press.].
love14-concepts.md&6.4&>- Mark Bernstein: “I have a wife, Nancy, whom I love very much. Let us suppose that I were informed that, tomorrow, my wife Nancy would no longer be part of my life, that she would leave and forever be unseen and unheard of by me. But, in her stead, a Nancy\* would appear, a qualitatively indistinguishable individual from Nancy. Nancy and Nancy\* would look precisely alike, act precisely alike, think precisely alike, indeed would be alike in all physical or mental details.”
love14-concepts.md&6.4&
love14-concepts.md&6.5&## Criticism (4): Substituting Nancy (2)
love14-concepts.md&6.5&
love14-concepts.md&6.5&>- How would you react? Would you love Nancy\*?
love14-concepts.md&6.5&>- The problem with this argument is the initial assumption that Nancy is entirely indistinguishable from Nancy\*!
love14-concepts.md&6.5&>- If this is true, then nobody could tell that Nancy has been replaced -- not even Nancy herself! (Soble). Nancy might have been replaced already previously and I wouldn’t have noticed.
love14-concepts.md&6.5&>- The problem with Nancy can be addressed by replacing appraisal theories with, for example, a “love as union” theory.
love14-concepts.md&6.5&
love14-concepts.md&6.6&## Fungibility again
love14-concepts.md&6.6&
love14-concepts.md&6.6&>- We could also try to argue that appraisal theories don’t necessarily lead to fungibility of lovers.
love14-concepts.md&6.6&>- Soble, Structure, Ch.3 (p.48ff): Roger Scruton (“Sexual desire”) tries to show how love can be reason-dependent, but at the same time exclusive:
love14-concepts.md&6.6&>     - Imagine, he says, that there is someone who loves Beethoven’s Violin Concerto. 
love14-concepts.md&6.6&>     - This love is reason-based: someone who enjoys the Violin Concerto must be able to answer the question why he enjoys it, by referring to its properties. 
love14-concepts.md&6.6&>     - Yet this person may enjoy no other music, or even dislike all other compositions.
love14-concepts.md&6.6&>     - If such a person is *possible* then aesthetic and love reasons are not general. They can be specific to particular things and don’t need to apply to everything that fulfils the criteria.
love14-concepts.md&6.6&>- *Do you think that this is a good argument?*
love14-concepts.md&6.6&
love14-concepts.md&6.7&## Criticising exclusive appraisal love
love14-concepts.md&6.7&
love14-concepts.md&6.7&>- Martha Nussbaum: What Scruton should have concluded from his example is exactly the opposite of his actual conclusion: that his love for music *cannot* rationally be exclusive.
love14-concepts.md&6.7&>- The music lover *must* either like any other piece of music that satisfies his criteria, or he must give some additional reason why he does not.
love14-concepts.md&6.7&>- His position is irrational.
love14-concepts.md&6.7&>- Scruton’s position would only make sense if he argued that:
love14-concepts.md&6.7&>     - His criteria for loving this particular piece of music are general; but
love14-concepts.md&6.7&>     - These criteria are only satisfied *by this particular* piece of music and no other music.
love14-concepts.md&6.7&>- *But is this likely? Are we truly unique as persons? Is our particular mix of properties unique?*
love14-concepts.md&6.7&
love14-concepts.md&6.8&## Human uniqueness (1)
love14-concepts.md&6.8&
love14-concepts.md&6.8&Are we really unique personalities?
love14-concepts.md&6.8&
love14-concepts.md&6.8&>- Soble: no. (Structure, p.52):
love14-concepts.md&6.8&>- “Of course, Jesus was non-trivially unique, and so were Gandhi, Moses [and others]. Alkibiades loved Socrates, and only Socrates, because “he is like no other human being, living or dead.” [...] But substantive uniqueness is not very common.”
love14-concepts.md&6.8&>-“Most of us draw on the same stock supply of merits and defects, good traits and bad traits, in building our personalities. ... Our mannerisms, physiognomy, ways of walking, sense of humour, and linguistic habits are close copies of the traits of our parents, siblings, peers and other models. ... Indeed, worries about the homogenization of personality and the conformity fostered by the media and schools presuppose that substantive uniqueness is rare enough to be a matter of social concern.”
love14-concepts.md&6.8&
love14-concepts.md&6.9&## Human uniqueness (2)
love14-concepts.md&6.9&
love14-concepts.md&6.9&>- But we could try to argue against that.
love14-concepts.md&6.9&>- William Galston: “we view human beings as unique ... [because] even though every quality an individual has is possessed by others to some extent, the manner in which qualities are combined and emphasized ... is distinctive.”
love14-concepts.md&6.9&>- *What do you think of this argument?*
love14-concepts.md&6.9&
love14-concepts.md&6.10&## Human uniqueness (3)
love14-concepts.md&6.10&
love14-concepts.md&6.10&>- Criticism:
love14-concepts.md&6.10&>     - First, many combinations will only be trivially different from others. Persons near each other on the various dimensions will not be easily distinguishable (Soble, Structure, 53).
love14-concepts.md&6.10&>     - Second, the idea that each dimension takes on a “very large number of discrete values” is questionable. How many kinds of humour, wit, or beauty are there? If the number of properties is only a dozen or so, then the number of combinations is not very large.
love14-concepts.md&6.10&>     - Third, attributes and their values are not distributed randomly. Some properties always go together with others, and some are more likely to appear in clusters. Social factors destroy randomness and make people congregate at specific locations in that possibility space.
love14-concepts.md&6.10&
love14-concepts.md&7.0&# Does bestowal need appraisal?
love14-concepts.md&7.0&
love14-concepts.md&7.1&## Bestowal of love
love14-concepts.md&7.1&
love14-concepts.md&7.1&>- For Singer, bestowal of love consists in projecting a special kind of value into the beloved person: “Love ... confers importance no matter what the object is worth.” (p.273)
love14-concepts.md&7.1&>- Bestowal means “caring about the needs and interests of the beloved, ... wishing to benefit or protect her, ... delighting in her achievements” and so on. (p.270)
love14-concepts.md&7.1&>- This seems like the robust concern view. But what is the difference?
love14-concepts.md&7.1&>- In the robust concern view, the concern is the core of love.
love14-concepts.md&7.1&>- For Singer, the concern is the *consequence or effect* of the love that is established through bestowal.
love14-concepts.md&7.1&
love14-concepts.md&7.1&
love14-concepts.md&7.2&## Criticism of bestowal theories (1)
love14-concepts.md&7.2&
love14-concepts.md&7.2&>- Can you see how Singer’s view can be criticised?
love14-concepts.md&7.2&>- The idea of bestowal of love without appraisal is not clear at all.
love14-concepts.md&7.2&>     - Without any appraisal, I would have to love everyone in the same way. There would be no way to tell the difference between one person and another in terms of lovability.
love14-concepts.md&7.2&>     - This is fine for agape, but not a convincing way to describe romantic love.
love14-concepts.md&7.2&>     - In this view, Anna could not change her mind about loving Harry Lime, not would she have reason to.
love14-concepts.md&7.2&>- The justification of love therefore becomes impossible, and love remains a mystery.
love14-concepts.md&7.2&>- Or we admit that we *do* appraise at least *some* qualities of the beloved, but then the theory becomes a hybrid appraisal/bestowal theory, not pure bestowal.
love14-concepts.md&7.2&
love14-concepts.md&7.2&
love14-concepts.md&7.3&## Criticism of bestowal theories (2)
love14-concepts.md&7.3&
love14-concepts.md&7.3&>- Finally, how to distinguish between bestowal love, admiration, respect and other such attitudes? 
love14-concepts.md&7.3&>     - All seem to be bestowed in similar ways.
love14-concepts.md&7.3&>     - In order to distinguish between them, I’d have to introduce other theories again, for example, robust concern, union and so on.
love14-concepts.md&7.3&
love14-concepts.md&7.3&
love14-concepts.md&7.4&## Appraisal *and* bestowal
love14-concepts.md&7.4&
love14-concepts.md&7.4&>- Jollimore (2011)^[Jollimore, T, 2011, Love's Vision, Princeton, NJ: Princeton University Press.]:
love14-concepts.md&7.4&>     - Appraisal is something like perception. Bestowal is something like action.
love14-concepts.md&7.4&>     - We perceive particular properties in the beloved that we consider valuable (appraisal).
love14-concepts.md&7.4&>     - Then we attend to that person and appreciate their properties in a way that we don’t do with the properties of strangers.
love14-concepts.md&7.4&>     - Our appreciation of the beloved’s properties “silences” the similar properties of strangers for our perception.
love14-concepts.md&7.4&>     - So love is both appraisal (initially) but develops towards a bestowal. The beloved is perceived as unique as a consequence of that bestowal.
love14-concepts.md&7.4&
love14-concepts.md&7.5&## Appraisal *and* bestowal
love14-concepts.md&7.5&
love14-concepts.md&7.5&>- An approach like this tries to bring together the two theories.
love14-concepts.md&7.5&>- This seems similar to the “relationship view” we talked about last time (see the Julia Driver reading on Love and Duty).
love14-concepts.md&7.5&>- In order to make sense of that, we must distinguish:
love14-concepts.md&7.5&>     - “Falling in love,” which is based on appraisal of the future beloved’s qualities; and
love14-concepts.md&7.5&>     - “Staying in love,” which is based on the bestowal of love as the basis of a special relationship that defines that love.
love14-concepts.md&7.5&
love15-justifying.md&0.1&---
love15-justifying.md&0.1&title:  "Love and Sexuality: 15. Justifying love"
love15-justifying.md&0.1&author: Andreas Matthias, Lingnan University
love15-justifying.md&0.1&date: September 1, 2019
love15-justifying.md&0.1&...
love15-justifying.md&0.1&
love15-justifying.md&1.0&# Justifying love
love15-justifying.md&1.0&
love15-justifying.md&1.1&## Sources
love15-justifying.md&1.1&
love15-justifying.md&1.1&>- Edyvane, D. (2003). Against unconditional love. Journal of Applied Philosophy, 20(1), 59-75.
love15-justifying.md&1.1&>- Frankfurt, H. (2004). The Reasons of Love. Princeton: Princeton University Press.
love15-justifying.md&1.1&>- McKeever, N. (2018). What Can We Learn about Romantic Love from Harry Frankfurt's Account of Love. J. Ethics & Soc. Phil., 14, 204.
love15-justifying.md&1.1&>- Smuts, A. (2014). Normative Reasons for Love, Part I. Philosophy Compass, 9(8), 507-517.
love15-justifying.md&1.1&>- Smuts, A. (2014). Normative reasons for love, part II. Philosophy Compass, 9(8), 518-526.
love15-justifying.md&1.1&>- Wolf, Susan: “The True, the Good and the Lovable”, in Contours of Agency, 227-244, ed. Buss and Overton.
love15-justifying.md&1.1&
love15-justifying.md&1.1&
love15-justifying.md&2.0&# Harry Frankfurt’s theory of love
love15-justifying.md&2.0&
love15-justifying.md&2.1&## Kinds of love
love15-justifying.md&2.1&
love15-justifying.md&2.1&Frankfurt: 
love15-justifying.md&2.1&
love15-justifying.md&2.1&> “It is important to avoid confusing love ... with infatuation, lust, obsession, possessiveness, and dependency in their various forms [including romantic and sexual relatioships.] ... Relationships of those kinds typically include a number of vividly distracting elements, which do not belong to the essential nature of love as a mode of disinterested concern, but that are so confusing that they make it nearly impossible for anyone to be clear about just what is going on. Among relationships between humans, the love of parents for their infants or small children is the species of caring that comes closest to offering recognizably pure instances of love.” (The Reasons of Love, 43)
love15-justifying.md&2.1&
love15-justifying.md&2.1&. . . 
love15-justifying.md&2.1&
love15-justifying.md&2.1&So, Frankfurt is examining *storge* or *philia* rather than *eros* or romantic love.
love15-justifying.md&2.1&
love15-justifying.md&2.2&## Frankfurt’s necessary features of love
love15-justifying.md&2.2&
love15-justifying.md&2.2&“Four main conceptually necessary features of love of any variety:”
love15-justifying.md&2.2&
love15-justifying.md&2.2&. . .
love15-justifying.md&2.2&
love15-justifying.md&2.2&1. Love “consists most basically in a disinterested concern for the well-being or flourishing of the person who is loved.”
love15-justifying.md&2.2&
love15-justifying.md&2.2&. . . 
love15-justifying.md&2.2&
love15-justifying.md&2.2&2. Love is “ineluctably personal.”
love15-justifying.md&2.2&
love15-justifying.md&2.2&. . . 
love15-justifying.md&2.2&
love15-justifying.md&2.2&3. “The lover identifies with his beloved.”
love15-justifying.md&2.2&
love15-justifying.md&2.2&. . . 
love15-justifying.md&2.2&
love15-justifying.md&2.2&4. “Love is not a matter of choice.”
love15-justifying.md&2.2&
love15-justifying.md&2.2&
love15-justifying.md&2.3&## 1. Disinterested concern
love15-justifying.md&2.3&
love15-justifying.md&2.3&>- According to Frankfurt, love has to be “disinterested,” means: the lover should not be following his own interests, but only look towards the interests of the beloved.
love15-justifying.md&2.3&>- To love someone for personal gain is not real love.
love15-justifying.md&2.3&>- The beloved is a “final end.” (Meaning: a Kantial end in himself).
love15-justifying.md&2.3&
love15-justifying.md&2.4&## 1. Disinterested concern
love15-justifying.md&2.4&
love15-justifying.md&2.4&>- Problem: If love is important in our lives, then the lover will *always* also profit from their love. Because they get love in return for profiting the beloved. Therefore, no love can be totally “disinterested” in the lover’s own benefit.
love15-justifying.md&2.4&>- Solution?
love15-justifying.md&2.4&>- Frankfurt: Yes, the lover also gets something out of the love relationship, namely a life that includes love. But this can be achieved only through *disinterested* love first; so the condition still holds.
love15-justifying.md&2.4&>- Only by focusing on the needs of the beloved can the lover gain what they themselves need (the loving relationship).
love15-justifying.md&2.4&
love15-justifying.md&2.5&## 2. Love is personal
love15-justifying.md&2.5&
love15-justifying.md&2.5&>- Love is personal in that the particular person matters.
love15-justifying.md&2.5&>- A general agape towards all human beings is not proper love for Frankfurt.
love15-justifying.md&2.5&>- Loving a person must mean loving *this* person. Not even a copy or a better version of this person will do.
love15-justifying.md&2.5&>- This is supposed to exclude appraisal theories of love and the problem of “substituting Nancy” (see previous lecture).
love15-justifying.md&2.5&>- What makes a person *distinct* from others is not a matter of particular characteristics.
love15-justifying.md&2.5&
love15-justifying.md&2.6&## 3. Identification with the beloved
love15-justifying.md&2.6&
love15-justifying.md&2.6&>- Identification means to accept the interests of the beloved as one’s own.
love15-justifying.md&2.6&>- The charitable act of giving a beggar money does not fulfil this criterion. After the donation, I forget about the beggar. I do not robustly identify with their interests.
love15-justifying.md&2.6&>- Also, helping a homeless person is not personal. I want to help “a homeless” person. Not *this* particular person.
love15-justifying.md&2.6&>- Problem: Do you see a conflict between points 1 and 3 (disinterested concern and identification)?
love15-justifying.md&2.6&>     - If I identify with the beloved, then my concern cannot any more be “disinterested,” because now I am sharing the beloved’s interests.
love15-justifying.md&2.6&>     - We identified this in the previous session as a problem of union theories in general. After uniting, the two people form a “we,” in which it is impossible for the one person to benefit the other without also benefiting herself.
love15-justifying.md&2.6&
love15-justifying.md&2.7&## 4. Love is not a matter of choice
love15-justifying.md&2.7&
love15-justifying.md&2.7&>- Frankfurt: “Love is not a matter of choice but is determined by conditions that are outside our immediate voluntary control.” (The Reasons of Love, 80)
love15-justifying.md&2.7&>- Love creates a “volitional necessity”: 
love15-justifying.md&2.7&>     - Anna knows that she *could* and *should* stop loving Harry Lime, but she cannot bring herself to actually stop loving him. Her will is not strong enough.
love15-justifying.md&2.7&>- In a similar way, we cannot generally decide whether to stay alive or not. Although, in principle, we could commit suicide, for most of us this is not something that we can actually want or do.
love15-justifying.md&2.7&>- It is a “volitional necessity” to want to stay alive.
love15-justifying.md&2.7&
love15-justifying.md&2.8&## Constraint and freedom
love15-justifying.md&2.8&
love15-justifying.md&2.8&>- Paradoxically, Frankfurt maintains that although we are not free to stop loving, this does not affect our autonomy. How could this be?
love15-justifying.md&2.8&>- Frankfurt has a complex theory of what freedom means, but the main idea is that our freedom can be constrained from either the inside or the outside.
love15-justifying.md&2.8&>- Can you see how drug addiction is different from love or the will to stay alive?
love15-justifying.md&2.8&
love15-justifying.md&2.9&## Second order volitions
love15-justifying.md&2.9&
love15-justifying.md&2.9&>- The drug addict’s will is constrained from the outside. 
love15-justifying.md&2.9&>- He might want to not be a drug addict, but he is not free to stop taking the drug, because the drug (outside influence) is influencing his will.
love15-justifying.md&2.9&>- The person who has to want to stay alive, constrains their will “from the inside”: it is their own wish to stay alive that makes them want to avoid dying.
love15-justifying.md&2.9&>- Similarly, it is the own will of the lover to be someone who is in love with another person. 
love15-justifying.md&2.9&>     - Although we don’t have a choice about the person, loving is in accordance with our deeper wish to experience love. 
love15-justifying.md&2.9&>     - Therefore, we are not unfree when we are forced to love, because being in love is in line without our (deeper) wishes.
love15-justifying.md&2.9&>- Frankfurt calls these deeper wishes “second order volitions.”
love15-justifying.md&2.9&>- As long as our (first order) desires are in line with our second order volitions, we experience ourselves as free, even if we don’t have a choice about our first order desires.
love15-justifying.md&2.9&
love15-justifying.md&2.10&## Determinism and freedom
love15-justifying.md&2.10&
love15-justifying.md&2.10&>- This brings up a fundamental problem in the theory of love that we already talked about in the session about duty: can we control our emotions?
love15-justifying.md&2.10&>- One possible answer: Yes, we can exercise internal and external control on our emotions (Liao).
love15-justifying.md&2.10&>- Another answer: We can not control the *emotion itself,* but we can stop ourselves from acting on that emotion (Driver).
love15-justifying.md&2.10&>- A third answer is Frankfurt’s: although we cannot control our emotions, we can have the *feeling* that we are free, since our will (that we cannot control) is in line with our second order desires, and therefore feels like “our will”: It is making us (unfreely) will things that we would (freely) will anyway.
love15-justifying.md&2.10&
love15-justifying.md&2.11&## Love and value
love15-justifying.md&2.11&
love15-justifying.md&2.11&>- Frankfurt: What we love *acquires* value because we love it. (Not the other way round).
love15-justifying.md&2.11&>- This is a bestowal theory of love.
love15-justifying.md&2.11&>- Since love does not come about as a response to the value of a thing, a person’s properties are irrelevant to whether we love that person. 
love15-justifying.md&2.11&>- Frankfurt: The example of parental love clearly shows this. Parents would love their children even if these children had no objective worth (they were lazy, naughty, stupid, and so on).
love15-justifying.md&2.11&>- But if love does not respond to value, should Anna then be free to love Harry Lime?
love15-justifying.md&2.11&>- More generally, does it then matter at all whom we love? Is love’s choice entirely random?
love15-justifying.md&2.11&
love15-justifying.md&2.12&## The party guest
love15-justifying.md&2.12&
love15-justifying.md&2.12&>- Edyvane (p.71): Imagine you just met a stranger at a party. The stranger looks at you and says: “I’ve just seen you, but I am now deeply in love with you. Let’s go away together and stay together for the rest of our lives.”
love15-justifying.md&2.12&>- *How would you react?*
love15-justifying.md&2.12&>- You would find this creepy, perhaps? We wouldn’t generally accept a declaration of love that comes out of nowhere.
love15-justifying.md&2.12&>- Why not?
love15-justifying.md&2.12&>- Because we *do* think that there must be *some* reasons for love. 
love15-justifying.md&2.12&>- A totally unreasoned, unmotivated love seems unreasonable, crazy.
love15-justifying.md&2.12&>- To the party guest, you would perhaps answer: “But you don’t know me!”
love15-justifying.md&2.12&>- Which could mean: knowing me would have revealed particular properties of mine to you, which would make your declaration of love justifiable.
love15-justifying.md&2.12&
love15-justifying.md&2.13&## Worthy of being loved?
love15-justifying.md&2.13&
love15-justifying.md&2.13&>- But what properties could make an individual worthy of being loved?
love15-justifying.md&2.13&>- It cannot be that these are intrinsic properties of a person, like the hair colour, the humour, or the fact that they can dance. Why not?
love15-justifying.md&2.13&>- Because then we’d just be back at the appraisal theory of love and the problem of replacing our lover with another person who has all the same properties to an even higher degree.
love15-justifying.md&2.13&>- We need some way of saying that love is not totally arbitrary and random, but, at the same time, it should not be based *only* on properties of the beloved (thus making the beloved fungible!)
love15-justifying.md&2.13&
love15-justifying.md&2.14&## Susan Wolf: What makes someone worthy of love?
love15-justifying.md&2.14&
love15-justifying.md&2.14&>- Susan Wolf: An individual has to have “objective worth.”
love15-justifying.md&2.14&>- “People should care about only what is somewhat worth caring about; and how much people should care about things, both in themselves and relative to other things they care about, depends somewhat on how much worth caring about the objects in question are.” (p.232) 
love15-justifying.md&2.14&>- So one’s love needs to be proportional to the lovability of the beloved.
love15-justifying.md&2.14&
love15-justifying.md&2.15&## Susan Wolf: How to judge love-worthiness? (1)
love15-justifying.md&2.15&
love15-justifying.md&2.15&>- Whether (and how much) the object in question is itself worth caring about. 
love15-justifying.md&2.15&>     - Harry Lime? A football club? A lover who is not faithful, but violent etc?
love15-justifying.md&2.15&>     - This means that we can sensibly make an argument that one lover might be objectively better, more valuable, more worth caring about than another.
love15-justifying.md&2.15&>- Whether (and how much) the person has an affinity for the object in question. 
love15-justifying.md&2.15&>     - “Affinity” is “the suitability of an object for our affection.”
love15-justifying.md&2.15&>     - Similar to the suitability of a pair of shoes to a particular wearer.
love15-justifying.md&2.15&>     - This is similar to Velleman’s “contingent fit.”
love15-justifying.md&2.15&>- Whether (and how much) the relation between the person and the object has the potential to create or bring forth experiences, acts, or objects of further value.
love15-justifying.md&2.15&
love15-justifying.md&2.16&## Susan Wolf: How to judge love-worthiness? (2)
love15-justifying.md&2.16&
love15-justifying.md&2.16&>- Observe how especially the 2nd and 3rd are *relational* qualities. They are different from “appraisable” properties of the beloved.
love15-justifying.md&2.16&>- Therefore, they don’t cause the problems of fungibility of the beloved.
love15-justifying.md&2.16&
love15-justifying.md&2.17&## Edyvane on the value of the beloved
love15-justifying.md&2.17&
love15-justifying.md&2.17&>- For Edyvane, the proof of the value of love is the fact that the lover is trying to confirm that the beloved is worthy of their love.
love15-justifying.md&2.17&>- The moment lovers stop trying to convince themselves (and others) of the value of their beloved, the love has lost some of its value.
love15-justifying.md&2.17&>- “It is a part of the best kind of love that the lover takes an interest in the non-subjective worth of her beloved. It is a declaration that, at the point at which the lover loses interest in striving to confirm the worth of the beloved, she has ceased to love as much as she once had.” (Edyvane, 64)
love15-justifying.md&2.17&>- “When a person declares her love for us, she is also implicitly declaring her sincere belief that it would in principle be possible to provide some account of *why* we are worthy of that love.” (Edyvane, 72)
love15-justifying.md&2.17&
love15-justifying.md&2.18&## Justification as proof of worth
love15-justifying.md&2.18&
love15-justifying.md&2.18&>- For both Wolf and Edyvane, and opposed to the no-reasons view, there must be some assumption that the object of love is worthy of that love.
love15-justifying.md&2.18&>- This assumption must be proven, or demonstrated in an “objective” way, for others to see. 
love15-justifying.md&2.18&>- If it cannot be proven, at least there must be an attempt made by the lovers to do that.
love15-justifying.md&2.18&>- When lovers give up on the attempt to demonstrate the love-worthiness of the beloved, the love itself suffers.
love15-justifying.md&2.18&
love15-justifying.md&2.18&
love15-justifying.md&3.0&# Smuts: Normative Reasons for Love
love15-justifying.md&3.0&
love15-justifying.md&3.1&## Is love a relationship?
love15-justifying.md&3.1&
love15-justifying.md&3.1&>- Smuts, 509: Attitudes are different from relationships.
love15-justifying.md&3.1&>- I can have a friendship as a relationship. This includes an attitude towards my friend (a friendly disposition, a liking).
love15-justifying.md&3.1&>- Now my friend moves away or dies. The relationship is not there any more.
love15-justifying.md&3.1&>- But the attitude can persist. I can still feel friendliness towards my dead/moved away friend, even if it has become impossible to have a *relationship* of friendship now.
love15-justifying.md&3.1&
love15-justifying.md&3.2&## Kinds of lovable things
love15-justifying.md&3.2&
love15-justifying.md&3.2&Smuts, 509/510:
love15-justifying.md&3.2&
love15-justifying.md&3.2&>- We can love particular things and not others.
love15-justifying.md&3.2&>- A good test for lovable things is the “funny” test: does a sentence that says that we love X sound funny?
love15-justifying.md&3.2&>- Example: “I love my wife and my child more than anything.”
love15-justifying.md&3.2&>     - Not funny, so wife and child are lovable.
love15-justifying.md&3.2&>- “Before I met my wife, I loved fried chicken more than anything.”
love15-justifying.md&3.2&>     - This *is* kind of funny. So fried chicken is not a good object for my love.
love15-justifying.md&3.2&>     - “Love” for fried chicken is of a different kind than love to one’s wife.
love15-justifying.md&3.2&>- Love must include “caring for the beloved for her own sake.”
love15-justifying.md&3.2&
love15-justifying.md&3.2&
love15-justifying.md&3.3&## Motivating vs normative reasons
love15-justifying.md&3.3&
love15-justifying.md&3.3&>- When we are asked to justify something (like our love for a person), we can distinguish two different kinds of reasons:
love15-justifying.md&3.3&>- “Motivating reasons explain why someone did something. A motivating reason is the efficacious motive of an action.” (Smuts 512)
love15-justifying.md&3.3&>- But sometimes, one’s motivating reasons can also be justifying reasons. 
love15-justifying.md&3.3&>     - For instance, a [policeman] might have shot a terrorist to prevent him from setting off a bomb at a crowded event.
love15-justifying.md&3.3&>     - This does not only *explain* why he did it, but it also provides a (moral, legal) *justification* for that act. 
love15-justifying.md&3.3&>     - It is a *normative* reason.
love15-justifying.md&3.3&>- “When it comes to actions, normative reasons are those that count in favor of an action.”
love15-justifying.md&3.3&
love15-justifying.md&3.4&## Awareness of normative reasons
love15-justifying.md&3.4&
love15-justifying.md&3.4&>- If there *is* a normative reason, but we not aware of it, then this reason cannot justify our action! (For example, killing a terrorist before he triggers his bomb, at the time when we could not have known that this person is, indeed, a terrorist).
love15-justifying.md&3.4&>- “An attitude is a mere happy accident in relation to some normative reason unless the attitude is a *response* to that normative reason.” (Smuts, 512).
love15-justifying.md&3.4&
love15-justifying.md&3.5&## Strong and weak justifications
love15-justifying.md&3.5&
love15-justifying.md&3.5&>- Now, when we use a justifying (normative) reason, we have to make sure that we know what kind of justification that reason provides.
love15-justifying.md&3.5&>- There are three types of moral evaluation:
love15-justifying.md&3.5&>     - Reasons showing that an action is prohibited.
love15-justifying.md&3.5&>     - Reasons showing that an action is permissible.
love15-justifying.md&3.5&>     - Reasons showing that an action is required.
love15-justifying.md&3.5&>- Is Anna...
love15-justifying.md&3.5&>     - Not allowed to love Harry Lime?
love15-justifying.md&3.5&>     - Permitted to love Harry Lime or to withdraw her love?
love15-justifying.md&3.5&>     - Required to love Harry Lime?
love15-justifying.md&3.5&
love15-justifying.md&3.6&## Strong and weak justifications
love15-justifying.md&3.6&
love15-justifying.md&3.6&>- A strong justification shows that an attitude is required.
love15-justifying.md&3.6&>- A weak justification would only show that an attitude is permissible.
love15-justifying.md&3.6&>- Can reasons *strongly* justify love?
love15-justifying.md&3.6&>- Can I give a reason why I *ought to* love someone? (That is, why it would not be permissible to *not* love them?)
love15-justifying.md&3.6&>- Can I give a reason why it would *not* be permissible to love someone?
love15-justifying.md&3.6&>     - What if that person is Hitler? (Or, for that matter, Harry Lime?)
love15-justifying.md&3.6&>- Here, again, we see the asymmetry that Julia Driver mentioned in her paper.
love15-justifying.md&3.6&>     - There seem to be reasons *not* to love people, or to fall out of love with them.
love15-justifying.md&3.6&>     - But there don’t seem to be good reasons to fall in love with them.
love15-justifying.md&3.6&
love15-justifying.md&3.7&## Conclusions (1)
love15-justifying.md&3.7&
love15-justifying.md&3.7&>- What can we learn from that whole discussion?
love15-justifying.md&3.7&>- We must distinguish agape and eros. Romantic love seems to be more reasons-responsive and selective than agape.
love15-justifying.md&3.7&>- We must distinguish between (a) falling in love; (b) staying in love; and (c) falling out of (romantic) love.
love15-justifying.md&3.7&>- These three phenomena seem to work in different ways.
love15-justifying.md&3.7&>- It seems strange to say that *falling* in love is *not* dependent on reasons. We do have shopping lists and a concept of our ideal partner even before we meet them, and we evaluate candidates carefully before we fall in love with them (see online dating).
love15-justifying.md&3.7&
love15-justifying.md&3.8&## Conclusions (2)
love15-justifying.md&3.8&
love15-justifying.md&3.8&>- But it seems that the reasons fade away when we already *are* in a relationship. Then the relationship itself becomes the reason for the love.
love15-justifying.md&3.8&>- It also seems like there should be good reasons to fall *out of* love. Discovering that your beloved is an evil criminal might be such a reason.
love15-justifying.md&3.8&>- Even if we cannot entirely control our feelings, we are able to control their manifestation in action. Therefore, we might be held responsible for immoral actions we perform “out of love”.
love15-justifying.md&3.8&>- Maybe we can, to some extent, influence and control our emotions.
love15-justifying.md&3.8&>- Irrational/crazy love does not seem to be a good thing either. We expect our love to make some sense, and we expect to be able to show that the beloved is worthy of our love.
love15-justifying.md&3.8&
love15-justifying.md&3.8&
love15-justifying.md&3.8&
love15-justifying.md&3.8&
love16-emotion.md&0.1&---
love16-emotion.md&0.1&title:  "Love and Sexuality: 16. Is love an emotion?"
love16-emotion.md&0.1&author: Andreas Matthias, Lingnan University
love16-emotion.md&0.1&date: October 29, 2019
love16-emotion.md&0.1&...
love16-emotion.md&0.1&
love16-emotion.md&0.1&
love16-emotion.md&1.0&# Theories of emotion
love16-emotion.md&1.0&
love16-emotion.md&1.1&## Sources
love16-emotion.md&1.1&
love16-emotion.md&1.1&>- Pismenny, Prinz (2017): Is Love an Emotion? In: C. Grau and A. Smuts (ed.) The Oxford Handbook of Philosophy of Love, New York: OUP.
love16-emotion.md&1.1&>- Smuts, A. (2014). Normative Reasons for Love, Part I. Philosophy Compass, 9(8), 507-517.
love16-emotion.md&1.1&>- Smuts, A. (2014). Normative reasons for love, part II. Philosophy Compass, 9(8), 518-526.
love16-emotion.md&1.1&
love16-emotion.md&1.1&
love16-emotion.md&2.0&# Smuts: Normative Reasons for Love
love16-emotion.md&2.0&
love16-emotion.md&2.1&## Cognitive theory of emotions
love16-emotion.md&2.1&
love16-emotion.md&2.1&>- The cognitive theory of the emotions holds that emotions are object-directed attitudes that essentially involve evaluations. (Smuts, 510)
love16-emotion.md&2.1&>- Therefore, we can always say: “X has the emotion E that F,” where X is a person, E is an emotion and F is a fact.
love16-emotion.md&2.1&>     - “I am *afraid that* this disease will kill my tomato plants.”
love16-emotion.md&2.1&>     - “Jack is *angry that* Jill left the bucket on the hill.”
love16-emotion.md&2.1&>     - Emotions are always *about* something, they have an *object.*
love16-emotion.md&2.1&>- As opposed to proper emotions, *moods* do not take specific objects:
love16-emotion.md&2.1&>     - My brother is grumpy today.
love16-emotion.md&2.1&>     - My roommate is aggressive.
love16-emotion.md&2.1&
love16-emotion.md&2.2&## Is love a normal emotion?
love16-emotion.md&2.2&
love16-emotion.md&2.2&>- Three reasons against the thesis that love is an emotion (Smuts, 510, bottom):
love16-emotion.md&2.2&>- Reason One: Love is not episodic like fear.
love16-emotion.md&2.2&>     - “Episodic” means that it comes and goes in response to some particular, external condition.
love16-emotion.md&2.2&>     - For example, my fear of spiders is “activated” when I actually see a spider. 
love16-emotion.md&2.2&>     - When no spider is present, I have no actual fear. 
love16-emotion.md&2.2&>     - I might *know* that I am someone who is afraid of spiders (as a general disposition or feature of my personality), but I am not *actually* afraid at any moment when no spiders are present.
love16-emotion.md&2.2&>- It seems that love is more like a disposition. When I say that “I love my child,” then I mean a general condition, like “I am afraid of spiders.” I don’t mean that I necessarily actually feel that love right now.
love16-emotion.md&2.2&
love16-emotion.md&2.3&## Is love a normal emotion?
love16-emotion.md&2.3&
love16-emotion.md&2.3&>- Three reasons against the thesis that love is an emotion (Smuts, 510, bottom):
love16-emotion.md&2.3&>- Reason Two: There is no evaluation to distinguish love from other kinds of affect.
love16-emotion.md&2.3&>     - According to the cognitive theory of love, it is the evaluation we make that distinguishes one emotion from the other, not how it feels.
love16-emotion.md&2.3&>     - Many emotions feel similar (fear and anger).
love16-emotion.md&2.3&>     - But fear and anger differ in the evaluations they involve: anger involves having been wronged. Fear follows the judgement that something that one values is in danger.
love16-emotion.md&2.3&>     - Similarly: jealousy and envy; pride and joy.
love16-emotion.md&2.3&>- Only love does not have a plausible evaluation, except that the object is lovable.
love16-emotion.md&2.3&
love16-emotion.md&2.3&
love16-emotion.md&2.4&## Is love a normal emotion?
love16-emotion.md&2.4&
love16-emotion.md&2.4&>- But that, Smuts says, “is circular and entirely uninformative. Just what is it to judge an object to be loveable?”
love16-emotion.md&2.4&>- *Do you agree with Smuts here? Do we need to agree with him that saying that an object is lovable would be circular and uninformative? Is there any way that we could plausibly judge what kinds of objects are lovable?*
love16-emotion.md&2.4&>     - Remember Velleman (based on Kant): Lovability is a contingent fit between us and somebody else’s behaviour, based on the fundamental recognition of the other’s unlimited worth as a human being (=dignity).
love16-emotion.md&2.4&>     - This might be a kind of evaluation that could satisfy Smuts’ requirement above.
love16-emotion.md&2.4&
love16-emotion.md&2.4&
love16-emotion.md&2.4&
love16-emotion.md&2.5&## Is love a normal emotion?
love16-emotion.md&2.5&
love16-emotion.md&2.5&>- Three reasons against the thesis that love is an emotion (Smuts, 510, bottom):
love16-emotion.md&2.5&>- Reason Three: Emotions require that we care about something that will be affected. (Smuts, 511)
love16-emotion.md&2.5&>     - “Emotions are concern-based. They are evaluations of situations based on our concerns.”
love16-emotion.md&2.5&>     - “When we care about something, we feel fear when it is threatened, happy when it is benefited, angry when it is harmed, and hopeful when it stands to benefit.”
love16-emotion.md&2.5&>     - But then, where is the place of love? We want to say that “a mother’s love for her son *explains why* she is angry when he is injured, fearful when he is threatened, happy when does well ...” [and so on].
love16-emotion.md&2.5&>     - So, here love is not just one of the other emotions, but the principle of explanation behind all the more specific emotions! It is *the reason* to have emotions, and not itself one of these emotions.
love16-emotion.md&2.5&>- Conclusion: It doesn’t seem like love is a normal emotion.
love16-emotion.md&2.5&
love16-emotion.md&2.5&
love16-emotion.md&2.6&## The rationality of emotions
love16-emotion.md&2.6&
love16-emotion.md&2.6&>- What makes an emotion rational or appropriate, as opposed to an emotion that is irrational? How can we judge the rationality of emotions?
love16-emotion.md&2.6&>     - An emotion has to be reasonable;
love16-emotion.md&2.6&>     - it has to fit the situation;
love16-emotion.md&2.6&>     - its intensity should be proportional to its object;
love16-emotion.md&2.6&>     - it should be in our own long-term best interest;
love16-emotion.md&2.6&>     - it must be understandable. We must be able to understand why someone behaves that way. (Smuts, 512)
love16-emotion.md&2.6&>- Do these apply to love?
love16-emotion.md&2.6&>     - Most criteria don’t work very well for love, showing that love is not a normal, rational emotion.
love16-emotion.md&2.6&
love16-emotion.md&2.6&
love16-emotion.md&3.0&# Pismenny/Prinz: Is love an emotion?
love16-emotion.md&3.0&
love16-emotion.md&3.0&
love16-emotion.md&3.1&## Characteristics of love (Pismenny/Prinz, 1-2)
love16-emotion.md&3.1&
love16-emotion.md&3.1&>- Love is not felt all the time. It can be occurrent (actually felt) or dispositional (being in love without *feeling* it all the time).
love16-emotion.md&3.1&>- Love may be pleasant or unpleasant.
love16-emotion.md&3.1&>- As a mental state, love is always directed towards a particular object. It is intentional. One is always in love with a particular person, not just “generally in love.”
love16-emotion.md&3.1&>- Lovers tend to idealise the beloved.
love16-emotion.md&3.1&>- Love’s occurrence or absence are not in our control. We cannot simply choose to stop loving.
love16-emotion.md&3.1&
love16-emotion.md&3.1&
love16-emotion.md&3.1&
love16-emotion.md&3.2&## Basic features of emotions (1)
love16-emotion.md&3.2&
love16-emotion.md&3.2&>- They are *reactions* to experiences.
love16-emotion.md&3.2&>     - Encountering a spider causes fear.
love16-emotion.md&3.2&>     - Listening to a nice piece of music causes joy.
love16-emotion.md&3.2&>     - As such, they are not directly under our control.
love16-emotion.md&3.2&>- Emotions *feel* in a particular way.
love16-emotion.md&3.2&>     - Dispositional emotions (for example, a fear of spiders when no spider is present) must first be triggered to be felt.
love16-emotion.md&3.2&>- Emotions have two ‘objects’:
love16-emotion.md&3.2&>     - The *target*: the thing which the emotion is directed at (the spider, the wild dog, ...)
love16-emotion.md&3.2&>     - The *formal object*: the property that we fear in the target (the dangerousness of the spider or wild dog).
love16-emotion.md&3.2&
love16-emotion.md&3.2&
love16-emotion.md&3.3&## Basic features of emotions (2)
love16-emotion.md&3.3&
love16-emotion.md&3.3&>- Emotions therefore are *felt evaluations.*
love16-emotion.md&3.3&>- The function of emotions is to track what matters to us.
love16-emotion.md&3.3&>- As evaluations, emotions can be correct or incorrect, rational or irrational.
love16-emotion.md&3.3&>- An emotion is justified if “the formal object that it is picking out is actually provided by the target.”
love16-emotion.md&3.3&>     - A fear of that dog is justified if that dog is actually dangerous.
love16-emotion.md&3.3&>     - A fear of that spider might not be justified if that spider is not dangerous. (Pismenny/Prinz, 2)
love16-emotion.md&3.3&
love16-emotion.md&3.3&
love16-emotion.md&3.4&## Emotions as cognitive states (1)
love16-emotion.md&3.4&
love16-emotion.md&3.4&>- “Cognitive theories” see emotions as beliefs or judgements.
love16-emotion.md&3.4&>- To be afraid of a dog means to believe that there is a dog present and that it is dangerous.
love16-emotion.md&3.4&>- But what about love? Can love be described as a cognitive state in this way?
love16-emotion.md&3.4&>     - “Perhaps in love one would believe that a particular person has certain properties and judge them to be lovable.” (Pismenny/Prinz, 3)
love16-emotion.md&3.4&>     - But if this was the case, such properties should always bring about the emotion of love.
love16-emotion.md&3.4&>     - Compare spiders: every spider will bring about the belief that the spider is dangerous and the desire to run away from it.
love16-emotion.md&3.4&>     - But not every lovable person will bring about the “emotion” of love towards them. We are also often incapable of even saying *what exactly* the lovable properties of a person are.
love16-emotion.md&3.4&
love16-emotion.md&3.5&## Emotions as cognitive states (2)
love16-emotion.md&3.5&
love16-emotion.md&3.5&> “If the formal object of lovability is grounded in the focal properties the beloved possesses, then, as with other emotions, the conditions that make a given emotion apt generalize over all cases for that emotion. So just as fear is always apt in the presence of danger, so too, it would seem, love should always be apt in the presence of certain focal properties of the beloved.” (Pismenny/Prinz, 3)
love16-emotion.md&3.5&
love16-emotion.md&3.6&## Emotions as perceptions of the body (1)
love16-emotion.md&3.6&
love16-emotion.md&3.6&>- According to William James and Carl Lange, emotions are how particular bodily changes subjectively *feel* to us.
love16-emotion.md&3.6&>- Fear has a particular way of affecting the body: faster heartbeat, increased blood pressure, and other such biological effects.
love16-emotion.md&3.6&>- When we feel our body having these reactions, we call the resulting feeling “fear.”
love16-emotion.md&3.6&>- Could love be described in this way?
love16-emotion.md&3.6&>     - Perhaps we can agree that fear has a particular way of being expressed in body states.
love16-emotion.md&3.6&>     - But it does not seem that we can say the same about love.
love16-emotion.md&3.6&>     - Love seems to express itself in many different ways in the body. We can feel happiness, sadness, longing, sexual arousal and other bodily reactions that are all caused by love.
love16-emotion.md&3.6&
love16-emotion.md&3.7&## Emotions as perceptions of the body (2)
love16-emotion.md&3.7&
love16-emotion.md&3.7&> “But here we encounter an obvious difficulty. Love, even when romantic, can be felt in the absence of arousal. There can be cozy and endearing feelings of love, longing feeling, even feelings of fury, occasioned by mistreatment by a lover. All of these are manifestations of love. Conversely, we can feel arousal in response to those we do not love. This is a problem that has no obvious fix. The problem is that love has a variable phenomenology.” (Pismenny/Prinz, 4)
love16-emotion.md&3.7&
love16-emotion.md&3.8&## Emotions as perceptions of value
love16-emotion.md&3.8&
love16-emotion.md&3.8&>- We have already seen theories that view love as a response to the perceived *value of the beloved.*
love16-emotion.md&3.8&>     - For example, Velleman sees love as our response to other people’s dignity.
love16-emotion.md&3.8&>     - Singer sees love as creating value in the beloved through a process of bestowal.
love16-emotion.md&3.8&>- But if love was an emotion, we should be able to say when it is rational or justified.
love16-emotion.md&3.8&>- Theories that link love to values don’t clearly specify *what* values would justify love.
love16-emotion.md&3.8&>- “Love lacks reliable causes.” (Pismenny/Prinz, 6)
love16-emotion.md&3.8&
love16-emotion.md&3.8&
love16-emotion.md&4.0&# Basic, non-basic emotions and sentiments
love16-emotion.md&4.0&
love16-emotion.md&4.0&
love16-emotion.md&4.1&## Basic emotions (1)
love16-emotion.md&4.1&
love16-emotion.md&4.1&>- Emotions are called “basic” when:
love16-emotion.md&4.1&>     - They have “biologically prepared” responses; and
love16-emotion.md&4.1&>     - They are not themselves made up of other emotions. (Pismenny/Prinz, 6)
love16-emotion.md&4.1&>- For example, fear vs nostalgia:
love16-emotion.md&4.1&>     - Fear exists in babies and animals. We are born with the capacity to experience fear. Our reaction to fear is biologically determined.
love16-emotion.md&4.1&>     - Nostalgia seems to be learned later in life. Animals don’t have it.
love16-emotion.md&4.1&
love16-emotion.md&4.1&
love16-emotion.md&4.2&## Basic emotions (2)
love16-emotion.md&4.2&
love16-emotion.md&4.2&>- Basic emotions seem to have their own, typical facial expressions, which stay the same across cultures: anger, disgust, fear, happiness, sadness, and surprise.
love16-emotion.md&4.2&>- People from different cultures recognise the same emotion when shown a picture of a face with one of the basic emotions.
love16-emotion.md&4.2&>- Basic emotions come with particular changes in the body (heart beat, blood pressure and so on) that cause the individual to show particular, automated responses.
love16-emotion.md&4.2&>- These responses are quick and automatic and not based on reasoning. For example, you cannot just lose your fear of spiders by telling yourself that the particular kind of spider is not dangerous.
love16-emotion.md&4.2&
love16-emotion.md&4.3&## Is love a basic emotion?
love16-emotion.md&4.3&
love16-emotion.md&4.3&>- Is love a basic emotion?
love16-emotion.md&4.3&>- Just that love is common across cultures does not mean that it is a basic emotion. Nostalgia, for example, is also common, although it is not a basic emotion.
love16-emotion.md&4.3&>- Among mammals, long-term, exclusive bonding is rare (Pismenny/Prinz, 8). Most animals do not restrict sexual activity to one partner.
love16-emotion.md&4.3&>- Even in human beings, monogamy is relatively recent (since the rise of agriculture).
love16-emotion.md&4.3&>- We already saw that marriages were often not based on love.
love16-emotion.md&4.3&>- And romantic love only goes back to the 18th century.
love16-emotion.md&4.3&
love16-emotion.md&4.3&
love16-emotion.md&4.4&## Non-basic emotions
love16-emotion.md&4.4&
love16-emotion.md&4.4&>- Non-basic emotions are blends of basic emotions. Examples?
love16-emotion.md&4.4&>- “Despair may be a blend of fear and sadness ... contempt may be a blend of anger and disgust.” (Pismenny/Prinz, 8)
love16-emotion.md&4.4&>- *Is love a combination of lust and attachment?*
love16-emotion.md&4.4&>- “Lust is characterized as sexual desire, which can be short-lived, can be satisfied, and its target is fungible.”
love16-emotion.md&4.4&>- “In contrast, romantic love is characterized as an obsessive passionate state that has a normal shelf life of about eighteen months to four years, and its targets are deemed to be nonfungible.”
love16-emotion.md&4.4&>- “Attachment is characterized by a much more calm kind of state that can last one’s lifetime; it is something that appears after romantic love comes into existence, provided that it succeeds in forming a relationship. All three of these can exist separately.”
love16-emotion.md&4.4&
love16-emotion.md&4.4&
love16-emotion.md&4.5&## Sentiments (1)
love16-emotion.md&4.5&
love16-emotion.md&4.5&>- Sentiments are dispositions towards things (ways in which we tend to relate to these things) that are caused by repeated interactions with these things.
love16-emotion.md&4.5&>- Sentiments *cause* emotions:
love16-emotion.md&4.5&>- “Thus, if you are hopeful that some end can be achieved, then you normally ought also to be afraid when its accomplishment is threatened, relieved when the threat does not materialize, angry at those who intentionally obstruct progress toward it, and satisfied when you finally achieve it (or disappointed when you fail).” (Pismenny/Prinz, 10)
love16-emotion.md&4.5&
love16-emotion.md&4.5&
love16-emotion.md&4.6&## Sentiments (2)
love16-emotion.md&4.6&
love16-emotion.md&4.6&>- Criticism of love as sentiment:
love16-emotion.md&4.6&>     - Sentiments seem to narrow. A sentiment is a disposition towards particular emotions, but love also causes thoughts, beliefs and behaviours (the belief that the beloved is worthy of love, the behaviours of wanting to be with the beloved and so on).
love16-emotion.md&4.6&>     - Like emotions, sentiments are triggered by their target. They do not arise spontaneously.
love16-emotion.md&4.6&>     - Love, in contrast, is less reactive. We might be in love even without being in the presence of the beloved. (Pismenny/Prinz, 11)
love16-emotion.md&4.6&
love16-emotion.md&4.6&
love16-emotion.md&5.0&# Love as a syndrome
love16-emotion.md&5.0&
love16-emotion.md&5.1&## Syndromes (1)
love16-emotion.md&5.1&
love16-emotion.md&5.1&>- A syndrome is “an organized set of responses (behavioural, physiological, and/or cognitive).” ^[Averill, James (1985). “The Social Construction of Emotion: With Special Reference to Love.” The Social Construction of the Person edited by K.J. Gergan and K.E. Davis, pp 89–109. New York: Springer.]
love16-emotion.md&5.1&>- The love syndrome has several features:
love16-emotion.md&5.1&>     - idealization of the loved one
love16-emotion.md&5.1&>     - suddenness of onset (‘love at first sight’)
love16-emotion.md&5.1&>     - physiological arousal
love16-emotion.md&5.1&>     - commitment to, and willingness to make sacrifices for, the loved one.” (Pismenny/Prinz, 11)
love16-emotion.md&5.1&>- Ronald de Sousa: Love is “a syndrome: not a kind of feeling, but an intricate pattern of potential thoughts, behaviors, and emotions that tend to ‘run together’.”
love16-emotion.md&5.1&>- Greek: syn-: together; dromos: way. Literally: the walking together along a way, travelling together.
love16-emotion.md&5.1&
love16-emotion.md&5.2&## Syndromes (2)
love16-emotion.md&5.2&
love16-emotion.md&5.2&>- Syndromes are often used to describe mental health conditions (Pismenny/Prinz, 12).
love16-emotion.md&5.2&>- We can also use them to describe other illnesses.
love16-emotion.md&5.2&>- A common cold can cause a set of responses:
love16-emotion.md&5.2&>     - Bodily/physiological responses: fever, a running nose, cough.
love16-emotion.md&5.2&>     - Behavioural responses: inability to stand up for long periods, tendency to fall asleep.
love16-emotion.md&5.2&>     - Cognitive responses: slowness in thinking, inability to concentrate, no desire to engage in sports.
love16-emotion.md&5.2&>- These responses are different from person to person. Other responses are possible, and the ones mentioned might be absent.
love16-emotion.md&5.2&>- Syndromes are not rational responses.
love16-emotion.md&5.2&>- They might be influenced by culture, but are not *created* by a particular culture.
love16-emotion.md&5.2&
love16-emotion.md&5.3&## Reasons for love to be a syndrome
love16-emotion.md&5.3&
love16-emotion.md&5.3&>- Syndromes don’t need to have a formal object (while emotions do).
love16-emotion.md&5.3&>- Love does not need to be justifiable by reasons. The same is true of syndromes.
love16-emotion.md&5.3&>- Love is not associated with a particular bodily state. The same is true of syndromes.
love16-emotion.md&5.3&>- Love is not innate and not limited to brief episodes. Syndromes don’t need to be either.
love16-emotion.md&5.3&>- Love is not a sentiment because it encompasses beliefs and behaviours as well as feelings. This is also true of syndromes. (Pismenny/Prinz, 13)
love16-emotion.md&5.3&>- “Love is not a detector of properties; it is a way of being in the world.”
love16-emotion.md&5.3&
love16-emotion.md&5.3&
love17-classics.md&0.1&---
love17-classics.md&0.1&title:  "Love and Sexuality: 17. Classic theories of love"
love17-classics.md&0.1&author: Andreas Matthias, Lingnan University
love17-classics.md&0.1&date: October 20, 2019
love17-classics.md&0.1&...
love17-classics.md&0.1&
love17-classics.md&1.0&# Sternberg’s Triangular Theory of Love (1986)
love17-classics.md&1.0&
love17-classics.md&1.1&## Sources
love17-classics.md&1.1&
love17-classics.md&1.1&>- Sternberg (1986): A Triangular Theory of Love. Psychological Review 93(2): 119-135.
love17-classics.md&1.1&>- Regan (2008): General Theories of Love. In: Regan, P. (ed): The Mating Game: A Primer on Love, Sex, and Marriage. SAGE Publications.
love17-classics.md&1.1&>- Masahiro Masuda (2003): Meta-analyses of love scales. Japanese Psychological Research, 45(1), 25–37.
love17-classics.md&1.1&>- Sternberg, R. J. (1997). Construct validation of a triangular love scale. European Journal of Social Psychology, 27(3), 313-335.
love17-classics.md&1.1&
love17-classics.md&1.1&
love17-classics.md&1.2&## Triangular theory of love
love17-classics.md&1.2&
love17-classics.md&1.2&![](graphics/sternberg-triangle.png)\
love17-classics.md&1.2&
love17-classics.md&1.2&
love17-classics.md&1.2&
love17-classics.md&1.3&## Three components
love17-classics.md&1.3&
love17-classics.md&1.3&>- Intimacy: 
love17-classics.md&1.3&>     - Emotional component (how it feels).
love17-classics.md&1.3&>     - Warmth, closeness, connection, bondedness.
love17-classics.md&1.3&>- Passion:
love17-classics.md&1.3&>     - Motivational component (motivates action).
love17-classics.md&1.3&>     - Romantic, physical, sexual attraction.
love17-classics.md&1.3&>- Decision/Commitment:
love17-classics.md&1.3&>     - Cognitive component (abstract thought, resolutions).
love17-classics.md&1.3&>     - Short-term decision that one individual loves another.
love17-classics.md&1.3&>     - Longer term commitment to maintain that love.
love17-classics.md&1.3&
love17-classics.md&1.4&## Different properties
love17-classics.md&1.4&
love17-classics.md&1.4&>- Intimacy, passion and decision/commitment differ in respect 
love17-classics.md&1.4&>- How stable they are:
love17-classics.md&1.4&>     - Intimacy and decision/commitment more stable than passion.
love17-classics.md&1.4&>- How much they can be consciously controlled:
love17-classics.md&1.4&>     - Commitment most easy to control, intimacy somewhat less, passion least.
love17-classics.md&1.4&>- How much the lover is aware of them:
love17-classics.md&1.4&>     - Passion is usually quite obvious to the subject; but intimacy or commitment may not always be consciously perceived in everyday life.
love17-classics.md&1.4&
love17-classics.md&1.5&## Types of love
love17-classics.md&1.5&
love17-classics.md&1.5&>- Nonlove (no intimacy, passion, or decision/commitment).
love17-classics.md&1.5&>- Liking (intimacy alone): (superficial) friendship.
love17-classics.md&1.5&>- Infatuation (passion alone): “love at first sight,” extreme attraction.
love17-classics.md&1.5&>- Empty love (decision/commitment alone): Often at the end of long-term relationships.
love17-classics.md&1.5&>- Romantic love (intimacy + passion): feelings of closeness and connection together with strong physical attraction. 
love17-classics.md&1.5&>- Companionate love (intimacy + decision/commitment): long-term, stable, and committed friendship (best friend, long term marriage).
love17-classics.md&1.5&>- Fatuous love (passion + decision/commitment): commitment based on passion alone. Typically unstable.
love17-classics.md&1.5&>- Consummate love (intimacy + passion + decision/commitment): “Complete” romantic love.
love17-classics.md&1.5&
love17-classics.md&1.6&## Types of love
love17-classics.md&1.6&
love17-classics.md&1.6&![](graphics/sternberg-table.png)\
love17-classics.md&1.6&
love17-classics.md&1.6&
love17-classics.md&1.6&
love17-classics.md&1.7&## Discussion (1)
love17-classics.md&1.7&
love17-classics.md&1.7&>- Sternberg^[Sternberg, R. J. (1997). Construct validation of a triangular love scale. European Journal of Social Psychology, 27(3), 313-335.]: We have to consider:
love17-classics.md&1.7&>     - Self- and other-perceived triangles. Two partners in a relationship might judge the relationship in a different way.
love17-classics.md&1.7&>     - Difference between real and ideal triangles of love. We might have particular ideals about love, but not behave in a way that is consistent with these ideals.
love17-classics.md&1.7&>     - There might also be a difference between the way we *feel* about love in general, and how important particular aspects of it are to our real relationships. For example, most people would say that Romeo and Juliet is a great love story, but most people wouldn’t want to have a relationship like that.
love17-classics.md&1.7&
love17-classics.md&1.8&## Discussion (2)
love17-classics.md&1.8&
love17-classics.md&1.8&>- In the Sternberg (1997) paper, respondents agreed more on what is *characteristic* in a relationship (in principle) than they agreed on what is *important* to them personally.
love17-classics.md&1.8&>- This perhaps shows a strong influence of the culture in shaping our expectations of love; but also people’s ability to separate the cultural prototype from their lived experiences.
love17-classics.md&1.8&>- It was shown that the Sternberg scale ratings correlated very strongly with relationship *satisfaction.*
love17-classics.md&1.8&>- So if a relationship scored high on all three dimensions, then the participants would probably also experience the relationship as satisfactory. 
love17-classics.md&1.8&>- Satisfaction was correlated stronger with intimacy (0.86) than with passion (0.77) and commitment (0.75).
love17-classics.md&1.8&
love17-classics.md&1.8&
love17-classics.md&1.9&## Discussion (3)
love17-classics.md&1.9&
love17-classics.md&1.9&>- Do the three “dimensions” really measure different things?
love17-classics.md&1.9&>     - Studies have found (Hendrick\&Hendrick 1989) that couples who score high on one dimension, also score high on the others. 
love17-classics.md&1.9&>     - So it seems that the scale actually measures only *one* kind of thing (“love”?) rather than three independent dimensions.
love17-classics.md&1.9&>     - Sternberg (1997) disagrees.
love17-classics.md&1.9&
love17-classics.md&1.9&
love17-classics.md&1.10&## Discussion (4)
love17-classics.md&1.10&
love17-classics.md&1.10&>- Why only these three dimensions and not others?
love17-classics.md&1.10&>     - The three dimensions cover all three: emotion, motivation and thought. 
love17-classics.md&1.10&>     - This seems to be a good thing. But why is the motivation only measured in terms of passion? Could we not be motivated by “intimacy,” for example?
love17-classics.md&1.10&>     - We could also imagine totally different dimensions (like Lee has done, below). What makes the particular three dimensions of Sternberg special, so that we should believe that there are only these three that are relevant?
love17-classics.md&1.10&>      - For example: Hatfield (1984, 1988): passionate and companionate love. Davis (1985): physical attraction, caring, and liking.
love17-classics.md&1.10&
love17-classics.md&1.10&
love17-classics.md&1.11&## Discussion (5)
love17-classics.md&1.11&
love17-classics.md&1.11&>- Sternberg’s choice of words is odd and does not reflect today’s use of language. 
love17-classics.md&1.11&>     - “Intimacy” seems to denote sexual acts;
love17-classics.md&1.11&>     - “Passion” can mean passion for one’s work or hobby.
love17-classics.md&1.11&>- It does not seem like a good idea to misuse language in this way. This makes comparing different theories of love harder.
love17-classics.md&1.11&
love17-classics.md&1.11&
love17-classics.md&2.0&# John Alan Lee: The Colours of Love
love17-classics.md&2.0&
love17-classics.md&2.1&## Colours of love (1)
love17-classics.md&2.1&
love17-classics.md&2.1&![](graphics/lee-colour-wheel.png)\
love17-classics.md&2.1&
love17-classics.md&2.1&
love17-classics.md&2.1&
love17-classics.md&2.2&## Colours of love (2)
love17-classics.md&2.2&
love17-classics.md&2.2&>- A metaphor of primary and secondary colours.
love17-classics.md&2.2&>- Similar to Sternberg’s triangle in that there are also three main and three secondary kinds of love.
love17-classics.md&2.2&
love17-classics.md&2.2&
love17-classics.md&2.3&## Colours of love (3)
love17-classics.md&2.3&
love17-classics.md&2.3&>- Eros: 
love17-classics.md&2.3&>     - Immediate and powerful attraction to the beloved individual. 
love17-classics.md&2.3&>     - Triggered by a particular physical type.
love17-classics.md&2.3&>     - “Love at first sight.”
love17-classics.md&2.3&>     - Intense need for daily contact with the beloved.
love17-classics.md&2.3&>     - Exclusive and sexual relationship.
love17-classics.md&2.3&>- Ludus (game-like love):
love17-classics.md&2.3&>     - Several partners simultaneously.
love17-classics.md&2.3&>     - Playful, not looking towards the future.
love17-classics.md&2.3&>     - Justifying lies and deception.
love17-classics.md&2.3&>     - Wide variety of physical types.
love17-classics.md&2.3&>     - Sexual activity is an opportunity for pleasure rather than for intense emotional bonding.
love17-classics.md&2.3&
love17-classics.md&2.4&## Colours of love (4)
love17-classics.md&2.4&
love17-classics.md&2.4&>- Storge:
love17-classics.md&2.4&>     - Stable affection, based on trust, respect, and friendship.
love17-classics.md&2.4&>     - “Old friends.” 
love17-classics.md&2.4&>     - No intense emotions or physical attraction associated with erotic love.
love17-classics.md&2.4&>     - Shared interests with the partner.
love17-classics.md&2.4&>     - Shy about sex, demonstrating affection in nonsexual ways.
love17-classics.md&2.4&>- Love is an extension of friendship and an important part of life.
love17-classics.md&2.4&
love17-classics.md&2.4&
love17-classics.md&2.5&## Colours of love (5)
love17-classics.md&2.5&
love17-classics.md&2.5&>- In addition to the three primary colours, there are three secondary colours:
love17-classics.md&2.5&>- Pragma: 
love17-classics.md&2.5&>     - Combination of storge and ludus.
love17-classics.md&2.5&>     - The love that goes shopping for a suitable mate” (Lee, 1973, p. 124). 
love17-classics.md&2.5&>     - Practical outlook on love, seeking a compatible lover.
love17-classics.md&2.5&>     - Shopping list of features or attributes desired.
love17-classics.md&2.5&>- Mania: Combination of eros and ludus.
love17-classics.md&2.5&>     - Obsessive, jealous love.
love17-classics.md&2.5&>     - Self-defeating emotions, desperate attempts to force affection from the beloved, and the inability to believe in or trust any affection the loved one actually does display.
love17-classics.md&2.5&>     - Distrust and extreme possessiveness.
love17-classics.md&2.5&>     - Often unhappy.
love17-classics.md&2.5&
love17-classics.md&2.6&## Colours of love (6)
love17-classics.md&2.6&
love17-classics.md&2.6&>- Agape:
love17-classics.md&2.6&>     - Combination of eros and storge. 
love17-classics.md&2.6&>     - Similar to Lewis’s concept of charity.
love17-classics.md&2.6&>     - All-giving, selfless love.
love17-classics.md&2.6&>     - Loving and caring for others without any expectation of reciprocity or reward.
love17-classics.md&2.6&>     - Unselfishly devoted to the partner.
love17-classics.md&2.6&
love17-classics.md&2.6&
love17-classics.md&2.6&
love17-classics.md&2.7&## Can Sternberg and Lee be compared? (1)
love17-classics.md&2.7&
love17-classics.md&2.7&>- According to Masahiro Masuda^[Masahiro Masuda (2003): Meta-analyses of love scales. Japanese Psychological Research, 45(1), 25–37.], we can clearly see a dichotomy of love scales into:
love17-classics.md&2.7&>     - Sexual attraction to romantic partners (“E-Love”); and 
love17-classics.md&2.7&>     - non-sexual psychological closeness to partners, characterized by respect and caring (“C-Love”).
love17-classics.md&2.7&>- But Lee’s storge may not be associated with satisfaction in romantic relationships in the same way as Sternberg’s C-love types.
love17-classics.md&2.7&>- “Lee’s Storge may be a qualitatively different psychological construct from other theorists’ C-Loves.” (Masuda, Love scales, 35)
love17-classics.md&2.7&
love17-classics.md&2.8&## Can Sternberg and Lee be compared? (2)
love17-classics.md&2.8&
love17-classics.md&2.8&>- As opposed to Sterberg’s triangle, in this theory the “perfect” love is not supposed to be an equal mixture of all three primary “colours.”
love17-classics.md&2.8&>- The “colours” are more seen as “styles” rather than “components” of a love relationship. (But different styles can also be mixed in particular relationships).
love17-classics.md&2.8&>- It seems strange that Lee would see Eros as part of Agape (!) If agape is non-appraising, selfless charity, then how can it contains elements of sexual love? This doesn’t seem to make sense.
love17-classics.md&2.8&>- Given that (Masuda) Lee’s storge also scores differently in surveys than Sternberg’s “companionate love,” it seems that something is wrong here. Lee’s concept of storge does not seem to be the same as Christian agape!
love17-classics.md&2.8&
love17-classics.md&2.8&
love17-classics.md&3.0&# Final conclusions
love17-classics.md&3.0&
love17-classics.md&3.1&## Harry Lime and Anna’s problem
love17-classics.md&3.1&
love17-classics.md&3.1&>- We began this discussion a few sessions ago with the question whether Anna could or should love/despise the criminal Harry Lime.
love17-classics.md&3.1&>- We identified multiple problems related to this question:
love17-classics.md&3.1&>     - Can Anna be expected to control her emotions?
love17-classics.md&3.1&>     - Is it even possible to control one’s emotions?
love17-classics.md&3.1&>     - Is love responsive to reasons?
love17-classics.md&3.1&>     - Does love need a justification?
love17-classics.md&3.1&>     - Is love an emotion at all?
love17-classics.md&3.1&>     - Or is love a complex social phenomenon, composed of different components (emotions, motivations, commitments)?
love17-classics.md&3.1&
love17-classics.md&3.2&## Anna’s options (1)
love17-classics.md&3.2&
love17-classics.md&3.2&>- Is love an emotion? (probably no).
love17-classics.md&3.2&>- Love seems to be a complex of multiple things:
love17-classics.md&3.2&>     - Sternberg: Intimacy (emotion), passion (motivation), decision/commitment (thought).
love17-classics.md&3.2&>     - This is consistent with the Pismenny/Prinz concept of love as a syndrome of physiological, behavioural and cognitive responses.
love17-classics.md&3.2&>- Sternberg might say that Anna might not be able to control her emotion or passion, but she should be able to control and negate her commitment to Harry Lime.
love17-classics.md&3.2&
love17-classics.md&3.3&## Anna’s options (2)
love17-classics.md&3.3&
love17-classics.md&3.3&>- A Kantian approach would suggest that Anna should respect Harry Lime’s dignity and humanity; but that she should also let him be caught and not try to save him. 
love17-classics.md&3.3&>- By being a criminal, Harry Lime has shown disregard for others’ dignity, and it is fair that now others treat him in the same way.
love17-classics.md&3.3&
love17-classics.md&3.4&## Anna’s options (3)
love17-classics.md&3.4&
love17-classics.md&3.4&>- If we distinguish “falling” from “staying” in love, things get more complicated. 
love17-classics.md&3.4&>- The first may not require reasons, the second might. 
love17-classics.md&3.4&>- Relationship theories would see the falling in love as an appraisal (or mysterious) event, while staying in love is a bestowal of love, based on the relationship itself.
love17-classics.md&3.4&>- In this view, it would be harder to justify not “staying” in love with Harry Lime, since the relationship itself is what counts.
love17-classics.md&3.4&>- But one might question that the relationship is honest and based on the true properties of the lovers (since Harry Lime was not honest about his criminal acts to Anna).
love17-classics.md&3.4&
love17-classics.md&3.5&## Anna’s options (4)
love17-classics.md&3.5&
love17-classics.md&3.5&>- Susan Wolf believes (and Edyvane seems to agree) that we must have reasons to love a particular person. Love cannot be irrational.
love17-classics.md&3.5&>- In fact, it might be that looking for such reasons is a good sign for the presence of love.
love17-classics.md&3.5&>- The lover must be “worthy of love” (Susan Wolf).
love17-classics.md&3.5&
love17-classics.md&3.5&
love17-classics.md&3.6&## Anna’s options (5)
love17-classics.md&3.6&
love17-classics.md&3.6&>- Even if we think of love as an emotion, we might question whether Anna has to *act* upon her emotion.
love17-classics.md&3.6&>- Perhaps it’s possible to have an emotion but not to act upon it (Julia Driver). We can separate the “emotional” from the “evaluative commitment” part.
love17-classics.md&3.6&>- Or perhaps we can command and influence our emotions more than we think (Matthew Liao). Anna could reflect on her emotions, visualise her reasons for loving or hating Harry Lime, and try to cultivate appropriate emotions toward him (by visualising his victims, for example). Internal and external control methods are available, and this removes Anna’s excuse for having the wrong emotions about Harry Lime.
love17-classics.md&3.6&
love17-classics.md&3.6&
love18-ethology.md&0.1&---
love18-ethology.md&0.1&title:  "Love and Sexuality: 18. The biology and ethology of love"
love18-ethology.md&0.1&author: Andreas Matthias, Lingnan University
love18-ethology.md&0.1&date: September 1, 2019
love18-ethology.md&0.1&...
love18-ethology.md&0.1&
love18-ethology.md&1.0&# Human ethology
love18-ethology.md&1.0&
love18-ethology.md&1.1&## What is ethology?
love18-ethology.md&1.1&
love18-ethology.md&1.1&>- Main source for this chapter: Eibl-Eibesfeldt, I. (2017). Human ethology. Routledge.
love18-ethology.md&1.1&>- Ethology is the scientific study of animal behaviour under natural conditions (as opposed to laboratory experiments). 
love18-ethology.md&1.1&>- Ethologists generally view animal behaviour as the result of evolutionary adaptation.
love18-ethology.md&1.1&>- “Human ethology” was founded by Irenaeus Eibl-Eibesfeldt, the author of today’s reading.
love18-ethology.md&1.1&>- Human ethology tries to understand human behaviour in the same way as it studies animal behaviour, and sees it as a result of evolutionary adaptation.
love18-ethology.md&1.1&
love18-ethology.md&1.2&## Ethnology and ethology
love18-ethology.md&1.2&
love18-ethology.md&1.2&>- Ethology is often confused with ethnology.
love18-ethology.md&1.2&>     - Ethos (Greek): custom, habit, morality (where “ethics” also comes from).
love18-ethology.md&1.2&>     - Ethnos (also Greek): nation.
love18-ethology.md&1.2&>- Therefore, ethology: the study of behaviours and customs.
love18-ethology.md&1.2&>- Ethnology: the study of different “nations,” “tribes,” cultures and groups of people.
love18-ethology.md&1.2&>- The two are loosely related, because in both cases the object of study are people, often from different cultures.
love18-ethology.md&1.2&>     - But ethology is a part of biology, it describes people’s behaviour as a kind of animal behaviour.
love18-ethology.md&1.2&>     - Ethnology, on the other hand, is more like social science: it describes cultural practices and how they differ across societies, without referring to biological terms.
love18-ethology.md&1.2&
love18-ethology.md&1.2&
love18-ethology.md&1.3&## Main proponents of ethology
love18-ethology.md&1.3&
love18-ethology.md&1.3&>- Ethology (as the biologically-inspired study of animal behaviour) begins with Charles Darwin (1809-1882) who studied the expression of animal emotions.
love18-ethology.md&1.3&>- Konrad Lorenz (Austrian, 1903–1989) is one of the founders of modern ethology.
love18-ethology.md&1.3&>     - He studied animal behaviour in everyday situations and described “imprinting,” the concept that newborn animals will attach themselves to a caregiver, even if the caregiver is of a different species.
love18-ethology.md&1.3&>     - In 1973, he received the Nobel Prize together with Niko Tinbergen and Karl von Frisch.
love18-ethology.md&1.3&>     - Tinbergen mainly studied instinctual actions of animals, especially honey bees, and created a model of how a bee decides which flowers to approach.
love18-ethology.md&1.3&>     - Karl von Frisch also worked on bees and tried to decode the bee dance. He also researched how bees perceive colour and compass directions.
love18-ethology.md&1.3&
love18-ethology.md&1.3&
love18-ethology.md&1.4&## Some topics ethologists study
love18-ethology.md&1.4&
love18-ethology.md&1.4&>- “Fixed action patterns”: Whole, fixed sequences of animal behaviour that are triggered by a stimulus and then executed “automatically” to the end of the sequence.
love18-ethology.md&1.4&>     - These include mating behaviour, attacking ‘enemies’ that have particular characteristics, snakes following bitten prey, the sucking reflex of newborn babies, and other behaviours.
love18-ethology.md&1.4&>     - The point is that these behaviours are seen to be “hard-wired” into the animal. Once triggered, they cannot be changed or stopped.
love18-ethology.md&1.4&>     - This, of course, is interesting also for humans. It has been suggested that some bodily features might be such triggers for sexual responses.
love18-ethology.md&1.4&>     - If this was true, it would move part of our sexuality away from the area of conscious decision and make it into an (involuntary) instinct.
love18-ethology.md&1.4&
love18-ethology.md&1.4&
love18-ethology.md&1.5&## Cautionary notes about ethology (1)
love18-ethology.md&1.5&
love18-ethology.md&1.5&>- Ethology, obviously, is not free of controversy.
love18-ethology.md&1.5&>- For example, describing human behaviour in the same terms as instinctive animal behaviour will offend Christians, Kantians, and anyone who believes in the primacy of reason over our animal nature.
love18-ethology.md&1.5&>- Ethology might (and occasionally does) provide evidence for particular “built-in” role behaviours of women and men. This might be questioned by those who don’t believe in biologically fixated gender roles.
love18-ethology.md&1.5&>- Ethology might describe homosexuality as “unnatural” behaviour in the sense that it might not be evolutionary advantageous. Also, ethology would look at whether animals show homosexual behaviours in order to understand the role of human homosexuality.
love18-ethology.md&1.5&
love18-ethology.md&1.5&
love18-ethology.md&1.6&## Cautionary notes about ethology (2)
love18-ethology.md&1.6&
love18-ethology.md&1.6&>- All these methods and conclusions might be considered not politically correct, not scientifically correct, or not desirable by different groups within society.
love18-ethology.md&1.6&>- For example, sometimes ethology has been accused on “cherry-picking” evidence that supports its claims, and overlooking other evidence that might contradict them.
love18-ethology.md&1.6&>- So please do your own research and engage with ethology in a critical and scientific way. 
love18-ethology.md&1.6&>     - Don’t accept its statements as a religion. 
love18-ethology.md&1.6&>     - It is a field of study like any other and, like any other science, it can occasionally be mistaken. 
love18-ethology.md&1.6&>     - But it can also be right where we don’t like it. We should also be prepared to accept its statements where they can be shown to be true, and not reject them out of a misguided wish to please particular social groups.
love18-ethology.md&1.6&
love18-ethology.md&1.7&## Irenaeus Eibl-Eibesfeldt
love18-ethology.md&1.7&
love18-ethology.md&1.7&>- Irenaeus Eibl-Eibesfeldt (1928-2018) became particularly known for applying ethology research methods to humans.
love18-ethology.md&1.7&>- The German Wikipedia has a long article about him and his work at: <https://de.wikipedia.org/wiki/Irenäus_Eibl-Eibesfeldt> You can try to Google translate that.
love18-ethology.md&1.7&>- Eibl-Eibesfeldt, despite being the recipient of at least 18 major scientific awards (see the English Wikipedia page), has been extensively criticised for some of his opinions, for example for stating that humans are naturally reluctant to accept strangers from other tribes into their own community (read: migrants).
love18-ethology.md&1.7&>- This would seem to provide a biologically justified basis for the rejection of migrants.
love18-ethology.md&1.7&
love18-ethology.md&1.8&## Justification of anti-migration stance?
love18-ethology.md&1.8&
love18-ethology.md&1.8&>- *What do you think? Is this necessarily so?*
love18-ethology.md&1.8&>     - Not really. Showing that humans have a natural tendency to mistrust migrants from other cultures (assuming for the sake of the argument that this is true) would not necessarily allow us to conclude that it is *right* to mistrust migrants.
love18-ethology.md&1.8&>     - In ethics, this is called the Is-Ought-Fallacy (look it up).
love18-ethology.md&1.8&>     - In the same way, we could try to argue: “Because humans can steal, and naturally they desire the shiny possessions of others, therefore stealing is morally right.”
love18-ethology.md&1.8&>     - This does not follow. Even if it is true that we desire others’ possessions, being human places particular moral demands on us, for example to resist such desires.
love18-ethology.md&1.8&>     - The fact that I desire someone else’s possessions does not justify my action to take away these possessions from the other person.
love18-ethology.md&1.8&>- In the same way, what ethology considers “natural” behaviour, is not therefore automatically “desirable” or “morally right” behaviour.
love18-ethology.md&1.8&
love18-ethology.md&2.0&# Patterns of courtship and love
love18-ethology.md&2.0&
love18-ethology.md&2.1&## Three layers of sexual behaviour
love18-ethology.md&2.1&
love18-ethology.md&2.1&>- The cultural layer.
love18-ethology.md&2.1&>- The (oldest) archaic layer of *agonistic* [agon, Greek: fight] sexuality, characterized by male dominance and female submission (“reptilian heritage”).
love18-ethology.md&2.1&>     - This still determines certain aspects of human sexual behavior.
love18-ethology.md&2.1&>- It is controlled by *affiliative* [affiliation, companionship] sexuality, which is mammalian heritage, and by individual bonding (p. 253)
love18-ethology.md&2.1&
love18-ethology.md&2.1&
love18-ethology.md&2.2&## Extended courtship and female reactions (1)
love18-ethology.md&2.2&
love18-ethology.md&2.2&>- The female partner in love with a male typically responds with “coy” resistance.
love18-ethology.md&2.2&>- This is ritualised and forces the man to expend a great deal of effort and time to win his woman.
love18-ethology.md&2.2&>- The greater the effort the male must expend, the more important it is for him to keep that partner at a later time, otherwise the cost of courtship would outweigh the benefits.
love18-ethology.md&2.2&>- The coyer the female partner, the more valuable she becomes in terms of invested courtship efforts, up to an optimal level (p.239)
love18-ethology.md&2.2&>- From this, we are supposed to conclude that humans are meant to have long-term relationships.
love18-ethology.md&2.2&
love18-ethology.md&2.3&## Extended courtship and female reactions (2)
love18-ethology.md&2.3&
love18-ethology.md&2.3&>- The responses of the partner being courted determine the course of courtship.
love18-ethology.md&2.3&>- By her coyness a woman also tests the male's readiness (preparedness) to invest into the relation as well as his characteristics as provider and protector.
love18-ethology.md&2.3&>- The woman’s investment by the physiological burden and risk of pregnancy and birthgiving is much higher than that of the male.
love18-ethology.md&2.3&>- Sex differences in behaviour reflect evolutionary pressures, and that the tactics of self-presentation, criteria for mate choice, and strategies used in the development of relationships correspond to different "ultimate necessities" in both sexes.
love18-ethology.md&2.3&>- *Of what does this remind you?*
love18-ethology.md&2.3&>     - Schiefenhoevel’s romantic behaviour as “honest signal.”
love18-ethology.md&2.3&>     - The unreachable female in courtly love.
love18-ethology.md&2.3&
love18-ethology.md&2.4&## Grammer study 
love18-ethology.md&2.4&
love18-ethology.md&2.4&>- German young adults study: the subjects, who did not know each other before the experiment, were left alone in a room and filmed through a one-way mirror. After 10 minutes, a questionnaire was presented:
love18-ethology.md&2.4&>     - subjective ratings of attractiveness of the partner, 
love18-ethology.md&2.4&>     - the readiness to date the partner,
love18-ethology.md&2.4&>     - and the probability (risk) of acceptance by the partner.
love18-ethology.md&2.4&
love18-ethology.md&2.5&## Results (1)
love18-ethology.md&2.5&
love18-ethology.md&2.5&>- Males show an overall greater willingness to date a female, which is dependent on the rating of the partner's attractiveness in both sexes: the higher the rating of attractiveness, the higher the risk perception and the greater the willingness to date the partner. 
love18-ethology.md&2.5&>- But whether the male initiates contact also depends on his self-esteem. 
love18-ethology.md&2.5&>- If she is too attractive, he might consider his chances low and accordingly refrain from courting in order to save face. 
love18-ethology.md&2.5&>- Only males take their own attractiveness into account for risk perception: the more attractive they rated themselves, the lower the risk they perceive (p.240)
love18-ethology.md&2.5&
love18-ethology.md&2.6&## Results (2)
love18-ethology.md&2.6&
love18-ethology.md&2.6&>- A high rate of laughing signified interest of a woman to join the young man.
love18-ethology.md&2.6&>- Verbal self-presentation varies with perception of risk. Males are more indirect the higher they perceive the risk of female nonacceptance. 
love18-ethology.md&2.6&>     - When perceived risk is high, they avoid the use of “I did, I will.” 
love18-ethology.md&2.6&>     - Instead, they use the inclusive “we,” or even no personal pronouns.
love18-ethology.md&2.6&>- Thus, tactics of male self-presentation depend on risk perception, generated by ratings of attractiveness of the partner. 
love18-ethology.md&2.6&
love18-ethology.md&2.7&## Results (3)
love18-ethology.md&2.7&
love18-ethology.md&2.7&>- Although males take the verbal initiative and present themselves verbally as potential mates, the episodes are structured by the female's nonverbal behavior. 
love18-ethology.md&2.7&>- Females control the structure of the episodes by manipulation of male risk perception and encourage or impede male self-presentation. 
love18-ethology.md&2.7&>- According to biological theories, females show a great degree of selectivity, whereas males tend to advertise overtly due to the pressures of male-male competition.
love18-ethology.md&2.7&
love18-ethology.md&2.8&## Results (4)
love18-ethology.md&2.8&
love18-ethology.md&2.8&>- Nonverbal signs for readiness for contact:
love18-ethology.md&2.8&>     - Raised hands folded behind the neck;
love18-ethology.md&2.8&>     - All kinds of “open” positions: legs open, arms open;
love18-ethology.md&2.8&>     - Visual presentation of secondary sex characteristics;
love18-ethology.md&2.8&>     - Orientation toward the partner (p.240)
love18-ethology.md&2.8&
love18-ethology.md&2.8&
love18-ethology.md&2.9&## Approach strategies
love18-ethology.md&2.9&
love18-ethology.md&2.9&>- Approach strategies have to be subtle if the partners are not yet well acquainted. 
love18-ethology.md&2.9&>- The prominent white eyeball enables us to perceive partners' eye movements easily. 
love18-ethology.md&2.9&>- Eye contact return is a sign of a positive response.
love18-ethology.md&2.9&>- In the succeeding contact conversations the partners test each other's interest in further contact.
love18-ethology.md&2.9&>- If the partners are unacquainted, they will attempt to develop a common reference system. Common interests are determined and compatibility is expressed.
love18-ethology.md&2.9&>- The partners' next step is to construct a basis of trust. One confides in another partner by disclosing one's weaknesses, but always in conjunction with a positive projection of self (p.241)
love18-ethology.md&2.9&
love18-ethology.md&2.10&## Trust and dominance 
love18-ethology.md&2.10&
love18-ethology.md&2.10&>- Dominance displays attempt to achieve a dominant position relative to the other interacting party. 
love18-ethology.md&2.10&>- In courtship display the male attempts to demonstrate that he is in a position to dominate others and thus to protect his partner.
love18-ethology.md&2.10&>- This elevates her esteem for him.
love18-ethology.md&2.10&>- The courtship threat display is meant to impress the partner without being directed at her. Geese show similar behaviours.
love18-ethology.md&2.10&>- Positive self-image can also be displayed through display of material goods and caregiving expressions. 
love18-ethology.md&2.10&>- Also, both partners present themselves through infantile appeals as a suitable object for caregiving which complies with the wish for mutual care (p.242)
love18-ethology.md&2.10&
love18-ethology.md&2.11&## Body contact
love18-ethology.md&2.11&
love18-ethology.md&2.11&>- Body contact is brought about in a noncommital manner. 
love18-ethology.md&2.11&>- In some cultures, rituals offers opportunity for contact, as in dance. 
love18-ethology.md&2.11&>- Often, the man takes the initiative by utilizing a quasi-accidental and harmless contact, as for example laying a shawl over the woman's shoulder or otherwise offering assistance that the partner may invite in a subtle way.
love18-ethology.md&2.11&>- The man usually makes the proposition and the woman makes the decisive selection by accepting or rejecting her suitor.
love18-ethology.md&2.11&
love18-ethology.md&2.12&## Kissing and kiss-feeding
love18-ethology.md&2.12&
love18-ethology.md&2.12&>- Kissing leads to sexual foreplay and this breaches the standards of modesty that exist in all cultures.
love18-ethology.md&2.12&>- These behavioral patterns of physical affection are found in various cultures, even in those that were obviously not influenced by Europeans. 
love18-ethology.md&2.12&>- Kissing is found in Peruvian ceramics dating from pre-Columbian times.
love18-ethology.md&2.12&>- Affectionate kiss feeding is also observed in diverse cultures.
love18-ethology.md&2.12&>     - In the Kamasutra, lovers are described sipping wine mouth-to-mouth.
love18-ethology.md&2.12&>     - Viru lovers (southern highlands of Papua/New Guinea) utilize mouth-to-mouth feeding. 
love18-ethology.md&2.12&>- Embracing, stroking erogenous zones, fondling, and social grooming are all universally practiced.
love18-ethology.md&2.12&
love18-ethology.md&2.13&## Formalised courtship rituals
love18-ethology.md&2.13&
love18-ethology.md&2.13&>- In many cultures these intimate contacts are preceded by formalized courtship rituals, particularly in those cultures that do not permit premarital sex in an effort to prevent illegitimacy. 
love18-ethology.md&2.13&>- In these situations the society arranges partner contacts.
love18-ethology.md&2.13&
love18-ethology.md&2.14&## Formalised courtship rituals
love18-ethology.md&2.14&
love18-ethology.md&2.14&>- In the Medlpa of New Guinea the parents of marriageable girls invite potential husbands to a meeting. 
love18-ethology.md&2.14&>     - The colourfully decorated partners sit in pairs in a communal room. They sing and after introductory head swaying rub foreheads and noses, always twice. Then they bow deeply twice, rub noses two more times, and continue the courtship dance by alternating bowing and rubbing noses.
love18-ethology.md&2.14&>     - The "head rolling" is a courtship dance during which course the partners synchronize their movements. 
love18-ethology.md&2.14&>     - The song itself does not impart the rhythm. The partners develop a common rhythm during the dance.
love18-ethology.md&2.14&>     - The easier they obtain movement synchrony, the better the partners seem to adapt to each other, as synchronization serves to express harmony. 
love18-ethology.md&2.14&>     - Same with dances found in western culture (p.243)
love18-ethology.md&2.14&
love18-ethology.md&2.15&## Sexual modesty
love18-ethology.md&2.15&
love18-ethology.md&2.15&>- Sexual modesty is another cultural universal element of behaviour. 
love18-ethology.md&2.15&>- It is found in all cultures and is expressed in various ways. 
love18-ethology.md&2.15&>- The sexual organs are often covered by clothing. But sometimes the cover is only symbolic.
love18-ethology.md&2.15&>- Yanomami (Amazon tribe) women wear only a thin cord around their waists. But even this cord is symbolic “clothing.” 
love18-ethology.md&2.15&>     - If a Yanomami woman is asked to remove the cord she becomes just as embarrassed as a woman in our culture would be if she were asked to remove her clothing.
love18-ethology.md&2.15&
love18-ethology.md&2.16&## Origins of modesty
love18-ethology.md&2.16&
love18-ethology.md&2.16&>- Modesty demands that one conceal sexual activity from others. A number of attempts have been made to identify the origin of modesty.
love18-ethology.md&2.16&>- Is ti to cover this “unattractive part of the body”? 
love18-ethology.md&2.16&>- To maintain an undisturbed group life?
love18-ethology.md&2.16&>- Human women, unlike other mammalian females, do not have a visible estrus [see next slide]. That certainly facilitates group life through reducing overt sexual competition (p. 246)
love18-ethology.md&2.16&
love18-ethology.md&2.17&## The biological role of sexual modesty
love18-ethology.md&2.17&
love18-ethology.md&2.17&>- A sexual union can provoke attacks, especially by higher ranking male group members. 
love18-ethology.md&2.17&>- They attempt to interrupt copulation with threats and even attacks. 
love18-ethology.md&2.17&>- This is a form of sexual rivalry through which the higher ranking members maintain multiple options for propagating their genes.
love18-ethology.md&2.17&>- Another reason for concealing sexual activity is that during the sexual act a person is so involved with the partner that he cannot perceive the environment accurately and thus is vulnerable (to attacks) (p.246)
love18-ethology.md&2.17&
love18-ethology.md&2.18&## The Oneida community
love18-ethology.md&2.18&
love18-ethology.md&2.18&>- The Oneida community in the United States: founded in 1830, had 500 members at its height.
love18-ethology.md&2.18&>- All possessions, including clothing and childrens' toys, were common property.
love18-ethology.md&2.18&>- The children were raised communally and were taught to love all the adults as if they were their own parents. 
love18-ethology.md&2.18&>- Adults were expected to respond equally to all others; romantic love was considered to be selfish and monogamy was thought to be detrimental to community life. 
love18-ethology.md&2.18&>- While the lack of ownership and a classless society were achieved, the group failed to abolish:
love18-ethology.md&2.18&>     - sexual division of labour, 
love18-ethology.md&2.18&>     - dominance of men over women and children, 
love18-ethology.md&2.18&>     - the formation of individual sexual partnerships, and 
love18-ethology.md&2.18&>    - parent-child bonds.
love18-ethology.md&2.18&> The community was dissolved in 1881 (after about 50 years) (p. 248)
love18-ethology.md&2.18&
love18-ethology.md&2.18&
love18-ethology.md&2.19&## The biological and cultural role of sexual positions
love18-ethology.md&2.19&
love18-ethology.md&2.19&>- Most mammals, including the anthropoid apes, mate with the male mounting the female posteriorly. 
love18-ethology.md&2.19&>- The sexual signals of the female are on her bottom and are presented to the male by displaying the rear. 
love18-ethology.md&2.19&>- During the course of hominization, with the evolution of upright body posture, a new orientation arose with the sexual-releasing signals being transposed to the front of the body.
love18-ethology.md&2.19&>- Humanization: the act whereby the mutual orientation toward each partner's face plays an important role.
love18-ethology.md&2.19&>- Upright posture facilitated this orientation.
love18-ethology.md&2.19&
love18-ethology.md&2.20&## The rear and the breast (1)
love18-ethology.md&2.20&
love18-ethology.md&2.20&>- The breast plays a role as a sexual signal.
love18-ethology.md&2.20&>- The form-giving lipid bodies of the breast are not required for nourishment of the child, but instead give the breast display value.
love18-ethology.md&2.20&>- D. Morris (1968) maintains that the breast became a display organ in conjunction with the development of the upright posture as sexual orientation changed to the front of the woman. 
love18-ethology.md&2.20&>- Morris claims that in humans the breast acts as a mimicry of the posterior, copying the buttocks in form (p. 250)
love18-ethology.md&2.20&
love18-ethology.md&2.20&
love18-ethology.md&2.21&## Other sexual signals (1)
love18-ethology.md&2.21&
love18-ethology.md&2.21&>- E. A. Hess (1977) developed a way to measure preferences using pupillary reactions. 
love18-ethology.md&2.21&>     - The pupils dilate briefly whenever someone sees something interesting and pleasing. 
love18-ethology.md&2.21&>     - Hess constructed an apparatus with which a person's pupil could be filmed while the subject observed slides. 
love18-ethology.md&2.21&>- The pupillary reaction study showed that normal males and females respond to naked women and muscular men, respectively, even if the female subjects claimed they had no interest in muscle men.
love18-ethology.md&2.21&
love18-ethology.md&2.22&## Other sexual signals (2)
love18-ethology.md&2.22&
love18-ethology.md&2.22&>- Hess found that male subjects were divided into two groups. 
love18-ethology.md&2.22&>     - One showed a clear preference for large female breast;
love18-ethology.md&2.22&>     - Others preferred pictures of women with well-developed but not excessively large breasts and in which the women were presented in such a way as to also display the posterior body portion.
love18-ethology.md&2.22&>- Most heterosexual American men conformed to the first type.
love18-ethology.md&2.22&>- Europeans conformed, in general, to the second type. 
love18-ethology.md&2.22&>- However, each group contained a number of individuals that did not conform to the within-group norms.
love18-ethology.md&2.22&
love18-ethology.md&2.23&## Other sexual signals (3)
love18-ethology.md&2.23&
love18-ethology.md&2.23&>- Later Hess found a correlation between these preferences and bottle or breastfeeding. 
love18-ethology.md&2.23&>- Those Americans and Europeans who preferred large breasts had been bottle fed.
love18-ethology.md&2.23&>- One is inclined to speculate that these subjects retained an infantile desire for the maternal breast. 
love18-ethology.md&2.23&>- Those who had been nursed preferred the young woman's breast but not the unusually large ones. 
love18-ethology.md&2.23&>- Homosexuals could also be identified with this test, for they reacted positively to partners of the same sex and not to those of the opposite sex. 
love18-ethology.md&2.23&
love18-ethology.md&2.24&## Beards 
love18-ethology.md&2.24&
love18-ethology.md&2.24&>- While men respond highly to visual stimuli of their female sexual partners, females also respond to such stimuli in men, albeit not to them alone. 
love18-ethology.md&2.24&>- We know that narrow hips, a small firm buttocks, broad shoulders, and, in general, a muscular body are perceived as handsome features in men.
love18-ethology.md&2.24&>- The beard is one of the prominent male secondary sexual characteristics in many races. 
love18-ethology.md&2.24&>- It is less a heterosexual signal and more for its display value for other male group members as a signal of strength and maturity.
love18-ethology.md&2.24&
love18-ethology.md&2.24&
love18-ethology.md&3.0&# Sexuality and domination
love18-ethology.md&3.0&
love18-ethology.md&3.1&## Sexuality and domination (1)
love18-ethology.md&3.1&
love18-ethology.md&3.1&>- Male dominance behavior is closely associated with male sexuality.
love18-ethology.md&3.1&>- In many fishes and some higher vertebrates, courtship is initiated with mutual display behavior.
love18-ethology.md&3.1&>- Pair formation only succeeds when the male is able to dominate his partner.
love18-ethology.md&3.1&>- Male sexuality is thus associated with aggressivity but not with fear. 
love18-ethology.md&3.1&>- Female sexuality is the exact opposite: in lower vertebrates aggressive motivations inhibit sexuality, although flight motivation (fear) does not necessarily suppress it.
love18-ethology.md&3.1&
love18-ethology.md&3.2&## Sexuality and domination (2)
love18-ethology.md&3.2&
love18-ethology.md&3.2&>- In the marine iguana, for example, the courtship of the male consists of a threat display.
love18-ethology.md&3.2&>- Receptive females respond by submission -- by lying flat on their belly, which invites copulation. 
love18-ethology.md&3.2&>- The reptile pattern of agonistic sexuality is thus characterized by a male dominance and a female submission sexuality. 
love18-ethology.md&3.2&>- In birds and mammals, this archaic agonistic sexuality is superceded by a phylogenetically new pattern of affiliative sexuality which in some of them, including man, gave rise to a love relationship based upon individual bonding.
love18-ethology.md&3.2&>- Affiliative sexuality can be traced to the evolution of maternal behaviour.
love18-ethology.md&3.2&
love18-ethology.md&4.0&# Incest
love18-ethology.md&4.0&
love18-ethology.md&4.1&## The role of incest
love18-ethology.md&4.1&
love18-ethology.md&4.1&>- Incest (sexual relations with members of one’s own family) is not advantageous genetically.
love18-ethology.md&4.1&>- First, it leads to offspring that have less genetic variability than possible. Since genetic variation is a good thing (and the only point of sexual reproduction), incestual relations should be avoided for purely genetical reasons.
love18-ethology.md&4.1&>- Second, some hereditary illnesses are more likely to occur when both parents have a particular defective gene, and this probability is obviously higher if both parents are related.
love18-ethology.md&4.1&>- This is why all societies prohibit incest (p.261).
love18-ethology.md&4.1&>- This is interesting, because love as care, union, concern etc (without sex) is generally encouraged within the family (one could say, it is the main point of forming a family). But there is a sharp limit to it where it crosses the line to incest.
love18-ethology.md&4.1&
love18-ethology.md&4.2&## Incest is rare
love18-ethology.md&4.2&
love18-ethology.md&4.2&>- Finkelhor (1980) found that only 2% of students had had sexual contact with family members, and most were between siblings under the age of 13.
love18-ethology.md&4.2&>- Only 3 out of 796 students had attempted intercourse with siblings after the age of 13.
love18-ethology.md&4.2&>- Among 18,000 psychiatric patients, Meiselman (1979) found only 9 instances of parent/child incest.
love18-ethology.md&4.2&
love18-ethology.md&4.3&## Is the incest taboo biological or cultural?
love18-ethology.md&4.3&
love18-ethology.md&4.3&>- Until the 1960s, the prevailing opinion was that the incest taboo is a cultural arrangement (p.261).
love18-ethology.md&4.3&>- In the early 1970s, it was shown that many animals have incest inhibitions. 
love18-ethology.md&4.3&>- Animals, including primates, will not copulate with animals they grew up with.
love18-ethology.md&4.3&>- In human families, close contact during childhood leads to sexual aversion to the other person. (Now you know why you find your brother or sister unbearable!)
love18-ethology.md&4.3&>- Even plants sometimes have mechanisms preventing self-fertilisation.
love18-ethology.md&4.3&
love18-ethology.md&4.4&## Kibbutz upbringing
love18-ethology.md&4.4&
love18-ethology.md&4.4&>- The kibbutz is a kind of communal living environment in Israel.
love18-ethology.md&4.4&>- The idea was to abolish sex and class differences by having all people grow up together.
love18-ethology.md&4.4&>- Boys and girls used the same showers and toilets, in an effort to stop sex discrimination.
love18-ethology.md&4.4&>- This worked until the children reached the age of 12. After that, they avoided contact with the opposite sex.
love18-ethology.md&4.4&>- After puberty, the relations between them were friendly.
love18-ethology.md&4.4&>- But age class members never married each other.
love18-ethology.md&4.4&>- Out of 2769 marriages between persons raised in the kibbutz, not one was between two people who had been brought up together.
love18-ethology.md&4.4&
love18-ethology.md&4.5&## Incest and attraction
love18-ethology.md&4.5&
love18-ethology.md&4.5&Interesting graphic (p.264):
love18-ethology.md&4.5&
love18-ethology.md&4.5&![](graphics/18-incest.png)\
love18-ethology.md&4.5&
love18-ethology.md&4.5&
love18-ethology.md&4.5&
love24-machines.md&0.1&---
love24-machines.md&0.1&title:  "Love and Sexuality: 24. Can one love a machine?"
love24-machines.md&0.1&author: Andreas Matthias, Lingnan University
love24-machines.md&0.1&date: November 24, 2019
love24-machines.md&0.1&...
love24-machines.md&0.1&
love24-machines.md&1.0&# Affective computing and machine emotions
love24-machines.md&1.0&
love24-machines.md&1.1&## Sources (1)
love24-machines.md&1.1&
love24-machines.md&1.1&- Danaher, J., Earp, B. D., & Sandberg, A. (forthcoming). Should we campaign against sex robots? In J. Danaher & N. McArthur (Eds.) Robot Sex: Social and Ethical Implications [working title]. Cambridge, MA: MIT Press. Draft available online ahead of print at:
love24-machines.md&1.1&https://www.academia.edu/25063138/Should_we_campaign_against_sex_robots.
love24-machines.md&1.1&- Levy, D., & Loebner, H. (2007, April). Robot prostitutes as alternatives to human sex workers. In IEEE international conference on robotics and automation, Rome.(http://www. roboethics. org/icra2007/contributions/LEVY% 20Robot% 20Prostitutes% 20as% 20Alternatives% 20to% 20Human% 20Sex% 20Workers. pdf). Accessed (Vol. 14).
love24-machines.md&1.1&- Nitsch, V., & Popp, M. (2014). Emotions in robot psychology. Biological cybernetics, 108(5), 621-629.
love24-machines.md&1.1&
love24-machines.md&1.2&## Sources (2)
love24-machines.md&1.2&
love24-machines.md&1.2&- Picard, R. W. (2004). Toward Machines with Emotional Intelligence. In ICINCO (Invited Speakers) (pp. 29-30).
love24-machines.md&1.2&- Picard, R. W. (1995). Affective Computing. M.I.T Media Laboratory Perceptual Computing Section Technical Report No. 321. Revised November 26, 1995. Available online.
love24-machines.md&1.2&- Sharkey, Wynsberghe, Robbins, Hancock (2017). Our sexual future with robots. A Foundation for Responsible Robotics Consultation Report.
love24-machines.md&1.2&- Sullins, J. P. (2012). Robots, love, and sex: The ethics of building a love machine. IEEE transactions on affective computing, 3(4), 398-409.
love24-machines.md&1.2&- Turkle, S. (2005). Relational artifacts/Children/Elders: the complexities of cybercompanions. Presented at: Cognitive Science Society, July 25-26, Stresa, Italy. Available online.
love24-machines.md&1.2&
love24-machines.md&2.0&# Emotions
love24-machines.md&2.0&
love24-machines.md&2.1&## What is an emotion? (1)
love24-machines.md&2.1&
love24-machines.md&2.1&>- Before we can ask whether robots can have emotions, we need to understand what emotions are. Do you remember some of the theories we talked about?
love24-machines.md&2.1&>- One theory: William James (1884): An emotion is the subjective feeling of a physiological change in the body.
love24-machines.md&2.1&>     - This means that the primary thing that identifies and constitutes an emotion is the physiological change (blood pressure, heartbeat, breathing).
love24-machines.md&2.1&>     - The subjective feeling of that change is secondary.
love24-machines.md&2.1&>     - An emotion is not primarily a mental state but a physiological (body) state that feels in a particular way.
love24-machines.md&2.1&
love24-machines.md&2.2&## What is an emotion? (2)
love24-machines.md&2.2&
love24-machines.md&2.2&>- Another way to define an emotion (more in line with AI) would be a functionalist way.
love24-machines.md&2.2&>- Functionalism (in the philosophy of mind) defines a mental state through its role or function in relation to other mental states.
love24-machines.md&2.2&>     - An emotion is defined as a mental/bodily state that plays a particular role in relation to other mental/bodily states.
love24-machines.md&2.2&>     - Fear, for example, would then be a state that is caused by something in the environment that I consider to be dangerous and that leads to other mental/bodily states, for example the state of running away from the feared thing, elevated blood pressure, screaming etc.
love24-machines.md&2.2&
love24-machines.md&2.3&## What is an emotion? (3)
love24-machines.md&2.3&
love24-machines.md&2.3&>- I could also describe an emotion in a purely *behavioural* way. 
love24-machines.md&2.3&>- A behaviourist would only look at how a system behaves. He would not ask what is happening “inside” the system. So:
love24-machines.md&2.3&>     - Fear is identical to a particular behaviour: screaming, running away, pushing the feared thing away if it comes too close, and so on.
love24-machines.md&2.3&>- How is this different from the functionalist description?
love24-machines.md&2.3&>     - In this description we don’t care about the inner mental states of the subject at all.
love24-machines.md&2.3&>     - It is all about the observable behaviours. Whether there are any particular mental states associated with these behaviours is irrelevant.
love24-machines.md&2.3&
love24-machines.md&2.3&
love24-machines.md&2.4&## What is an emotion? (4)
love24-machines.md&2.4&
love24-machines.md&2.4&>- How would you evaluate these approaches in terms of AI systems? Which descriptions are more/less meaningful when applied to robots?
love24-machines.md&2.4&>- The William James approach is least useful to AI, since it identifies an emotion with a particular physiological reaction.
love24-machines.md&2.4&>     - If we defined emotions like that, then machines could never have similar emotions to ours, since they are constructed in a different way and our physiology does not apply to them.
love24-machines.md&2.4&
love24-machines.md&2.5&## What is an emotion? (5)
love24-machines.md&2.5&
love24-machines.md&2.5&>- The behaviourist approach seems to also be problematic:
love24-machines.md&2.5&>     - If an emotion is defined behaviourally, then there’s nothing else to “having an emotion” than just displaying a particular behaviour. 
love24-machines.md&2.5&>     - This seems too superficial.
love24-machines.md&2.5&>     - Behaviours can be faked (actors on stage), or behaviours can be suppressed.
love24-machines.md&2.5&
love24-machines.md&2.6&## What is an emotion? (6)
love24-machines.md&2.6&
love24-machines.md&2.6&>- The most promising seems to be a functionalist approach.
love24-machines.md&2.6&>- If a machine has internal states that relate to each other as emotional states in humans relate to other states of the human system, then we can describe these internal machine states as “emotion-equivalent.”
love24-machines.md&2.6&>- So if a machine sees something that is justifiably perceived as dangerous to it; and it runs away from that thing, screaming; then we would be justified in ascribing “fear” to the machine.
love24-machines.md&2.6&
love24-machines.md&2.7&## Rationality criteria for emotions
love24-machines.md&2.7&
love24-machines.md&2.7&>- But not every random mental state that leads to some behaviour is an emotion. Justifiable (rational) emotions must fulfil some criteria.
love24-machines.md&2.7&>- See the previous lecture on emotions for more details. Here is a recap:
love24-machines.md&2.7&>     - An emotion has to be reasonable;
love24-machines.md&2.7&>     - it has to fit the situation;
love24-machines.md&2.7&>     - its intensity should be proportional to its object;
love24-machines.md&2.7&>     - it should be in our own long-term best interest;
love24-machines.md&2.7&>     - it must be understandable. We must be able to understand why someone behaves that way. (Smuts, 512)
love24-machines.md&2.7&
love24-machines.md&2.7&
love24-machines.md&2.8&## Basic features of emotions (1)
love24-machines.md&2.8&
love24-machines.md&2.8&>- Emotions are *reactions* to experiences.
love24-machines.md&2.8&>     - Encountering a spider causes fear.
love24-machines.md&2.8&>     - Listening to a nice piece of music causes joy.
love24-machines.md&2.8&>     - As such, they are not directly under our control.
love24-machines.md&2.8&>- Emotions *feel* in a particular way.
love24-machines.md&2.8&>     - Dispositional emotions (for example, a fear of spiders when no spider is present) must first be triggered to be felt.
love24-machines.md&2.8&>- Emotions have two ‘objects’:
love24-machines.md&2.8&>     - The *target*: the thing which the emotion is directed at (the spider, the wild dog, ...)
love24-machines.md&2.8&>     - The *formal object*: the property that we fear in the target (the dangerousness of the spider or wild dog).
love24-machines.md&2.8&
love24-machines.md&2.8&
love24-machines.md&2.9&## Basic features of emotions (2)
love24-machines.md&2.9&
love24-machines.md&2.9&>- Emotions therefore are *felt evaluations.*
love24-machines.md&2.9&>- The function of emotions is to track what matters to us.
love24-machines.md&2.9&>- As evaluations, emotions can be correct or incorrect, rational or irrational.
love24-machines.md&2.9&>- An emotion is justified if “the formal object that it is picking out is actually provided by the target.”
love24-machines.md&2.9&>     - A fear of that dog is justified if that dog is actually dangerous.
love24-machines.md&2.9&>     - A fear of that spider might not be justified if that spider is not dangerous. (Pismenny/Prinz, 2)
love24-machines.md&2.9&
love24-machines.md&2.9&
love24-machines.md&2.10&## What does it mean for AI to “have” emotions?
love24-machines.md&2.10&
love24-machines.md&2.10&There are different ways to understand what it means for a machine to be able to deal with emotions.
love24-machines.md&2.10&
love24-machines.md&2.10&>1. Being the (emotionless) object of a human emotion. Eliciting emotions without “having” them.
love24-machines.md&2.10&>2. Recognising or being aware of human emotions, without “having” them.
love24-machines.md&2.10&>3. Reacting appropriately to human emotions, without “having” them.
love24-machines.md&2.10&>4. Displaying (behaviourally simulating) appropriate emotional responses, without “having” emotions.
love24-machines.md&2.10&>5. Actually “feeling” or “having” an emotion.
love24-machines.md&2.10&
love24-machines.md&2.10&. . . 
love24-machines.md&2.10&
love24-machines.md&2.10&These options are located along an axis from “weak emotional AI” to “strong emotional AI,” in analogy to the weak/strong AI distinction regarding mental states in general.
love24-machines.md&2.10&
love24-machines.md&2.11&## Subjects and objects of emotions (1)
love24-machines.md&2.11&
love24-machines.md&2.11&Similarly, we can distinguish between:
love24-machines.md&2.11&
love24-machines.md&2.11&>- The machine being the object of a human emotion; and
love24-machines.md&2.11&>- The machine being the subject (the carrier) of an emotion.
love24-machines.md&2.11&>- The conditions for these two are very different.
love24-machines.md&2.11&
love24-machines.md&2.12&## Subjects and objects of emotions (2)
love24-machines.md&2.12&
love24-machines.md&2.12&>- Everything can (in principle) be the object of a human emotion. The object doesn’t need to have any particular properties.
love24-machines.md&2.12&>     - People have emotional attitudes (including love and liking) towards children, pets, insects, computers, political parties, football clubs, books, pictures and food.
love24-machines.md&2.12&>     - In this sense, of course they will also have emotions towards robots.
love24-machines.md&2.12&>- But being the subject/carrier of an emotion is much more difficult.
love24-machines.md&2.12&>     - Depending on the description of emotions we have in mind, this can range from simulating emotional responses to actually having a mind. 
love24-machines.md&2.12&
love24-machines.md&2.13&## Can I love a robot?
love24-machines.md&2.13&
love24-machines.md&2.13&>- We have already talked about theories of what love is:
love24-machines.md&2.13&>- 1. Love is a *union* between two partners.
love24-machines.md&2.13&>     - By loving each other, two people fuse their interests and form a union that has new characteristics, different from those of the two separate people (“our” house, children, bank account and so on).
love24-machines.md&2.13&>- 2. Love is *robust concern* for the other person.
love24-machines.md&2.13&>     - What defines a loving relationship is (1) reciprocal, (2) enduring (robust) and (3) unselfish concern for the (4) well-being of the (5) other person.
love24-machines.md&2.13&>- 3. Love is the *result* of having a particular kind of *relationship* to someone. 
love24-machines.md&2.13&>     - In this view, it is not the “falling” in love that is significant, but the “staying” in love. The particular history of a relationship between two people *creates* a bond of love between them.
love24-machines.md&2.13&
love24-machines.md&2.14&## Love as union
love24-machines.md&2.14&
love24-machines.md&2.14&>- We can try to apply these theories to loving a machine:
love24-machines.md&2.14&>- Can I form a *union* of interests with a machine?
love24-machines.md&2.14&>     - Not really. Machines (at least at present) don’t have their own interests. The machine’s interest (for example not to be destroyed) is only relevant because of *my* interest to have that machine working.
love24-machines.md&2.14&>     - So any interest “of the machine” is, in reality, derived from the user’s interests.
love24-machines.md&2.14&>     - Therefore, there can be no union of interests.
love24-machines.md&2.14&
love24-machines.md&2.15&## Love as robust concern
love24-machines.md&2.15&
love24-machines.md&2.15&>- What defines a loving relationship is (1) reciprocal, (2) enduring (robust) and (3) unselfish concern for the (4) well-being of the (5) other person.
love24-machines.md&2.15&>     - “Reciprocal” concern is difficult to imagine. Can a machine be “concerned” about the user’s well-being? Is the user really concerned about *this* individual machine, given that all machines are replaceable?
love24-machines.md&2.15&>     - Is the concern enduring? We normally use machines as means to some end. After the end is achieved, we don’t have a further interest in the machine. For example, does the astronaut have an interest in a rocket’s spent first stage?
love24-machines.md&2.15&>     - Is the user’s concern for a machine truly unselfish?
love24-machines.md&2.15&>     - How do we define well-being for machines? And can a machine judge the well-being of a person independently of its own purpose? Could a sex robot *refuse* to provide sex services if it knew that by doing so it would benefit its user?
love24-machines.md&2.15&
love24-machines.md&2.15&
love24-machines.md&2.16&## Love as relationship
love24-machines.md&2.16&
love24-machines.md&2.16&>- This is perhaps the most promising approach.
love24-machines.md&2.16&>- Users can indeed forge long-term emotional relationships with artefacts (for example, cars, jewellery, old photographs, childhood toys).
love24-machines.md&2.16&>- But the relationship is not reciprocal.
love24-machines.md&2.16&>     - Is one-sided love, even towards humans, still “proper” love?
love24-machines.md&2.16&>     - For example, if someone “loves” a movie star, but the movie star neither knows or cares about that person, is this really “love” or just one-sided admiration?
love24-machines.md&2.16&>     - In the same way, is the “love” towards a toy “real” love? Or is it just “liking,” “admiration” and so on?
love24-machines.md&2.16&
love24-machines.md&2.17&## An Aristotelian view of love (recap)
love24-machines.md&2.17&
love24-machines.md&2.17&>- For Aristotle, love is primarily a form of friendship (“philia”).
love24-machines.md&2.17&>- There are three types of friendship (also applies to love):
love24-machines.md&2.17&>     - Friendship based on usefulness;
love24-machines.md&2.17&>     - Friendship based on pleasure;
love24-machines.md&2.17&>     - Friendship based on the good character of the beloved (“true friendship,” “true love”).
love24-machines.md&2.17&>- The ultimate point of all human activity (including love and friendship) is to make us better (and therefore, happier) persons.
love24-machines.md&2.17&>- Moral action and a good character are the basic components for a good, fulfilled life.
love24-machines.md&2.17&
love24-machines.md&2.18&## Can robots be Aristotelian partners?
love24-machines.md&2.18&
love24-machines.md&2.18&>- A robot can be useful.
love24-machines.md&2.18&>- A robot can be pleasurable (sex robots, entertaining chatbots).
love24-machines.md&2.18&>- But can a robot improve the user’s morality? Can it show a “good moral character” and benefit the user’s moral growth?
love24-machines.md&2.18&>- Presently, it is not clear how it would do that.
love24-machines.md&2.18&>- If the robot deceives the user (as in the Turing test) it is unlikely to instill a love of truthfulness in the user.
love24-machines.md&2.18&>- Only an authentic, truthful, wise and morally good robot could be said to benefit the user’s character.
love24-machines.md&2.18&>- But we don’t (presently?) know how to build such a thing. (This is Sullins’ criticism in the reading).
love24-machines.md&2.18&
love24-machines.md&3.0&# Uses of loving machines
love24-machines.md&3.0&
love24-machines.md&3.1&## Affective computing and love (1)
love24-machines.md&3.1&
love24-machines.md&3.1&>- “Affective” computing is an area of research that studies emotions towards computers and robots and how these robots could react back.
love24-machines.md&3.1&>- Love is not normally a goal of engineering affective computers.
love24-machines.md&3.1&>- Affective computers have many other practical uses. 
love24-machines.md&3.1&>- Some emotions are an important part of a user’s interaction with a machine (satisfaction, frustration, gratitude, hate, rage). It would be good if the machine could recognise such emotions and react to them as part of shaping the user’s experience.
love24-machines.md&3.1&
love24-machines.md&3.1&
love24-machines.md&3.2&## Affective computing and love (2)
love24-machines.md&3.2&
love24-machines.md&3.2&>- Human “love” as an emotional attitude does not seem to require very special capabilities of the target machine.
love24-machines.md&3.2&>     - People love their cars, primitive blow-up sex dolls, tamagotchis, teddy bears and many other things that have no computational power whatsoever. 
love24-machines.md&3.2&>     - They love babies, that also don’t. 
love24-machines.md&3.2&>- So the problem with loving machines is not an issue of affective computing!
love24-machines.md&3.2&
love24-machines.md&3.3&## How can robots recognise emotions? (1)
love24-machines.md&3.3&
love24-machines.md&3.3&There are multiple “channels” through which a computer can gain information about a user’s emotional state (Picard 2004):
love24-machines.md&3.3&
love24-machines.md&3.3&>- Postural movements while seated;
love24-machines.md&3.3&>- Physiology (for example, blood pressure, pupil dilation and so on);
love24-machines.md&3.3&>- Dialogue (the computer can ask the user or observe the user’s dialogue with a third party); 
love24-machines.md&3.3&>- Facial expressions.
love24-machines.md&3.3&
love24-machines.md&3.4&## Facial expressions (1)
love24-machines.md&3.4&
love24-machines.md&3.4&The six basic emotions:
love24-machines.md&3.4&
love24-machines.md&3.4&![](graphics/19-nitsch1.png)\ 
love24-machines.md&3.4&
love24-machines.md&3.4&
love24-machines.md&3.4&
love24-machines.md&3.5&## Facial expressions (2)
love24-machines.md&3.5&
love24-machines.md&3.5&> “The FLOBI humanoid head utilizes so-called babyface cues, consisting of large round eyes, and a small nose and chin. Emotions are primarily conveyed through different configurations of eyebrow, eyelid and lip movements.” (Nitsch 2014)
love24-machines.md&3.5&
love24-machines.md&3.6&## Facial expressions (3)
love24-machines.md&3.6&
love24-machines.md&3.6&![](graphics/19-nitsch2.png)\ 
love24-machines.md&3.6&
love24-machines.md&3.6&
love24-machines.md&3.6&
love24-machines.md&3.7&## Relational agents
love24-machines.md&3.7&
love24-machines.md&3.7&>- Relational agents are computer programs that try to establish long-term relations with the user.
love24-machines.md&3.7&>- “Tricks” to create a relational bond include:
love24-machines.md&3.7&>     - greetings, 
love24-machines.md&3.7&>     - pretending to be happy to see the user again, 
love24-machines.md&3.7&>     - showing concern if the user reports bad news,
love24-machines.md&3.7&>     - subtly changing the style of language over time as the program and the user get more familiar with each other, and 
love24-machines.md&3.7&>     - referencing past interactions. (Picard 2004)
love24-machines.md&3.7&
love24-machines.md&3.8&## Uses of loving machines
love24-machines.md&3.8&
love24-machines.md&3.8&Robots that express love or other feelings and that can relate to the user emotionally would be crucial for:
love24-machines.md&3.8&
love24-machines.md&3.8&>- Childcare
love24-machines.md&3.8&>- Eldercare
love24-machines.md&3.8&>- Medical care
love24-machines.md&3.8&>- Partnership for lonely people
love24-machines.md&3.8&>- ... and perhaps sexual services.
love24-machines.md&3.8&
love24-machines.md&3.8&
love24-machines.md&3.9&## Advantages of loving machines
love24-machines.md&3.9&
love24-machines.md&3.9&>- Relational, love-exhibiting machines could save resources in child- and eldercare and help keep them affordable.
love24-machines.md&3.9&>     - As populations grow increasingly older, there might not be enough young people to pay for the care of the elderly.
love24-machines.md&3.9&>     - Robots can help keep reasonable quality eldercare affordable.
love24-machines.md&3.9&>     - But these robots will need very good relational skills. Nobody wants to age in the company of a vacuum cleaner.
love24-machines.md&3.9&>- Elder-care robots could also keep an eye on people who live alone and provide help (or call for help) in case of accidents in the home.
love24-machines.md&3.9&>- And, finally, they could provide sexual services without the exploitation of other human beings that is part of today’s sex trade.
love24-machines.md&3.9&
love24-machines.md&3.9&
love24-machines.md&4.0&# Some problems of loving machines
love24-machines.md&4.0&
love24-machines.md&4.1&## The authenticity problem (1)
love24-machines.md&4.1&
love24-machines.md&4.1&>- Turkle (2005) points out the *authenticity problem* of robots that exhibit emotional expressions without having the right corresponding affective states (“pretending to love”).
love24-machines.md&4.1&>- She describes how people in elderly care homes interacted with talking doll, overcoming loneliness and even working through issues with their real-life relationships by explaining them to the doll.
love24-machines.md&4.1&>- Children reacted very differently to a robotic pet (“AIBO”):
love24-machines.md&4.1&>     - Some exhibited a detached-biological approach: “This is only a machine, it doesn’t have feelings.”
love24-machines.md&4.1&>     - Some showed an imaginative-maternal approach: they believed that the robot dog has genuine feelings, that it can get ill or die.
love24-machines.md&4.1&>     - Some used the robot dog as a metaphor in order to talk about issues that they could not talk about directly (“life-situating approach”).
love24-machines.md&4.1&
love24-machines.md&4.1&
love24-machines.md&4.2&## The authenticity problem (2)
love24-machines.md&4.2&
love24-machines.md&4.2&Turkle:
love24-machines.md&4.2&
love24-machines.md&4.2&> Authenticity in relationships is a human purpose. So, from that point of view, the fact that our parents, grandparents, and our children might say “I love you” to a robot, who will say “I love you” in return, does not feel completely comfortable and raises questions about what kind of authenticity we require of our technology. Do we want robots saying things that they could not possibly “mean?” 
love24-machines.md&4.2&
love24-machines.md&4.2&
love24-machines.md&4.3&## The authenticity problem (3)
love24-machines.md&4.3&
love24-machines.md&4.3&> Robots might, by giving timely reminders to take medication or call a nurse, show a kind of caretaking that is appropriate to what they are, but it’s not quite as simple as that. Elders come to love the robots that care for them, and it may be too frustrating if the robot does not say the words “I love you” back to the older person, just as we can already see that it is extremely frustrating if the robot is not programmed to say the elderly person’s name. These are the kinds of things we need to investigate, with the goal of having the robots serve our human purposes.
love24-machines.md&4.3&
love24-machines.md&4.4&## The authenticity problem (4)
love24-machines.md&4.4&
love24-machines.md&4.4&>- Sullins (2012): The problem is that love, and particularly philia (friendship), are also meant to make us better as persons (Aristotle).
love24-machines.md&4.4&>- Love has a moral component. Robots can never fulfil this essential function of love and friendship.
love24-machines.md&4.4&>- In Aristotelian terms, robots could be friends from usefulness or pleasure, but they could never be genuine, complete friends.
love24-machines.md&4.4&
love24-machines.md&4.5&## The uncanny valley problem
love24-machines.md&4.5&
love24-machines.md&4.5&>- Although robot likeability increases as robots become more human-like in appearance and behaviour, there is a point when robots become too similar to humans, but not perfectly human-like and likeability drops suddenly.
love24-machines.md&4.5&>- This is called the “uncanny valley” of robotics (Mori M. (1970) The Uncanny Valley. In: Energy 7 (4), pp 33–35.)
love24-machines.md&4.5&>- The Uncanny Valley is the perception that “something is off” with a human-like thing’s behaviour. 
love24-machines.md&4.5&>- The *expectations* for the thing’s behaviour that are triggered by its (human-like) appearance are *not satisfied by the actual behaviour* of the thing.
love24-machines.md&4.5&>     - Example: Corpses, very lifelike baby dolls, humanoid robots.
love24-machines.md&4.5&>     - Stone statues don’t create this effect, because when we see that they are just made of stone, we don’t create expectations of human-like behaviour for them.
love24-machines.md&4.5&
love24-machines.md&4.5&
love24-machines.md&4.6&## Bunraku puppets
love24-machines.md&4.6&
love24-machines.md&4.6&![](graphics/19-bunraku.jpg)\ 
love24-machines.md&4.6&
love24-machines.md&4.6&
love24-machines.md&4.6&
love24-machines.md&4.6&
love24-machines.md&4.7&## Sophia the robot
love24-machines.md&4.7&
love24-machines.md&4.7&![](graphics/19-sophia.jpg)\
love24-machines.md&4.7&
love24-machines.md&4.7&
love24-machines.md&4.7&
love24-machines.md&4.7&
love24-machines.md&4.8&## The Nao robot (a counter-example)
love24-machines.md&4.8&
love24-machines.md&4.8&![](graphics/19-nao.png)\ 
love24-machines.md&4.8&
love24-machines.md&4.8&
love24-machines.md&4.8&
love24-machines.md&4.9&## Uncanny valley
love24-machines.md&4.9&
love24-machines.md&4.9&![](graphics/19-uncanny.png)\ 
love24-machines.md&4.9&
love24-machines.md&4.9&
love24-machines.md&4.9&
love24-machines.md&4.10&## Factors causing the phenomenon
love24-machines.md&4.10&
love24-machines.md&4.10&>- When an artefact does not look human-like at all, we have no expectations regarding its behaviour.
love24-machines.md&4.10&>- But as the artefact becomes more similar to humans, we automatically tend to expect it to behave in a “human-like” way.
love24-machines.md&4.10&>- The more human-like it looks, the more it needs to behave like a human.
love24-machines.md&4.10&>- A robot that looks very much like a human (like Sophia) but does not behave “right,” is alarming, in the same way like a human would be if he behaved in an off way.
love24-machines.md&4.10&>- We are ourselves sensitive to odd behaviour from others, that is a signal of possible danger. So we perceive an oddly behaving robot as dangerous.
love24-machines.md&4.10&>- This is not the case for a robot that looks like a toy, because there our innate expectations about what behaviour would be “odd” don’t work.
love24-machines.md&4.10&
love24-machines.md&4.11&## What can be done about the uncanny valley (1)
love24-machines.md&4.11&
love24-machines.md&4.11&>- How can the effect be mitigated?
love24-machines.md&4.11&>- First, we could stop trying to make robots human-like in appearance. 
love24-machines.md&4.11&>     - For most applications, a rough approximation of the human form, or even a pet shape, would be sufficient and would not cause the uncanny valley problem.
love24-machines.md&4.11&>     - For example, the toys in the Toy Story movies don’t cause this problem.
love24-machines.md&4.11&
love24-machines.md&4.12&## What can be done about the uncanny valley (2)
love24-machines.md&4.12&
love24-machines.md&4.12&>- Second, we could try to make robots behave in more human-like ways.
love24-machines.md&4.12&>     - If we use increasingly refined neural networks to control a robot’s movement, it is likely that the effect of the uncanny valley will be overcome at some point, as robots become sufficiently human-like to not cause discomfort to the observer.
love24-machines.md&4.12&>     - A good example are deceased, digitally recreated actors in Hollywood movies. If done right, they look perfectly human and don’t cause any problem.
love24-machines.md&4.12&
love24-machines.md&4.13&## What can be done about the uncanny valley (3)
love24-machines.md&4.13&
love24-machines.md&4.13&>- Third, humans get used to everything.
love24-machines.md&4.13&>     - The uncanny valley might be a temporary effect, because right now we are not used to seeing “almost-human” artefacts.
love24-machines.md&4.13&>     - We know that people had similar problems when the first railroads were built, and they wondered whether anyone could survive travelling at 50 or more kilometres per hour. Today we have no problem travelling at 900 km/h in commercial airplanes.
love24-machines.md&4.13&>     - If the uncanny valley is a problem of the mismatch between expected and actual behaviour, then if our expectations are adjusted (due to increasing familiarity with robots) the problem might disappear.
love24-machines.md&4.13&
love24-machines.md&4.14&## The degradation of love problem (1)
love24-machines.md&4.14&
love24-machines.md&4.14&>- One could argue that calling what machines do “love” degrades the concept of love.
love24-machines.md&4.14&>- Particularly Aristotelians would insist that, since robots lack human feelings, wisdom and personality, they can never be adequate partners for human friendship or love.
love24-machines.md&4.14&>- Calling such superficial relationships “love” makes us lose sight of what love really is (or should be).
love24-machines.md&4.14&
love24-machines.md&4.15&## The degradation of love problem (2)
love24-machines.md&4.15&
love24-machines.md&4.15&>- *Do you agree? What could we say about this?*
love24-machines.md&4.15&>     - For one, love is an incredibly complex phenomenon, even where no robots are involved. 
love24-machines.md&4.15&>     - Not all humans love in the same way. Not all human relationships are deep and meaningful.
love24-machines.md&4.15&>     - Humans also have (and always had) superficial relationships, sex without love, one-sided love, obsessive love, jealousy and all kinds of other “bad love” phenomena. 
love24-machines.md&4.15&>     - It is not clear that robot love is worse than some bad forms of human “love” have been.
love24-machines.md&4.15&
love24-machines.md&4.16&## The degradation of love problem (3)
love24-machines.md&4.16&
love24-machines.md&4.16&>- Second, you could say that we already have a degradation of the concepts of love and friendship through dating sites, facebook and so on.
love24-machines.md&4.16&>     - A facebook “friend” is not a “friend” in the traditional sense of the word.
love24-machines.md&4.16&>     - A dating experience that begins with swiping pictures of people right and left on a phone screen and ends with a meaningless date is also not much of a “love” experience.
love24-machines.md&4.16&>- Technology has already appropriated these words and changed their meaning.
love24-machines.md&4.16&>- But of course, this alone does not justify watering down these concepts further.
love24-machines.md&4.16&
love24-machines.md&4.17&## The social isolation problem (1)
love24-machines.md&4.17&
love24-machines.md&4.17&>- Will intimacy with robots lead to greater social isolation?^[The following discussion is from Sharkey 2017.]
love24-machines.md&4.17&>- Sullins (2012): “These machines will not help their users form strong friendships that are essential to an ethical society.”
love24-machines.md&4.17&>- Whitby (2011): “An individual who consorts with robots, rather than humans, may become more socially isolated.” 
love24-machines.md&4.17&>- Turkle (2011): Real sexual relationships could become overwhelming because relations with robots are easier.
love24-machines.md&4.17&>- Snell (1997): For the same reason, sex with robots could become addictive.
love24-machines.md&4.17&
love24-machines.md&4.18&## The social isolation problem (2)
love24-machines.md&4.18&
love24-machines.md&4.18&>- Kaye (2016): Sexual relations with robots will "desensitise humans to intimacy and empathy, which can only be developed through experiencing human interaction and mutual consenting relationships." 
love24-machines.md&4.18&>- Vallor (2015): Moral and social deskilling, which can lead to an inability to form social bonds. 
love24-machines.md&4.18&>- In a study about robots in the home, Dautenhahn et al. (2005) found that although 40% of participants were in favour of the idea of having a robot companion in the home, they mostly saw their role as being an assistant, machine or servant. Few were open to the idea of having a robot as a friend or mate.
love24-machines.md&4.18&
love24-machines.md&4.19&## The social isolation problem (3)
love24-machines.md&4.19&
love24-machines.md&4.19&Sullins:
love24-machines.md&4.19&
love24-machines.md&4.19&> Computing technology is such a compelling surrogate for human interaction because it is so malleable to the wishes of its user. If it does not do what the user wants, then a sufficiently trained user can reprogram it or fix the issue to make the machine perform in line with the user’s wishes. ... Fellow humans, on the other hand, represent a much more difficult problem and do not always readily change to accommodate one’s every need. They provide resistance and have their own interests and desires that make demands on the other person in the relationship. Compromise and accommodation are required and this is often accompanied by painful emotions.
love24-machines.md&4.19&
love24-machines.md&4.19&
love24-machines.md&4.20&## The social isolation problem (4)
love24-machines.md&4.20&
love24-machines.md&4.20&>- Graaf and Allouch found that (Sharkey 2007):
love24-machines.md&4.20&>     - 20.5% think that companion robots could decrease loneliness; 
love24-machines.md&4.20&>     - 14.3% said that robot companions could increase social deprivation or isolation; 
love24-machines.md&4.20&>     - 38.4% thought that there would be no positive consequences from using them.
love24-machines.md&4.20&>- But maybe this is all too negative. There are already example of people taking their sex dolls with them to bars (Sharkey 2017, 20). 
love24-machines.md&4.20&>- Perhaps people will learn to socially accept sex robots as actual partners, in the same way as society has got accustomed to divorce, sex before marriage, contraception, and homosexuality, which all were once thought to be not socially acceptable.
love24-machines.md&4.20&>- If there was no social stigma attached to robots partners, then perhaps they would not lead to social isolation.
love24-machines.md&4.20&
love24-machines.md&5.0&# Is robot prostitution immoral?
love24-machines.md&5.0&
love24-machines.md&5.1&## The (im-)morality of human prostitution (1)
love24-machines.md&5.1&
love24-machines.md&5.1&>- Although prostitution is morally accepted and legal in some countries, in others it is considered immoral and/or illegal. Can you think of reasons why someone would consider prostitution immoral?
love24-machines.md&5.1&>     - Prostitution harms, exploits and demeans women; 
love24-machines.md&5.1&>     - It leads to the spread of sexual diseases;
love24-machines.md&5.1&>     - It fuels drug problems;
love24-machines.md&5.1&>     - It can lead to an increase in organised crime;
love24-machines.md&5.1&>     - It breaks up relationships; and more (Levy 2007).
love24-machines.md&5.1&
love24-machines.md&5.2&## The (im-)morality of human prostitution (2)
love24-machines.md&5.2&
love24-machines.md&5.2&>- But we could also see the beneficial aspects of prostitution:
love24-machines.md&5.2&>     - In a well-organised welfare state, prostitution can be regulated by law, eliminating exploitation and the danger of diseases;
love24-machines.md&5.2&>     - Prostitutes can teach the sexually inexperienced how to become better lovers;
love24-machines.md&5.2&>     - Prostitution can help alleviate loneliness, stress and tension;
love24-machines.md&5.2&>     - It provides sex without commitment for those who want it.
love24-machines.md&5.2&
love24-machines.md&5.3&## Robot prostitution
love24-machines.md&5.3&
love24-machines.md&5.3&>- We can see that most of the objections don’t really apply (at least not directly) to robot sex:
love24-machines.md&5.3&>     - Robot prostitution does not (directly) harm, exploit and demean women; 
love24-machines.md&5.3&>     - It does not lead to the spread of sexual diseases;
love24-machines.md&5.3&>     - It does not fuel drug problems;
love24-machines.md&5.3&>     - There is no obvious connection to organised crime;
love24-machines.md&5.3&>     - It probably won’t break up any relationships.
love24-machines.md&5.3&
love24-machines.md&5.4&## Demeaning women?
love24-machines.md&5.4&
love24-machines.md&5.4&>- Although “female-like” sex robots don’t directly involve real women, can we still say that they demean women? How?
love24-machines.md&5.4&>     - You can demean someone without their involvement. 
love24-machines.md&5.4&>     - For example, a drawing of a person can be used to demean or attack them. 
love24-machines.md&5.4&>     - A doll that looks like someone can be used to demean the target person.
love24-machines.md&5.4&>     - A written story about someone can be used to demean them.
love24-machines.md&5.4&>     - In these cases, the physical presence of the target is not required.
love24-machines.md&5.4&>- Similarly, we can argue that using a sex robot that “looks like” population segment X (for example, women) promotes the objectification of that population segment and demeans them.
love24-machines.md&5.4&
love24-machines.md&5.4&
love24-machines.md&5.5&## Why men visit prostitutes
love24-machines.md&5.5&
love24-machines.md&5.5&Levy (2007) provides a list of reasons why men visit prostitutes:
love24-machines.md&5.5&
love24-machines.md&5.5&>- Variety of the sexual experiece;
love24-machines.md&5.5&>- Lack of complications and constraints;
love24-machines.md&5.5&>- Lack of success with the opposite sex.
love24-machines.md&5.5&>- Robots can provide these services and thus be beneficial to particular members of society.
love24-machines.md&5.5&
love24-machines.md&5.6&## Ethics of robot prostitution (1)
love24-machines.md&5.6&
love24-machines.md&5.6&Levy (2007) argues that there are a few ethical considerations involving sex robots:
love24-machines.md&5.6&
love24-machines.md&5.6&>- *Making robot prostitutes available for general use.*
love24-machines.md&5.6&>- If this was a problem, Levy says, then why does society not object to the sale of vibrators for women, which have the same purpose?
love24-machines.md&5.6&>- Do you agree that this is a good analogy?
love24-machines.md&5.6&>     - One could argue that vibrators don’t look like men, and that, therefore, they don’t objectify men in the same way as a full-sized, moving and speaking “female-like” robot.
love24-machines.md&5.6&
love24-machines.md&5.7&## Ethics of robot prostitution (2)
love24-machines.md&5.7&
love24-machines.md&5.7&>- *Justifying the use of sex robots to society in general.*
love24-machines.md&5.7&>- On the one hand, Levy says, it might be easier to justify using a sex robot that is not alive, than a human prostitute who is.
love24-machines.md&5.7&>- On the other, some states in the US (and probably some counties elsewhere too) have laws against the sale of vibrators.
love24-machines.md&5.7&>- It is probable that such places will outlaw sex robots too.
love24-machines.md&5.7&
love24-machines.md&5.8&## Ethics of robot prostitution (3)
love24-machines.md&5.8&
love24-machines.md&5.8&>- Robots might take part in the sexual life of a couple without the usual problems that are associated with human third parties (infidelity, jealousy, fear of indiscretion and diseases).
love24-machines.md&5.8&>- This will depend on the sexual morality and habits of individual couples, but one can think of many uses of sex robots that enrich the sex life of couples without causing any harm.
love24-machines.md&5.8&
love24-machines.md&5.9&## Ethics of robot prostitution (4)
love24-machines.md&5.9&
love24-machines.md&5.9&>- Human prostitutes might lose their jobs.
love24-machines.md&5.9&>- On the one hand, this might be considered a good thing, since prostitution is generally seen as a degrading and dangerous job that is performed in a bad environment and often associated with drugs and exploitation. 
love24-machines.md&5.9&>- On the other hand, prostitution is often the “last resort” job for people who have no other options. Putting prostitutes out of work might make it impossible for them to earn a living in any other way.
love24-machines.md&5.9&
love24-machines.md&5.10&## Ethics of robot prostitution (5)
love24-machines.md&5.10&
love24-machines.md&5.10&>- *Is this a good argument to keep prostitution going?*
love24-machines.md&5.10&>     - It doesn’t seem to be. With the same argument, one might want to protect killers’ or street drug dealers’ jobs. Often these too don’t have other options.
love24-machines.md&5.10&>     - The solution is not to let them do their (immoral, degrading, exploitative, dangerous) job, but to create a society in which all citizens have better options to earn a living.
love24-machines.md&5.10&>     - In the last resort, a society could provide sufficient unemployment benefits to people who are unable to find a job, so that nobody is forced to work as a prostitute or killer.
love24-machines.md&5.10&
love24-machines.md&5.11&## Advantages of sex robots for society
love24-machines.md&5.11&
love24-machines.md&5.11&>- “Many who would otherwise have become social misfits, social outcasts, or even worse will instead be better balanced human beings.” (Levy).
love24-machines.md&5.11&>- If sex robots really have this effect, they should be considered a therapeutic tool (Sharkey 2017).
love24-machines.md&5.11&>- Dolls might be used in care homes as companions (like we saw above in the Turkle paper). In principle, they might also provide additional, sexual services.
love24-machines.md&5.11&>- UK Human Rights Act 1998 and Equality Act 2010: It is illegal not to support disabled people to enjoy the same pleasures as others enjoy in the privacy of their own homes. (Sharkey 2017)
love24-machines.md&5.11&
love24-machines.md&5.12&## Could sex robots reduce sex crimes? (1)
love24-machines.md&5.12&
love24-machines.md&5.12&>- Could sex robots help reduce sex crimes?
love24-machines.md&5.12&>- Some sexual preferences cause harm to others or the person who has the preference themselves: voyeurism, exhibitionism, paedophilia. 
love24-machines.md&5.12&>- Would we accept a robot child substitute as a solution that satisfies a paedophile and keeps him or her from assaulting real children?
love24-machines.md&5.12&>- Ronald Arkin, robotics professor at the Georgia Institute of Technology: “people should not only legally be permitted to have such dolls, but perhaps some should be handed prescriptions for them. [Sex robots] might function as an outlet for people to express their urges, redirecting dark desires toward machines and away from real children.” (Sharkey 2017, 26).
love24-machines.md&5.12&
love24-machines.md&5.13&## Could sex robots reduce sex crimes? (2)
love24-machines.md&5.13&
love24-machines.md&5.13&>- Peter Fagan from the John Hopkins School of Medicine is sceptical that there ever will be therapeutic use for sex robots. [Such robots] would likely have a “reinforcing effect” on paedophilic ideation and “in many instances, cause it to be acted upon with greater urgency.” (Morin, 2016, cited after Sharkey 2017)
love24-machines.md&5.13&
love24-machines.md&5.13&
love24-machines.md&5.14&## Could sex robots reduce sex crimes? (3)
love24-machines.md&5.14&
love24-machines.md&5.14&Philosophy professor and robot ethicist Patrick Lin (California Polytechnic):
love24-machines.md&5.14&
love24-machines.md&5.14&> Treating paedophiles with robot sex-children is both a dubious and repulsive idea. Imagine treating racism by letting a bigot abuse a brown robot. Would that work? Probably not. If expressing racist feelings is a cure for them, then we wouldn’t see much racism in the world. (Sharkey, 2017)
love24-machines.md&5.14&
love24-machines.md&5.14&
