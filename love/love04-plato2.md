---
title:  "Love and Sexuality: 4. Plato’s theory of erotic love and Irving Singer’s criticism"
author: Andreas Matthias, Lingnan University
date: September 1, 2019
...

# Plato’s views on love

## Diotima’s ladder

>- Love of beautiful bodies, to beautiful works, to beautiful institutions, to beautiful knowledge, and finally to a glimpse of Beauty itself.
>- The text of the Symposion itself illustrates Diotima’s ladder:
>     - It begins with the bodily aspects of love (Phaedrus, Pausanias, Eryximachus).
>     - It goes into philosophy and the search for truth (Aristophanes: union, Agathon: beauty and virtue).
>     - It ends with Socrates, who is himself ugly (and therefore not a suitable object of cheap, bodily love), but he is a symbol of the much more important love of wisdom, which is true eros.
>     - Alcibiades, at the end, illustrates the point by declaring his love of Socrates. He, the attractive young man, is himself attracted by Socrates’ inner qualities rather than the philosopher’s appearance.

## Erotic love in ancient Greece

>- “Platonic” love is not what we today think when we say that word.
>- Homoeroticism was common and considered the highest kind of love.
>- But it co-existed with other kinds of love: Socrates was married and Alcibiades had many affairs with women.

## Sexual identity (from: Cooksey, Plato’s Symposium: A Reader’s Guide)

>- “Sexual identity was understood in terms of the role in the sexual act, whether the partner was male or female.”
>- “The masculine, as distinct from the male, was defined as the active part in the sexual relationship, and the partner as the passive part, independently of biological sex.”
>- “The non-masculine partner was anyone ‘inferior’ to the masculine, whether in age, gender, or social status.”
>- “Thus normal masculine behavior might focus on a younger man, a woman, or a slave. A boy might assume the passive role with an older man, but the active role with a younger boy.”
>- The older man, the erastes (lover), was subject to desire (eros), while the boy, the eromenos (beloved) was the object of that desire, supposedly motivated by a mixture of admiration, gratitude, and affection.” (Cooksey, Plato’s Symposium, 12)

## Ancient Greek eros is not symmetrical

>- An erotic relationship is not symmetrical.
>- There is the erastes (lover), the older, wiser man, who leads the relationship. 
>- And the eromenos (beloved), who is usually a boy, and receives both the older man’s erotic attention and his protection and guidance in society. 
>- A more (but not perfectly) symmetric relationship would be philia (friendship). See Aristotle for details (later in this class).

## The word ‘erosic’

>- Alan Soble uses the word “erosic” (from Greek ‘eros’), to denote Plato’s version of love.
>- In this way, it is not confused with the modern usage of the word “erotic.”

# Plato: Universals and ideas

## Love as desire (1)

>- Plato: “Love is desire for the perpetual possession of the good.”
>- In principle, all desires are desires for something considered “good.” Otherwise why would we desire these things that are not good?
>- But we might be mistaken about what we really want.
>- In Diotima’s ladder, the lover who desires the beauty of the body is simply mistaken about his desire.
>- Similarly, when we chase after money, what we want is not the money itself, but the happiness that (we hope) will result from being wealthy.
>- But this might be a mistake. If material wealth didn’t promote happiness, then chasing after it would be counter-productive.
>- The role of philosophy is to help us clarify what our ultimate desires are, so that we can direct our efforts towards our ultimate good.  (Irving Singer I,53 ff.)

## Love as desire (2)

>- It is important to see that, for Plato, love for a person is just a special case of a more general “love”: admiration for natural beauty, fascination with social order and ritual, love for wisdom.
>- Some loves are “lower” (physical attraction to one’s boy- or girlfriend) and some are of “higher” quality (the love of the good, the love of perfect ideas). But both are variations of the same basic phenomenon.

## Perfect circles

>- Consider a circle drawn onto a piece of paper.
>- This certainly will not be a *true* circle.
>- It will be irregular at places, perhaps it will not even close properly.
>- But that doesn’t matter. We can easily recognise that it is “supposed to be a circle.”
>- Even mechanically drawn circles, or circles on a computer screen are never absolutely true and perfect circles.
>- So how do we know what a circle is if we have never really seen one?
>- Plato: all those specific examples of (imperfect) circles partake in the Idea (capital I) or Form (capital F) of a perfect circle.

## Forms (Ideas)

>- These perfect Forms of things are outside the realm of physical things.
>- No physical circle can be perfect. No physical, existing tree is a perfect example of a tree. Existing things are always deficient in some way.
>- Forms are perfect and they exist in a world of their own. The world of perfect things.
>- The world of perfect things (Forms) is not the physical world, and it is also not just a world of the mind. For Plato, Forms are real, but *somewhere else.*


## Forms and reality

>- The physical world is a bad, imperfect picture of the world of Forms, in the same way as the shadow of a horse is an imperfect image of that horse.
>- We live like people who are sitting in a cave, looking only at shadows on the cave wall. We know the real world (the world of Forms) only through its shadows (the physical things in our world).
>- As an example of the cleverness of the Symposion’s construction, you could see the five speeches at the beginning as imperfect images or shadows of Diotima’s perfect explanation of the ladder of love.

## The Symposion itself as a metaphor

>- Thus the whole text provides an illustration of Plato’s theory of Forms: 
>     - Diotima is the absent, not physically present participant. She is in another than the physical realm and she has the perfect knowledge of love.
>     - The other speakers are physically (!) present and they have only faint images of the whole picture.
>     - It is only Socrates, the wisest of them, who can approach Diotima’s wisdom and explain it to the others, make it accessible. 
>     - But the eternal Form, Diotima herself, stays out of reach and out of the physical world of the Symposion.

## Forms/Ideas (Singer)

>- “Existence is merely the actualization of a form; the form, however, is real whether or not anything does exist.” (Singer, I,58)
>- Properties of forms (Singer, I, 58):
>     - Forms are “eternal” (that is, independent of time and space).
>     - They neither come into being nor pass away (for they do not [materially] exist and cannot therefore cease to exist).
>     - They don’t vary with the beholder (because they simply are what they are, the abstract green-ness or tree-ness).
>     - They are unique (there can be only a single essence for each type of entity).
>     - They are such that actual things partake of them (for all things that exist must be instances of some universal).


#  Plato’s influence and other notes

## Plato and Christianity

There are obvious parallels between Plato and basic Christian beliefs. Consider Plato:

. . . 

>1. Love is desire.
>2. All desires for lesser things are really part of a great “ladder” that leads to the ultimate desire for the (morally) good and beautiful.
>3. This love is not limited to men, but part of the plan of nature.
>4. Men might not always recognize this, but it is the truth, and this truth can be revealed by the study of philosophy.
>5. Love for the idea of perfect beauty and goodness is the highest form of love (all other forms of love are parts of it and lead towards it.)
> 6. Love strives for perpetual possession of the good. Since we are mortal, procreation, creativity, and the study of eternal things are the closest ways we can come to the perpetual possession of the good.

## Christian version

>1. Love is desire (for God).
>2. All desires for lesser things are really part of a great “ladder” that leads to the ultimate desire for the moral perfection and beauty of God.
>3. This love is not limited to men, but part of the whole plan of nature that strives towards God. (...)
>4. Men might not always recognize this, but it is the truth, and this truth can be revealed by the study of philosophy (or God’s revelation in the Bible).
>5. Love for God as the embodiment of perfect beauty and goodness is the highest form of love (all other forms of love are parts of it and lead towards it.)
>6. Love strives for perpetual possession of the good. Since we are mortal, the perpetual possession of the good (contemplation of God in eternity) will occur in the afterlife.^[This comes from somewhere, but I’ve lost the reference.]

## Singer’s criticism of merging and wedding unions

>- Plato says that love is based on the desire to be “unified” with the good (compare Aristophanes’ contribution: the halves looking for their other half). 
>- The fulfilment of love is union (with the other half, or, ultimately, with the idea of the good).
>- Singer (p.65ff) distinguishes between two kinds of union:
>     - Wedding: the “union” of meat and rice in a dish.
>     - Merging: the “union” of tea and sugar in a hot tea.
>- A “wedding” union retains the characteristics of the parts.
>- A “merging” union dissolves the individual parts into a new whole.
>- Merging; Mixing of paint into a new colour. Wedding: the union of members of a family, or colleagues in a company.

## Wedding and merging

>- Is Platonic eros an instance of merging or wedding?
>- For physical love, it must be a wedding. Alcibiades and Socrates don’t physically merge. They don’t lose their individual identities.
>- But for the love of perfect beauty, it must be a kind of merging. 
>- Like in the Aristophanes myth, the philosopher remains “in constant union” (Plato) with the good. The true lover becomes god-like and immortal himself, Plato says. This is so, because he *unites* with the absolute good.
>- But if this is a rational, philosophical process, is this likely? Thinking requires a distance, a rational agent. We would imagine a mystic or a shaman to *merge* with God, but not a philosopher.
>- In Plato’s system, it is the *rational* recognition of the good that leads the philosopher up Diotima’s ladder. It is not a mystical process. 

## Love and emotion

>- How would Plato judge a love that is based on feelings (emotions) towards the loved person?
>- He would say that this is essentially a mistaken form of love. It is an early stage of love, before one has ascended Diotima’s ladder. 
>- When one matures as a lover of beauty, feelings towards particular persons have to be left behind, to make the contemplation of absolute beauty possible.
>- This makes Platonic eros look quite strange as a kind of love.

## Platonic exclusivity?

>- What would Plato think of the idea of monogamy (marrying one partner only) or the romantic notion that there is the one perfect partner for everyone? (As in Aristophanes’ myth: the original other half.)
>- He would say that this is nonsense.
>- Erosic love is not limited to any one object of love.
>- Instead, all individual objects of love are just steps in a ladder that leads up the final, single, *one* object of love. The steps don’t matter in themselves.

## Appraisal and bestowal

>- Is Platonic love “appraisal” or “bestowal” love?
>- Platonic love is not “bestowal” love. The Platonic lover seeks the absolute perfection that is there in eternity. It is not created or altered in any way by the act of loving itself! 
>- Platonic love is purely an act of appraisal of the eternal beauty as it shows itself in material things first, and in ideas later, and finally in the contemplation of the eternal beauty itself.
>- Any problems with this idea?
>- This means that we can never really love any actual human.
>- *All humans are only ‘apparent’ objects of love* (Singer I,69).
>- They *seem* or *appear* to be what we love, but this appearance is mistaken. We love the Idea instead (even if we don’t know it).

## Devaluing of human love

>- This position seems to devalue actual human love.
>- If we never love any actual human being, then human beings become replaceable, irrelevant in themselves.
>- Which other ancient school of philosophy has a similar understanding of love, and sees such an impersonal love also as the “proper” kind of love?
>- The Stoics.
>     - They have a whole theory about how it is wise to love only “kinds of things” instead of “individual things,” and how this is considered to be the best type of love (see Epictetus, Enchiridion).
>     - The Stoics emphasize the possible loss of the loved thing or person. 
>     - The only object of love that can’t be lost is the idea of the loved thing, or a whole class of things, of which each can replace every other, like beautiful bodies replace one another at the lower stages of Plato’s ladder of love.

## Plato today

>- What would Plato’s say about erotic love in today’s sense (that means, bodily, sexual love)?
>- Sexual love is at the low stages of Diotima’s ladder.
>- It is a passion that should be overcome in order for the lover to reach the higher forms of love.
>- This is strange if we consider the modern use of “erotic.” We could say that Plato is ultimately anti-erotic or non-erotic.

## Summary of Singer’s criticism

Singer I,73:

> “Platonism [...] ignores so much that is important to love: not only the bestowal of value, which is not reducible to reason, but also such feelings as tenderness, warmth, and that caring about in which bestowing manifests itself. Moreover, the emphasis upon sight tends to eliminate physical intimacy. The whole question of sexual relations seems to be pushed aside.”

## The End. Thank you for your attention!

Questions?



<!--

%% Local Variables: ***
%% mode: markdown ***
%% mode: visual-line ***
%% mode: cua ***
%% mode: flyspell ***
%% End: ***

-->
