---
title:  "Love and Sexuality: 15. Justifying love"
author: Andreas Matthias, Lingnan University
date: September 1, 2019
...

# Justifying love

## Sources

>- Edyvane, D. (2003). Against unconditional love. Journal of Applied Philosophy, 20(1), 59-75.
>- Frankfurt, H. (2004). The Reasons of Love. Princeton: Princeton University Press.
>- McKeever, N. (2018). What Can We Learn about Romantic Love from Harry Frankfurt's Account of Love. J. Ethics & Soc. Phil., 14, 204.
>- Smuts, A. (2014). Normative Reasons for Love, Part I. Philosophy Compass, 9(8), 507-517.
>- Smuts, A. (2014). Normative reasons for love, part II. Philosophy Compass, 9(8), 518-526.
>- Wolf, Susan: “The True, the Good and the Lovable”, in Contours of Agency, 227-244, ed. Buss and Overton.


# Harry Frankfurt’s theory of love

## Kinds of love

Frankfurt: 

> “It is important to avoid confusing love ... with infatuation, lust, obsession, possessiveness, and dependency in their various forms [including romantic and sexual relatioships.] ... Relationships of those kinds typically include a number of vividly distracting elements, which do not belong to the essential nature of love as a mode of disinterested concern, but that are so confusing that they make it nearly impossible for anyone to be clear about just what is going on. Among relationships between humans, the love of parents for their infants or small children is the species of caring that comes closest to offering recognizably pure instances of love.” (The Reasons of Love, 43)

. . . 

So, Frankfurt is examining *storge* or *philia* rather than *eros* or romantic love.

## Frankfurt’s necessary features of love

“Four main conceptually necessary features of love of any variety:”

. . .

1. Love “consists most basically in a disinterested concern for the well-being or flourishing of the person who is loved.”

. . . 

2. Love is “ineluctably personal.”

. . . 

3. “The lover identifies with his beloved.”

. . . 

4. “Love is not a matter of choice.”


## 1. Disinterested concern

>- According to Frankfurt, love has to be “disinterested,” means: the lover should not be following his own interests, but only look towards the interests of the beloved.
>- To love someone for personal gain is not real love.
>- The beloved is a “final end.” (Meaning: a Kantial end in himself).

## 1. Disinterested concern

>- Problem: If love is important in our lives, then the lover will *always* also profit from their love. Because they get love in return for profiting the beloved. Therefore, no love can be totally “disinterested” in the lover’s own benefit.
>- Solution?
>- Frankfurt: Yes, the lover also gets something out of the love relationship, namely a life that includes love. But this can be achieved only through *disinterested* love first; so the condition still holds.
>- Only by focusing on the needs of the beloved can the lover gain what they themselves need (the loving relationship).

## 2. Love is personal

>- Love is personal in that the particular person matters.
>- A general agape towards all human beings is not proper love for Frankfurt.
>- Loving a person must mean loving *this* person. Not even a copy or a better version of this person will do.
>- This is supposed to exclude appraisal theories of love and the problem of “substituting Nancy” (see previous lecture).
>- What makes a person *distinct* from others is not a matter of particular characteristics.

## 3. Identification with the beloved

>- Identification means to accept the interests of the beloved as one’s own.
>- The charitable act of giving a beggar money does not fulfil this criterion. After the donation, I forget about the beggar. I do not robustly identify with their interests.
>- Also, helping a homeless person is not personal. I want to help “a homeless” person. Not *this* particular person.
>- Problem: Do you see a conflict between points 1 and 3 (disinterested concern and identification)?
>     - If I identify with the beloved, then my concern cannot any more be “disinterested,” because now I am sharing the beloved’s interests.
>     - We identified this in the previous session as a problem of union theories in general. After uniting, the two people form a “we,” in which it is impossible for the one person to benefit the other without also benefiting herself.

## 4. Love is not a matter of choice

>- Frankfurt: “Love is not a matter of choice but is determined by conditions that are outside our immediate voluntary control.” (The Reasons of Love, 80)
>- Love creates a “volitional necessity”: 
>     - Anna knows that she *could* and *should* stop loving Harry Lime, but she cannot bring herself to actually stop loving him. Her will is not strong enough.
>- In a similar way, we cannot generally decide whether to stay alive or not. Although, in principle, we could commit suicide, for most of us this is not something that we can actually want or do.
>- It is a “volitional necessity” to want to stay alive.

## Constraint and freedom

>- Paradoxically, Frankfurt maintains that although we are not free to stop loving, this does not affect our autonomy. How could this be?
>- Frankfurt has a complex theory of what freedom means, but the main idea is that our freedom can be constrained from either the inside or the outside.
>- Can you see how drug addiction is different from love or the will to stay alive?

## Second order volitions

>- The drug addict’s will is constrained from the outside. 
>- He might want to not be a drug addict, but he is not free to stop taking the drug, because the drug (outside influence) is influencing his will.
>- The person who has to want to stay alive, constrains their will “from the inside”: it is their own wish to stay alive that makes them want to avoid dying.
>- Similarly, it is the own will of the lover to be someone who is in love with another person. 
>     - Although we don’t have a choice about the person, loving is in accordance with our deeper wish to experience love. 
>     - Therefore, we are not unfree when we are forced to love, because being in love is in line without our (deeper) wishes.
>- Frankfurt calls these deeper wishes “second order volitions.”
>- As long as our (first order) desires are in line with our second order volitions, we experience ourselves as free, even if we don’t have a choice about our first order desires.

## Determinism and freedom

>- This brings up a fundamental problem in the theory of love that we already talked about in the session about duty: can we control our emotions?
>- One possible answer: Yes, we can exercise internal and external control on our emotions (Liao).
>- Another answer: We can not control the *emotion itself,* but we can stop ourselves from acting on that emotion (Driver).
>- A third answer is Frankfurt’s: although we cannot control our emotions, we can have the *feeling* that we are free, since our will (that we cannot control) is in line with our second order desires, and therefore feels like “our will”: It is making us (unfreely) will things that we would (freely) will anyway.

## Love and value

>- Frankfurt: What we love *acquires* value because we love it. (Not the other way round).
>- This is a bestowal theory of love.
>- Since love does not come about as a response to the value of a thing, a person’s properties are irrelevant to whether we love that person. 
>- Frankfurt: The example of parental love clearly shows this. Parents would love their children even if these children had no objective worth (they were lazy, naughty, stupid, and so on).
>- But if love does not respond to value, should Anna then be free to love Harry Lime?
>- More generally, does it then matter at all whom we love? Is love’s choice entirely random?

## The party guest

>- Edyvane (p.71): Imagine you just met a stranger at a party. The stranger looks at you and says: “I’ve just seen you, but I am now deeply in love with you. Let’s go away together and stay together for the rest of our lives.”
>- *How would you react?*
>- You would find this creepy, perhaps? We wouldn’t generally accept a declaration of love that comes out of nowhere.
>- Why not?
>- Because we *do* think that there must be *some* reasons for love. 
>- A totally unreasoned, unmotivated love seems unreasonable, crazy.
>- To the party guest, you would perhaps answer: “But you don’t know me!”
>- Which could mean: knowing me would have revealed particular properties of mine to you, which would make your declaration of love justifiable.

## Worthy of being loved?

>- But what properties could make an individual worthy of being loved?
>- It cannot be that these are intrinsic properties of a person, like the hair colour, the humour, or the fact that they can dance. Why not?
>- Because then we’d just be back at the appraisal theory of love and the problem of replacing our lover with another person who has all the same properties to an even higher degree.
>- We need some way of saying that love is not totally arbitrary and random, but, at the same time, it should not be based *only* on properties of the beloved (thus making the beloved fungible!)

## Susan Wolf: What makes someone worthy of love?

>- Susan Wolf: An individual has to have “objective worth.”
>- “People should care about only what is somewhat worth caring about; and how much people should care about things, both in themselves and relative to other things they care about, depends somewhat on how much worth caring about the objects in question are.” (p.232) 
>- So one’s love needs to be proportional to the lovability of the beloved.

## Susan Wolf: How to judge love-worthiness? (1)

>- Whether (and how much) the object in question is itself worth caring about. 
>     - Harry Lime? A football club? A lover who is not faithful, but violent etc?
>     - This means that we can sensibly make an argument that one lover might be objectively better, more valuable, more worth caring about than another.
>- Whether (and how much) the person has an affinity for the object in question. 
>     - “Affinity” is “the suitability of an object for our affection.”
>     - Similar to the suitability of a pair of shoes to a particular wearer.
>     - This is similar to Velleman’s “contingent fit.”
>- Whether (and how much) the relation between the person and the object has the potential to create or bring forth experiences, acts, or objects of further value.

## Susan Wolf: How to judge love-worthiness? (2)

>- Observe how especially the 2nd and 3rd are *relational* qualities. They are different from “appraisable” properties of the beloved.
>- Therefore, they don’t cause the problems of fungibility of the beloved.

## Edyvane on the value of the beloved

>- For Edyvane, the proof of the value of love is the fact that the lover is trying to confirm that the beloved is worthy of their love.
>- The moment lovers stop trying to convince themselves (and others) of the value of their beloved, the love has lost some of its value.
>- “It is a part of the best kind of love that the lover takes an interest in the non-subjective worth of her beloved. It is a declaration that, at the point at which the lover loses interest in striving to confirm the worth of the beloved, she has ceased to love as much as she once had.” (Edyvane, 64)
>- “When a person declares her love for us, she is also implicitly declaring her sincere belief that it would in principle be possible to provide some account of *why* we are worthy of that love.” (Edyvane, 72)

## Justification as proof of worth

>- For both Wolf and Edyvane, and opposed to the no-reasons view, there must be some assumption that the object of love is worthy of that love.
>- This assumption must be proven, or demonstrated in an “objective” way, for others to see. 
>- If it cannot be proven, at least there must be an attempt made by the lovers to do that.
>- When lovers give up on the attempt to demonstrate the love-worthiness of the beloved, the love itself suffers.


# Smuts: Normative Reasons for Love

## Is love a relationship?

>- Smuts, 509: Attitudes are different from relationships.
>- I can have a friendship as a relationship. This includes an attitude towards my friend (a friendly disposition, a liking).
>- Now my friend moves away or dies. The relationship is not there any more.
>- But the attitude can persist. I can still feel friendliness towards my dead/moved away friend, even if it has become impossible to have a *relationship* of friendship now.

## Kinds of lovable things

Smuts, 509/510:

>- We can love particular things and not others.
>- A good test for lovable things is the “funny” test: does a sentence that says that we love X sound funny?
>- Example: “I love my wife and my child more than anything.”
>     - Not funny, so wife and child are lovable.
>- “Before I met my wife, I loved fried chicken more than anything.”
>     - This *is* kind of funny. So fried chicken is not a good object for my love.
>     - “Love” for fried chicken is of a different kind than love to one’s wife.
>- Love must include “caring for the beloved for her own sake.”


## Motivating vs normative reasons

>- When we are asked to justify something (like our love for a person), we can distinguish two different kinds of reasons:
>- “Motivating reasons explain why someone did something. A motivating reason is the efficacious motive of an action.” (Smuts 512)
>- But sometimes, one’s motivating reasons can also be justifying reasons. 
>     - For instance, a [policeman] might have shot a terrorist to prevent him from setting off a bomb at a crowded event.
>     - This does not only *explain* why he did it, but it also provides a (moral, legal) *justification* for that act. 
>     - It is a *normative* reason.
>- “When it comes to actions, normative reasons are those that count in favor of an action.”

## Awareness of normative reasons

>- If there *is* a normative reason, but we not aware of it, then this reason cannot justify our action! (For example, killing a terrorist before he triggers his bomb, at the time when we could not have known that this person is, indeed, a terrorist).
>- “An attitude is a mere happy accident in relation to some normative reason unless the attitude is a *response* to that normative reason.” (Smuts, 512).

## Strong and weak justifications

>- Now, when we use a justifying (normative) reason, we have to make sure that we know what kind of justification that reason provides.
>- There are three types of moral evaluation:
>     - Reasons showing that an action is prohibited.
>     - Reasons showing that an action is permissible.
>     - Reasons showing that an action is required.
>- Is Anna...
>     - Not allowed to love Harry Lime?
>     - Permitted to love Harry Lime or to withdraw her love?
>     - Required to love Harry Lime?

## Strong and weak justifications

>- A strong justification shows that an attitude is required.
>- A weak justification would only show that an attitude is permissible.
>- Can reasons *strongly* justify love?
>- Can I give a reason why I *ought to* love someone? (That is, why it would not be permissible to *not* love them?)
>- Can I give a reason why it would *not* be permissible to love someone?
>     - What if that person is Hitler? (Or, for that matter, Harry Lime?)
>- Here, again, we see the asymmetry that Julia Driver mentioned in her paper.
>     - There seem to be reasons *not* to love people, or to fall out of love with them.
>     - But there don’t seem to be good reasons to fall in love with them.

## Conclusions (1)

>- What can we learn from that whole discussion?
>- We must distinguish agape and eros. Romantic love seems to be more reasons-responsive and selective than agape.
>- We must distinguish between (a) falling in love; (b) staying in love; and (c) falling out of (romantic) love.
>- These three phenomena seem to work in different ways.
>- It seems strange to say that *falling* in love is *not* dependent on reasons. We do have shopping lists and a concept of our ideal partner even before we meet them, and we evaluate candidates carefully before we fall in love with them (see online dating).

## Conclusions (2)

>- But it seems that the reasons fade away when we already *are* in a relationship. Then the relationship itself becomes the reason for the love.
>- It also seems like there should be good reasons to fall *out of* love. Discovering that your beloved is an evil criminal might be such a reason.
>- Even if we cannot entirely control our feelings, we are able to control their manifestation in action. Therefore, we might be held responsible for immoral actions we perform “out of love”.
>- Maybe we can, to some extent, influence and control our emotions.
>- Irrational/crazy love does not seem to be a good thing either. We expect our love to make some sense, and we expect to be able to show that the beloved is worthy of our love.




