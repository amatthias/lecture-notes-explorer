---
title:  "Love and Sexuality: 6. Aristotle: Discussion"
author: Andreas Matthias, Lingnan University
date: September 1, 2019
theme: lisbonsmall
transition: fade
slidenumber: 'c/t'
autoslide: 0
...


# Introductory remark

## How to locate quotes

>- If you want to locate a piece of text in Aristotle (or many other ancient sources), the easiest way is to use Perseus:
>- <http://www.perseus.tufts.edu/hopper/text?doc=Aristot.+Nic.+Eth.+1157b&fromdoc=Perseus%3Atext%3A1999.01.0053> 
>- If you click on that, you’ll get to Nicomachean Ethics, 1157b. There you can also enter other numbers and jump directly to the text.
>- You can also switch between Greek and English, if you want to look at particular words in the original language (e.g. philia, eros, agape etc). This can be useful even if you don’t fluently speak ancient Greek (as long as you learn the alphabet).


# Aristotle on friendship and love


## Virtue and function in Aristotle

>- The Greek word for virtue (*arete*) is associated with *aristos* (excellent, best) and means the goodness or excellence of a kind of thing.
>- “A thing’s virtue [or excellence] is *relative to its own proper function* (ergon), that is, the characteristic activity peculiar to something or its distinctive mark.”^[Jiyuan Yu (1998): Virtue: Confucius and Aristotle. Philosophy East&West, 48(2)]
>     - A good pen is a pen that writes well, that is, a pen that fulfils its distinctive function well. A good tree is a tree that performs its function of being a tree well.
>     - “Any kind of thing can be said to possess its (specific) virtue by performing its function well.” (op.cit)
>- Aristotle: "The virtue of a human being will likewise be the state that makes a human being good and makes him perform his function well." (1106a23-24).
>- What is the distinctive “function” of humans? 
>     - To be virtuous in a rational way. No other animals can rationally control their virtues (=have phronesis).

## General characteristics of Aristotle’s theory of love (1)

>- From what you know now about Aristotle, try to guess what his opinions on love might be!
>- Every virtue results from moderation between the extremes of lack and excess. Love should exist in moderation.
>     - The point of all virtues is that they are based on rational evaluation of the right amount. When rationality is affected or the virtue’s exercise becomes irrational, it is not a virtue any more.
>     - Indifference to others is lack; sexual eros is excess (because it clouds the rational mind). So what is the optimal level of love?
>     - Companionship, friendship. Clear-eyed, rational, serving the benefit of both the lover and the beloved.

## General characteristics of Aristotle’s theory of love (2)

>- The ultimate goal of human life is eudaimonia.
>     - So love has to serve the eudaimonia (happiness) of all participants.
>     - Overly emotional love that makes people unhappy is not the right thing. It is a (bad) passion rather than a (good) virtue, exercised with phronesis.
>- We need friends in order to learn phronesis and progress towards eudaimonia.
>     - So we need to select our lovers so that they help us improve.
>     - This will likely be the case if our lovers are close to us in terms of phronesis. A criminal being friends with a saint won’t make for a mutually useful relationship. The same applies to old/young, educated/uneducated and so on.
>     - Aristotle’s relationships are based on mutual usefulness and the lovers have to be roughly equal in phronesis, so that the relationship benefits both. Otherwise it’s a teacher/student relationship, but not *love*.

## “Philia”

>- The word “philia” for Aristotle can mean different things:
>     - Friendship.
>     - The feelings one has towards one’s family.
>     - The feelings towards people we share interests or hobbies with.
>- Aristotle gives examples of philia: Young lovers, lifelong friends, partner cities, business contacts, parents and children, fellow-voyagers, members of the same religious society, business owners and their customers.
>- Also: “The friendship of children to parents, and of men to gods” (NE VIII, 12, p. 141).
>- We see that this is quite a bit more than what English “friendship” means.

## Definition of friendship (1)

>- Friendship is a relationship in which persons **similarly** love each other, and in which they reciprocally wish good things to each other “in that very respect in which they love each other.” (Nicomachean Ethics 8.3).
>- Remember that Eros is an asymmetric relationship. The older erastes and the younger eromenos don’t love each other “similarly.” This is how friendship differs from Eros.

## Definition of friendship (2)

>- In the Rhetoric: “[Philia is] wanting for someone what one thinks good, for his sake and not for one’s own, and being inclined, so far as one can, to do such things for him.” (1380b36–1381a2)”
>- John M. Cooper (“Aristotle on the Forms of Friendship,” The Review of Metaphysics 30, 1976–1977, pp. 619–648): “the central idea of φιλíα is that of doing well by someone for his own sake, out of concern for him (and not, or not merely, out of concern for oneself).”
>- Aristotle says that philia is necessary for happiness, since all men need friendship. 

## Kinds of love and friendship

>- Aristotle thinks that we value a person only because of their goodness or usefulness (NE 8.2): “For not everything seems to be loved but only the lovable, and this is good, pleasant, or useful.”
>- There are two ways in which someone may be good or pleasant/useful: 
>     - Someone may be good or pleasant/useful either “in his own right” 
>     - or “in relation to you.” 

## Complete friendship

>- Someone is “good in his own right” if he is a good human being, virtuous, acting according to the virtues etc. That is, a person with phronesis.
>- To love such a person is pleasant and valuable in itself.
>- A complete friendship is a friendship in which this sort of love is reciprocated.
>- So, in a complete friendship, both friends are good, virtuous, sophron human beings.

## Usefulness and pleasure

>- Some people are “good in relation to you,” that is, they are useful to you.
>- Others are “pleasant in relation to you”: they are entertaining.
>- Someone may be useful without being entertaining, or entertaining without being useful.
>- These two relations define two further kinds of friendship:
>     - Friendships based on usefulness; and
>     - friendships based on shared entertainment and pleasure.

## Are usefulness and pleasure really different?

>- Can you give an example to show that these two types of friendship are really distinct?
>- If you are involved in a traffic accident with others, you might help them and they might help you (usefulness), but you may not like them at the same time. Usefulness does not need liking. Or, a friend who tells good jokes might be pleasant to have around, but otherwise not useful to you.
>- Same with shopping: the people who sell you something in a shop are useful to you, but they don’t become your friends.

## Philia vs frienship

>- How is Aristotle’s philia different from our modern English “friendship?”
>     - We would call only “complete friendship” a real friendship.
>     - We would not accept “friendship for usefulness” as genuine friendship at all.
>     - “Friendship for pleasure” would be somewhere in between, a weak form of sympathy from amusement, but not “real” friendship either.

## Similarity and friendship

>- In Aristole’s view, do similar or dissimilar persons become friends?
>- Remember that for Aristotle, friendship is symmetrical, that is, the love of one friend answers to the love of the other in roughly equal ways. Therefore, friends will tend to be similar in character and virtue.
>- Ancient Greek “pederasty” (the sexual love between older and younger men) is not a symmetrical relationship. Therefore, the two parts will not be able to fully relate as equals to each other and their relationship will be unstable and not a true friendship. The older man will not be able to love the virtue in the younger, and the younger will not be able to love the body of the older.

## Goodness and complete friendship

>- What is the relationship between goodness and friendship? Can bad people be friends with each other?
>- Complete friendship is based on good character, so only good persons can be friends in a complete friendship.
>- But this does not matter as much for the other kinds of friendship: I can be entertained by someone or find him useful, despite the fact that he is not a good person.

## Can we love things?

>- Can we love things, for example: wine? What would Aristotle say?
>- I can talk of “loving wine,” but I cannot be friends with wine.
>- Since Aristotle’s love is reciprocal, I’d have to expect the wine to love me back. 
>- Also, philia for Aristotle means to “wish the other well.” But we cannot wish the wine well. We can only wish that *it keeps well,* so that we can have it to ourselves.
>- Therefore, no true love (friendship) is possible towards things. The talk of “loving” wine is wrong and not meant to be taken seriously.

## Loving animals? (1)

>- Can I love an animal? Can I have “philia” towards my cat or dog?
>- “[Philia is] wanting for someone what one thinks good, for his sake and not for one’s own, and being inclined, so far as one can, to do such things for him.” (1380b36–1381a2)”
>- John M. Cooper (“Aristotle on the Forms of Friendship,” The Review of Metaphysics 30, 1976–1977, pp. 619–648): “the central idea of φιλíα is that of doing well by someone for his own sake, out of concern for him (and not, or not merely, out of concern for oneself).”
>- If we see it like this, yes, I can love an animal. I can wish my dog well and I can do good things for it. I can “do well by my dog” for its own sake.

## Loving animals? (2)

>- Also, things (or people) are lovable because they are “good, pleasant or useful.” (NE 8.2)
>- Can a dog be good, pleasant and useful?
>- A dog can be pleasant and useful. These are the two main reasons why people keep dogs. 
>- A dog cannot be good in itself, because this would mean that the dog is virtuous and has phronesis. Only human beings can be “good” in a moral sense.
>- Consequently, I can have a relationship based on usefulness or pleasure with an animal. But it cannot be real friendship.

## Loving animals? (3)

>- Also, real friendship is a relationship in which persons **similarly** love each other, and in which they reciprocally wish good things to each other “in that very respect in which they love each other.” (Nicomachean Ethics 8.3).
>- It is not clear that a dog can do that, and much less a cat or a bird in a cage. A dog might try to help its owner in an emergency, but is this really “wishing good things” to the human?
>- In order to “wish good things” I need to know what the other considers ‘good.’ I need to have some empathy. 
>     - For example, most humans would value a good job, a good and secure income, health and a happy family.
>     - Does a dog really wish these things to its owner?
>- It seems that reciprocity is severely limited in the case of love towards animals.

## Loving a country (1)

>- Can we love a country? Can I be friends with Greece, for example?
>- Again: “[Philia is] wanting for someone what one thinks good, for his sake and not for one’s own, and being inclined, so far as one can, to do such things for him.” (1380b36–1381a2)”
>- And: John M. Cooper (“Aristotle on the Forms of Friendship,” The Review of Metaphysics 30, 1976–1977, pp. 619–648): “the central idea of φιλíα is that of doing well by someone for his own sake, out of concern for him (and not, or not merely, out of concern for oneself).”
>- I can certainly wish my country well (as opposed to the wine). I can do good things to my country. I can be concerned about my country.


## Loving a country (2)

>- Also, things (or people) are lovable because they are “good, pleasant or useful.” (NE 8.2)
>- Can a country be good, pleasant and useful?
>- A country can be pleasant and useful. Greece is pleasant as a place where I spend my holidays. Being a citizen of Greece is useful to me, because I then become a citizen of the EU, I can get employment in the EU and so on.
>- But can a country be good by itself? Can it, as a country, have virtues and phronesis?
>- (See also next slide: functionalism)


## Loving a country (3)

>- Can a country reciprocate my feelings? Can it “wish me well”?
>- This is difficult to answer. A social system can take care of me. A country’s hospitals and other infrastructure (roads, playgrounds, parks) may “do well by me” and help me live a good life.
>- But does a country do this consciously? 
>- If a forest is good to me (it relaxes me, it improves my health) can I say that the forest “wishes me well”?
>- Probably not. 
>- But a country is more complex than a forest. It acts, raises taxes, builds streets, protects the citizens. Does it also “wish”?
>     - This depends on how we approach mental states like virtues or wishes.
>     - If we are *functionalists* about mental states, we might say that a country has a mental state (e.g. makes a wish) if that “wish” relates to its other states in the same way as a human wish relates to other mental states inside a human brain. A country, in this view, *might* be able to wish, love, threaten etc.


# Plato and Aristotle

## Main differences between Plato’s Aristotle’s love (1)

>- Try to summarise the main differences between the concepts of love in the Symposion and Aristotle’s writings!
>- Plato’s love is asymmetrical. Eros is directed towards the good, either as it is personified in particular people, or the abstract idea of the good itself. Lovers have different power and get treated differently in a relationship, and the good itself does not love you back.
>- Aristotle’s love is ideally symmetrical. Both parties wish each other well, they both enjoy spending time together etc. If friendships based on utility are asymmetrical, this can work for a while, but such a relationship is unstable.
>- Plato’s love is at least passionate; Aristotle’s love is a kind of rational exchange of goods: utility, pleasure, or moral character.

## Main differences between Plato’s Aristotle’s love (2)

>- On the other hand, Aristotle does acknowledge the human who is on the other end of a relationship more than Plato does. 
>     - For Plato, every human is just the carrier of some part of the idea of the good and beautiful. 
>     - For Aristotle, humans are valuable as carriers of virtue, and their personalities actually are important for love. Not every human being will be an equally good friend.
>     - Singer (I, 91): “The Aristotelian friends are related to one another as something more than vehicles to the highest form. If theirs is not the love of persons, at least it is the love of good character in other persons.”

## Love as moderation between extremes

>- You remember that, for Aristotle, virtue is the moderation between extremes of character.
>- Aristotle describes *philia* as the highest kind of love. *Eros* for him has a more specific, sexual meaning (different from Plato!)
>- Eros, for Aristotle, is a “desire for pleasure” and a “sort of excess in feeling.”
>- Generally, sexual love (eros) is an extreme and true friendship (philia) is the (good) mean (Singer I, 92).


# Is Aristotle’s love a bestowal or appraisal love?

## Appraisal?

>- Could we classify Aristotle’s love as an “appraisal” love? Why?
>- Irving Singer believes that Aristotle’s love is purely an “appraisal” kind of love.
>- When we love our friends, we appraise their qualities of character and we directly respond to these qualities.
>- If our friends didn’t have the required qualities, they would not be suitable as our friends and we wouldn’t love them.

## Bestowal (1)

>- Are there any “bestowal” elements in Aristotle’s philia?
>- But Richard Kraut (“The Importance of Love in Aristotle’s Ethics”, 1975) makes another point.
>- “Those who love have an intense desire to benefit someone for his own sake. If one wants to help another solely as a way of benefiting or pleasing oneself, then one “loves” that individual only by an extension of the term (1156al4-19, 1157a30-32). Commercial partnerships (1158a21) and purely sexual relationships (1156bl-3) typify such “incidental” forms of love; one person aids or pleases the other only to increase his own wealth or to enhance his own physical pleasure.”
>- “When love is real, on the other hand, the advantages and pleasures one might receive will be valued, but one does not help the person one loves merely as a means to such goods.”

## Bestowal (2)

>- “Aristotle recognizes two very different types of genuine love. In one case, we respond to an individual's character and love him for the kind of person he is. This love begins with good will, a mild well-wishing caused by the recognition of some apparent virtue in a person. As one discovers through greater acquaintance that the object of one's good will really is virtuous and therefore worthy of one’s love (1156b25-32), and as one continues to act on one's desire to benefit him for his own sake, this desire, at first weak, strengthens until it becomes love.”
>- “One comes to love the object of one's benevolence just as a craftsman loves the artifact he has created (1167b31-1168a9). Both have gone to considerable lengths and have exercised their skills, and they see their efforts embodied in the person benefited or product created.”

## Bestowal (3)

>- This now sounds much more like bestowal. 
>- It is not only the immediate appreciation of another’s character that causes me to love them.
>- Instead, the emerging love is the product of my virtuous interaction with the other person, my being benevolent towards them and benefiting them.
>- In this sense, we could imagine that almost any other person (within limits) could be the object of our virtuous interaction, our “love bestowal.”
>- Love, in this sense, is *created* by the lover, rather than just a response to features of the beloved.

## Bestowal (4)

>- Kraut: “There is a second form of love, however, which has very different features. This is the love of one's child, which is natural to all parents (1155a16-18) and arises immediately at birth (1161b24-25).”
>- “Rather than developing gradually with increased acquaintance, it arises suddenly with full intensity, and it is not caused by the perception of some good quality in the person loved. We love our children for their own sake (ekeinon heneka), but not for themselves (di'hautous, kath'hautous); that is, not for their character.”
>- This can be disputed. Erich Fromm, for example, in “The Art of Loving” (1956) distinguishes between “father’s love,” which is conditional on the qualities and behaviour of the child; and “mother’s love” which is supposed to be unconditional.
>- In any case, love for one’s children would be a pure “bestowal” love for Aristotle.

# Is love an emotion?

## Is love an emotion for Aristotle? (1)

>- Is love an emotion for Aristotle?
>- No. The feeling/emotion he calls *philesis,* and he distinguishes it from the character trait *philia:*
>     - “Liking seems to be an emotion, friendship a fixed disposition, for liking can be felt even for inanimate things, but reciprocal liking involves deliberate choice, and this springs from a fixed disposition.” (1157b)
>- Kraut: “Loving someone, in other words, is not a matter of being struck by an urge, but is rather a fixed attitude that reflects one's values. That love also involves experiencing an emotion if not denied (1126b11).”


## Is love an emotion for Aristotle? (2)

>- This is different from modern understanding:
>     - Love: “a feeling of strong personal attachment” and “ardent affection” (Webster's New International Dictionary, 2nd ed., 1959)
>     - “Companionate love... combines feelings of deep attachment, commitment, and intimacy” (Hatfield and Rapson, Handbook of Emotions (2000: 655, both cited after Konstan (2008): Aristotle on Love and Friendship, Schole, II(2))
>- Aristotle says nothing about feelings and focuses exclusively on the intention to benefit the other person who is one’s friend (Konstan).


# Is (self-) love a virtue?

## Is Aristotle’s love essentially egoistic? (1)

>- You could argue that Aristotle’s virtues are egoistic, because what we want from exercising them is, ultimately, to promote our own *eudaimonia.* Is this a correct reading of Aristotle?
>- Probably not. Aristotle wants us to exercise our virtues not *because* we will benefit ourselves, but *because* we want to help others, be good, honest, caring and so on.
>- The benefit to us comes as a *consequence* of these actions, but should not be their driving force.

## Is Aristotle’s love essentially egoistic? (2)

>- Example: You find something valuable on the street and give it back. As a consequence, you get a reward. What would Aristotle say? Are you an egoist because you wanted the reward?
>- No. If you are *sophron,* then you returned the thing *because* you have, through long practice, achieved the state of acting virtuously automatically.
>- You don’t choose your action consciously, and you have no choice. You couldn’t have kept the thing. It’s in your character to return it.
>- The reward might come, and it might be pleasant, but it’s not the *reason* you returned the thing.


## Should we love ourselves? (1)

>- What would Aristotle say about self-love?
>- Aristotle has a discussion on p.155/156 (Book IX, 8) of our edition of the Nicomachean Ethics (see Moodle).
>- “The question is also debated, whether a man should love himself most, or some one else. People criticize those who love themselves most, and call them self-lovers, using this as an epithet of disgrace, and a bad man seems to do everything for his own sake, and the more so the more wicked he is—and so men reproach him, for instance, with doing nothing of his own accord—while the good man acts for honour’s sake, and the more so the better he is, and acts for his friend’s sake, and sacrifices his own interest.”

## Should we love ourselves? (2)

>- But, Aristotle says, all these properties apply first to the relation one has with oneself.
>- “For men say that one ought to love best one’s best friend, and man’s best friend is one who wishes well to the object of his wish for his sake, even if no one is to know of it; and these attributes are found most of all in a man’s attitude towards himself, and so are all the other attributes by which a friend is defined...”

## Should we love ourselves? (3)

>- “Living according to a rational principle is [different] from living as passion dictates, and desiring what is noble from desiring what seems advantageous. Those, then, who busy themselves in an exceptional degree with noble actions all men approve and praise; and if all were to strive towards what is noble and strain every nerve to do the noblest deeds, everything would be as it should be ... Therefore the good man should be a lover of self (for he will both himself profit by doing noble acts, and will benefit his fellows), but the wicked man should not; for he will hurt both himself and his neighbours, following as he does evil passions.”
>- So we *are* allowed to love ourselves, but only if this self-love is the basis for a virtuous character that seeks to benefit others as one loves oneself.
>- It is *not* good to be selfish from a lack of virtue.
>- “In this sense, then, as has been said, a man should be a lover of self; but in the sense in which most men are so, he ought not.”

# Criticism of Aristotle’s concept of friendship

## Reason and emotion

>- How can we criticise Aristotle’s concept of friendship?
>- Aristotle’s love comes at the high price: “Even more than Plato, Aristotle associates love with reason and against emotion.” (Singer, I, 91). 
>- Plato’s lovers are “enthusiasts yearning for the good” (Singer). – “The Aristotelian friends are businessmen who share a partnership in virtue.” (Singer, I, 92). 

## Eros

>- Is eros a virtue for Aristotle?
>- No. Aristotle calls it “a sort of excess of feeling.” This does not seem to do it justice.

## Philia and choice

>- Do we have a choice about whom we love, according to Aristotle?
>- Aristotelian philia is based on choice. Love is bestowed by choice, and can be withheld by choice, because it is a rational act, not a feeling.
>- This neglects the emotional character of love as we experience it.
>- For example, an Aristotelian could justify an arranged marriage with a suitably virtuous partner one has never met.

## Philia and persons

>- Do Aristotelian lovers really relate to the person they love?
>- Aristotelian lovers don’t love the person itself, but some attributes of their character. In principle, every human with those characteristics would do as well. Love objects become interchangeable.
>- This is a problem of every “appraisal”-type concept of love.

## Love and justice

>- Do you see a relationship between love and justice in Aristotle’s idea of love?
>- For Aristotle, love is linked to justice. 
>     - One *deserves* love because of one’s character attributes (Singer I, 95). 
>     - One has a justifiable claim to be loved because of one’s character. 
>     - In a sense, this is crazy: one cannot force love, arguing that one deserves it. One can *honour* deserving people, but not, on command, *love* them.
>- This seems to be the opposite of agape or Christian love of the next man: Aristotle’s love is completely dependent on merit. While agape is, by its nature, unmerited and unearned, bestowed without reason.



# The End. Thank you for your attention!

<!--

%% Local Variables: ***
%% mode: markdown ***
%% mode: visual-line ***
%% mode: cua ***
%% mode: flyspell ***
%% End: ***

-->




