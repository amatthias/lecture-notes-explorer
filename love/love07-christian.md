---
title:  "Love and Sexuality: 7. Christian Love: Agape"
author: Andreas Matthias, Lingnan University
date: September 1, 2019
...

# Christian love

## Christian love

Common sources of Christian love concepts:

>- The Bible, particularly the letters of St Paul.
>- St Augustine
>- The Desert Fathers
>- Thomas Aquinas
>- Anders Nygren
>- C. S. Lewis


## Subjects and objects of love

Christian religion, like any other theory of love, has to provide descriptions and explanations for different kinds of love:

>- Love of humans towards God
>- Love of God towards humans
>- Love of humans towards the next man 
>- Charitable love
>- Erotic love


## St Paul (4 BCE - ~63 CE)

>- “Love is patient, love is kind. It does not envy, it does not boast, it is not proud. It is not rude, it is not self-seeking, it is not easily angered, it keeps no record of wrongs. Love does not delight in evil but rejoices with the truth. It always protects, always trusts, always hopes, and always perseveres. ... And now these three remain: faith, hope and love. But the greatest of these is love.” (1 Corinthians 13)


## Flesh and spirit (1)

St Paul, Galatians 5:

>- Here we already find the opposition between “flesh” and “spirit”:
>- “13 You, my brothers and sisters, were called to be free. But do not use your freedom to indulge the flesh; rather, serve one another humbly in love [agape]. 14 For the entire law is fulfilled in keeping this one command: “Love your neighbor as yourself.” ... 16 So I say, walk by the Spirit, and you will not gratify the desires of the flesh. 17 For the flesh desires what is contrary to the Spirit, and the Spirit what is contrary to the flesh. They are in conflict with each other, so that you are not to do whatever you want.

## Flesh and spirit (2)

St Paul, Galatians 5:

>- 19 The acts of the flesh are obvious: sexual immorality, impurity and debauchery; 20 idolatry and witchcraft; hatred, discord, jealousy, fits of rage, selfish ambition, dissensions, factions 21 and envy; drunkenness, orgies, and the like. I warn you, as I did before, that those who live like this will not inherit the kingdom of God.
>- Clearly, St Paul has some idea like Plato’s in mind, where Eros should, ideally, be removed from the bodily desires and directed towards some eternal version of the good.


## Is Christian love exclusive? (1)

>- The New Testament is not always clear: are Christians asked to love *everyone* or only other Christians?
>- John 13:34: “A new command I give you: Love one another. As I have loved you, so you must love *one another*. 35 By this everyone will know that you are my disciples, if you love one another.” (emph. added)
>- Galatians 3:28: “There is neither Jew nor Gentile, neither slave nor free, nor is there male and female, for you are all one in Christ Jesus.”
>- Colossians 3:11: “Here there is no Gentile or Jew, circumcised or uncircumcised, barbarian, Scythian, slave or free, but Christ is all, and is in all.”

## Is Christian love exclusive? (2)

>- Matthew 22:35-40:
>     - 36 “Teacher, which is the greatest commandment in the Law?”
>     - 37 Jesus replied: “‘Love the Lord your God with all your heart and with all your soul and with all your mind.’[c] 38 This is the first and greatest commandment. 39 And the second is like it: ‘Love your neighbor as yourself.’ 40 All the Law and the Prophets hang on these two commandments.”
>- Here is a conflict: what if the love for God contradicts our love to our neighbour? ^[Liu Qingping (2007): “On a Paradox of Christian Love”. Journal of Religious Ethics.]


## St Augustine (Augustine of Hippo, 354-430 AD)

>- Difference between love and lust.
>- Remember, Aristotle distinguishes love from pleasure and love from goodness. Similarly, Augustine sees that a love relationship has both elements that are pleasurable and elements that are (morally) good.
>- Central to Augustine’s understanding of rightly ordered sexuality is his belief that *the pleasure of the act should not be separated from its good (procreation).* (Meilaender, G. (2001). Sweet necessities: food, sex, and saint Augustine. Journal of Religious Ethics, 29(1), 3-18.)


## St Augustine (2)

>- Augustine distinguishes between the *good* of an activity (its appropriate end or purpose) and the *pleasure* the activity gives. 
>- “There is, in Augustine’s view, nothing wrong with taking pleasure in any permitted activity. What is wrong is trying to separate the pleasure from the good—trying to get the pleasure when one has no interest in or need of the good to which the activity tends or ought to tend.” (Meilaender)
>- (This is also at the basis of one of the Christian/Catholic arguments against contraception).


## St Augustine (3)

>- Metaphor with food: 
>     - The good of food: nourishment.
>     - The good of sex: children.
>     - Both give pleasure, but: “As we should not grasp for the pleasure of eating apart from the good purpose to which it is divinely ordered, so, also, we should not seek the pleasure of the sexual act apart from the good to which it is divinely ordered. ... Its purpose is offspring.” (Meilaender)

## St Augustine (4)

>- “[A] man had to love his spouse, but preferably not her body, and both parties were permitted an ordinate sexual pleasure in conjugal relations as long as they were not motivated by desire. This is like saying it is acceptable to enjoy eating, but not to feel hungry.” (Power 1996, 229)
>- What do you think of that? Is this a good comparison?
>- Hunger is a signal of a life-threatening condition: starvation. Abstinence from sexual activities is not life-threatening.
>- Hunger directs us towards “the good” (eating as nourishment). Sexual appetite, in contrast, is directed towards the pleasure of sex. We don’t normally feel a sexual urge specifically directed towards making children.
>- Perhaps “appetite” or “lust for fancy food” should be substituted for “hunger” above: “It is acceptable to enjoy eating, but not to feel lust for fancy food or delicacies.” This would describe Augustine’s position better.


## Desert fathers (approx. 3rd-5th century AD)

>- Anthony the Great (~AD 270) heard a sermon that said that one could achieve perfection by giving away all of one’s possessions to the poor and following Christ.
>- Anthony interpreted that to mean leaving society. He moved to the desert to be alone.
>- Over time, the movement grew. Life in the desert was intentionally hard, renouncing all usual comforts and pleasures of the senses.
>- The monks living in the desert were rumoured to be wise and people came to listen to them and get advice. When Anthony died, the dwellings in the desert were like a small city.


## Desert fathers (approx. 3rd-5th century AD)

>- The teachings of the desert fathers often emphasise the need to act in a selfless way, ignoring one’s own wishes, and living a life in total service to others.
>- Multiple collections of texts describe the sayings of the desert fathers.
>- An easy to read, small collection is: Thomas Merton (1960). *The Wisdom of the Desert.*

## Stories of the desert fathers (1)

>- There was once a hermit who was attacked by robbers. His cries alerted the other hermits, and together they all managed to capture the robbers. The robbers were put in jail, but the hermits were ashamed and sad, because, on their account, the robbers had been put to jail. They had acted selfishly, and not with sufficient love for the robbers. So in the night they went into the city, broke into the jail, and freed the robbers.

## Stories of the desert fathers (2)

>- Abbot Anastasius, another hermit, had a very expensive book, a Bible, his only possession. One day, a visitor stole his book, but Anastasius did not pursue him, because he didn’t want to make the other man lie about the book. A few days later, a used books seller from the city came to Anastasius and said: a man wanted to sell me this book, but because it looks quite expensive, I wanted to hear your opinion. Is this really a valuable book? Anastasius said yes, and told the book seller the real value of the book, without mentioning that it was his own. When the thief heard that, he took the book back to Anastasius and begged him to take back the book. But Anastasius didn’t want the book, and he gave it to the thief as a present. The thief was so impressed by the whole episode that he became Anastasius’ student and lived with him in the desert for the rest of his life.
>- In these stories, you can see an extreme and uncompromising version of Christian love for one’s neighbour. 


## Thomas Aquinas (1225-1274) (1): Amor and caritas

>- The greatest Christian philosopher of all time.
>- Influenced by Aristotle, he tried to synthesise and combine Platonic, Aristotelian and Christian ideas.
>- Aquinas uses different words for love.^[Much of the following discussion is from: Stump, E. (2006). Love, by All Accounts. Proceedings and Addresses of the American Philosophical Association, Vol. 80, No. 2 (Nov., 2006), pp. 25-43.]
>- “Amor” is the most general sense, which is included in the other types.
>- “Caritas” is love in the real and most perfect (Christian) sense.


## Thomas Aquinas (2): God

>- According to Aquinas, the ultimate proper object of love is God.
>- But since every human being is made in the image of God, the divine goodness is also reflected in every human person. Consequently, the proper object of love also includes human beings.
>- (You can see the Platonic aspect of this: human beings have part in the perfection of the Form/Idea of beauty. Their physical beauty is an imperfect “image” of the eternal Form.)
>- Thus, love is primarily the love of persons.
>- The most general kind of love for persons is friendship. (You see Aristotle here!)

## Thomas Aquinas (3): Desires in love

>- Love consists of two desires:
>     - the desire for the good of the beloved; and
>     - the desire for union with the beloved.
>- Again, see how this synthesises Aristotle (first desire) and Plato (second desire).
>- Differently from Plato, Aquinas did not think that the desire ends when we have achieved what we desire. You can be in the presence or possession of the desired thing and still have a desire towards it. (Plato would put this differently: how?)
>     - Plato: the desire that we have for X after possessing X is the desire for the continued possession of X in eternity.

## Thomas Aquinas (4): Goodness

>- What is “goodness”?
>- Not only moral goodness, but also beauty, elegance, efficiency and other positive traits. 
>- Goodness, for Aquinas, is objective and measurable.
>- “So to desire the good of the beloved is to desire for the beloved those things which in fact contribute to the beloved's flourishing.” (Stump, op.cit.)
>- What counts is the actual flourishing of the beloved, not only the intentions of the lover. So one has to do for one’s friends what is actually good for *them,* not what one would like oneself (which may be something different).
>- “On this understanding, it is possible for a person Jerome to think that he loves another person Paula when he actually does not, in virtue of the fact that Jerome has an intrinsic desire for what is harmful for Paula. On Aquinas's views, therefore, a person can be mistaken in his beliefs about whom he loves.” (Stump)


## Thomas Aquinas (5): Union and offices

>- Second, the desire for union is only a “loving” desire if the union is in the interests of the flourishing of the beloved.
>- Stump: “If a mother who wants her son with her tries to prevent him from ever leaving home, when leaving home is necessary for his flourishing, then she is not in fact desiring the good for her son. For that reason, her desire for union with him is also not a desire of love.”


## Thomas Aquinas (6): Union and offices

>- What form that “union” takes will be different depending on whom we want to unite with. 
>- “The kind of union ... appropriate for the people who are [one’s] spouse, parent, child, colleague, and priest will be different, depending on the relationship in question.” (Stump)
>- Sometimes people might have multiple relationships to each other: a teacher might, for example, be teaching their own child. Different types of love will be appropriate at the different phases of this relationship.
>- The “office” of teacher (or parent, or husband) will require particular responses in terms of the kind of love shown and experienced.

## Thomas Aquinas (7): Appraisal and office

>- Some theories have problems explaining particular kinds of love. For example, an “appraisal” theory will not be able to explain parental love well. Can you see why?
>     - Because there is little to appraise in small babies. They are a lot of trouble and don’t have many features of excellence. 
>     - If one’s child is less good or beautiful than another’s child, the parent won’t love the other more. The “appraisal” approach cannot explain the constancy of parental love.
>- How would Aquinas solve this problem?
>- For Aquinas, “the love between a parent and her child derives from the office which connects them, and the office is a function of the relational characteristics of the lover and the beloved. It is not a response to the intrinsic chacteracteristics of the beloved. Consequently, the parent's love does not co-vary with the intrinsic characteristics of her child either.” (Stump)

## Thomas Aquinas (8): Appraisal and office

>- What would Aquinas say about meeting someone who has better characteristics than the beloved?
>- “Furthermore, on Aquinas's account, love will not be readily transferable to any other person just because the other person's valuable intrinsic characteristics are the same as those of the beloved. The mere possession of the beloved's valuable characteristics on the part of some person other than the beloved is not enough for establishing that other person in an *office of love with the lover*” (emphasis added). (Stump)
>- On the other hand, Aquinas does allow us to be responsive to valuable properties in the beloved.

## Thomas Aquinas (9): Love from lack and love as friendship

Thomas Aquinas also distinguishes between:

>- “Amor concupiscentiae” the love that seeks from the beloved that which is lacking in oneself and wants that thing for oneself. We can again see the Platonic element in that (Aristophanes in the Symposion); and
>- “Amor benevolentiae” (the benevolent love) or amor amicitiae (the friendship love) which is the love for another person because of who that person is rather than what the lover is able to give to another person.
>- As a summary, we can see that Aquinas has a very complex and differentiated account of love that seems to be more Platonic and Aristotelian than actually Christian in the modern sense of “agape” love.


## Anders Nygren (1890-1978): Agape and Eros (1930/1936)

>- Anders Nygren was a Swedish theologian and bishop. In his book “Agape and Eros” (1930/1936) he emphasised the famous distinction between (bad, self-interested, conditional) Eros and (Christian, selfless, unconditional) Agape.
>- Because Eros is selfish, it is not a proper kind of love at all.

## Anders Nygren (1890-1978): Agape and Eros (1930/1936)

>- Agape, for Nygren, is modelled on the self-sacrifice of Jesus on the cross. ^[Pope, S: Love in Contemporary Christian Ethics, Journal of religious ethics. , 1995, Vol.23(1), p.165-197.]
>- It is radically self-sacrificial, spontaneous, uncalculating, bestowing rather than appraising, and unmotivated by considerations of reciprocity (Pope, 167).
>- Agape “abandons all thought of the worthiness of the object.” (Nygren)
>- This contradicts Aquinas, who saw Agape as much more of a mutually beneficial union than a radical self-sacrifice. Therefore, Catholic thinkers tended to ignore Nygren, while Protestants accepted it more.
>- Since then, Nygren’s distinction has often been criticised.


## Criticism of Nygren

>- First, it is possible that the change in the New Testament which speaks of “agape” instead of “eros” was a purely linguistic development of the Greek language.
>- The spoken language at the time the New Testament was written, was “koine,” the “common” Greek, as opposed to the classic Greek of Plato.
>- “The history of the classical word eros, so frequently found in the Socratic dialogues, is that in the connotation of “loving” it went more and more out of use from the fourth century B.C. onward, while agape was more and more used instead of it. ... As early as the 2nd cent. B.C. agape was the common word for ‘loving’, both in the spoken and in the written language of everybody.”^[C. J. de Vogel (1981). Greek Cosmic Love and the Christian Love of God. Vigiliae Christianae 35, 57-81]
>- Accordingly, it might just be an accident of language that the Bible speaks of Agape rather than Eros, and not a very deep difference in meaning.


## Outka’s concept of Agape (Pope 1995) (1)

>- In 1972, Gene Outka published “Agape. An Ethical Analysis,” which is discussed in Pope (1995).
>- Agape acknowledges the human dignity of every person, independently of merit, attractiveness, or ability to reciprocate. Every human being possesses irreducible worth and cannot only be used as a “means” (Kant!) to one’s own interests.
>- Agape respects the worth of all human beings equally. Worth does not increase or diminish with social role or relationship to the agent (“office” in Aquinas’ terms). Exclusiveness, partiality and discrimination of any form are not allowed.

## Outka’s concept of Agape (Pope 1995) (2)

>- Agape admits reasonable self-regard. Agape does not exclude or overcome self-love (as Nygren said); but it also does not allow self-love to become stronger than neighbour-love. 
>- Agape commands genuine and responsible service to one’s neighbour, but it does not require indiscriminate service to the neighbour.
>- We can call this concept “Agape as equal regard” (Pope, 1995, 168)

## Criticising “Agape as equal regard”

>- Do you see how this concept of Agape could be criticised?
>- Pope (1995):
>     - The concept is excessively individualistic. It seems to talk about persons that are detached from their community, families and traditions.
>     - “Equal regard” does not sufficiently respect our feelings towards particular people.
>     - It does not sufficiently account for family, friendship, marriage and other institutions that create “special relationships” with special rights and obligations towards others.


## C. S. Lewis: The Four Loves (1)

>- C.S. Lewis (1960): The Four Loves. Lewis (1898-1963) was a professor of English literature, close friend of Tolkien, and author of the Chronicles of Narnia and other books, among them Christian theory.
>- Similar to Nygren, he distinguishes:
>- Storge, an “empathy bond.” This means liking people through familiarity, like in family relationships. It is natural and does not require “worthiness” in the object of love. This is, for Lewis, the basis for most of human happiness.
>- Philia. This is the love of friends for each other. It is based on shared values, interests or activities. It is freely chosen and highly selective. In modern times, he thought, genuine friendship was not appreciated enough.


## C. S. Lewis: The Four Loves (2)

>- Eros (romantic love). This is the romantic sense of “being in love” rather then the raw sexual desire. Eros is a strong motivational force for people, and it can lead to good or bad results.
>- Agape or Charity: unconditional love, based on God’s love. Selfless and the greatest of the four loves. The other kinds of love can turn bad or be used for bad ends, while only agape is always good.


# Peter Black: The Broken Wings of Eros

## Five questions about Eros in Christianity

>- What is it? 
>- Why has it been so difficult to incorporate it into Christian sexual ethics?
>- What is the importance of doing so?
>- Why has it been forgotten or denied?
>- Who is leading the rediscovery?

## History of the tension between Agape and Eros (1)

>- In Plato, Eros is the desire to unite with the loved.
>- But ultimately, “Lovers, with their passions, desires, and impulses, in search of the other half and longing for wholeness, are only true lovers and under the power of Eros, according to Plato, when what they seek is good.” (Black, 109)
>- Origen of Alexandria (184–253 AD) thought that the Bible “only substitutes agape for eros to prevent the weak and uninformed from thinking about carnal desire and passion.” (Black 110). Otherwise, he sees God’s Agape as a Platonic “Eros” relationship.


## History of the tension between Agape and Eros (2)

>- In modern times, Nygren (as we saw above) saw Eros as opposed to (Christian) Agape.
>- Karl Barth (theologian, 1886-1968) “portrayed eros as a ravenous desire, a ... strengthening of natural self-assertion, to be contrasted with Christian love.” (Black 110)
>- Since Platonic eros can be seen as being “acquisitive, egocentric, and devaluing of persons” (Black 110), this also seems to suggest that it is not a proper basis for Christian love.
>- For C.S. Lewis (see above), Eros works more like in Diotima’s Ladder: it begins with the appreciation of another’s body, but develops towards a love of the whole person. But Eros is not constant and reliable. It comes and goes and there is a danger that, as a culture, we might idolise Eros itself too much (Black, 113).

## Edward Vacek’s “three loves”

>- Agape;
>- Agape for self;
>- Eros.
>- All three are vital for Christian life (Black, 116).
>- Eros is just another (indirect) form of self-love:
>     - “In the first [agape], the immediate object of love is our own self; in the second [eros] the immediate object is something other than ourselves, which we love as a way of loving ourselves. With the first, we love ourselves for our own sake: agapic self-love. With the second, we love another for our own sake: eros.” (Vacek, cited in Black, 116)

## Self-love

>- What is the importance of self-love?
>- Self-love is important because:
>     - It overcomes unhealthy self-loathing;
>     - It can give a positive direction to our lives;
>     - It gives us a positive self-identity;
>     - It leads us to preserve our lives and to actualise ourselves.
>- In the end, even our love for God is derived from our own needs: “If God did not fulfill our need then there would be no reason to love Him.” (Black, 116)
>- Eros, therefore, is real love for others, although it is compatible with (and based on) self-love: “It does not seem unreasonable to suggest that we love others partly for what they can do for us”

## Eros

>- Black distinguishes three forms of Eros:
>     - “There is the sensual *epithymic* form [Greek: epithymia = desire], where the loved one gives us sexual sense pleasure.”
>     - “In the *psychological* form eros seeks the other because they make us comfortable and cheerful.” 
>     - “With *spiritual* form of eros, we hope to share in the sublimity of the beautiful object and its religious form.
>     - “[E]ros wants God to be perfect because otherwise our quest for the perfectly fulfilling good would seem thwarted.” (Black, 117)
>- See how the latter one makes a bridge between Plato’s forms and God.

## Eros has been suppressed in the Christian tradition

>- Black makes the point that the erotic element has been suppressed in the Christian tradition, although it can often be found in art (p.119)
>- Two approaches to spirituality (p.121):
>     - One approach “places the stress on detachment, denial of desire, and the power of the intellect.”
>     - The other approach “emphasizes attachment, the release of desire, feeling, and the body.”
>- Joan Timmerman: “Those who deny their bodies and their feelings, thinking that the real self is the mental subject, are never wholly available. Some part, the vital, spontaneous part, is always under constraint. Touch is always feared.” (p.121)

## Conclusion

>- What is the conclusion of Black’s paper?
>- The tension between Christian Eros and Agape does not need to be as strong as seems to be in Nygren.
>- We can understand both Eros and Agape as results of proper (moral) self-love.
>- Self-love (and, therefore, “selfish” Eros) don’t need to be morally bad, but can be the basis for self-acceptance and positive Christian love.
>- Both art and modern feminist authors show how Eros can return to its proper place in Christian theology.
>- “The renewed flight of eros in certain quarters of ethics invites us to reconsider issues concerning power and knowledge, self-love and self-giving, the sexual and the sacred and the many guises of desire and pleasure. The experience of and reflection on eros connects humans to the mysterious, the perplexing, the vulnerable and attractive dimensions of the other.” (p.126)

# Richardson on Christian Agape

## C. Richardson: “Love, Greek and Christian” (1943)

>- After discussing Plato and Aristotle, Richardson turns to Christian love (p.178)
>- “Thus Christian agape is something which is both realized and yet not fully achieved in mortal existence. It is realized in so far as men pass from self-centeredness to a truer selfhood (a state of growth which the Christian calls “sanctification”); but it is unrealized in the sense that they never fully overcome the barriers which separate them from their fellows.” (p.178)
>- Two senses of Agape: (1) Love towards God; and (2) Love towards one’s fellows (p.179). We cannot love both in the same way.
>- Agape towards others is (as in Aquinas and Aristotle) “eu-poiein,” “to do good” to others.
>- Agape towards God means serving God.

## The historic evil of self-love

>- “Aquinas can say: ‘The good look upon their rational nature ... as being the chief thing in them. On the other hand, the wicked reckon their sensitive and corporeal nature ... to hold the first place.” (p.181)
>- This led to the “repressive nature of medieval asceticism.” (We saw some of that in the discussion of the Desert Fathers, above).
>- Martin Luther (1483-1546) was opposed to the idea that humans can try to “earn” God’s love by having particular properties.
>- He saw God’s love as entirely “bestowal” love.
>- “God's love,” he wrote, “does not *find,* but *creates* its lovable object.” (p.182)

## Meanings of Eros

>- Sexual passion.
>- Passion for creativity (p.183).
>- The cosmic principle in mysticism. That is the meaning that it has when we contemplate the Platonic Form of ideal beauty. “Eros descends from above ... and turns all things toward the divine beauty” (Proclus Lycaeus, 412-485 AD).
>- Conclusion: Eros is a creative force: “To me eros is concerned with the experience of being enraptured in the moment of creativity.” (p.184)
>- It is fulfilled in the harmony of self-affirmation and self-abandon.
>- Christianity has, so far, failed to unite Eros and Agape. But these two must be balanced.











