---
title:  "Love and Sexuality: 11. Is romantic love universal?"
author: Andreas Matthias, Lingnan University
date: October 3, 2019
...

# Is romantic love universal?

## The universality of romantic love

>- Is romantic love universal or does it depend on particular historical events that happened in the West?
>- Is romantic love therefore a typically Western phenomenon, or does it exist in similar ways in other cultures? 
>- What do you think? How could we decide the question?

## The argument for universal romantic love

>- If romantic love was *universal* we should be able to find it practised in similar ways in different cultures that don’t share a common history.
>- But this is not easy to do. Can you see why?
>- Today it is difficult to find independent cultures. Most of the world’s cultures have been heavily influenced by Western ideas, often through mass media: comics, movies, books, music.
>- Therefore, if we want to find evidence for the universality of romantic love, we’d have to look at:
>     - Historical accounts of different cultures;
>     - Old myths, stories and literary works;
>     - Cultures that have had little contact with the West: indigenous, largely isolated cultures in remote areas Africa, South America and parts of Asia.


## Why would romantic love be universal?

>- If we want to claim that romantic love is universal, we should also be able to provide an explanation for *why* this would be the case.
>- How could we do that?
>- If you want to claim that there is something that unites us, despite all our differences in culture, then this is likely some biological factor that is common to all human beings.
>- Therefore, we would look at a biological explanation for romantic love.

## The evolutionary advantage argument

>- In our (Western) conception of biology and evolution, we could try to find a reason why romantic love provides an evolutionary advantage over other concepts of love.
>- If we could do that, we would not only justify the universality of romantic love, but also leave some room for (less well-adapted) societies that have *not* developed the same concept.

## Countering the evolutionary advantage argument (1)

>- Evolutionary explanations of this type are hard to disprove with counter-examples, because one can always claim that the particular counter-example is just some less-fit society that will in future die out.
>     - Since Western culture continues to expand, this claim is very hard to disprove. Indeed, cultures that do things in a non-Western way are rare and often threatened by extinction.
>     - But is this because they are less adapted in terms of their love concepts, or is it because the world has other incentives to adopt Western culture (economic ones, for instance)? Or is it because we have the power to grab other cultures’ living spaces away from them and push them towards extinction? (Australia, Amazon and many others).

## Countering the evolutionary advantage argument (2)

>- So, rather than pointing towards the factual evolutionary success of a cultural practice (for which the reasons may be unclear) it is necessary for the proponent of an “evolutionary advantage” theory to actually make a plausible case *why precisely* a particular practice is advantageous.
>- The explanation itself has to be convincing. We cannot only make an argument from the historical success of cultures that have that practice.


# Thesis I: Romantic love is the result of historical developments

## Beigel, H.G.: Romantic Love.^[American Sociological Review, Vol. 16, No. 3 (Jun., 1951), pp. 326-334]

>- Three phases of romantic love:
>     - The origin of courtly love in the twelfth century;
>     - Its revival at the turn of the nineteenth century;
>     - Its present state and significance for marital selection.

## Courtly love

>- “Courtly love ... institutionalized certain aspects of the male-female relationship outside marriage.”
>- “In conformity with the Christian concept of and contempt for sex, the presupposition for [courtly love] was chastity. ... Such love was deemed to be impossible between husband and wife.”


## The loss of family ties

>- “Starting in the fourteenth century, the dissolution of the broader family had progressed to the point where its economic, religious, and political functions were gone.”
>- “With increasing urbanization the impact of social isolation made itself felt upon the individual.”

## The Romantic movement

>- “The Romanticists rebelled against the progressing de-humanization, all-devouring materialism and rationalism, and sought escape from these dangers in the wonders of the emotions.”
>- In the basic feelings of humanity they hoped to find security and a substitute for eliminated cultural values.”

## Beigel, summary

>- “To summarize: courtly love, romantic love, and their modern derivative should be considered cultural phenomena evolved from basic human feelings that have gradually developed forms useful as replacements for discarded or decaying cultural concepts.”
>- “Love aims at and assists in the adjustment to frustrating experiences.”
>- You see here an attempt to justify the development of romantic love from the historical contingencies and necessities of:
>     - The courtly period’s values and family structure; and
>     - The loss of family ties and the commercialisation of society towards the 19th century (the high point of Romanticism).


## Lindholm: Anthropology vs sociobiology

>- Two different approaches:
>- “Anthropological students of emotion are interested in discovering when and where romantic love occurs, and in correlating its emergence with particular social and psychological preconditions.” (Lindholm, RLA, 13)^[Lindholm, Charles (2006): Romantic Love and Anthropology. Etnofoor 10:1-12]
>- “Those influenced by sociobiology, in contrast, believe love must necessarily appear in all human societies, since it is genetically ingrained.”
>- So, the question is: Is romantic love a cultural practice dependent on society, or is it a biological phenomenon that appears in all societies in the same way?

## Love and marriage

>- One reason to assume that cultural practices determine our understanding of romantic love is that, in other cultures, love and marriage don’t always go together.
>- “In fact, in most of the complex societies for which we have records of romantic passion, conjugal love between husband and wife was considered both absurd and impossible.” (RLA 14)
>- Why would this be?
>     - The membership in one’s father’s clan determines claims to property, leadership or honour.
>     - Marriage created connections to other clans and their respective claims.
>     - “In this context matrimony was too important a matter to be decided by young people swept away by passion. Rather, marriage arrangements were negotiated by powerful elders whose job was to advance the interests of the clan -- much as royal marriages are still arranged today.” (RLA 14)


## Romantic feelings outside of marriage

>- “Because love with one's spouse was next to impossible, romantic feelings were directed instead toward individuals one could not marry.” (RLA 15)
>- This could cause problems:
>     - In Tokugawa Japan (1603-1868), “love dramas always revolved around the conflicts caused by relationships between respectable men and their courtesans.” (RLA 15)
>     - In ancient Rome, powerful men would fall in love with slaves.
>     - “Roman poets idealized their beloved slave prostitutes as domina, literally reversing the role of master and slave.”
>- In order to avoid such problems, many societies explicitly stress the chastity of romantic relationships (like in the Middle Ages).

## The Marri Baluch (1)

>- Tribes of people living in Pakistan, Iran and Afghanistan.
>- “The Marri inhabit a harsh, isolated and unforgiving world. They are highly individualistic, self-interested and competitive, and expect opportunism and manipulation from all social transactions. Their personal lives are dominated by fear, mistrust, and hostility; secrecy and social masking are at a premium, while collective action and cooperation are minimal.” (RLA 18)

## The Marri Baluch (2)

>- But romantic relationships are idealised.
>- A love affair implies “absolute trust, mutuality, and loyalty; such a love is to be pursued at all costs. Romance is both the stuff of dreams, and of life. Frustrated lovers among the Marri may commit suicide, and become celebrated in the romantic poems and songs that are the mainstay of Marri art.”
>- Romance, for the Marri, is “absolutely opposed to marriage, which is never for love.”
>- While marriage is public and officially endorsed, romantic love has to be kept secret and is dangerous. It is also non-sexual (RLA 22)


## Different societies and their ideas of romantic love (1)

>- In rigid, antagonistic (Marri) and complex societies (courtly, ancient Roman, old Japanese), “the idealization offered by romantic love offers a way of imagining a different and more fulfilling life. But because of the objective reality of the social environment, romance can never form the base for actually constructing the family ... It must instead stand against and outside of the central social formation.” (RLA 24)

## Different societies and their ideas of romantic love (2)

>- In societies with fluid social relations, that are individualistic and operating in insecure environments, people “may find meaning and emotional warmth in the mutuality of romantic relationships. Romance in these societies is associated with marriage, since the couple is idealized as the ultimate refuge against the hostile world, and functions as the necessary nucleus of the atomized social organization.” (RLA 25)


# Thesis II: Romantic love is universal

## Schiefenhoevel: Symptoms of romantic love

>- Romantic love can be identified by particular “symptoms” (that are similar to addiction):
>     - “Electrified” reactions, elated, joyous feelings, “butterflies in the stomach”, increased heartbeat and sweat production, changed body posture and facial expression ...
>     - when thinking of him/her, when her/his name is said by someone, when the other comes into view, when one is together with the other ... (Schiefenhoevel 40)^[Schiefenhoevel (2009): Romantic Love. Human Ontogenetics 3(2).]
>- For Schiefenhoevel, these are expressions of a deeper, biological mechanism.
>- The secrecy of romantic love, for example, “may well be another endowment of our evolutionary past when powerful alpha males were trying to prevent other males to copulate with females ...”

## The universality of romantic love

>- Ancient texts show that romantic love is universal, even in Western history, where it has been disputed (see previous reading!)
>     - Helena and Paris;
>     - Heracles and Omphale;
>     - Dido and Aeneas;
>     - Medea and Jason. (Schiefenhoevel, 41-42)
>- Middle Ages, Walther von der Vogelweide: “Under der Linden”;
>- Love song of an Eipo woman (New Guinea);
>- ... and other examples (Schiefenhoevel, 46-47)

## The evolutionary role of romantic love (1)

>- “The more we know about the underlying biology of our erotic, sexual and social feelings the more it becomes evident that the corresponding perceptions, feelings, and behaviours are common human heritage, basically part of all people, not likely to be dramatically altered by culture.”
>- “I am sure that the universality of romantic love will be increasingly demonstrated by future crosscultural and other research.” (Schiefenhoevel, 48-49)

## The evolutionary role of romantic love (2)

>- “Possibly then, this transformation of emotions to an artistically shaped metalevel is also a biologically efficient enhancement mechanism: One may live one’s highs and peaks again and repeatedly by creating a poem or song capturing them. One’s feelings will, thus, become even more powerful. This could have been a base for its becoming an evolved trait.” (Schiefenhoevel, 48-49)

## The evolutionary role of romantic love (3)

>- “My hypothesis is that the psychic condition of being involved in romantic love functions as honest signal.” (Schiefenhoevel, 48-49)
>- “Honest” signals are signals of interest that cannot be faked: erection, sweat patterns and others.
>- Such signals are valuable because they allow the other party to see that the lover’s interest is genuine.
>- And genuine interest is better for procreation and the stability of the family.
>- “To have a powerful, convincing love song made by somebody, to receive other signs of his/her deep involvement, must have always exercised a strong attraction. As women are the ones who choose partners for reproduction ... it is likely that expressing romantic love ... in poetry, song or otherwise, should be a male form of courtship behaviour.”


# Thesis III: Romantic love is universal, but expressed differently in different cultures

## Source

>- Victor Karandashev (2015): A Cultural Perspective on Romantic Love

## China (1)

>- In early Chinese history, attitudes toward passionate love and sexual desire were generally positive.
>- “In the Late Empire (approximately 1,000 years ago), when the Neo-Confucianists gained political and religious power, Chinese attitudes gradually altered and became more repressive concerning sexuality.”
>- “Erotic art and literature were often burned. Since neither spouse had chosen each other, it mattered little whether or not they were sexually attracted to each other. Their primary duty was to procreate.”
>- “The husband was assumed to be biologically destined to seek satisfaction with a variety of women. ... The woman was not  so destined; her chief function was to give birth to children, and she was expected to remain faithful to her husband (Murstein, 1974, p. 469).” (Karandashev, 6)

## China (2)

>- “Displays of love outside marriage were restricted. Even in the event that the partners were highly attracted to each other, it was contrary to custom to express the slightest degree of public affection.”
>- In the People’s Republic of China (after 1949):
>     - Party policy constructed an altruism which assumed that men and women work hard during the day, without being “deflected or confused” by love, sexual desire, or any strivings for private happiness (Gil, 1992, p. 571). 
>     - “True” happiness was based on spiritual rather than material enjoyment, on public rather than private interest, on collective welfare rather than on individual happiness” (Murstein, 1974, p. 482).^[Karandashev 6-7]


## Europe

>- Medieval England (before 12th century): Love is self-sacrificing and unselfish, similar to friendship.
>- 12th-14th centuries: Growth of the courtly love ideal. “The key feature of courtly love was suffering and longing due to separation from the loved one.”
>- 16th-17th centuries: Shakespeare, “love was described as a consuming passion, strong illness, or powerful force that is impossible to resist.”
>- 20th century: “Relaxation of sexual morals in Europe and the “sexual revolution” of the 1960s to early 1970s. Furthermore, some expressions concerning love began to refer to sexual desire.” (Karandashev 7)

## Anthropological studies

>- Jankowiak and Fischer (1992) explored romantic love in 166 cultures around the world.
>- Indicators of love: 
>     - young lovers talk about passionate love, 
>     - they recount tales of love, 
>     - sing love songs, 
>     - and speak of the longings and anguish of infatuation.
>- The researchers found that romantic love was present in 147 out of 166 cultures (88.5%). 
>- For the remaining 19 cultures, there were no signs indicating that people experience romantic love (Karandashev 8).
>- But in the original paper, Jankowiak and Fischer guess that it is the lack of material and research that makes these cultures appear to not have romantic love (for example the absence of recorded songs), rather than a true absence of the phenomenon.


## Biological basis of romantic love (1)

>- Bartels and Zeki (2000) interviewed young men and women from 11 countries ... who claimed to be “truly, deeply, and madly” in love and then used fMRI (brain imaging) techniques to identify the corresponding brain activities.
>- The authors concluded that passionate love suppresses the activity in the areas of the brain responsible for critical thought.
>- Passion also produced increased activity in the brain areas associated with euphoria and reward, and decreased levels of activity in the areas associated with distress and depression.
>- Brain scan using fMRI technique have found only small differences ... even if comparing the results of two very different cultures like America and China (Xu et al., 2010). The brain regions associated with romantic love seem to be very primitive ones leading scholars to conclude that love feelings were always present in hominid evolution (Fisher, 2004).^[(Karandashev 9-10).]

## Biological basis of romantic love (2)

>- People who are madly in love and asked to think about their beloved tend to show activity in the ... area associated with euphoria and addiction. 
>- These findings confirm that passionate love is rooted in a very basic evolutionary system. 
>- Passionate love served to guarantee mate selection, long-term romantic relationships, and the survival of our species. (Karandashev 11)

## Individualism and collectivism (1)

>- Karen and Kenneth Dion (1991) found that people who are more individualistic exhibit less likelihood of ever having been in love.
>- Greater individualism was associated with a perception of their relationships as less rewarding and less deep.
>- Generally, the more individualistic a person, the lower the quality of experience of love for his or her partner. (Karandashev 12)
>- Dion and Dion (2005) found that people who are high in individualism tend to report less happiness in their marriages as well as lower satisfaction with their family life and friends.

## Individualism and collectivism (2)

>- In collectivistic cultures, people experience the dependencies in their lives being embedded in multiple relationships with their family and close friends. 
>- Collectivism is related to the view of love as pragmatic, based on friendship, and having altruistic goals (Dion & Dion, 2005). 
>- Women in collectivistic cultures endorse an altruistic view of love more commonly than women in individualistic cultures; they consequently place greater emphasis on a broader network of close friendships. (Karandashev 12)


## Americans, Lithuanians and Russians: The core of romantic love

>- Comparison between United States, Lithuania, and Russia (Karandashev 13)
>- People from all three countries agreed on the following characteristics as the “core” of romantic love: 
>     - (1) the eros component of love (physical attraction), 
>     - (2) the essence of altruistic love (agape), 
>     - (3) the tendency of lovers to engage in intrusive thinking about the beloved, 
>     - (4) a concept of transcendence: the feeling that the union of two lovers results in something more meaningful than just the two lovers.
>- There was agreement across all cultures that love is a strong feeling and that lovers ultimately want to be together. 
>- Only altruism was considered more important in the United States than in Lithuania and Russia.

## Americans, Lithuanians and Russians: The core of romantic love

>- Several differences among individualism (US) and collectivism (Lithuania and Russia): 
>     - Lithuanians and Russians resembled each other much more than either group resembles Americans. 
>     - Russians and Lithuanians perceived love as an unreal fairy tale and expect it at some point either to come to an end or to transfer to a more real and enduring relationship that lacks the initial excitement of romantic love. 
>     - Only then, they believed, “real” love and friendship set in. 
>     - In the United States, participants perceived romantic love as more realistic and less illusionary. 


## Americans, Lithuanians and Russians: The core of romantic love

>- Americans (in contrast with Lithuanians and Russians) also included friendship in romantic love.
>- Lithuanians and Russians fall in love much more quickly than Americans. 
>     - The study found that 90% of Lithuanians reported that they fell in love within a month or less, whereas 58% of Americans fell in love within a time frame of two months to a year (de Munck, Korotayev, de Munck, & Khaltourina, 2011). 
>- When Sprecher and her colleagues (1994) asked people of different nationalities whether they were currently in love, Russians were the people who were found to be most in love (67%), Americans were in the middle (58%), and the Japanese were the least likely to be in love (52%). (Karandashev 13)


## Emotional investment across countries (1)

>- Ingersoll-Dayton, Campbell, Kurokawa, and Saito (1996) compared how marriages develop over the long term in the United States versus Japan. 
>- In the United States marriages start out with a relatively high level of intimacy and the respective partners try to keep the intimacy of the relationship while maintaining a separate identity.
>- Japanese marriages, on the other hand, are at first characterized by many obligations that the married couple has to the other people in their social relationships. 
>- The intimacy develops later in life when other close family members, to whom the couple had obligations, die and when the husband becomes more willing to share affection with the wife. (Karandashev 14)

## Emotional investment across countries (2)

>- Schmitt (2006) and his colleagues explored cross-culturally emotional investment as a dimension of love: Loving, affectionate, cuddlesome, compassionate, and passionate.
>     - 15,234 participants from 48 countries.
>     - North American participants exhibited a higher level of emotional investment, significantly higher than those in all other regions of the world.
>     - East Asia had levels of emotional investment significantly lower than those of all other world regions.
>     - Tanzania, Hong Kong, and Japan were the countries with the lowest levels of emotional investment, whereas the United States, Slovenia, and Cyprus were the countries with the highest levels of emotional investment (Schmitt et al., 2009). (Karandashev 14)


## Karandashev’s conclusions

>- Romantic love is universal but expressed in different cultural patterns.
>- Love emotions are experienced by many people, in various historical periods, and in most cultures of the world. 
>- Yet, these feelings display diversity - cultures influence how people feel, think, and behave being in romantic love.
>- Thus, love is universal, but still culturally specific.
>- The cultural perspective is as much powerful as evolutionary heritage in understanding of love.
>- Evolutionary psychology and neuroscience explain why passionate love is universal and equally intense in different cultures, while cultural influence demonstrates how romantic love is expressed in multiple cultural forms.
>- The research findings indicate that universal features primarily relate to love *experience,* while culturally influenced features are ones that pertain to the *expressions of love:* cultural rituals of love.


## The End. Thank you for your attention!

Questions?

<!--

%% Local Variables: ***
%% mode: markdown ***
%% mode: visual-line ***
%% mode: cua ***
%% mode: flyspell ***
%% End: ***

-->
