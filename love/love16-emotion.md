---
title:  "Love and Sexuality: 16. Is love an emotion?"
author: Andreas Matthias, Lingnan University
date: October 29, 2019
...


# Theories of emotion

## Sources

>- Pismenny, Prinz (2017): Is Love an Emotion? In: C. Grau and A. Smuts (ed.) The Oxford Handbook of Philosophy of Love, New York: OUP.
>- Smuts, A. (2014). Normative Reasons for Love, Part I. Philosophy Compass, 9(8), 507-517.
>- Smuts, A. (2014). Normative reasons for love, part II. Philosophy Compass, 9(8), 518-526.


# Smuts: Normative Reasons for Love

## Cognitive theory of emotions

>- The cognitive theory of the emotions holds that emotions are object-directed attitudes that essentially involve evaluations. (Smuts, 510)
>- Therefore, we can always say: “X has the emotion E that F,” where X is a person, E is an emotion and F is a fact.
>     - “I am *afraid that* this disease will kill my tomato plants.”
>     - “Jack is *angry that* Jill left the bucket on the hill.”
>     - Emotions are always *about* something, they have an *object.*
>- As opposed to proper emotions, *moods* do not take specific objects:
>     - My brother is grumpy today.
>     - My roommate is aggressive.

## Is love a normal emotion?

>- Three reasons against the thesis that love is an emotion (Smuts, 510, bottom):
>- Reason One: Love is not episodic like fear.
>     - “Episodic” means that it comes and goes in response to some particular, external condition.
>     - For example, my fear of spiders is “activated” when I actually see a spider. 
>     - When no spider is present, I have no actual fear. 
>     - I might *know* that I am someone who is afraid of spiders (as a general disposition or feature of my personality), but I am not *actually* afraid at any moment when no spiders are present.
>- It seems that love is more like a disposition. When I say that “I love my child,” then I mean a general condition, like “I am afraid of spiders.” I don’t mean that I necessarily actually feel that love right now.

## Is love a normal emotion?

>- Three reasons against the thesis that love is an emotion (Smuts, 510, bottom):
>- Reason Two: There is no evaluation to distinguish love from other kinds of affect.
>     - According to the cognitive theory of love, it is the evaluation we make that distinguishes one emotion from the other, not how it feels.
>     - Many emotions feel similar (fear and anger).
>     - But fear and anger differ in the evaluations they involve: anger involves having been wronged. Fear follows the judgement that something that one values is in danger.
>     - Similarly: jealousy and envy; pride and joy.
>- Only love does not have a plausible evaluation, except that the object is lovable.


## Is love a normal emotion?

>- But that, Smuts says, “is circular and entirely uninformative. Just what is it to judge an object to be loveable?”
>- *Do you agree with Smuts here? Do we need to agree with him that saying that an object is lovable would be circular and uninformative? Is there any way that we could plausibly judge what kinds of objects are lovable?*
>     - Remember Velleman (based on Kant): Lovability is a contingent fit between us and somebody else’s behaviour, based on the fundamental recognition of the other’s unlimited worth as a human being (=dignity).
>     - This might be a kind of evaluation that could satisfy Smuts’ requirement above.



## Is love a normal emotion?

>- Three reasons against the thesis that love is an emotion (Smuts, 510, bottom):
>- Reason Three: Emotions require that we care about something that will be affected. (Smuts, 511)
>     - “Emotions are concern-based. They are evaluations of situations based on our concerns.”
>     - “When we care about something, we feel fear when it is threatened, happy when it is benefited, angry when it is harmed, and hopeful when it stands to benefit.”
>     - But then, where is the place of love? We want to say that “a mother’s love for her son *explains why* she is angry when he is injured, fearful when he is threatened, happy when does well ...” [and so on].
>     - So, here love is not just one of the other emotions, but the principle of explanation behind all the more specific emotions! It is *the reason* to have emotions, and not itself one of these emotions.
>- Conclusion: It doesn’t seem like love is a normal emotion.


## The rationality of emotions

>- What makes an emotion rational or appropriate, as opposed to an emotion that is irrational? How can we judge the rationality of emotions?
>     - An emotion has to be reasonable;
>     - it has to fit the situation;
>     - its intensity should be proportional to its object;
>     - it should be in our own long-term best interest;
>     - it must be understandable. We must be able to understand why someone behaves that way. (Smuts, 512)
>- Do these apply to love?
>     - Most criteria don’t work very well for love, showing that love is not a normal, rational emotion.


# Pismenny/Prinz: Is love an emotion?


## Characteristics of love (Pismenny/Prinz, 1-2)

>- Love is not felt all the time. It can be occurrent (actually felt) or dispositional (being in love without *feeling* it all the time).
>- Love may be pleasant or unpleasant.
>- As a mental state, love is always directed towards a particular object. It is intentional. One is always in love with a particular person, not just “generally in love.”
>- Lovers tend to idealise the beloved.
>- Love’s occurrence or absence are not in our control. We cannot simply choose to stop loving.



## Basic features of emotions (1)

>- They are *reactions* to experiences.
>     - Encountering a spider causes fear.
>     - Listening to a nice piece of music causes joy.
>     - As such, they are not directly under our control.
>- Emotions *feel* in a particular way.
>     - Dispositional emotions (for example, a fear of spiders when no spider is present) must first be triggered to be felt.
>- Emotions have two ‘objects’:
>     - The *target*: the thing which the emotion is directed at (the spider, the wild dog, ...)
>     - The *formal object*: the property that we fear in the target (the dangerousness of the spider or wild dog).


## Basic features of emotions (2)

>- Emotions therefore are *felt evaluations.*
>- The function of emotions is to track what matters to us.
>- As evaluations, emotions can be correct or incorrect, rational or irrational.
>- An emotion is justified if “the formal object that it is picking out is actually provided by the target.”
>     - A fear of that dog is justified if that dog is actually dangerous.
>     - A fear of that spider might not be justified if that spider is not dangerous. (Pismenny/Prinz, 2)


## Emotions as cognitive states (1)

>- “Cognitive theories” see emotions as beliefs or judgements.
>- To be afraid of a dog means to believe that there is a dog present and that it is dangerous.
>- But what about love? Can love be described as a cognitive state in this way?
>     - “Perhaps in love one would believe that a particular person has certain properties and judge them to be lovable.” (Pismenny/Prinz, 3)
>     - But if this was the case, such properties should always bring about the emotion of love.
>     - Compare spiders: every spider will bring about the belief that the spider is dangerous and the desire to run away from it.
>     - But not every lovable person will bring about the “emotion” of love towards them. We are also often incapable of even saying *what exactly* the lovable properties of a person are.

## Emotions as cognitive states (2)

> “If the formal object of lovability is grounded in the focal properties the beloved possesses, then, as with other emotions, the conditions that make a given emotion apt generalize over all cases for that emotion. So just as fear is always apt in the presence of danger, so too, it would seem, love should always be apt in the presence of certain focal properties of the beloved.” (Pismenny/Prinz, 3)

## Emotions as perceptions of the body (1)

>- According to William James and Carl Lange, emotions are how particular bodily changes subjectively *feel* to us.
>- Fear has a particular way of affecting the body: faster heartbeat, increased blood pressure, and other such biological effects.
>- When we feel our body having these reactions, we call the resulting feeling “fear.”
>- Could love be described in this way?
>     - Perhaps we can agree that fear has a particular way of being expressed in body states.
>     - But it does not seem that we can say the same about love.
>     - Love seems to express itself in many different ways in the body. We can feel happiness, sadness, longing, sexual arousal and other bodily reactions that are all caused by love.

## Emotions as perceptions of the body (2)

> “But here we encounter an obvious difficulty. Love, even when romantic, can be felt in the absence of arousal. There can be cozy and endearing feelings of love, longing feeling, even feelings of fury, occasioned by mistreatment by a lover. All of these are manifestations of love. Conversely, we can feel arousal in response to those we do not love. This is a problem that has no obvious fix. The problem is that love has a variable phenomenology.” (Pismenny/Prinz, 4)

## Emotions as perceptions of value

>- We have already seen theories that view love as a response to the perceived *value of the beloved.*
>     - For example, Velleman sees love as our response to other people’s dignity.
>     - Singer sees love as creating value in the beloved through a process of bestowal.
>- But if love was an emotion, we should be able to say when it is rational or justified.
>- Theories that link love to values don’t clearly specify *what* values would justify love.
>- “Love lacks reliable causes.” (Pismenny/Prinz, 6)


# Basic, non-basic emotions and sentiments


## Basic emotions (1)

>- Emotions are called “basic” when:
>     - They have “biologically prepared” responses; and
>     - They are not themselves made up of other emotions. (Pismenny/Prinz, 6)
>- For example, fear vs nostalgia:
>     - Fear exists in babies and animals. We are born with the capacity to experience fear. Our reaction to fear is biologically determined.
>     - Nostalgia seems to be learned later in life. Animals don’t have it.


## Basic emotions (2)

>- Basic emotions seem to have their own, typical facial expressions, which stay the same across cultures: anger, disgust, fear, happiness, sadness, and surprise.
>- People from different cultures recognise the same emotion when shown a picture of a face with one of the basic emotions.
>- Basic emotions come with particular changes in the body (heart beat, blood pressure and so on) that cause the individual to show particular, automated responses.
>- These responses are quick and automatic and not based on reasoning. For example, you cannot just lose your fear of spiders by telling yourself that the particular kind of spider is not dangerous.

## Is love a basic emotion?

>- Is love a basic emotion?
>- Just that love is common across cultures does not mean that it is a basic emotion. Nostalgia, for example, is also common, although it is not a basic emotion.
>- Among mammals, long-term, exclusive bonding is rare (Pismenny/Prinz, 8). Most animals do not restrict sexual activity to one partner.
>- Even in human beings, monogamy is relatively recent (since the rise of agriculture).
>- We already saw that marriages were often not based on love.
>- And romantic love only goes back to the 18th century.


## Non-basic emotions

>- Non-basic emotions are blends of basic emotions. Examples?
>- “Despair may be a blend of fear and sadness ... contempt may be a blend of anger and disgust.” (Pismenny/Prinz, 8)
>- *Is love a combination of lust and attachment?*
>- “Lust is characterized as sexual desire, which can be short-lived, can be satisfied, and its target is fungible.”
>- “In contrast, romantic love is characterized as an obsessive passionate state that has a normal shelf life of about eighteen months to four years, and its targets are deemed to be nonfungible.”
>- “Attachment is characterized by a much more calm kind of state that can last one’s lifetime; it is something that appears after romantic love comes into existence, provided that it succeeds in forming a relationship. All three of these can exist separately.”


## Sentiments (1)

>- Sentiments are dispositions towards things (ways in which we tend to relate to these things) that are caused by repeated interactions with these things.
>- Sentiments *cause* emotions:
>- “Thus, if you are hopeful that some end can be achieved, then you normally ought also to be afraid when its accomplishment is threatened, relieved when the threat does not materialize, angry at those who intentionally obstruct progress toward it, and satisfied when you finally achieve it (or disappointed when you fail).” (Pismenny/Prinz, 10)


## Sentiments (2)

>- Criticism of love as sentiment:
>     - Sentiments seem to narrow. A sentiment is a disposition towards particular emotions, but love also causes thoughts, beliefs and behaviours (the belief that the beloved is worthy of love, the behaviours of wanting to be with the beloved and so on).
>     - Like emotions, sentiments are triggered by their target. They do not arise spontaneously.
>     - Love, in contrast, is less reactive. We might be in love even without being in the presence of the beloved. (Pismenny/Prinz, 11)


# Love as a syndrome

## Syndromes (1)

>- A syndrome is “an organized set of responses (behavioural, physiological, and/or cognitive).” ^[Averill, James (1985). “The Social Construction of Emotion: With Special Reference to Love.” The Social Construction of the Person edited by K.J. Gergan and K.E. Davis, pp 89–109. New York: Springer.]
>- The love syndrome has several features:
>     - idealization of the loved one
>     - suddenness of onset (‘love at first sight’)
>     - physiological arousal
>     - commitment to, and willingness to make sacrifices for, the loved one.” (Pismenny/Prinz, 11)
>- Ronald de Sousa: Love is “a syndrome: not a kind of feeling, but an intricate pattern of potential thoughts, behaviors, and emotions that tend to ‘run together’.”
>- Greek: syn-: together; dromos: way. Literally: the walking together along a way, travelling together.

## Syndromes (2)

>- Syndromes are often used to describe mental health conditions (Pismenny/Prinz, 12).
>- We can also use them to describe other illnesses.
>- A common cold can cause a set of responses:
>     - Bodily/physiological responses: fever, a running nose, cough.
>     - Behavioural responses: inability to stand up for long periods, tendency to fall asleep.
>     - Cognitive responses: slowness in thinking, inability to concentrate, no desire to engage in sports.
>- These responses are different from person to person. Other responses are possible, and the ones mentioned might be absent.
>- Syndromes are not rational responses.
>- They might be influenced by culture, but are not *created* by a particular culture.

## Reasons for love to be a syndrome

>- Syndromes don’t need to have a formal object (while emotions do).
>- Love does not need to be justifiable by reasons. The same is true of syndromes.
>- Love is not associated with a particular bodily state. The same is true of syndromes.
>- Love is not innate and not limited to brief episodes. Syndromes don’t need to be either.
>- Love is not a sentiment because it encompasses beliefs and behaviours as well as feelings. This is also true of syndromes. (Pismenny/Prinz, 13)
>- “Love is not a detector of properties; it is a way of being in the world.”


