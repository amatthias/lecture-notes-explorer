# Introduction to the course
## Welcome!
## Presentation of course outline
# Introduction to the course
## Welcome!
## Talk about
## Notes about this class
## Any questions?
# What is love?
## Your ideas?
## Does love depend on the properties of the beloved? (1)
## Does love depend on the properties of the beloved? (2)
## Appraisal and bestowal
## Greek and Latin words for love
## The moral side of loving
## The properties of love
## Objects of love
## Love and sex
## The End. Thank you for your attention!
# Plato and the Symposion
## Why read the Symposion at all?
## Symposia
## Plato
## Socrates
## After Socrates’ death
## The Symposion
## People in the text (only the main ones) (1)
## People in the text (only the main ones) (2)
# Structure of the text
## Narrative structure (1)
## Narrative structure (2)
## The seven speeches
# The speeches
## How to read the text
## Phaedrus (literary man)
## Pausanias (lawyer)
## Eryximachus (doctor)
## Aristophanes (comic writer)
## Agathon (tragic poet)
## Socrates (also representing Plato’s views)
## Diotima’s ladder (1): Preliminary clarifications
## Diotima’s ladder (2): Immortality
## Diotima’s ladder (3): The progression of love
## Diotima’s ladder (4): Recap [211d, p.59]
## Alcibiades (Socrates’ lover)
# The text itself
## Now let’s look at the text!
## The End. Thank you for your attention!
# Plato’s views on love
## Diotima’s ladder
## Erotic love in ancient Greece
## Sexual identity (from: Cooksey, Plato’s Symposium: A Reader’s Guide)
## Ancient Greek eros is not symmetrical
## The word ‘erosic’
# Plato: Universals and ideas
## Love as desire (1)
## Love as desire (2)
## Perfect circles
## Forms (Ideas)
## Forms and reality
## The Symposion itself as a metaphor
## Forms/Ideas (Singer)
#  Plato’s influence and other notes
## Plato and Christianity
## Christian version
## Singer’s criticism of merging and wedding unions
## Wedding and merging
## Love and emotion
## Platonic exclusivity?
## Appraisal and bestowal
## Devaluing of human love
## Plato today
## Summary of Singer’s criticism
## The End. Thank you for your attention!
# Intro
## Aristotle (384-322 BC)
# Virtues
## What is a virtue?
### Virtue (Greek: *arete*):
## Virtues are good only in the right amount
## What is the right amount of a virtue? (1)
## What is the right amount of a virtue? (2)
## What is the right amount of a virtue? (3)
## What is the right amount of a virtue? (4)
## What is the right amount of a virtue? (5)
# Phronesis ("practical wisdom")
## Phronesis (practical wisdom)
### Definition:
## Do children have phronesis?
## How do people acquire phronesis?
## Do older people have *more* phronesis?
## Can all people acquire phronesis equally well? (1)
## Can all people acquire phronesis equally well? (2)
# Eudaimonia
## Life with perfect phronesis (1)
## Life with perfect phronesis (2)
## Eudaimonia
### Eudaimonia:
## Eudaimonia is the highest good
## Eudaimonia (the word)
## Can a criminal be eudaimon? (1)
## Can a criminal be eudaimon? (2)
## Can a criminal be eudaimon? (3)
## How do virtues become "deep?"
## Handwriting
## Mastering skills
## Mastering moral behaviour
## Three types of people (1)
## Three types of people (2)
## Three types of people (3)
# The End. Thank you for your attention!
# Introductory remark
## How to locate quotes
# Aristotle on friendship and love
## Virtue and function in Aristotle
## General characteristics of Aristotle’s theory of love (1)
## General characteristics of Aristotle’s theory of love (2)
## “Philia”
## Definition of friendship (1)
## Definition of friendship (2)
## Kinds of love and friendship
## Complete friendship
## Usefulness and pleasure
## Are usefulness and pleasure really different?
## Philia vs frienship
## Similarity and friendship
## Goodness and complete friendship
## Can we love things?
## Loving animals? (1)
## Loving animals? (2)
## Loving animals? (3)
## Loving a country (1)
## Loving a country (2)
## Loving a country (3)
# Plato and Aristotle
## Main differences between Plato’s Aristotle’s love (1)
## Main differences between Plato’s Aristotle’s love (2)
## Love as moderation between extremes
# Is Aristotle’s love a bestowal or appraisal love?
## Appraisal?
## Bestowal (1)
## Bestowal (2)
## Bestowal (3)
## Bestowal (4)
# Is love an emotion?
## Is love an emotion for Aristotle? (1)
## Is love an emotion for Aristotle? (2)
# Is (self-) love a virtue?
## Is Aristotle’s love essentially egoistic? (1)
## Is Aristotle’s love essentially egoistic? (2)
## Should we love ourselves? (1)
## Should we love ourselves? (2)
## Should we love ourselves? (3)
# Criticism of Aristotle’s concept of friendship
## Reason and emotion
## Eros
## Philia and choice
## Philia and persons
## Love and justice
# The End. Thank you for your attention!
# Christian love
## Christian love
## Subjects and objects of love
## St Paul (4 BCE - ~63 CE)
## Flesh and spirit (1)
## Flesh and spirit (2)
## Is Christian love exclusive? (1)
## Is Christian love exclusive? (2)
## St Augustine (Augustine of Hippo, 354-430 AD)
## St Augustine (2)
## St Augustine (3)
## St Augustine (4)
## Desert fathers (approx. 3rd-5th century AD)
## Desert fathers (approx. 3rd-5th century AD)
## Stories of the desert fathers (1)
## Stories of the desert fathers (2)
## Thomas Aquinas (1225-1274) (1): Amor and caritas
## Thomas Aquinas (2): God
## Thomas Aquinas (3): Desires in love
## Thomas Aquinas (4): Goodness
## Thomas Aquinas (5): Union and offices
## Thomas Aquinas (6): Union and offices
## Thomas Aquinas (7): Appraisal and office
## Thomas Aquinas (8): Appraisal and office
## Thomas Aquinas (9): Love from lack and love as friendship
## Anders Nygren (1890-1978): Agape and Eros (1930/1936)
## Anders Nygren (1890-1978): Agape and Eros (1930/1936)
## Criticism of Nygren
## Outka’s concept of Agape (Pope 1995) (1)
## Outka’s concept of Agape (Pope 1995) (2)
## Criticising “Agape as equal regard”
## C. S. Lewis: The Four Loves (1)
## C. S. Lewis: The Four Loves (2)
# Peter Black: The Broken Wings of Eros
## Five questions about Eros in Christianity
## History of the tension between Agape and Eros (1)
## History of the tension between Agape and Eros (2)
## Edward Vacek’s “three loves”
## Self-love
## Eros
## Eros has been suppressed in the Christian tradition
## Conclusion
# Richardson on Christian Agape
## C. Richardson: “Love, Greek and Christian” (1943)
## The historic evil of self-love
## Meanings of Eros
# Background
## Sources
## Where we are
## Mysticism (1)
## Mysticism (2)
## The Bible: Song of Songs (1)
## The Bible: Song of Songs (2)
## The Bible: Song of Songs (3)
## The Last Supper
## Asceticism
## The “ladder of perfection” (King, p.9)
# Medieval mysticism
## God as a person
## The ladder/mountain metaphor (1)
## The ladder/mountain metaphor (2)
## Freud on ladders and eros (Singer p. 182)
## Ladders in love (Richard of St Victor)
# The experiences of Christian mystics
## St. Bernard of Clairvaux (1090–1153)
## St. Bernard of Clairvaux (2)
## Hildegard of Bingen (1098–1179)
## Jan van Ruusbroec (1293–1381)
## St. Catherine of Genoa (1447–1510)
# The erotic element in mystical union
## St Gertrude (1256–1302)
## Mechtild von Magdeburg (1241–1299)
## Hadewijch of Antwerp (first half of thirteenth century)
## Hadewijch of Antwerp (2)
## Bernini “The Ecstasy of Saint Teresa” (1652)
## St Teresa of Avila (1515–1582)
## St Teresa of Avila (1515–1582)
## Conclusion
# The End. Thank you for your attention!
# Courtly love
## What is courtly love?
## What is courtly love?^[https://www.ancient.eu/Courtly_Love]
## What is courtly love?
## Stages of courtly love^[Barbara Tuchman (1978): “A Distant Mirror”]
# The first troubadour: William the ninth, Duke of Aquitaine
## William’s life (1)
## William’s life (2)
## William’s life (3)
## William’s life (4)
# Troubadours and their love poems
## Guillaume de Machaut: “Foy porter” (14th cent.)
## Guillaume de Machaut: “Foy porter” (14th cent.)
## Guillaume de Machaut: “Foy porter” (14th cent.)
## Guillaume de Machaut: “Foy porter” (14th cent.)
## Features of the poem
## Another love song of Guillaume de Machaut (14th cent.) (1)
## Another love song of Guillaume de Machaut (14th cent.) (2)
# Stories of romantic love
## Ancient, pre-courtly: Ovid, The Metamorphoses, Book X
## Orpheus and Eurydice
## Orpheus and Eurydice
## Orpheus and Eurydice
## Orpheus and Eurydice
## Orpheus and Eurydice
## Orpheus and Eurydice
## Orpheus and Eurydice
## Orpheus and Eurydice
#  The typical representative: Tristan and Isolde
## Tristan and Isolde (Iseult)
## Tristan and Iseult: The plot (1)
## Tristan and Iseult: The plot (2)
## Tristan and Iseult: The plot (3)
## Tristan and Iseult: The plot (4)
# A real-life story: Abelard and Heloise
## Abelard and Heloise (1)
## Abelard and Heloise (2)
## Abelard and Heloise (3)
## Abelard and Heloise (4)
## Abelard and Heloise (5)
## Letters - Heloise to Abelard
## Letters - Heloise to Abelard
## Letters - Heloise to Abelard
## Letters - Heloise to Abelard
## Letters - Heloise to Abelard
## Letters - Heloise to Abelard
## Letters - Heloise to Abelard
## Abelard and Heloise
## Abelard and Heloise
# Characteristics of courtly love
## Characteristics of courtly love (Wagoner)
## Characteristics of courtly love (Wagoner)
## Love and church
## Impossibility
## Otherworldly innocence (Wagoner)
## Otherworldly innocence
## Transcendent value of the beloved
## Obstacles
## The absence of fulfilment
## The role of reason and passion
## Self-destruction
## The role of sexuality^[Wagoner, following W.T.H. Jackson, “The Anatomy of Love”]
## Body and soul
## Tristan’s sword
## Obstacles and love
## The role of death
# Five criteria for the definition of courtly love (Singer II, 23)
## Sexual love between man and woman is something good
## Love ennobles both the lover and the beloved
## Jealousy
##  Sexual love cannot be reduced to a mere bodily function
##  Love is not necessarily related to marriage
##  Love is an intense, passionate relationship
##  Francis Newman on Courtly Love
# Platonism in courtly love
## Platonism in courtly love
## Idealization of the beloved (Singer II, 49)
## Idealization of love itself (Singer II, 50)
## The revolutionary and heretic character of courtly love (Singer II, 51)
## The love of nature (leading to romanticism) (Singer II, 54)
## The love of nature (leading to romanticism) (Singer II, 54)
## The End. Thank you for your attention!
# From courtly to romantic love
## Towards romantic love: Romeo and Juliet
## Differences between Romeo/Juliet and courtly love
## William Shakespeare: Sonnet 18
## Modern English
## Observations
# Examples of romanticism in literature and art
## Novalis (1772-1801)
## A typical representative: Love in “Wuthering Heights” (Emily Brontë, 1847)
## A typical representative: Love in “Wuthering Heights” (Emily Brontë, 1847)
##  Schelling’s Naturphilosophie (Singer II, 387)
##  Shelley (Singer II, 412): Essay on love^[Percy Bysshe Shelley (1792 – 8 July 1822)]
## Shelley, observations
#  The Romantics
##  Feeling rather than reason (Singer II, 285)
##  Oneness rather than dualism (Singer II, 288)
##  Is romantic love appraisal or bestowal love?
## Romance and revolt (1)
## Romance and revolt (2)
## Romance and revolt (Singer)
## The End. Thank you for your attention!
# Is romantic love universal?
## The universality of romantic love
## The argument for universal romantic love
## Why would romantic love be universal?
## The evolutionary advantage argument
## Countering the evolutionary advantage argument (1)
## Countering the evolutionary advantage argument (2)
# Thesis I: Romantic love is the result of historical developments
## Beigel, H.G.: Romantic Love.^[American Sociological Review, Vol. 16, No. 3 (Jun., 1951), pp. 326-334]
## Courtly love
## The loss of family ties
## The Romantic movement
## Beigel, summary
## Lindholm: Anthropology vs sociobiology
## Love and marriage
## Romantic feelings outside of marriage
## The Marri Baluch (1)
## The Marri Baluch (2)
## Different societies and their ideas of romantic love (1)
## Different societies and their ideas of romantic love (2)
# Thesis II: Romantic love is universal
## Schiefenhoevel: Symptoms of romantic love
## The universality of romantic love
## The evolutionary role of romantic love (1)
## The evolutionary role of romantic love (2)
## The evolutionary role of romantic love (3)
# Thesis III: Romantic love is universal, but expressed differently in different cultures
## Source
## China (1)
## China (2)
## Europe
## Anthropological studies
## Biological basis of romantic love (1)
## Biological basis of romantic love (2)
## Individualism and collectivism (1)
## Individualism and collectivism (2)
## Americans, Lithuanians and Russians: The core of romantic love
## Americans, Lithuanians and Russians: The core of romantic love
## Americans, Lithuanians and Russians: The core of romantic love
## Emotional investment across countries (1)
## Emotional investment across countries (2)
## Karandashev’s conclusions
## The End. Thank you for your attention!
# Kant’s view on duties and emotions
## Sources
## Kant's good motivation
### Kant:
## Motivation and inclination
## Benefiting friends
## Central ideas of Kant's system
## Kant: Overview (graphics)
## Notes on Kant (1)
## Notes on Kant (2)
## Derived and original value (worth)
## Love as a “pathological” feeling
## Duty out of inclination is selfish and unreliable
## Duty and inclination:
## Love that delights and love that wishes well
## Love cannot be commanded
## Beneficence can be commanded (1)
## Beneficence can be commanded
## Problems with Kant’s concept
## The un-loving love of Kant
# The no-reasons view
## No reasons for love
## No-reasons and hate
## The Relationship view
# Loving Harry Lime
## The trouble with Harry
## What should Anna do?
## Confucius on family ties
## Does Anna have a duty to not love Harry?
## Does Anna have a duty to not love Harry?
## Can we block our own emotions?
## The asymmetry of a duty to love
## The reason for the asymmetry
# Can love be commanded?
## Commandability
## The attitudes of love
## Is love more than attitudes?
# Can we control our emotions?
## Internal control and reasons for emotions
## 1. Creating and visualising reasons
## 2. Reflecting on reasons
## 3. Cultivating emotions
## External control
## Internal and external control
## The difficulty of cultivating emotions
## Is romantic love special? (1)
## Is romantic love special? (2)
## Romantic love is also rational
## Romantic love is also rational
## Romantic love is also rational
## The End. Thank you for your attention!
# Features of love
## Sources
## Characteristics of love
## Loving and liking
## Is liking instrumental?
# Love as union
## Love as union
## Love as a union of interests
## Nozick on love as a union (1)
## Nozick on love as a union (2)
## Love as union: Criticism (1)
## Love as union: Criticism (2)
## Love as union: Criticism (3)
## Love as union: Criticism (4)
## Love as union: Criticism (5)
## Love as federation of selves
# Love as life choice
## Love as a life choice (1)
## Love as a life choice (2)
# Love as robust concern
## Love as robust concern
## Criticism of the “robust concern” theory (1)
## Criticism of the “robust concern” theory (2)
## Criticism of the “robust concern” theory (3)
# Kant, dignity, and love as valuing
## Love as valuing
## Kant, dignity and love
## Velleman: Love is the appreciation of the dignity of others (1)
## Velleman: Love is the appreciation of the dignity of others (2)
## Velleman: Criticism
# Criticism of ‘appraisal’ theories of love
## Criticism of appraisal accounts in general (1)
## Criticism (2): Love of types rather than individuals
## Criticism (3): Fungibility
## Criticism (4): Substituting Nancy (1)
## Criticism (4): Substituting Nancy (2)
## Fungibility again
## Criticising exclusive appraisal love
## Human uniqueness (1)
## Human uniqueness (2)
## Human uniqueness (3)
# Does bestowal need appraisal?
## Bestowal of love
## Criticism of bestowal theories (1)
## Criticism of bestowal theories (2)
## Appraisal *and* bestowal
## Appraisal *and* bestowal
# Justifying love
## Sources
# Harry Frankfurt’s theory of love
## Kinds of love
## Frankfurt’s necessary features of love
## 1. Disinterested concern
## 1. Disinterested concern
## 2. Love is personal
## 3. Identification with the beloved
## 4. Love is not a matter of choice
## Constraint and freedom
## Second order volitions
## Determinism and freedom
## Love and value
## The party guest
## Worthy of being loved?
## Susan Wolf: What makes someone worthy of love?
## Susan Wolf: How to judge love-worthiness? (1)
## Susan Wolf: How to judge love-worthiness? (2)
## Edyvane on the value of the beloved
## Justification as proof of worth
# Smuts: Normative Reasons for Love
## Is love a relationship?
## Kinds of lovable things
## Motivating vs normative reasons
## Awareness of normative reasons
## Strong and weak justifications
## Strong and weak justifications
## Conclusions (1)
## Conclusions (2)
# Theories of emotion
## Sources
# Smuts: Normative Reasons for Love
## Cognitive theory of emotions
## Is love a normal emotion?
## Is love a normal emotion?
## Is love a normal emotion?
## Is love a normal emotion?
## The rationality of emotions
# Pismenny/Prinz: Is love an emotion?
## Characteristics of love (Pismenny/Prinz, 1-2)
## Basic features of emotions (1)
## Basic features of emotions (2)
## Emotions as cognitive states (1)
## Emotions as cognitive states (2)
## Emotions as perceptions of the body (1)
## Emotions as perceptions of the body (2)
## Emotions as perceptions of value
# Basic, non-basic emotions and sentiments
## Basic emotions (1)
## Basic emotions (2)
## Is love a basic emotion?
## Non-basic emotions
## Sentiments (1)
## Sentiments (2)
# Love as a syndrome
## Syndromes (1)
## Syndromes (2)
## Reasons for love to be a syndrome
# Sternberg’s Triangular Theory of Love (1986)
## Sources
## Triangular theory of love
## Three components
## Different properties
## Types of love
## Types of love
## Discussion (1)
## Discussion (2)
## Discussion (3)
## Discussion (4)
## Discussion (5)
# John Alan Lee: The Colours of Love
## Colours of love (1)
## Colours of love (2)
## Colours of love (3)
## Colours of love (4)
## Colours of love (5)
## Colours of love (6)
## Can Sternberg and Lee be compared? (1)
## Can Sternberg and Lee be compared? (2)
# Final conclusions
## Harry Lime and Anna’s problem
## Anna’s options (1)
## Anna’s options (2)
## Anna’s options (3)
## Anna’s options (4)
## Anna’s options (5)
# Human ethology
## What is ethology?
## Ethnology and ethology
## Main proponents of ethology
## Some topics ethologists study
## Cautionary notes about ethology (1)
## Cautionary notes about ethology (2)
## Irenaeus Eibl-Eibesfeldt
## Justification of anti-migration stance?
# Patterns of courtship and love
## Three layers of sexual behaviour
## Extended courtship and female reactions (1)
## Extended courtship and female reactions (2)
## Grammer study 
## Results (1)
## Results (2)
## Results (3)
## Results (4)
## Approach strategies
## Trust and dominance 
## Body contact
## Kissing and kiss-feeding
## Formalised courtship rituals
## Formalised courtship rituals
## Sexual modesty
## Origins of modesty
## The biological role of sexual modesty
## The Oneida community
## The biological and cultural role of sexual positions
## The rear and the breast (1)
## Other sexual signals (1)
## Other sexual signals (2)
## Other sexual signals (3)
## Beards 
# Sexuality and domination
## Sexuality and domination (1)
## Sexuality and domination (2)
# Incest
## The role of incest
## Incest is rare
## Is the incest taboo biological or cultural?
## Kibbutz upbringing
## Incest and attraction
# Sources
## Sources
# Eibl-Eibesfeldt: Differentiation of sex roles
## Are gender roles biologically determined? (1)
## Are gender roles biologically determined? (2)
## Cross-cultural sex roles
## Sex differences in children (1)
## Sex differences in children (2)
## Sex differences in children (3)
## Sex differences in children (4)
## Sex differences in adolescents
## Body development differences (1)
## Body development differences (2)
## Different mental abilities in adults
## Cross-cultural adjective use
## Reasons for behavioural differences
## Summary (1)
## Summary (2)
## Summary (3)
## Summary (4)
# Sex and gender roles
## Development of love and family attitudes of men
## Public and private sphere
## New developments after 1920s
## Esquire magazine
## Same-sex friendships (women/men)
## Same-sex friendships (women/men)
## Same-sex friendships (women/men)
# The feminisation of love
## Biology vs gender roles
## Is love a feminised concept?
## Sex and love attitudes of men and women
## Negative consequences of the “feminisation” of love
## Negative consequences of the “feminisation” of love
## Negative consequences of the “feminisation” of love
## Negative consequences of the “feminisation” of love
# Gender differences in love styles
## Perceptions of love across the lifespan
## Gender differences
## Results of the Sumter study (1)
## Results of the Sumter study (2)
## Gender differences in sex and love (1)
## Gender differences in sex and love (2)
## Gender differences in sex and love (3)
## Serotonin processing by gender (1)
## Serotonin processing by gender (2)
## Serotonin processing by gender (3)
## Serotonin processing by gender (4)
## Serotonin processing by gender (5)
# Adolescents’ perceptions of love
## The Montgomery dating study
## When do boys and girls first experience romantic feelings?
## Gender differences in adolescent love
## Gender differences in adolescent love
# Gendered reactions to sex
## Strength of sex drive
## Sexual fantasies as indicators of sex drive
## Sexual fantasies as indicators of sex drive
## Desired frequency of intercourse
## Desired frequency of intercourse
## Desired frequency of intercourse
## Reasons for women’s reduced sex drive (1)
## Reasons for women’s reduced sex drive (2)
## Number of sex partners
## Number of sex partners
## Liking for sexual practices
## Liking for sexual practices
## Do differences in sex drive reflect biological reasons?
## Do differences in sex drive reflect biological reasons?
## Do differences in sex drive reflect biological reasons?
# Additional gender differences related to love and sexuality
## Gender differences in receptivity to sexual offers (1)
## Gender differences in receptivity to sexual offers (2)
## Gender differences in receptivity to sexual offers (3)
## Gender differences in receptivity to sexual offers (4)
## Gender differences in receptivity to sexual offers (5)
## Other gender differences related to sex (1)
## Other gender differences related to sex (2)
## Other gender differences related to sex (3)
## What girls and boys want from relationships (1)
## What girls and boys want from relationships (2)
## Love and addictions (1)
## Love and addictions (2)
# Showing love: husbands and wives
# Perceptions of infidelity
# Regret in women and men
# Language, love and gender
# Perception of TV reality
# Introduction to the course
## Welcome!
## Presentation of course outline
# Introduction to the course
## Welcome!
## Presentation of course outline
# Introduction to the course
## Welcome!
## Presentation of course outline
# Introduction to the course
## Welcome!
## Presentation of course outline
# Affective computing and machine emotions
## Sources (1)
## Sources (2)
# Emotions
## What is an emotion? (1)
## What is an emotion? (2)
## What is an emotion? (3)
## What is an emotion? (4)
## What is an emotion? (5)
## What is an emotion? (6)
## Rationality criteria for emotions
## Basic features of emotions (1)
## Basic features of emotions (2)
## What does it mean for AI to “have” emotions?
## Subjects and objects of emotions (1)
## Subjects and objects of emotions (2)
## Can I love a robot?
## Love as union
## Love as robust concern
## Love as relationship
## An Aristotelian view of love (recap)
## Can robots be Aristotelian partners?
# Uses of loving machines
## Affective computing and love (1)
## Affective computing and love (2)
## How can robots recognise emotions? (1)
## Facial expressions (1)
## Facial expressions (2)
## Facial expressions (3)
## Relational agents
## Uses of loving machines
## Advantages of loving machines
# Some problems of loving machines
## The authenticity problem (1)
## The authenticity problem (2)
## The authenticity problem (3)
## The authenticity problem (4)
## The uncanny valley problem
## Bunraku puppets
## Sophia the robot
## The Nao robot (a counter-example)
## Uncanny valley
## Factors causing the phenomenon
## What can be done about the uncanny valley (1)
## What can be done about the uncanny valley (2)
## What can be done about the uncanny valley (3)
## The degradation of love problem (1)
## The degradation of love problem (2)
## The degradation of love problem (3)
## The social isolation problem (1)
## The social isolation problem (2)
## The social isolation problem (3)
## The social isolation problem (4)
# Is robot prostitution immoral?
## The (im-)morality of human prostitution (1)
## The (im-)morality of human prostitution (2)
## Robot prostitution
## Demeaning women?
## Why men visit prostitutes
## Ethics of robot prostitution (1)
## Ethics of robot prostitution (2)
## Ethics of robot prostitution (3)
## Ethics of robot prostitution (4)
## Ethics of robot prostitution (5)
## Advantages of sex robots for society
## Could sex robots reduce sex crimes? (1)
## Could sex robots reduce sex crimes? (2)
## Could sex robots reduce sex crimes? (3)
# Introduction to the course
## Welcome!
## Presentation of course outline
