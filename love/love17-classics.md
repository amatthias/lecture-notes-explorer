---
title:  "Love and Sexuality: 17. Classic theories of love"
author: Andreas Matthias, Lingnan University
date: October 20, 2019
...

# Sternberg’s Triangular Theory of Love (1986)

## Sources

>- Sternberg (1986): A Triangular Theory of Love. Psychological Review 93(2): 119-135.
>- Regan (2008): General Theories of Love. In: Regan, P. (ed): The Mating Game: A Primer on Love, Sex, and Marriage. SAGE Publications.
>- Masahiro Masuda (2003): Meta-analyses of love scales. Japanese Psychological Research, 45(1), 25–37.
>- Sternberg, R. J. (1997). Construct validation of a triangular love scale. European Journal of Social Psychology, 27(3), 313-335.


## Triangular theory of love

![](graphics/sternberg-triangle.png)\



## Three components

>- Intimacy: 
>     - Emotional component (how it feels).
>     - Warmth, closeness, connection, bondedness.
>- Passion:
>     - Motivational component (motivates action).
>     - Romantic, physical, sexual attraction.
>- Decision/Commitment:
>     - Cognitive component (abstract thought, resolutions).
>     - Short-term decision that one individual loves another.
>     - Longer term commitment to maintain that love.

## Different properties

>- Intimacy, passion and decision/commitment differ in respect 
>- How stable they are:
>     - Intimacy and decision/commitment more stable than passion.
>- How much they can be consciously controlled:
>     - Commitment most easy to control, intimacy somewhat less, passion least.
>- How much the lover is aware of them:
>     - Passion is usually quite obvious to the subject; but intimacy or commitment may not always be consciously perceived in everyday life.

## Types of love

>- Nonlove (no intimacy, passion, or decision/commitment).
>- Liking (intimacy alone): (superficial) friendship.
>- Infatuation (passion alone): “love at first sight,” extreme attraction.
>- Empty love (decision/commitment alone): Often at the end of long-term relationships.
>- Romantic love (intimacy + passion): feelings of closeness and connection together with strong physical attraction. 
>- Companionate love (intimacy + decision/commitment): long-term, stable, and committed friendship (best friend, long term marriage).
>- Fatuous love (passion + decision/commitment): commitment based on passion alone. Typically unstable.
>- Consummate love (intimacy + passion + decision/commitment): “Complete” romantic love.

## Types of love

![](graphics/sternberg-table.png)\



## Discussion (1)

>- Sternberg^[Sternberg, R. J. (1997). Construct validation of a triangular love scale. European Journal of Social Psychology, 27(3), 313-335.]: We have to consider:
>     - Self- and other-perceived triangles. Two partners in a relationship might judge the relationship in a different way.
>     - Difference between real and ideal triangles of love. We might have particular ideals about love, but not behave in a way that is consistent with these ideals.
>     - There might also be a difference between the way we *feel* about love in general, and how important particular aspects of it are to our real relationships. For example, most people would say that Romeo and Juliet is a great love story, but most people wouldn’t want to have a relationship like that.

## Discussion (2)

>- In the Sternberg (1997) paper, respondents agreed more on what is *characteristic* in a relationship (in principle) than they agreed on what is *important* to them personally.
>- This perhaps shows a strong influence of the culture in shaping our expectations of love; but also people’s ability to separate the cultural prototype from their lived experiences.
>- It was shown that the Sternberg scale ratings correlated very strongly with relationship *satisfaction.*
>- So if a relationship scored high on all three dimensions, then the participants would probably also experience the relationship as satisfactory. 
>- Satisfaction was correlated stronger with intimacy (0.86) than with passion (0.77) and commitment (0.75).


## Discussion (3)

>- Do the three “dimensions” really measure different things?
>     - Studies have found (Hendrick\&Hendrick 1989) that couples who score high on one dimension, also score high on the others. 
>     - So it seems that the scale actually measures only *one* kind of thing (“love”?) rather than three independent dimensions.
>     - Sternberg (1997) disagrees.


## Discussion (4)

>- Why only these three dimensions and not others?
>     - The three dimensions cover all three: emotion, motivation and thought. 
>     - This seems to be a good thing. But why is the motivation only measured in terms of passion? Could we not be motivated by “intimacy,” for example?
>     - We could also imagine totally different dimensions (like Lee has done, below). What makes the particular three dimensions of Sternberg special, so that we should believe that there are only these three that are relevant?
>      - For example: Hatfield (1984, 1988): passionate and companionate love. Davis (1985): physical attraction, caring, and liking.


## Discussion (5)

>- Sternberg’s choice of words is odd and does not reflect today’s use of language. 
>     - “Intimacy” seems to denote sexual acts;
>     - “Passion” can mean passion for one’s work or hobby.
>- It does not seem like a good idea to misuse language in this way. This makes comparing different theories of love harder.


# John Alan Lee: The Colours of Love

## Colours of love (1)

![](graphics/lee-colour-wheel.png)\



## Colours of love (2)

>- A metaphor of primary and secondary colours.
>- Similar to Sternberg’s triangle in that there are also three main and three secondary kinds of love.


## Colours of love (3)

>- Eros: 
>     - Immediate and powerful attraction to the beloved individual. 
>     - Triggered by a particular physical type.
>     - “Love at first sight.”
>     - Intense need for daily contact with the beloved.
>     - Exclusive and sexual relationship.
>- Ludus (game-like love):
>     - Several partners simultaneously.
>     - Playful, not looking towards the future.
>     - Justifying lies and deception.
>     - Wide variety of physical types.
>     - Sexual activity is an opportunity for pleasure rather than for intense emotional bonding.

## Colours of love (4)

>- Storge:
>     - Stable affection, based on trust, respect, and friendship.
>     - “Old friends.” 
>     - No intense emotions or physical attraction associated with erotic love.
>     - Shared interests with the partner.
>     - Shy about sex, demonstrating affection in nonsexual ways.
>- Love is an extension of friendship and an important part of life.


## Colours of love (5)

>- In addition to the three primary colours, there are three secondary colours:
>- Pragma: 
>     - Combination of storge and ludus.
>     - The love that goes shopping for a suitable mate” (Lee, 1973, p. 124). 
>     - Practical outlook on love, seeking a compatible lover.
>     - Shopping list of features or attributes desired.
>- Mania: Combination of eros and ludus.
>     - Obsessive, jealous love.
>     - Self-defeating emotions, desperate attempts to force affection from the beloved, and the inability to believe in or trust any affection the loved one actually does display.
>     - Distrust and extreme possessiveness.
>     - Often unhappy.

## Colours of love (6)

>- Agape:
>     - Combination of eros and storge. 
>     - Similar to Lewis’s concept of charity.
>     - All-giving, selfless love.
>     - Loving and caring for others without any expectation of reciprocity or reward.
>     - Unselfishly devoted to the partner.



## Can Sternberg and Lee be compared? (1)

>- According to Masahiro Masuda^[Masahiro Masuda (2003): Meta-analyses of love scales. Japanese Psychological Research, 45(1), 25–37.], we can clearly see a dichotomy of love scales into:
>     - Sexual attraction to romantic partners (“E-Love”); and 
>     - non-sexual psychological closeness to partners, characterized by respect and caring (“C-Love”).
>- But Lee’s storge may not be associated with satisfaction in romantic relationships in the same way as Sternberg’s C-love types.
>- “Lee’s Storge may be a qualitatively different psychological construct from other theorists’ C-Loves.” (Masuda, Love scales, 35)

## Can Sternberg and Lee be compared? (2)

>- As opposed to Sterberg’s triangle, in this theory the “perfect” love is not supposed to be an equal mixture of all three primary “colours.”
>- The “colours” are more seen as “styles” rather than “components” of a love relationship. (But different styles can also be mixed in particular relationships).
>- It seems strange that Lee would see Eros as part of Agape (!) If agape is non-appraising, selfless charity, then how can it contains elements of sexual love? This doesn’t seem to make sense.
>- Given that (Masuda) Lee’s storge also scores differently in surveys than Sternberg’s “companionate love,” it seems that something is wrong here. Lee’s concept of storge does not seem to be the same as Christian agape!


# Final conclusions

## Harry Lime and Anna’s problem

>- We began this discussion a few sessions ago with the question whether Anna could or should love/despise the criminal Harry Lime.
>- We identified multiple problems related to this question:
>     - Can Anna be expected to control her emotions?
>     - Is it even possible to control one’s emotions?
>     - Is love responsive to reasons?
>     - Does love need a justification?
>     - Is love an emotion at all?
>     - Or is love a complex social phenomenon, composed of different components (emotions, motivations, commitments)?

## Anna’s options (1)

>- Is love an emotion? (probably no).
>- Love seems to be a complex of multiple things:
>     - Sternberg: Intimacy (emotion), passion (motivation), decision/commitment (thought).
>     - This is consistent with the Pismenny/Prinz concept of love as a syndrome of physiological, behavioural and cognitive responses.
>- Sternberg might say that Anna might not be able to control her emotion or passion, but she should be able to control and negate her commitment to Harry Lime.

## Anna’s options (2)

>- A Kantian approach would suggest that Anna should respect Harry Lime’s dignity and humanity; but that she should also let him be caught and not try to save him. 
>- By being a criminal, Harry Lime has shown disregard for others’ dignity, and it is fair that now others treat him in the same way.

## Anna’s options (3)

>- If we distinguish “falling” from “staying” in love, things get more complicated. 
>- The first may not require reasons, the second might. 
>- Relationship theories would see the falling in love as an appraisal (or mysterious) event, while staying in love is a bestowal of love, based on the relationship itself.
>- In this view, it would be harder to justify not “staying” in love with Harry Lime, since the relationship itself is what counts.
>- But one might question that the relationship is honest and based on the true properties of the lovers (since Harry Lime was not honest about his criminal acts to Anna).

## Anna’s options (4)

>- Susan Wolf believes (and Edyvane seems to agree) that we must have reasons to love a particular person. Love cannot be irrational.
>- In fact, it might be that looking for such reasons is a good sign for the presence of love.
>- The lover must be “worthy of love” (Susan Wolf).


## Anna’s options (5)

>- Even if we think of love as an emotion, we might question whether Anna has to *act* upon her emotion.
>- Perhaps it’s possible to have an emotion but not to act upon it (Julia Driver). We can separate the “emotional” from the “evaluative commitment” part.
>- Or perhaps we can command and influence our emotions more than we think (Matthew Liao). Anna could reflect on her emotions, visualise her reasons for loving or hating Harry Lime, and try to cultivate appropriate emotions toward him (by visualising his victims, for example). Internal and external control methods are available, and this removes Anna’s excuse for having the wrong emotions about Harry Lime.


