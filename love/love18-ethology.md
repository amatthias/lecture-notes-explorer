---
title:  "Love and Sexuality: 18. The biology and ethology of love"
author: Andreas Matthias, Lingnan University
date: September 1, 2019
...

# Human ethology

## What is ethology?

>- Main source for this chapter: Eibl-Eibesfeldt, I. (2017). Human ethology. Routledge.
>- Ethology is the scientific study of animal behaviour under natural conditions (as opposed to laboratory experiments). 
>- Ethologists generally view animal behaviour as the result of evolutionary adaptation.
>- “Human ethology” was founded by Irenaeus Eibl-Eibesfeldt, the author of today’s reading.
>- Human ethology tries to understand human behaviour in the same way as it studies animal behaviour, and sees it as a result of evolutionary adaptation.

## Ethnology and ethology

>- Ethology is often confused with ethnology.
>     - Ethos (Greek): custom, habit, morality (where “ethics” also comes from).
>     - Ethnos (also Greek): nation.
>- Therefore, ethology: the study of behaviours and customs.
>- Ethnology: the study of different “nations,” “tribes,” cultures and groups of people.
>- The two are loosely related, because in both cases the object of study are people, often from different cultures.
>     - But ethology is a part of biology, it describes people’s behaviour as a kind of animal behaviour.
>     - Ethnology, on the other hand, is more like social science: it describes cultural practices and how they differ across societies, without referring to biological terms.


## Main proponents of ethology

>- Ethology (as the biologically-inspired study of animal behaviour) begins with Charles Darwin (1809-1882) who studied the expression of animal emotions.
>- Konrad Lorenz (Austrian, 1903–1989) is one of the founders of modern ethology.
>     - He studied animal behaviour in everyday situations and described “imprinting,” the concept that newborn animals will attach themselves to a caregiver, even if the caregiver is of a different species.
>     - In 1973, he received the Nobel Prize together with Niko Tinbergen and Karl von Frisch.
>     - Tinbergen mainly studied instinctual actions of animals, especially honey bees, and created a model of how a bee decides which flowers to approach.
>     - Karl von Frisch also worked on bees and tried to decode the bee dance. He also researched how bees perceive colour and compass directions.


## Some topics ethologists study

>- “Fixed action patterns”: Whole, fixed sequences of animal behaviour that are triggered by a stimulus and then executed “automatically” to the end of the sequence.
>     - These include mating behaviour, attacking ‘enemies’ that have particular characteristics, snakes following bitten prey, the sucking reflex of newborn babies, and other behaviours.
>     - The point is that these behaviours are seen to be “hard-wired” into the animal. Once triggered, they cannot be changed or stopped.
>     - This, of course, is interesting also for humans. It has been suggested that some bodily features might be such triggers for sexual responses.
>     - If this was true, it would move part of our sexuality away from the area of conscious decision and make it into an (involuntary) instinct.


## Cautionary notes about ethology (1)

>- Ethology, obviously, is not free of controversy.
>- For example, describing human behaviour in the same terms as instinctive animal behaviour will offend Christians, Kantians, and anyone who believes in the primacy of reason over our animal nature.
>- Ethology might (and occasionally does) provide evidence for particular “built-in” role behaviours of women and men. This might be questioned by those who don’t believe in biologically fixated gender roles.
>- Ethology might describe homosexuality as “unnatural” behaviour in the sense that it might not be evolutionary advantageous. Also, ethology would look at whether animals show homosexual behaviours in order to understand the role of human homosexuality.


## Cautionary notes about ethology (2)

>- All these methods and conclusions might be considered not politically correct, not scientifically correct, or not desirable by different groups within society.
>- For example, sometimes ethology has been accused on “cherry-picking” evidence that supports its claims, and overlooking other evidence that might contradict them.
>- So please do your own research and engage with ethology in a critical and scientific way. 
>     - Don’t accept its statements as a religion. 
>     - It is a field of study like any other and, like any other science, it can occasionally be mistaken. 
>     - But it can also be right where we don’t like it. We should also be prepared to accept its statements where they can be shown to be true, and not reject them out of a misguided wish to please particular social groups.

## Irenaeus Eibl-Eibesfeldt

>- Irenaeus Eibl-Eibesfeldt (1928-2018) became particularly known for applying ethology research methods to humans.
>- The German Wikipedia has a long article about him and his work at: <https://de.wikipedia.org/wiki/Irenäus_Eibl-Eibesfeldt> You can try to Google translate that.
>- Eibl-Eibesfeldt, despite being the recipient of at least 18 major scientific awards (see the English Wikipedia page), has been extensively criticised for some of his opinions, for example for stating that humans are naturally reluctant to accept strangers from other tribes into their own community (read: migrants).
>- This would seem to provide a biologically justified basis for the rejection of migrants.

## Justification of anti-migration stance?

>- *What do you think? Is this necessarily so?*
>     - Not really. Showing that humans have a natural tendency to mistrust migrants from other cultures (assuming for the sake of the argument that this is true) would not necessarily allow us to conclude that it is *right* to mistrust migrants.
>     - In ethics, this is called the Is-Ought-Fallacy (look it up).
>     - In the same way, we could try to argue: “Because humans can steal, and naturally they desire the shiny possessions of others, therefore stealing is morally right.”
>     - This does not follow. Even if it is true that we desire others’ possessions, being human places particular moral demands on us, for example to resist such desires.
>     - The fact that I desire someone else’s possessions does not justify my action to take away these possessions from the other person.
>- In the same way, what ethology considers “natural” behaviour, is not therefore automatically “desirable” or “morally right” behaviour.

# Patterns of courtship and love

## Three layers of sexual behaviour

>- The cultural layer.
>- The (oldest) archaic layer of *agonistic* [agon, Greek: fight] sexuality, characterized by male dominance and female submission (“reptilian heritage”).
>     - This still determines certain aspects of human sexual behavior.
>- It is controlled by *affiliative* [affiliation, companionship] sexuality, which is mammalian heritage, and by individual bonding (p. 253)


## Extended courtship and female reactions (1)

>- The female partner in love with a male typically responds with “coy” resistance.
>- This is ritualised and forces the man to expend a great deal of effort and time to win his woman.
>- The greater the effort the male must expend, the more important it is for him to keep that partner at a later time, otherwise the cost of courtship would outweigh the benefits.
>- The coyer the female partner, the more valuable she becomes in terms of invested courtship efforts, up to an optimal level (p.239)
>- From this, we are supposed to conclude that humans are meant to have long-term relationships.

## Extended courtship and female reactions (2)

>- The responses of the partner being courted determine the course of courtship.
>- By her coyness a woman also tests the male's readiness (preparedness) to invest into the relation as well as his characteristics as provider and protector.
>- The woman’s investment by the physiological burden and risk of pregnancy and birthgiving is much higher than that of the male.
>- Sex differences in behaviour reflect evolutionary pressures, and that the tactics of self-presentation, criteria for mate choice, and strategies used in the development of relationships correspond to different "ultimate necessities" in both sexes.
>- *Of what does this remind you?*
>     - Schiefenhoevel’s romantic behaviour as “honest signal.”
>     - The unreachable female in courtly love.

## Grammer study 

>- German young adults study: the subjects, who did not know each other before the experiment, were left alone in a room and filmed through a one-way mirror. After 10 minutes, a questionnaire was presented:
>     - subjective ratings of attractiveness of the partner, 
>     - the readiness to date the partner,
>     - and the probability (risk) of acceptance by the partner.

## Results (1)

>- Males show an overall greater willingness to date a female, which is dependent on the rating of the partner's attractiveness in both sexes: the higher the rating of attractiveness, the higher the risk perception and the greater the willingness to date the partner. 
>- But whether the male initiates contact also depends on his self-esteem. 
>- If she is too attractive, he might consider his chances low and accordingly refrain from courting in order to save face. 
>- Only males take their own attractiveness into account for risk perception: the more attractive they rated themselves, the lower the risk they perceive (p.240)

## Results (2)

>- A high rate of laughing signified interest of a woman to join the young man.
>- Verbal self-presentation varies with perception of risk. Males are more indirect the higher they perceive the risk of female nonacceptance. 
>     - When perceived risk is high, they avoid the use of “I did, I will.” 
>     - Instead, they use the inclusive “we,” or even no personal pronouns.
>- Thus, tactics of male self-presentation depend on risk perception, generated by ratings of attractiveness of the partner. 

## Results (3)

>- Although males take the verbal initiative and present themselves verbally as potential mates, the episodes are structured by the female's nonverbal behavior. 
>- Females control the structure of the episodes by manipulation of male risk perception and encourage or impede male self-presentation. 
>- According to biological theories, females show a great degree of selectivity, whereas males tend to advertise overtly due to the pressures of male-male competition.

## Results (4)

>- Nonverbal signs for readiness for contact:
>     - Raised hands folded behind the neck;
>     - All kinds of “open” positions: legs open, arms open;
>     - Visual presentation of secondary sex characteristics;
>     - Orientation toward the partner (p.240)


## Approach strategies

>- Approach strategies have to be subtle if the partners are not yet well acquainted. 
>- The prominent white eyeball enables us to perceive partners' eye movements easily. 
>- Eye contact return is a sign of a positive response.
>- In the succeeding contact conversations the partners test each other's interest in further contact.
>- If the partners are unacquainted, they will attempt to develop a common reference system. Common interests are determined and compatibility is expressed.
>- The partners' next step is to construct a basis of trust. One confides in another partner by disclosing one's weaknesses, but always in conjunction with a positive projection of self (p.241)

## Trust and dominance 

>- Dominance displays attempt to achieve a dominant position relative to the other interacting party. 
>- In courtship display the male attempts to demonstrate that he is in a position to dominate others and thus to protect his partner.
>- This elevates her esteem for him.
>- The courtship threat display is meant to impress the partner without being directed at her. Geese show similar behaviours.
>- Positive self-image can also be displayed through display of material goods and caregiving expressions. 
>- Also, both partners present themselves through infantile appeals as a suitable object for caregiving which complies with the wish for mutual care (p.242)

## Body contact

>- Body contact is brought about in a noncommital manner. 
>- In some cultures, rituals offers opportunity for contact, as in dance. 
>- Often, the man takes the initiative by utilizing a quasi-accidental and harmless contact, as for example laying a shawl over the woman's shoulder or otherwise offering assistance that the partner may invite in a subtle way.
>- The man usually makes the proposition and the woman makes the decisive selection by accepting or rejecting her suitor.

## Kissing and kiss-feeding

>- Kissing leads to sexual foreplay and this breaches the standards of modesty that exist in all cultures.
>- These behavioral patterns of physical affection are found in various cultures, even in those that were obviously not influenced by Europeans. 
>- Kissing is found in Peruvian ceramics dating from pre-Columbian times.
>- Affectionate kiss feeding is also observed in diverse cultures.
>     - In the Kamasutra, lovers are described sipping wine mouth-to-mouth.
>     - Viru lovers (southern highlands of Papua/New Guinea) utilize mouth-to-mouth feeding. 
>- Embracing, stroking erogenous zones, fondling, and social grooming are all universally practiced.

## Formalised courtship rituals

>- In many cultures these intimate contacts are preceded by formalized courtship rituals, particularly in those cultures that do not permit premarital sex in an effort to prevent illegitimacy. 
>- In these situations the society arranges partner contacts.

## Formalised courtship rituals

>- In the Medlpa of New Guinea the parents of marriageable girls invite potential husbands to a meeting. 
>     - The colourfully decorated partners sit in pairs in a communal room. They sing and after introductory head swaying rub foreheads and noses, always twice. Then they bow deeply twice, rub noses two more times, and continue the courtship dance by alternating bowing and rubbing noses.
>     - The "head rolling" is a courtship dance during which course the partners synchronize their movements. 
>     - The song itself does not impart the rhythm. The partners develop a common rhythm during the dance.
>     - The easier they obtain movement synchrony, the better the partners seem to adapt to each other, as synchronization serves to express harmony. 
>     - Same with dances found in western culture (p.243)

## Sexual modesty

>- Sexual modesty is another cultural universal element of behaviour. 
>- It is found in all cultures and is expressed in various ways. 
>- The sexual organs are often covered by clothing. But sometimes the cover is only symbolic.
>- Yanomami (Amazon tribe) women wear only a thin cord around their waists. But even this cord is symbolic “clothing.” 
>     - If a Yanomami woman is asked to remove the cord she becomes just as embarrassed as a woman in our culture would be if she were asked to remove her clothing.

## Origins of modesty

>- Modesty demands that one conceal sexual activity from others. A number of attempts have been made to identify the origin of modesty.
>- Is ti to cover this “unattractive part of the body”? 
>- To maintain an undisturbed group life?
>- Human women, unlike other mammalian females, do not have a visible estrus [see next slide]. That certainly facilitates group life through reducing overt sexual competition (p. 246)

## The biological role of sexual modesty

>- A sexual union can provoke attacks, especially by higher ranking male group members. 
>- They attempt to interrupt copulation with threats and even attacks. 
>- This is a form of sexual rivalry through which the higher ranking members maintain multiple options for propagating their genes.
>- Another reason for concealing sexual activity is that during the sexual act a person is so involved with the partner that he cannot perceive the environment accurately and thus is vulnerable (to attacks) (p.246)

## The Oneida community

>- The Oneida community in the United States: founded in 1830, had 500 members at its height.
>- All possessions, including clothing and childrens' toys, were common property.
>- The children were raised communally and were taught to love all the adults as if they were their own parents. 
>- Adults were expected to respond equally to all others; romantic love was considered to be selfish and monogamy was thought to be detrimental to community life. 
>- While the lack of ownership and a classless society were achieved, the group failed to abolish:
>     - sexual division of labour, 
>     - dominance of men over women and children, 
>     - the formation of individual sexual partnerships, and 
>    - parent-child bonds.
> The community was dissolved in 1881 (after about 50 years) (p. 248)


## The biological and cultural role of sexual positions

>- Most mammals, including the anthropoid apes, mate with the male mounting the female posteriorly. 
>- The sexual signals of the female are on her bottom and are presented to the male by displaying the rear. 
>- During the course of hominization, with the evolution of upright body posture, a new orientation arose with the sexual-releasing signals being transposed to the front of the body.
>- Humanization: the act whereby the mutual orientation toward each partner's face plays an important role.
>- Upright posture facilitated this orientation.

## The rear and the breast (1)

>- The breast plays a role as a sexual signal.
>- The form-giving lipid bodies of the breast are not required for nourishment of the child, but instead give the breast display value.
>- D. Morris (1968) maintains that the breast became a display organ in conjunction with the development of the upright posture as sexual orientation changed to the front of the woman. 
>- Morris claims that in humans the breast acts as a mimicry of the posterior, copying the buttocks in form (p. 250)


## Other sexual signals (1)

>- E. A. Hess (1977) developed a way to measure preferences using pupillary reactions. 
>     - The pupils dilate briefly whenever someone sees something interesting and pleasing. 
>     - Hess constructed an apparatus with which a person's pupil could be filmed while the subject observed slides. 
>- The pupillary reaction study showed that normal males and females respond to naked women and muscular men, respectively, even if the female subjects claimed they had no interest in muscle men.

## Other sexual signals (2)

>- Hess found that male subjects were divided into two groups. 
>     - One showed a clear preference for large female breast;
>     - Others preferred pictures of women with well-developed but not excessively large breasts and in which the women were presented in such a way as to also display the posterior body portion.
>- Most heterosexual American men conformed to the first type.
>- Europeans conformed, in general, to the second type. 
>- However, each group contained a number of individuals that did not conform to the within-group norms.

## Other sexual signals (3)

>- Later Hess found a correlation between these preferences and bottle or breastfeeding. 
>- Those Americans and Europeans who preferred large breasts had been bottle fed.
>- One is inclined to speculate that these subjects retained an infantile desire for the maternal breast. 
>- Those who had been nursed preferred the young woman's breast but not the unusually large ones. 
>- Homosexuals could also be identified with this test, for they reacted positively to partners of the same sex and not to those of the opposite sex. 

## Beards 

>- While men respond highly to visual stimuli of their female sexual partners, females also respond to such stimuli in men, albeit not to them alone. 
>- We know that narrow hips, a small firm buttocks, broad shoulders, and, in general, a muscular body are perceived as handsome features in men.
>- The beard is one of the prominent male secondary sexual characteristics in many races. 
>- It is less a heterosexual signal and more for its display value for other male group members as a signal of strength and maturity.


# Sexuality and domination

## Sexuality and domination (1)

>- Male dominance behavior is closely associated with male sexuality.
>- In many fishes and some higher vertebrates, courtship is initiated with mutual display behavior.
>- Pair formation only succeeds when the male is able to dominate his partner.
>- Male sexuality is thus associated with aggressivity but not with fear. 
>- Female sexuality is the exact opposite: in lower vertebrates aggressive motivations inhibit sexuality, although flight motivation (fear) does not necessarily suppress it.

## Sexuality and domination (2)

>- In the marine iguana, for example, the courtship of the male consists of a threat display.
>- Receptive females respond by submission -- by lying flat on their belly, which invites copulation. 
>- The reptile pattern of agonistic sexuality is thus characterized by a male dominance and a female submission sexuality. 
>- In birds and mammals, this archaic agonistic sexuality is superceded by a phylogenetically new pattern of affiliative sexuality which in some of them, including man, gave rise to a love relationship based upon individual bonding.
>- Affiliative sexuality can be traced to the evolution of maternal behaviour.

# Incest

## The role of incest

>- Incest (sexual relations with members of one’s own family) is not advantageous genetically.
>- First, it leads to offspring that have less genetic variability than possible. Since genetic variation is a good thing (and the only point of sexual reproduction), incestual relations should be avoided for purely genetical reasons.
>- Second, some hereditary illnesses are more likely to occur when both parents have a particular defective gene, and this probability is obviously higher if both parents are related.
>- This is why all societies prohibit incest (p.261).
>- This is interesting, because love as care, union, concern etc (without sex) is generally encouraged within the family (one could say, it is the main point of forming a family). But there is a sharp limit to it where it crosses the line to incest.

## Incest is rare

>- Finkelhor (1980) found that only 2% of students had had sexual contact with family members, and most were between siblings under the age of 13.
>- Only 3 out of 796 students had attempted intercourse with siblings after the age of 13.
>- Among 18,000 psychiatric patients, Meiselman (1979) found only 9 instances of parent/child incest.

## Is the incest taboo biological or cultural?

>- Until the 1960s, the prevailing opinion was that the incest taboo is a cultural arrangement (p.261).
>- In the early 1970s, it was shown that many animals have incest inhibitions. 
>- Animals, including primates, will not copulate with animals they grew up with.
>- In human families, close contact during childhood leads to sexual aversion to the other person. (Now you know why you find your brother or sister unbearable!)
>- Even plants sometimes have mechanisms preventing self-fertilisation.

## Kibbutz upbringing

>- The kibbutz is a kind of communal living environment in Israel.
>- The idea was to abolish sex and class differences by having all people grow up together.
>- Boys and girls used the same showers and toilets, in an effort to stop sex discrimination.
>- This worked until the children reached the age of 12. After that, they avoided contact with the opposite sex.
>- After puberty, the relations between them were friendly.
>- But age class members never married each other.
>- Out of 2769 marriages between persons raised in the kibbutz, not one was between two people who had been brought up together.

## Incest and attraction

Interesting graphic (p.264):

![](graphics/18-incest.png)\



