---
title:  "Love and Sexuality: 5. Aristotle overview"
author: Andreas Matthias, Lingnan University
date: September 1, 2019
theme: lisbonsmall
transition: fade
slidenumber: 'c/t'
autoslide: 0
...

# Intro

## Aristotle (384-322 BC)

>- How do Socrates, Plato and Aristotle relate to each other?
>- Socrates was teacher of Plato. Plato was teacher of Aristotle.

# Virtues

## What is a virtue?

Ethical virtues:^[Aristotle distinguishes between intellectual virtues (virtues of the mind), and the ethical virtues (virtues of character). For a first understanding of his theory, it is sufficient to look at the ethical virtues (like the three above), and to ignore the intellectual virtues.]

- Courage
- Kindness
- Honesty

... and many more.

### Virtue (Greek: *arete*):
A property of one's character that is beneficial to oneself and to others.

## Virtues are good only in the right amount

Too little | Virtue | Too much
------------|----------|------------
cowardice | courage | recklessness
to be mean, selfish | kindness | to be a "doormat"
liar, to deceive others | honesty | harsh, hurting others 

Conclusion: Virtues are only good if the agent has them *in the right amount.*

## What is the right amount of a virtue? (1)

This poses the next question: "What *is* the right amount of virtue?"

A common misunderstanding would be:

- The right amount is the *middle* amount.
- If cowardice is 0% courage, and recklessness is 100% courage, then the ideal would be 50% courage.

Is this true? Or are there situations where it would be perfectly right to have 0% or 100% courage, instead of 50%?

## What is the right amount of a virtue? (2)

Situations where **0%** courage would be appropriate:

- A lion attacks you and you have no weapon, only a pen. The best here would be to run, and not try to be courageous at all.
- You see someone drowning at the beach, but you cannot swim yourself. It would be pointless to try to be brave and jump into the water. This would actually make it more difficult for the rescuers to rescue the other person, because now they'll have to rescue two people.

## What is the right amount of a virtue? (3)

Situations where **100%** courage would be appropriate:

- You are a life saver at the beach, and now you see someone drown. In this situation, you *are* supposed to jump into the water and save them.
- A lion attacks someone, and you are a policeman, trained in shooting, and you have your gun ready. Now you should be courageous and kill that lion, not run away.

## What is the right amount of a virtue? (4)

What do we learn from this?

>- You cannot say that the ideal amount of a virtue is 50% virtue. Not only would this be hard to measure, but it is obviously not true.
>- Sometimes the right amount of a virtue is 0%, sometimes 100%.
>- It depends entirely on the situation.
>- Even the same situation can call for different amounts of a virtue, depending on *who you are* in that situation (see above, life saver example).
>- Conclusion: *The amount of virtue is not constant, but changes with every situation and with your role in it.*

## What is the right amount of a virtue? (5)

If it is true that every situation has its own "right amount" of virtue, the next question obviously is:

"Then how do I know what the *right amount* of virtue is in every particular situation?"

# Phronesis ("practical wisdom")

## Phronesis (practical wisdom)

### Definition:
Phronesis (practical wisdom) is the ability to judge what is the right amount of every virtue in every particular situation.

Having practical wisdom means two things:

1. The wise person knows the *means* to certain *good ends* 
2. The wise person knows *how much particular ends are worth* 

## Do children have phronesis?

Think of a 5-year old child. Does it have **virtues?**

- Yes. Even a small child can be courageous, or honest.

But can a 5-year old **judge the right amount** of courage or honesty in every situation?

- No. Although the child possesses the virtues themselves, it needs to learn how much is the right amount. Otherwise he will hurt people by being too honest, or fall out of the window because he is too courageous and balances on the windowsill.

For Aristotle, therefore, children do not have phronesis. They cannot act morally right by themselves.


## How do people acquire phronesis?

>- Own experience
>- Education
>- Knowledge of own abilities and shortcomings
>- Exchange with friends, observation of friends' lives

. . . 

Although the own experience is most direct, not everything can or should be experienced directly (wars, extreme poverty, being the victim of a crime).

We can know that being robbed is bad without actually having experienced it (from the tales of others who have).

## Do older people have *more* phronesis?

If children don't have phronesis, and older people have it, and phronesis grows through experience, is it true that the older you get the *more* phronesis you have?

Do older people always have more phronesis than younger people?

>- No.
>- Because it is not the length of one's life that counts, but the actual experiences one has learned from.
>- A shorter life can contain more experiences than a longer life.
>- Even if one has had many experiences, it is also important whether one can *learn* from these experiences.

## Can all people acquire phronesis equally well? (1)

No. Some people can have a harder time getting new, different kinds of experiences.

Factors that make acquiring phronesis from experience easier:

>- A healthy body. People with disabilities will be limited in the kinds of experiences that are accessible to them. Only a healthy body will have maximum access to all experiences available to humans.
>- A healthy, intelligent mind.
>- Some reasonable amount of wealth. Extremely poor people cannot afford to have some of the possible experiences (e.g. travelling the world).
>- Perhaps not too much wealth. Extremely rich people tend to be busy with many obligations, so that they too don't have the time and leisure to go out and make new experiences.

## Can all people acquire phronesis equally well? (2)

>- Living in a free society affords more possibilities to make new experiences, as opposed to a rigid, oppressive social system.
>- A supportive environment is also helpful (supportive family, easy leave-taking from job, easy access to travel, libraries, media, friends, teachers).
>- All these external factors are *necessary but not sufficient* for acquiring phronesis easily.
>- Even under ideal conditions, some people will not want to make experiences or learn from them.
>- On the other hand, there are always examples of extremely disadvantaged people who manage to acquire phronesis and live good lives. But Aristotle would say that these are exceptional, not the rule.

# Eudaimonia

## Life with perfect phronesis (1)

Assuming you have acquired a great amount of practical wisdom:

- You have all the virtues to exactly the right amount.
- You are always doing the right thing, never too little or too much.
- Your actions are always beneficial to yourself and to others.

How will your life be?

## Life with perfect phronesis (2)

How will your life be?

>- **Successful** (because everyone will like you, you will have many friends, your employers will praise your good judgement and your virtues, and so on.)
>- **Morally good** (because your virtues, developed and used correctly, will always be beneficial to oneself and to others. Moral badness is a lack of virtue, which, we assumed, you wouldn't have.)
>- **Happy** (how can you fail to be happy if you are successful in everything, morally good, with lots of friends, perfectly virtuous, and admired by everybody?)

. . . 

This kind of life is the *ideal human life* for Aristotle.

## Eudaimonia

### Eudaimonia:
The performance of a life that is, at the same time, successful, morally good, and happy.

**Eudaimonia is the ultimate goal of human life.**

## Eudaimonia is the highest good

>- Difference to Plato here: the highest good is *not* the contemplation of abstract beauty and perfection, but a morally good, successful, and happy life!
>- We already saw Aristotle’s argument to support that a happy life is the ultimate goal of human beings:
>     - Everybody wants happiness.
>     - We want other things *for the sake of* happiness. I want money in order to be happy. I want a family in order to be happy. And so on.
>     - Nobody would say: I want to be happy in order to earn more money. It just doesn’t make sense.
>     - The same applies to fame, learning, work and everything else.
>     - Conclusion: Happiness must be the ultimate goal of human life. All other aims are subordinate to it (less valuable).


## Eudaimonia (the word)

The word has two parts:

- "eu-" means "good" in Greek.
- A "daimon" (daemon) is a spirit or ghost.

If you are "eudaimon," you have a life that is so good and successful, that is looks *like* it is guided by a good spirit.

This is a metaphor only. Aristotle doesn't mean that there is actually such a spirit!

## Can a criminal be eudaimon? (1)

Imagine that an assassin has a good day, made a lot of money killing people, the weather is good, and he is happily going for a walk with his girlfriend. Out of all this good mood, he is giving a beggar on the street 100 HKD.

Is this a morally valuable action? Is the criminal now a good man?

## Can a criminal be eudaimon? (2)

- For Aristotle, not isolated *actions* count, but only whether a *life* is eudaimon or not.
- In other words, whether an agent is exercising his virtues in such a way as to maximise his phronesis and, therefore, his future eudaimonia.
- The question is: will the criminal's action eventually lead to an increase in phronesis and, in the long term, to eudaimonia?

## Can a criminal be eudaimon? (3)

- No.
- First, because although the criminal has *some* virtues (courage, for example), he does not have others (honesty, compassion).
- His virtues are not *balanced.*
- Second, although he does give the beggar money, this charitable action is not really part of his character. If he was generally charitable, he wouldn't be a criminal in the first place. His charity is "out of character." It is not *deeply* rooted in his character, but is *superficial.*
- Third, although he has some virtues, he does not apply them correctly. His virtues are not beneficial to himself and to others.

**Virtues need to be (1) deeply rooted in one's character and (2) well-balanced in order to count as real virtues.**


## How do virtues become "deep?"

- How do virtues become "deeply rooted in one's character?"
- Similarly: how do any character traits or skills become deeply rooted in a person's character?
- Through constant *practice.*

## Handwriting

Consider the skill of handwriting:

>- When you were a small child, you learned to write.
>- At first, this was very hard. You had to concentrate completely on just writing the shapes of the letters correctly, and you couldn't concentrate on the content of what you wrote at all.
>- After you learned the letters, you began to write words. Again, this was so difficult that you couldn't imagine writing whole sentences at once.
>- Later you had so much mastery of the letters and the words, that you could attempt to write sentences. Slowly, after long time of practising, you managed to learn that, too.
>- Today you can take notes while listening to a lecture, and you can concentrate completely on the content of the lecture, without wasting a single thought on the writing process.
>- The writing has become *automatic.* It is not a conscious effort at all any more.

## Mastering skills

The same principle applies to all skills (drawing, driving a car, walking, playing football, taking good photographs).

**When you have mastered a skill, the skill becomes something you do automatically, without any conscious thought or effort.**

## Mastering moral behaviour

- Things work similarly for moral behaviour.
- Through your constant exercise of phronesis, you become proficient in acting morally good, so that acting morally right becomes automatic.
- You do it without conscious effort.

## Three types of people (1)

Now consider that someone in front of you, on the street, accidentally dropped some money, and didn't notice it. As he walks away, you pick up the money. What happens next?

Three possibilities:

>1. You keep the money and walk the other way.
>2. You think for a moment whether you should keep the money or not, but then decide that it wouldn't be morally good to keep it, and that you should perhaps return it. Reluctantly, you call to the other person, and hand over the money. He thanks you and praises you for being morally good.
>3. You immediately run to catch up with the other person and give him back his money without even the thought that you might keep it for yourself.

## Three types of people (2)

These three types of people Aristotle thinks of as *stages in the moral development* of humans:

>1. The first type (the *akrates* in Greek) is the one who is always tempted to do the morally bad thing, and unable to resist the temptation. As a result, he always does the bad thing.
>2. The second type (the *enkrates*) is one who is always tempted, but manages to overcome the temptation. As a result, he acts morally right, but only after some inner fight against himself.
>3. The third type (the *sophron*) is the one who naturally and without effort does the right thing. He is not even tempted to do something that is not morally right. Long practice has made phronesis *automatic* in him.

The human life is (ideally) a process of advancing from akrates$\to$enkrates$\to$sophron.

## Three types of people (3)

Compare this with other skills:

>- For an expert walker, walking becomes automatic. He is never tempted to fall over while walking. A half-trained walker (like a small child, for example) is tempted to fall over, and sometimes does.
>- For an expert writer, writing correctly is automatic. He is never tempted to write the letters or words wrongly. If you are occasionally in doubt about how you should write a word, then you are not yet an expert in writing, but a learner (like a student in primary school).



# The End. Thank you for your attention!

<!--

%% Local Variables: ***
%% mode: markdown ***
%% mode: visual-line ***
%% mode: cua ***
%% mode: flyspell ***
%% End: ***

-->




