---
title:  "Love and Sexuality: 3. Plato’s Symposion"
author: Andreas Matthias, Lingnan University
date: September 1, 2019
...

# Plato and the Symposion


## Why read the Symposion at all?

>- A 2400 years old text?
>- Somebody said that all of philosophy is just “footnotes to Plato.”
>- This is a little exaggerated, but in some areas of philosophy it is true.
>- The philosophy of love, and our everyday understanding of what love is, have not changed radically since Plato’s time.
>- The text is also surprisingly modern, very well constructed and works on multiple levels. You will see how nicely it’s done in the end, when we have read it.

## Symposia

>- “Syn-”: plus, together.
>- “pino,” “pos-”: to drink.
>- “Symposion”: the drinking-together; a party.
>- Greek: symposion, Latin: symposium. Often, books refer to it as “symposium” (Latin), but this is misleading. Plato spoke and wrote in Greek.
>- At symposia, only men were normally drinking together, singing songs, telling jokes, and having a good time. They reclined on long benches and the host could dictate how strong the wine should be. In this way, the host could control how things developed.
>- Women were only present as dancers or musicians, not as participants. 
>- In Plato’s Symposion, the wisest character, the one who even teaches Socrates about love, is a woman, Diotima!


## Plato

>- Who was Plato?
>     - An ancient Greek philosopher.
>- Where and when did he live?
>     - Athens, 427-347 BCE.
>- How is he related to other ancient Greek philosophers you know?
>     - Student of Socrates (470-399 BCE), teacher of Aristotle.

## Socrates

>- What happened to Socrates?
>     - He was accused of corrupting the youth with his teachings. His way of publicly disputing with influential people and humiliating them surely didn’t make him more popular.
>     - In a famous trial, Socrates gave a long speech (“Apology”), in which he refused to apologise and even made fun of the judges.
>     - He was sentenced to death in 399 BCE. 
>     - Plato and other friends and students offered to get him out of prison, but he refused and was executed.
>     - By not escaping, he wanted to make the point that obeying the laws and being morally upright is more important than even one’s life.
>     - Socrates is generally described as an ugly, old man, which is important in the context of the Symposion.

## After Socrates’ death

>- Plato travelled around the Mediterranean.
>- He tried to convince the leader of Syracuse to try out Plato’s political theory and create an ideal state that would be ruled by philosophers.
>- He failed, returned back to Athens, and founded a philosophy school, in which Aristotle later studied.


## The Symposion

>- The particular symposion took place around 35 years before it was re-told (or imagined) by Plato.
>- Is is probably not a faithful retelling of a real symposion, but to some extent made up to tell the story that Plato wanted to tell.
>- The people were still known to Plato’s readers, but all of them, except for Aristophanes, were already dead. Aristophanes might have been in the last year of his life.
>- Imagine a book about a meeting of famous people in the mid-1980s: politicians of that time, generals, an actor or two, a writer, and a philosopher; now all dead or very old. This is how the Athenians of Plato’s time would have read the Symposion, recognising the names in the text.


## People in the text (only the main ones) (1)

>- Agathon (about 31 years old at the Symposion): A poet who wrote tragedies (none survived).
>- Alcibiades (about 35 years old): A famous aristocrat and rich party boy, later politician and general of the Athenian fleet. He was known for switching sides as it served him and only looking at his own benefit. Socrates saved his life in battle on two different occasions, and Alcibiades always looked up at Socrates as his teacher.
>- Aristophanes (about 34 years old): One of the “big four” writers in classical Athens (the others are Aeschylus, Euripides, Sophocles). Aristophanes wrote comedies, some of which made fun of Socrates.
>- Eryximachus (about 30 years old): A physician, friend of the others.

## People in the text (only the main ones) (2)

>- Pausanias: Has a relationship with Agathon. Legal expert. There are many people in ancient history with that name.
>- Phaedrus (about 28 years old): Friend of Eryximachus, also appears in other works of Plato. Educated young man who likes literature.
>- Socrates (about 54 years. old): one of the most famous philosophers of ancient times. He didn’t write anything down himself, so his thoughts are only known through the works of others (mainly Plato). 


# Structure of the text

## Narrative structure (1)

>- Many layers of “recollections,” perhaps to indicate that the Symposion is not really a transcript of the party as it happened, but a text that reflects the values and preferences of the people that retell it.
>- The Symposion is the recollection of someone called Apollodoros, of what he was told long ago by one Aristodemos, who himself was reconstructing the events from memory.

## Narrative structure (2)

>- Seven people decide to have a party in which they’ll give speeches about love.
>- Each person talks from their own perspective. Some are more romantic, some funny, some more oriented towards medicine or the natural sciences.
>- Through these seven people, Plato manages to talk about love from many different perspectives before finally giving the word to Socrates.
>- Socrates provides the crowning contribution of the evening. Interestingly, he attributes his ideas to a woman, Diotima, an unusual move for the time.


## The seven speeches

>- Introduction. First glimpse of Socrates.
>- 1. Phaedrus (literature): the moral side of love.
>- 2. Pausanias (law): “higher” (heavenly) and “lower” (popular) love.
>- 3. Eryximachos (medicine): love as an expression of universal harmony.
>- 4. Aristophanes (comedy): love as union between separated parts of an individual.
>- 5. Agathon (tragedy): the connection between beauty and virtue.
>- 6. Socrates (philosophy): the ladder of love, attributed to Diotima.
>- 7. Alcibiades (lover of Socrates): application of Diotima’s ladder.
>- End of the party.

# The speeches

## How to read the text

>- Ignore the references to mythological and literary figures.
>- Back then, they would have been known to everyone, like when we today talk of Superman or any Disney movie character.
>- Also, remember that this is a game to pass the time. The speakers don’t *want* to speak directly and make their points. They want to take their time, show their wit, and entertain their audience. You’ll have to look over all that to find the core message of each speaker.

## Phaedrus (literary man)

>- Love brings moral improvement.
>- Lovers try to “look good” to their beloved, so they become better people.
>- Only lovers will die for others.
>- Therefore, love is the source of all virtue and happiness for men.

## Pausanias (lawyer)

>- Lawyers make all kinds of distinctions in law. Pausanias applies this method to his speech about love.
>- Love is not one thing. It’s two different things: Heavenly and popular love.
>- Generally, things are not in themselves good or bad. The context and the way we do them makes them good or bad.
>- “Good” love is the love of older men towards boys that educates and improves the beloved. All other (selfish, pleasure-focused) kinds of love are vulgar (popular) instead of virtuous and refined (heavenly).
>- Pausanias here already foreshadows Socrates’ later point that love is a progression from the love of the body to the love of the mind.

## Eryximachus (doctor)

>- Love is not only about morality and virtue, but also about health and well-being.
>- Love is not restricted to humans. All things strive towards harmony and order.
>- This is according to ancient theories of health that see illness as an imbalance of elements in the body (both in ancient Greece and China!). The physician diagnoses this imbalance and prescribes what is necessary to restore the balance = heal the patient.
>- Opposites (or dissimilar things) attract each other. Medicine, music and other arts are nothing but the attempt to order and harmonise different aspects (heat and cold, musical notes, elements in the body), and so are kinds of love.
>- If the opposites come together in the wrong way, they cause illness and destruction. 

## Aristophanes (comic writer)

>- Aristophanes tells a funny story about how love was created.
>- Originally, there were three kinds of men: Male-Male, Female-Female and Female-Male. These were double beings (Imagine two of today’s humans standing together back-to-back as one being.) Each had four arms and legs.
>- At one point, humans became too powerful. So the gods decided to split them in half. The two halves would eternally be searching for their other half, trying to reach the original unity of their person.
>- The best kind of love is the love from man to man, since the original beings were Male-Male (and thus had the best properties).
>- The absurd picture of half people looking for their other halves leads to the first definition of love in the Symposion:
>     - *Love is the craving and pursuit of wholeness. [193a]*

## Agathon (tragic poet)

>- Love is always the love of beauty.
>- It “flees from old age... [Love] hates old age by nature and refuses to come within any distance of it.” [195b]. Love “seeks its food among flowers” [196b].
>- Love is desire, but it also is the desire for “beautiful things” [197c] and for the arts that create these things. Therefore, love is also the basis for arts, crafts and all kinds of skills.

## Socrates (also representing Plato’s views)

>- On p.39 of our reading (around 200a) you can see the “Socratic method” at work. Socrates asks someone (who considers himself an expert) questions that reveal that the expert must be mistaken (and that Socrates’ views are right).
>- Love is not in the *possession* of the good and beautiful, but the *desire* for it.
>- It doesn’t make sense to love health or riches if one is already healthy and rich (except as a with to continue to have these things in the future).
>- Love is the *desire* to have something *now and for ever* in the future.
>- But if love is desire, and we cannot desire something we already have, then love cannot be beautiful or good. Since only the lack of goodness or beauty would inspire love for these things.
>- Now we reach a point at which it is unclear how to proceed. And here Socrates tells the tale of Diotima.

## Diotima’s ladder (1): Preliminary clarifications

>- Things get strange here. After all have agreed that proper love can only be between men, now Socrates, the wisest, presents a *woman* as his teacher in love! Socrates here makes fun of himself, by taking on the role of the receiving, dumb end of a typical Socratic dialogue.
>- Diotima is, in every way, an exception: Woman among men, non-Athenian among Athenians, and absent among present guests.
>- Love desires good things, because the possession of good things leads to happiness as the ultimate good [p.49, 205a].
>- As opposed to Aristophanes, Diotima thinks that we don’t always have a desire to unite with our other halves, but only if we perceive them as something *good.* [p.50, 205e].
>- *“Love loves the good to be one’s own for ever.”* [p.51,206a]

## Diotima’s ladder (2): Immortality

>- Since love loves the good to be one’s own *for ever,* but people die, love necessarily desires immortality. [p.53, 207a]
>- “For here, too, on the same principle as before, [207d] the mortal nature ever seeks, as best it can, to be immortal. In one way only can it succeed, and that is by generation; since so it can always leave behind it a new creature in place of the old.” (p.54)
>- Remember that Plato sees himself as a student of Socrates, who was dead by the time this was written; Plato continued his work and made his teacher immortal.
>- Every being tries to have children in order to have part in this immortality. But children themselves are mortal.
>- Only the love between learned men is ideal, because only their “children” (their ideas) are really immortal.

## Diotima’s ladder (3): The progression of love

>- In the first stage, we are in love with a particular body [p.57]
>- But then we realise that many bodies are equally beautiful. So we love all beautiful bodies.
>- From that, we go on to love the beauty of the souls that are inside these bodies.
>- More practice leads us to love the beauty of our society, its rituals, its laws and all branches of knowledge that have their own beauty (mathematics, physics, art, ...)
>- We look away from the fragile beauty of the individual and “turn towards the main ocean of the beautiful,” to be found in discourse, meditation and philosophy.
>- Finally, we look for eternal and perfect beauty. This is only to be found beyond the realm of material things, in the world of the *ideas* behind the things.

## Diotima’s ladder (4): Recap [211d, p.59]

>- “Beginning from obvious beauties he must for the sake of that highest beauty be ever climbing aloft, as on the rungs of a ladder, from one to two, and from two to all beautiful bodies; from personal beauty he proceeds to beautiful observances, from observance to beautiful learning, and from learning at last to that particular study which is concerned with the beautiful itself and that alone; so that in the end he comes to know [211d] the very essence of beauty.”

## Alcibiades (Socrates’ lover)

>- Socrates has finished presenting Diotima’s theory. Suddenly, a drunk Alcibiades crashes in.
>- Alcibiades is jealous that Socrates got a seat beside handsome Agathon [213c, p.62].
>- He goes on to praise Socrates: Socrates, he says, is like one of these ugly statues that one can open, and inside they are hollow and beautifully painted and decorated [p.65].
>- In this way, he illustrates Diotima’s point: that although Socrates looks like an ugly old man from the outside, if one looks at his inner qualities he is more beautiful than everybody else. And loving this inner beauty is a step up on Diotima’s ladder from just loving Socrates’ body.
>- But Alcibiades has not yet reached the higher rungs of the ladder. He is still in love with Socrates’ (inner) beauty, unable to transcend it and see the absolute beauty of ideas (which is what Socrates does).

# The text itself

## Now let’s look at the text!

Read the Symposion (highlighted passages).


## The End. Thank you for your attention!

Questions?


<!--

%% Local Variables: ***
%% mode: markdown ***
%% mode: visual-line ***
%% mode: cua ***
%% mode: flyspell ***
%% End: ***

-->
