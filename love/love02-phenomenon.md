---
title:  "Love and Sexuality: 2. The phenomenon of love"
author: Andreas Matthias, Lingnan University
date: September 1, 2019
...

# What is love?

## Your ideas?

>- What is love? How can we define it for someone who’s just arrived from Mars?
>- What distinguishes love from other social phenomena (e.g. friendship)?
>- What is romantic love? What distinguishes that from other forms of love (e.g. Christian love towards one’s neighbour)?
>- Is romantic love the same as a sexual relationship? Can I have sex without love and love without sex?
>- What is the difference between loving and liking?
>- Can one believe one is in love but be wrong about it? 
>- Can one live a worthwhile life without love?

## Does love depend on the properties of the beloved? (1)

>- Appraisal or bestowal?
>- What if the beloved’s properties change (for example, due to ageing)? Does our love then diminish or disappear? 
>- What if I am mistaken about the beloved’s properties? (e.g. he turns out to be a secret agent, a spy, a criminal, or to be a woman instead of a man, like in some Shakespearean plays). Does my love suddenly become unjustifiable?
>- Are there different kinds of properties that we must distinguish (e.g. inner/outer)?

## Does love depend on the properties of the beloved? (2)

>- What if someone else comes along who has the same properties to an even higher degree? Do we then switch the object of our love? Would it be right to switch?
>- If two people have the same properties, is it a random choice whom I fall in love with?
>- If we don’t think that love is based on properties, can we then just love random people?
>- Is the *history* of a love relationship relevant to the actual love experienced at a particular moment?

## Appraisal and bestowal

![](graphics/02-appraisal-bestowal.png)\ 



## Greek and Latin words for love

>- Eros
>- Philia: Friendship.
>- Agape: The unconditional love of God towards his creation, or the unconditional love required of Christians towards their neighbour.
>- Caritas: Charity. Is charity the same as love?
>- Storge: Care, for example for one’s old parents or children.

## The moral side of loving

>- Is love a virtue or a passion?
>- If it is a virtue, is then someone who does not love a bad person? Do we need to love in order to be morally good? Do we have a duty to love?
>- When I benefit from a friendship, is this then a “worse” friendship? Does friendship have to be entirely selfless? Does such a thing as selfless friendship even exist?
>- Is selfless love possible?
>- What if my love contradicts my moral duties? (e.g. I love a killer). What should be stronger? My love or my moral duty?
>- Is self-love morally right, wrong, necessary, meaningless?
>- Is it permissible to be blinded by love, or must love be completely clear-eyed? Does love depend on correct and complete knowledge about the beloved?
>- Is love compatible with obsession?

## The properties of love

>- How would I recognise a love relationship between two people?
>- Is concern for the other person necessary for love?
>- Is concern for the other person sufficient for love?
>- Does love need to be exclusive? Can I love multiple people at the same time?
>- Does love need to be constant? Can I love different people every month but still honestly love them?
>- Is it necessary that I think of the beloved person as irreplaceable? 
>- What if the beloved dies and I find another person to love? Does this mean that I didn’t love the first one? Does it mean that I don’t love the second one? What if they are both alive at the same time?

## Objects of love

>- Can non-human animals love? Can they be objects of love?
>- Can I love someone who is in a coma in a hospital bed? Would this be different from loving someone from a distance who doesn’t know about me? In what way?
>- Is love for inanimate objects (e.g. one’s country) possible? Is it really “love”? In what sense?
>- Is one-sided love possible? Is it still love? Is it worse than reciprocated love?
>- What if I am mistaken about the feelings of the beloved? If what looks like reciprocated love is fake?
>- Is it possible to love and hate the same person at the same time, or are these feelings mutually exclusive?

## Love and sex

>- What is the relationship between love and sex?  (Is totally asexual love deficient in any way?)
>- Is a life without sex a worse life than a life without chocolate? Why?

## The End. Thank you for your attention!

Questions?

<!--

%% Local Variables: ***
%% mode: markdown ***
%% mode: visual-line ***
%% mode: cua ***
%% mode: flyspell ***
%% End: ***

-->
