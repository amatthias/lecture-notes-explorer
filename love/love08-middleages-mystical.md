---
title:  "Love and Sexuality: 8. Middle Ages (1): God’s love and the mystical experience"
author: Andreas Matthias, Lingnan University
date: September 1, 2019
...

# Background

## Sources

Much of this session is based on: Ursula King, “Christian Mystics.” Routledge 2001, 2004.

Also on Irving Singer, “The Nature of Love,” vol.1: Plato to Luther.

## Where we are

>- Let us summarise what we know about the tradition of the concepts of love so far.
>- Love in Plato: Eros, union with the eternal Form of the absolute good. Hierarchical, not symmetrical.
>- Love for Aristotle: Philia, wanting to benefit like-minded friends for their own sake. Symmetrical, mutual.
>- Christian love: A synthesis of:
>     - Platonic ideas (the absolute good as God);
>     - Aristotelian philia (to do good to others); and
>     - Agape (the unconditional love of God that is the basis of our unconditional love towards others).
>- Today we will see how the eros towards God turns, in mysticism, into a personal (and sometimes even sexual) kind of love.
>- In the next session we will see how this forms the basis of medieval courtly and later “romantic” love, of which the essential elements survive until today.

## Mysticism (1)

>- “Mysticism” comes from the Greek μυω = “to conceal.” 
>- “Mystical” referred to secret religious rituals. Such rituals existed in all ancient cultures, they are not specific to Christianity.
>- A μύστης or μυστικός (mystes or mystikos), is an initiate, someone who has been introduced to a secret religious ritual.
>- In the Middle Ages, Christian mystics were mostly monks who experienced a union with God and wrote about it.

## Mysticism (2)

>- Often the language to describe this experience is explicitly erotic or sexual, talking of a “merging” of the mystic with God in an act that is reminiscent of a sexual union.
>- Since God is not a physical body but a spiritual force, that “merging” can be much more intense and complete than it could be in a physical sexual act with another human being.
>- In this sense, it is the ultimate sexual union, the “Platonic Form” of a sexual union.
>- Often the mystics were women, who otherwise are underrepresented in medieval philosophy.
>     - It seems that the social conditions at that time made it hard for women to get an education, but that women’s monasteries were an exception, and that women could there cultivate their talents more freely, resulting in many famous women mystics that were famous and respected even in their own time.


## The Bible: Song of Songs (1)

>- The Bible contains, among other passages that are relevant to mysticism, the “Song of Songs.”

. . . 

> She: “Let him kiss me with the kisses of his mouth —  
  for your love is more delightful than wine.  
  Pleasing is the fragrance of your perfumes;  
  your name is like perfume poured out.  
  No wonder the young women love you!  
  Take me away with you—let us hurry!  
  Let the king bring me into his chambers.”

. . . 

>- Interestingly, it’s a “she” here, like in the mystical tradition of the Middle Ages, where many famous mystics were nuns.
>- The “she” is probably meant to be the soul of man that enters into “marriage” with God’s spirit (and since God is traditionally perceived as male, the soul had to be seen as female).

## The Bible: Song of Songs (2)

> She: Like an apple tree among the trees of the forest  
  is my beloved among the young men.  
  I delight to sit in his shade,  
  and his fruit is sweet to my taste.  
  Let him lead me to the banquet hall,  
  and let his banner over me be love.  
  Strengthen me with raisins,  
  refresh me with apples,  
  for I am faint with love.  
  His left arm is under my head,  
  and his right arm embraces me.
  
. . .

>- It’s hard to see this still as a metaphor for the soul’s journey to God. 
>- The Song of Songs mixes the spiritual journey with the language of bodily love and desire. We will see this later also in the descriptions of the mystics.

## The Bible: Song of Songs (3)

> He: How beautiful your sandaled feet,  
  O prince’s daughter!  
  Your graceful legs are like jewels,  
  the work of an artist’s hands.  
  Your navel is a rounded goblet  
  that never lacks blended wine.  
  Your waist is a mound of wheat  
  encircled by lilies.


> Your neck is like an ivory tower  
  Your eyes are the pools of Heshbon  
  ...  
  Your nose is like the tower of Lebanon  
  looking toward Damascus.

. . . 

>- (That last one is a bit puzzling.)

## The Last Supper

>- At the Last Supper, Jesus explicitly sees the sharing of the bread as a kind of bodily union:

. . . 

> “I am that living bread which has come down from heaven: if anyone eats this bread he shall live for ever. Moreover, the bread which I will give is my own flesh; I give it for the life of the world.... Whoever eats my flesh and drinks my blood dwells continually in me and I dwell in him.” (King, Christian Mystics, p.6)


## Asceticism

>- We already saw how the Desert Fathers attempted to approach God by removing themselves from the temptations of the flesh through fasting, living in the desert, and enduring hardships.
>- Later, out of this community of men came the first monks that organised themselves into monasteries (under Pachomius, 292–348 AD).
>- “It was the aim of ascetic and monastic life to achieve the conquest of self through renunciation so that, once purified from all obstacles, the soul might live the perfect life face to face with God, in direct communion and union with him. In the silence of the desert, in the solitude of the cell, free from all worldly entanglements, the mystic could ascend to the contemplation and knowledge of God, and loving union with him.” (King, p.8)


## The “ladder of perfection” (King, p.9)

>- Three stages through which the mystic had to pass to achieve union with God:
>- It ... begins with the lowest stage ..., the way of purification, understood as detachment, renunciation and asceticism, to move away from the world of the senses and ego to the higher, eternally abiding reality of God.
>- [This] leads to the second stage, which is the illuminative life. At this stage the mystic draws nearer to divine unity, reaching the heights of loving contemplation. 
>- Fully illumined, he or she realizes the ultimate mystery of all that exists and dwells with joy in a state of sublime ignorance, likened to utter darkness, to an abyss of nothingness.
>- This is followed by the highest stage, the unitive life, the ultimate goal of loving union with God, an ecstatic experience of overwhelming joy. 

# Medieval mysticism

## God as a person

>- Singer (p.174) sees St Augustine as the thinker who understood that normal people cannot be emotionally involved with a religion that worships an abstract, transcendent idea (or Platonic Form) of the good.
>- Christianity had to offer the faithful a God that was a relatable *person*.
>- To this contribute both the idea that we were made “in the image of God” and the life and suffering of Christ as a human being on Earth. God is seen as a “father” rather than an abstract idea.
>- Thus, God could be loved even by those who weren’t Platonic philosophers:
>- “In asserting the primacy of feeling, mysticism denies that the philosopher (whether Socrates or another) is worthy of being considered the true lover. The love of God does not require superior training [in philosophy]. ... It depends in part on faith, in part on the purification of erotic feeling.” (p.174)

## The ladder/mountain metaphor (1)

>- We saw that both Plato and Aristotle had their ladder/ascent metaphors:
>     - Plato talks of Diotima’s ladder of Eros, from the bodily to the abstract and purely spiritual.
>     - Aristotle talks of the ladder towards Eudaimonia, the way one develops from akrates to sophron. This corresponds to more and more refined ways of enjoying the philia of similarly good men.

## The ladder/mountain metaphor (2)

>- The medieval mystics often also use a ladder metaphor (Singer, p.179):
>     - “At first, the mystic tries to overcome his sins through fasting, ... meditation, prayer,” and so on.
>     - God then gives the mystic a vision of divinity, a glimpse of God’s world: illumination, enlightenment.
>     - God shows himself to the mystic, and the mystic’s soul can enter into a first loving “union” with God.
>     - Further exercises and purification remove the last bits of pride and attachment to the self (remember the Desert Fathers!) and makes the human worthy of “marrying” God.
>     - The mystic then unites with God to the greatest extent possible for a human being. Human and God become “one spirit.”
>     - In the moment of ecstasy (Greek: ek-stasis, to stand outside oneself) the mystic has lost his own self and is pure spirit, pure divine love. This is the top of the ladder in this world.
>     - After death, the ladder continues upward into heaven.

## Freud on ladders and eros (Singer p. 182)

>- Can you see the connection of ladders to erotic imagery?
>- In his dream analysis, the psychologist Sigmund Freud (1856-1939) likens ladders to the sexual act:
>     - “Steps, ladders or staircases, or ... walking up or down them, are representations of the sexual act. ... We come to the top in a series of rhythmical movements and with increasing breathlessness and then, with a few rapid leaps, we can get to the bottom again. Thus the rhythmical pattern of copulation is reproduced in going upstairs.”

## Ladders in love (Richard of St Victor)

>- (As cited in Singer, p.184)
>- First degree: lovers are stricken by the “bolt out of the blue,” an unexpected meeting that arouses the desires. The lover is “shot through the heart.”
>- In the second stage, love overwhelms the senses and the imagination. It binds the lover to the object of his devotion (p.185). The lover constantly dreams of the beloved. In the love of God, the lover sees God in all his beauty.
>- In the third stage, “the soul is captivated, all other interests driven out. ... The lover loses all autonomy.” In mystical love, this is the point where the soul passes over into God (ecstasy).
>- In the fourth stage, love becomes unbearable and the lover finally rejects the control that the beloved has over him. In the spiritual love, the soul “demeans itself” to serve God in the same way as Christ “demeaned himself” when he became a man. The soul now returns to Earth, happy to serve God, loving all things and all mankind for the sake of God.


# The experiences of Christian mystics

## St. Bernard of Clairvaux (1090–1153)

>- Bernard distinguishes carnal and social love, love of self and love of others, and divides love into four degrees. 
>     - First we love ourselves for our own sake; 
>     - then we love God, but for our own sake. 
>     - Different again is when we love God for his sake and, 
>     - the highest degree, our love of ourselves for God’s sake. The divine love is sincere because it does not seek its own advantage.^[King, p.32]

## St. Bernard of Clairvaux (2)

> “To lose yourself, as if you no longer existed, to cease completely to experience yourself, to reduce yourself to nothing is not a human sentiment but a divine experience...

> “It is deifying to go through such an experience. As a drop of water seems to disappear completely in a big quantity of wine, even assuming the wine’s taste and color, just as red, molten iron becomes so much like fire it seems to lose its primary state; just as the air on a sunny day seems transformed into a sunshine instead of being lit up; so it is necessary for the saints that all human feelings melt in a mysterious way and flow into the will of God. Otherwise, how will God be all in all if something human survives in man?” ^[King, p.32]

## Hildegard of Bingen (1098–1179)

>- Hildegard von Bingen sees God’s presence everywhere in nature, as the force that gives life to all things (itself an erotic motive).
>- “For her, wisdom is less about thinking than tasting. In Latin, wisdom (sapientia) and taste (sapere) are words stemming from the same root.” (King, p.39)

. . . 

> “I, the fiery life of divine essence, am aflame beyond the beauty of the meadows. I gleam in the waters. I burn in the sun, moon, and stars. With every breeze, as with invisible life that contains everything, I awaken everything to life.

> “I am the breeze that nurtures all things green. I encourage blossoms to flourish with ripening fruits. I am the rain coming from the dew that causes the grasses to laugh with the joy of life.”


## Jan van Ruusbroec (1293–1381)

> “If we look deep within ourselves, there we shall feel God’s Spirit driving and urging us on in the impatience of love; and if we look high above ourselves, there we shall feel God’s Spirit drawing us out of ourselves and bringing us to nothing in the essence of God, that is, in the essential love in which we are one with Him, the love which we possess deeper and wider than every other thing.”


## St. Catherine of Genoa (1447–1510)

> “I am so...submerged in His immense love, that I seem as though immersed in the sea, and nowhere able to touch, see or feel [anything] but water. ... My being is God, not by simple participation but by a true transformation of my being.”


# The erotic element in mystical union


## St Gertrude (1256–1302)

> “My heart craves the kiss of your love, my soul thirsts for the most intimate embrace joining me to you.”


## Mechtild von Magdeburg (1241–1299)

> “Lord, love me hard, love me long and often. I call you, burning with desire. Your burning love enflames me constantly. I am but a naked soul, and you, inside it, a richly adorned guest.”

## Hadewijch of Antwerp (first half of thirteenth century)

> “My heart and my arteries, and all my limbs quivered and trembled with desire. I felt myself so violently and dreadfully tested it appeared that if I did not give satisfaction to my lover entirely, to know him, to taste him in every part of his body and if he did not respond to my desire, I would die of rage...”


## Hadewijch of Antwerp (2)

> “He came, handsome and sweet, ... I approached him submissively ... And he gave himself to me as he usually does, in the form of the sacrament. Then he came to me in person and took me in his arms and locked me in his arms. All my limbs felt this contact with his with equal intensity, following my heart, as I had desired. Thus externally, I was satisfied and quenched. ... Following which, I remained merged with my lover until I had melted entirely within him in such a way as nothing was left of me.”

## Bernini “The Ecstasy of Saint Teresa” (1652)

![](graphics/bernini3.png)\


## St Teresa of Avila (1515–1582)

> “I saw in his hand a long spear of gold, and at the iron's point there seemed to be a little fire. He appeared to me to be thrusting it at times into my heart, and to pierce my very entrails; when he drew it out, he seemed to draw them out also, and to leave me all on fire with a great love of God. The pain was so great, that it made me moan; and yet so surpassing was the sweetness of this excessive pain, that I could not wish to be rid of it.”

## St Teresa of Avila (1515–1582)

> “The soul is satisfied now with nothing less than God. The pain is not bodily, but spiritual; though the body has its share in it. It is a caressing of love so sweet which now takes place between the soul and God, that I pray God of His goodness to make him experience it who may think that I am lying.”


## Conclusion

>- Christian love, beginning as a version of Platonic Eros, turns into actual human-like love in the mystical experience.
>- God stops being only an abstract concept and becomes an *experienced* being, someone who can be touched and with whom one can undergo a kind of physical, even erotic, union.
>- In the next session we will see how this move of God down from the level of abstraction and into the realm of physical experience became the basis for courtly love in the Middle Ages.
>- From courtly love, in turn, we have inherited many ideas still present in modern, romantic love.


# The End. Thank you for your attention!

<!--

%% Local Variables: ***
%% mode: markdown ***
%% mode: visual-line ***
%% mode: cua ***
%% mode: flyspell ***
%% End: ***

-->
