---
title:  "Love and Sexuality: 14. Concepts and features of love"
author: Andreas Matthias, Lingnan University
date: October 21, 2019
...


# Features of love

## Sources

>- Helm, B. W. (2010): Love, friendship, and the self: Intimacy, identification, and the social nature of persons. Oxford University Press.
>- An older version of the Stanford Encyclopedia of Philosophy article: “Love.”
>- Alan Soble (1990): The Structure of Love, Yale University Press.


## Characteristics of love

>- *How is loving different from liking?* I can love my girlfriend, but I can like a chocolate cookie. Are these relationships different and in what way?
>- Love generally is assumed to be distinguished from other relationships or emotions by having particular characteristics:
>     - Exclusivity
>     - Constancy
>     - Reciprocity
>     - Uniqueness
>     - Irrepleaceability of the beloved^[Soble (1990): The Structure of Love.]
>- Try to apply these to the cookie example to see how it works out.

## Loving and liking

>- How is loving different from liking?
>- Other possible answers:
>     - Love acknowledges that the other person has an intrinsic value, a value as a person. 
>     - Accordingly, we can “wish her well for her own sake” as Aristotle and Aquinas would say.
>     - Liking does not seem to require that. 
>     - I can like a chocolate cookie by seeing the cookie as only instrumental to my own happiness, not as something that has a value in itself.


## Is liking instrumental?

>- Singer: Liking is a matter of desiring something for *my* own good (not for the desired thing’s or person’s own good).
>- Is this convincing?
>     - It seems that there might be relationships in between the two extremes. 
>     - I might like a colleague at work for their intrinsic value and wish for their own good without actually *loving* them.
>     - The same is true for agape love towards a beggar on the street. I might wish to do good to them for their own sake, but we wouldn’t call that proper, personal love.

# Love as union

## Love as union

>- Another attempt would be to see love as a *union* between two people.
>- Liking, in contrast, does not establish a union.
>- *But what makes a union a union?*
>- Some kind of serious commitment, as a result of which the interests of the lover fuse with the interests of the beloved.
>- A union of two people is created, in which both partners share common interests as a union, rather than only adding up their individual interests.

## Love as a union of interests

>- The “union of interests” can take different forms:
>- Scruton (1986): Love exists “just so soon as reciprocity becomes community: that is, just so soon as all distinction between my interests and your interests is overcome.”^[Scruton, R., 1986, Sexual Desire: A Moral Philosophy of the Erotic, Free Press.] (p.230)
>- Solomon (1988)^[1988, About Love: Reinventing Romance for Our Times, Simon & Schuster.]: “Love is the concentration and the intensive focus of mutual definition on a single individual, subjecting virtually every personal aspect of one's self to this process.” (p.197)

## Nozick on love as a union (1)

>- Nozick (1989)^[Nozick, R., 1989, “Love's Bond”, in The Examined Life: Philosophical Meditations, Simon \& Schuster, 68–86.]:
>     - What is necessary for love is not so much the actual union, but the *desire* to form a “we,” together with the desire that your beloved reciprocates.
>- That new “we” creates a new web of relationships between the lovers which makes them be no longer separate individuals.
>- Lovers “pool” their well-beings together: The well-being of each is tied up with that of the other.
>- They also pool together their autonomy: “Each transfers some previous rights to make certain decisions unilaterally into a joint pool” (p.71).

## Nozick on love as a union (2)

Nozick: The lovers:

>1. want to be perceived publicly as a couple;
>2. attend to their pooled well-being; and
>3. accept a “certain kind of division of labor” in their relationship. For example, one might find something interesting to read, but leave it for the other person to read instead.

. . . 

In this sense, the lovers form a new being, a “we,” that is separate from their two individual beings.


## Love as union: Criticism (1)

>- Is this a good description of love? Criticism?
>     - It would explain why charity towards a beggar is not proper love.
>     - It would explain why liking a cookie is not love.
>     - But what about arranged (or medieval) marriages? They create unions but they don’t necessarily require (or encourage) love.
>     - Courtly love happens largely outside of the sphere of common interests, as does unfulfilled romantic love.
>     - Other kinds of relationships also create common interest unions (sometimes only for a short time) without being romantic love.
>     - For example, work relationships, boss and employee, teacher and student, taxi driver and passenger, kidnapper and kidnapped.

## Love as union: Criticism (2)

>- One could criticise the union theory as taking away the autonomy of the lovers.
>- Generally, we tend to see autonomy as something good and morally valuable.
>- For example, Kant sees moral autonomy as the basis of all moral behaviour and human value (“dignity”).
>- If we give our autonomy away in love, does this make love bad? Do we become worse human beings when we are in love, because we are less autonomous?
>- Singer: a necessary part of loving is respect for your beloved as the particular person he or she is, and this requires respecting their autonomy.
>- Taking the lover’s autonomy away would therefore diminish love itself.

## Love as union: Criticism (3)

>- Finally, love as union seems to exclude love where union is not possible due to the circumstances (medieval courtly love, loving the dead and so on).
>- What could a “love as union” theorist answer to these criticisms?
>     - This loss of autonomy is actually desirable and it is what makes love special.
>     - Or we could say that it is a loss, but an acceptable loss, considering the benefits from such a union.
>     - Solomon calls the loss of autonomy in love “the paradox of love.”

## Love as union: Criticism (4)

>- Aristotle and Aquinas both think that love is defined as a wish to benefit the other person, to act so that one benefits them for their own sake.
>- Do you see how this creates a problem for the union view?
>- If, in the union view, the two lovers unite and their interests merge, then how can one benefit the other?
>- Benefiting the other would always be selfish: one would be benefiting (in part) oneself.

## Love as union: Criticism (5)

>- What do you think of this? What could the “love as union” supporter answer?
>     - This is actually a good feature of the theory.
>     - The “love as union” theory explains how individuals (who are essentially egoistic) can overcome their egoism and act so that they benefit others (because they are, at the same time, also benefiting themselves).
>     - What does this remind you of? Who other philosopher had a similar concept of the interdependence of human interests?
>     - This is similar to Aristotle’s idea that the sophron individual benefits others and himself at the same time. 
>     - Virtues are properties of character that benefit oneself and others.


## Love as federation of selves

>- In order to avoid the problems of a union view, Friedman (1998)^[Friedman (1998): “Romantic Love and Personal Autonomy”, Midwest Studies in Philosophy, 22: 162–81.] proposed a “federation” model for love:
>- “On the federation model, a third unified entity is constituted by the interaction of the lovers, one which involves the lovers acting in concert across a range of conditions and for a range of purposes. This concerted action, however, does not erase the existence of the two lovers as separable and separate agents with continuing possibilities for the exercise of their own respective agencies.” (p.165)
>- This would allow for altruism in love. Since the lovers are only “acting” together, but not really merging into one, they can still benefit each other and care for each other’s interests.


# Love as life choice

## Love as a life choice (1)

>- The union view also emphasises that who we are is, in part, defined by whom we love.
>- Nussbaum (1990)^[Nussbaum, M., 1990, “Love and the Individual: Romantic Rightness and Platonic Aspiration”, in Love's Knowledge: Essays on Philosophy and Literature, Oxford: Oxford University Press, 314–34.]: “The choice between one potential love and another can feel, and be, like a choice of a way of life, a decision to dedicate oneself to these values rather than these.”
>- Liking is not so exclusive. I can like chocolate cookies, but I can also like a cheese cake. I don’t need to “dedicate myself” to either.

## Love as a life choice (2)

>- Criticism?
>- This might be a good *additional* criterion, but it’s not enough to alone define love.
>- For example, the choice of a job requires identification of this kind. By choosing a job, I am dedicating my life to one kind of activity and values. But this is not love.
>- People who like a football club, pop group, health or political cause, are sometimes changed by that liking, in that “being a fan/follower/promoter of X” becomes an important part of their self-description. Still, we wouldn’t call such associations “love.”


# Love as robust concern

## Love as robust concern

>- A second view would put the *concern for the beloved* into the centre of a definition of love.
>- What makes a relationship “love” is a “robust concern” for the other person. (Robust: strong and enduring over time.)
>- Frankfurt: “That a person cares about or that he loves something has less to do with how things make him feel, or with his opinions about them, than with the more or less *stable motivational structures* that shape his preferences and that guide and limit his conduct.”
>- What counts are the “stable motivational structures,” that is, the lover’s enduring concern for the welfare of the beloved.

## Criticism of the “robust concern” theory (1)

>- How can we criticise this view of love as robust concern?
>- One problem is that, in this view, love could be one-sided and missing the interaction between the lovers.
>- I can have a concern for another person, although they don’t love me back (or are not concerned about me). Is this then “proper” love? Wouldn’t it be better if the concern was reciprocal?
>- What if my concern makes me act in a way that *I* think will benefit my beloved, although the beloved does not want me to act in this way?
>     - For example, I see my beloved smoke. 
>     - Robust concern for their health might lead me to take away their cigarettes.
>     - Is my action an expression of love? Or would love require me to respect my beloved’s autonomy instead?

## Criticism of the “robust concern” theory (2)

>- Can there be love without a robust concern?
>- For example, can I “love” a troublesome relative of mine whom I don’t actually want to meet or benefit?
>- Can we love the dead? (Badhwar 2003)^[“Love”, in H. LaFollette (ed.), Practical Ethics, Oxford: Oxford University Press, 42–69.] If love is robust concern, and the dead are beyond any concern (because it is impossible to benefit or harm them), how can I say that I love a dead person?

## Criticism of the “robust concern” theory (3)

>- Conversely, can I be concerned about the well-being of people or things without being in love with them?
>- Doctors, psychologists, politicians, teachers might be genuinely concerned about the welfare of others without being in love with them.
>- A dog keeper might be robustly concerned about the welfare of their dog. Still, this is not “romantic love”.
>- It seems like “robust concern” is not quite enough to distinguish romantic love from weaker forms of benefiting, caring and liking.

# Kant, dignity, and love as valuing

## Love as valuing

>- We already talked about the two different forms of valuing something: 
>     - Valuing its qualities (appraisal); or 
>     - Giving value to something as the result of a decision that is not dependent on the properties of the valued thing (or person) (bestowal).
>- In the bestowal view, the beloved *comes to be valuable* to the lover as a result of the lover loving the beloved.
>- For the appraisal theory, it is not enough to appraise qualities *as part* of a love relationship. Even bestowal probably involves some initial appraisal.
>- The point of an appraisal theory must be that *love consists entirely in that appraisal.*

## Kant, dignity and love

>- Remember Kant. What is the relation between dignity and price?
>- Things have a value that can be compared to the values of other things.
>- We can exchange things of similar value with each other.
>- But human beings cannot be replaced by other humans “of similar value.” There is no such thing as a measurable, finite “value” for humans.
>- Instead, human beings have a unique kind of absolute value that Kant calls “dignity.” Dignity comes from the realisation of the moral autonomy of human beings.
>- As a result of this unique, infinite value, we can never treat human beings as mere means to other ends, but must treat them as “ends in themselves.”
>- Examples?
>- Taxi driver, waiter in restaurant. Furthering *their ability to pursue their own ends,* instead of my own.

## Velleman: Love is the appreciation of the dignity of others (1)

>- Love is a response to the dignity of persons. It is the dignity of the object of our love that justifies that love.
>- As a result, I cannot treat my lover as a means to my ends only, but have to treat them as an end in themselves.
>- Therefore, it is impossible to love a chocolate cookie, a dog, or a country. These things don’t have moral autonomy and therefore they don’t have dignity.
>- But not all appreciation of human dignity is love.
>- Just paying the taxi driver or restaurant waiter is not an act of love.
>- For Velleman^[Velleman, J. D., 1999, “Love as a Moral Emotion”, Ethics, 109: 338–74.], we can have different responses to other people’s dignity.
>- Love is *the maximal response* to others’ dignity.

## Velleman: Love is the appreciation of the dignity of others (2)

>- What makes me have this maximal response to particular people and not others? 
>     - For Kant, all people have the same value (dignity). 
>     - Why don’t I respond to all in the same way?
>- Helm^[Helm, B. W. (2010). Love, friendship, and the self: Intimacy, identification, and the social nature of persons. Oxford University Press.]: “The answer ... lies in the contingent fit between the way some people behaviorally express their dignity as persons and the way I happen to respond to those expressions by becoming emotionally vulnerable to them. ... The right sort of fit makes someone ‘lovable’ by me.”


## Velleman: Criticism

>- Do you see any problems with this account?
>- First, what exactly is meant by “the contingent fit” that causes me to respond in a particular way to others’ behavioural expressions of their dignity as persons?
>- Why would only some people be lovable to me and others not?
>- If the reason are particular attributes of the beloved, then we have a common “appraisal” theory of love. Then the whole thing about Kant and dignity is not needed and does not contribute anything.
>- If we speak with Kant of appraising human dignity, then we must extend this appraisal to all humans equally. This was a very important point to Kant: all humans (and only humans) have dignity, and all are equal in dignity. But then, why is love selective and not universal?

# Criticism of ‘appraisal’ theories of love

## Criticism of appraisal accounts in general (1)

>- Some of the problems with Velleman’s account are problems of appraisal theories of love in general:
>     - Appraisal does not only lead to love. We appraise things and people all the time.
>     - I appraise a bike that I want to buy. I appraise a basketball player whom I admire. I appraise a good student’s performance in class.
>     - Why do these appraisals not constitute love?
>     - You might say, because they don’t have that particular strong effect on my emotions and my motivations that love has.
>     - But this seems to be begging the question. *Why* do these other appraisals not have the same effects? If there is a difference between love and other kinds of appraisal, then love is more than appraisal, rather than identical with it.
>     - An appraisal theory of love must explain what exactly makes appraisal-love different from appraisal-admiration, appraisal of students’ performance, appraisal of consumer goods an so on.
>     - Common appraisal theories don’t do that.

## Criticism (2): Love of types rather than individuals

>- Vlastos (1981)^[Vlastos, G., 1981, “The Individual as Object of Love in Plato”, in Platonic Studies, Princeton, NJ: Princeton University Press, 3–42, 2nd edn.]: Appraisal accounts are really a love of properties rather than a love of persons. This is also true of Plato and Aristotle.
>- By loving a person with properties (P1...Pn), I really love a *type of person* who has such properties, rather than an *individual* person.

## Criticism (3): Fungibility

>- A second problem is *fungibility.* 
>- To be fungible: to be replaceable by another relevantly similar object without any loss of value.
>- Money is fungible: I can change a twenty dollar bill into two ten dollar bills and nothing is lost.
>- But can I do the same with people? Can I exchange my girlfriend with another with similar properties without any loss?

## Criticism (4): Substituting Nancy (1)

>- An extreme example of fungibility is the substitution argument^[Soble (1990): The Structure of Love, Yale University Press.].
>- Mark Bernstein: “I have a wife, Nancy, whom I love very much. Let us suppose that I were informed that, tomorrow, my wife Nancy would no longer be part of my life, that she would leave and forever be unseen and unheard of by me. But, in her stead, a Nancy\* would appear, a qualitatively indistinguishable individual from Nancy. Nancy and Nancy\* would look precisely alike, act precisely alike, think precisely alike, indeed would be alike in all physical or mental details.”

## Criticism (4): Substituting Nancy (2)

>- How would you react? Would you love Nancy\*?
>- The problem with this argument is the initial assumption that Nancy is entirely indistinguishable from Nancy\*!
>- If this is true, then nobody could tell that Nancy has been replaced -- not even Nancy herself! (Soble). Nancy might have been replaced already previously and I wouldn’t have noticed.
>- The problem with Nancy can be addressed by replacing appraisal theories with, for example, a “love as union” theory.

## Fungibility again

>- We could also try to argue that appraisal theories don’t necessarily lead to fungibility of lovers.
>- Soble, Structure, Ch.3 (p.48ff): Roger Scruton (“Sexual desire”) tries to show how love can be reason-dependent, but at the same time exclusive:
>     - Imagine, he says, that there is someone who loves Beethoven’s Violin Concerto. 
>     - This love is reason-based: someone who enjoys the Violin Concerto must be able to answer the question why he enjoys it, by referring to its properties. 
>     - Yet this person may enjoy no other music, or even dislike all other compositions.
>     - If such a person is *possible* then aesthetic and love reasons are not general. They can be specific to particular things and don’t need to apply to everything that fulfils the criteria.
>- *Do you think that this is a good argument?*

## Criticising exclusive appraisal love

>- Martha Nussbaum: What Scruton should have concluded from his example is exactly the opposite of his actual conclusion: that his love for music *cannot* rationally be exclusive.
>- The music lover *must* either like any other piece of music that satisfies his criteria, or he must give some additional reason why he does not.
>- His position is irrational.
>- Scruton’s position would only make sense if he argued that:
>     - His criteria for loving this particular piece of music are general; but
>     - These criteria are only satisfied *by this particular* piece of music and no other music.
>- *But is this likely? Are we truly unique as persons? Is our particular mix of properties unique?*

## Human uniqueness (1)

Are we really unique personalities?

>- Soble: no. (Structure, p.52):
>- “Of course, Jesus was non-trivially unique, and so were Gandhi, Moses [and others]. Alkibiades loved Socrates, and only Socrates, because “he is like no other human being, living or dead.” [...] But substantive uniqueness is not very common.”
>-“Most of us draw on the same stock supply of merits and defects, good traits and bad traits, in building our personalities. ... Our mannerisms, physiognomy, ways of walking, sense of humour, and linguistic habits are close copies of the traits of our parents, siblings, peers and other models. ... Indeed, worries about the homogenization of personality and the conformity fostered by the media and schools presuppose that substantive uniqueness is rare enough to be a matter of social concern.”

## Human uniqueness (2)

>- But we could try to argue against that.
>- William Galston: “we view human beings as unique ... [because] even though every quality an individual has is possessed by others to some extent, the manner in which qualities are combined and emphasized ... is distinctive.”
>- *What do you think of this argument?*

## Human uniqueness (3)

>- Criticism:
>     - First, many combinations will only be trivially different from others. Persons near each other on the various dimensions will not be easily distinguishable (Soble, Structure, 53).
>     - Second, the idea that each dimension takes on a “very large number of discrete values” is questionable. How many kinds of humour, wit, or beauty are there? If the number of properties is only a dozen or so, then the number of combinations is not very large.
>     - Third, attributes and their values are not distributed randomly. Some properties always go together with others, and some are more likely to appear in clusters. Social factors destroy randomness and make people congregate at specific locations in that possibility space.

# Does bestowal need appraisal?

## Bestowal of love

>- For Singer, bestowal of love consists in projecting a special kind of value into the beloved person: “Love ... confers importance no matter what the object is worth.” (p.273)
>- Bestowal means “caring about the needs and interests of the beloved, ... wishing to benefit or protect her, ... delighting in her achievements” and so on. (p.270)
>- This seems like the robust concern view. But what is the difference?
>- In the robust concern view, the concern is the core of love.
>- For Singer, the concern is the *consequence or effect* of the love that is established through bestowal.


## Criticism of bestowal theories (1)

>- Can you see how Singer’s view can be criticised?
>- The idea of bestowal of love without appraisal is not clear at all.
>     - Without any appraisal, I would have to love everyone in the same way. There would be no way to tell the difference between one person and another in terms of lovability.
>     - This is fine for agape, but not a convincing way to describe romantic love.
>     - In this view, Anna could not change her mind about loving Harry Lime, not would she have reason to.
>- The justification of love therefore becomes impossible, and love remains a mystery.
>- Or we admit that we *do* appraise at least *some* qualities of the beloved, but then the theory becomes a hybrid appraisal/bestowal theory, not pure bestowal.


## Criticism of bestowal theories (2)

>- Finally, how to distinguish between bestowal love, admiration, respect and other such attitudes? 
>     - All seem to be bestowed in similar ways.
>     - In order to distinguish between them, I’d have to introduce other theories again, for example, robust concern, union and so on.


## Appraisal *and* bestowal

>- Jollimore (2011)^[Jollimore, T, 2011, Love's Vision, Princeton, NJ: Princeton University Press.]:
>     - Appraisal is something like perception. Bestowal is something like action.
>     - We perceive particular properties in the beloved that we consider valuable (appraisal).
>     - Then we attend to that person and appreciate their properties in a way that we don’t do with the properties of strangers.
>     - Our appreciation of the beloved’s properties “silences” the similar properties of strangers for our perception.
>     - So love is both appraisal (initially) but develops towards a bestowal. The beloved is perceived as unique as a consequence of that bestowal.

## Appraisal *and* bestowal

>- An approach like this tries to bring together the two theories.
>- This seems similar to the “relationship view” we talked about last time (see the Julia Driver reading on Love and Duty).
>- In order to make sense of that, we must distinguish:
>     - “Falling in love,” which is based on appraisal of the future beloved’s qualities; and
>     - “Staying in love,” which is based on the bestowal of love as the basis of a special relationship that defines that love.

