---
title:  "Love and Sexuality: 9. Middle Ages (2): Courtly and Romantic Love"
author: Andreas Matthias, Lingnan University
date: September 1, 2019
...

# Courtly love

## What is courtly love?

>- A kind of love that first was only a literary device, but soon was accepted in the Middle Ages as the proper way for a knight to be in love with a lady.
>- It was exclusively a phenomenon for the upper classes.
>- Love songs were sung by travelling singers, called troubadours.
>- Courtly love was not generally meant to lead to any kind of (sexual or otherwise) union.
>- The lady would generally be married to someone else. Courtly love was a kind of a game, a fictional relationship, a dream.
>- As such, it was accepted even by the husband of the lady.
>- Marriages were forced for political and economic reasons and had little to do with love.
>- Courtly love, in contrast, was free and voluntary.
>- It was inspired by works of the Roman poet Ovid, and this is where our word “romantic” and “romance” comes from, meaning: the “Roman” style of love poetry (=Ovid’s style).


## What is courtly love?^[https://www.ancient.eu/Courtly_Love]

>- Andreas Capellanus (12th cent. CE): De Amore. He sets down some of the rules of the genre:
>     - Marriage is no excuse for not loving
>     - One who is not jealous, cannot love
>     - No one can be bound by a double love
>     - Love is always increasing or decreasing

## What is courtly love?

Central motifs of courtly love poetry:

>- A beautiful woman who is inaccessible (either because she is married or imprisoned)
>- A noble knight who has sworn to serve her
>- A forbidden, passionate love shared by both
>- The impossibility or danger of consummating that love


## Stages of courtly love^[Barbara Tuchman (1978): “A Distant Mirror”]

>- Attraction to the lady, usually via eye contact
>- Worship of the lady from afar
>- Declaration of passionate devotion
>- Virtuous rejection by the lady
>- Renewed wooing with oaths of virtue and eternal faithfulness
>- Moans of approaching death from unsatisfied desire (and other physical manifestations of lovesickness)
>- Heroic deeds which win the lady's heart
>- (The last two are often not present:)
>     - Consummation of the secret love
>     - Endless adventures and deceptions to avoid detection


# The first troubadour: William the ninth, Duke of Aquitaine

## William’s life (1)

>- William’s life is worth retelling, because it’s such a beautiful tale of adventure. One can hardly believe that this is a real life and not a Hollywood production! (From: William Reddy, The Making of Romantic Love)
>- William, ninth duke of Aquitaine and seventh count of Poitou (1071-1126, ruled 1086-1126), is the earliest-known troubadour. 
>- Troubadours were composers of songs that defined courtly love.
>- Many others followed William’s example and copied the style of his poems and compositions, creating the whole genre of courtly love poetry.

## William’s life (2)

>- His father, William VIII (1024–86, ruled 1058–86), had terminated his first two marriages. You were not supposed to do that in the Middle Ages.
>- The local bishop of Paris supported William’s father and did not object to him re-marrying, but the Popes (first Alexander II, then Gregory VII) questioned the third marriage.
>- Since the third marriage finally gave William a son (William IX), his father was very eager to have his marriage legalised and his son fully recognised.
>- The Pope sent delegates to discuss the situation.

## William’s life (3)

>- When William saw that they would decide against him, he sent his knights to break up the meeting.
>- Threatened by the Pope, William (the father) went to Rome. The Pope did not recognise his marriage, but he accepted his son as legitimate. In return, William promised to found and finance a new monastery in his home town.
>- When he came back, he forced three of his vassals (men who had lands but who depended on William) to sign away their lands to the new monastery.

## William’s life (4)

>- The young William IX soon started to participate in his father’s deals.
>- In 1100, having become duke himself, he decided to join the crusade to Jerusalem.
>- The crusade was a disaster. The majority of William’s soldiers were killed.
>- William fought for a few months in Jerusalem, then returned home.
>- His authority being questioned and without military success, he retreated from public life.
>- He started drinking and writing love poems and became the first troubadour.

# Troubadours and their love poems

## Guillaume de Machaut: “Foy porter” (14th cent.)

> I want to stay faithful, guard your honor,
  Seek peace, obey
  Fear, serve and honor you,
  Until death,
  Peerless Lady.

## Guillaume de Machaut: “Foy porter” (14th cent.)

> For I love you so much, truly,
  that one could sooner dry up
  the deep sea
  and hold back its waves
  than I could constrain myself
  from loving you,

> without falsehood; for my thoughts
  my memories, my pleasures
  and my desires are perpetually
  of you, whom I cannot leave or even briefly forget.


## Guillaume de Machaut: “Foy porter” (14th cent.)

> There is no joy or pleasure
  or any other good that one could feel
  or imagine which does not seem to me worthless
  whenever your sweetness wants to sweeten my
  bitterness.

## Guillaume de Machaut: “Foy porter” (14th cent.)

> You are the true sapphire
  that can heal and end all my sufferings,
  the emerald which brings rejoicing,
  the ruby to brighten and comfort the heart.
  
> Your speech, your looks,
  Your bearing, make one flee and hate and detest
  all vice and cherish and desire all that is good.

## Features of the poem

>- “Your looks, ... make one ... desire all that is good.”
>     - Plato, but also the Christian ideal of God (“all that is good”) now projected onto the beloved.
>     - As opposed to Platonic eros, the other person *is* all that is good, instead of just being a mere “shadow” of some transcendent, ideal good.


## Another love song of Guillaume de Machaut (14th cent.) (1)

Lady, I am one of those who willingly endures
your wishes, so long as I can endure;
but I do not think I can endure it for long
without dying, since you are so hard on me
as if you wanted to drive me away from you,
so I should never again see the great and true beauty
of your gentle body, which has such worth
that you are of all good women the best.

## Another love song of Guillaume de Machaut (14th cent.) (2)

Alas! thus I imagine my death.
But the pain I shall have to bear
would be sweet, if I could only hope,
that before my death, you let me see you again.
Lady, if ever my heart undertakes anything
which may honor or profit my heart,
it will come from you, however far you may be,
for never without you, whom I love very loyally,
nor without Love, could I undertake it or know it.

# Stories of romantic love

## Ancient, pre-courtly: Ovid, The Metamorphoses, Book X

>- Publius Ovidius Naso (43 BC – 17/18 AD).
>- One of the greatest poets of Roman literature.
>- The *Metamorphoses* is a very long poem, first published around 8 AD.
>- Although it is a work of the ancient world, it was very influential in the Middle Ages. It inspired many writers, including the troubadours and later also William Shakespeare.
>- Part of it is the ancient Greek story of Orpheus and Eurydice.

## Orpheus and Eurydice

> While the newly wedded bride, Eurydice, was walking through the grass, ... she was killed, by a bite on her ankle, from a snake, sheltering there. When the poet Orpheus had mourned for her, greatly, in the upper world, he dared to go down to [the Underworld] to see if he might not move the dead [to return her].

## Orpheus and Eurydice

> He came to ... the lord of the shadows, he who rules the joyless kingdom. Then striking the lyre-strings to accompany his words, he sang: ‘O gods of this world, placed below the earth, to which all, who are created mortal, descend; ... My wife is the cause of my journey. ... A [snake] ... robbed her of her best years. I longed to be able to accept it, and I do not say I have not tried: Love won.’

## Orpheus and Eurydice

> ‘Eurydice, too, will be yours [the gods’ of the underworld] to command, when she has lived out her fair span of years, to maturity. I ask this benefit as a gift; but, if the fates refuse my wife this kindness, I am determined not to return: you can delight in both our deaths.’

## Orpheus and Eurydice

> The bloodless spirits wept as he spoke, accompanying his words with the music. ... The king of the deep and his royal bride could not bear to refuse his prayer, and called for Eurydice.

## Orpheus and Eurydice

> She was among the recent ghosts, and walked haltingly from her wound. Orpheus received her, and, at the same time, accepted this condition, that he must not turn his eyes behind him, until he emerged [to the upper world], or the gift would be null and void.

## Orpheus and Eurydice

> They took the upward path, through the still silence, steep and dark, shadowy with dense fog, drawing near to the threshold of the upper world. Afraid she was no longer there, and eager to see her, the lover turned his eyes. In an instant she dropped back, and he, unhappy man, stretching out his arms to hold her and be held, clutched at nothing but the receding air. 

## Orpheus and Eurydice

> Dying a second time, now, there was no complaint to her husband (what, then, could she complain of, except that she had been loved?). She spoke a last ‘farewell’ that, now, scarcely reached his ears, and turned again towards that same place [the underworld].

## Orpheus and Eurydice

>- What does this tale say? In what sense does it illustrate romantic love?
>     - Love (between man and woman) is stronger than death (originally, since Orpheus gets his wife back).
>     - Love (between man and woman) is stronger than caution or rationality (since Orpheus cannot help but look at Eurydice!), thus losing her again.
>- Compare this tale to Platonic eros and Aristotelian philia. How would Plato and Aristotle judge this story and Orpheus’ behaviour?
>     - This would never have been a tale that Aristotle or Plato would have appreciated!
>     - Wisdom?
>     - Eudaimonia?
>     - Phronesis?
>     - Emotional detachment?

#  The typical representative: Tristan and Isolde

## Tristan and Isolde (Iseult)

>- Gottfried of Strassburg’s (died approx. 1210) version was the most popular and well-known in the Middle Ages.
>- It was made popular in the 12th century and had a great influence on how romantic love was (and still is) perceived.
>- We may have forgotten the story, but we still embrace its vision of love.
>- Story (as told by Joseph Bedier, transl. by Belloc, 1945), quoted from Wagoner: “The Meanings of Love.”

## Tristan and Iseult: The plot (1)

>- After defeating another knight, Tristan goes to Ireland to bring back the fair Iseult for his uncle King Mark to marry. 
>- Along the way, they ingest a love potion (believing it is just wine), which causes the pair to fall madly in love.
>- In the courtly version, the potion's effects last for a lifetime; in the common versions, the potion's effects wane after three years.
>- In some versions, they ingest the potion accidentally; in others, the potion's maker instructs Iseult to share it with Mark, but Iseult deliberately gives it to Tristan instead. 

## Tristan and Iseult: The plot (2)

>- Although Iseult marries Mark, she and Tristan are forced by the potion to seek one another as lovers. 
>- While the typical noble Arthurian character would be shamed from such an act, the love potion that controls them frees Tristan and Iseult from responsibility. 
>- The king's advisors repeatedly try to have the pair tried for adultery, but again and again the couple use trickery to preserve their façade of innocence. They even replace Isolde by another women in her wedding night with the king.

## Tristan and Iseult: The plot (3)

>- At some point, King Mark finds them sleeping in the forest side by side, but *with a sword lying between them.*^[We will see later why this is important.] He concludes that they stayed honourably separated, and lets them return to his court.
>- Various endings. In some versions the love potion wears off. In others, it persists. Some end tragically, as their love is discovered by the king. In others, Tristan meets another woman later.

## Tristan and Iseult: The plot (4)

>- In some versions, the story of Tristan and Iseult also ends in death. 
>- Long separated from Iseult, Tristan dies from wounds suffered in battle before Iseult can reach him, despite her belief that they are fated to die together. 
>- She then dies of grief beside his body. 
>- King Mark has them buried in separate tombs, but a green and leafy wild rose springs from Tristan's tomb and falls to root at Iseult's tomb. It is only in death that the lovers are finally united. 


# A real-life story: Abelard and Heloise

## Abelard and Heloise (1)

>- Another medieval love story that seemed to thrive on obstacles was that of Abelard and Heloise.
>- Heloise (pronounced: E-louise)
>- Story told in Wagoner (op cit), p.55.
>- In contrast to the mythical romance of Tristan and Iseult, however, this affair actually happened and the locus was the church, not courtly society. 
>- Abelard was a brilliant and attractive young philosopher in Paris. He quickly achieved fame and popularity as a teacher and skilled debater. 
>- He was retained by [a high-ranking church official] to tutor his young niece, Heloise, who was also very bright and literate.

## Abelard and Heloise (2)

>- The two young people fell in love and their affair became an open scandal. 
>- The poet-professor's love songs to Heloise were sung all over [Paris]. 
>- “Heloise was Isolde, and would in a moment have done what Isolde did.” (Henry Adams)
>- Everyone knew about it -- except, of course, Heloise's uncle.
>- When Heloise became pregnant, Abelard offered to marry her ... but Heloise refused to have him give up his independence. 
>- At length, she yielded, however , and they were married.
>- So as not to compromise Abelard's career in the church, they struggled unsuccessfully to keep the marriage a secret. 


## Abelard and Heloise (3)

>- Finally, at Abelard's request, Heloise “took the veil” and became a nun, and the child was taken by Abelard's sister. 
>- Thinking that Abelard had found a way to [get rid of] the marriage, Heloise's uncle was furious at Abelard's treachery and hired thugs to assault and to castrate him.
>- Following this catastrophe, Abelard went into monastic seclusion and later established a convent where Heloise became the highly respected abbess. 

## Abelard and Heloise (4)

>- The two remained mostly out of touch for ten years. 
>- Communication was restored when Heloise happened upon a letter written by Abelard to a friend in which he recited the history of his calamities. When she then wrote to him she revealed, astonishingly, that her love for Abelard had not diminished at all.


## Abelard and Heloise (5)

>- All their ... long separation and monastic vows had served only to heighten the intensity of her feeling for him -- and this was true despite the fact that she knew he was incapable of sexual response. 
>- Six hundred years later, the French Empress Josephine Bonaparte ( 1763-1814) had their remains moved to a common tomb. 
>- Their tomb has become a tourist attraction for lovers from all over the world.

## Letters - Heloise to Abelard

> “To her lord, or rather, father; to her husband, or rather, brother; from his servant, or rather daughter; his wife, or rather sister: To Abelard from Heloise.
> “You know, beloved, as the whole world knows, how much I have lost in you ... You are the sole cause of my sorrow, and you alone can grant me the grace of consolation. You alone have the power to make me sad, to bring me happiness or comfort.
> *“I have feared to offend you rather than God, and tried to please you more than him”*

## Letters - Heloise to Abelard

> “Solitude is insupportable to the uneasy mind; its troubles increase in the midst of silence, and retirement heightens them. Since I have been shut up in these walls I have done nothing but weep our misfortunes. This cloister has resounded with my cries, and, like a wretch condemned to eternal slavery, I have worn out my days with grief. 
> “Instead of fulfilling God's merciful design towards me I have offended against Him; I have looked upon this sacred refuge as a frightful prison, and have borne with unwillingness the yoke of the Lord. Instead of purifying myself with a life of penitence I have confirmed my condemnation.”

## Letters - Heloise to Abelard

> “Among those who are wedded to God I am wedded to a man; among the heroic supporters of the Cross I am the slave of a human desire; at the head of a religious community I am devoted to Abelard alone. What a monster am I!”

## Letters - Heloise to Abelard

> “Enlighten me, O Lord, for I know not if my despair or Thy grace draws these words from me! I am, I confess, a sinner, but one who, far from weeping for her sins, weeps only for her lover; far from abhorring her crimes, longs only to add to them; and who, with a weakness unbecoming my state, please myself continually with the remembrance of past delights when it is impossible to renew them.”

## Letters - Heloise to Abelard

> “... When you please, anything seems lovely to me, and nothing is ugly when you are by. I am only weak when I am alone and unsupported by you, and therefore it depends on you alone to make me such as you desire.”

## Letters - Heloise to Abelard

> “God knows I never sought anything in you except yourself; I wanted simply you, nothing of yours. I looked for no marriage bond, no marriage portion. … The name of wife may seem more sacred or more binding, but sweeter for me will always be the word mistress, or, if you will permit me, that of concubine or whore.”

## Letters - Heloise to Abelard

> “When prayer should be purest, the obscene imagining of these pleasures so completely overwhelms my poor soul that I yield to their shameful delectation rather than to prayer. I who should tremble at what I have done, sigh over what I have lost.”

## Abelard and Heloise

>- If you look at the love story and the words of Abelard and Heloise, what kind of love comes to mind? Can you try to place it, by finding its Platonic, Aristotelian, Christian, and courtly components?
>     - Platonic: Idealization of the other, to the extent that rightfully belongs only to absolute beauty or to God. Eros includes the love of the body, which is everywhere present in their letters.
>     - Aristotelian: Companionship, common striving towards eudaimonia, even after their separation (through their letters)
>     - Christian: Replacement of God by a human (Abelard for Heloise).

## Abelard and Heloise

>- Courtly elements:
>     - (Hetero-)sexual love is more than bodily love. 
>     - It is in itself noble, and not something to be ashamed of.
>     - Only their status as monk and nun make things difficult. But in the eyes of the world, they don’t feel shame. Heloise is always pointing out how she still loves Abelard and how she refuses to see this as morally bad.
>     - Love can be expressed in letters. It is about courtesy and courtship, but, although it is sexually motivated, it does not need any direct contact between the lovers. A life-long, passionate relationship can be had through letter-writing only!
>     - Their love establishes a one-ness between them (that is mostly secret) and that unites the two of them against the world (that is the cause of their suffering) -  This is a common romantic love motive.

# Characteristics of courtly love

## Characteristics of courtly love (Wagoner)

>- Courtly love is love between nobles.
>- One does not only idolize the lady because she’s a woman, but also because she’s (usually) a high noblewoman.
>- Peasants did not participate in this kind of love, and didn’t share its ideals.
>- As a form of love, it compensated for the too practical aspects of marriage in the middle ages. 
>     - Marriage between nobles was *only* a means of securing land and allies. 
>     - There was no provision for love between husband and wife. 
>     - So courtly love stepped in to address the emotional needs of the people in this situation.

## Characteristics of courtly love (Wagoner)

>- In contrast to marriage, courtly love was free. 
>- Wagoner (53): 
>     - “If at length she responded with a smile or with some more generous gesture, then [the lover] would know that he had won her heart because it was done voluntarily; it was outside the duties and constraints of marriage.”
>     - “The very thing that certifies that it is in fact genuine love is that it is absolutely free.” (We have the same idea today!)

## Love and church

>- The church contributed (unwillingly) to the idea of courtly love: Wagoner (53):
>     - First, against the inclination of the landowning barons to arrange marriages as a means of estate management, the church insisted that marriage had to be voluntary.
>     - This had the ironic effect of making courtly love affairs appear to be more “moral” (because they were indeed voluntary) than most of the contracted marriages of the time. 
>     - Moreover, the church taught that “God is love,” and while the priests and bishops may have meant something quite different by this, it had the effect of giving divine sanction to powerful feelings of desire. How could God not be on the side of lovers? 

## Impossibility

>- One of the main points is that romantic love has to be impossible love.
>- The feeling between the lovers is so strong that it ignores all obstacles and the “impracticality, irrationality, and immorality of their relationship.” (Wagoner)

## Otherworldly innocence (Wagoner)

>- The difficulties and the impossibility of courtly love affairs were taken to suggest that somehow God must be on the side of the lovers.
>- Iseult's handmaiden says to her after the lovers have just narrowly escaped being caught: “God has worked a miracle for you, Iseult, for he is compassionate and will not hurt the innocent in heart.”
>- “Even though the lovers betray the most sacred vows of loyalty - Tristan to his lord and Iseult to her husband - the innocent totality of their love lifts them above ordinary moral constraints.” (Wagoner)
>- Iseult: “For men see this and that outward thing, but God alone the heart, and in the heart alone is crime and the sole judge is God.”

## Otherworldly innocence

>- Neither Tristan nor Iseult will concede the slightest bit of guilt when they are called upon to repent: “I will not say one word of penance for my love.” (Iseult)
>- The same is true of Heloise: “If I am hurting you exceedingly, I am, as you know, exceedingly innocent.” (And other quotes we saw above)
>- Heloise displaces God and replaces him with Abelard. (See quotes above)

## Transcendent value of the beloved

>- We see that, in a way, the value that Plato bestowed on the transcendent form of the Good; and that Christians bestow on the transcendent God; in courtly love becomes value bestowed on the transcendent (because inaccessible) lady.
>- Wagoner: “The ascription of transcendent value to one's beloved ... is the defining relation” of courtly love.

## Obstacles

>- Every obstacle, instead of diminishing the love, increases the lovers’ devotion.
>- Even achieving the union with the beloved becomes secondary. The suffering is what counts.
>- The yearning and passion is how the lover knows that this is “true love.” We will see the same idea in the discussion of modern romantic love (later in this course).

## The absence of fulfilment

>- Wagoner: “Like erotic love, romantic love thrives on what it does not have.”
>- “As with erotic love, success could be the worst thing that could happen.”
>- Romantic stories (for example, in movies) *end* with marriage! Marriage is the *end* of romantic love, because it cannot exist afterwards in its form as unfulfilled passion and suffering.
>- See also what Heloise says about marriage above: she prefers to be Abelard’s whore than his wife! Why?

## The role of reason and passion

>- In ancient philosophy, loving is a rational activity that has the purpose of achieving some good (the vision of the eternal form of Good or a good life with phronesis and eudaimonia).
>- In this way, ancient love is self-interested, rational behaviour.
>- Under the influence of Christianity and its vision of *agape,* courtly love becomes selfless and not calculating.
>- The lovers are not out to get any benefits from their love. They just want to experience its passion.
>- The primacy of passion over reason is a Christian attitude. Religious faith itself is an act of passion that defies reason.

## Self-destruction

>- The lovers’ passion is so powerful that self-annihilation becomes desirable (as the only way of finally being united in death). The same motive that we saw in the Christian mystics (but there it was directed towards God).
>- “Ah, what do you ask? That I come? No. Be still, death waits for us ... What does death matter? You call me, you want me, I come!” (Tristan)
>- And Heloise: “I was powerless to oppose you in anything. I found strength at your command to destroy myself.”

## The role of sexuality^[Wagoner, following W.T.H. Jackson, “The Anatomy of Love”]

>- Courtly love points out a contradiction: “How can love be reconciled with a social order which recognizes sex only in marriage but regards marriage as a contract made without love?”
>- The answer was to move away from the reproductive function of sex (which was a concern of marriage, not love!) towards an idealised, Platonic vision of sexual union.

## Body and soul

>- The body of the beloved is the means by which her or his freedom is given to the lover, but it is at the same time that which limits the expression of that love.
>- It separates as well as unites. (Wagoner)
>- The ingenuity of romantic lovers is thus endlessly tested to find new ways of overcoming their physical separation at the same time that their separateness is enhanced. 

## Tristan’s sword

>- What does the drawn sword signify that lies between Tristan and Iseult in the forest?
>     - Wagoner 61: Remember that Tristan loves Iseult as a lady, a queen, and he is her servant.
>     - But in the woods together she is not a queen and he is not a servant. 
>     - Their love for the moment is somehow weaker. Tristan laments that Iseult was a queen at King Mark's side, “but in this wood she lives a slave, and I waste her youth.” (Bedier 78).
>     - In other words, their love, while it is otherworldly in its aspirations, requires the separations of worldly roles for its intensity.
>     - There has to be a sword between them in order for them to be in love.
>     - In a perverse sort of way, romantic lovers actually need the world in order to continually transcend it and thereby keep their "heavenly love" alive.

## Obstacles and love

>- Meeting obstacles is precisely what makes courtly love possible and what keeps it alive.
>- The lovers don’t want each other so much as they want the *longing towards each other.*
>- In Abelard and Heloise’s story, Heloise’s sexual desire reaches its height when actually Abelard is not available any more as a sexual partner (due to him being both a monk now and castrated). Her longing is both sexual and dependent on the fact that this longing can not be satisfied.

## The role of death

>- Both Tristan/Iseult and Abelard/Heloise are only united in death.
>- Wagoner, 65: 
>     - If romantic love is indeed a passion that surpasses all worldly bounds, then it is not surprising that death, as a release from the world, is its logical outcome.
>     - Denis de Rougemont (1906-1985, author of “Love in the Western World”) argues that the dark secret of romantic love is that it is really in love with death rather than life.
>     - Lovers do not so much want death as defy it. 
>- The message of romantic love is that death, too, can be overcome by love.


# Five criteria for the definition of courtly love (Singer II, 23)

## Sexual love between man and woman is something good

>- This is quite revolutionary.
>- Up to that time, there wasn’t such a notion.
>- Both Plato’s eros and Aristotelian philia are:
>     - mainly homosexual
>     - directed towards a goal that lies outside and beyond the love experience itself!
>- The Christian ideals of love in part follow Plato (union with God), in part they concentrate on agape. Neither involves sexual or erotic love between man and woman.
>- Love between the sexes was for all three sources (Plato, Aristotle, Christianity) a means of reproduction, the basis of marriage, a basis for social stability and child raising. But love between the sexes was never a good in itself.

## Love ennobles both the lover and the beloved

>- Courtly love makes both partners better beings.
>- Being noble in the middle ages meant to be faithful to one’s lord or husband.
>- This, in courtly love, was translated into the devotion of the knight to his lady, and the faithfulness of the lady to her knight.
>- Often the woman is described as a divine being, an embodiment of perfection (quite a Platonic description!)
>- Most often, the lady would have sexual relations with her husband, while staying faithful to her knight. She just loved her knight differently and more truly than her husband, who often was someone she was married to for practical or political reasons.
>- The lady’s behaviour thus was parallel to the knight’s, who also served his lord, and the church, and his lady, all at the same time (in different ways).

## Jealousy

>- The knight might also enjoy other sexual contacts (his wife, mistresses, peasant girls), without confusing such pleasures with the “special ecstasy” (Singer II, 27) he found in the company of the beloved.
>- Since this loving relationship was exclusive, it was the basis of jealousy. A lady could only love a single knight in this special way, so this knight had the right to be jealous of all others who might take his lady’s love away from him!
>- In a way, jealousy was a *proof* for that special relation between a knight and his lady.
>- This is another new characteristic of courtly love that is not in Plato, Aristotle, or Christian love.

##  Sexual love cannot be reduced to a mere bodily function

>- From the ancient world, people perceived the tension between sexuality and high ideals of love.
>- Plato solved the problem by assuming that “real” love is something higher and outside of sex.
>- Aristotle does not need sexual relations at all in his concept of philia.
>- Courtly love, although explicitly sexual, is not reducible to the sexual component.
>- The main idea is to worship love itself, both in its sexual and asexual forms. Sex was a part of that, but the merging of the souls was the higher good.

##  Love is not necessarily related to marriage

>- Courtly love emphasises the rituals of courtship (compare mating rituals of birds or other animals).
>- The partners in courtly love enjoy one another’s company in a context that is more or less sexual, but without being related to an institution such as marriage.
>- Courtly love is an attempt to re-introduce sexual courtesy and individual choice in an area of life that had been controlled by economic, political, and largely impersonal considerations. (Singer II, 29).
>- It provided a safe way of fulfilling the demands of one’s erotic imagination without actually endangering one’s marriage.
>- On the other hand, sometimes the ideals of courtly love are indeed married people (as in Ovid’s story of Orpheus and Eurydice).

##  Love is an intense, passionate relationship

>- Nowadays it seems normal to connect love with passion.
>- But it was not like that for Plato or Aristotle, or even Christian love concepts.
>- Prior to courtly love, man and woman had a relationship that was expected to be practical, not passionate.
>- For Plato and Aristotle, passion was itself not the basis of good conduct. The ancient ideal was one of *ataraxia* or *apatheia* (Stoics, Epicureans).
>     - Ataraxia: to not be troubled by emotions.
>     - Apatheia: to not be driven by passion.
>- Passion enters Christianity with the passionate love of God by the mystics. But there, passion was reserved for the love of God. It would have been sinful to be passionate in the same way towards a human being. Courtly love helped change that perception.

##  Francis Newman on Courtly Love

>- Francis Newman (“The Meaning of Courtly Love,” 1968): 
>- “A love at once illicit^[Illicit: not allowed by laws or rules, or strongly disapproved of by society.] and morally elevating, passionate and disciplined, humiliating and exalting^[To exalt: To raise something or someone to a higher level.], human and transcendent.”
>- This characterization is (on the surface) contradictory. Try to explain it so that it makes sense!


# Platonism in courtly love

## Platonism in courtly love

>- All Platonists claim that the function of human love is to ultimately transcend the human and unite with something beyond the individual (ultimate beauty, God etc)
>- The troubadours are unconcerned about anything outside of the beloved person.
>- Their beloved is indeed more than just another beautiful form in the external world: she is for them the supreme instance of beauty and that is why they love her. (Singer II, 47)
>- The relation [the troubadour] has to his lady is modelled on the relation humans should have towards God, but the object of that relation has shifted and become fully human.

## Idealization of the beloved (Singer II, 49)

>- The idealization of the beloved is a Platonic element in courtly love.
>- The troubadour loves his lady *because* she is perfect.
>- So perfection here is the reason for the love. This is clearly an appraisal kind of love.
>- If the beloved were not perfect, she would not have the troubadour’s love.
>- William IX, the first troubadour: “For you are whiter than ivory. This is why I love no other”
>- Pons de Capdeuil: “You are so worthy, courteous in true speaking, frank and gentle, gay with humility, beautiful and pleasing, that ... you do not lack any good quality one could wish in a lady.”


## Idealization of love itself (Singer II, 50)

>- The troubadours also idealized love itself, their own desire, their own condition as lovers. (Singer II, 50)
>- Bernard de Ventadour: “No man is of value without love. ... By nothing is man made more excellent than by love and the service of women, for thence arises delight and song and all that pertains to excellence.” And: “I crave so noble a love that my longing is already a gain.”

## The revolutionary and heretic character of courtly love (Singer II, 51)

>- Troubadour love is revolutionary and heretical.
>- It refuses to define itself as subordinate to God’s love.
>- It encourages self-sufficiency among human beings, and makes them independent of God.
>- This is what particularly disturbed the church.
>     - Before there had been love stories that were focussed on sexuality and desire, but that desire had never been of the spiritual quality that courtly love had, and that threatened to compete with the desire for God itself!
>     - (Compare Heloise’s letters to Abelard).

## The love of nature (leading to romanticism) (Singer II, 54)

>- Since romantic love cannot be satisfied in this world, it needs another outlet. Troubadours used poetry as a means to channel the creative powers that love inspired.
>- With the troubadours begins the poetry of nature.
>- In their writings we find “sensitivity to landscapes, the love of nature, ... which the Romantics [much later] revived.” (Singer)
>- The troubadours were among the first to see the things of the world as objects to be enjoyed in the imagination.

## The love of nature (leading to romanticism) (Singer II, 54)

>- Bernard de Ventadour: “When tender leafage doth appear, When vernal meads grow gay with flowers, And aye with singing loud and clear, The nightingale fulfills the hours, I joy in him, and joy in every flower, and in myself, and in my lady more.”
>- Later this will inspire St. Francis of Assisi (1182–1226), who transformed courtly love back to a love of all the creatures of God. He spoke with birds, called the wind, the fire and the sun his brothers and sisters, and generally displayed an unlimited love towards all humans, animals, and natural phenomena.
>- Love of nature much later becomes the basis of European Romanticism.

## The End. Thank you for your attention!

Questions?

<!--

%% Local Variables: ***
%% mode: markdown ***
%% mode: visual-line ***
%% mode: cua ***
%% mode: flyspell ***
%% End: ***

-->
