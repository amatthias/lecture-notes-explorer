---
title:  "Love and Sexuality: 10. Romantic love"
author: Andreas Matthias, Lingnan University
date: October 7, 2019
...

# From courtly to romantic love
 
## Towards romantic love: Romeo and Juliet

Simplified plot:

>- Romeo and Juliet, children of enemy families, fall in love with each other.
>- They are secretly married.
>- In a series of fights and duels, Romeo is forced to kill a cousin of Juliet.
>- As a consequence, he is exiled from his city, Verona.
>- Juliet’s parents decide to marry her off to someone else.
>- In order to avoid that, Juliet drinks a “poison” that will *appear* to kill her for 42 hours. Then she will wake up again and be fine.
>- She sends a messenger to Romeo to tell him about that, but the messenger doesn’t reach him.
>- Romeo hears of Juliet’s death (which he believes to be genuine) and commits suicide himself at her grave.
>- Juliet awakens to find Romeo dead, so she kills herself, this time for real.

## Differences between Romeo/Juliet and courtly love

>- Q: How is this story different from courtly love?
>     - The love is more equal, mutual, less one-sided and idealizing only Juliet.
>     - It is a love that seeks sexual fulfilment, as opposed to the distance in courtly love.
>     - Neither Romeo nor Juliet tolerate that the other might get married to another person. For a courtly couple, this would not be such a great problem.
>     - But observe that in the Tristan/Iseult story they also don’t want to give up their sexual love and accept Iseult’s marriage to the king!
>     - So there is an ambiguity here about the exact role of sexuality and abstinence from it in courtly love!
>     - As opposed to courtly love, romantic love is clearly sexual with the intention of the lovers to unite in every way.

## William Shakespeare: Sonnet 18

> Shall I compare thee to a summer’s day?   
  Thou art more lovely and more temperate:   
  Rough winds do shake the darling buds of May,   
  And summer’s lease hath all too short a date:   
  Sometime too hot the eye of heaven shines,   
  And often is his gold complexion dimm’d;   
  And every fair from fair sometime declines,   
  By chance or nature’s changing course untrimm’d;  
  But thy eternal summer shall not fade   
  Nor lose possession of that fair thou owest;   
  Nor shall Death brag thou wander’st in his shade,   
  When in eternal lines to time thou growest:   
  So long as men can breathe or eyes can see,   
  So long lives this and this gives life to thee.

## Modern English

> Shall I compare you to a summer’s day?  
  You are more lovely and more moderate:  
  Harsh winds disturb the delicate buds of May,   
  and summer doesn’t last long enough.   
  Sometimes the sun is too hot,   
  and its golden face is often dimmed by clouds.   
  All beautiful things eventually become less beautiful,  
  either by the experiences of life or by the passing of time.  
  But your eternal beauty won’t fade,  
  nor lose any of its quality.   
  And you will never die,  
  as you will live on in my enduring poetry.   
  As long as there are people still alive to read poems,  
  this sonnet will live, and you will live in it.^[From: http://www.nosweatshakespeare.com/sonnets/18.]

## Observations

>- Observe:
>     - The use of nature images
>     - Comparison of the beloved with the beauty of nature
>     - Overcoming death (the same motive as in Tristan/Iseult and Abelard/Heloise stories!). Interestingly, here the death is overcome through poetry, which is a Platonic concept.


# Examples of romanticism in literature and art

## Novalis (1772-1801)

> ... through the cloud I saw the glorified face of my beloved. In her eyes eternity reposed -- I laid hold of her hands, and the tears became a sparkling bond that could not be broken. Into the distance swept by, like a tempest, thousands of years. On her neck I welcomed the new life with ecstatic tears. It was the first, the only dream -- and just since then I have held fast an eternal, unchangeable faith in the heaven of the Night, and its Light, the Beloved.

## A typical representative: Love in “Wuthering Heights” (Emily Brontë, 1847)

> “My love for Linton is like the foliage in the woods: time will change it, I'm well aware, as winter changes the trees. My love for Heathcliff resembles the eternal rocks beneath: a source of little visible delight, but necessary. Nelly, I am Healthcliff! He's always, always in my mind: not as a pleasure, any more than I am always a pleasure to myself, but as my own being.”

## A typical representative: Love in “Wuthering Heights” (Emily Brontë, 1847)

> “Be with me always - take any form - drive me mad! only do not leave me in this abyss, where I cannot find you! Oh, God! it is unutterable! I can not live without my life! I can not live without my soul!”

. . . 

>- You see the desire for union and also how the speaker identifies with the beloved as her “own being.”
>- But, like in courtly romances, the lovers here can be united only after death.

##  Schelling’s Naturphilosophie (Singer II, 387)

>- The whole universe is created as an act of God’s love.
>- By loving, human beings themselves can experience unity with the universe.
>- Love is, therefore, the force that keeps the universe together.

##  Shelley (Singer II, 412): Essay on love^[Percy Bysshe Shelley (1792 – 8 July 1822)]

> This is Love. This is the bond and the sanction [=permission] which connects not only man with man, but with every thing which exists. ... We dimly see within our intellectual nature a miniature as it were of our entire self, yet deprived of all that we condemn or despise, the ideal prototype of every thing excellent and lovely that we are capable of conceiving as belonging to the nature of man. ... Hence in solitude, or in that deserted state when we are surrounded by human beings, and yet they sympathize not with us, we love the flowers, the grass, the waters, and the sky. In the motion of the very leaves of spring, in the blue air, there is then found a secret correspondence with our heart.

## Shelley, observations

>- The love of nature is a kind of our love of our ideal selves.
>- Our ideal selves: “We dimly see within our intellectual nature a miniature as it were of our entire self, yet deprived of all that we condemn or despise, the ideal prototype of every thing excellent and lovely that we are capable of conceiving as belonging to the nature of man.” -- This is clearly Platonic, the Form of man.
>- In nature we find this perfection that we cannot easily find in human company: “In that deserted state when we are surrounded by human beings, and yet they sympathize not with us.”
>- Love connects us to everything that exists. It is a love of the whole universe, not a personal love.
>- In this way, the romantic ideal goes back to Plato in a much stronger way than courtly love did.

#  The Romantics
 
##  Feeling rather than reason (Singer II, 285)

>- The Romantic movement (peak around 1800-1850) emphasised the importance of emotion over rationality.
>- Coleridge (1772-1834): “Deep thinking is attainable only by a man of deep feeling.”
>- William Blake (1757-1827): Human imagination is the faculty that reveals God’s own creativity. It is the means by which the world becomes a unity instead of a system of unrelated objects. Through the imagination ... we participate in God’s being as the creator of such unity. We thereby indentify with him, with nature, and with all men and women. (Singer II, 287) 

##  Oneness rather than dualism (Singer II, 288)

>- Blending and merging (Singer II, 290/291) instead of wedding!
>- August Wilhelm Schlegel (1767-1845): Romantic poetry is literature that perceives the cosmos as “all in all at one and the same time.”
>- Novalis idealizes the poet who “blends himself with all the creatures of nature, one might say: feels himself into them.”
>- Shelley in a poem to Emilia Viviani: “I am not thine. I am part of thee!”
>- Bronte, Wuthering heights: “He’s more myself than I am. Whatever our souls are made of, his and mine are the same.”
>- This returns to Platonic merging, but now it is not the absolute beauty of Plato, not the God of Christians, not the lady of the troubadours, but all nature that is the object of merging.

##  Is romantic love appraisal or bestowal love?

>- Unclear.
>- On the one hand, it is essentially Platonic, meaning that it responds to and idealizes properties of the beloved (beauty, kindness etc). This would make it appraisal love.
>- But: Wuthering Heights: “Nelly, I am Healthcliff! He's always, always in my mind: not as a pleasure, any more than I am always a pleasure to myself, but as my own being.” -- Here she does not appreciate particular qualities of the beloved. He is not even “a pleasure” to her.
>- Also, when romantic love is extended to all of nature, we cannot really speak of an appraisal.
>- If I love everyone because they are part of God’s nature, then my love has essentially become unconditional *agape*.

## Romance and revolt (1)

>- Love qua bestowal is inherently revolutionary. 
>- We can see that Catherine, in Wuthering Heights, decides to love someone who is totally unsuitable by the usual social standards.
>- Romeo and Juliet are drawn to each other although they are unsuitable partners in the eyes of their society.
>- The same with Abelard and Heloise.
>- This is a difference to courtly love, which was, for the most part, well-behaved and within the limits of the social conventions of its time.

## Romance and revolt (2)

>- More modern examples: 
>     - Rose and Jack in “Titanic”
>     - The homosexual love in Brokeback Mountain
>     - Beauty and the Beast
>     - Maid in Manhattan
>     - Pretty Woman
>     - Notting Hill
>     - (sorry if the list is a bit dated -- I’m 50)

## Romance and revolt (Singer)

>- While appraisal orients itself around given systems of values, bestowal negates or ignores these given values.
>- Instead, it confers private value that is distinguished by not being identical (or even compatible) with given, accepted values. 
>- Proper love is therefore romantic, in that its bestowal creates private value, possibly opposed to societal valuing norms. 
>- And by being opposed in this way, love becomes conscious of itself and affirms itself.

## The End. Thank you for your attention!

Questions?

<!--

%% Local Variables: ***
%% mode: markdown ***
%% mode: visual-line ***
%% mode: cua ***
%% mode: flyspell ***
%% End: ***

-->
