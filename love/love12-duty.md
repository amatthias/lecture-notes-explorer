---
title:  "Love and Sexuality: 12. Is there a duty to love?"
author: Andreas Matthias, Lingnan University
date: September 1, 2019
...

# Kant’s view on duties and emotions

## Sources

- Arroyo, C. (2016). Kant on the Emotion of Love. European Journal of Philosophy, 24(3), 580-606.
- Driver, Julia. "Love and Duty." Philosophic Exchange 44.1 (2014): 1.
- Liao, S. Matthew. "The idea of a duty to love." The Journal of Value Inquiry 40.1 (2006): 1-22.


## Kant's good motivation

### Kant:
A good motivation: “To do what is right *for no other reason* than that it is right.”

**Note:** for no *other* reason (not: for no reason!)

## Motivation and inclination

>- What does “inclination” mean? How is it related to morality?
>- “Inclination” means that I like to do something. For example, I have an inclination to do gardening, to sing, to help my friends, to go to the cinema and so on.
>- What happens if the moral duty coincides with what I like to do? Can I then still distinguish the motives?
>- Say, I like to sing, and I fulfill my moral duty by singing for free at charity concerts. Is this morally praiseworthy?
>- Now say somebody else (a big star) hates singing for free, but also does it for charity. Is he more praiseworthy than me?
>- Kant: probably yes, because he acts against his inclination. He does what is right *for no other reason* than that it is right.

## Benefiting friends

>- For Kant, acting **against inclination is praiseworthy,** because if I have an inclination to act in a particular way, then my inclination is a *reason* to act in this way, and so I’m not acting without “any other reason than that my action is morally right.”
>- If two of your classmates ask you to copy your lecture notes from the previous session, and one is your friend, while the other is someone you hate. Which one would it be more praiseworthy to let copy your notes? 
>- (The one you hate!)


## Central ideas of Kant's system

>- **The morally right action is the rational action.**
>- Acting against reason means acting in a morally wrong way.
>- We can use our reason to find out what the morally right action is.

## Kant: Overview (graphics)
![](graphics/kant-01.png)

## Notes on Kant (1)
*Numbers refer to the previous picture!*

1. Good motivation is necessary but not sufficient for morally right action.
2. Categorical: must be obeyed, unconditional; Imperative: order, command.
3. Maxim: the principle behind an action.
4. You simply can not will a maxim that is immoral. Doing so would lead to a contradiction (for perfect duties), or to a world you don't want (imperfect duties). Morality is dictated by reason.

## Notes on Kant (2)

5. "Humanity:" All rational and autonomous beings (for example including rational and autonomous aliens, but not animals!)
6. You have to respect yourself in the same way as you respect others (sacrifice, suicide!).
7. You *can* treat others as means, but not *only* as means!
8. At the same time you treat them as means, you must *also* treat them as ends.
9. "End:" the target of an action. The ultimate reason for doing something.

## Derived and original value (worth)

>- We must distinguish things that have relative and derived value (like diamond rings, or cinema tickets) from things that have intrinsic worth, or dignity (like human beings).
>- Things that have dignity cannot be traded in the same way as things that have mere value.
>- I can trade a bicycle for a surfboard, but I can not trade one child for another.
>- Human beings are special.
>- Out of all things in nature, only they are able to be *autonomous,* that is, to make decisions for themselves. This is what gives them their intrinsic worth.


## Love as a “pathological” feeling

>- What would Kant say about romantic or emotional love? Is it a good thing?
>- No, because such love is “pathological”: it is based on “pathos,” emotion.
>- Why is this bad?
>- First, emotions can be irrational and deceiving. We might not feel love for those we should love (for example, family members). Or we might love those that don’t deserve our love.
>- Second, every action that we perform in order to satisfy our own emotions is, in a way, selfish.


## Duty out of inclination is selfish and unreliable

Kant:

> “If we now, on the other hand, take well-doing (Wohltätigkeit) from love, and consider a man who loves from inclination, we find that such a man has a need of other folk, to whom he can show his kindness. He is not content if he does not find people to whom he can do good. A loving heart has an immediate pleasure and satisfaction in well-doing, and finds more pleasure in that than in its own enjoyment.”

## Duty and inclination:

Vigilantius’ notes of Kant’s lectures on the Metaphysics of Morals:

> “No duty, and hence, not the duty of love either, can be founded on inclination ... This is the basis of love in all those cases where, in exercising the act of love, we have our own welfare in view ... We call this love from inclination ‘kindness’ ..., when it has the intention of laying upon the other an obligation towards us, and is thus coupled with an interest.”


## Love that delights and love that wishes well

>- Kant distinguishes:
>     - The pathological love that wishes well (Liebe des Wohlwollens); and
>     - The love that delights (Liebe des Wohlgefallens).
>- “Love that delights” is appraisal love (or, in Aristotle’s terms, friendship for pleasure).
>- Love that wishes well (beneficence) is not as valuable when it is based on delight love.
>- Both are “pathological” forms of love, expressions of love that are not guided by rational moral principles but by one’s own self-love (i.e., a concern for one’s own good). (Arroyo p.588)

## Love cannot be commanded

>- For Kant, both kinds of love cannot be commanded:
>     - The love that delights is based on appraisal of qualities that cause us to be delighted. In the absence of such qualities, there is no way to command that sentiment to come into existence.
>     - The love that wishes well is dependent on the love that delights. It is well-wishing towards someone who delights us. Therefore we cannot love at will, but only if we have an urge to love.


## Beneficence can be commanded (1)

Kant, The Metaphysics of Morals (transl. Mary J. Gregor):

> “Beneficence is a duty. If someone practices it often and succeeds in realizing his beneficent intention, he eventually comes actually to love the person he has helped. So the saying ‘you ought to love your neighbor as yourself’ does not mean that you ought immediately (first) to love him and (afterwards) by means of this love do good to him. It means, rather, do good to your fellow human beings, and your beneficence will produce love of them in you (as an aptitude of the inclination to beneficence in general).”


## Beneficence can be commanded

> “Love as an inclination cannot be commanded; but beneficence from duty, when no inclination impels us and even when a natural and unconquerable aversion opposes such beneficence, is practical, and not pathological, love. Such love resides in the will and not in the propensities of feeling, in principles of action and not in tender sympathy; and only this practical love can be commanded.” (Kant, Grounding of the Metaphysics of Morals, transl. James Ellington)


## Problems with Kant’s concept

>- Remember that, according to Kant, we have to treat all humans as ends and not as means only. Human beings have an unlimited worth (dignity) and not merely a price (financial value).
>- But if the core of love is the appreciation of someone’s value as a person, and since we all have value as persons, we all ought to love each other. (Driver p.2)


## The un-loving love of Kant

>- Do you see any problem with Kant’s account of love?
>- The problem is that “practical love” (beneficence from duty) does not account for the special relationships that we call love.
>- If I have to treat everyone equally with respect and as a being of unlimited value, then what is special in the love to my wife, as opposed to my attitude towards any random person on the street?
>- Obviously, Kant misses the point of romantic love here (or even of love in general).


# The no-reasons view

## No reasons for love

>- Some authors have proposed that we can never justify love with reasons (Frankfurt, Smuts). We will talk about this thesis in a later session.
>- A similar view is “bestowal” love. We bestow love on someone not because of any appraisal of his or her qualities. Why then? We cannot provide a reason (without returning to an appraisal view).
>- The no-reasons view is different from the bestowal view in that the bestowal view would see the bestowal itself as a reason to love the other person after that love has been bestowed.
>- The no-reasons view would never assume that there are *any* rational and justifiable reasons for love.


## No-reasons and hate

>- A problem with the no-reasons view is the asymmetry between love and hate.
>- Since we have no reasons to love, what about reasons to hate?
>- It seems that we generally believe that there are good reasons to hate people, or that we can distinguish between justifiable hate and irrational, not justifiable hate.
>- So why is it that love does not have reasons but hate has?
>- Driver (p.5): “Advocates of the No Reasons View can simply hold that hate is just unlike love in this respect -- that one needs reasons to justify hatred, even though one doesn’t need them ... in the case of love. But a view that preserves a connection in terms of justification is to be preferred...”


## The Relationship view

>- The relationship view, love is a kind of relationship between two people that involves a *central commitment.*
>- The relationship *creates* particular demands that the two people have to satisfy.
>- Reasons for love can be found in the relationship itself.
>- The relationship view does not make it clear why we *fall* in love, but it does explain why we *stay* in love. (Driver p.6)
>- What does this view remind you of?
>- Thomas Aquinas: “Office” of love. An “office” for Aquinas is a defining relationship that prescribes particular kinds of behaviours, including love.


# Loving Harry Lime

## The trouble with Harry

>- In Graham Greene’s novel “The Third Man,” the main character is Harry Lime.
>- He is someone who, during a war, was selling diluted black-market antibiotics that caused the deaths of many children.
>- His lover, Anna, did not know this when she fell in love with him.
>- How should she react now that she suddenly learns that the man she loves is, in reality, an immoral criminal?
>- More generally:
>     - Is it permissible to fall in love with Harry Lime?
>     - Is it permissible to stay in love with Harry Lime after one knows the truth?
>     - Is anything wrong about loving Harry Lime, and what exactly?


## What should Anna do?

>- What should Anna do?
>- Options: 
>     - It is right to love Harry Lime and to act upon that love.
>     - It is okay to (privately) love Harry Lime, but not to act upon that love (that is, it is not okay to help him escape or to defend him).
>     - It is not okay to even privately love him. Harry Lime must be despised, not loved.
>- What if Harry Lime was one’s father or son? Would this change the situation? Do we have to love our family even if they are acting immorally?

## Confucius on family ties

>- What would Confucius say?
>- “One has stronger moral obligations toward, and should have stronger emotional attachment to, those who are bound to oneself by community, friendship, and especially kinship.” (van Norden)
>- Analects 13.18: “The Governor of She in conversation with Confucius said, ‘In our village there is someone called [Upright] Person.  When his father stole a sheep, he reported him to the authorities.’  Confucius replied:  ‘Those who are [upright] in my village conduct themselves differently.  A father covers for his son, and a son covers for his father. [Uprightness consists of this].’”
>- So, Confucius would say that fathers should help their sons cover up crimes. But does this also apply to lovers? To friends? To acquaintances? Does it cover Anna?


## Does Anna have a duty to not love Harry?

>- Can one have a duty one is unable to fulfil?
>- Can Anna have a duty to not love Harry any more if she cannot control her love for him?
>- Possible answer: If I say that Anna has a duty to not love Harry, then I must make sure that she’s able to fulfil that duty. I must make sure that she *can* fall out of love with him.
>     - An appraisal theory would allow for that. Anna appraises the (newly discovered) quality of Harry to be a murderer and decides that this terminates her love for him.
>     - But what if Anna still has feelings for Harry? Is it possible to manipulate one’s feelings based on one’s reason? Can Anna convince herself not to love Harry?

## Does Anna have a duty to not love Harry?

>- Another possible answer: “Even though [Anna] is not blameworthy for loving Lime, she still has a *duty* to not love him.” (Driver, p.7)
>- A third possible answer: “The duty to not love does not involve a duty to eradicate one’s feelings for someone, but will involve a duty to *try* to eradicate those feelings, and subvert any dispositions that come along with those feelings -- dispositions to seek the well-being of the loved one when that conflicts with justice, and dispositions to make excuses, and ignore the evidence.” (Driver, emphasis added)


## Can we block our own emotions?

>- Can we manipulate and block our own emotions?
>- Examples:
>     - A boxer before a fight?
>     - Someone who wants to feel more religious might go to church in order to achieve that.
>     - Animal rights campaigners might show us pictures of animal suffering in order to convince us to not eat meat. What if we show these pictures to ourselves? Would this work to convince us to not eat meat?
>     - Smokers who want to stop smoking might watch movies about the health effects of smoking in order to manipulate their own emotions towards smoking.


## The asymmetry of a duty to love

Driver:

> “The thesis of this paper is that there is an important asymmetry between a duty to love and a duty to not love: there is no duty to love as a fitting response to someone’s very good qualities, but there is a duty to not love as a fitting response to someone’s very bad qualities.” (p.1)

## The reason for the asymmetry

Two parts of love:

1. the emotional part; and 
2. the evaluative commitment part.

. . . 

“One cannot directly, or ‘at will,’ control an emotional response, but one can undermine any commitment one would normally have under the circumstances.” (Driver p.2)


# Can love be commanded?


## Commandability

>- The main problem with a “duty to love” is that one must be able to fulfil one’s duties, at least in principle.
>- “Ought implies can.”
>- We would perhaps agree that a hermit, who finds a child in a forest, ought to love that child in order to give it a chance to develop normally; although the hermit has not chosen to have this child. Still, it seems that we can have a duty to love even when we have not willingly committed to doing so. (Liao, 16)


## The attitudes of love

>- One could argue that love is misunderstood. It is not an emotion, but a collection of *attitudes.*
>- Attitudes are things like:
>     - Valuing the person we love for the person’s sake.
>     - Wanting to promote another person’s well-being for the person’s sake.
>     - Wanting to be with the person.
>- Do you see how this solves the problem?
>- Although we cannot influence our emotions, we *can* control our attitudes.
>- Therefore, we *can* control our love; and therefore, we are responsible for it (and can fulfil it as a duty). (Liao)

## Is love more than attitudes?

>- But love does not seem to be only attitudes.
>- We can have strong attitudes to help others or be with them (for example, our friends or younger colleagues) without being in love with them in a romantic sense.
>- “Indeed, a friendly colleague may have all the attitudes typically associated with love such as valuing the other person and wanting to promote the other person’s well-being for the other person’s sake. But it does not follow that the colleague loves the other person.” (Liao)

# Can we control our emotions?

## Internal control and reasons for emotions

>- How can we control our emotions?
>- Internal and external control.
>- Ways of internal control:

. . . 

>1. Creating and visualising reasons
>2. Reflecting on reasons
>3. Cultivating emotions (like Aristotle’s habits)

## 1. Creating and visualising reasons

>- “For example, let us suppose that we are not in a good mood on a particular day and on the particular day, we by chance are also invited to attend a close friend’s wedding.”
>- “Knowing that our close friend would not want us to be in a bad mood and would instead want us to be joyful on her wedding day, we might tell ourselves that because it is a special occasion, we should not be in a bad mood and that we should instead be joyful.”
>- “In giving ourselves a reason to be joyful and not to be in a bad mood, there is a good chance that we would not be in a bad mood, and would instead be joyful.” (Liao 5)


## 2. Reflecting on reasons

>- “Another method of internal control calls on us to reflect on the reasons why we tend to experience particular emotions in particular circumstances or toward particular persons.”
>- “Through reflecting on these reasons, we might then decide to continue to have particular emotions, if the emotions are supported by good reasons, or to discontinue to have particular emotions, if the emotions are not supported by good reasons.”


## 3. Cultivating emotions

>- “We can also cultivate our emotional capacities such that we would be more likely to have particular emotions in appropriate circumstances.”
>- “As Aristotle says, cultivation involves habituation as well as reflection.”
>- “One strategy for cultivating certain emotions is to behave as if we have particular emotions. After some time and effort, it is likely that we would cultivate the capacities for these emotions.”
>- “For example, if we wish to cultivate our capacity for joy, we might begin by behaving as if we are joyful. We might smile, whistle, sing ... Through engaging in these forms of behavior repeatedly over time, it is likely that we would cultivate the capacity for joy.”
>- “Indeed,  observes that through enacting the behavior associated with religious rituals, we seem to increase our capacity for religious feelings.” (Liao 7)

## External control

>- External control uses the outside world as a support in manipulating our emotions.
>- “Repeatedly placing ourselves in situations in which we know that we would probably experience particular emotions.”
>- “Using our previous examples, let us suppose that we know that attending church services and visiting homeless shelters often elicit emotions of piety and compassion in us. To cultivate our capacities for such emotions, we might repeatedly visit such places. In doing so, there is a good chance that we would cultivate these emotional capacities.”

## Internal and external control

>- What do you think of these methods of emotion control? 
>- Are they likely to be effective?
>- Are they likely to help Anna deal with her love of Harry Lime?

## The difficulty of cultivating emotions

>- One might argue that cultivating emotions in these ways is very difficult.
>- Is this a good argument for avoiding to cultivate the emotions and for accepting that we cannot have a duty to love?
>- “This suggests that to cultivate our emotional capacities, we may be required to evaluate critically some of our fundamental values, and to alter some of our ingrained character traits such as becoming more reflective than superficial, or becoming more altruistic than self-interested.”
>- “Such cultivation can be difficult. However, the cultivation of physical abilities such as learning how to play the piano or to sail a boat is often equally difficult. Yet people acquire these capacities nevertheless.”
>- Therefore, it is a too strong conclusion to say that love is *never* commandable.

## Is romantic love special? (1)

>- But even if we agree that some kinds of love might be commandable (for example, parental love), we might disagree that romantic love is.
>- This might be because romantic love involves passion, which is not under the control of our reason.

## Is romantic love special? (2)

>- “The idea that romantic love is not commandable may be a relic of the German Romanticism of the eighteenth and nineteen centuries, according to which there is a sharp boundary between reasons and emotions.” (Liao)
>- “It is therefore possible that we have inherited this possible bias from German romanticism, and that we have continued to perpetuate this idea in our media, literature, and music.”
>- “Indeed, there are alternative views of emotions, according to which there is not such a sharp boundary between reasons and emotions.”
>- An Aristotelian approach would not emphasise passion and uncontrollable emotions so much, but would be more accessible to reason. (Liao, 20)

## Romantic love is also rational

>- Is romantic love really completely irrational?
>- An argument that even romantic and passionate love is rational to some extent can help reduce the impression that romantic love is not controllable by reason at all.
>- We don’t love randomly. We have particular expectations of our beloved.
>- “For example, our expectations might have been built up from our childhood from when our parents and teachers read us stories about brave knights and beautiful princesses. They might have further been reinforced through our peers, society, and the media.”

## Romantic love is also rational

>- “Given this, when we meet a certain individual, we already have a fairly extensive checklist for deciding whether that individual is the right person for us. It is not surprising that we fall in love when someone seems to fit all the criteria, until we find out later that the person is not as perfect as we thought.”
>- “We fail to fall in love when someone lacks certain important criteria on our list, even though that person may seem to others perfect for us in all respects. ...”
>- “Finally, we fall out of love either when we realize that the other person does not fit our expectations or our expectations have changed as a result of certain life events such as midlife crisis.” (Liao 22)

## Romantic love is also rational

>- If romantic love is rational, then it’s also commandable.
>- “But, if we do have these expectations, then romantic love should be commandable, provided that there are good reasons to believe that we should revise our expectations.” (Liao 22)
>- As a conclusion, love, including romantic love, seems to be sufficiently controllable by rationality as to be commandable. 
>- Therefore, there *can* rationally be a duty to love (or not to love) in particular circumstances.


## The End. Thank you for your attention!

Questions?

<!--

%% Local Variables: ***
%% mode: markdown ***
%% mode: visual-line ***
%% mode: cua ***
%% mode: flyspell ***
%% End: ***

-->
