
---
title:  "AI and Society: 12. Final exam information"
author: Andreas Matthias, Lingnan University
date: April 29, 2019
...

# Session 25: Final exam

## The answer booklet

>- WRITE YOUR NAME on the back flap in English letters (as on your student ID card).
>- Write your student ID and your seat number on the front cover.
>- DON’T write anything else onto the front cover.
>- Leave the front cover fields for question number, marks, programme/stream, subject and everything else empty.

## How to write your answers

>- DON’T WASTE SPACE!
>- Write all your answers one after the other. Don’t leave half pages empty.
>- Write multiple answers on the same page, if they fit.
>- I don’t want you to use additional paper! The answer booklet is more than enough for this exam.

## Exam structure

>- 4 short answer questions (about 2 paragraphs each), each 10 marks = 40%
>- 2 out of 3 essays (about 1.5-2 pages each), each 30 marks = 60%
>- Longer answers don’t give you a better grade.

## Content (1)

>- Answer the questions precisely and fully.
>- Check if you have answered all the parts of a question.
>- If you don’t know the answer, don’t write something unrelated. You get points ONLY for the precise answer, not for unrelated stuff.
>- SKIP INTRODUCTIONS to your essays (“It is a long-discussed problem in philosophy whether we should or should not ...”).
>- Get directly to the core of your answer!
>- WRITE CLEARLY. If I cannot read something, you get no points for it.

## Content (2)

>- Keep track of the time. Reserve some time at the end to read what you wrote and to compare your answer to the question.
>- The point is *not* to answer the question in a general way, but to *show me that you know the theory* and how to apply it.
>- Therefore, show off your knowledge of the material. 
>     - If you can quote something, do! 
>     - If there are arguments in the lecture notes about the topic, I want to see them in your exam. 
>     - Remember and mention the thinkers’ names. 
>     - And so on.

## Coming to the venue

>- Pack your papers away BEFORE YOU COME IN.
>- If you have your notes open on the desk, you are cheating.
>- Phones, notes, books, calculators are all cheating.
>- Put everything away (in a bag under your seat). You are allowed to have only a pencil case with the usual writing equipment.
>- TURN YOUR PHONE OFF while you are still outside. If your phone rings, or I see you play with it, you will be asked to leave.
>- Only paper dictionaries for general use (not topic-specific!)
>- No electronic dictionaries allowed!

## Leaving the venue

>- When you finish, raise your hand and wave to get my attention.
>- Don’t give your paper to anyone else!
>- There will be dozens of people there, but they are teachers of different courses. 
>- DON’T GIVE YOUR PAPER TO ANYONE BUT ME.
>- DON’T JUST LEAVE when you’re done. 
>- Call me and WAIT until I’ve come and checked your paper. This is for your own good. I will check whether your name is written correctly on the flap. Otherwise your exam might get lost.

## Questions?

Any questions?

