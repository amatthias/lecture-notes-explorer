---
title:  "Love and Sexuality: 24. Can one love a machine?"
author: Andreas Matthias, Lingnan University
date: November 24, 2019
...

# Affective computing and machine emotions

## Sources (1)

- Danaher, J., Earp, B. D., & Sandberg, A. (forthcoming). Should we campaign against sex robots? In J. Danaher & N. McArthur (Eds.) Robot Sex: Social and Ethical Implications [working title]. Cambridge, MA: MIT Press. Draft available online ahead of print at:
https://www.academia.edu/25063138/Should_we_campaign_against_sex_robots.
- Levy, D., & Loebner, H. (2007, April). Robot prostitutes as alternatives to human sex workers. In IEEE international conference on robotics and automation, Rome.(http://www. roboethics. org/icra2007/contributions/LEVY% 20Robot% 20Prostitutes% 20as% 20Alternatives% 20to% 20Human% 20Sex% 20Workers. pdf). Accessed (Vol. 14).
- Nitsch, V., & Popp, M. (2014). Emotions in robot psychology. Biological cybernetics, 108(5), 621-629.

## Sources (2)

- Picard, R. W. (2004). Toward Machines with Emotional Intelligence. In ICINCO (Invited Speakers) (pp. 29-30).
- Picard, R. W. (1995). Affective Computing. M.I.T Media Laboratory Perceptual Computing Section Technical Report No. 321. Revised November 26, 1995. Available online.
- Sharkey, Wynsberghe, Robbins, Hancock (2017). Our sexual future with robots. A Foundation for Responsible Robotics Consultation Report.
- Sullins, J. P. (2012). Robots, love, and sex: The ethics of building a love machine. IEEE transactions on affective computing, 3(4), 398-409.
- Turkle, S. (2005). Relational artifacts/Children/Elders: the complexities of cybercompanions. Presented at: Cognitive Science Society, July 25-26, Stresa, Italy. Available online.

# Emotions

## What is an emotion? (1)

>- Before we can ask whether robots can have emotions, we need to understand what emotions are. Do you remember some of the theories we talked about?
>- One theory: William James (1884): An emotion is the subjective feeling of a physiological change in the body.
>     - This means that the primary thing that identifies and constitutes an emotion is the physiological change (blood pressure, heartbeat, breathing).
>     - The subjective feeling of that change is secondary.
>     - An emotion is not primarily a mental state but a physiological (body) state that feels in a particular way.

## What is an emotion? (2)

>- Another way to define an emotion (more in line with AI) would be a functionalist way.
>- Functionalism (in the philosophy of mind) defines a mental state through its role or function in relation to other mental states.
>     - An emotion is defined as a mental/bodily state that plays a particular role in relation to other mental/bodily states.
>     - Fear, for example, would then be a state that is caused by something in the environment that I consider to be dangerous and that leads to other mental/bodily states, for example the state of running away from the feared thing, elevated blood pressure, screaming etc.

## What is an emotion? (3)

>- I could also describe an emotion in a purely *behavioural* way. 
>- A behaviourist would only look at how a system behaves. He would not ask what is happening “inside” the system. So:
>     - Fear is identical to a particular behaviour: screaming, running away, pushing the feared thing away if it comes too close, and so on.
>- How is this different from the functionalist description?
>     - In this description we don’t care about the inner mental states of the subject at all.
>     - It is all about the observable behaviours. Whether there are any particular mental states associated with these behaviours is irrelevant.


## What is an emotion? (4)

>- How would you evaluate these approaches in terms of AI systems? Which descriptions are more/less meaningful when applied to robots?
>- The William James approach is least useful to AI, since it identifies an emotion with a particular physiological reaction.
>     - If we defined emotions like that, then machines could never have similar emotions to ours, since they are constructed in a different way and our physiology does not apply to them.

## What is an emotion? (5)

>- The behaviourist approach seems to also be problematic:
>     - If an emotion is defined behaviourally, then there’s nothing else to “having an emotion” than just displaying a particular behaviour. 
>     - This seems too superficial.
>     - Behaviours can be faked (actors on stage), or behaviours can be suppressed.

## What is an emotion? (6)

>- The most promising seems to be a functionalist approach.
>- If a machine has internal states that relate to each other as emotional states in humans relate to other states of the human system, then we can describe these internal machine states as “emotion-equivalent.”
>- So if a machine sees something that is justifiably perceived as dangerous to it; and it runs away from that thing, screaming; then we would be justified in ascribing “fear” to the machine.

## Rationality criteria for emotions

>- But not every random mental state that leads to some behaviour is an emotion. Justifiable (rational) emotions must fulfil some criteria.
>- See the previous lecture on emotions for more details. Here is a recap:
>     - An emotion has to be reasonable;
>     - it has to fit the situation;
>     - its intensity should be proportional to its object;
>     - it should be in our own long-term best interest;
>     - it must be understandable. We must be able to understand why someone behaves that way. (Smuts, 512)


## Basic features of emotions (1)

>- Emotions are *reactions* to experiences.
>     - Encountering a spider causes fear.
>     - Listening to a nice piece of music causes joy.
>     - As such, they are not directly under our control.
>- Emotions *feel* in a particular way.
>     - Dispositional emotions (for example, a fear of spiders when no spider is present) must first be triggered to be felt.
>- Emotions have two ‘objects’:
>     - The *target*: the thing which the emotion is directed at (the spider, the wild dog, ...)
>     - The *formal object*: the property that we fear in the target (the dangerousness of the spider or wild dog).


## Basic features of emotions (2)

>- Emotions therefore are *felt evaluations.*
>- The function of emotions is to track what matters to us.
>- As evaluations, emotions can be correct or incorrect, rational or irrational.
>- An emotion is justified if “the formal object that it is picking out is actually provided by the target.”
>     - A fear of that dog is justified if that dog is actually dangerous.
>     - A fear of that spider might not be justified if that spider is not dangerous. (Pismenny/Prinz, 2)


## What does it mean for AI to “have” emotions?

There are different ways to understand what it means for a machine to be able to deal with emotions.

>1. Being the (emotionless) object of a human emotion. Eliciting emotions without “having” them.
>2. Recognising or being aware of human emotions, without “having” them.
>3. Reacting appropriately to human emotions, without “having” them.
>4. Displaying (behaviourally simulating) appropriate emotional responses, without “having” emotions.
>5. Actually “feeling” or “having” an emotion.

. . . 

These options are located along an axis from “weak emotional AI” to “strong emotional AI,” in analogy to the weak/strong AI distinction regarding mental states in general.

## Subjects and objects of emotions (1)

Similarly, we can distinguish between:

>- The machine being the object of a human emotion; and
>- The machine being the subject (the carrier) of an emotion.
>- The conditions for these two are very different.

## Subjects and objects of emotions (2)

>- Everything can (in principle) be the object of a human emotion. The object doesn’t need to have any particular properties.
>     - People have emotional attitudes (including love and liking) towards children, pets, insects, computers, political parties, football clubs, books, pictures and food.
>     - In this sense, of course they will also have emotions towards robots.
>- But being the subject/carrier of an emotion is much more difficult.
>     - Depending on the description of emotions we have in mind, this can range from simulating emotional responses to actually having a mind. 

## Can I love a robot?

>- We have already talked about theories of what love is:
>- 1. Love is a *union* between two partners.
>     - By loving each other, two people fuse their interests and form a union that has new characteristics, different from those of the two separate people (“our” house, children, bank account and so on).
>- 2. Love is *robust concern* for the other person.
>     - What defines a loving relationship is (1) reciprocal, (2) enduring (robust) and (3) unselfish concern for the (4) well-being of the (5) other person.
>- 3. Love is the *result* of having a particular kind of *relationship* to someone. 
>     - In this view, it is not the “falling” in love that is significant, but the “staying” in love. The particular history of a relationship between two people *creates* a bond of love between them.

## Love as union

>- We can try to apply these theories to loving a machine:
>- Can I form a *union* of interests with a machine?
>     - Not really. Machines (at least at present) don’t have their own interests. The machine’s interest (for example not to be destroyed) is only relevant because of *my* interest to have that machine working.
>     - So any interest “of the machine” is, in reality, derived from the user’s interests.
>     - Therefore, there can be no union of interests.

## Love as robust concern

>- What defines a loving relationship is (1) reciprocal, (2) enduring (robust) and (3) unselfish concern for the (4) well-being of the (5) other person.
>     - “Reciprocal” concern is difficult to imagine. Can a machine be “concerned” about the user’s well-being? Is the user really concerned about *this* individual machine, given that all machines are replaceable?
>     - Is the concern enduring? We normally use machines as means to some end. After the end is achieved, we don’t have a further interest in the machine. For example, does the astronaut have an interest in a rocket’s spent first stage?
>     - Is the user’s concern for a machine truly unselfish?
>     - How do we define well-being for machines? And can a machine judge the well-being of a person independently of its own purpose? Could a sex robot *refuse* to provide sex services if it knew that by doing so it would benefit its user?


## Love as relationship

>- This is perhaps the most promising approach.
>- Users can indeed forge long-term emotional relationships with artefacts (for example, cars, jewellery, old photographs, childhood toys).
>- But the relationship is not reciprocal.
>     - Is one-sided love, even towards humans, still “proper” love?
>     - For example, if someone “loves” a movie star, but the movie star neither knows or cares about that person, is this really “love” or just one-sided admiration?
>     - In the same way, is the “love” towards a toy “real” love? Or is it just “liking,” “admiration” and so on?

## An Aristotelian view of love (recap)

>- For Aristotle, love is primarily a form of friendship (“philia”).
>- There are three types of friendship (also applies to love):
>     - Friendship based on usefulness;
>     - Friendship based on pleasure;
>     - Friendship based on the good character of the beloved (“true friendship,” “true love”).
>- The ultimate point of all human activity (including love and friendship) is to make us better (and therefore, happier) persons.
>- Moral action and a good character are the basic components for a good, fulfilled life.

## Can robots be Aristotelian partners?

>- A robot can be useful.
>- A robot can be pleasurable (sex robots, entertaining chatbots).
>- But can a robot improve the user’s morality? Can it show a “good moral character” and benefit the user’s moral growth?
>- Presently, it is not clear how it would do that.
>- If the robot deceives the user (as in the Turing test) it is unlikely to instill a love of truthfulness in the user.
>- Only an authentic, truthful, wise and morally good robot could be said to benefit the user’s character.
>- But we don’t (presently?) know how to build such a thing. (This is Sullins’ criticism in the reading).

# Uses of loving machines

## Affective computing and love (1)

>- “Affective” computing is an area of research that studies emotions towards computers and robots and how these robots could react back.
>- Love is not normally a goal of engineering affective computers.
>- Affective computers have many other practical uses. 
>- Some emotions are an important part of a user’s interaction with a machine (satisfaction, frustration, gratitude, hate, rage). It would be good if the machine could recognise such emotions and react to them as part of shaping the user’s experience.


## Affective computing and love (2)

>- Human “love” as an emotional attitude does not seem to require very special capabilities of the target machine.
>     - People love their cars, primitive blow-up sex dolls, tamagotchis, teddy bears and many other things that have no computational power whatsoever. 
>     - They love babies, that also don’t. 
>- So the problem with loving machines is not an issue of affective computing!

## How can robots recognise emotions? (1)

There are multiple “channels” through which a computer can gain information about a user’s emotional state (Picard 2004):

>- Postural movements while seated;
>- Physiology (for example, blood pressure, pupil dilation and so on);
>- Dialogue (the computer can ask the user or observe the user’s dialogue with a third party); 
>- Facial expressions.

## Facial expressions (1)

The six basic emotions:

![](graphics/19-nitsch1.png)\ 



## Facial expressions (2)

> “The FLOBI humanoid head utilizes so-called babyface cues, consisting of large round eyes, and a small nose and chin. Emotions are primarily conveyed through different configurations of eyebrow, eyelid and lip movements.” (Nitsch 2014)

## Facial expressions (3)

![](graphics/19-nitsch2.png)\ 



## Relational agents

>- Relational agents are computer programs that try to establish long-term relations with the user.
>- “Tricks” to create a relational bond include:
>     - greetings, 
>     - pretending to be happy to see the user again, 
>     - showing concern if the user reports bad news,
>     - subtly changing the style of language over time as the program and the user get more familiar with each other, and 
>     - referencing past interactions. (Picard 2004)

## Uses of loving machines

Robots that express love or other feelings and that can relate to the user emotionally would be crucial for:

>- Childcare
>- Eldercare
>- Medical care
>- Partnership for lonely people
>- ... and perhaps sexual services.


## Advantages of loving machines

>- Relational, love-exhibiting machines could save resources in child- and eldercare and help keep them affordable.
>     - As populations grow increasingly older, there might not be enough young people to pay for the care of the elderly.
>     - Robots can help keep reasonable quality eldercare affordable.
>     - But these robots will need very good relational skills. Nobody wants to age in the company of a vacuum cleaner.
>- Elder-care robots could also keep an eye on people who live alone and provide help (or call for help) in case of accidents in the home.
>- And, finally, they could provide sexual services without the exploitation of other human beings that is part of today’s sex trade.


# Some problems of loving machines

## The authenticity problem (1)

>- Turkle (2005) points out the *authenticity problem* of robots that exhibit emotional expressions without having the right corresponding affective states (“pretending to love”).
>- She describes how people in elderly care homes interacted with talking doll, overcoming loneliness and even working through issues with their real-life relationships by explaining them to the doll.
>- Children reacted very differently to a robotic pet (“AIBO”):
>     - Some exhibited a detached-biological approach: “This is only a machine, it doesn’t have feelings.”
>     - Some showed an imaginative-maternal approach: they believed that the robot dog has genuine feelings, that it can get ill or die.
>     - Some used the robot dog as a metaphor in order to talk about issues that they could not talk about directly (“life-situating approach”).


## The authenticity problem (2)

Turkle:

> Authenticity in relationships is a human purpose. So, from that point of view, the fact that our parents, grandparents, and our children might say “I love you” to a robot, who will say “I love you” in return, does not feel completely comfortable and raises questions about what kind of authenticity we require of our technology. Do we want robots saying things that they could not possibly “mean?” 


## The authenticity problem (3)

> Robots might, by giving timely reminders to take medication or call a nurse, show a kind of caretaking that is appropriate to what they are, but it’s not quite as simple as that. Elders come to love the robots that care for them, and it may be too frustrating if the robot does not say the words “I love you” back to the older person, just as we can already see that it is extremely frustrating if the robot is not programmed to say the elderly person’s name. These are the kinds of things we need to investigate, with the goal of having the robots serve our human purposes.

## The authenticity problem (4)

>- Sullins (2012): The problem is that love, and particularly philia (friendship), are also meant to make us better as persons (Aristotle).
>- Love has a moral component. Robots can never fulfil this essential function of love and friendship.
>- In Aristotelian terms, robots could be friends from usefulness or pleasure, but they could never be genuine, complete friends.

## The uncanny valley problem

>- Although robot likeability increases as robots become more human-like in appearance and behaviour, there is a point when robots become too similar to humans, but not perfectly human-like and likeability drops suddenly.
>- This is called the “uncanny valley” of robotics (Mori M. (1970) The Uncanny Valley. In: Energy 7 (4), pp 33–35.)
>- The Uncanny Valley is the perception that “something is off” with a human-like thing’s behaviour. 
>- The *expectations* for the thing’s behaviour that are triggered by its (human-like) appearance are *not satisfied by the actual behaviour* of the thing.
>     - Example: Corpses, very lifelike baby dolls, humanoid robots.
>     - Stone statues don’t create this effect, because when we see that they are just made of stone, we don’t create expectations of human-like behaviour for them.


## Bunraku puppets

![](graphics/19-bunraku.jpg)\ 




## Sophia the robot

![](graphics/19-sophia.jpg)\




## The Nao robot (a counter-example)

![](graphics/19-nao.png)\ 



## Uncanny valley

![](graphics/19-uncanny.png)\ 



## Factors causing the phenomenon

>- When an artefact does not look human-like at all, we have no expectations regarding its behaviour.
>- But as the artefact becomes more similar to humans, we automatically tend to expect it to behave in a “human-like” way.
>- The more human-like it looks, the more it needs to behave like a human.
>- A robot that looks very much like a human (like Sophia) but does not behave “right,” is alarming, in the same way like a human would be if he behaved in an off way.
>- We are ourselves sensitive to odd behaviour from others, that is a signal of possible danger. So we perceive an oddly behaving robot as dangerous.
>- This is not the case for a robot that looks like a toy, because there our innate expectations about what behaviour would be “odd” don’t work.

## What can be done about the uncanny valley (1)

>- How can the effect be mitigated?
>- First, we could stop trying to make robots human-like in appearance. 
>     - For most applications, a rough approximation of the human form, or even a pet shape, would be sufficient and would not cause the uncanny valley problem.
>     - For example, the toys in the Toy Story movies don’t cause this problem.

## What can be done about the uncanny valley (2)

>- Second, we could try to make robots behave in more human-like ways.
>     - If we use increasingly refined neural networks to control a robot’s movement, it is likely that the effect of the uncanny valley will be overcome at some point, as robots become sufficiently human-like to not cause discomfort to the observer.
>     - A good example are deceased, digitally recreated actors in Hollywood movies. If done right, they look perfectly human and don’t cause any problem.

## What can be done about the uncanny valley (3)

>- Third, humans get used to everything.
>     - The uncanny valley might be a temporary effect, because right now we are not used to seeing “almost-human” artefacts.
>     - We know that people had similar problems when the first railroads were built, and they wondered whether anyone could survive travelling at 50 or more kilometres per hour. Today we have no problem travelling at 900 km/h in commercial airplanes.
>     - If the uncanny valley is a problem of the mismatch between expected and actual behaviour, then if our expectations are adjusted (due to increasing familiarity with robots) the problem might disappear.

## The degradation of love problem (1)

>- One could argue that calling what machines do “love” degrades the concept of love.
>- Particularly Aristotelians would insist that, since robots lack human feelings, wisdom and personality, they can never be adequate partners for human friendship or love.
>- Calling such superficial relationships “love” makes us lose sight of what love really is (or should be).

## The degradation of love problem (2)

>- *Do you agree? What could we say about this?*
>     - For one, love is an incredibly complex phenomenon, even where no robots are involved. 
>     - Not all humans love in the same way. Not all human relationships are deep and meaningful.
>     - Humans also have (and always had) superficial relationships, sex without love, one-sided love, obsessive love, jealousy and all kinds of other “bad love” phenomena. 
>     - It is not clear that robot love is worse than some bad forms of human “love” have been.

## The degradation of love problem (3)

>- Second, you could say that we already have a degradation of the concepts of love and friendship through dating sites, facebook and so on.
>     - A facebook “friend” is not a “friend” in the traditional sense of the word.
>     - A dating experience that begins with swiping pictures of people right and left on a phone screen and ends with a meaningless date is also not much of a “love” experience.
>- Technology has already appropriated these words and changed their meaning.
>- But of course, this alone does not justify watering down these concepts further.

## The social isolation problem (1)

>- Will intimacy with robots lead to greater social isolation?^[The following discussion is from Sharkey 2017.]
>- Sullins (2012): “These machines will not help their users form strong friendships that are essential to an ethical society.”
>- Whitby (2011): “An individual who consorts with robots, rather than humans, may become more socially isolated.” 
>- Turkle (2011): Real sexual relationships could become overwhelming because relations with robots are easier.
>- Snell (1997): For the same reason, sex with robots could become addictive.

## The social isolation problem (2)

>- Kaye (2016): Sexual relations with robots will "desensitise humans to intimacy and empathy, which can only be developed through experiencing human interaction and mutual consenting relationships." 
>- Vallor (2015): Moral and social deskilling, which can lead to an inability to form social bonds. 
>- In a study about robots in the home, Dautenhahn et al. (2005) found that although 40% of participants were in favour of the idea of having a robot companion in the home, they mostly saw their role as being an assistant, machine or servant. Few were open to the idea of having a robot as a friend or mate.

## The social isolation problem (3)

Sullins:

> Computing technology is such a compelling surrogate for human interaction because it is so malleable to the wishes of its user. If it does not do what the user wants, then a sufficiently trained user can reprogram it or fix the issue to make the machine perform in line with the user’s wishes. ... Fellow humans, on the other hand, represent a much more difficult problem and do not always readily change to accommodate one’s every need. They provide resistance and have their own interests and desires that make demands on the other person in the relationship. Compromise and accommodation are required and this is often accompanied by painful emotions.


## The social isolation problem (4)

>- Graaf and Allouch found that (Sharkey 2007):
>     - 20.5% think that companion robots could decrease loneliness; 
>     - 14.3% said that robot companions could increase social deprivation or isolation; 
>     - 38.4% thought that there would be no positive consequences from using them.
>- But maybe this is all too negative. There are already example of people taking their sex dolls with them to bars (Sharkey 2017, 20). 
>- Perhaps people will learn to socially accept sex robots as actual partners, in the same way as society has got accustomed to divorce, sex before marriage, contraception, and homosexuality, which all were once thought to be not socially acceptable.
>- If there was no social stigma attached to robots partners, then perhaps they would not lead to social isolation.

# Is robot prostitution immoral?

## The (im-)morality of human prostitution (1)

>- Although prostitution is morally accepted and legal in some countries, in others it is considered immoral and/or illegal. Can you think of reasons why someone would consider prostitution immoral?
>     - Prostitution harms, exploits and demeans women; 
>     - It leads to the spread of sexual diseases;
>     - It fuels drug problems;
>     - It can lead to an increase in organised crime;
>     - It breaks up relationships; and more (Levy 2007).

## The (im-)morality of human prostitution (2)

>- But we could also see the beneficial aspects of prostitution:
>     - In a well-organised welfare state, prostitution can be regulated by law, eliminating exploitation and the danger of diseases;
>     - Prostitutes can teach the sexually inexperienced how to become better lovers;
>     - Prostitution can help alleviate loneliness, stress and tension;
>     - It provides sex without commitment for those who want it.

## Robot prostitution

>- We can see that most of the objections don’t really apply (at least not directly) to robot sex:
>     - Robot prostitution does not (directly) harm, exploit and demean women; 
>     - It does not lead to the spread of sexual diseases;
>     - It does not fuel drug problems;
>     - There is no obvious connection to organised crime;
>     - It probably won’t break up any relationships.

## Demeaning women?

>- Although “female-like” sex robots don’t directly involve real women, can we still say that they demean women? How?
>     - You can demean someone without their involvement. 
>     - For example, a drawing of a person can be used to demean or attack them. 
>     - A doll that looks like someone can be used to demean the target person.
>     - A written story about someone can be used to demean them.
>     - In these cases, the physical presence of the target is not required.
>- Similarly, we can argue that using a sex robot that “looks like” population segment X (for example, women) promotes the objectification of that population segment and demeans them.


## Why men visit prostitutes

Levy (2007) provides a list of reasons why men visit prostitutes:

>- Variety of the sexual experiece;
>- Lack of complications and constraints;
>- Lack of success with the opposite sex.
>- Robots can provide these services and thus be beneficial to particular members of society.

## Ethics of robot prostitution (1)

Levy (2007) argues that there are a few ethical considerations involving sex robots:

>- *Making robot prostitutes available for general use.*
>- If this was a problem, Levy says, then why does society not object to the sale of vibrators for women, which have the same purpose?
>- Do you agree that this is a good analogy?
>     - One could argue that vibrators don’t look like men, and that, therefore, they don’t objectify men in the same way as a full-sized, moving and speaking “female-like” robot.

## Ethics of robot prostitution (2)

>- *Justifying the use of sex robots to society in general.*
>- On the one hand, Levy says, it might be easier to justify using a sex robot that is not alive, than a human prostitute who is.
>- On the other, some states in the US (and probably some counties elsewhere too) have laws against the sale of vibrators.
>- It is probable that such places will outlaw sex robots too.

## Ethics of robot prostitution (3)

>- Robots might take part in the sexual life of a couple without the usual problems that are associated with human third parties (infidelity, jealousy, fear of indiscretion and diseases).
>- This will depend on the sexual morality and habits of individual couples, but one can think of many uses of sex robots that enrich the sex life of couples without causing any harm.

## Ethics of robot prostitution (4)

>- Human prostitutes might lose their jobs.
>- On the one hand, this might be considered a good thing, since prostitution is generally seen as a degrading and dangerous job that is performed in a bad environment and often associated with drugs and exploitation. 
>- On the other hand, prostitution is often the “last resort” job for people who have no other options. Putting prostitutes out of work might make it impossible for them to earn a living in any other way.

## Ethics of robot prostitution (5)

>- *Is this a good argument to keep prostitution going?*
>     - It doesn’t seem to be. With the same argument, one might want to protect killers’ or street drug dealers’ jobs. Often these too don’t have other options.
>     - The solution is not to let them do their (immoral, degrading, exploitative, dangerous) job, but to create a society in which all citizens have better options to earn a living.
>     - In the last resort, a society could provide sufficient unemployment benefits to people who are unable to find a job, so that nobody is forced to work as a prostitute or killer.

## Advantages of sex robots for society

>- “Many who would otherwise have become social misfits, social outcasts, or even worse will instead be better balanced human beings.” (Levy).
>- If sex robots really have this effect, they should be considered a therapeutic tool (Sharkey 2017).
>- Dolls might be used in care homes as companions (like we saw above in the Turkle paper). In principle, they might also provide additional, sexual services.
>- UK Human Rights Act 1998 and Equality Act 2010: It is illegal not to support disabled people to enjoy the same pleasures as others enjoy in the privacy of their own homes. (Sharkey 2017)

## Could sex robots reduce sex crimes? (1)

>- Could sex robots help reduce sex crimes?
>- Some sexual preferences cause harm to others or the person who has the preference themselves: voyeurism, exhibitionism, paedophilia. 
>- Would we accept a robot child substitute as a solution that satisfies a paedophile and keeps him or her from assaulting real children?
>- Ronald Arkin, robotics professor at the Georgia Institute of Technology: “people should not only legally be permitted to have such dolls, but perhaps some should be handed prescriptions for them. [Sex robots] might function as an outlet for people to express their urges, redirecting dark desires toward machines and away from real children.” (Sharkey 2017, 26).

## Could sex robots reduce sex crimes? (2)

>- Peter Fagan from the John Hopkins School of Medicine is sceptical that there ever will be therapeutic use for sex robots. [Such robots] would likely have a “reinforcing effect” on paedophilic ideation and “in many instances, cause it to be acted upon with greater urgency.” (Morin, 2016, cited after Sharkey 2017)


## Could sex robots reduce sex crimes? (3)

Philosophy professor and robot ethicist Patrick Lin (California Polytechnic):

> Treating paedophiles with robot sex-children is both a dubious and repulsive idea. Imagine treating racism by letting a bigot abuse a brown robot. Would that work? Probably not. If expressing racist feelings is a cure for them, then we wouldn’t see much racism in the world. (Sharkey, 2017)


