---
title:  "AI and Society: 7. Cyc and common sense computing"
author: Andreas Matthias, Lingnan University
date: September 2, 2019
...


# Cyc and the problem of common sense

## What is "common sense"?

>- It is more than just explaining the meaning of words.
>     - For example, "sibling" or "daughter" can be explained in Prolog with a dictionary-like definition. This does not require common sense.
>- Common sense can mean that you don't have to explain everything in a conversation, because you can assume that most people (in your group) have similar *background knowledge.*
>     - Background knowledge: "You don't usually walk from Hong Kong to Germany." "Restaurants are closed in the night."
>- The knowledge of the *context* of what people say and do. It is this context that makes their utterances and actions understandable.
>     - See Grice: Conversational Implicature.

. . . 

Textbook: <https://medium.com/@MoralRobots/can-machines-have-common-sense-6bfd6cebfa68>

## There is not *one* common sense

>- "Common" sense is common to particular groups of people, but can differ between groups.
>- The common sense and background knowledge of a hunter living in a small village in Africa will be different from that of a Hong Kong teenager.
>- Common sense depends on culture, gender, age, social group, and other factors.
>- It is not "common" to *all* humans.

## What is involved

The command "Go to the restaurant" already needs common sense to be interpreted correctly:

>- What means of transport can be used? (Generally, for this route, at this time of day, etc)
>- If you walk to the restaurant, you have to observe the traffic and not be killed on the way.
>- If you take the bus, you have to have money or an Octopus card.
>- ... and so on.

## Human and machine "common sense"

>- How do humans acquire common sense?
>     - By growing up in a particular society and making experiences in it.
>     - This takes a long time (typically 18 years until a human is able to function fully).
>- How can machines acquire common sense?
>     - Machines lack the "growing up" phase.
>     - How can this problem be solved?

## Giving machines common sense

>- Option 1: Let the machine "grow up" like a human and develop common sense in the same way like humans do.
>- Option 2: Provide the machine with common sense in the form of facts and rules (like in a computer program).
>- But: What common sense should a machine have?
>     - Option 2a: Machine common sense? (Is there even a useful concept of "machine common sense"?)
>     - Option 2b: Human common sense?
>- Option 2b is what the Cyc project attempts to do (discussed later): To provide a machine with a (big) set of facts and rules that encode all of human background knowledge.

## Option 1: "Raising" machines with human-like common sense (1)

>- In order for this to work, the machine must be able to learn by itself.
>- The machine must be able to make human-like experiences.
>- In order to have *human-like* experiences, a machine will need:
>     - A human body (to experience hunger, fatigue, love)
>     - Emotions, desires (or can perhaps one *understand* emotions without *having* them??? -- See previous presentation)
>     - Interactions with humans in order to learn social norms
>     - Family, friends (in order to understand their importance and our emotions towards them)

## Option 1: "Raising" machines with human-like common sense (2)

>- In order to have *human-like* experiences, a machine will need (continued):
>     - To *think like a human:*
>         - Spontaneous reactions (catching a ball)
>         - Everyday, unconsciously performed  skills (walking, handwriting, picking up a glass of water)
>         - Forgetting, having limited memory
>         - To have sensors similar to humans (visible spectrum of light etc)
>- In the end, a perfect human-like common sense seems to require both structural equivalence with humans *and* a complete human life experience! (Not practical!)

## Option 2a: "Machine-like" common sense

>- What does it mean for a machine to have a "machine-like" common sense?
>- Compare: pets. Pets are socialised to behave *compatibly* with humans (although they don't have the same common sense or background knowledge).
>- Robots that share living space with humans must also behave compatibly: e.g. a robotic Roomba vacuum cleaner.
>- *But what does it mean for behaviour to be "compatible"? What behaviour do we expect from our pets or robots?*
>- The behaviour of pets/robots should be:
>     - *Not surprising,* this means:
>     - **Predictable** and
>     - **Controllable**

## Machine common sense

Looking back at the arguments just made, we can see:

>- Creating machines that develop genuine human-like common sense through being raised as human-like creatures may be impossible as long as machines are not *structurally* equivalent to a human and not *raised as human beings.*
>- Giving machines only "machine-like" common sense may not be sufficient if we want them to act in a predictable and (to us) understandable way (because of the structural differences and the epistemic disadvantages involved).
>- Thus, a third option might be more promising: to try to give machines ready-made human-like common sense *without* the human experience. This is discussed in the next section.

## Option 2b: Cyc: Human-like common sense *without* the experience (1)

Cyc was a ten-year project by Douglas Lenat, aimed at providing a computer with a common sense database of everyday knowledge (1984--1994).

- General introductions:
    - <https://www.technologyreview.com/s/600984/an-ai-with-30-years-worth-of-knowledge-finally-goes-to-work>
    - <https://www.technologyreview.com/s/403757/cycorp-the-cost-of-common-sense/>

- Douglas Lenat:
    - Computers versus Common Sense: <https://www.youtube.com/watch?v=gAtn-4fhuWA>
    - <http://tedxtalks.ted.com/video/Computers-with-common-sense-Dou>

## Option 2b: Cyc: Human-like common sense *without* the experience (2)

- Lucid intros:
    - <https://www.youtube.com/watch?v=WSlTT0_eMNM>
    - <https://www.youtube.com/watch?v=J_dK24FW548>
	- <https://www.youtube.com/watch?v=kwYaj-1EVJ0>

- Cyc:
    - <http://sw.opencyc.org> (to download OpenCyc)
    - <http://www.cyc.com/platform/opencyc> (try some of the links)
    - <http://www.cyc.com/about/media-coverage/know-it-all-machine/> (article about Cyc)

## CycL query language

“A frightened person” in CycL:

| (#\$and
|   (#\$isa ?x #\$Person)
|   (#\$feelsEmotion ?x #\$Fear #\$High))

## CycL query language

More complex relations:

>- Wearing the red shirt rather than a green one caused the bull to charge rather than ignore him.
>- (#\$causes-Contrastive WearingARedShirt WearingAGreenShirt BullCharges BullDoesNotCharge)^[https://www.cyc.com/the-cyc-platform/frequently-asked-questions/what-differentiates-the-cyc-ontology]

## CycL query language

More examples:

>-  (#\$isa #\$BillClinton #\$UnitedStatesPresident)
>-  (#\$genls #\$Tree-ThePlant #\$Plant) (=All trees are plants)
>-  (#\$capitalCity #\$France #\$Paris) (=Paris is the capital of France)
>-  (#\$relationAllExists #\$biologicalMother #\$ChordataPhylum #\$FemaleAnimal)
>     - ... “which means that for every instance of the collection #\$ChordataPhylum (i.e. for every chordate), there exists a female animal (instance of #\$FemaleAnimal), which is its mother (described by the predicate #\$biologicalMother).” ^[All examples and explanations on this page from Wikipedia:Cyc.]


## Resolving ambiguities

- The police arrested the demonstrators because **they** feared violence.
- The police arrested the demonstrators because **they** advocated violence. 

. . . 

- Mary saw the dog in the store window and wanted **it**.
- Mary saw the dog in the store window and pressed her nose up against **it**.

## OpenCyc knowledge base (example)

![](graphics/kbase.png) \ 

## Extent of Cyc's knowledge base

>- The number vary according to the source and the time of publication. Since the system is continuously developed, it is a moving target.
>- 300,000+  concepts, forming an ontology in the domain of human consensus reality.
>- Nearly 3,000,000 assertions (facts and rules), using 26,000+ relations, that interrelate, constrain, and, in effect, (partially) define the concepts (1994 source)
>- According to CyCorp (2019) the Cyc Knowledge Base contains 25 million assertions.

## Microtheories

>- Not all concepts need to be consistent! A **microtheory** is an *internally consistent* collection of facts about a particular domain.
>- This allows storage of wrong of contradictory beliefs ("Nuclear power is dangerous," "we need more nuclear power because of global warming.")
>- In this way we don't need to keep the whole belief database free of contradictions (which is probably impossible).
>- “For instance, we could assert in the #\$TheSimpsonsMt (the microtheory for knowledge about the Simpsons) that Bart is a male fourth-grader. But in another context, #\$RealWorldDataMt (the knowledge about the real world), we can assert that Bart is a cartoon character.” (CyCorp website, op.cit)

## Microtheories

Other uses for microtheories:

>- “There are many different legal contexts: e.g. you should drive on different sides of the road in the United States versus England.”
>- “Newtonian and Quantum Physics are inconsistent, but it is often very useful to act as if one or the other is the right model to use.”
>- “In personal belief contexts microtheories are very useful: we can build a microtheory that contains all and only the beliefs held by a given agent to see what would be reasonable for that agent to conclude.” (CyCorp website, op.cit)

## Advantages of Cyc? (1)

>- What would the advantages of a working Cyc-like system be?
>- Avoid "brittleness": a program can easily come to the conclusion that a man is pregnant, or that a 20 year old has worked for 22 years if the input data say so. Cyc would be able to refuse such conclusions.
>- Easing communication between programs: for example, exchanging address books between email programs is hard. On the other hand, it's easy for a human to understand the address book of *any* email program.

## Advantages of Cyc? (2)

>- Understanding of meaning in order to resolve syntactic ambiguities (see examples above).
>- Knowledge retrieval from natural language texts, for example from the web.
>- Better computer software
>     - a calendar should not let you schedule a date with your vegetarian girlfriend in a steak-house!
>     - a calendar should consider travel times when making appointments in different locations!

## Practical successes of Cyc

>- “In a report by Vaughn Pratt of his visit with Ramanathan Guha in 1994 for a demo of the Cyc system, he noted that the system was correctly able to identify inconsistencies in two different sources of information - the first was a spreadsheet that indicated that a (fictitious) organization had destroyed a village while a second spreadsheet identified that same organization as a pacifist organization. Even though the information was found in two different sources, the system had correctly identified that a pacifist organization would not destroy a village - therefore one of the sources of information was incorrect.”^[https://www.cise.ufl.edu/~ddd/cap6635/Fall-97/Short-papers/36.htm]

## Practical successes of Cyc

>- Wikipedia (Cyc) has a list of typical applications:
>     - Pharmaceutical Term Thesaurus Manager/Integrator (Glaxo, about 300,000 terms)
>     - Terrorism Knowledge Base
>     - Cleveland Clinic Foundation (natural language interface for medical information)
>     - MathCraft (help for students in 6th grade)

## Practical successes of Cyc

>- According to CyCorp: “Cyc is the product of more than four million engineering hours invested over three-plus decades of intense R&D and hundreds of successful client deployments for some of the largest and most respected Fortune Global 100 companies, as well as for numerous US and allied government, defense, and intelligence agencies.”^[https://www.cyc.com/products]
>- The general consensus seems to contradict this optimistic view. 
>     - According to Wikipedia, there are “over 100” (not “hundreds”) of applications using Cyc.
>     - In the AI community, Cyc is little used, and there doesn’t seem to be much research around the product.
>- AI research nowadays focuses on deep learning, neural networks, natural language and image or video processing, and not so much on knowledge representation and common sense.

## Criticism (1)

>- How can we criticise the Cyc project?
>- There is an almost infinite number of common-sense facts. We cannot hope to encode them all.
>- Common sense is relative to culture/gender/age of subject. There isn't just *one* common sense for all humans.
>- For example: "The police arrested the demonstrators because they advocated violence." The ambiguity might resolve differently in a peaceful, well-ordered country than in a place that's ruled by a violent dictatorship.
>- Common sense and background knowledge change constantly (as our culture and environment changes).

## Criticism (2)

>- Terry Winograd (author of SHRDLU!):
>     - "Blindness of representation":
>     - If there is no rule for a particular case, then the system is "blind" to that case
>     - Human reasoning can always make sense of never before seen, new events (how does it feel to be chased by an alien monster?)
>     - A machine wouldn't know if this wasn't in its database
>     - Cyc can reduce this problem, but can it solve it entirely?
>- The human mind doesn't seem to work that way. We don't store huge databases of common-sense facts in our heads.
>- The symbols used (“ear”, “food”, “walk”) don't mean anything to the machine!

## Does Cyc contain "meaning"? (1)

>- Ontologies of this type can be huge, but do they contain "meaning?"
>- Consider a made-up language:
>     - Nouns: bloop, foop, noop.
>     - Verbs: bleep, feep, neep.
>     - Adjectives: blep, fep, nep.
>     - Sentences: The blep foop neeped. The fep noop does not bleep any more. Only if the nep noop neeps, the blep foop will bleep.
>     - Now I give you the definitions:
>          - A bloop is a very fep noop.
>          - A foop is a kind of zoob.
>          - A noop, on the other hand, is just a foop that has been neeped.
>- Do you now “understand?”

## Does Cyc contain "meaning"? (2)

>- Answer A: No, because “meaning” depends on the reference of a symbol to something other than a symbol (a “thing” that is experienced through the senses).
>- Answer B: Yes, because an Aristotelian definition supplies the “meaning” of a symbol in a way that is sufficient for us to handle the symbol properly. Proper symbol handling is all that is required for intelligence (behavioural approach).
>- Answer C: Some symbols can be defined in terms of other symbols, but ultimetely there must be some symbols which really refer to something other than other symbols in order to root the symbol system in reality. (This is called the "symbol grounding problem"!)

## Does Cyc contain "meaning"? (3)

>- Two of the three answers suggest that "true understanding" requires some kind of sensory experience (as a blind person cannot be said to fully understand the meaning of colour words). We will talk more about this in the next session.
>- See also: Mary's Room Argument; Qualia. (Explained in later session.)



## The End. Thank you for your attention!

Questions?

<!--

%% Local Variables: ***
%% mode: markdown ***
%% mode: visual-line ***
%% mode: cua ***
%% mode: flyspell ***
%% End: ***

-->
