---
title:  "AI and Society: 13. Machine learning and genetic computing"
author: Andreas Matthias, Lingnan University
date: October 12, 2019
...

# Machine Learning Terminology

## Machine and deep learning

>- Machine Learning (ML): Every kind of computing system that is able to learn. This can include symbolic systems (like Prolog or expert systems), neural networks, reinforcement learning, etc.
>- Deep Learning: A type of machine learning that is based on deep artificial neural networks.
>- Deep neural networks: Artificial neural networks with more than 3 layers, typically 10 or more. These are particularly good at visual recognition tasks, but also used for other applications.

## Main tasks of machine learning

There are three main tasks of machine learning:

- Regression
- Classification
- Clustering

Some of these typically run supervised, some unsupervised.

## Regression (1)

>- Regression means to put a line or a curve through a collection of data points, so that one is able to predict where new data points will fall.

![Linear Regression](graphics/05-Linear_Regression.png)

## Regression (2)

>- In other words, it is about understanding the relationship between two variables, so that one can predict the value of one if the value of the other is known.

![Sometimes data can be more complicated](graphics/05-gaussian-regression.jpg)

## Examples for regression

>- From a list of house features (living area, location, age, number of rooms), predict the sales price for that house.
>- From a list of tumour features (type of tumour, stage of development, organs affected), predict the probability of successful treatment.
>- From a list of student features (secondary school grades, frequency of Facebook updates, list of hobbies and interests, number of languages spoken, participation in student societies, grade in the midterm exam), predict the expected final grade for this course.
>- From a video feed from a camera in front of a car, predict where the other cars will be one second later.

## Classification

>- Classification is different from regression in that the desired output of the neural network is not a continuous number, but one of N different “bins” into which the input will fall.
>- The point here is to classify the input as being a particular one (out of N) kind of thing.
>- We can distinguish binary from multiple-class classification.

## Examples for classification

>- From a picture of an animal, find out if it is a cat or a dog.
>- From a picture of a person, find out if it is a man or a woman.
>- From a person’s *given name,* find out whether it is a man or a woman.
>- Recognise the objects in a video image.
>- Recognise what a self-driving car sees on the street in front of it.

## Examples for classification

![](graphics/05-classification.png)

## Try it out!

<https://www.ibm.com/watson/services/visual-recognition/>

## Classification vs. regression

>- The two uses are similar.
>- Generally, when we expect a numerical value as output, we talk of regression.
>- When we expect one of N discrete “bins” (or “tags”) as output, we talk of classification.
>- But a very fine-grained classification can be similar to a regression (for example, the prediction of a student’s grade above).
>     - If I predict A, B, C, D, F, then it would be a classification task.
>     - If I predict a number between 0-100%, it would be a regression task.

## Clustering

>- Clustering means that the machine will try to find similarities between different data objects by itself.
>- Given a collection of data points, it will try to “cluster” them together to a number of clusters.

## Example of clustering

>- Consider the data set:
>     - Red Toyota
>     - Black cat
>     - Red bird
>     - Blue Tesla
>     - Yellow bird
>- What clusters do you see?
>- Cars, animals, birds, red things, black things, blue things, yellow things.
>- Some things can belong to multiple clusters.


## Difference between clustering and classification

>- In clustering, we don’t know in advance which clusters we will find.
>- We just have a bunch of data, and try to find some regularities.
>- While the machine processes the data set, it will create new “bins” or “tags” as needed.
>- Therefore, clustering is an example of *unsupervised* learning.
>- In classification, the “bins” or “tags” are given in advance.
>- The machine has to sort new data into the existing bins, but not create new bins.
>- Since the bins (and examples of what goes in them) are given during machine training, classification is an example of *supervised* learning.

## Supervised learning

>- In “supervised” learning, we have input patterns and a desired output pattern for each input pattern. The algorithm learns to match the output to the input pattern.
>- The goal is to be able to guess, for a new input pattern, what the correct output pattern would be.
>- The process of learning from the training data resembles a teacher supervising the learning process by comparing expected and actual output patterns until the actual output matches the expected output (=the error is minimised).

## Unsupervised learning

>- In unsupervised learning, we only have input patterns, but no corresponding output patterns.
>- The goal of unsupervised learning is to learn more about the data by classifying and analysing them.
>- In unsupervised learning, there is no “teacher” and no model answers. Like in k-clustering, the algorithm has to find a structure in the data by itself.

# Genetic computing

## Concepts of evolution

>- Population of individual organisms with genes, situated in a particular environment.
>- Variation in genetic material.
>- Possibly (but not necessarily): mutations.
>- Recombination (particularly in sexual reproduction).
>- Selection under pressure from the environment.
>- Offspring with similar genetic material as parental organisms.

## Concepts of evolution

![](graphics/02-ga.png)\ 

## Mutations

>- Mutations are small, random changes of the genetic material.
>- Mutations are not always beneficial. In fact, most of the time they are harmful to the organism.
>- The more complex the organism, the more likely it is that a random mutation will be harmful. *Why?*
>- Because more complex organisms depend on a more precise relation between their parts to function. Random changes are more likely to break something.
>- Example: A hole in the leaf of a plant does not affect the plant much. A same-sized hole in the body of a human is more likely to be a serious injury or even lethal.

## Mutations

>- Another example: The more complex a word is, the more likely it is that a random mutation will “break” the word (= make it into a non-word).
>- Three-letter words are more robust to one-letter changes:
>     - “cut” -- a one-letter mutation can produce the good words: cat, cup, but, put, ...
>     - It might also produce garbage: cpt, cet, cuc, ...
>- With a five-letter word, it’s much harder to find “meaningful” mutations:
>     - “hello” -- Good mutations: cello. It’s hard to think of another.
>     - Most other mutations are destructive: xello, hzllo, hejjo, hellp.

## Genetic algorithms

Programs can evolve over many generations to perform particular tasks:

>- Playing snake: <https://www.youtube.com/watch?v=3bhP7zulFfY>
>- Cars evolving to drive up a mountain road without falling over: <http://rednuht.org/genetic_cars_2>
>- Virtual creatures evolving to perform various tasks: <https://www.youtube.com/watch?v=bBt0imn77Zg>
>- Learning to shoot an enemy creature: <https://www.youtube.com/watch?v=u2t77mQmJiY>
>- More evolving cars: <https://www.youtube.com/watch?v=lmPJeKRs8gE>

## Differences between neural networks and genetic algorithms

- Neural networks adapt their weights inside one individual, during a training phase. The individual itself learns.
- Genetic algorithms don’t (typically) change the genome of an individual. Instead, a whole population of individuals competes. The most successful ones survive and reproduce. The population as a whole ‘learns,’ not a particular individual.


# Practical significance of neural and genetic computing

## Consequences (1): No inspection of stored information

>- Stored information is a vector of “meaningless” numbers.

## Consequences (2): No full predictability of behaviour

>- The behaviour of a trained ANN can only be inferred by observation in test cases (like it is with humans or animals).
>- No number of tests can reliably cover every aspect of the ANN's future operation.
>- Thus, the future behaviour of the ANN will never be fully predictable.

## Consequences (3): Learning and application merge

>- In reinforcement learning (RL), learning and operation happen at the same time.
>- This means that the training phase is not under the control of a single trainer any more.
>- The environment trains the ANN!
>- This environment is not controlled and not predictable.
>- Thus, the learned content and the future behaviour of the ANN is not controlled or predictable either.

## Consequences (4): Inevitability of “error” with RL

>- “Error” in an reinforcement learning artificial neural network is just another name for the exploration of possible new behaviours in order to optimise the system's operation.
> “Error” is not an avoidable “mistake” any more, but a necessary feature of the adaptive system! (Like it is with animals and small children)

## Genetic algorithms 

>- Genetic algorithms have similar issues to neural networks.
>- Additionally, they are extremely dependent on the environment, since their "training" is performed *by the environment.*
>- Neural networks usually try to optimise a particular, narrowly defined behaviour, inside a system that is unchanging as a whole.
>- Genetic algorithms usually are free to develop the whole morphology of the organism in totally new (and unexpected) ways in order to optimise the program's fitness.
>- The programmer is not "programming" the particular creature any more. He becomes a "creator" of a software organism that then develops further on its own, entirely outside of the programmer's control.

