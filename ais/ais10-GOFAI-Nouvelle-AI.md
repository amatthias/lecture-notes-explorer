---
title:  "AI and Society: 10. Subsymbolic AI - Braitenberg vehicles and Nouvelle AI"
author: Andreas Matthias, Lingnan University
date: September 30, 2019
...

# Intelligence Without Representation

## Symbolic AI and subsymbolic AI

>- As opposed to symbolic AI, in *connectionism* (or subsymbolic AI) we don't have explicit symbols which stand for discrete meanings.
>- In brains, we don't find symbols. Instead, we have a network of neurons (nerve cells), which send electrical impulses to each other.
>- On an even more basic level, even complex *behaviour* doesn't need to be symbolically encoded!

## Braitenberg vehicles

>- Valentino Braitenberg (1926--2011): Neuroscientist and cyberneticist. Book: "Vehicles: Experiments in Synthetic Psychology."
>- Braitenberg's point was that even a very simple system can exhibit quite complex behaviours. Complex behaviour does not need symbolic representation!

## Braitenberg vehicle 1

What will this vehicle do?

![](graphics/05-bv-01-small.jpg)\ 

. . .

"Approach."

## Braitenberg vehicle 2

What will these vehicles do?

![](graphics/05-bv-02-small.jpg)\ 

. . .

"Fear." -- "Aggression."

## Braitenberg vehicle 2

![](graphics/05-bv-04-small.jpg)\ 

Braitenberg: "Let Vehicles 2a and 2b move around in their world for a while and watch them. Their characters are quite opposite. Both *dislike* sources. But 2a becomes restless in their vicinity and tends to avoid them, escaping until it safely reaches a place where the influence of the source is scarcely felt. Vehicle 2a is a *coward*, you would say.

Not so Vehicle 2b. It, too, is excited by the presence of sources, but resolutely turns toward them and hits them with high velocity, as if it wanted to destroy them. Vehicle 2b is *aggressive*, obviously."

## Braitenberg vehicle 3

Now imagine that we replace the connection between sensors and motor so that the sensors *inhibit* the motor.

What will these vehicles do?

![](graphics/05-bv-02-small.jpg)\ 


## Love and exploration

![](graphics/05-bv-05-small.jpg)\ 

Braitenberg: "You will have no difficulty giving names to this sort of behavior. These vehicles *like* the source, you will say, but in different ways. Vehicle 3a *loves* it in a permanent way, staying close by in quiet admiration from the time it spots the source to all future time. Vehicle 3b, on the other hand, is an *explorer*. It likes the nearby source all right, but keeps an eye open for other, perhaps stronger sources."

## More complex vehicles

"[Add] not just one pair of sensors but four pairs, tuned to different qualities of the environment, say light, temperature, oxygen concentration, and amount of organic matter."

. . . 

Expected behavior:

Given appropriate connections, "this is a vehicle with really interesting behavior. It dislikes high temperature, turns away from hot places, and at the same time seems to dislike light bulbs with even greater passion, since it turns towards them and destroys them... You cannot help admitting that Vehicle 3c has a system of *values*, and, come to think of it, *knowledge*." (Braitenberg)

## Real Braitenberg vehicles

A simulator:

<http://www.harmendeweerd.nl/braitenberg-vehicles/>

A video:

<https://www.youtube.com/watch?v=yUVcI5Pw2o4>

A longer video, with very detailed explanations of many types of Braitenberg vehicles:

<https://www.youtube.com/watch?v=A-fxij3zM7g>


## Braitenberg vehicles and AI

>- *What are the advantages of (complex) Braitenberg vehicles over symbolic AI systems?*
>- “A brilliant chess move while the room is filling with smoke because the house is burning down does not show intelligence.” (Anatol Holt, 1974, unpublished; cited by Winograd). Braitenberg vehicles react to their environment. They don't have *gaps of anticipation.*
>- They are stable and reliable, not brittle. They won't stop working if a rule is missing.
>- Imagine, for example, that you try to confuse Braitenberg vehicles with multiple light sources, or that you put obstacles in their way.
>- They might not behave *optimally,* but they will do *something* rather than just freeze or crash.
>- This is called *graceful degradation* (discussed below).
>- Much faster reactions, because no symbolic processing is needed.


## Rodney Brooks: Symbolic and subsymbolic representation

>- In the sixties and seventies, several laboratories attempted to build robots that used symbolic AI to represent the world and plan actions. These projects had limited success.
>- In the eighties, Rodney Brooks of MIT was able to build robots that had superior ability to move and execute tasks without the use of symbolic reasoning at all.
>- Brooks (and others) demonstrated that our most basic skills of motion, survival, perception, balance and so on did not seem to require high level symbols at all.
>- He said that he was inspired to create a new robotic architecture when he observed the movement of insects, who can survive and thrive in a real, complex environment, despite having only minimal nervous systems.

## Coke can collection

Task: Program a robot to walk through offices and collect empty Coke cans.

. . . 

Symbolic version:

>- Prepare a map of the office space, including desks, tables, chairs (but these are moving?!)
>- Give a symbolic description of a coke can.
>- Write a program to make the robot walk through the office space and look for Coke cans.
>- Consider:
    - Collisions with people,
	- with furniture,
	- different views of a coke can in different perspectives,
	- unexpected obstacles (locked doors, things left lying around),
	- other “Coke-can-like” objects, ...
>- The task seems almost impossible!

## Brooks: "Subsumption architecture" (1)

>- Build systems from the bottom up, in independent layers.
>- Each layer directly connects perception to action.
>- The lowest layer is just concerned with moving the robot around.
>- A layer on top of that avoids obstacles when moving.
>- The next layer plans where the robot should go. It knows nothing of actually moving the robot or avoiding obstacles (this is done by the lower layer!)
>- An independent low layer recognizes objects in field of vision.
>- Another layer on top of that identifies Coke cans.
>- A still higher layer makes the robot move close to the Coke cans.

## Brooks: "Subsumption architecture" (2)

>- This was particularly beneficial at a time when computers were slow and it was difficult to process complex environments symbolically (for example to store huge, detailed and changing maps of the environment).
>- A subsumption architecture robot does not store any information about the environment. Instead, it *senses* the environment itself!
>- In this way, every environment is its own map and does not consume computational or storage resources of the robot.

## Subsumption architecture

<https://www.youtube.com/watch?v=YtNKuwiVYm0>

Another example:

![](graphics/05-ssa.jpg)\ 

## Advantages of subsumption architectures

>- Low processing overhead.
>- Tight coupling between perception and action.
>- Modular structure.
>- Rapid, reflexive responses.
>- Long-term and short-term goals.
>- Easy to construct relatively complex systems.
>- Systems tend to work well in the real world, particularly in changing environments and on slow computers.

## Drawbacks of subsumption architectures

>- Difficult to extend beyond controlling simple creatures.
>- Hard to do:
    - Simultaneous (parallel) goals and complex goal structures.
    - Planning and control of complex actions in multiple layers.
>- The original coke-can collecting robot could not easily find its way back to its home position, because it did not have a map.
>- In order to find back, it always went through doors in a north direction, so that it could always go back by travelling south. But this seems to be a pretty inefficient way to navigate.
>- Nowadays, with faster computers, we can combine symbolic and subsymbolic approaches to achieve better performance.

## Rodney Brooks: Symbolic and subsymbolic representation

Rodney Brooks, *Elephants Don't Play Chess* (1990):

>- AI cannot be made in a symbolic way.
>- Symbolic processing is not necessary or sufficient for AI.
>- Robots should not make an internal symbolic model of the world.
>- Successful AI must be situated in an environment. This solves the symbol grounding problem, too!
>- The *world itself* "is its own best model. It is always exactly up to date. It always has every detail there is to be known. The trick is to sense it appropriately and often enough."
>- Elephants are "intelligent" in their behaviour (in the sense that their behaviour is adapted to their survival needs), but they don't need to "play chess" to prove it (that is, they don't need to do symbolic processing).

## Symbolic and subsymbolic processing

>- Such architectures are called "subsymbolic" because they operate on a level "below" the symbolic level.
>- Neural networks (discussed a little later) are another example of subsymbolic AI.
>- If you think about it, human performance is a mixture of symbolic and non-symbolic elements:
>     - Mathematics, logic, chess and language are symbolic abilities.
>     - Digestion, heartbeat, walking, avoiding obstacles are subsymbolic skills.
>     - Many skills (e.g. driving a car, playing chess on a physical board) are a mixture of symbolic and non-symbolic parts.
>     - We are born with some subsymbolic skills (heartbeat, digestion). We have to learn others explicitly (walking, opening a door). We have to learn almost all our explicitly symbolic processing abilities (maths, language).

## “Graceful degradation”

>- “Graceful degradation” means that in case of a program error, the robot should not just “hang” or reboot standing in the middle of the room.
>- Instead, if a few functions fail, a subsumption system could still go on working, because it is comprised of a multitude of independent systems.
>- If the movement planning goes wrong, for example, the Coke can collecting robot would perhaps walk into the wrong room.
>- But it would still go on walking about, it would not collide with objects, it would still identify and collect Coke cans correctly.

## GOFAI and Nouvelle AI

>- AI research programs that work along these lines are often called “Nouvelle AI,” to distinguish them from GOFAI, “Good Old Fashioned AI.”
>- Nouvelle AI would question the idea that symbolic AI systems are necessary or sufficient to achieve intelligent action.
>- Symbolic systems are not *necessary,* because there are systems that can behave intelligently (=appropriately in their environment) which don’t do symbolic processing (e.g. insects, or elephants).
>- Symbolic systems that don’t have sensors are not *sufficient,* because they don’t have the direct access to reality that is needed for intelligent action.

## Nouvelle AI

*Nouvelle AI* (French for 'new'), as opposed to GOFAI:

>- Grounded symbols (robot has access to referential semantics).
>- Situated, not abstract cognition.
>- Optimal behaviour in a live and changing environment.
>- Graceful degradation of functions in case of error.

## Importance of Nouvelle AI

>- What can we learn from the theses of Nouvelle AI for our understanding of intelligence?
>     - "Intelligence" is not a binary ability. It is not something a thing has or doesn’t have.
>     - Rather, intelligent behaviour seems to be a continuum of abilities from simple to increasingly complex.
>     - A cockroach that runs away from a fire does have some intelligent abilities.
>     - A computer that plays chess but does not understand fires has other abilities.
>     - Both have some abilities that count as "intelligence," without having the full spectrum of human intelligence.
>     - But this can also be said of humans (for example, not all humans play chess; not all are as intelligent as Einstein; and so on).

