
---
title:  "AI and Society: 24. War Robots and Arkin’s Ethical Governor"
author: Andreas Matthias, Lingnan University
date: November 25, 2019
...

# Arkin's "Ethical Governor"

## Arkin's "Ethical Governor"

In the discussion about the moral control of autonomous machines, one of the most influential proposals is Ronald Arkin's "Ethical Governor" (2009).

For example:

- Arkin, R. (2009). *Governing lethal behaviour in autonomous robots.* Boca Raton, London, New York: CRC Press.
- Arkin, R., Ulam, P., & Duncan, B. (2009). *An ethical governor for constraining lethal action in an autonomous system.* Tech. Report No. GIT-GVU-09-02, GVU Center, Georgia Institute of Technology.
- Arkin, R.C. (2007). *Governing lethal behavior: Embedding ethics in a hybrid deliberative/reactive robot architecture.* Technical Report GIT-GVU-07-11, Mobile Robot Laboratory, College of Computing, Georgia Institute of Technology.

## Read the paper

See “24-arkin-constraining-lethal-action.pdf” in Moodle!

## Capabilities and goals

>- The ethical governor is supposed to be “capable of restricting lethal action of an autonomous system in a manner consistent with the Laws of War and Rules of Engagement” (Arkin et al., 2009)
>- The goal of the architecture is “to ensure that these systems conform to the legal requirements and responsibilities of a civilised nation.”
>- The governor “is a transformer/suppressor of system-generated lethal action to ensure that it constitutes an ethically permissible action, either nonlethal or obligated ethical lethal force.” (Arkin et al., 2009)

## Points addressed

The main points the Ethical Governor is trying to address:

1. Discrimination.
2. Proportionality.

*Can you explain these two?*

## Discrimination

>- In the Just War Theory, "discrimination" means to be able to distinguish civilians from combatants.
>- While it is considered morally acceptable to target combatants in a war, the fighting parties should not be allowed to target civilians.
>- *Any comments on this?*
>     - How to distinguish? (factory-working women in WW2 Germany, "terrorist" fighters, suicide bombers)
>     - Is this the only useful distinction? What about a soldier on leave? A soldier taking a break?
>     - Who defines what a civilian is? Each party for itself? The enemy? International conventions?

## Proportionality

>- In Just War Theory, "proportionality" means that the damage caused by a military action should be "proportional" to the expected gain from that action.
>- Violence should not be excessive, but used to the minimum amount necessary to reach a particular goal.
>- *Comments?*
>     - *Whose* gain is looked at in order to justify an act of violence? The attacker's? The victim's? Humanity's in general?
>     - Who defines what goal is a worthy goal?
>     - How to judge necessity? Were the atomic bombs in Hiroshima and Nagasaki really necessary? Who defines necessity and according to whose values?
>     - Was the trade-off of the atomic bombs a good one? (Essentially exchanging dead Japanese for dead Americans)
>     - Can there ever be "proportionality" for intentionally killing people? (Kant!)

## Morality of war

A few conclusions:

>- It's important to be aware of the fact that, when we talk about the morality of war robots, we already make a series of assumptions that morally justify war and killing under particular circumstances.
>- We distinguish between "good" war and "bad" war, "good" killing and "bad" killing.
>- Still, we are often not consciously aware of the criteria that we use to distinguish "good" from "bad" killing.
>- Often they favour our own culture, values, or interests, over the enemy's.
>- But how can such a bias be justified?
>- Keep these points in mind for the following discussion.

## What is a "governor"? (1)

James Watt: Centrifugal governor (negative feedback controller):

![](graphics/08-governor.png)\




## What is a "governor"? (2)

>- “The term governor is inspired by Watts’ invention of the mechanical governor for the steam engine, a device that was intended to ensure that the mechanism behaved safely and within predefined bounds of performance. ... The same notion applies, where here the performance bounds are ethical ones.” (Arkin, 2007)

## What is a "governor"? (3)

>- When the engine goes too fast, the two rotating balls at the top of the centrifugal governor will move upwards, and they will pull on a lever that will close the steam valve, thus causing the machine to go slower.
>- When the engine is too slow, the balls will drop, and the valve will open, causing the engine to move faster.
>- This is a negative feedback loop that keeps the engine working withing a specified speed limit.

## Similar approaches to robot morality

>- Arkin’s is not the only concept aiming at an algorithmic moral control of robots.
>- If we look at *civilian* robot use, we find more work addressing the moral control of autonomous robots.
>     - Cloos (2005) discusses the Utilibot project, a proposal for robot control supposedly based on utilitarianist principles.
>     - Lucas and Comstock (2011) argue for a variant of utilitarianism (satisficing hedonistic act utilitarianism, SHAU) in robot control.
>     - Anderson and Anderson (2008) on the other hand, argue against utilitarianist approaches and in favour of moral robot control based on a concept of prima facie duties.

## Technical description

>- "Technically, the ethical governor is a constraint-driven system, which, on the basis of predicate and deontic logic, tries to evaluate an action, ... by satisfying various sets of constraints, like $C_{forbidden}$, $C_{obligate}$ and so on."
>- Every constraint is a data structure which has a type (e.g. “prohibition”), an origin (“laws of war”), and a logical form (“TargetDiscriminated AND TargetWithinProximityOfCulturalLandmark”)

## Architecture

![](graphics/08-arkin-screenshot-1.png)\ 




## Constraint data type

![](graphics/08-arkin-constraints-1.png)\ 




## Constraint example

![](graphics/08-arkin-constraints-2.png)\ 




## Origins of constraints

![](graphics/08-arkin-constraints-4.png)\ 




## Constraints application algorithm

![](graphics/08-arkin-constraints-3.png)\ 




## Military necessity overrides constraints

![](graphics/08-arkin-necessity.png)\ 





## Criticism of Arkin's Ethical Governor

*You got an idea of how the Ethical Governor is supposed to work. How can the concept be criticised?*

## Criticism: Interest conflict (1)

>- Watt’s governor: the operator of the machine has an interest in its safe operation, and the governor helps him achieve that. 
>- Even if the operator would like the steam engine to work with more power, or at a higher speed than it was designed for, it would make no sense for him to override the centrifugal governor, since doing so would just destroy the machine without achieving any higher efficiency of its operation. 
>- The steam governor, as designed and constructed by the steam engine’s designer, therefore acts in the best interests of the engine’s operator at run-time.

## Criticism: Interest conflict (2)

>- This is not so with the ethical governor.
>- The designer of the ethical governor has the aim of implementing a device which will limit the possible actions of an autonomous war robot to a set of morally permissible  actions (assuming, for the moment, that the latter set can be clearly defined at all).
>- The operator of the war robot, on the other hand, has a conflicting interest: that of achieving the maximum tactical efficiency of the machine, and of carrying out a military operation successfully, thus achieving a predefined set of mission objectives.
>- The operator of the machine (the commanding officer for the particular operation) will therefore have an incentive to override the constraints imposed by the ethical governor, in order to achieve a better military result.

## Criticism: Interest conflict (3)

>- Arkin et al. (2009): permissible collateral damage is a *function of military necessity alone* (see image above).
>- Since the designer of the governor is the same as its operator (in both cases the same military hierarchy), it would be irrational to expect that the governor would be designed so as to act against military interests.

## Criticism: Democratic control (1)

> “Code is an efficient means of regulation. But its perfection makes it something different. One obeys these laws as code not because one should; one obeys these laws as code because one can do nothing else. There is no choice about whether to yield to the demand for a password; one complies if one wants to enter the system. In the well implemented system, there is no civil disobedience.” (Lessig, 1996) 

## Criticism: Democratic control (2)

At the same time, the code which both requires and enforces perfect obedience, is itself removed from view:

> “The key criticism that I’ve identified so far is transparency. Code-based regulation -- especially of people who are not themselves technically expert -- risks making regulation invisible.” (Lessig, 2006, 138)

## Criticism: Democratic control (3)

>- Both Laws of War and Rules of Engagement are publicly visible and democratically approved documents, regulating in an open and transparent way a nation’s forces’ behaviour at war.
>- These documents are accessible both to the public which, in the final instance, authorizes them, and to the soldiers, whose behaviour they intend to guide. 

## Criticism: Democratic control (4)

>- Things change when Laws of War and Rules of Engagement become software.
>- Words, which for a human audience have more or less clear, if fuzzily delineated meanings, like "combatant" or "civilian," need to be “codified,” that is, turned into an unambiguous, machine-readable representation of the concept they denote. 
>- This interpretation cannot be assumed to be straightforward for various reasons.
>- *Can you guess?*

## Criticism: Translation problems (1)

>- *When is a hammer "ready to hand?" (Heidegger).*
>- When it is ready for me to use it for my purposes.
>- Readiness-to-hand as well as Dasein, cannot be expressed adequately by sets of “objective” properties at all (Dreyfus, 1990).
>- Whether, for instance, a hammer is “too heavy” for use is not translatable into one single, numerical expression of weight:
>     - The hammer’s “unreadiness to hand” will vary not only across different users,
>     - but also depending on the time of day,
>     - the health status and the mood of the user,
>     - and perhaps even the urgency of the task towards which the hammer is intended to be used.

## Criticism: Translation problems (2)

>- Arkin's concept, being based on a naive symbolic representation of world entities in the machine's data structures, does not even try to acknowledge this problem.
>- Bruno Latour (2009), Terry Winograd (1991): The utilisation of an artefact always involves a process of *translation.*
>- Winograd (1991): Crucial but often overlooked shifts in the meaning of words as they are translated from everyday, context-rich human language into an algorithmic, context-free, “blind” representation.
>- These translation processes do crucially alter the meaning of the words and concepts contained in the Laws of War and Rules of Engagement.

## Criticism: Translation problems (3)

>- But whereas these documents have been the object of public scrutiny and the result of public deliberation, their new, algorithmic form, and which is far from being a faithful translation, has been generated behind the closed doors of an industry laboratory, in a project which, most likely, will be classified as secret.
>- What reaches the public and its representatives will most likely be not the code itself, but advertising material promoting the machine in question and the features which its manufacturer wishes to highlight.
>- If the precise morally relevant rule content of a “governor”-like system is made available at all, it will most likely be in a back-translated form, *not as actual code,* but as Rules of Engagement or Laws of War, thus *hiding the very translation* which is the problem the public should be able to examine and to address.

## Criticism: Translation problems (4)

>- Whether the technology actually does what it purports to do depends upon its code (Lessig, 2006).
>- And if that code is closed, the moral values and decisions that it implements will be removed from public scrutiny and democratic control.

## Criticism: Collective agency and goal translation (Bruno Latour)

>- Latour: The use of an artefact by an agent changes the behaviour of *both* the agent and the artefact.
> - Not only the gun is operating according to the wishes of its user.
> - Rather, it is in equal measure the gun that forces the user to behave *as a gun user.*
>     - forces him to assume the right posture for firing the gun,
>     - to stop moving while firing,
>     - to aim using the aiming mechanism of the gun... and so on.

## Bruno Latour: Goal translation (1)

> - Even more importantly, having a gun to his disposal, will change the user's *goals* as well as the methods he considers in order to achieve these goals (avoid or confront a danger).
>- The "composite agent" composed of *myself and the gun* is thus a different agent, with different goals and methods at his disposal, than the original agent (me without the gun) had been.
>- This might be the translation of an originally aimed for goal into another, because the architecture and capabilities of the machine differ from that of a human operator and thus cause the “collective entity” of the human operator (or programmer) together with the machine to select a goal more appropriate to the new set of capabilities of that collective entity.

## Bruno Latour: Goal translation (2)

>- A good example might be that of a human soldier who might seek cover in case he is being shot at.
>- The machine, not fearing injury, would instead shoot back.
>- Assisted in targeting by the enemy fire itself, the machine would be able (and thus expected) to inflict lethal damage where the human soldier would perhaps seek to proceed more cautiously, to negotiate, to retreat, or to employ a whole array of possible other, non-lethal options.

## Criticism: Is discrimination morally relevant? (1)

>- Discrimination, in this context, means the ability of a machine which is engaged in a battle to distinguish reliably:
>     - between friend and foe; and
>     - between legitimate and illegitimate targets of violent military action.
>- Discrimination therefore, as a cognitive operation, is an instance of classification.
>- The persons perceived by the machine as present inside its radius of action are sorted into any number of categories, for example combatants, non-combatants, attackers, bystanders, dangerous, harmless and wounded persons. 

## Criticism: Is discrimination morally relevant? (2)

>- Although such classification is a *necessary* condition for moral action, morality cannot be said to be *identical* to making these categories; it presupposes them.
>- Predicates like “morally right” and “morally wrong” cannot be applied to persons or to classes of persons.
>- Discrimination is, like other instances of automated machine classification, a purely technical problem, which can be successfully solved by engineering means.
>- Concentration of attention to the issue of discrimination (that is solvable by engineering means) serves primarily to distract from the real moral issues (that may be not).

## Criticism: Is discrimination morally relevant? (3)

>- “The person standing beside the tree T is an enemy soldier” (an instance of discrimination) is a straightforward classification result, not a moral issue. 
>- Although all classification has already been successfully completed in this sentence (the machine knows that Y is an enemy solder), the moral problems are not yet resolved.
>- “It is morally right for robot X to kill the enemy soldier Y beside the tree T” is, in fact, a statement which includes a genuine moral evaluation.
>- This evaluation cannot be tackled with the engineering apparatus available for classification problems.

## Criticism: Is discrimination morally relevant? (4)

>- On the other hand, the question whether it is morally right or not for the machine to kill the enemy soldier Y requires insight
>     - into the nature of life and death,
>     - into the question whether enemy soldiers in the present conflict deserve death,
>     - and whether the machine is morally permitted to apply lethal force against this particular human.

## Criticism: Is discrimination morally relevant? (5)

>- This might involve a deliberation:
>     - about the aims of the present war,
>     - about the moral justification of lethal force in this conflict,
>     - about the moral rightness of the machine’s operator’s cause,
>     - and about available alternatives to the application of lethal force.
>- The ethical governor, as described by Arkin, does none of these.

## Criticism: Feedback morality? (1)

>- Negative feedback controllers (like the “ethical” governor) work under the following assumptions:
>     - There is a scalar value to control. The value can be accurately measured at any point in time.
>     - A numerical deviation between the desired (target) and the measured (actual) value of this variable can be calculated.
>     - This will be used in determining the direction (sign) and the strength of the corrective action.
>     - Application of the corrective action using a suitable direction and strength will bring the value that is controlled closer to the target value.
>     - This means that the effect of the corrective action must be predictable and it must be expressible as a decrease of the measured deviation.
>     - (A chaotic system, for example, cannot be regulated with a feedback controller.)

## Criticism: Feedback morality? (2)

>- Crucial question: Is morality a matter of correcting the deviation from a target value by applying a corrective action which is proportional in strength to the deviation?
>- If so, what is the target or reference value?

## Criticism: Finding the reference value

>- We cannot expect warring parties to share a common moral framework (Islamic vs Christian morality)
>- Arkin, confronted with this problem, attempts to substitute *other sets of rules* for the missing set of unambiguous and universally accepted morality.
>- Arkin: “... an autonomous robotic system architecture potentially capable of adhering to the International Laws of War (LOW) and Rules of Engagement (ROE) to ensure that these systems conform to the *legal* requirements and responsibilities of a civilized nation. [The ethical governor] is a transformer/suppressor of system-generated lethal action to ensure that it constitutes an *ethically permissible* action, either nonlethal or obligated ethical lethal force.” (Arkin et al., 2009, 1)
>- *Observe how Arkin switches from "legal" to "ethical"! Are these the same???*

## Criticism: Problems with LOW/ROE (1)

>- Rules of Engagement, being rules which are issued by an army for the use and benefit of its own soldiers, are not necessarily acceptable to *all* parties in an armed conflict. 
>- It is absurd to assume that rules which have been issued and are accepted only by one side in a conflict, should ensure ethical action in a way which is supposed to be acceptable to the opponent.

## Criticism: Problems with LOW/ROE (2)

>- If this were the case, it would imply that the US military is actively concerned to issue rules that counteract its own tactical interests in favour of moral principles which are compatible with the morality of the enemy.
>- The very aim of battlefield action, which is to score a victory over the enemy, is in direct conflict with this idea. 
>- What Arkin is advocating here is a kind of moral imperialism, based on the principle that the party which has the robots is therefore entitled to unilaterally prescribe the moral rules which come into play when these weapons are deployed.

## Criticism: Problems with LOW/ROE (3)

>- The Laws of War and Rules of Engagement as cited by Arkin are full of contradictions.
>- For example, “individual civilians, the civilian population as such and civilian objects are protected from intentional attack” (Arkin, 2007, 26), but a legitimate military target would be “enemy civilian aircraft when flying (i) within the jurisdiction of the enemy [...]” (Arkin, 2007, 25). 
>- If the “jurisdiction of the enemy” includes the area controlled by their air traffic control authorities, then it is hard to see how an “enemy civilian aircraft” could possibly avoid to be declared a legitimate target. 
>- Or: “In general, any place the enemy chooses to defend makes it subject to attack,” (Arkin, 2007, 24) including cities and any civilian installations.

## Criticism: Problems with LOW/ROE (4)

>- Standing Rules of Engagement: “[Necessity for military action arises] when a hostile act  occurs or when a force or terrorists exhibits hostile intent” (Arkin, 2007, 32). 
>- Observe how here the line between combatants and non-combatants is obscured by the use of the word “terrorists,” which is meant to legitimise attacks against non-uniformed and possibly unarmed persons, who, according to the international Laws of War, would be considered civilians and thus *not* legitimate targets.
>- Also, the criterion for a military action is lowered to the exhibition of “hostile intent,” leaving unspecified what is supposed to mean in particular and how the machine is supposed to go about identifying “hostile intent” by a non-uniformed (!) “terrorist” enemy.

## Criticism: Problems with LOW/ROE (4)

>- Even the most concrete and specific samples of rules which Arkin proposes to use are unclear, contradictory, and open to endless interpretation. 
>- This interpretation must be either performed by a human controlling the war robot (which would render the whole concept of the ethical governor obsolete), or by the machine itself.
>- This would require both unavailable factual knowledge (for example about the intentions of enemies) and powers of natural language disambiguation and practical wisdom that are currently not available in any machine.

## Criticism: Dissent and conscience (1)

>- But even following shared, generally accepted rule sets alone does not describe what we understand to be morality.
>- *What more is needed?*

## Criticism: Dissent and conscience (2)

>- Our common understanding of moral behaviour rests on two pillars:
>- 1. A shared set of moral rules, and
>- 2. Acting in accordance with one’s deepest conviction about what is right and wrong (“conscience,” or moral autonomy). 
>- If an agent is not free to act following his convictions, then we usually would not consider him a fully responsible moral agent.
>- If, for example, a soldier is ordered to perform a morally praiseworthy action, we would not ascribe the full amount of moral praise to the soldier himself, but to those who issued the command.

## Criticism: Dissent and conscience (3)

>- Perfect obedience is not moral action.
>- Moral action requires moral autonomy, or the capability for disobedience.
>- A machine controlled by a (fictional) ethical governor would be perfectly obedient.
>- Thus, it would not fulfill the criteria for moral action.

## Criticism: Dissent and conscience (4)

>- On the negative side, this also means that such a machine provides no safeguard against grossly immoral commands by the programmers or its superiors.
>- These superiors are able to override the ethical governor's suggestions (and we saw above that this is exactly what the governor allows them to do). 
>- With human soldiers there is always the possibility of dissent, of the soldier recognising the immorality of a command and refusing to act on it.
>- There are many stories, from all wars, where such acts were seen as morally highly praiseworthy.
>- With machines we have a guarantee of perfect obedience, which also means that immoral commands will be executed without any final moral deliberation in the form of a soldier's conscience coming into play.

## Criticism: Dissent and conscience (5)

>- Arkin (2007, 76): “On a related note, does a lethal autonomous agent have a right, even a responsibility, to refuse an unethical order? The answer is an unequivocal yes.”
>- Arkin (p.9): “I personally do not trust the view of setting aside the rules by the autonomous agent itself, as it begs the question of responsibility if it does so, but it may be possible for a human to assume responsibility for such deviation if it is ever deemed appropriate (and ethical) to do so.”
>- Unfortunately, these two statements contradict each other. 


## Summary: Is the ethical governor a good idea? (1)

>- One could argue that even a limited moral control of war robot actions is better than none, and that, therefore, the ethical governor is still a useful and beneficial device.
>- But, first: As has been shown above, the ethical governor does not lead to the machine acting “ethically,” but only (in the idealised optimum of its performance) in accordance with the Geneva and Hague conventions and the Rules of Engagement of the deploying military system, and this only as long as these rules do not interfere with the machine's military objectives.

## Summary: Is the ethical governor a good idea? (2)

>- Second: The Rules of Engagement, being issued by the military itself, are not in any sense of the word “moral” rules, but rules made by one side in a conflict for its own benefit. 
>- They are often phrased in a way which leaves them open to extensive interpretation and makes them unsuitable as guidelines for moral action.

## Summary: Is the ethical governor a good idea? (3)

>- Third: The ethical governor, giving only suggestions, can be overridden at any time by the commanding officers in charge of the machine’s operation.
>- Since the ethical governor is designed and implemented by the same military hierarchy which deploys the robot, creates a fundamental conflict of interest.
>- In this conflict, the ethical governor will naturally always have a lower priority than the military objectives which motivated the creation and the deployment of the war robot in the first place.

## Summary: Is the ethical governor a good idea? (4)

>- Fourth: The ethical governor, being a “closed-code” implementation of moral principles, is removed from public scrutiny and democratic control. 
>- This problem can only be addressed by requiring the actual ethical governor code to be open-sourced, so that government and the public can be involved in the necessary and inevitable translation process of fuzzy, human terms into context-free, semantically unambiguous, computerised ones.

## Summary: Is the ethical governor a good idea? (5)

>- Fifth: The ethical governor is in principle only able to deal with a simple, conflict-free subset of rule-based ethics, since it lacks all mechanisms which are commonly assumed to be necessary for resolving moral rule conflicts:
>     - phronesis,
>     - moral intuition,
>     - or an understanding of human preferences and the utilitarian value of specific consequences.
>- But this “toy ethics” is not sufficient to resolve real-world moral problems on the battlefield, which typically involve conflicting options about questions of life and death, of justified causes, of retribution and retaliation, and of culture-specific ethics codes.

## Summary: Is the ethical governor a good idea? (6)

>- Sixth: An ethical governor lacks autonomy as a key ingredient of moral agency, and is thus incapable of dissent as a last line of protection against immoral robot deployment.
>- Despite all this, the ethical governor is promoted by its developers as a step towards the creation of autonomous, morally acting machines.

## Summary: Is the ethical governor a good idea? (7)

>- As a consequence of proposing the "ethical governor" and similar systems, it is increasingly accepted that humans move “out of the loop” of war robot control, based on the (mistaken) premise that moral behaviour can be implemented into the machine itself.
>- This leads to the public acceptance of increased deployment of autonomously acting war robots, which will, in reality, not be able to act morally right in any significant sense of the word.
>- The misconception about the capabilities and aims of the ethical governor could therefore be argued to be misleading and more dangerous than the absence of such a device (and the resulting placement of humans in morally critical places of control) would be.

