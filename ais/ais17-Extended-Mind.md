---
title:  "AI and Society: 16. Extended Mind and Hybrid Agents"
author: Andreas Matthias, Lingnan University
date: October 16, 2019
bibliography: papers.bib
csl: apa.csl
...

# Extended Mind and Hybrid Agents

## Otto's notebook

>- Otto suffers from Alzheimer’s disease.
>- Otto carries a notebook around with him everywhere he goes.
>- When he learns new information, he writes it down.
>- When he needs some old information, he looks it up.

. . .

> “Otto decides to see an exhibition at the Museum of Modern Art. He consults the notebook, which says that the museum is on 53rd Street, so he walks to 53rd Street and goes into the museum.

. . .

> For Otto, his notebook plays the role usually played by a biological memory.”

[@clark1998extended 33; page numbers cited after @menary2010extended]

## Tetris

![](graphics/tetris-small.jpg)\ 


>- Kirsh and Maglio [-@kima1994]: the physical rotation of a shape through 90 degrees takes about 100 milliseconds, plus about 200 milliseconds to select the button.
>- To achieve the same result by mental rotation takes about 1,000 milliseconds.
>- "Physical rotation is used not just to position a shape ready to fit a slot, but often to help determine whether the shape and the slot are compatible." [@clark1998extended]

## Epistemic actions

This kind of shape rotation it Tetris is an "epistemic action."

“Epistemic actions alter the world so as to aid and augment cognitive processes such as recognition and search. Merely pragmatic actions, by contrast, alter the world because some physical change is desirable for its own sake.”

>- Epistemic action demands spread of epistemic credit.
>- If a part of the world functions in the same way as a cognitive process, then that part of the world *is* a cognitive process.
>- "Cognitive processes ain’t (all) in the head!” [@clark1998extended]

# Criteria for a “mind”

## Portability

>- According to Clark and Chalmers, the mind must be “portable,” that is, its basic, core functions must always be available to the subject, no matter what the environment looks like.
>- One does not lose one’s mind in the desert or when one is walking on a beach, away from a desk and an Internet connection.
>- Therefore, such “easily decoupled” systems would not count as mind.

## Reliability

>- Similar to portability is reliability. 
>- We expect our cognitive resources to be *reliably* available.
>- If I can easily lose access to a calculator, for example, then the calculator should not be considered part of my mind.
>- But what if the calculator was built into my brain and always available?


# Seven versions of the claim

## 1. Causal coupling and cognitive equivalence

> “In these cases \[Tetris, notebook\], the human organism is linked with an external entity in a two-way interaction, creating a coupled system that can be seen as a cognitive system in its own right.” (p.29)

## 2. Epistemic actions

> “Epistemic actions alter the world so as to aid and augment cognitive processes such as recognition and search.” (p.28)

## 3. Use of external computing resources

> “Consider the use of pen and paper to perform long multiplication …, the use of physical rearrangements of letter tiles to prompt word recall in Scrabble … , and … language, books, diagrams, and culture.” (p.28)

## 4. Extended beliefs

> “What makes some information count as a belief is the role it plays, and there is no reason why the relevant role can be played only from inside the body.” (p. 35)

## 5. Extended desires

> “For example, the waiter at my favorite restaurant might act as a repository of my beliefs about my favorite meals (this might even be construed as a case of extended desire).” (p. 38)

## 6. Extended mental states

> “Could my mental states be partly constituted by the states of other thinkers? … In an unusually interdependent couple, it is entirely possible that one partner’s beliefs will play the same sort of role for the other as the notebook plays for Otto.” (p. 38)

## 7. Spread selves

> “Does the extended mind imply an extended self? It seems so … The information in Otto’s notebook ... is a central part of his identity as a cognitive agent. ... Otto himself is best regarded as an extended system, a coupling of biological organism and external resources.” (p. 39)

## Problems and criticisms

> * The coupling-constitution fallacy
>      - A thermostat is coupled to the room temperature, but the temperature does not *constitute* the thermostat. In the same way, the notebook is coupled to Otto's cognition, but not itself a cognitive process.
> * The "mark of the cognitive"
>      - The specifically "cognitive" property is the possession of underived mental representations. Extended "minds" don't have any, so they are not minds.
> * Cognitive equivalence
>     - If a system acts "as if" it was a cognitive system, then it is one. This is a functionalist position that can be criticized in various ways.

# The hybrid agent (Bruno Latour)

## Hybrids

>- Except for a few, rare cases, we won't get autonomous machines that operate independently of humans, in a vacuum of their own.
>- Usually, autonomous agents will be machines that cooperate and interact closely with humans in order to perform their function:
	- a driver and an autonomous car
	- a hiker and Google Maps
	- a speaker, a listener, and Google Translate
	- a soldier and a drone
	- a shopper and Amazon's Alexa
	- a policeman and a law-enforcement robot
	- a doctor, a nurse, a patient, and a number of hospital robots

## Classic (wrong) concept

Let's look closer at how this works.

![](graphics/human-uses-artefact.png)\


>- The human is the *agent.*

## Classic (wrong) concept

![](graphics/human-uses-artefact-2.png)\


- The human is the *agent.*
- It is in his mind that the decision to act is taken and the action plan is formed.

## Classic (wrong) concept

![](graphics/human-uses-artefact-3.png)\


- The human is the *agent.*
- It is in his mind that the decision to act is taken and the action plan is formed.
- After that decision has been taken, the human agent uses the artefact in the preconceived way to achieve his goal.


## Classic (wrong) concept

![](graphics/human-uses-artefact-3.png)\


>- *This is possibly incorrect.*
>- Bruno Latour: The use of an artefact by an agent changes the behaviour of *both* the agent and the artefact.

## Bruno Latour: Composite agents [@blhn]

![](graphics/human-uses-artefact-4.png)\


>- Not only the gun is operating according to the wishes of its user.
>- Rather, it is in equal measure the gun that forces the user to behave *as a gun user.*
>     - forces him to assume the right posture for firing the gun,
>     - to stop moving while firing,
>     - to aim using the aiming mechanism of the gun... and so on.

## Bruno Latour: Composite agents [@blhn]

![](graphics/human-uses-artefact-5.png)\


Even more importantly, having a gun to his disposal, will change the user's *goals* as well as the methods he considers in order to achieve these goals (avoid or confront a danger).

## Bruno Latour: Goal translation

![](graphics/human-uses-artefact-6.png)\


>- The "composite agent" composed of *myself and the gun* is thus a different agent, with different goals and methods at his disposal, than the original agent (me without the gun) had been.
>- It's not any more "me using the gun."
>- Composite agent $\xrightarrow[considering\ collective\ properties]{using\ collective\  resources}$ Goal (translated)
>- My options to make a moral choice are constrained (and sometimes determined) by the properties of the composite system.

## Bruno Latour: Goal translation

Examples:

>- Original goal: Make peace with enemy to minimise casualties
>- Available tool: Drones that can kill without endangering our soldiers.
>- Goal after translation: Bomb enemy with drones.

. . .

>- Original goal: Drive home safely, don't drink beer before driving.
>- Available tool: Tesla autopilot that effectively works (even if illegal without supervision).
>- Goal after translation: Drive home drunk and nap in the car.

## Artefact design

>- The design and properties of the artefact in composite agents determine my options to act.
>- The design of the artefact (and how I can use it in the pursuit of my goals) determine:
>     - The amount of *control* I have over the artefact.
>     - The degree to which I can be held responsible for the collective action (because responsibility requires effective control over the action!)
>     - The extent to which the artefact will encourage or require a translation of my original goals to the capabilities of the composite agent.
>- Thus: **The design of the artefact in composite agents becomes morally relevant.**


# Epistemic credit and hybrid agents

## Epistemic credit and hybrid agents

>- The extended mind thesis is a thesis about the externalised, shared execution of an algorithm, which leads to shared agency.
>- *Hybrid agents:* Computational aggregates, that are composed of internal and external parts, which jointly achieve cognitive results that lead to them acting in a particular way as agents.
>- Hybrid agents are not multiple, cooperating agents. Their components fuse into *one* agent.


# The epistemic disadvantage of humans

## Epistemic disadvantage

>- Supervising a child or a dog is easy. Why?
>- *My* ability to understand the likely outcomes of my actions and to predict the behaviour of the world around me is more developed than that of the child or animal I am supervising.
>- The *epistemic advantage* I have over the supervised agent makes me a suitable supervisor.
>- I have a greater degree of control over the interactions of the supervised agent with his environment than he would have if left to his own devices.

## Epistemic advantage, supervision, and responsibility

>- By knowing more than the child, I am in a position to foresee harm both to the child and to the environment, and thus to avert it.
>- This higher degree of control also bestows on me a higher responsibility: commonly parents as well as dog owners are held responsible for the actions of their children and pets, as long as they have been supervising them at the moment the action occurred.

## Hybrids of humans and non-humans

>- Is it the same when supervising a computational hybrid agent?
>- In computational hybrids of humans with non-humans, it is often the *human* part who is at an *epistemic disadvantage.*
>- At the same time, it is the *human* whom we traditionally single out as the bearer of responsibility for the actions of the hybrid system.

## Epistemic disadvantage of humans in hybrid agents

Supervising a nuclear power plant:

>- As opposed to supervising a dog, I have to rely on artificial sensors (radioactivity, high temperatures, high pressures) without which I am unable to receive crucial information.
>- Without artificial sensors and artificial computational devices I am not able to control the nuclear power plant *at all.*

## Computational externalism and epistemic disadvantage (1)

>- I and the control devices of the power plant constitute a hybrid agent.
>- Control of the power plant is obtained through the execution of an algorithm, but this algorithm cannot be executed by the human brain and its sensors alone.
>- Additional sensors are necessary (for example, radioactivity, or high temperature sensors), but also high-speed computations, that make it possible to coordinate the various subsystems of the power plant in real time (see Clark&Chalmers Tetris example!)
>- Part of the algorithm that controls the power plant is *necessarily* executed outside of human bodies, and therefore *no human can be said to be "controlling" the power plant.*
>- The human *together* with the computers and sensors and switches is in control. The hybrid agent is.

## Computational externalism and epistemic disadvantage (2)

The phenomenon is common:

- Control systems of air planes and air traffic
- Deep space exploration devices
- Military self-targeting missiles
- Internet search engines

>- There is no way a human could outperform or effectively supervise such machines.
>- He is a slave to their decisions
>- He is physically unable to control and supervise them in real-time. 

## Epistemic disadvantage and responsibility

A common misconception when talking about hybrid agents:

"Humans should exercise better control over the actions of the non-human part, supervise it more effectively, so that negative consequences of the operation of the hybrid entity can be seen in advance and averted."

. . .

>- Responsibility for a process and its consequences can only be ascribed to someone who is in effective *control* of that process
>- Ascribing responsibility to agents who are in a position of epistemic disadvantage is not just, and poses problems of justification.

# Examples

## Self-driving car

>- Example: Tesla website (November 2019):
>     - “Eight surround cameras provide 360 degrees of visibility around the car at up to 250 meters of range.”
>     - “Twelve updated ultrasonic sensors complement this vision, allowing for detection of both hard and soft objects.”
>     - “A forward-facing radar with enhanced processing provides additional data about the world on a redundant wavelength that is able to see through heavy rain, fog, dust and even the car ahead.” (https://www.tesla.com/en_HK/autopilot)
>- You can see how this could be superior to the senses of a human driver under particular circumstances (rain, fog, dust, blind spots).
>- Can a human driver really be responsible for the car’s actions under conditions where the car clearly has to know better?

## Boeing MCAS system (1)

>- “Maneuvering Characteristics Augmentation System.”
>- Designed to make some newer Boeing 737 models behave like older models, so that pilots can fly them without having to be specially trained for the new model.
>- The MCAS also is meant to prevent pilots from putting the plane into a dangerous flight configuration.
>- Essentially, the pilot’s steering inputs don’t directly control the plane, but they first go into a computer and the computer decides how to translate the pilot’s input into movements of the plane’s aerodynamic surfaces.

## Boeing MCAS system (2)

>- One of the functions of the MCAS is to push the aircraft’s nose down when the airspeed is too low, in order to make the airplane gain more speed.
>- As opposed to a system that just assists the pilot, the MCAS has the full authority to bring the aircraft nose down, and can not be overridden by the pilot.
>- The MCAS led to two crashes: Lion Air Flight 610 (October 2018) and Ethiopian Airlines Flight 302 (March 2019). 

## Boeing MCAS system (3)

>- In both cases, it seems that the sensors that told the MCAS how fast the plane was flying and how high its nose was pointing, were giving wrong values to the system. 
>- The MCAS, thinking that the plane’s nose was too high (while it wasn’t) pushed the nose down, causing the plane to crash.
>- In principle, the pilot could cut electricity to the plane’s tail controls, thus deactivating all automatic control of the plane’s angle. But pilots were never told how to do that, and the MCAS was not mentioned or explained in the official Boeing training manuals.

## Boeing MCAS system (4)

>- *So, who is responsible for the crashes?*
>     - Boeing installed a potentially dangerous system that overrides pilot input and that cannot be switched off easily.
>     - Boeing did not inform the airlines or the pilots how to deal with the new system in cases of malfunction.
>     - Most airlines did not order additional indicators (that would have cost more money and that were optional) that could have alerted the pilots to the problem.
>     - The pilots could have reacted better by disabling the horizontal stabiliser power supply and disabling the MCAS.
>     - Some pilots were able to come up with this solution on other flights (that did not crash). The pilots on the crashed flights did not. (<https://www.theguardian.com/world/2019/mar/20/lion-air-pilots-were-looking-at-handbook-when-plane-crashed>)

## Responsibility for accidents involving autonomous systems

>- This shows how difficult it is to ascribe responsibility for the failure of complex, computerised, autonomous systems. The manufacturer, the designer, the owner and the operator all share responsibility for accidents to some extent.
>- The same is true of Tesla car crashes on autopilot mode:
>     - Partially, accidents can be attributed to errors in programming caused by Tesla.
>     - Partially, they are the fault of the car owners, who use autopilot without staying alert on the car’s wheel.
>     - But: can we even expect humans to stay alert in a car that drives itself, without doing anything? It is very difficult to keep one’s concentration on the road if one isn’t actually controlling the car. 
>     - So here’s the failure of the designer to design the system so that it keeps the driver alert, instead of luring him into believing that he is safe.

## Overconfidence in autonomous technologies

>- Overestimation of what the autonomous system can do (and the resulting overconfidence in the system’s performance) is one of the greatest problems with autonomous technologies.
>- On the one hand, companies want to sell the autonomous product, which causes them to exaggerate its abilities.
>- On the other hand, we want the users to be conscious of the product’s limitations and to not use it where it cannot perform safely.
>- This is a conflict that seems unavoidable. The only way out is very strict state regulation of autonomous technologies and how they can be advertised.



# Conclusions

## Conclusion (1)

>- The Extended Mind Thesis shows how the components of a hybrid agent can jointly execute *one* single act.
>- In this case it is impossible to ascribe credit or blame for the result to any particular subsystem of the joint agent.
>- Bruno Latour: "Using" an artefact is not a one-way action, but an interaction, a back-and-forth between agent and artefact, in which both have to change and to adapt, forming a new, compound agent.

## Conclusion (2)

>- Humans as part of hybrid agents cannot effectively control the agent:
>      - Because of epistemic disadvantage and limited access to sensory data and computational resources.
>      - Because of the lack of an incentive to dispute the machine's decisions (the "man-in-the-loop fallacy"). We will talk about this later (see discussion of Arkin).

## Conclusion (3)

>- Classic models of responsibility ascription operate under the mistaken assumption that there are agents and, separately, objects of agency, things that the agents make use of in executing their agency.
>- This might be wrong, particularly when we talk about hybrid agents where the cognitive algorithm is executed in part outside of the human component's body, that is, by the artefact.
>- In these cases, the agent and the extended components of his agency form a computational hybrid, *which cannot be any more separated into distinct parts for the purposes of responsibility ascription.*
>- A joint agent is one unit, one indivisible whole, and responsibility for its actions diffuses across the whole of it.

## Possible consequences for society

>- Accepting cognitive externalism might force us to acknowledge that artefacts are not passive tools of human autonomy.
>- They might crucially shape human intentions and options for action.
>- Actions performed by hybrid agents might require a new distribution of responsibility between the parts of the hybrid agent.
>- The idea of total human autonomy might, in the context of actions involving artefacts, be a fiction.

## The role of the designer

>- Designers of artefacts have influence on the decisions humans will take when using these artefacts (Latour).
>- Therefore, designers of such artefacts might share responsibility for the actions performed using the artefacts.
>- Artefact design might need to be legally regulated with these issues in mind.

## References
\footnotesize 

