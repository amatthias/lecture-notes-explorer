---
title:  "AI and Society: 18. Hybrots, Connectomes and Cyborgs"
author: Andreas Matthias, Lingnan University
date: November 1, 2019
bibliography: papers.bib
csl: andreas-matthias-apa.csl
...

# Artificial connectomes

## What is a connectome?

> A connectome is a comprehensive map of neural connections in the brain, and may be thought of as its "wiring diagram". More broadly, a connectome would include the mapping of all neural connections within an organism's nervous system. (Wikipedia)

## Connectome example: Caenorhabditis elegans

![](graphics/05-Celegans.jpg)\ 

- The C. elegans is a little worm (about 1mm in length).
- The male has 1031 cells and 302 neurons.
- Its genome has been sequenced completely, and it’s the only organism of which we have a complete neural map.

## Connectome example: C. elegans

A brief introduction to C. elegans:

<https://www.youtube.com/watch?v=zjqLwPgLnV0>

## Touch sensitivity

>- Gentle touch stimulus delivered to the body (Chalfie and Sulston, 1981; Sulston et al., 1975)
>- Harsh touch to the midbody (Way and Chalfie, 1989)
>- Harsh touch to the head or tail (Chalfie and Wolinsky, 1990)
>- Nose touch (Kaplan and Horvitz, 1993) and texture (Sawin et al., 2000).
>- (Source: http://www.wormbook.org/chapters/www_behavior/behavior.html#sec2)

## Nose behaviours

>- Animals respond to touch to the side of the nose by rapidly moving their nose away from the stimulus; this is called head withdrawal.
>- C. elegans responds to gentle touch to the nose by initiating backward locomotion. (Kaplan and Horvitz, 1993)
>- (Source: http://www.wormbook.org/chapters/www_behavior/behavior.html#sec2)

## Reaction to chemicals

>- The worm swims towards low salt concentrations, but away from high salt concentrations.
>- Its senses get used to salt, so it will later tolerate higher salt concentrations after having been for some time in an area with lower salt concentration.
>- (Source: http://www.wormbook.org/chapters/www_behavior/behavior.html#sec2)

## C. elegans driving robots

C. elegans neurorobotics:

<https://www.youtube.com/watch?v=YWQnzylhgHc>

## Human connectome project

Timeline:

- Phase I (2010-2012): research teams designed the project’s 16 major components.
- Phase II (2012-2015): the various scientists performed the actual work of gathering data.

Datasets are publicly available.

## Human connectome project

- <http://www.humanconnectomeproject.org/>
- <http://www.humanconnectomeproject.org/informatics/relationship-viewer/>

Data releases:

- <https://www.humanconnectome.org/study/hcp-young-adult/data-releases>

## Some outcomes

>- The connecting fibers are organized into an orderly 3D grid, where axons run up and down and left and right, without diagonals or tangles.
>- The brain’s layout is like a city, with its streets running in two directions and buildings’ elevators running up and down.
>- Strangely, in flat areas of the grid, the fibers overlap at precise 90 degree angles and weave together much like a fabric.
>- The resolution is not sufficient to see how individual neurons connect to each other.

## Some outcomes

>- Perhaps knowing how individual neurons connect is necessary to understand differences between individual people.
>- Strong relationship between positive behavior traits and the wiring of your brain. While some brains appear to be wired for a lifestyle that includes education and high levels of satisfaction, other brains appear to be wired for anger, rule-breaking, and substance use, according to the Oxford researchers.
>- Connectivity profiles are "intrinsic," acting "as a 'fingerprint' that can accurately identify subjects from a large group," regardless of how the brain is engaged during the scan.
>- Connectivity profiles even predicted levels of fluid intelligence and cognitive behavior.
>- (Source: <http://www.medicaldaily.com/10-faq-about-human-connectome-project-astonishing-sister-nihs-human-genome-project-362650>)

## Significance of these outcomes?

>- No detailed neuronal map of the brain yet (as opposed to C.elegans).
>- Prediction of intelligence (?) and behaviour traits.
>- Definition of intelligence? Problems of IQ tests?
>- Identification of neural infrastructure for rule-breaking and law enforcement?
>- Use of connectome data by dictatorial regimes?
>- Use by insurance companies?
>- Use by prospective marriage partners?
>- Use by banks giving study loans to students?

# Hybrots

## Hybrots

>- Rat Brain robot: <https://www.youtube.com/watch?v=1-0eZytv6Qk>
>- More complex rat brain robot: <https://www.youtube.com/watch?v=1QPiF4-iu6g>
>- A hybrot (short for "hybrid robot") is a cybernetic organism in the form of a robot controlled by a computer consisting of both electronic and biological elements. The biological elements are typically rat neurons connected to a computer chip.
>- Source: https://en.wikipedia.org/wiki/Hybrot

## Steve Potter’s hybrots

>- Steve Potter, a professor of biomedical engineering at the Georgia Institute of Technology.
>- Thousands of neuron cells are put onto a chip that contains 60 electrodes that read the neuron’s electrical activity.
>- The electrical signals are then converted and sent to a robot’s motors.
>- Every movement of the robot is the direct result of a signal from the neurons. The light sensors of the robot also send signals back to the neurons.

## Source

- Bakkum, D., Shkolnik, A., Ben-Ary, G., Gamblen, P., DeMarse, T., & Potter, S. (2004). Removing some ‘A’from AI: embodied cultured networks. Embodied artificial intelligence, 629-629.

(In Moodle as: Potter-Removing-A-from-AI-NOTES.pdf)

## Hybrots vs cyborgs

>- Cyborg: a human or animal that has “enhanced” capabilities by incorporating technological parts.
>- Hybrot: a new kind of creature that has a partly biological nervous system in an artificial, robot body.
>- Hybrots live longer than isolated neurons. Neurons separated from a living brain usually die after only a couple of months. In a hybrot, the same cells can live for up to two years. (https://en.wikipedia.org/wiki/Hybrot)

## Differences between connectome-based robots and hybrots

>- What are some morally significant differences between connectome-based robots and hybrots?
>- Connectome-based robots:
>     - Not real neurons.
>     - Mental states (for example, pain) are only simulated, not real.
>     - The animal’s brain is completely simulated, and does not deserve more protection than a character in a computer game.
>     - But is this true? What if we had a robot based on a complete human connectome, that reacts entirely human?
>- Hybrots:
>     - Real cells.
>     - Therefore, real mental (=neuronal) states.
>     - Real body, real neurons. Animal might have a claim to be protected, or to not be mistreated.

# Warwick: Cyborg morals, cyborg values, cyborg ethics

## Warwick: Cyborg morals, cyborg values, cyborg ethics

Warwick, Kevin (2003): Cyborg morals, cyborg values, cyborg ethics. Ethics and Information Technology 5: 131–137.

We will discuss this article in the following section.

## What is a cyborg?

>- Blind man with cane?
>- Hearing aids?
>- Glasses?
>- Wearable computers?
>- Cochlea implants, hip replacements, heart pacemakers?

## Repair vs enhance

>- Technologies to repair deficiencies vs. technologies to enhance human function.
>- The important dividing line here would be the “normal” level of human functioning.
>- Military technologies:
>     - Night (IR) vision in helmet.
>     - Fighter pilot helmets with weapon targeting aids built-in.

## Enhancements in the animal world

>- Spiders building webs?
>- Chimpanzee using a stick to reach a banana?
>- In both cases, only bodily function is enhanced, not cognitive ability!
>- Same with glasses, hearing aids etc.
>- The moral question becomes important when we talk about cognitive enhancement!
>- Direct link between environmental input and human brain, rather than through the senses.
>- Cyborg thus defined as “a unit formed by a human/machine brain/nervous system coupling.” (Warwick).
>- Only such cyborgs considered in the paper.

## Human autonomy

>- Human with glasses (computerized or not), remains autonomous.
>- Direct neural link:
>     - Puts individuality in question.
>     - Allows autonomy to be compromised.

## Main question of the paper

> The main question arising from this discourse being: when an individual’s consciousness is based on a part human part machine nervous system, in particular when they exhibit Cyborg consciousness, will they also hold to Cyborg morals, values and ethics? These being potentially distinctly different to human morals, values and ethics. Also, as a consequence, will cyborgs, acting as post humans, regard humans in a Nietzscheian like way (Nietzsche 1961 [corrected spelling!]) rather akin to how humans presently regard cows or chimpanzees?

## Differences between human and machine intelligence

>- Calculating speed.
>- Perfect memory and retrieval.
>- Additional senses (x-rays, radioactivity, IR, ...)
>- Handling of additional dimensions (human brain limited to 3 plus time).
>- Human communication: Low-bandwidth, high error rates. Machines: low error rate, parallel messaging, high-bandwidth, high-speed.

## Advantages of becoming a cyborg

>- With a human brain linked to a computer brain, that individual could have the ability to:
>     - use the computer part for rapid maths
>     - call on an internet knowledge base, quickly
>     - have memories that they have not themselves had
>     - sense the world in a plethora of ways
>     - understand multi dimensionality
>     - communicate in parallel, by thought signals alone, i.e., brain to brain
>- But:
>     - At what cost?
>     - What might the consequences be?
>     - What about the problems associated with actually becoming a cyborg?

## The 1997 cockroach experiment

>- 1997: University of Tokyo: motor neurons of a cockroach interfaced with microprocessor.
>- Microprocessor sends signals to cockroach body.
>- Cockroach has to act out computer commands, independent of its own volition.

## Brain-controlled machines

>- Today many examples of thought-controlled BCI (brain-computer-interface) applications.
>- Brain-controlled wheelchairs (go to minute 2:00):
>- <https://www.youtube.com/watch?v=yNpOgEbKHbw> 

## Warwick’s first chip implant

>- In 1998, he had an identifying chip implanted in his arm.
>- Automated door opening, computer login, location tracking.
>- Initially worried about tracking, but later found it natural.
>- Just like our use of credit cards.
>- He felt attached to the implant and to his computer.

## Removal of the implant

>- “Subsequently, when the implant was removed, on the one hand I felt relieved because of the medical problems that could have occurred, but on the other hand something was missing, it was as though a friend had died.”
>- “If I had to draw one conclusion from my experience it would be that when linked with technology inside my body, it is no longer a separate piece of technology.”
>- “Mentally I regard such technology as just as much part of me as my arms and legs. If my brain was linked with a computer it is difficult to imagine where I would feel my body ended.”

## Second implant

>- 2002: Direct links with nerves in the arm.
>- Implant could read and trigger nerve action.
>- Fed ultrasound sensor output directly into his own arm nerves.
>- Could store arm movements on computer and play them back to his arm.
>- Could record emotional events (anger etc) on the computer and play the signals back to his arm.
>- His wife got an implant too. They could send signals to each other.
>- Sent arm signals from the US to a robot hand in the UK.

## Implications

>- Useful for treatment of medical conditions, for example paralysis.
>- But: remote controlling of other people’s movements?
>- Helping blind people by introducing a new sense to them.
>- If brains could transmit thoughts in this way, would speech become obsolete?
>- If cyborgs are connected to a network, do they give up their individuality?
>- Should every human have a right to be upgraded to a cyborg?
>- Would non-upgraded individuals be seen by the cyborgs like chimpanzees?

# Trimper: Ethics of brain interfaces

## Trimper: Ethics of brain interfaces

Trimper, J. B., Wolpe, P. R., & Rommelfanger, K. S. (2014). When “I” becomes “We”: ethical implications of emerging brain-to-brain interfacing technologies. Frontiers in Neuroengineering, 7, 4.

See file in Moodle: Trimper-Ethics-of-Brain-Interfacing-Technologies.pdf

## BTBI

>- BTBI: Brain-to-Brain-Interfaces.
>- Two rat classes: encoders and decoders.
>- Rats had to choose “rewarded” lever. Encoders saw a light. Decoders did not see the light, but chose the right lever more often than by chance.
>- BTBI provides a direct access to another brain, bypassing the senses.

## Moral problems of BTBI

Problems related to:

>- Extraction of information
>- Delivery of information
>- Combination of the two

## BTBI: Privacy (1)

>- Via connectome research, brain data might be identifiable as those of particular persons.
>- fMRI technologies can already reconstruct the images seen by subjects:
>     - <https://www.youtube.com/watch?v=nsjDnYxJ0bo&feature=youtu.be>
>     - This is not a live reconstruction of what the subjects see. Paper (Nishimoto 2011) explains the details, but they don’t matter to us now.
>- New dimension: not only extraction of information from one brain, but also introduction into a new brain. The receiver brain has no way to block (refuse or inhibit) the transmission.
>- BTBI information can be transmitted over the Internet. This makes BTBI interfaces hackable.

## BTBI: Privacy (2)

>- “Choi (2013) decoded with high accuracy the shoulder and arm joint movements of participants based on non-invasive recordings. If this information could be transferred via stimulation to the relevant regions of a second individual’s brain, it may be possible to control the limbs of the receiving subject via encoder’s neural activity.” (Trimper)
>- In the future, it might be possible to remote-control people with this technology (prisoners, for instance).

## BTBI: Cognitive enhancement

>- Possible advantage to students from coupling brains.
>- BTBI (expensive) might widen social gap in education.
>- People are already experimenting with Transcranial Direct Current Stimulation Devices (tDCS) that are supposed to increase focus.

## BTBI: Some moral problems

>- Non-therapeutical use of BCIs violates individual authenticity, disrespects the “limits of nature,” and puts us as risk of losing “what makes us human” (?)
>- Loss of autonomy and the potential for coercive control over another creature?
>- Concept of self: If one is defined by his or her neurophysiology, then how is individuality defined when a brain is synched with another’s, or perhaps many others’?
>- Will this create a sense of communal identity?
>- Responsibility ascription to actions of “decoders”? Are they at all responsible for their actions?
>- Ownership of BTBI generated ideas: If ideas are generated during the use of BTBI, who rightfully owns those ideas?


# Allhoff and Lin: Ethics of Human Enhancement: 25 Questions and Answers

## I. Definition and Distinctions

>- What is human enhancement?
>- Natural-artificial distinction.
>- Internal-external distinction.
>- Therapy-enhancement distinction.

## What is human enhancement? (1)

“Natural” human enhancement:

> “Any activity by which we improve our bodies,  minds, or abilities—things we do to enhance our well-being. So reading a book, eating vegetables, doing homework, and exercising may count as enhancing ourselves.” (p.3)

## What is human enhancement? (2)

Artificial human enhancement:

>- “Boosting our capabilities beyond the species-typical level or statistically-normal range of functioning for an individual.” (p.3)
>- Human enhancement is different from therapy.
>- Therapy: “Treatments aimed at pathologies that compromise health or reduce one’s level of functioning below this species-typical or statistically-normal level ...” (p.3)
>- Human enhancement, as opposed to therapy, changes the structure and function of the body (Greely, 2005).

## Natural-artificial distinction

>- Difficult to defend.
>- The term “natural” is vague.
>- “For instance, if we can consider X to be natural if X exists without any human intervention or can be performed without human-engineered artifacts, then eating food (that is merely found but perhaps not farmed) and exercising (e.g., running barefoot but not lifting dumbbells) would still be considered natural, but reading a book no longer qualifies as a natural activity (enhancement or not), since books do not exist without humans.” (p.4)
>- Often, the natural-artificial distinction is theologically motivated.

## Internal-external distinction

>- Enhancement is not only “use of tools.”
>- Enhancement: “Tools integrated into our bodies.” (p.5)
>- What is so special about incorporating tools as part of our bodies? Why should this be the criterion for enhancement?
>- Integrating tools into our bodies gives “unprecedented advantages.”
>     - Easier, immediate, always-on access.
>     - Intimate, enhanced connection with the tools.
>     - Substantial advantage for the enhanced person.
>- Grey area: enhanced clothing, glasses.
>- Conclusion: This distinction is not central to the problem.

## Therapy-enhancement distinction

>- Is there really a difference between therapy and enhancement?
>     - Vaccinations?
>     - If vaccinations are enhancements, should they be restricted?
>     - If not, is our definition of enhancement wrong?
>     - If a genius suffers a head injury that reduces his IQ to normal levels, would returning him to his normal “superhuman” level be a therapy or an enhancement?
>     - More specifically, what is the baseline for the “normal” state that therapy seeks to restore?
>- But common sense seems to expect this distinction, so we should keep making it.


## II. Contexts and Scenarios

>- Does the context matter?
>- Examples of enhancement for cognitive performance.
>- Examples of enhancement for physical performance.
>- Should a non-therapeutic procedure that provides no net benefit be called an “enhancement”?

## Does the context matter?

>- We have to consider the *historical, social and cultural context* of enhancement technologies.
>- For example, fossil fuel use is morally different in 1910 from 2010!
>- The morality of human enhancements depends on context too.
>     - Perhaps future societies have adapted to human enhancements.
>     - For example, if in future sports events *all* participants are enhanced, or there are separate competitions for enhanced and not-enhanced participants, then there would be no moral problem.
>     - Sometimes, the injustice created by access to a technology might be preferable to not having the technology at all (vaccinations, Wikipedia).

## Examples of enhancement for cognitive performance

>- Ritalin: medicine that can enhance concentration.
>- Anxiety-reducing medicines.
>- Recreational drugs used to relax and increase creativity.
>- Future: neural implants, interfaces to knowledge databases, remote sensors, and other technologies.

## Examples of enhancement for physical performance

>- Steroids for athletes increase physical performance.
>- Cosmetic surgery to increase perceived attractiveness.
>- Prosthetic limbs and exoskeletons increase physical strength.
>- Life extension technologies (diet, medical procedures, or uploading of brain content into a machine).

## Should a non-therapeutic procedure that provides no net benefit be called an “enhancement”?

>- Transforming into a cat, lizard or snake.
>- Transforming the body to swim better.
>- Morality of un-enhancements?
>     - Amputate healthy limbs?
>     - Deaf parents choose a deaf child through in-vitro fertilisation?

## III. Freedom and Autonomy

>- Can we justify human enhancement technologies by appealing to our right to be free?
>     - Freedom is always restricted in society.
>     - Some restrictions actually increase freedom (traffic laws).
>- Could we justify enhancing humans if it harms no one other than perhaps the individual?
>     - What if enhancement drugs are addictive?
>     - Is informed consent sufficient? (How informed is it? Seat belts? Smoking?)
>     - There is always an impact on others (family, friends).
>     - Aggregate harms can be significant even if individual harms are not (watering one’s lawn vs a state-wide lack of water).

## IV. Fairness and Equity

>- Does human enhancement raise issues of fairness, access, and equity?
>     - Unenhanced people might become “dinosaurs in a hypercompetitive world.” (p.17)
>     - Economic inequality and access to enhancement technologies? 
>- Will it matter if there is an “enhancement divide”?
>     - Did the digital revolution increase the digital divide?
>     - Are enhancement technologies likely to create a new, unfair divide?

## V. Societal Disruptions (1)

>- What kind of societal disruptions might arise from human enhancement?
>     - Sports
>     - Jobs
>     - Pensions and social security for longer-living people
>     - Competition in schools, job markets
>     - Privacy concerns from implanted recording devices
>- Are societal disruptions reason enough to restrict human enhancement?
>     - History: typewriters, spreadsheets (accountants)
>     - Not sufficient reason to restrict technologies, but reason to plan ahead!

## V. Societal Disruptions (2)

>- If individuals are enhanced differently, will communication be more difficult or impossible?
>     - New sensors would create new experiences (for some)
>     - Communication and a shared social life depends on having similar bodies and sensory equipment

## VI. Human Dignity and The Good Life (1)

>- Does the notion of human dignity suffer with human enhancements?
>     - Will enhancements create more dissatisfaction and discontent? (see today: cosmetic surgery for young girls)
>- Will enhancements make people happier?
>     - Increased communication with friends on social media increases stress
>     - Increased competition with others might lead to an “arms race” of enhancement

## VI. Human Dignity and The Good Life (2)

>- Will we need to rethink the notion of a “good life”?
>     - James Moor: there are certain underlying core values that all people have (Moor, 1999):
>     - Life, happiness (pleasure), and autonomy.
>     - In order to exercise our autonomy we require
>          - the ability to do various things,
>          - the security to do them,
>          - the knowledge about doing them,
>          - the freedom and opportunity to do them,
>          - and finally the resources to accomplish our goals.
>- “Currently, people around the world are more or less the same. We know in general what sorts of things make people happy, what makes them suffer, what gives pleasure and pain, and so on. If human enhancements become widespread, it is likely that people will become very different from each other.” (p.23)
>- “Could one be a friend of a much more enhanced person?” (p.24)

## VII. Rights and Obligations (1)

>- Is there a right to be enhanced?
>     - Rights: 1. a class of human rights, sometimes called “natural rights”; 2. a class of more conventional rights based on the specific customs, roles, and laws of a society.
>     - “Humans should be able to exercise their right to enhancements to the extent that it promotes their life, liberty, or the pursuit of happiness.” (p.24)
>     - Laws that allow some enhancements and prohibit others.
>     - How justified such conventional rights or prohibitions are depends upon how good the reasons for them are.

## VII. Rights and Obligations (2)

>- Could human enhancement give us greater or fewer rights?
>     - What rights and duties would be affected if future enhancements give some individuals in society much greater physical and mental abilities than they have now?
>     - Should they have greater rights or liberties than unenhanced persons?
>     - Would the enhanced then have some duty to care for the unenhanced, just as the better-informed and capable parent has a duty to care for her child?
>- Is there an obligation in some circumstance to be enhanced?
>     - Vaccinations?
>     - Enhancements for pilots that make flying safer?
>     - Enhancements for tracking prisoners?

## VII. Rights and Obligations (3)

>- Should children be enhanced?
>     - Children cannot give valid informed consent.
>     - Parents may make “crucial decisions about the capabilities of their children that may be irreversible and limit their children’s future choices and opportunities.” (p.27)
>     -  “Will the child agree with the choices when he or she is older?”
>     - Parents already making such choices (school, diet, moral guidance).
>     - Children’s right to education? Does it include education boosting technologies (chemicals, or AI cognitive enhancements)?
>     - Context matters: What do the other children do? Competition in school? What is the new “normal” baseline?

## VIII. Policy and Law (1)

>- What are the policy implications of human enhancement?
>     - Possible reactions: (1) no restrictions, (2) some restrictions, or (3) a moratorium or full ban.
>     - “Critics have argued that any regulation would be imperfect and likely ineffectual, much like laws against contraband or prostitution (Naam, 2005); but it is not clear that eliminating these laws would improve the situation, all things considered.” (p.29)
>- Might enhanced humans count as someone’s intellectual property?
>     - Diamond v. Chakrabarty: US Supreme Court ruled that a genetically-modified, oil-eating bacterium -- which is not naturally occurring -- is patent-eligible (US Supreme Court, 1980).
>     - Should some human-enhancing treatments belong to all of humanity and not be protected by intellectual property law?

## VIII. Policy and Law (2)

>- Will we need to rethink ethics itself?
>     - “To a large extent, our ethics depends on the kinds of creatures that we are.” (p.31)
>     - Concepts of happiness, benefit, autonomy, ownership of mental content, friendship, virtue, and so on might have different meaning for enhanced people!






