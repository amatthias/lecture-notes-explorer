---
title:  "AI and Society: 5. Prolog. Symbols and meaning"
author: Andreas Matthias, Lingnan University
date: September 2, 2019
...

# Prolog

## Prolog

Prolog ("Programming in Logic," 1972) is a programming language used to represent simple deductive reasoning from facts and rules.

Prolog and similar languages (for example, Answer Set Programming) are still used in AI today, for example in systems that try to derive conclusions from particular premises or to store knowledge in a rule-based format.

Try it out here:

<http://swish.swi-prolog.org>

or

<http://www.tau-prolog.org>

Textbook: <https://moral-robots.com/technology/prolog-programming-in-logic/>

## Example

>- Prolog: **cat(tom).**
>- English interpretation: “Tom is a cat.”
>- Note: You have to write "tom" in lower case. Words that begin with a big letter are *variables* in Prolog.

## Queries

>- Prolog: **? cat(tom).**
>- English: “Is Tom a cat?”
>- Answer: true.

>- Prolog: **? cat(X).**
>- English: "Which X is a cat?"
>- X is an uppercase letter, so Prolog knows it's a variable!
>- Answer: X=tom.

## Prolog does not "understand"!

- Observe that the system does not know anything about cats or Tom.
- It just uses the same symbols the human operator used.
- It is up to the operator to project an interpretation into those symbols!

For example:

>- Prolog: **gugu(lala).**
>- English interpretation: ?
>- Prolog: **? gugu(X).**
>- English: "Which X is a gugu?"
>- Answer: X=lala.

## Syntactic manipulation

“Syntactic” symbolic AI assumes that, in many cases, the computer does not need to understand the semantics (=meaning) of the symbols used.

. . . 

### Assumption of symbolic AI:

If the symbol manipulation preserves the original relations between the symbols, the mapping of symbol to meaning can be left to mind of the operator.


## Mapping of symbols to meaning

![](graphics/pai04-35.png) \ 

## A more complex example (1)

Read ":-" as "if":

\vskip2ex\hrule

| mother_child(trude, sally).
| father_child(tom, sally).
| father_child(tom, erica).
| father_child(mike, tom).
| sibling(X, Y) :- parent_child(Z, X), parent_child(Z, Y).
| parent_child(X, Y) :- father_child(X, Y).
| parent_child(X, Y) :- mother_child(X, Y).

\vskip2ex\hrule

**Try it out!** What strange thing happens if you ask for siblings?

## A more complex example (2)

>- Problem: Everybody is considered their own sibling! (Logical, but strange).
>- We need to change the definition of sibling, to require that the two potential siblings are *different* people!

. . .

\vskip2ex\hrule

| mother_child(trude, sally).
| father_child(tom, sally).
| father_child(tom, erica).
| father_child(mike, tom).
| sibling(X, Y):- parent_child(Z, X), parent_child(Z, Y), **X\\=Y**.
| parent_child(X, Y) :- father_child(X, Y).
| parent_child(X, Y) :- mother_child(X, Y).

\vskip2ex\hrule

## Interpretation

| sibling(X, Y) :- parent_child(Z, X), parent_child(Z, Y).

English interpretation:

X is a sibling of Y: if (Z is a parent of X) AND (Z is a parent of Y)

(makes sense).

## More queries

| ? sibling(sally, erica).
| true.
| ? father_child(Father, Child).
| (outputs all father/child pairs)

## Exercise

Look at the code above. In a similar fashion, write a series of facts and rules describing the following relations:

- Kant is a philosopher
- Plato is a philosopher
- James likes Kant
- Mary likes Kant and Plato
- Peter likes what Mary likes

Then ask the following questions:

- Who likes Kant?
- Who likes Kant and Plato?
- Whom does Mary like?
- Whom does Peter like?

## Solution

| philosopher(kant).
| philosopher(plato).
| likes(james, kant).
| likes(mary, kant).
| likes(mary, plato).
| likes(peter,X) :- likes(mary,X).

Test:

| ?- likes(james, plato).  → false
| ?- likes(peter, X).  →  kant, plato.

## Second example: Classmates

```
teacher( sandy ).
teacher( john ).
student( mary ).
student( peter ).
student( nick ).
student( anna ).
```

## Simple queries

After entering these "facts" into the Prolog system, we could ask a question in form of a *query:*

```
teacher( X ).
```

The Prolog system would then try to match the *variable* X (note that it is a capital letter, which tells Prolog that this is a variable!) with the names given in the collection of facts. It would then give the answers:

```
X=sandy
X=john
```

## Two-place predicates

But we can do more with Prolog. We could, for example, define a two-place teacherOf(teacher, student) predicate, to record who is teaching whom:

```
teacherOf( john, mary ).
teacherOf( john, peter ).
teacherOf( sandy, nick ).
teacherOf( sandy, anna ).
```

This works as we would expect: *teacherOf( X, mary )* would return *X=john.*

## More complex predicates

But we can do more with that. We can now define a predicate "classmate" that uses the teacher predicate. The idea is that a classmate is someone with whom you have the same teacher:

```
classmate( X, Y ) :- student(X), student(Y),
   teacherOf( A, X ), teacherOf( A, Y ).
```

The ":-" means that the expression on the left will be true if the expressions on the right are true. You can just read it as "if". The commas on the right side (",") express a logical "and". That means that *all* the expressions on the right have to be true in order for the system to consider the predicate *classmate* to be true.

## More queries

Now we can ask:

```
classmate( X, peter ).
```

This means: Who (X) is classmate of Peter? The system answers:

```
X=mary
X=peter
```

## A problem

Oops. Something strange happens here. What exactly? Well, if you look at the predicate classmate, it never says that one cannot be *one's own* classmate! Since everyone has the same teacher as oneself, logically, everyone who is a student is always also their own classmate!

## The “not equal” operator

If we wanted to change that, we would have to exclude the case that someone is one's own classmate, like this:

```
classmate( X, Y ) :- student(X), student(Y),
   teacherOf( A, X ), teacherOf( A, Y ),
   X\=Y.
```

The operator "\=" in Prolog means "not equal," so that in this case it is additionally required that X is different from Y. Let's see:

```
classmate( X, peter ).
```

Answer: *X=mary,* which is the expected answer.

## Another query

We can also try:

```
classmate( X, sandy ).
```

which correctly answers *false,* meaning that no solution can be found, since Sandy is not a student but a teacher, and we explicitly limited the *classmate* predicate to students.


## Definitions and symbolic processing

This way of symbolic processing is similar to *defining* things.

A definition gives the genus (family) of something and a specific difference. (Aristotle)

. . . 

"An elephant **is a** mammal that **has** a long trunk, big ears and is very big and grey."

. . .

- "isA" relation: genus (family)
- "has" relation: difference

## Prolog version

"An elephant **is a** mammal that **has** a long trunk, big ears and is very big and grey."

| isA(elephant, mammal).
| has(elephant, trunk).
| has(elephant, big_ears).
| has(elephant, big_size).
| has(elephant, colour_grey).

. . . 

Query: **has( X, big_size ).**

## isA and has

We can go a long way in representing facts symbolically with these two relations:

- isA() for establishing the Aristotelian genus of something.
- has() for enumerating its properties (differences).

What results is sometimes called an “ontology” (in the computing sense!)

## Limits of Aristotelian logic in real life

- “If it’s winter, then the heating is on.”
- All birds fly.

Are these always true?

. . .

No. There might be many reasons for the heating to be actually off:

>- Lack of money to pay the bill,
>- No fuel,
>- The heater is broken,
>- The occupant has gone out,
>- It's a warm winter's day and no heating is needed.

. . . 

>- “All birds fly,” but chickens fly very badly, and penguins don’t fly at all.


## Tweety

Consider the statements:

>- Tweety is a bird.
>- All birds fly.
>- If something is a penguin, then it is a bird.
>- Tweety is a penguin.
>- Penguins don't fly.
>- **Does Tweety fly?**

>- No.

## Tweety

- Tweety is a bird.
- **All birds fly.**
- **If something is a penguin, then it is a bird.**
- Tweety is a penguin.
- **Penguins don't fly.**
- *Does Tweety fly?*

- No.
- The list of statements above contains **contradictions** in normal deductive logic!

## "Defeasibly follows"

In these cases, we want to express that some statements are only true “most of the time,” or “usually,” but not strictly *always.*

*Defeasible* rules are "reasons to believe." They  are general rules that will be true most of the time, but might also be false at particular moments.

. . . 

Textbook: <https://medium.com/@MoralRobots/aristotle-and-the-penguins-a78e80b4783a>

## "Defeasibly follows"

>- winter -> heating_on ; or:
>- heating_on <- winter (in Prolog order)

. . .

... mean: "If it is winter, then the heating is on."

. . . 

Compare:

>- winter $\leadsto$ heating_on ; or:
>- heating_on **-<** winter (in Prolog order)

. . . 

... mean:

>- "If it is winter, then the heating will (usually) be on" ; or:
>- "If it is winter, there is *reason to believe that* the heating will be on."


## Defeasible (D:) assumptions

- Tweety is a bird.
- *D: All birds fly.*
- If something is a penguin, then it is a bird.
- Tweety is a penguin.
- *D: Penguins don't fly.*
- Does Tweety fly?

D-Prolog <http://tweetyproject.org/w/delp/>:

| Bird(tweety).
| Fly(X) **-<** Bird(X).
| Bird(X) <- Penguin(X).
| Penguin(tweety).
| ~Fly(X) **-<** Penguin(X).
|
| ?- Fly(tweety) (Correct answer: false)


## Defeasible logic

>- Defeasible logic (proposed by Donald Nute) is used to formalize defeasible reasoning.
>- Three different types of propositions:
>     - Strict rules: a fact is always a consequence of another;
>     - Defeasible rules: a fact is typically a consequence of another;
>     - Undercutting defeaters: specify exceptions to defeasible rules.
>- During the process of deduction, the strict rules are always applied, while a defeasible rule can be applied only if no defeater of a higher priority specifies that it should not. (Wikipedia)


## Why do we need ‘defeasible’ logic? (1)

>- It would be best if we could have a probabilistic logic, where every statement has an attached probability. In this case, we would do deductions by calculating the probability of the conclusion given the probabilities of the premises.
>- But this is impossible for various reasons:
>     - In many cases, we don’t know the correct probabilities (how many birds are penguins? How many species of bird don’t fly?)
>     - Humans are bad at estimating probabilities for everyday events.

## Why do we need ‘defeasible’ logic? (2)

>- Therefore, the concept of a ‘defeasible’ conclusion is a kind of intermediate, stop-gap solution.
>- Instead of precisely calculating probabilities (which would be impossible in real life), or completely ignoring them and creating contradictory sets of premises, we take a middle road.
>- A defeasible assumption is *generally* true, but not *always* true. The machine can treat it as an assumption, but if there are good reasons to assume that it’s false, then the machine can assume that it’s false without creating a contradiction.
>- This gives us the advantages of both extremes (binary logic and probabilities), without their respective drawbacks.
	

## The End. Thank you for your attention!

Questions?

<!--

%% Local Variables: ***
%% mode: markdown ***
%% mode: visual-line ***
%% mode: cua ***
%% mode: flyspell ***
%% End: ***

-->
