---
title:  "AI and Society: 4. Detecting intelligence. Definitions of AI."
author: Andreas Matthias, Lingnan University
date: September 2, 2019
...


# How to detect intelligence?

## "Intelligent" robots?

>- Assume you want to find out whether a robot, that stands in front of you, is intelligent.
>- How could you judge that?
>- More generally, how could you judge that *any* thing is intelligent?
>- I can ask the same questions about a dog, or a chair: 
>     - “Does it think?”
>     - “Is it intelligent?”
>     - “And how would I know?”

## Are chairs intelligent? Structural equivalence

>- “Is this chair intelligent?” How to decide? How to know *for sure?*
>- One way is to argue like this:
>     - Thinking (and intelligence) requires a brain.
>     - Chairs don’t have brains. We can cut them open and see that they are just made of wood and more wood. The wood is not structured anything like a brain.
>     - Conclusion: Chairs cannot possibly be thinking.
>- This is not an entirely bad argument (nor is it very strong). We will call this the **structural equivalence** argument.

## Behavioural equivalence (1)

>- Another way would be to look at the *behaviour* of the chair. 
>- What does a chair do? 
>     - It just sits there. 
>     - You kick it, it doesn’t move away. 
>     - You put fire to it, it doesn’t protest. 
>     - You talk to it, it doesn’t answer.
>     - A dog, on the other hand, does display all sorts of interesting reactions: it comes when called, flees or fights when attacked, and runs away from fire. 
>     - Humans have even more nuanced behavioural responses. 

## Behavioural equivalence (2)

>- We could say that some thing X is intelligent if (and only if) the behaviour of X in various situations is similar to the behaviour of another thing Y that is known (or assumed) to be intelligent; a human, for example.
>- If we compare the behaviour of a chair to a human, we can conclude that it is not intelligent at all.
>- If we compare the behaviour of a dog to a human, we can conclude that it is somewhat intelligent, but it lacks some of the more interesting behaviours (for example, to read a book, or create poetry). 
>- This is the **behavioural equivalence** argument.

## Functional equivalence

>- ‘Intelligence’ is not something rooted in material structure alone (like in the structural equivalence thesis); and also not as some purely behavioural attribute (like with the behavioural equivalence thesis).
>- Instead, we could try to distinguish ‘hardware’ from ‘software,’ the *structural* from the *functional* aspect of an artefact’s operation. 
>- Example: a computer. 
>     - A program that adds two numbers can run on any number of different hardware platforms. Your mobile phone can add two numbers. Your desktop computer can. An abacus can, too. Or you can do it with pen and pencil. 
>     - All these different ‘hardware’ implementations could be said to run the same ‘program’: the ‘software’ that actually performs the function of adding two numbers together. We will call this the **functional equivalence** thesis.

## Functional vs behavioural equivalence

>- As opposed to a purely behavioural equivalence, the functional equivalence does take into account the *abstract organisation of the hardware that performs the function* we are examining. When I add two numbers, there are a number of functional units that are required:
>     - I need to remember which numbers I am adding, and any intermediate results (a kind of memory).
>     - I need some functional unit that can take two digits and perform the actual addition, keeping track of any carry-over numbers (a kind of processing unit).
>     - I need some mechanism that controls the whole process and remembers which digits to add next, and where to put the result (a kind of operations controller).
>     - I need some input and output unit: a way to enter the numbers to be added, and a way to see the result (an I/O interface).

## Functional vs structural equivalence

>- What makes this approach different form the structural equivalence is that now I can account for differences in structure. 
>- I can distinguish *function* from *implementation* (the way a function is physically realised in a particular piece of hardware). 
>- A calculator, an abacus, and a human with a pencil and paper all have the same functional units described above, although these are implemented in very different ways in their respective hardware (electronics, wooden beads, neurons and muscles). 
>- Still, a functional description could easily identify that these devices are, despite their hardware differences, functionally equivalent.

## Problems of the structural equivalence thesis (1)

>- How can we criticise the structural equivalence thesis?
>- Who says that the chair’s wood is structurally dissimilar to a nervous system? They’re both made up of cells. Just from looking at the wood’s structure under a microscope, it would be very difficult to conclude that it cannot do what the nervous system can do.
>- The argument ignores the possibility of having intelligence in a different structure. 
>     - It is like saying that a table has to be made of wood, and have four legs. This is not true.
>     - Why should the ‘table-ness’ of an object depend on the material it’s made of, or an incidental property like the number of its legs?
>- This argument begs the question regarding AI: no AI can be possible as long as artefacts don’t have biological, human brains. Thus, artificial intelligence is impossible by definition.

## Problems of the structural equivalence thesis (2)

>- No two brains are identical. Even human brains show some variation between individuals. If one insists on strict structural equivalence, one must say that only the specific reference brain is optimally thinking. All other brains, being structurally not exactly the same, will necessarily have a lower intelligence, because the are, to some extent, structurally dissimilar to the ‘reference’ brain.
>- Structural equivalence as a test for intelligence is implausible, because this is simply not how we judge intelligence in our lives. 
>     - When you ask yourself if someone is intelligent, or more or less intelligent than you, then you don’t go and dissect their brain to find out.
>     - You just look at them. If they can calculate faster, solve logic puzzles, and understand relativity theory, then you might admit that they are intelligent. 
>     - So we never actually use structural equivalence to judge intelligence. Instead, we use behavioural criteria.

## Problems of the behavioural equivalence thesis (1)

>- Behavioural equivalence underlies the most famous test for artificial intelligence, the Turing Test: If a computer can chat in a way that is indistinguishable from chatting with another human, then the computer can be said to be intelligent. What could be wrong with that?
>- Not all intelligent systems need to be able to do everyday chatter (self-driving cars, image recognition programs, chess- and go-playing programs, programs that diagnose cancer and heart diseases, and many more). None of these can talk. But are they therefore not intelligent?

## Problems of the behavioural equivalence thesis (2)

>- Even clearly intelligent people sometimes fail to converse: intelligent people in foreign countries whose language they don’t speak. Such a person would fail the Turing Test in the foreign country, but it would be wrong to conclude that they are not intelligent.
>- A programmed character in a computer game can be made do perform any action and portray any feeling: pain, fear, love. Still, we clearly understand that these are programmed behaviours, and that the computer program does not actually understand or feel anything.
>- The Chinese Room argument (see a previous lecture).

## The functional equivalence thesis (1)

>- A functionalist, looking at the possibility that something (a chair, a Chinese room, an alien, a robot) is intelligent, would ask: does this candidate have a functional organisation that is likely to lead to intelligent, adaptive behaviour? 
>- Although an MP3 player might be able to talk or make music, the functionalist would see that it lacks the functional units needed to autonomously generate speech or music. It is just playing a pre-recorded sound.
>- The Chinese room is not intelligent because the functionalist could see that it does not contain the right functional units that are needed for intelligence: the Chinese room has no memory, no unit that associates symbols with meaning, no way to acquire meaning from experience.

## The functional equivalence thesis (2)

>- Functional equivalence does not require the equivalent things to be structurally similar. 
>     - The wing of a bird is, for example, functionally equivalent to the wing of a butterfly, and both are (within limits) functionally equivalent to the wings of airplanes.
>     - But all three are structurally different, are made of different materials, and even work physically in different ways.
>     - Still, in the functional economy of a thing that flies, they perform similar functions, and thus could be called functionally equivalent.
>- The functionalist would be able to recognise that an alien is intelligent, even if his brain is made up of entirely different materials. He would see the possibility that a (suitably complex) computer might be intelligent.

## Problems of the functional equivalence thesis: Chinese nation (1)

>- The Chinese nation argument (this has nothing to do with the Chinese room!):
>- Assume you have a brain that works well using one billion neurons (this is about a hundredth of what our brains have, but this doesn’t matter for the experiment. We can make the argument with any number). 
>- Every neuron in this brain is connected with other neurons. 
>- Signals travel through the brain when a neuron receives signals from other neurons on its ‘input’ side, and then produces a new signal on its ‘output’ side. This signal is then propagated as an input to other neurons. 

## Problems of the functional equivalence thesis: Chinese nation (2)

>- Now I could, in principle, take out any one neuron in the brain and put a human in there to play the role of that missing neuron. I’d give instructions to the human to act exactly like the neuron he replaces: when the input signals are such-and-such, he should initiate an output signal. Otherwise, he should not. I give him dials to show the input signals, and a button to produce an output signal that is connected to other neurons.
>- If this man does his job as advertised, the brain should continue to function exactly as before. 
>- Now I can replace more neurons with more people and dials and buttons. Every time, I replace one neuron by one person that acts exactly as the neuron did. 
>- Now you see why we need a *Chinese* nation (rather than a Greek, or Maltese) to play neurons. 

## Problems of the functional equivalence thesis: Chinese nation (3)

>- What if I replace all one billion neurons with one billion Chinese people? 
>- Accepting the premises of the argument, I would expect nothing strange to happen. The brain should work just as before, although it now does not contain a single neuron. 
>- The brain should be able to converse, make jokes, play chess, and write love poems. 
>- But at the same time, none of the people who are involved in this brain converses, jokes, plays chess or writes poems. They only press a button that fires ‘their’ neuron. 
>- So how does this ‘magic’ ability of the brain to do these things come from? And where is the brain’s consciousness now stored?

## Problems of the functional equivalence thesis: Chinese nation (4)

>- The point of the argument is this: 
>     - If functionalism is correct, then the Chinese nation brain should be identical in function to the original brain. 
>     - It has exactly the same functional units that implement exactly the same ‘software’ (or behaviour). 
>     - It should then also have consciousness, pain, emotions, and a sense of self. 
>     - But where are these things? The sense of self of that Chinese nation brain is nowhere in the people that make it up. Has the brain’s sense of self then suddenly disappeared? 
>     - So either functionalism is wrong, or we must attribute some mysterious sense of self, humour, emotions and consciousness to a collection of people who don’t have (as individuals) any of these states (they have their own, but not the original brain’s!).


# Definitions of AI

## Discussion

Textbook: <https://medium.com/@MoralRobots/what-is-ai-some-classic-definitions-a85d6acc4cb8>

## Systems that do things that require intelligence of humans (1)

> "AI is concerned with building machines that can act and react appropriately, adapting their response to the demands of the situation. Such machines should display behaviour compatible with that considered to require intelligence in humans." (Finlay-Dix)

> "The act of creating machines that perform functions that require intelligence when performed by people." (Kurzweil, 1990)

>- How can we criticise these definitions?

## Systems that do things that require intelligence of humans (2)

>- "Behaviour compatible with what is considered to require intelligence in humans:"
>   - To add two numbers.
>	- To change money when a customer buys a coke.
>	- To regulate the room temperature by turning an air-conditioner on or off.
>- Obviously, these things can be done by primitive, non-intelligent machines (calculators, coke vending machines, air-con thermostats).

## Systems that *think like humans*

> "The exciting new effort to make computers think ... machines with minds, in the full and literal sense." (Haugeland, 1985)

> "The automation of activities that we associate with human thinking, activities such as decision-making, problem solving, learning..." (Bellman, 1978)

>- How can you criticise Haugeland's definition?
>    - This would be strong AI only.

## Systems that *act like humans*

> "The study of how to make computers do things at which, at the moment, people are better." (Rich and Knight, 1991)

>- Criticism?
>   - Digestion?
>	- This is self-defeating. As soon as machines get better than humans at X, doing X will stop being an example of AI

## Systems that *think rationally*

> "The study of mental faculties through the use of computational models" (Charniak and McDermott, 1985)

> "The study of the computations that make it possible to perceive, reason, and act." (Winston, 1992)

>- Criticism:
>    - Charniak/McDermott: No application. A "study" is not AI!
>    - Winston: too narrow. Presupposes that intelligence *is* computation, which is begging the question!

## Systems that *act rationally*

> "Computational intelligence is the study of the design of intelligent agents." (Poole et al, 1998)

> "AI ... is concerned with intelligent behaviour in artefacts." (Nilsson, 1998)

>- Criticism:
>    - Both are circular. Poole even narrows it down to computational intelligence, which is only a subset of AI, and not even the most promising one.

## Summary of definitions

![](graphics/03-claims.png) \
 

## More reading materials about this section

- Aims and claims of AI: <https://moral-robots.com/philosophy/aims-and-claims-of-ai/>
- Behavioural, functional, structural equivalence: <https://moral-robots.com/philosophy/do-chairs-think-ais-three-kinds-of-equivalence/>
- Aristotle’s influence on AI: <https://moral-robots.com/philosophy/aristotles-ai/>
- Alan Turing: <https://moral-robots.com/philosophy/alan-turing-1912-1954/>
- AI in Games: <https://moral-robots.com/philosophy/philosophical-issues-ai-in-games-2017/>

... and all the “Textbook” links above.



## The End. Thank you for your attention!

Questions?

<!--

%% Local Variables: ***
%% mode: markdown ***
%% mode: visual-line ***
%% mode: cua ***
%% mode: flyspell ***
%% End: ***

-->
