---
title:  "AI and Society: 3. History and claims of AI"
author: Andreas Matthias, Lingnan University
date: February 14, 2019
...

# History of AI

## Aristotle (384-322 BC)

![](graphics/02-aristotle-2.jpg) \


What did he contribute to AI?

. . . 

Laws of thought, syllogisms.

Textbook: <https://chatbotslife.com/aristotles-ai-ec1d4befeee6>

## Ramon Lull (ca. 1232 – ca. 1315)

An alphabet of human thought:

- All concepts are nothing but combinations of a relatively small number of simple concepts
- (Just as words are combinations of letters)
- All truths may be expressed as appropriate combinations of concepts,
- which can in turn be decomposed into simple ideas.

## Ramon Lull (ca. 1232 – ca. 1315)

![](graphics/02-lull.jpg) \


The most important instance of this is the combinatory wheel, made up of moving 
concentric circles marked with figures. By revolving the wheels mentally, various combinations of the concepts represented by the figures were possible.

## Alan Turing (1912-1954)

![](graphics/02-turing.jpg) \


>- Turing *Machine* (a machine which can compute everything that is computable)
>- Turing *Test* (a test for intelligence based on conversation with a computer)

. . . 

Textbook: <https://towardsdatascience.com/alan-turing-1912-1954-956ccc3da6b2>

## Turing Test

![Source: Wikimedia](graphics/ttwm.png) \


Which chatter is the human, which is the machine?

## SHRDLU (1)

Terry Winograd (1946-): Programmer and philosopher, later an AI critic.

![](graphics/02-shrdlu.jpg) \



## SHRDLU (2)

| Person: Pick up a big red block.
| Computer: OK.
| Person: Grasp the pyramid.
| Computer: I don't understand which pyramid you mean.
| Person (changing their mind): Find a block which is taller than the one you are holding and put it into the box.
| Computer: By "it", I assume you mean the block which is taller than the one I am holding. -- OK.
| Person: What does the box contain?
| Computer: The blue pyramid and the blue block.
| Person: What is the pyramid supported by?
| Computer: The box.

. . .

\vskip2ex\hrule

*Does the program "understand"?*

## Searle's Chinese Room

![Source: Wikimedia](graphics/02-2-chinese-room.jpg) \


In a closed room, somebody who does not speak Chinese has a rule book, which tells him what to answer to every Chinese question that he encounters.

. . .

You can talk to him through written cards, and he always gives a sensible Chinese answer.

. . .

Does he *understand* Chinese?

## Neuron

![Biological neuron (simplified)](graphics/CNeuron-trim.png) \


## Artificial neural networks

![A three-layer artificial neural network](graphics/CLayerNetwork-trim.png) \


## Artificial neural networks (2)

![How a neural network recognises a letter](graphics/Cocr-trim.png) \



## Artificial neural networks (3)

- Computer vision
- Handwriting recognition
- Face recognition
- Image recognition

. . . 

Generally:

>- Can be *trained* by example, rather than having to write a program to do some task.
>- Good at recognising features even when the pattern perceived is slightly different from the pattern that was learned!

## Artificial neural networks (4)

- Rosenblatt (1962): Perceptrons.
- Artificial Neural Netwoks (ANN) again after 1986. Multiple layers.
- Deep learning, deep neural networks (more than 2 layers) (Igor Aizenberg and colleagues, 2000)

## Facebook DeepFace recognition

![Source: facebook](graphics/02-deepface.png) \


## Image style transfer (1)

![Credit: Twitter user \@DmitryUlyanovML](graphics/01-style1.jpg) \


## Image style transfer (2)

![](graphics/01-style2.jpg) \



## Evolutionary computing

Holland: Genetic algorithm (1975).

![](graphics/02-ga.png) \


## Moore's Law (1)

Gordon E. Moore, co-founder of Intel (1965): Density of electronic chips doubles every two years. -- *Why is this important?*

![Credit: https://commons.wikimedia.org/wiki/User:Wgsimon](graphics/02-moores-law.png) \


## Moore's Law (2)

An "Osborne Executive" portable computer, from 1982, and an iPhone, released 2007 (iPhone 3G in picture).

The Executive weighs 100 times as much, has nearly 500 times the volume, cost 10 times as much, and has a 100th the clock frequency of the iPhone. (Wikipedia)

![](graphics/02-moore-compare.png) \


# Claims of AI

## Claims of AI

Try to formulate the central claim of AI!

>- It is *not* to make an artificial human!
>- Why not?
>       - We don't want it to have rights
>	     - We don't want it to forget things
>	     - We don't want it to fall in love
>	     - We don't want it to be in bad mood
>	     - We don't want it to spend money for itself
>	     - We don't want it to take 18 years of learning in order to be useful

## "Strong" and "weak" AI

>- **“Strong AI”:** An artificial intelligence system can think and have a mind, mental states, and consciousness.
>- **“Weak AI”:** Machines can demonstrate (simulate?) intelligence, but do not necessarily have a mind, mental states or consciousness.

. . . 

Textbook: <https://aboveintelligent.com/aims-and-claims-of-ai-60150ac202c6>

## Psychological and phenomenal concepts of mind (Chalmers)

David J. Chalmers, “The Conscious Mind”:

>- The phenomenal “… is the concept of mind as conscious experience, and of a mental state as a consciously experienced mental state” (p.11)
>- The psychological “… is the concept of mind as the causal or explanatory basis for behaviour … it plays the right sort of causal role in the production of behaviour, or at least plays an appropriate role in the explanation of behaviour … What matters is the role it plays in a cognitive economy” (p.11)

. . . 

**“On the phenomenal concept, mind is characterized by the way it feels; on the psychological concept, mind is characterized by what it does.” **

## AI and Philosophy of Mind

>- Much of the discussion on mind has to do with phenomenal properties. It is not relevant to AI that only *behaves* like humans.
>- It would be relevant to AI that *feels* like humans.
>- *But do we even want such AI systems?*

## Differences strong/weak claim?

>- The distinction weak/strong AI corresponds to the distinction in the Philosophy of Mind between the *psychological* and the *phenomenal* aspects of mind.
>- A strong AI system would think.
>- A weak AI system would “think” (or: pretend to think).
>- Normally AI researchers don't worry about strong AI.
>- *Why not???*
>- Target of AI is to make intelligent machines, not minds!

## Consequences of phenomenally complete  AI

What would be some consequences of “phenomenally complete” AI?

. . . 

>- Machines would feel pain.
>- Machines would be tired, in bad mood.
>- Machines might be “persons.”
>- Machines would probably need to have rights and be protected just like humans or higher animals.

## Some criticisms of weak AI

>- No machine has yet passed the Turing Test (or come close).
>- Machines still have very little common sense and are not good with ambiguities.
>- "Full" intelligence might need a body and an environment to grow in.

## Some criticisms of strong AI

>- Searle's Chinese Room (does a machine “understand”)?
>- Can a machine have mental states that “feel” like something? (Comparable to how it "feels" to a human to have fear or to fall in love)
>- Substance dualist positions would require us to put a “mind substance” into the computer.
>- In the extreme case we might need 18 years to grow an unreliable machine with all the drawbacks of humans (because it might be that advantageous and negative traits of the mind go necessarily together!)



## The End. Thank you for your attention!

Questions?

<!--

%% Local Variables: ***
%% mode: markdown ***
%% mode: visual-line ***
%% mode: cua ***
%% mode: flyspell ***
%% End: ***

-->
