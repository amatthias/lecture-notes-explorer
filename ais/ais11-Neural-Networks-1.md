---
title:  "AI and Society: 11. Neural Networks"
author: Andreas Matthias, Lingnan University
date: September 30, 2019
...

# Neurons and perceptrons

## Biological neurons

Artificial Neural Networks (ANN) are "neurally inspired." (McLaughlin 142)

![](graphics/02-neuron.jpg)\ 

Dendrites, cell body with signal integration, axon.

## Perceptron (Rosenblatt 1957)

![](graphics/05-perceptron.jpg)\ 

Inputs, weights, summing function, cut-off value, output.

## Multi-layer networks

![](graphics/05-3lnet.jpg)\ 

Input layer, hidden layer, output layer.

## Calculating an OR

Imagine you have a system that you want to perform the "logical or" function on its inputs A and B:

  Input 1 (A)     Input 2 (B)    Output
 -------------   -------------  --------
       0             0             0
       0             1             1
       1             0             1
       1             1             1
 ---------------------------------------

## Calculating an OR

![](graphics/neurons-or-nnwm.png)\ 


## An analogy (1)

>- Imagine that the inputs and outputs are electrical signals, say, a sound. The output is connected to a speaker. The neurons’ synaptic weights are volume dials that allow you to dial the volume of that particular audio line down, from 100\% to 0.
>- You would start by putting the signals for the first case (A=0, B=0) onto the input side. Then you would tweak the buttons until the output signal is 0.
>- Next you would put a signal ("1") on input A and leave B at 0. You would tweak the buttons until the signal at C (output) is on ("1").
>- Then you would go back to the first case, and make sure that this still works. If not, tweak some more until both cases work correctly.
>- Add the third case in the "OR" table. And so on.
>- After some time, you would manage to find a configuration of button positions that exactly satisfies the "OR" function.

## An analogy (2)

>- This is exactly how artificial neural networks work!
>- The buttons are the "synaptic weights" between the neurons and the thresholds that make them fire or not fire.
>- All neurons are fully connected to the next layer.
>- In the beginning, synaptic weights are random, but later the network changes them (during training) to match the desired output. This can take thousands of cycles of testing, adjustment, and error, and more testing.
>- When the learning process is finished, the system has "learned" a function. This function is now stored in the synaptic weights (the button positions that encode the behaviour we want).

## Backpropagation

>- When the neural network is in its learning phase, it will compare the *actual* output (that it produces with a particular set of weights) to the *desired* output.
>- The difference between the two is the output *error.*
>- There is a simple mathematical method used to change the synaptic weights *backwards* from the last layer to the previous layers of the neural network, so that the error is minimised.
>- This process, of changing the synaptic weights backwards in order to minimise the output error of the network, is called *backpropagation* (of the error correction).
>- We don’t need the mathematical details, but if you are interested, you can google “backpropagation” or look at the Wikipedia article.

# Neurons in spreadsheets

## Neurons in spreadsheets

- You can use a normal spreadsheet (Google Sheets or Excel) to simulate artificial neurons.
- A good introduction is here: <http://toritris.weebly.com>
- Of course, this doesn’t do backpropagation. You’ll have to figure out the error and the correct synaptic weights yourself.

## AND neuron

![](graphics/05-and.jpg)\


## OR neuron

![](graphics/05-or.jpg)\


## XOR neuron

![](graphics/05-xortable.jpg)\ 

![](graphics/05-xor.jpg)\

(see these live on Google Docs!)


## What are the differences to symbolic AI?

>- There is no symbolic representation of the "xor" function anywhere in the system!
>- If I look at an ANN, I cannot say what it will do. Even if I know all the synaptic weights (button positions), these don't tell me how the system will behave.
>- The only way to see the system's behaviour is to actually test it with inputs and see what comes out as an output.
>- This is the same way it is with humans. Examinations are needed to see what's stored in the brain! The brain does not contain symbols!

## Character recognition

![](graphics/05-ocr2.jpg)\ 

# Learning in neural networks

## How does a neural network learn? (1)

>- The neural network is initialised with random synaptic weights. 
>- Then the user applies one input pattern and the corresponding desired output pattern. 
>- The neural network processes the input with its random synaptic weights and produces a random output that is probably not the output we wanted. Then it calculates the difference of that output to the desired output that was given by the user. 

## How does a neural network learn? (2)

>- Next, the artificial neural network changes its synaptic weights, using backpropagation, in order to minimise this error. 
>- Then the next pattern is applied, and the process is repeated. This applying of patterns and then minimising the error takes place thousands of times with all the desired learning patterns until the output error is smaller than a particular value that the user considers “good enough”.

## How does a neural network learn? (3)

![](graphics/05-learning.jpg)\ 





## Generalisation (1)

>- The important feature of ANNs over other ways of programming a computer is that they can *generalise.*
>- They can not only identify the same features that were used in their training, but they can also correctly classify *similar* (but different) features.

. . . 

- Example:

![](graphics/05-mnist.png)



## Generalisation (2)

>- The ability to generalise is what makes neural networks so useful.
>- A neural network that can recognise a face only if it looks exactly the same as in the training picture would be of little practical use. 
>- Look back at our spreadsheet neuron. If you think about it (or if you try it out), you will see that small changes in the input of the neuron don't change the output. 
>     - Whether my input values are 0 or 0.1 or 0.2 does not make any difference to the output of the neuron, because this neuron is mapping a *whole range* of inputs to one single output value. 
>     - Therefore, if I train this neuron to recognise the input pattern 0 successfully, then it will also be able to recognise the input pattern 0.1 successfully. Here we already see generalisation in action.

## Generalisation (3)

The same applies to more complicated networks. Assume that I want to train a network to recognise handwriting. I would then perhaps use a square area of pixels as the input pattern and connect every pixel to one input of the neural network. This could look like this: 

. . . 

![](graphics/Cocr-trim.png "Letter A in a grid")\



## Generalisation (4)

Now I train my network to recognise a capital A by providing it with a collection of capital A patterns. 

. . . 

![](graphics/Letters-trim.png "Different capital A letters")\




## Generalisation (5)

>- Assuming the network has been trained on these images and has learnt to recognise them successfully, I can now try to apply a test pattern that is not part of the input set.  
>- Because the neural network is stable in the face of small changes in its input patterns, the output will likely still remain the same although a few pixels are now different from what the network has been trained on. 

## Generalisation (6)

>- Question: is it good to train a neural network with many similar data or with widely different data? For example, if you want it to recognise a tree, would you train it with tree photos in different light, different times of day, and different seasons, or with more uniform tree pictures?
>- Obviously, the more different input patterns I can show to the neural network during training, the more it will be able to generalise and to process still more different input patterns after deployment. 
>- This is why, when we train the neural network, we try to present it with as many different inputs as possible. This should also include noisy and imperfect input.

## Generalisation (7)

>- Let us look again at the face recognition example. If I show the neural network only pictures of the person's face from the front in full light and without make-up, then the neural network will probably not be able to identify this person when make-up is applied and the shadows fall differently. 
>- But if I already trained the network using different light sources and different kinds of make-up, then these deviations from the optimal picture will already be accounted for in the network weights. 
>- By having been trained with images that contain always the same face but with different lighting and make-up, the neural network will have learnt that light and make-up do not constitute features that distinguish different input patterns from each other but are a kind of noise in the data.

## Generalisation (8)

>- Conclusion: It is important to remember that, as opposed to what one would normally think, good neural network training is not done with optimal and perfect data, but it is best done with noisy and imperfect data. 
>- In this way, the neural network can best learn to generalise and to distinguish between the features that it *should* learn and the accidental noise that it should ignore.

# Design considerations

## Stages in the design of an ANN

- Configuration.
- Training.
- Application.

## Configuration

Configuration:

1. Number of layers (input, output, hidden).
2. Number of neurons per layer.
3. *Semantics of input and output.*

## Configuration

>- Handwriting recognition: 
    - Input: Black and white pixels on a 8x8 grid (64 inputs). 
    - Output: the recognized character (26 outputs).
>-  Car control:
    - Input: Video image on 100x100 grid (10,000 inputs). Or: Input from distance sensors, radar and other sensors.
    - Output: Turn right/left, accelerator, brake (4 outputs).
    - Example: <https://www.youtube.com/watch?v=FKAULFV8tXw>


## Training and validation set

>- All the input patterns used for the training of the network are together called the *training set.* 
>- Usually what we want from a neural network is not only to memorise the training set, but we want it to be able to recognise and categorise *new* patterns that are not part of the training set. 
>- This is why for testing we would use a second set of input data, which is called the *testing* or *validation set*, and which is *not* part of the training set. 
>- In this way, we can test if the neural network can actually recognise new patterns that it has never seen before. 

## Features of the input data

>- The “features” are those properties of the input data set that are relevant to us and that we want the network to recognise.
>- So the point of the neural network is to learn to recognise a particular set of *features* of its input.
>- For example, a network for face recognition should learn to identify features like the eyes, the nose and the mouth of a person.
>- On the other hand, we don't want it to memorise the shadows that are falling onto a face in a particular picture. We also don't want it to memorise a particular cosmetic (make-up) or some temporary feature of a face, like a pimple. 
>- Although these are *part* of the input data, they are not the *features* we are interested in. 

## Minimisation of error

>- As the network learns and minimises the output error, it converges towards the desired output. The error over time goes down as the network converges towards the desired behaviour. 
>- If something is wrong with the network architecture, for example, if the network does not have enough neurons or enough hidden layers, then the network will not successfully converge. 
>- In this case, we will see that, although we keep training the network, the error stops getting smaller at some point and stays constant. 
>- Then we know that this particular network is not able to successfully learn the provided input patterns and that we either need to simplify our input or make the network bigger or try an entirely different network architecture.

## Problems with neural network design

>- The more hidden neurons a network has, the more synapses it has. Therefore, its capacity to learn will be higher.
>- If bigger networks perform better, why not use very large numbers of neurons and layers?
>- The bigger the network, the longer it takes to train, and this relationship is exponential. 
>     - If every neuron of one layer is connected to every neuron of the next layer (which we call a fully connected network), then in one layer of 10 neurons, you have 10 synapses. If you have a second layer, you have 100 synapses. If you have a third layer, which is the same size as the second, and to which the second fully connects, you will have 100 × 100 = 10,000 synapses. And so on. 

## Overfitting (1)

>- Another problems is what is called “overfitting.”
>- The synapses in a neural network essentially try to memorise their input patterns. 
>- If a neural network has too few synapses, then it will not be able to distinguish between all the relevant features of the input, and it will not converge. That is, its output error will not go towards 0. 
>- On the other hand, if it has too many synapses, then it will be able to just memorise every single feature of the input, and it will not generalise. 

## Overfitting (2)

>- In the case of the face recognition program, you can imagine that a network that is big enough might be able to memorise not only the face but also the shadows cast by the light and the make-up on the face. 
>- Such a network will not need to learn the difference between *relevant features* and *noise* in order to reduce its output error, because with its many synapses, it can just memorise all the input information and still achieve a low output error. 
>- It is only when the network is unable to memorise accidental noise, because it does not have enough synapses to do that, that it is forced to learn the difference between relevant features and accidental noise. 
>- It does this by encoding the relevant features that appear again and again in the synaptic weights, while one-time noise is not encoded and therefore later not recognised by the network. 

## Overfitting (3)

>- This is the reason why, when designing a neural network, you don't want to make it either too big or too small for the task at hand. 
>- Finding just the right dimensions for a neural network is an art. If done right, the network will optimise learning time, accuracy and the ability to generalise, all at the same time. 
>- Although there are some rules of thumb (different for every network type), neural network researchers get better at this only through long experience and by trying out different parameters and seeing what works best.

# Examples

## Neural network example

Train a neural network to recognise colour contrast:

<https://harthur.github.io/brain>

Look at the weights! (Click on “code”)

## More examples

Here are many more examples:

<https://cs.stanford.edu/people/karpathy/convnetjs/>

## Neural Networks Without the Math

I wrote a little book explaining neural networks. It is essentially the same content as these lecture notes, but explained a little more, with more examples and nice pictures.

You don’t need it for this class, but if you are interested in neural networks, you might like to have a look.

It is available on Amazon and other online bookstores, both as ebook and paperback.

![](cover-small.jpg)\

