---
title:  "AI and Society: 16. What is the mind?"
author: Andreas Matthias, Lingnan University
date: September 30, 2019
...

# Main Theories in the Philosophy of Mind

## Main theories

There are two main theory groups in the Philosophy of Mind:

>- Dualism
>     - Substance dualism
>     - Property dualism
>     - Epiphenomenalism
>- Monism
>     - Physicalism
>     - Materialism
>     - Reductive materialism (Identity Theory)
>     - Eliminative materialism
>- In addition, we need to understand
>     - Functionalism
>     - The Extended Mind Thesis

# Summary: Selected theories of mind and their relevance to AI

## Mental states

The whole discussion is about the true nature of mental states, like:

>- Pain, anger, thinking, beliefs, calculating, hope, visual perception (seeing “red”), qualia.
>- “Qualia:” The subjective “feeling” of consciousness or a sensory perception.
>     - *How it feels* to see red, or to taste chocolate.

## Dualism: mind $\neq$ body

>- Mind substance is different from body substance: *substance dualism.*
>- Problems:
>     - Brain damage leads to loss of cognitive abilities. Why?
>     - How can an immaterial cause can have a material effect if the material world is causally closed?

## Dualism: Epiphenomenalism (1)

Epiphenomenalism is a dualist theory, in which the body can have causal effects on the mind, but the mind cannot have causal effects on the body.

This solves both problems above:

>- Since the body can have causal effects on the mind, the theory explains why brain damage can impair cognitive abilities.
>- Since the mind cannot affect the body, the theory does not have to explain how an immaterial cause could affect the material world.

## Dualism: Epiphenomenalism (2)

>- So from a philosophical, argumentative perspective, it is a fine theory that solves the immediate problems of dualism. From the perspective of common sense, it is not a very satisfactory solution:
>     - Why would we have a mind that has no effect on our actions?
>     - Introspection disagrees: we think that our beliefs, intentions etc *do* affect our actions!

## Monism: There is only one basis for both mind and body

>- This one basis can be:
>     - material: materialism; or
>     - mental: idealism or solipsism (there’s only *one* mind around: me!)
>- Materialism can be:
>     - Reductive materialism (=identity theory): Mental states *are* brain states.
>     - Eliminative materialism: There are no mental states. All talk of mental states is misleading and talking about something that does not exist.

## Reductive materialism = identity theory of mind (1)

Mental states *are* brain states.

>- “Reductive:” a “reductionist” explanation is one in which I explain a higher-level phenomenon completely in terms of a lower-level phenomenon. The higher-level concept can thus be completely “reduced” to a lower-level concept.
>- Examples:
>     - Heat is identical to the mean kinetic energy of molecules
>     - Sound is identical to waves of different air density
>     - Colour is identical to light of different wavelengths
>- These lower-level concepts completely explain the higher-level concept and can fully replace it. The higher-level concept is just an abbreviated way of speaking about the lower-level concept.

## Reductive materialism = identity theory of mind (2)

Supporting arguments:

>- MRI and similar visualisations of working brains correlate well with mental states: When I am happy, particular areas in my brain show activity. When I am frightened or sad, other areas light up. This correlation is strong and consistent.
>- We can also reverse this effect: damage in particular areas of the brain will cause corresponding functions to be lost.
>- Or: stimulation of particular brain areas (or drugs) can cause particular mental states to appear (for example, phantom perceptions of light, smells etc)

## Reductive materialism = identity theory of mind (3)

Counter-arguments:

>- No two people have the same brain. Brains develop differently in different individuals. Yet we all think that we have the same mental states (hope, fear, love etc).
>- Even more different are brains of other species (dogs, cats, birds). Still, we would want to maintain that cats or bird can “be afraid.” -- How could this be if my fear was *identical* to a particular physical state of my brain?
>- Subjective experience indicates that we indeed do have mental states and they don’t feel like they are only states of our brains.

## Reductive materialism = identity theory of mind (4)

>- Daniel Dennett (philosopher) describes a thought experiment where you keep your brain alive outside of your body, in a lab. Your brain is connected to your body via wireless signals. Now you (your body) walk over to your brain, sitting “there” in its box full of machinery to keep it alive, and look at it. Do you have the sense of looking at yourself in any way?
>     - (No). But is this a good argument? Any counter-arguments?
>     - This is  a weak argument: introspection is particularly bad in revealing information about our bodies. We don’t feel our blood circulation, we don’t have any introspection about how our digestion actually works, we are often mistaken about what foods are good for us, and consume bad foods that we, by introspection into our mistaken cravings, think that we want.


## Eliminative materialism (1)

>- Eliminative materialism seeks to “eliminate” (remove) all mentalistic vocabulary.
>- Similar cases of elimination of misleading vocabulary in the history of science:
>     - Phlogiston (the supposed stuff that burns when there’s a fire)
>     - Cosmic ether (the supposed stuff that fills the void between stars and in which light waves propagate).

## Eliminative materialism (2)

>- The progress of science replaced these concepts entirely with better concepts. It eliminated phlogiston and the cosmic ether, because knowledge about oxidation in chemistry and the propagation of light in the vacuum did not any more require these wrong explanatory concepts!
>- In the same way, the eliminative materialist wants to get rid of the (supposedly misleading and wrong) mentalist terms in which we describe the mind.
>- Again, this does not seem plausible to most of us. Most of us *do* think that mental states are something real that needs to be explained rather than eliminated.


## Mary’s room (“qualia”) argument

The "Mary's Room argument" or "knowledge argument" (Frank Jackson, 1982) is an argument *against* the identity theory of mind.

## Mary's Room

> Mary is a brilliant scientist who is, for whatever reason, forced to investigate the world from a black and white room via a black and white television monitor. She specializes in the neurophysiology of vision and acquires, let us suppose, all the physical information there is to obtain about what goes on when we see ripe tomatoes, or the sky, and use terms like ‘red’, ‘blue’, and so on. She discovers, for example, just which wavelength combinations from the sky stimulate the retina, and exactly how this produces via the central nervous system the contraction of the vocal cords and expulsion of air from the lungs that results in the uttering of the sentence ‘The sky is blue’. [...] What will happen when Mary is released from her black and white room or is given a color television monitor? Will she learn anything or not? (Jackson 1982)

## Mary's Room

>- Mary is a scientist who knows everything there is to know about the *science* of color, but has never *experienced* color.
>- The question that Jackson raises is: once she experiences color, does she learn anything new?
>- Jackson claims that she does.

## Mary's Room

The argument:

>- Mary (before her release) knows everything *physical* there is to know about other people.
>- Mary (before her release) does not know everything there is to know about other people (because she learns something about them on her release).
>- Therefore, there are truths about other people (and herself) which escape the physicalist story.

## What is it that Mary will learn?

>- What is it exactly that Mary will learn that she didn't know before?
>- **Qualia: The way an experience *feels* to the subject of the experience.**

## Qualia

>- If Mary does learn something new, it shows that qualia (the subjective, qualitative properties of experiences) exist.
>- If Mary gains something after she leaves the room — if she acquires knowledge of a particular thing that she did not possess before — then that knowledge, Jackson argues, is knowledge of the qualia of seeing red.
>- Therefore, it must be conceded that qualia are real properties, since there is a difference between a person who has access to a particular quale and one who does not. (Wikipedia)

## Refutation of physicalism

>- Jackson argues that if Mary does learn something new upon experiencing color, then physicalism is false.
>- Specifically, the knowledge argument is an attack on the physicalist claim about the completeness of physical explanations of mental states.
>- Mary may know everything about the science of color perception, but can she know what the experience of red is like if she has never seen red?
>- Jackson says that, yes, she has learned something new, via experience, and hence, physicalism is false. (Wikipedia)

## Counterarguments to Mary's Room

>- Two meanings of “know”.
>     - "Knowing that"
>     - "Knowing how"
>- Possible pre-linguistic, pre-propositional knowledge from direct experience.
>- But can we call this "knowledge?"
>- The argument seems to be based on confusing these two types of knowledge.
>     - In the premise, Mary "knows (that)" all that is to know.
>     - In the conclusion Mary "learns (how)" something new.
>     - The argument is (possibly) not conclusive as stated.
>- Another counter-argument: If Mary already knows *everything* there is to know about colour, she would also know how "red feels like." There is no mysterious "qualia" left over to explain.

## Functionalism (1)

A mental state (e.g. fear) is defined by the role it plays in relation to:

>- particular body states (screaming, trembling, high blood pressure)
>- the environment (a fearsome thing must be present),
>- and in relation to other mental states (beliefs: a fearsome thing is around; desire: to run away; perception: of something that causes fear).

. . . 

### Functionalism:

The essential defining feature of a mental state is the set of its causal relations to environmental effects on the body, other mental states, and bodily behaviour.


## Functionalism (2)

>- All these *together* **define** the mental state as what it is, and **justify** it as appropriate.
>- If, for example, the cause of fear in the environment was missing, we would call the fear *inappropriate* (a hallucination, perhaps), and deal with it by convincing the subject that his fear is not warranted.
>- Or if someone had the fear but not the desire to flee, or the fear without the belief that there is a danger present; then we would not accept such “fear” as appropriate and we would say that this person is confused, crazy etc.
>- Counter-argument: Chinese nation argument (see above).

## Functionalism (3): Implementation independence

>- For a particular mental state to be present, some *appropriate functional organisation* is needed, but *not* identical material organisation!
>- *Functional organisation $\neq$ physical implementation!*
>- This is called: *“Implementation independence.”*
>- “Physical implementation” means: how the function is realised in the physical world. How something is constructed physically in order to fulfil this function.

## Functionalism (4): Implementation independence

>- Example: Lecture notes are the same lecture notes no matter whether they are written by hand on paper, printed out, read aloud in class, or presented in a video.
>- The “lecture notes” are a functional concept that is independent of its physical realisation. 
>- We don’t call something “lecture notes” only because it’s printed, or because it is projected on a wall. The physical form of the lecture notes is not essential.
>- “Lecture notes” are defined as such by the functional relations they have to lectures, universities, a student’s career, examinations, class readings and other class work, and so on. 
>- Every piece of information that has the same functional relations will be considered “lecture notes,” no matter how it is delivered physically (as an audio file, a book, a movie).


## Functionalism (5): Hardware and software

>- The functionalist understanding of mental states has sometimes been compared to the distinction between “hardware” and “software” in computing.
>- The same kind of software (for example, a web browser) can run on totally different hardware.
>- All these web browsers are functionally equivalent, although they are made by different companies, they are built in different ways, and they run on different and incompatible hardware. They run on big screens, wall projectors, and tiny phone screens.
>- In the same way, we can imagine the “mind” to be something like the “software” of the brain. 
>     - If this was the case, then the same mind could be “run” on different brains.
>     - Also, it would be possible to “run” the mind on a computer with entirely different hardware from the brain’s hardware.
>     - But all these different minds might be functionally equivalent: all might have equivalent (but differently realised!) states of fear, hope, beliefs and so on.

## Functionalism (6): Physical realisability

>- BUT: Implementation independence $\neq$ physical realisability!
>     - Some function can be thought of in an implementation-independent way, but be impossible to realise physically in this way!
>     - Examples:
>          - Very small humans (say, 1 mm small): the cells would become too small to be able to work.
>          - Very big insects: house-sized insects wouldn’t be able to breathe by air diffusion!
>- In a similar way, although “mind” might be implementation independent, it might be impossible to actually implement it outside of brains for physical reasons having to do with particular properties of the mind and the brain! (Nobody knows, of course).
>- A functionalist theory of mind leaves this empirical issue open.

## Philosophical behaviourism

>- Mental states *are* behaviours. They are just another way of talking about behaviours. 
>- If someone is “in fear,” then this is equivalent to saying that he will be exhibiting the behaviours of hiding or running away, sweating, having faster heartbeat etc.
>- “Being soluble” as analogy to “wants a Caribbean holiday”: operational definitions: “if I put X in water, it will dissolve.” If someone wants a Caribbean holiday, he will perform particular actions. If he doesn't, then obviously he doesn't want such a holiday.

## Functionalism vs behaviourism

>- *Can you give an example where we can clearly see how functionalism is different from behaviourism?*
>- Braitenberg vehicles:
>     - The behaviourist would say that if the vehicle behaves as if it feared the light, then it does fear the light. There is no sensible definition of "fear" beyond what can be expressed in the vehicle's behaviour.
>     - The functionalist would say that "fear" is a mental state that *is caused* by other mental states (like the belief that one saw something dangerous) and in turn *causes* other mental or body states (like the belief that running away will avoid the danger etc).
>     - The Braitenberg vehicle fulfils the conditions for behaviourist "fear," but it does not have the mental and body states required for genuine functionalist "fear." It just does not have any "beliefs" or other mental states that would qualify as functional equivalents of our mental states that accompany fear.

## Functionalism vs behaviourism

>- Another example: Actors on a stage, playing.
>- A behaviourist would watch them act “fear” and conclude that they have a mental state of fear.
>- A functionalist would notice that their supposed mental state of “fear” is not related in the appropriate way to other mental states: they don’t forget their lines, for example. They don’t have the desire to flee. They don’t have the belief that they are in danger. Therefore, they cannot have a genuine mental state of fear.


## Functionalism vs behaviourism

>- You see now that the Chinese Room argument is a functionalist’s criticism of a behaviourist claim!
>- The Turing Test is a purely behaviourist position: “What behaves as if it was intelligent, *is* intelligent.”
>- The Chinese Room argument argues that behaviour is not enough. The Chinese Room *does* behave as if it could understand, but, looking at its functional states (the inside of the room and how it works) we can clearly see that it lacks the right functional organisation.


## Arguments against behaviourism

>- Behaviourism denies inner aspect of mental states (e.g. “pain”).
>- Behaviours can be faked (for example, actors on a stage or in a movie).
>- Mental states can be present but not express themselves in behaviour (someone might be in fear, but paralysed and unable to run or scream).
>- Still, in the form of symbolic AI and the Turing Test, behaviourism about machine intelligence is a powerful idea in the history of AI.


## Arguments against functionalism: Chinese nation (1)

Absent qualia problem (Chinese Nation argument): We already discussed this in session 4, but I want to remind you of this argument again here.

. . . 

>- What if we assigned each Chinese person the function of a neuron in our brain?
>- Chinese are used for this example because they are so many. (You couldn't make a working brain out of Greeks, for example.)
>- Assuming each Chinese person copies exactly the behaviour of a real neuron in a brain, and that we have as many Chinese as neurons in our brains available for the experiment,
>- Then we should be able to perfectly simulate a brain using Chinese people.
>- That brain should work *exactly* as the original brain.
>- It should have consciousness and qualia.

## Arguments against functionalism: Chinese nation (2)

>- But where do they come from? Each individual Chinese replaces only a single (dumb) neuron.
>- How does the consciousness come into the system? Is it at all plausible to assume that such a "brain" would be conscious and have real mental states?
>- If the Chinese brain couldn't have mental states, functionalism must be wrong.
>- *What do you think?*

## Answers to the Chinese Nation argument (1)

>- Possible answer 1: The Chinese Nation brain doesn't have qualia, and we don't have qualia either (since we are functionally equivalent to a Chinese Nation brain).
>     - This is not good for functionalism. It is counter-intuitive to say that we don't have qualia.
>- Possible answer 2: (Dualism:) The whole experiment misses the point. Qualia in the human mind are not material, and our brain is not functionally equivalent to the Chinese Nation system.
>     - This is not good for functionalism either. It rejects the functionalist premise entirely.

## Answers to the Chinese Nation argument (2)

>- Possible answer 3: The Chinese Nation brain *does* have qualia.
>     - The only position compatible with functionalism, but slightly implausible. *Who* is the perceiving subject of these qualia? (The Chinese Nation as a whole).
>     - This is similar to the Systems Reply to the Chinese Room argument.
>     - It opens up the possibility of *strong AI* (since computers can have qualia, and thus, a genuine *mind*).
>     - So, for the functionalist strong AI, the systems reply to the Chinese Room, and the assumption that the Chinese Nation brain would have qualia of its own all go together.


## Consequences of these theories for AI

>- Theories of mind only have consequences for the *strong* claim of AI (“machines can have a mind and mental states”).
>- They don’t affect *weak* AI (“machines can act intelligently”).

. . . 

Effects of:

>- Substance dualism: Computers could never have a mind, as long as we don’t find a way to put “mind substance” into them.
>- Reductive materialism: Computers could not have a mind, as long as they are not constructed out of brains. (Because mental states *are* brain states).
>- Functionalism: Machines *could* have minds, if they have the appropriate functional organisation. But to implement such minds outside of the brain *might* actually turn out to be physically impossible for some (yet unknown) reason.


# Functionalism in AI

## Implementation independence (again)

>- If functionalism in the theory of mind is correct, then it is at least logically possible for a mind essentially like ours to be made of quite different stuff from ours.
>- The reason is that what is essential are *the structures and their functional organisation,* not the *material* of which the elements of the structures are made.
>- A Turing machine can be implemented on *any* hardware! The states are completely abstract.
>- As a consequence, if the mind is a Turing machine, then the mind can be implemented in any hardware too!

## Marr's levels of computation (1)

>- Three levels of analysis for addressing a computational problem (David Marr, 1982)
>- At the **computational level** of analysis, one specifies *what* cognitive function is being computed (for example, an addition.)
>- At the **algorithmic level**, one describes *how* the function is being computed, the algorithm used to compute it (for example, how to perform an addition of two long numbers, in principle).
>- And at the **implementational level**, one describes *how the steps* of that algorithm are implemented, that is, the underlying mechanism that performs the computation (how to add two long numbers using pen and paper, where to make ink marks on the paper etc).
>- *Exercise: Describe "a wall" at the three levels of description!*

## Marr's levels of computation (2)

A wall:

>- Computationally: A separation between two areas that makes it hard to cross from one area to the other.
>- Algorithmically: Determine the beginning and end point of each straight wall segment, and connect these two points with some kind of barrier. Repeat the same for all segments of the wall.
>- Implementation: Take bricks and mortar. Arrange the bricks in a row, put mortar on top, add a second row of bricks on top, then proceed in the same way for as many rows as desired.

## Marr's levels of computation (3)


>- Note that there are *many different* but equivalent implementations for the same algorithm! There are walls of bricks, of cement, of wood, stones, etc.
>- Note also that the implementation is largely independent of the algorithm! The implementation talks only about wall segments, and does not address the problem of planning where the wall goes.

## Marr's levels of computation (4)

>- The three levels are relative to a computational problem.
>- What is at the implementational level relative to one computational problem can be at either the computational or algorithmic level relative to another.
>- Marr (1982) suggested that computational level analysis could be carried out *largely independently* of algorithmic level analysis, and the latter *largely independently* of implementational analysis. (See the wall example above!)
>- *Infinitely many algorithms can compute the same function.*
>- Moreover, the algorithmic level in turn underdetermines the implementational level: *a given algorithm can be implemented in infinitely many possible ways.*

. . . 

*What are the consequences of this?*

## Functional equivalences

>- This is the basis of the "functional equivalence" thesis!
>- It allows us to study the mind without studying neurons.
>- It lets us hope that we can implement a mind on a differently structured architecture.

## Functional equivalences

>- *Can you find examples of other functional equivalences in everyday life?*
>- A wall is a wall no matter whether it is made of mud, of stone, of wood, of human bodies.
>- A car is a car whether it is made of steel or plastic, and independently of the details of the engine which is inside it. The detailed structure of various cars is very different, yet they are all cars.
>- A university is a university only due to functional equivalence with other universities. Neither the arrangement of buildings, not even the number and size of buildings, the type of campus, or the numbers and identities of the students and professors make it a university. All that defines a university is equivalence in function.
