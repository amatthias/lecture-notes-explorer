---
title:  "AI and Society: 1. Introduction"
author: Andreas Matthias, Lingnan University
date: September 2, 2019
...

# Introduction to the course

## Welcome!

Some of my names are:

- Andreas Matthias
- “Mr. Ma”
- “Andy”

(use which one you like!)

## Get to know each other

Now please everybody say something about him­ or herself:

- Your name and how you would like me to call you
- Why do you study philosophy?
- What area of philosophy and which philosopher do you like most?
- Have you ever programmed computers?
- Have you studied Philosophy of Mind or AI before?
- Why did you choose this class?

## Presentation of course outline

(see outline in Moodle)

## Online resources

- We use Moodle for this course.
- The reading, lecture notes and announcements will be available through Moodle.
- Make sure you know how to use it and look at it regularly.

## Additional reading material

Some additional material for this course can be found at:

<https://moral-robots.com/category/ai-society>

It is tagged with “Textbook:” in this presentation. When you see “Textbook” links, you are required to read them as part of the course materials.

## “Homework” for next session
Please find and post in Moodle at least three links to interesting Internet resources that show the present state of the research and applications of Artificial Intelligence.

. . . 

For each link, add a short explanation (about 1 paragraph of text) explaining why you think that this link is relevant to the philosophy of AI, and what philosophical questions it poses.

. . .

These could be, for example, YouTube videos showing “intelligent” machines in action, interviews with researchers or philosophers about AI, explanations about what AI is, or 
similar resources.

. . .

Please don't post links which have already been posted by others. You must see what others have found first and make sure that your links are unique. Only unique links with clear explanations of what philosophical issues they raise will be graded!

## The End. Thank you for your attention!

Questions?

<!--

%% Local Variables: ***
%% mode: markdown ***
%% mode: visual-line ***
%% mode: cua ***
%% mode: flyspell ***
%% End: ***

-->

