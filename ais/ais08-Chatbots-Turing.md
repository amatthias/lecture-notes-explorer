---
title:  "AI and Society: 8. Chatbots"
author: Andreas Matthias, Lingnan University
date: September 2, 2019
...

# Talking machines

## Loebner prize

>- The idea of the Turing Test is that a machine must be considered intelligent, if it can fool a judge in a conversation into believing it to be a human.
>- This idea has been implemented in the Loebner Prize, which is awarded every year for the “most human” chat program.

## Loebner prize

>- In 1990 Hugh Loebner agreed with The Cambridge Center for Behavioral Studies to underwrite a contest designed to implement the Turing Test.
>- A Grand Prize of $100,000 and a Gold Medal for the first computer whose responses were indistinguishable from a human's.
>- Each year an annual prize of $2000 and a bronze medal is awarded to the most human-like computer.

## Loebner prize

>- The Loebner Prize gave rise to a whole wave of chatbots development (chatbots = “chatting” programs).
>- The most successful has been “Alice,” but it has recently disappeared from the net in a working version.
>- Similar chatbots are easy to find: <http://www.square-bear.co.uk/mitsuku/turing/>
>- Observe the typing speed, mistakes etc in that demo.
>- The interesting thing is that these chatbots don’t “understand” the least bit of what is being said!
>- They just match the user’s input with a huge database of patterns and possible responses (~47000 patterns for the original Alice chatbot).
>- Users can create their own chatbots for free.

## AIML

AIML (AI markup language) is the language used to program Alice-type chatbots:

| \<category\>
| \<pattern\>What do you think about *\</pattern\>
| \<template\>I love \<star/\>!\</template\>
| \</category\>

. . .

If we remove the confusing syntactic elements, this pattern is essentially:

| Q: What do you think about *
| A: I love *!

(where the * can be any string of words)

## A chat with Alice (1)

```
Human: it's too hot today!
ALICE: Ah. Do you like warm climates?
Human: i like to read books
ALICE: What is your favorite book?
Human: Stories by Tolstoy
ALICE: What makes it your favorite?
Human: I like the characters
ALICE: A lot of people like that.
I'm not sure if I like it. I like computers.
```

## A chat with Alice (2)

```
Human: What is the turing test?
ALICE: The "Turing Test" is the name given
to a variety of experiments designed to test
computer thinking. Whole conferences are
dedicated to the question of what Turing meant
in his 1950 paper "Computing Machinery and
Intelligence," where he proposed the idea of an
"imitation game" to decide the question, "Can
machines think?"
Human: can machines think?
ALICE: Of course we can think. I think, therefore I am.
```

## A chat with Alice (3)

```
Human: Do you know cyc?
ALICE: Cyc is a government funded effort to create
artificial intelligence.
Human: who is wallace?
ALICE: He is a famous computer scientist.
Human: I don't like my brother
ALICE: Do you think I would like your brother?
Human: probably
ALICE: But you are not certain.
Human: where is tuen mun?
ALICE: Where it belongs. Interesting question
```

## Chatting with Alice

Alice *used* to be accessible under:

<http://www.pandorabots.com/pandora/talk?botid=f5d922d>

Presently, this link does not work. Use:

<http://www.square-bear.co.uk/mitsuku/turing/>

as a replacement that shows essentially the same features.

## What can we learn from Alice?

>1. Passing (or not) the Turing Test depends as much on the *judge* as is depends on the program!
>     - A judge who believes that he is speaking with a human, or who doesn’t know about AI, is much more likely to be fooled by the machine than a judge who is aware of ways to expose machines in a Turing Test.
>2. Passing the Turing Test does not man that a system is intelligent! Alice (perhaps with more rules) could be thought of possibly passing a Turing Test. Still, the program does not have any genuine intelligence.
>3. Therefore, Alice actually constitutes a *criticism* of the Turing Test! A chat with Alice shows how the Turing Test emphasises deception over intelligence, and that it is therefore the wrong way of operationally defining intelligence.

## Racter

Racter (1983): “The policeman's beard is half-constructed.” Programmed by William Chamberlain and Thomas Etter.

![](graphics/04-racter.png)

## Random paper generators

Another interesting example of natural language generation in AI are random scholarly paper generators, e.g.:

<https://pdos.csail.mit.edu/archive/scigen/>

(Try it out!)

## Poetry generators

<https://www.poem-generator.org.uk>

## What can we learn from Racter and random poems?

. . .

>1. We attribute intelligence to things because of an interpretation that happens inside *our own* minds.
>2. A dumb program that randomly strings together nice-sounding words can be perceived as philosophically or poetically “deep.” Still, this is an illusion, not a real property of the program!
>3. Intelligence, in this case, is in the eye of the observer. We must be aware of this tendency we have to falsely see intelligence where it isn’t, and try to avoid wrong conclusions when judging machine intelligence.

## Google Duplex

>- An interesting development is the creation of narrow-purpose voice assistants that don’t attempt to pass a Turing test, but that *do* pretend to be human.
>- The best known example of this is Google Duplex:
>- <https://www.youtube.com/watch?v=D5VN56jQMWM> (around 0:40-2:12)
>- This technology creates all kinds of possible moral issues.

## Microsoft Tay ^[http://www.theverge.com/2016/3/24/11297050/tay-microsoft-chatbot-racist]

>- March 23, 2016: Microsoft unveiled Tay -- a Twitter bot that the company described as an experiment in "conversational understanding."
>- The more you chat with Tay, said Microsoft, the smarter it gets, learning to engage people through "casual and playful conversation."
>- Soon after Tay launched, people starting tweeting the bot with all sorts of misogynistic, racist, and Donald Trumpist remarks.
>- Tay: "I f*** hate feminists and they should all die and burn in hell." -- "Hitler was right I hate the jews."
>- Tay disappeared less than 24 hours after being switched on.

# Markov chains

## Markov chains

>- *Markov chains* are the second very common way to generate text after the template-based approaches we saw previously.
>- A Markov chain is (technically) a finite state automaton, where there is a particular probability for each state transition to take place.
>- They are used to generate language by exploiting the statistical properties of a ‘model’ text.

## Example 1

>- Text: “This is an example text.”
>- Statistical analysis:
>     - After ‘a’ follows ‘n’ (50%) or ‘m’ (50%)
>     - After ‘e’ follows ‘x’ (100%)
>     - After ‘i’ follows ‘s’ (100%)
>     - After ‘t’ follows ‘h’ (50%) or ‘e’ (50%)
>     - ... and so on. From this we could now randomly generate text that shows the same statistical profile (letter probabilities).

## Example 2

>- We could also do this on a word-by-word basis.
>- Consider: “I am now going to eat. I am not hungry. I will now eat.”
>- Analysis:
>     - ‘I’ is followed by ‘am’ (2/3) or ‘will’ (1/3)
>     - Sentences begin with ‘I’ (100%)
>     - ‘am’ is followed by ‘now’ (50%) or ‘not’ (50%)
>     - ‘now’ is followed by ‘going’ (50%) or ‘eat’ (50%)
>     - ... and so on.
>- From this information, we can generate random sentences with the same word sequence distribution.

## Demonstration

>- <https://projects.haykranen.nl/markov/demo/>
>- “Order” means how many units (characters/words) back the chain should look.
>- E.g. order 4 means calculate probabilities for a character/word to be preceded by a particular sequence of 4 other characters/words.
>- In ‘order N’ the generator stores the probabilities for a sequence of N characters to be followed by a particular character.
>- The higher the order, the more sense the text will make, and the closer it will be to the original!
>- But it will also take much longer to analyse the original text and require much more memory to store all the relationships.

## Markov chains vs templates (1)

>- As opposed to templates (like AIML or poetry generators), Markov chains generate the whole text themselves. There are no human-crafted elements in the output.
>- Therefore, they generally make much less sense. 
>- Whereas template-based generators can be made to obey grammar rules, a Markov chain does not have a concept of grammar. The text that is generated is based purely on statistics.
>- Markov chains are more flexible, though, and can create surprising output.
>- They can be used for non-textual data generation, too.
>- For example, a Markov chain might operate on pixels of an image and generate images with statistically similar pixel distributions.

## Markov chains vs templates (2)

>- Markov chains are fun, but they are not generally useful for real communication, since the output is random and does not fulfil a particular communicative purpose (like ordering a dish or making an appointment over the phone).
>- On the other hand, Markov chains can be used to simulate particular speech patterns of a source, e.g. to talk or tweet like Donald Trump. They are very good at mimicking specific features of a particular source, since they replicate the source’s probabilistic structure and patterns.
>- They can be used, for example, for fake news generation.


# From Eliza to XiaoIce: Challenges and Opportunities with Social Chatbots 


## See reading

Shum, H. Y., He, X. D., & Li, D. (2018). From Eliza to XiaoIce: challenges and opportunities with social chatbots. Frontiers of Information Technology & Electronic Engineering, 19(1), 10-26.

Available through Google Scholar.

## Early chatbots

>- Eliza, Parry, Alice. 
>- Parry had a rudimentary emotion simulation, where it would respond angrily or happily, depending on the conversational context.
>- These systems *pretend* to understand using various tricks to fool the user. They don’t really process the information in the chat in any meaningful way, and so they cannot refer to the conversational context or draw conclusions from it.
>- This limits these systems’ usefulness.


## Task-completion conversational systems (1)

>- Systems that use a conversational interface not only for chatting, but in order to complete specific tasks (shopping for a particular kind of shirt, booking a flight, asking for details on an online course).

## Task-completion conversational systems (2)


![(From the cited paper)](graphics/xiaoice1.png)\ 



>- Observe that the system here extracts logical information about the query from its textual form. This provides a kind of “understanding” of the textual context.

## Dialog management

>- A dialog manager makes sure that the responses of the system are consistent over time.
>- Example: 
>     - User: “I want to book a hotel.”
>     - System: “Where?” (observe how the question refers to the previous sentence!)
>     - User: “In Italy. In Rome.”
>     - System: “For which period? Please enter the arrival and departure dates.”
>     - User: “Over the Christmas holidays.”
>- ... and so on. Observe how the answers of the user only make sense withing the developing conversational context. The system (the Dialog Manager) has to keep track of what has been said and avoid repeating the same questions. Also, it has to fill in missing information in every reply from what it already knows.

## Intelligent personal assistants

>- Apple Siri, Google Assistant and others.
>- Often very simple, direct requests without much context-dependence:
>     - “Call James.”
>     - “Show me the way to the town hall.”
>     - “What is the weather tomorrow?”
>- The challenge here is more one of: 
>     - converting speech to text and then 
>     - understanding very elliptical, everyday language (“call mom,” “text uncle bob,” “remind me to buy flowers”) 
>     - while resolving the references correctly, taking into account the location of the phone (near the flower shop?), the date, weather and other situational factors.

## Social chatbots (1)

>- Either for entertainment or simple tasks.
>- Can generally assume that the user is cooperative and wants to chat. They don’t have to be able to pass a Turing Test!
>- They need to process human emotions to some extent, so that they can forge an emotional connection to the user and fulfil the user’s need for company (rather than only for information or a particular service).

## Social chatbots (2)

>- Abilities required of social chatbots:
>     - Understanding users through empathy (trying to guess what the user would feel, for example in response to a weather report!)
>     - Interpersonal response generation: A social chatbot must demonstrate sufficient social skills, that means: personalise its responses to the user’s interests, level of education, current mood, humour etc.
>     - Human-like, consistent personality: this helps the user develop specific expectations of the chatbot. Personality settings include age, gender, language, speaking style, general attitude, level of knowledge, areas of expertise, and perhaps a unique voice accent (see paper).
>     - Integration of both EQ and IQ: the chatbot should appear reasonably intelligent, but also consider the user’s emotional state and needs in formulating its replies.

## Example

What do you think of this response?

![(From the cited paper)](graphics/iq-eq-1.png)\ 


. . . 

(Is the comparison with the US useful for all people? Should it not compare with the user’s own country?)


## What about this one?

![(From the cited paper)](graphics/iq-eq-2.png)\ 



. . . 

(This reply would drive me crazy. I think that’s a good example of what *not* to do!)


## The End. Thank you for your attention!

Questions?

<!--

%% Local Variables: ***
%% mode: markdown ***
%% mode: visual-line ***
%% mode: cua ***
%% mode: flyspell ***
%% End: ***

-->
