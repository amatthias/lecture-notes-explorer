---
title:  "AI and Society: 22. AI and Jobs"
author: Andreas Matthias, Lingnan University
date: October 29, 2019
bibliography: papers.bib
csl: andreas-matthias-apa.csl
...


# Introduction

## Sources

For this chapter:

- Frontier Economics (2018). The Impact of Artificial Intelligence on Work. An Evidence Review Prepared for the Royal Society and the British Academy (available online).
- Lexer MG and Scarcella L: “The effects of artificial intelligence on labor markets – A critical analysis of solution models from a tax law and social security law perspective,” available online.
- Other sources (particularly pictures) as cited in the text. 

## The scope of the discussion (Frontier report)

![Frontier: The impact of AI on work](graphics/22-royal-society-impact.png)\ 


## The scope of the discussion (2)

>- AI applications
>     - Applications of AI in specific sectors/occupations.
>     - Forecasts of future AI capabilities.
>     - Computer science on improving AI systems.
>- Impact of AI on work
>     - Analysis of economic and social impact of ICT on work.
>     - Forecasts of future skill demand.
>     - Quantitative estimates of impact of AI on work.
>- Social consequences
>     - Analysis of earlier technological change (industrial revolutions).
>     - Psychology of human-machine interaction.
>     - Economic impact of automation on workers.
>     - Work as a source of meaning.
>- Ethics, politics and regulation
>     - Political and wider implications of AI.
>     - Ethical considerations on the use of AI.
>     - Technoregulation issues.


# AI applications

## Applications of AI in specific sectors/occupations

>- It is often thought that AI and robot workers will primarily replace workers in factories.
>- *Do you think that this is true?*
>- Although factory workers will certainly also be replaced by AI systems, the job loss will not end there.
>- As opposed to previous industrial revolutions that primarily replaced human physical work with machine work, the AI revolution creates machines that can perform very complex tasks better than humans. We will see examples below.

## Past and present automation

>- Automation and the loss of jobs are not new phenomena.
>- Since the Industrial Revolution, human workers have been replaced by automated machines:
>     - Cloth making, weaving in the 19th century (power loom, Jacquard machine).
>     - Car production in the early 20th century (assembly line).
>     - Sales and manual fare collection (bus conductors replaced by Octopus cards,  shops replaced by vending machines, automation at McDonalds).
>     - Street shops replaced by online shopping.
>     - Doormen and liftboys replaced by automatic doors and elevators.
>     - Typists, secretaries, accountants and human computers (!) replaced by electronic computers, word processing, spreadsheet software.
>     - “The Post reported last year that a Foxconn plant in Kunshan, Jiangsu province, had already replaced 60,000 workers with robots.” (SCMP)

## McDonalds cashierless ordering

![](graphics/22-mcd-2.jpeg)\




## Industrial robots don’t threaten jobs

But:

![](graphics/22-industrial-robots-2.png)\




## Distinction between automation and autonomous robots

>- Automation: Use of (dumb or intelligent) machines in industrial production tasks (has a long history, going back to windmills and horse-carts).
>     - Automation has caused loss of jobs throughout the centuries. But we got used to this. These were primarily low-qualifications jobs.
>- Autonomous, learning robots: New phenomenon. 
>     - Traditional machines are not autonomous and don’t learn. Modern robots do.
>     - The job loss here will affect not only blue-collar workers, but also specialists.
>     - (Radiologists who read X-rays, dermatologists, ophthalmologists, insurance specialists, tax and investment consultants, and many other professions).

## What is new

>- Novel capabilities of robots. Previously limited to automation of repetitive tasks. Now they excel at typically human tasks:
>     - Diagnosing diseases from X-ray images and blood samples.
>     - Predicting insurance risk (no bias! Humans are very bad at probabilities!)
>     - Playing chess, go (advanced strategic thinking) and poker (able to deceive humans and to deal with incomplete knowledge and deception by other players).
>     - Replacing actors in movies (deep fake technology, reconstruction of deceased actors, for example in Star Wars).
>     - Driving cars and flying airplanes more successfully than human beings.
>     - Understanding spoken language, which enables efficient dictation to a machine and makes transcription jobs obsolete.
>     - Better and better machine translation, making translators for casual uses (almost) obsolete.


## Doctors

>- X-Ray specialists (radiologists) and dermatologists (Wired, 25.1.2017): 
>     - “Computer scientists and physicians at Stanford University recently teamed up to train a deep learning algorithm on 130,000 images of 2,000 skin diseases. The result, the subject of a paper out today in Nature, performed as well as 21 board-certified dermatologists in picking out deadly skin lesions.”


## Forecasts of future AI capabilities (1)

>- The capabilities of AI systems are likely to further improve:
>- Computers will continue to get faster and data storage cheaper.

## Forecasts of future AI capabilities (2)

>- More complex problems will need to be addressed, driving the development and financing of more capable AI systems, for example for:
>     - prediction of global warming effects and weather models, 
>     - development of stronger data encryption methods,
>     - AI-based search for resources (oil, rare elements needed in electronics),
>     - state surveillance (processing of large amounts of text, voice and video data),
>     - savings in labour costs (taxi and truck drivers, warehouse workers, factory workers),
>     - more targeted advertisements and AI-based marketing in general,
>     - AI-based manipulation of democratic processes,
>     - reduction of costs in health- and eldercare.
>- All these are areas with very obvious, strong incentives for big financial investments.


## Forecasts of future AI capabilities (3)

>- For these areas, machines will need to develop their capabilities:
>     - Faster computation, complex pattern analysis in big datasets (weather, global warming).
>     - Quantum computing (data encryption).
>     - Fast analysis of video and audio data (surveillance).
>     - New methods of surveillance (gait recognition, “seeing” through walls, analysis of stray electromagnetic emissions of equipment).
>     - Autonomy in robot movement in the real world (self-driving cars, buses, trucks; factory automation; hospital robots).
>     - Improvements in robot strength, robot vision and mechanical precision (robot surgery, factory floor uses).
>     - Better understanding of human emotions and the ability to engage emotionally with humans (eldercare, healthcare, emotional manipulation of voters and customers in advertising).


## Computer science research on AI

![](graphics/22-cs1.png)\





## Benefits of automation. Example: driving

Automation is not all bad, though. Here are some statistics on the quality of human car driving:

>- Tri-Level Study of the Causes of Traffic Accidents (1979): “human errors and deficiencies” are a definite or probable cause in 90-93% of the incidents examined. 
>- UK study (1980): driver error, pedestrian error, or impairment as the "main contributing factor" in 95% of the crashes examined.
>- US study (2001): “a driver behavioral error caused or contributed to” 99% of the crashes investigated.
>- NHTSA’s 2008 National Motor Vehicle Crash Causation Survey: Human error is the critical reason for 93% of crashes.
>- (Source: http://cyberlaw.stanford.edu/blog/2013/12/human-error-cause-vehicle-crashes)



# Impact of AI on work

## Analysis of economic and social impact of ICT on work

![](graphics/22-automation-potential.png)\



## Jobs lost in 2020 (percentages)^[http://careerbright.com/wp-content/uploads/2017/07/Technology-vs.-Jobs-in-2020_Infographic-03.jpg]

![](graphics/22-percentages.png)\




## Skills and unemployment

![](graphics/22-social-skills.png)\






## Skill demand and automation in medicine

![](graphics/22-repetitive-interactive.png)\



## The creative/interaction scale (1)

>- Observe in the previous graphic how we can generally divide job skills along two axes:
>     - Interaction-rich (teachers, psychologists, entertainers) vs. interaction-poor jobs (truck driver, construction worker, software developer, farmer).
>     - Creative/flexible (something new happens every day: journalist, psychologist, scientist, artist, businessman, some doctors) vs repetitive (same activities every day: news presenter, factory worker, airline pilot).

## The creative/interaction scale (2)

>- The airline pilot example is interesting. 
>     - One could argue that pilots are “creative” in that their job demands flexible reactions to unforeseeable events during flight.
>     - But, on the other hand, for the overwhelming majority of flights, these events are the same and largely predictable: winds, turbulence, low visibility, mechanical failures of the plane.
>- *So, would a pilot be easily replaceable by an AI system or not? What do you think?*

## Automation and loss of quality (1)

>- Some jobs (like the pilot’s) can be automated but with some loss of features.
>- An autopilot can steer a plane perfectly for the overwhelming majority of flights.
>- In 2016, there were around 2.5 accidents (0.39 fatal accidents) of airplanes per million (!) departures.
>- Assuming that a pilot would have prevented those, while an AI would have crashed, and assuming further that an AI system can fly the plane perfectly in the absence of an accident condition, this gives an increase in accidents of 0.00025% if we replace pilots with AI systems.

## Automation and loss of quality (2)

>- These numbers are just an example estimation. They are not meant to be a serious statement about flight safety. You should just see how you could go about estimating the risk in such scenarios.
>- Even if we are off by multiple orders of magnitude, it is unlikely that the risk from employing an autopilot will ever come close to even a hundredth of a percent.
>- Consider also that around 50% of all crashes are due to pilot error. These would automatically be eliminated by the use of AI too.
>- So it seems that it would be okay to replace pilots with AI systems, even if we lose the pilot’s (very rarely exercised) ability to react creatively to a flight emergency.

## Nothing is really clear

>- It is currently impossible to say whether the loss of jobs due to AI will lead to:
>     - a lack of jobs and the resulting mass unemployment (“Scenario A”); or 
>     - a complete change of required skills as a result of newly-created jobs (“Scenario B”)^[Lexer MG and Scarcella L: “The effects of artificial intelligence on labor markets – A critical analysis of solution models from a tax law and social security law perspective,” available online.].
>- The result is likely to be some combination of the two.



# Social consequences

## Economic impact of automation on workers (1)

![](graphics/22-automation-gdp.png)\



## Economic impact of automation on workers (2)^[Source: https://www.visualcapitalist.com/charting-automation-potential-of-u-s-jobs]


![](graphics/22-automation-wage.jpg)\




## Economic impact of automation on workers (3)^[Source: https://www.visualcapitalist.com/charting-automation-potential-of-u-s-jobs]

![](graphics/22-income-risk.jpg)\


## Economic impact of automation on workers (4)

>- What can we learn from these diagrams?
>     - In the first diagram, we can clearly see that we can draw a straight, falling line through the data points.
>     - There is a strong inverse correlation between GDP per person and jobs in risk of automation. 
>     - Poorer countries have many more jobs in risk of automation than richer countries. 
>- *Why is that?*
>     - The jobs in poorer countries are often manual manufacturing, worker or farmer jobs which can easily be automated.
>     - The jobs in richer countries are often service jobs (high on the interaction scale) or creative/development jobs (high on the creativity scale) and therefore difficult to automate.

## Economic impact of automation on workers (5)

>- Diagram 2 (the first with the coloured bubbles) shows clearly (?) that the automation potential is almost all concentrated on jobs paying less than 30-40 USD/hour.
>- Diagram 3 also shows that the best paid jobs (on the top of the vertical axis) are least likely to be automated. (Although you might disagree with their estimation regarding physicians and surgeons!)


## Full and partial automation of jobs

>- When we talk about unemployment from automation, we often overlook *partial* automation.
>- Job automation is not an all-or-nothing game.
>- For example, robot-assisted “minimally invasive surgery” does not replace doctors.
>- But it  leads to faster turnaround times, earlier discharge of patients from hospitals, and thus a higher hospital capacity, without the need to employ more doctors or nurses. 
>- So in the end, it **does** lead to less employment for medical staff, including specialists. 
>- If we can do the work of 6 doctors with only 4, then 2 doctors will never be employed.
>- This is often overlooked when we talk about unemployment. Greater efficiency is equal to partial unemployment.


# Ethics, politics and regulation

## Worldwide balance of automation (1)

![](graphics/22-industrial-robots.png)\



## Worldwide balance of automation (2)

>- In the previous diagram, you see that South Korea and Singapore are two of the countries with most robots per 10,000 manufacturing workers.
>- But you have to be careful with these numbers.
>- These also depend on the more general kind of manufacturing that is performed in a country.
>- Some kinds of manufacturing (cars, electronics) are done at a big scale and are more suitable for the use of robots. Therefore, good, efficient robots for such products have existed for a long time.
>- For other kinds of manufacturing (crafts, more specialised artefacts) there might not exist suitable factory robots, or the factories might be too small to make the development or deployment of robots in such factories economically viable.

## Worldwide balance of automation (3)

>- So when you see such a diagram, don’t assume that it says something only about robot use or industrialisation levels. It equally might say something about the kinds of products a country manufactures, how centralised the manufacturing sector is, and many other factors.


## Worldwide balance of automation (4)^[futurism.com]

![](graphics/22-developing.png)\


## Worldwide balance of automation (5)

>- Again, this is mostly because of the risk of automation in agriculture-related jobs.
>- Plant management and harvesting can now be done well by machines (at least for some kinds of produce) and this will reduce the need for seasonal workers in agriculture.


## Worldwide balance of automation (6)^[futurism.com]

![](graphics/22-developed.png)\



## Worldwide balance of automation (7)

>- In the cities, it is more the service and transportation jobs that are at risk:
>     - Cashiers, ticket counters, shops (replaced by Internet shopping).
>     - Bus, truck, taxi drivers (replaced by self-driving cars).
>     - Police and security (replaced by elaborate surveillance systems).
>     - Restaurant staff (replaced by ordering machines, takeout, delivery).
>     - Construction workers (replaced by machinery).
>     - Entertainment (cinemas, concert halls, live performances replaced by home entertainment options).


## Rich and poor (1)

>- Especially in HK, the gap between rich and poor is very big. These developments will make things even worse for those who are not qualified for more demanding or creative jobs.
>- Oxfam Hong Kong report (2015):
>     - About nine percent of Hong Kong’s population live in working poor families, 
>     - The number of working poor households in the city reached 189,500 last year, representing a 10.6 percent increase compared with five years ago.
>     - These households include 647,500 people, about nine percent of Hong Kong’s population.
>     - Same report: Hong Kong’s richest one percent owns 52.6 percent of the city’s total wealth, whereas the wealthiest 10 percent take up 77.5 percent of the total wealth.
>     - This is the highest among developed regions globally.

## Rich and poor (2)

>- *When we employ robots, who is most benefited? The rich or the poor?*
>- The income from robot use goes mostly to the already wealthy. Robots are a very efficient machine for the concentration of wealth.
>- Example: Tesla. 
>     - Tesla wants to create their own ride-sharing service (Reuters Oct 20, 2016).
>     - So they have the profit from selling the car, plus the continuous income from the use of the sold car.
>     - (The Tesla sales contract forbids the driver to offer his own commercial ride-sharing!)

## Rich and poor (3)

>- The same for other businesses: Robots allow hospitals and elder-care institutions to save money on human personnel.
>     - Robots are very cheap in comparison: 
>     - TUG robots in hospitals:
>     - Delivery of food, clean linen, medicines to the patient bed: reduction of cost by 50% to 80%.
>     - University of California, San Francisco Medical Center: 25 robots, each 240,000 USD, including all retrofitting and infrastructure work to the hospital itself.
>     - The UCSF Medical Center *generates* an annual income of $1.8 billion!
>     - The robots cost practically nothing.
>- Payback time for new robot installation: China: around 1.5 years, shrinking with time (was 2 years in 2013).


# Technoregulation issues

## The problem of technoregulation

>- When we talk about how technologies affect our life and economy, it looks like we (as societies) are free to decide whether we want to use particular technologies or not.
>- *But is this true? Do societies freely and rationally decide to use or not to use particular technologies?*
>- Who regulates technology? Can we, as citizens or societies, proactively regulate technology?


## Relation between technology and society (1)

Various theories are possible:

>- Technological determinism: Technology determines social developments.
>       - Printing press
>       - Automobile
>- Linear relation of science-to-technology development:
>       - A (mythical) linear succession of: Basic science, applied science, development, and commercialisation.
>- An extreme form of this is "autonomous technology": The thesis that technology is out of human or social control entirely, and develops according to its own logic.
>       - For example, cars lead to streets, lead to fuelling stations, lead to accidents, lead to hospitals, lead to insurance companies, and so on.

## Relation between technology and society (2)

>- Social constructivism: Groups in society compete to influence or control technological designs. The process goes from *interpretive flexibility* to stabilisation of designs.
>       - "Interpretative flexibility" means that each technological artifact has different meanings and interpretations for various groups.
>       - For example, bicycle air tires were not accepted equally by all users.
>       - Some found them comfortable.
>       - Others found them ugly.
>       - Sports cyclists thought that bikes would run slower with air tires. (Bijker and Pinch)

## Relation between technology and society (3)

>- Actor-Network Theory (ANT): Technology can be described as complex networks involving material things, humans, and concepts ("material-semiotic" relations).
>       - For example, the interactions in a bank involve both people, their ideas, and technologies. Together these form a single network.
>       - Actor-network theory tries to explain how material-semiotic networks come together to act as a whole (for example, a bank is network of people, things, and concepts. For certain purposes acts as a single entity).
>       - Such actor-networks are potentially transient. They dissolve and are re-made again.
>       - This means that relations need to be repeatedly “performed” or the network will dissolve. (The bank clerks need to come to work each day, and the computers need to keep on running.)

## STS (Science and Technology Studies)

>- These kinds of thoughts are the basis of a whole area of research, called "STS" (“science and technology studies,” or “science, technology, society”).
>- STS tries to explore how technology shapes society, and/or how social factors affect technological development.
>- Obviously, this is critical for our topic. 
>     - If technological determinism is true, then any attempts to regulate technology are futile.
>     - If social constructivism is true, then, again, regulation by the state alone cannot sufficiently control technology. All groups in society must cooperate in order to shape the future development of technology.


# Universal Basic Income

## Universal Basic Income: The idea

>- Robots take away human jobs.
>- Robots generate income doing these jobs, but they don’t need most of that income (they don’t consume things, have no social life, no children, no education needs).
>- Robots only need a small fraction of the generated value for energy costs, maintenance, repair and to pay back the initial investment.
>- This free income could be given to the ex-employees who were unemployed because of that robot.
>- To simplify the mechanics of that transaction, we could pool all robot-generated value and distribute it equally among the population.
>- This is then called a “Universal Basic Income (UBI)”.


## Universal Basic Income: Definition^[https://citizensincome.org/citizens-income/what-is-it/]

>- In order for income to qualify as UBI it must fulfil some criteria. It must be:
>     - Unconditional: A basic income would vary with age, but with no other conditions. Everyone of the same age would receive the same basic income, whatever their gender, employment status, family structure, contribution to society, housing costs, or anything else.
>     - Automatic: Someone's basic income would be automatically paid weekly or monthly into a bank account or similar.
>     - Non-withdrawable: Basic incomes would not be means-tested. Whether someone's earnings increase, decrease, or stay the same, their basic income will not change.
>     - Individual: Basic incomes would be paid on an individual basis and not on the basis of a couple or household.
>     - As a right: Every legal resident would receive a basic income, subject to a minimum period of legal residency and continuing residency for most of the year.


## Universal Basic Income: Benefits

>- The benefits of UBI go far beyond the “AI and Jobs” discussion.
>     - Under UBI, employees could afford to wait for better jobs rather than be forced to take the first that they can get. This would increase job satisfaction, bring them higher income, and make better use of the employees’ education and experience.
>     - Citizens could volunteer more and take time off for creative activities or further education.
>     - Citizens could afford to provide better care to elderly family members and children, reducing the need for costly and inefficient elder- and childcare schemes.
>     - Young couples would be encouraged to have more children.
>     - Much less bureaucracy than other welfare schemes that require extensive paperwork.
>- In terms of AI, UBI would make it easier for society to deal with the loss of jobs due to automation.


## Universal Basic Income: Problems

>- UBI could lead to inflation. If everyone has the same amount of money available, but we don’t have a greater supply of goods, then the prices must rise to balance the demand and supply.
>- This also means that, in the long run, there wouldn’t be an increased standard of living.
>- A UBI might cause people to get lazy and do nothing instead of looking for a job. In the long run, this would harm a society’s productivity.
>- Lacking work experience would make it more difficult for long-term unemployed people to find work again and would perpetuate unemployment.
>- Chronic unemployment might cause all sorts of psychological, family and social problems.

## Other functions of work

>- “If people cannot find work, then let’s pay them to stay at home! Since machines will take care of the production, society will be able to afford paying people who don’t need to work.” -- Do you see any problems with this statement?
>- Work is not only a source of income, but also a source of meaning for the working person.
>     - Work provides identification with a group,
>     - a regular daily activity that fills one’s time,
>     - friendships and other human relationships at the workplace,
>     - intellectual stimulation, learning and practice in problem-solving.
>- These are all important components of human well-being.
>- It will not be easy to abolish work and pay people for “doing nothing” without causing grave social problems.


## UBI experiments^[https://www.thebalance.com/universal-basic-income-4160668]

>- Alaska has a limited UBI since 1982. Each resident receives USD 1200 per year, which is only about a tenth of a full income.
>- Finland started a UBI experiment in 2017, giving 2000 unemployed people 560 Euro per month as a basic income. The Finnish government stopped the program one year later.
>- Scotland is funding research into a program that pays every citizen for life. Retirees would receive 150 pounds a week. Working adults would get 100 pounds and children under 16 would be paid 50 pounds a week.
>- In 2016, Switzerland voted against universal income. The government proposed paying every resident 2,500 Swiss francs per month.
>- So, presently the situation does not look encouraging. AI may change the basic conditions, though, and make a UBI inevitable.

## A robot tax? (1)

>- One proposal to finance a UBI would be a “robot tax,” that is, a tax to be payed by the employers of robots who have replaced human employees.
>- This runs into a few difficulties:
>     - If we calculate the tax proportionally to the number of replaced human workers, we would need to know how many humans have actually been replaced by the AI technology. This can be hard to calculate, especially for new businesses that have never employed humans, or for work that would be impossible to be done by humans.
>     - How to calculate tax related to working hours when robots work 24 hours a day? Should the tax be proportional to hours worked? Does a robot that works 16 hours (instead of 8) replace *two* humans? This will depend on the kind of work done. Work that involves cooperation with other humans might not be efficient when performed at night.

## A robot tax? (2)

>- Alternatively, we could tax the total generated value of a company over the course of a year, independently of the number of workers replaced. This would simplify calculations, but might treat enterprises that generate a big amount of value with relatively few employees unfairly.
>- Such taxes have been proposed in Austria (“Maschinensteuer,” 2016) and Italy (“IRAP,” regional tax on productive activities, 1997).
>- There doesn’t seem to be an ideal solution to the problem, but multiple approaches are being considered.
>- The problem of the effect of AI on jobs is not going away by itself, and some solution will have to be found.





