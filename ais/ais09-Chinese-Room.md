---
title:  "AI and Society: 09. Replies to Turing Test and Chinese Room"
author: Andreas Matthias, Lingnan University
date: February 14, 2019
...

# Criticism of the Turing Test

## Deception vs. “intelligence”

>- IG and TT are essentially deception games
>- The means of deception are not limited
>- Successful deception relies on “tricks”
    - To produce “unfalsifiable” forms of literature (Racter)
    - Not to calculate too fast
    - To make spelling mistakes
    - To forget
    - To pretend to have a human body, a birthday, to like ice cream
>- These tricks do not constitute intelligent behaviour
>- A machine could be intelligent without successful deception

## Purtill's "battle of wits"

>- When we look at a Turing Test judge who is talking to a computer, is the judge really talking *to the computer?*
>- For example, the judge asks a question, the computer gives a funny answer, the judge laughs. Can we conclude that *the computer* is funny?
>- *Your opinion?*

## Purtill's "battle of wits"

>- When we talk "to the machine" we are really talking to the *programmer* of the machine, not to the machine itself.
>- The wit of the machine is the wit of the programmer, stored in the machine.
>- We can see the Turing Test as "just a battle of wits between the questioner and the programmer: the computer is non-essential." (Purtill 1971)
>- Therefore, the conclusion of the Turing Test is wrong. It should not be: “the computer is intelligent”; but: “the designer of the program’s replies is intelligent.” And that is trivially true anyway.

## Purtill's "battle of wits"

This is a similar argument to the Chinese Room:

>- The whole understanding of the Chinese language must be attributed to the person who wrote the rule book.
>- Whatever dialog takes place, takes place between the people outside the room and the writer of the rule book.
>- The person *in the room* is unimportant and does not actually do anything mentally interesting.

## French: The Seagull Test

>- Imagine there is an island, where the only known bird is a particular species of seagull.
>- The scientists of this island want to construct a flying machine.
>- But: how to decide if it is flying successfully?
    - Thrown stones are not properly “flying”
    - Falling leaves are not “flying”
    - Feathers in the wind are not “flying”

## French: The Seagull Test

>- They observe the behaviour of the seagull and devise a "seagull test":
>- A machine is considered to fly successfully if, watched on a radar screen, is mistaken for a seagull by most observers.
>- Result: The seagull test can not be passed by airplanes, helicopters, bats, bees, or sparrows!
>- Either we say these don't fly, or the test is somehow wrong.

## French: subcongnitive processes

>- Find the plurals of “platch” and “snorp”!
>- Decide whether “flugly” would be a good name for
    - a male action star in a fighting role
    - a sexy female movie star
    - a cute stuffed animal

##  French: subcongnitive processes

>- We are able to do a lot of processing machines cannot do:
    - Find the plurals of “platch” and “snorp”!
    - Decide whether “flugly” would be a good name for a male action star in a fighting role, a sexy female movie star, or a cute stuffed animal
	- Using such questions in a Turing Test will immediately uncover the machine!
>- These abilities don't come from conscious, symbolic processing of information.
>- Rather, they are culturally developed, or taught to us while we are children.
>- French calls these "subcognitive" processes, because they happen below the conscious cognitive level.

##  French: subcongnitive processes

>- There are more subcognitive processes that can be used to uncover who is the machine in a Turing Test:
>- We know that humans *recognise words* much faster in context:
>- In: "car, love, philosophy, pepper, rain" the recognition of "pepper" will be much slower than in: "sugar, salt, pepper, cumin."
>- The delay is measurable experimentally, and can be used to detect the human partner in a Turing Test.

## French: rating games

>- “Rate A as B!”
>- Rate dry leaves as hiding places!
>- Rate toy airplanes as airplanes!
>- Rate broomsticks, keys, bananas and pillows as weapons!
>- These questions are very hard for a machine to answer.
>- CYC or similar common sense systems *might* be able to handle some of them.

## French: Conclusion

>- If I want to still have a machine pass the Turing Test, I would have to program it to imitate the subcognitive patterns of humans.
>- This is very probably impossible, since these patterns arise from the specific biological  or functional organization of the human body.
>- **The physical level and the cognitive level of intelligence are inseparable.**
>- A computer will never be able to pass a Turing Test.
>- It will always be possible to uncover it using questions which can only be anwered by having lived a typical human life in a human body.

# Variants of the Turing Test

## Turing test variants

>- If the Turing Test has all these problems, is there a way how we could try to improve it? Another test for intelligence that would be better and more accurate?



## The Kugel Test

![](graphics/04-kugel-test.jpg) \



>- The Kugel Test attempts to be a version of the Turing Test which rules out FSM's and essentially dumb programs like Alice.
>- A judge thinks of a concept.



## The Kugel Test

![](graphics/04-kugel-test.jpg)\ 



>- He throws picture cards into one of two bins, depending on whether they fit the chosen concept (“yes/no” bins).
>- The candidate has to guess which bin the next card is going to be thrown into.



## The Kugel Test

![](graphics/04-kugel-test.jpg)\ 



>- The test is run infinitely long.

## The Kugel Test

>- *Is this a good test for machine intelligence?*
>- Advantages:
>     - It does not use language, so people who speak another language would not fail.
>     - It uses abstract classes to judge intelligence. Abstraction and categorisation is a core “intelligent” task that is hard to fake (as opposed to generating correct language). 
>     - Each picture can fall into multiple categories, e.g. a bee can denote “insect,” “flying thing,” “yellow/black small object,” “dangerous animal” and so on. So there really *is* some intelligence necessary to get the right category depending on the other pictures seen previously.
>     - Consider also that the pictures can be photographs, drawings, children’s-book style drawings, or van-Gogh style images of things. To get the right identification for each of the different ways of depicting something is, in itself, a difficult and “intelligent” task of image content recognition.

## The Kugel Test

>- Would this be easy to pass?
    - By a human?
	- By a machine?
>- What about private concepts? (Things I saw last week)
>- Is this still an *intelligence* test?
>- Humans would also fail, depending on knowledge, cultural background, and so on.
>- Perhaps: The machine should be allowed to fail where humans also fail.

## Schweizer's TRTTT: Tokens and types

>- Schweizer realised that we shouldn't attempt to answer the question whether a random thing is intelligent.
>- Rather, we must distinguish between *kinds of things* that are intelligent, and kinds of things that aren't. A Turing Test only makes sense if I perform it on the kind of thing that has a chance of being intelligent.
>- Schweizer (1998): Truly Total Turing Test:
>- First, a species in question must pass a test as group, that is, they must show that they are able not only to play chess, but to *invent* the game of chess.
>- Second, after this has been achieved, they can be assumed to be intelligent as a *type* (kind), and then the individuals can be tested by token tests (like the Turing Test).

# The Chinese Room argument

## Overview

John Searle's Chinese Room argument, presented in 1980, attempted to show that a program (or any physical symbol system) could not be said to "understand" the symbols that it uses; that the symbols have no meaning for the machine, and so the machine can never be *truly* intelligent (or be said to "have a mind").

## Searle's argument (1)

> "Suppose that I’m locked in a room and given a large batch of Chinese writing. Suppose furthermore (as is indeed the case) that I know no Chinese, either written or spoken, and that I’m not even confident that I could recognize Chinese writing as Chinese writing distinct from, say, Japanese writing or meaningless squiggles. To me, Chinese writing is just so many meaningless squiggles. Now suppose further that after this first batch of Chinese writing I am given a second batch of Chinese script together with a set of rules for correlating the second batch with the first batch. The rules are in English, and I understand these rules as well as any other native speaker of English. They enable me to correlate one set of formal symbols with another set of formal symbols, and all that “formal” means here is that I can identify the symbols entirely by their shapes."

## Searle's argument (2)

> "Now suppose also that I am given a third batch of Chinese symbols together with some instructions, again in English, that enable me to correlate elements of this third batch with the first two batches, and these rules instruct me how to give back certain Chinese symbols with certain sorts of shapes in response to certain sorts of shapes given me in the third batch. Unknown to me, the people who are giving me all of these symbols call the first batch 'a script,' they call the second batch a 'story,' and they call the third batch 'questions.' Furthermore, they call the symbols I give them back in response to the third batch 'answers to the questions,' and the set of rules in English that they gave me, they call 'the program.'"

## Searle's argument (3)

> "Suppose also that after a while I get so good at following the instructions for manipulating the Chinese symbols and the programmers get so good at writing the programs that from the external points of view – that is, from the point of view of somebody outside the room in which I am locked – my answers to the questions are absolutely indistinguishable from those of native Chinese speakers." 

## Searle's argument (4)

> "As regards the [claims of strong AI], it seems to me quite obvious in the example that I do not understand a word of the Chinese stories. I have inputs and outputs that are indistinguishable from those of the native Chinese speaker, and I can have any formal program you like, but I still understand nothing. For the same reasons, Schank’s computer understands nothing of any stories, whether in Chinese, English, or whatever, since in the Chinese case the computer is me, and in cases where the computer is not me, the computer has nothing more than I have in the case where I understand nothing."

# Chinese Room: Criticisms and replies

## System reply

>- While Searle does not understand Chinese, the *system* (composed of person, rule-book, and cards) does.
>- Just because *a part of the system* (Searle) does not understand Chinese, we cannot conclude that the whole system does not.
>- More generally, this argument is a fallacy: **A system that has an ability X does not need to contain subsystems that have the same ability X!**
>- If this was the case, the chain of subsystems with the ability X would never end!
>- Consider: the human brain can "understand" (we assume). Still, individual neurons in our brain clearly do not understand. We cannot conclude that humans don't understand because their neurons don't!

## System reply

>- *What could Searle answer to that?*
>- Searle: Let me memorize the rule book and take away the room. Now all of the system is in me. But I still do not understand Chinese, so the system does not understand Chinese.
>- *Is this a good answer?*
>- Still the same problem.
>- Also, the rule-book is where the understanding of the Chinese Room comes from. The original experiment smuggles meaning into the room in the form of the (human-written) rule-book!
>- Like with Purtill's criticism of the Turing Test, we could say that the Chinese Room is a "battle of wits" between the people outside the room and the writer of the rule-book. The person inside the room is not important.

## Robot system reply (Harnad)

>- We must distinguish c-implementation (computer) and p-implementation (physical device).
>- Accordingly, we must distinguish symbolic functionalism from robotic functionalism.
>- Symbolic functionalism:
    - “The belief that mental function is really just symbolic (e.g. verbal, inferential, computational) function;
	- that the mind manipulates symbols the way a Turing machine does,
	- and hence that the brain just supports the hardware for doing computation.
>- Robotic functionalism:
    - “Includes non-symbolic functions, for example sensors and actuators.”

## C-implementation and p-implementation

*Give an example where we see the difference between c- and p-implementation!*

(For example, the implementation of an "airplane.")

. . . 

- C-implementation of an airplane:  a simulation of an airplane inside a computer.
- P- implementation: building a physical model of an airplane, or building an actual airplane that flies.

. . . 

Harnad: a c-implementation of a robot functionalist device will not have a mind. A p-implementation will.

*Is this plausible? Why would this be so?*

. . .

(Because of symbol grounding. But is symbol grounding sufficient for having a mind???)

## Mooney’s counterargument to robotic symbol grounding

>- Assume I send a robot to Mars.
>- It has direct sensory access, so it achieves a "mind."
>- Now I bring it back to earth and disconnect the sensors, and instead I connect a computer terminal.
>- Now I can still talk to the robot, but suddenly its “mind” is gone.
>- If I reattach the wheels and sensors, the “mind” comes back, although all the time I can continue having the same typed conversation with it. Essentially I can switch the mind on and off by removing and attaching the sensors and wheels.
>- *Possible reply to that?*

## Reply to Mooney's counterargument

>- The mind must not necessarily *disappear* if I remove the sensors.
>- Only the ability of the system to produce *new* symbol groundings will be impaired.
>- The already used symbols can be used further (like when someone with acquired blindness can speak meaningfully about remembered colours!)
>- (You see: Sometimes even great philosophers make bad arguments.)

## Combined systems/robot reply

>- Perhaps a combined robot/systems reply is best choice:
>- The system as a whole, including its sensors (which are required for symbol grounding),  and its environment (should this be mentioned here? how great is the contribution of the environment on a system's intelligence?), understands, even if Searle-in-the-room does not.
>- More pointedly, it is completely irrelevant whether Searle-in-the-room understands Chinese or not! (As it is irrelevant whether our neurons individually understand anything.)

## Robots and cognition

>- This brings us to a more general point about the importance of robots in AI.
>- Robots are not just “computers on wheels.”
>- A robot is an entirely different thing from a purely symbolic computer, because its sensory and motor equipment gives it the ability to acquire genuine “meaning” (the corresponding sensory impressions for each symbol in its database), so that it can escape the problems of the Turing Test and the Chinese room.
>- A robot could, conceivably, answer some of French’s subcognitive questions (“rate leaves as hiding places”) correctly from actual experience; a Prolog-based program could never do so from own experience.
>- This is why, from the next session on, we will talk about AI systems with actual bodies, sensors and actuators, that are more likely to exhibit “genuine” intelligence.





