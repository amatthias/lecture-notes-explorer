---
title:  "AI and Society: 6. Expert systems and their critics"
author: Andreas Matthias, Lingnan University
date: September 2, 2019
...

# Symbolic AI

## Symbolic AI (1)

>- The idea of symbolic AI is to represent reality as symbols inside the computer.
>- For example, in Prolog we have something like: “cat(tom)”.
>     - Here, the concept of “cat” is represented by the predicate “cat()”, which, in turn, is just a sequence of three letters, c-a-t. 
>     - These three letters “cat” don’t have any meaning inside the machine. Although they represent the concept of cats, they don’t *mean* anything to the computer.
>     - The same with the symbol “tom”, which for the computer is just a meaningless string of three letters, t-o-m.

## Symbolic AI (2)

>- Hubert Dreyfus, philosopher and AI critic, describes the basic idea of symbolic AI as: “The mind can be viewed as a device operating on bits of information according to formal rules.”
>- We could also say: “The purely syntactical manipulation of symbols according to rules is sufficient for intelligent behaviour” (avoiding talk of “mind”).
>- Or, as we said in the last session: “If the symbol manipulation preserves the original relations between the symbols, the mapping of symbol to meaning can be left to mind of the operator.”

## The Physical Symbol System Hypothesis

>- These different ways of expressing the characteristics of symbolic AI systems have been summarised in a famous statement that is called the *Physical Symbol System Hypothesis:*
>- “A physical symbol system has the necessary and sufficient means for general intelligent action.” (Allen Newell and Herbert A. Simon, 1976)
>- This means that we don’t need real “understanding” or “meaning” inside a computer. Just manipulating symbols in the right ways is both necessary and sufficient for intelligent action (note: not for “mind”!)

# Problems of symbolic AI

## Problems of symbolic AI

Symbolic AI has multiple problems. A few are:

>- The problem of noise:
>     - In reality, objects are never entirely clear.
>     - Objects in pictures can have a background that is distracting and obscuring the object.
>     - Voice input is obscured by noisy environments.
>     - Ordinary human language has all sorts of, ummm, you know, noisy features that don’t contribute too much to the actual, real, intended meaning, innit mate?
>- The symbol grounding problem: Can a system ever really understand anything if its symbols don’t correspond to anything real, that is, if they are meaningless to the system? If not, how can we make the system understand the meaning of its symbols?
>- The Frame Problem (see below).

## Frame problem

| paint(X,C) $\to$ colour(X,C).
| move(X,P) $\to$ position(X,P).
| colour(duck, red).
| position(duck, house).
| paint(duck, blue).
| move(duck, garden).

. . . 

*What is the state of the world now?*

- Colour of duck?
- Position of duck?

## Frame problem

| paint(X,C) $\to$ colour(X,C).
| move(X,P) $\to$ position(X,P).
| colour(duck, red).
| position(duck, house).
| paint(duck, blue).
| move(duck, garden).

>- Is it true that the duck is now blue and standing in the garden?
>- *Does this logically follow from the premises?*
>- No! It can not be ruled out that *the colour gets changed by the move* action!
>- (For example by moving the duck into a pot of paint!)
>- Thus we can only conclude: position(duck, garden)!

## Frame problem

>- If I *paint* a thing in my kitchen, which properties of the world will be affected?
>- The colour of that thing.
>- The colour of the brush I used.
>- The weight of the colour pot (because I used some colour up).
>- The weight of the painted thing.
>- The smell of the painted thing.
>- If I *explode a bomb* in my kitchen, which properties of the world will be affected?
>- Lots, depending on the size and type of the bomb!

## Frame problem

More general question:

- How do I know where to stop looking for consequences of my actions?
- How can we tell the machine which actions will affect which properties of the environment?

. . . 

Specifically, in symbolic AI:

- How can I describe the world with symbolic facts and rules without having to describe the (almost infinite) set of states which don't change with every single action?
- How can I distinguish what has changed after an action from what has remained the same?

## The "frame problem"

“To most AI researchers, the frame problem is the challenge of representing the effects of action in logic without having to represent explicitly a large number of intuitively obvious non-effects. To many philosophers, the AI researchers' frame problem is suggestive of a wider epistemological issue, namely whether it is possible, in principle, to limit the scope of the reasoning required to derive the consequences of an action.”

(Stanford Encyclopedia of Philosophy)


# Expert systems

## Expert systems

Expert systems are some of the most prominent examples of AI.

![](graphics/03-expert-system.png)

## Expert system rule tree

Diagnosis of TV faults:

![](graphics/03-expert-system-rules.jpg)

## Expert system architecture

>- Knowledge base (KB): a representation of the knowledge to be encoded, often as IF/THEN rules.^[Source: amzi.com: Expert Systems in Prolog.]
>- Inference engine: the code at the core of the system that derives recommendations from the knowledge base and the problem-specific data entered by the user.
>- User interface: The code that controls the dialog between the user and the system.

## Expert system individual roles

>- Domain expert: The person who is currently expert in solving the problems that the system is intended to solve.
>- Knowledge engineer: the person who extracts the expert’s knowledge (perhaps in a dialogue with the expert) and who encodes it in form that can be used by the expert system, thus creating the knowledge base.
>- System engineer: the person who builds the user interface, inference engine, and other program parts that make up the expert system (except for the expert knowledge itself).
>- User: The individual who will use the expert system as a replacement for the human expert.


# Hubert Dreyfus’ criticism of expert systems

## (Questionable) assumptions behind expert systems

>- Hubert Dreyfus: Heidegger expert and AI critic.
>- Reading: "From Socrates to Expert Systems."
>- Experts follow clear, black and white rules.
>- What makes the expert an expert is that he knows rules that other people don't.
>- You can extract the expertise in form of symbolic rules and put it into a machine.
>- Everyone who has these rules to follow, will be an expert.

. . .

*Can you describe what is wrong about these assumptions?*

## How does an "expert" work?

>- What is an "expert system"? Does it really model what a human expert does?
>- *Are* you *experts in anything?*
    - Possibly: Piano, violin, computer games, Chinese calligraphy
	- All of you: Writing, reading, taking lecture notes, Chinese, walking, jumping over obstacles
>- *How did you become an expert?*

## Dreyfus: From Socrates to Expert Systems

>- There are stages of development of expertise.
>- If we look at how human expertise develops, we will see that machine expert systems got the process all wrong!


## Novice stage: Recognising features

>- The student automobile driver learns to recognize such interpretation-free features as speed (indicated by the speedometer).
>- He is given rules such as shift to second gear when the speedometer needle points to ten miles an hour.
>- The novice chess player learns a numerical value for each type of piece regardless of its position, and the rule: "Always exchange if the total value of pieces captured exceeds the value of pieces lost."
>- The player also learns to seek center control when no advantageous exchanges can be found, and is given a rule defining center squares and a rule for calculating extent of control.
>- Most beginners are notoriously slow players, as they attempt to remember all these features and rules.

## Advanced beginner stage: Situational aspects and features (1)

>- As the novice gains experience actually coping with real situations, he begins to note, or an instructor points out, examples of meaningful additional *aspects of the situation.*
>- After seeing a sufficient number of examples, the student learns to recognise these new aspects.
>- Instructions now can refer to these new *situational* aspects, as well as to the objectively defined *nonsituational* features recognisable by the inexperienced novice.

## Advanced beginner stage: Recognising aspects and features (2)

>- The advanced beginner driver, using (situational) engine sounds as well as (non-situational) speed in his gear-shifting rules, learns the maxim: Shift up when the motor sounds like it is racing and down when it sounds like its straining.
>- He learns to observe the demeanor as well as position and velocity of pedestrians or other drivers.
>- He can, for example, distinguish the behavior of a distracted or drunken driver from that of an impatient but alert one.
>- Engine sounds and behaviour styles cannot be adequately captured by words, so words cannot take the place of a few choice examples in learning such distinctions.

## Advanced beginner stage: Recognising aspects and features (3)

>- With experience, the chess beginner learns to recognize such situational aspects of positions as a weakened king's side or a strong pawn structure despite the lack of a precise and situation-free definition.
>- The player can then follow maxims such as: Attack a weakened king’s side.

## Competence stage: Important vs. ignored elements (1)

>- With more experience, the number of potentially relevant elements that the learner is able to recognise becomes overwhelming.
>- At this point the student may well wonder how anyone ever masters the skill.
>- To cope with this overload and to achieve competence, people learn, through instruction or experience, to devise a plan or choose a perspective.
>- The perspective then determines which elements of the situation should be treated as important and which ones can be ignored.
>- By restricting attention to only a few of the vast number of possibly relevant features and aspects, such a choice of a perspective makes decision making easier.

## Competence stage: Important vs. ignored elements (2)

>- The competent performer thus seeks new rules and reasoning procedures to decide upon a plan or perspective.
>- But such rules are not as easy to come by as are the rules and maxims given beginners.
>- There are just too many situations differing from each other in subtle, nuanced ways.
>- More, in fact, than can be named or precisely defined, so no one can prepare for the learner a list of what to do in each possible situation.
>- Competent performers, therefore, must decide for themselves in each situation what plan to choose and when to choose it without being sure that it will be appropriate in that particular situation.

## Competence stage: Important vs. ignored elements (3)

>- Coping thus becomes frightening rather than exhausting.
>- Prior to this stage, if the learned rules didn't work out, the performer could rationalize that he hadn't been given adequate rules rather than feel remorsebecause of his mistake.
>- Now, however, the learner feels responsible for disasters.
>- Of course, often at this stage, things work out well, and the competent performer experiences a kind of elation unknown to the beginner.
>- Thus, learners find themselves on an emotional roller coaster.

## Competence stage: Emotional involvement (1)

>- A competent driver, after taking into account speed, surface condition, criticality of time, etc., may decide he is going too fast.
>- He then has to decide whether to let up on the accelerator, remove his foot altogether, or step on the brake and precisely when to do so.
>- He is relieved if he gets through the curve without being honked at and shaken if he begins to go into a skid.
>- The competent chess player may decide, after studying a position, that her opponent has weakened his king's defenses so that an attack against the king is a viable goal.
>- If she chooses to attack, she can ignore features involving weaknesses in her own position created by the attack as well as the loss of pieces not essential to the attack.
>- Successful plans induce euphoria, while mistakes are felt in the pit of the stomach.

## Competence stage: Emotional involvement (2)

>- As the competent performer become more and more emotionally involved in his tasks, it becomes increasingly difficult to draw back and to adopt the detached rule-following stance of the beginner.
>- While it might seem that this involvement would interfere with detached rule-testing and so would inhibit further skill development, in fact just the opposite seems to be the case.
>- As we shall soon see, if the detached rule-following stance of the novice and advanced beginner is replaced by involvement, one is set for further advancement, while resistance to the acceptance of risk and responsibility can lead to stagnation and ultimately to boredom and regression.

## Proficient stage: Seeing instead of calculating (1)

>- If events are experienced with involvement as the learner practices her skill, the resulting positive and negative experiences will strengthen successful responses and inhibit unsuccessful ones.
>- The performer's theory of the skill, as represented by rules and principles, will thus gradually be *replaced by situational discriminations* accompanied by associated responses.
>- Proficiency seems to develop if, and only if, experience is assimilated in this atheoretical way and intuitive behavior replaces reasoned responses.

## Proficient stage: Seeing instead of calculating (2)

>- Action becomes easier and less stressful as the learner simply *sees* what needs to be achieved rather than deciding, by a calculative procedure, which of several possible alternatives should be selected.
>- Remember that the involved, experienced performer sees goals and important aspects, but not what to do to achieve these goals.
>- This is inevitable since there are far fewer ways of seeing what is going on than there are ways of responding.
>- Thus, the proficient performer, after seeing the goal and the important features of the situation, must still decide what to do. To decide, he falls back on detached rule-following.

## Proficient stage: Seeing instead of calculating (3)

>- The proficient driver, approaching a curve on a rainy day, may feel in the seat of his pants that he is going dangerously fast.
>- He must then decide whether to apply the brakes or merely to reduce pressure on the accelerator by some selected amount.
>- The proficient chess player, who is classed a master, can recognize almost immediately a large repertoire of types of positions. She then deliberates to determine which move will best achieve her goal. She may, for example, know that she should attack, but she must calculate how best to do so.

## Expert stage: “Seeing” both problem and solution (1)

>- The proficient performer, immersed in the world of his skillful activity, *sees* what needs to be done, but must *decide* how to do it.
>- The expert not only sees what needs to be achieved; thanks to a vast repertoire of situational discriminations, he *sees how to achieve* his goal.
>- The ability to make more subtle and refined discriminations is what distinguishes the expert from the proficient performer.
>- With enough experience in a variety of situations, the brain of the expert performer gradually decomposes this class of situations into subclasses, each of which shares the same action.
>- This allows the immediate intuitive situational response that is characteristic of expertise.

## Expert stage: “Seeing” both problem and solution (2)

>- The expert chess player, classed as an international master or grandmaster, experiences a compelling sense of the issue and the best move.
>- Excellent chess players can play at the rate of 5 to 10 seconds a move and even faster without any serious degradation in performance.
>- At this speed they must depend almost entirely on intuition and hardly at all on analysis and comparison of alternatives.
>- Driving probably involves the ability to discriminate a similar number of typical situations. The expert driver not only feels when slowing down on an off ramp is required; he simply performs the appropriate action.
>- What must be done, simply is done.

## Conclusion (1): From beginner to expert

>- We can see now that a beginner calculates using rules and facts just like a heuristically programmed computer.
>- With talent and a great deal of involved experience, the beginner develops into an expert who intuitively sees what to do *without recourse to rules.*
>- Normally an expert does not calculate. He does not solve problems. He does not even think. He just does what normally works and, of course, it normally works.

## Conclusion (2): "Expert" systems

>- This description of skill acquisition enables us to understand why experts have trouble to articulate the rules they are using: Experts are simply not following any rules!
>- They are instead discriminating thousands of special cases.
>- This in turn explains why expert systems are never as good as experts. If one asks an expert for the rules he is using one will, in effect, force the expert to regress to the level of a beginner and state the rules he learned in school.
>- Thus, instead of using rules he no longer remembers, as the knowledge engineers suppose, the expert is forced to remember rules he no longer uses.

## Conclusion (3): "Expert" systems

>- If one programs these rules into a computer, one can use the speed and accuracy of the computer and itsability to store and access millions of facts to outdo a human beginner using the same rules.
>- But such systems are at best competent. No amount of rules and facts can capture the knowledge an expert has when he has stored his experience of the actual outcomes of tens of thousands of situations.

## Summary of Dreyfus' critique

Hubert Dreyfus *attacked* the basic assumption behind symbolic AI, calling it "the psychological assumption" and defining it thus:

"The mind can be viewed as a device operating on bits of information according to formal rules."

. . . 

>- Dreyfus refuted this by showing that human intelligence and expertise depended primarily on unconscious instincts rather than conscious symbolic manipulation.
>- Experts solve problems quickly by using their intuitions, rather than step-by-step trial and error searches.
>- Dreyfus argued that these unconscious skills would never be captured in formal rules.

## Dangers of calculative rationality (1)

>- But Dreyfus goes further than just this academic result.
>- He warns that seeing rationality as rule-driven behaviour poses great dangers to society.
>- *Can you guess why?*
>- The calculative picture of reason underlies a general movement towards calculative rationality in our culture, and that movement brings with it great dangers.
>- The increasingly bureaucratic nature of society is heightening the danger that in the future skill and expertise will be lost through over reliance on calculative rationality.

## Dangers of calculative rationality (2)

>- For example, judges and ordinary citizens serving on our juries are beginning to distrust anything but "scientific" evidence.
>- A ballistics expert who testified only that he had seen thousands of bullets and the gun barrels that had fired them, and that there was absolutely no doubt in his mind that the bullet in question had come from the gun offered in evidence, would be ridiculed by the opposing attorney and disregarded by the jury.
>- Instead, the expert has to talk about the individual marks on the bullet and the gun and connect them by rules and principles showing that only the gun in question could so mark the bullet. But in this he is no expert.

## Dangers of calculative rationality (3)

>- Calculative rationality, which is sought for good reasons, means a *loss of expertise.*
>- Another example: replacing expert carpenters with industrially made furniture leads to a loss of expertise in wood working across all of society.
>- But in facing the complex issues before us we need all the wisdom we can find.
>- Therefore, society must clearly distinguish its members who have intuitive expertise from those who have only calculative rationality.
>- It must encourage its children to cultivate their intuitive capacities in order that they may achieve expertise, not encourage them to reason calculatively and thereby become human logic machines.
>- In general, to preserve expertise we must foster intuition all levels of decision making, otherwise wisdom will become an endangered species of knowledge.

## Solutions to the problem

How could we solve the problems Dreyfus describes and design systems that don't have these problems?

>- AI systems should learn from experience. This would give them some common sense knowledge of the world.
>- We need systems that can *generalise* from existing knowledge and are able to find answers to problems that are *similar* (but not identical) to those that they already know how to solve (neural networks can do that).
>- Systems should have additional sensors for all kinds of environmental conditions and a body that can act in the physical world. This would address the symbol grounding problem and the Chinese Room criticisms.
>- Truly intelligent behaviour probably needs a body. See the "Subsumption Architecture" (Rodney Brooks: "Elephants don't play chess," discussed in a later lecture).


## The End. Thank you for your attention!

Questions?

<!--

%% Local Variables: ***
%% mode: markdown ***
%% mode: visual-line ***
%% mode: cua ***
%% mode: flyspell ***
%% End: ***

-->
