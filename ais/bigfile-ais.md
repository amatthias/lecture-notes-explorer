ais01-Introduction.md&0.1&---
ais01-Introduction.md&0.1&title:  "AI and Society: 1. Introduction"
ais01-Introduction.md&0.1&author: Andreas Matthias, Lingnan University
ais01-Introduction.md&0.1&date: September 2, 2019
ais01-Introduction.md&0.1&...
ais01-Introduction.md&0.1&
ais01-Introduction.md&1.0&# Introduction to the course
ais01-Introduction.md&1.0&
ais01-Introduction.md&1.1&## Welcome!
ais01-Introduction.md&1.1&
ais01-Introduction.md&1.1&Some of my names are:
ais01-Introduction.md&1.1&
ais01-Introduction.md&1.1&- Andreas Matthias
ais01-Introduction.md&1.1&- “Mr. Ma”
ais01-Introduction.md&1.1&- “Andy”
ais01-Introduction.md&1.1&
ais01-Introduction.md&1.1&(use which one you like!)
ais01-Introduction.md&1.1&
ais01-Introduction.md&1.2&## Get to know each other
ais01-Introduction.md&1.2&
ais01-Introduction.md&1.2&Now please everybody say something about him­ or herself:
ais01-Introduction.md&1.2&
ais01-Introduction.md&1.2&- Your name and how you would like me to call you
ais01-Introduction.md&1.2&- Why do you study philosophy?
ais01-Introduction.md&1.2&- What area of philosophy and which philosopher do you like most?
ais01-Introduction.md&1.2&- Have you ever programmed computers?
ais01-Introduction.md&1.2&- Have you studied Philosophy of Mind or AI before?
ais01-Introduction.md&1.2&- Why did you choose this class?
ais01-Introduction.md&1.2&
ais01-Introduction.md&1.3&## Presentation of course outline
ais01-Introduction.md&1.3&
ais01-Introduction.md&1.3&(see outline in Moodle)
ais01-Introduction.md&1.3&
ais01-Introduction.md&1.4&## Online resources
ais01-Introduction.md&1.4&
ais01-Introduction.md&1.4&- We use Moodle for this course.
ais01-Introduction.md&1.4&- The reading, lecture notes and announcements will be available through Moodle.
ais01-Introduction.md&1.4&- Make sure you know how to use it and look at it regularly.
ais01-Introduction.md&1.4&
ais01-Introduction.md&1.5&## Additional reading material
ais01-Introduction.md&1.5&
ais01-Introduction.md&1.5&Some additional material for this course can be found at:
ais01-Introduction.md&1.5&
ais01-Introduction.md&1.5&<https://moral-robots.com/category/ai-society>
ais01-Introduction.md&1.5&
ais01-Introduction.md&1.5&It is tagged with “Textbook:” in this presentation. When you see “Textbook” links, you are required to read them as part of the course materials.
ais01-Introduction.md&1.5&
ais01-Introduction.md&1.6&## “Homework” for next session
ais01-Introduction.md&1.6&Please find and post in Moodle at least three links to interesting Internet resources that show the present state of the research and applications of Artificial Intelligence.
ais01-Introduction.md&1.6&
ais01-Introduction.md&1.6&. . . 
ais01-Introduction.md&1.6&
ais01-Introduction.md&1.6&For each link, add a short explanation (about 1 paragraph of text) explaining why you think that this link is relevant to the philosophy of AI, and what philosophical questions it poses.
ais01-Introduction.md&1.6&
ais01-Introduction.md&1.6&. . .
ais01-Introduction.md&1.6&
ais01-Introduction.md&1.6&These could be, for example, YouTube videos showing “intelligent” machines in action, interviews with researchers or philosophers about AI, explanations about what AI is, or 
ais01-Introduction.md&1.6&similar resources.
ais01-Introduction.md&1.6&
ais01-Introduction.md&1.6&. . .
ais01-Introduction.md&1.6&
ais01-Introduction.md&1.6&Please don't post links which have already been posted by others. You must see what others have found first and make sure that your links are unique. Only unique links with clear explanations of what philosophical issues they raise will be graded!
ais01-Introduction.md&1.6&
ais01-Introduction.md&1.7&## The End. Thank you for your attention!
ais01-Introduction.md&1.7&
ais01-Introduction.md&1.7&Questions?
ais01-Introduction.md&1.7&
ais01-Introduction.md&1.7&<!--
ais01-Introduction.md&1.7&
ais01-Introduction.md&1.7&%% Local Variables: ***
ais01-Introduction.md&1.7&%% mode: markdown ***
ais01-Introduction.md&1.7&%% mode: visual-line ***
ais01-Introduction.md&1.7&%% mode: cua ***
ais01-Introduction.md&1.7&%% mode: flyspell ***
ais01-Introduction.md&1.7&%% End: ***
ais01-Introduction.md&1.7&
ais01-Introduction.md&1.7&-->
ais01-Introduction.md&1.7&
ais02-Examples.md&0.1&---
ais02-Examples.md&0.1&title:  "AI and Society: 2. Examples of AI and philosophical issues"
ais02-Examples.md&0.1&author: Andreas Matthias, Lingnan University
ais02-Examples.md&0.1&date: September 4, 2019
ais02-Examples.md&0.1&...
ais02-Examples.md&0.1&
ais02-Examples.md&1.0&# Some examples of applied AI systems
ais02-Examples.md&1.0&
ais02-Examples.md&1.1&## Deep Blue (IBM) vs. Kasparov, 1997
ais02-Examples.md&1.1&
ais02-Examples.md&1.1&![](graphics/01-chess.jpg) \
ais02-Examples.md&1.1&
ais02-Examples.md&1.1& 
ais02-Examples.md&1.2&## Stanford University's “Stanley”, 2005
ais02-Examples.md&1.2&
ais02-Examples.md&1.2&![](graphics/01-stanley.jpg) \
ais02-Examples.md&1.2&
ais02-Examples.md&1.2&
ais02-Examples.md&1.3&## Google driverless car (1)
ais02-Examples.md&1.3&
ais02-Examples.md&1.3&![](graphics/01-google-car-1.jpg) \
ais02-Examples.md&1.3&
ais02-Examples.md&1.3&
ais02-Examples.md&1.4&## Google driverless car (2)
ais02-Examples.md&1.4&
ais02-Examples.md&1.4&![](graphics/01-google-car-2.jpg) \
ais02-Examples.md&1.4&
ais02-Examples.md&1.4&
ais02-Examples.md&1.5&## Tesla autopilot
ais02-Examples.md&1.5&
ais02-Examples.md&1.5&<https://www.youtube.com/watch?v=3yCAZWdqX_Y>
ais02-Examples.md&1.5&
ais02-Examples.md&1.6&## Face recognition
ais02-Examples.md&1.6&
ais02-Examples.md&1.6&![](graphics/01-face.jpg) \
ais02-Examples.md&1.6&
ais02-Examples.md&1.6&
ais02-Examples.md&1.6&Automated tagging of faces in images: Facebook: June 2011, Google+: December 2011.
ais02-Examples.md&1.6&
ais02-Examples.md&1.7&## IBM Watson
ais02-Examples.md&1.7&
ais02-Examples.md&1.7&Image recognition:
ais02-Examples.md&1.7&
ais02-Examples.md&1.7&<http://visual-recognition-demo.mybluemix.net/>
ais02-Examples.md&1.7&
ais02-Examples.md&1.8&## IBM Watson wins Jeopardy (Feb 14, 2011)
ais02-Examples.md&1.8&
ais02-Examples.md&1.8&<https://www.youtube.com/watch?v=P18EdAKuC1U>
ais02-Examples.md&1.8&
ais02-Examples.md&1.9&## AlphaGo wins against Lee Sedol at Go (Weiqi)
ais02-Examples.md&1.9&
ais02-Examples.md&1.9&![9.3.2016: AlphaGo. 10.3.: AlphaGo. 12.3.: AlphaGo. 13.3: Lee Sedol. 15.3.: AlphaGo.](graphics/01-alphago-1.jpg) \
ais02-Examples.md&1.9&
ais02-Examples.md&1.9&
ais02-Examples.md&1.10&## AlphaGo wins against Ke Jie at Go (Weiqi)
ais02-Examples.md&1.10&
ais02-Examples.md&1.10&![23/25/27.5.2017: AlphaGo wins all three games.](graphics/01-kejie.jpg) \
ais02-Examples.md&1.10&
ais02-Examples.md&1.10&
ais02-Examples.md&1.11&## AlphaGo wins against Ke Jie at Go (Weiqi)
ais02-Examples.md&1.11&
ais02-Examples.md&1.11&AlphaGo also won in a team game against five top-ranking players (Chen Yaoye, Zhou Ruiyang, Mi Yuting, Shi Yue, and Tang Weixing).
ais02-Examples.md&1.11&
ais02-Examples.md&1.11&Right now, AlphaGo is clearly much better than the best human Go players in the world.
ais02-Examples.md&1.11&
ais02-Examples.md&1.11&This is a significant milestone, because Go was the last game that was considered to be so complex that machines would not be able to play it well (chess was already successfully ‘solved’ in 1997!)
ais02-Examples.md&1.11&
ais02-Examples.md&1.11&See: The Future of Go Summit (2017): <https://events.google.com/alphago2017/>
ais02-Examples.md&1.11&
ais02-Examples.md&1.12&## Robotics: Honda Asimo
ais02-Examples.md&1.12&
ais02-Examples.md&1.12&![](graphics/01-asimo.jpg) \
ais02-Examples.md&1.12&
ais02-Examples.md&1.12&
ais02-Examples.md&1.13&## Robotics: Aibo
ais02-Examples.md&1.13&
ais02-Examples.md&1.13&![](graphics/01-aibo.jpg) \
ais02-Examples.md&1.13&
ais02-Examples.md&1.13&
ais02-Examples.md&1.14&## Robotics: Pepper
ais02-Examples.md&1.14&
ais02-Examples.md&1.14&![](graphics/01-pepper.jpg) \
ais02-Examples.md&1.14&
ais02-Examples.md&1.14&
ais02-Examples.md&1.15&## Robotics videos
ais02-Examples.md&1.15&
ais02-Examples.md&1.15&Honda Asimo: <https://www.youtube.com/watch?v=QdQL11uWWcI>
ais02-Examples.md&1.15&
ais02-Examples.md&1.15&Boston Dynamics "Atlas" lifting robot: <https://www.youtube.com/watch?v=rVlhMGQgDkY>
ais02-Examples.md&1.15&
ais02-Examples.md&1.15&Boston Dynamics "Wild Cat" soldier assisting robot: <https://www.youtube.com/watch?v=wE3fmFTtP9g>
ais02-Examples.md&1.15&
ais02-Examples.md&1.16&## Philosophical issues
ais02-Examples.md&1.16&
ais02-Examples.md&1.16&What are the specific philosophical issues with 
ais02-Examples.md&1.16&
ais02-Examples.md&1.16&1. Chess and Go playing machines?
ais02-Examples.md&1.16&2. Self­driving cars?
ais02-Examples.md&1.16&3. Automated face recognition?
ais02-Examples.md&1.16&4. IBM Watson?
ais02-Examples.md&1.16&5. Autonomous toy robots for children?
ais02-Examples.md&1.16&6. The Boston Dynamics robots?
ais02-Examples.md&1.16&
ais02-Examples.md&1.16&Make a list of philosophical questions each technology poses! Then we will discuss your ideas.
ais02-Examples.md&1.16&
ais02-Examples.md&1.16&. . . 
ais02-Examples.md&1.16&
ais02-Examples.md&1.16&Textbook: <https://chatbotslife.com/philosophical-issues-ai-in-games-2017-207aff30dbb5>
ais02-Examples.md&1.16&
ais02-Examples.md&1.17&## Some results (1)
ais02-Examples.md&1.17&
ais02-Examples.md&1.17&- Chess and Go playing machines make the game less enjoyable to humans and threaten professional human players.
ais02-Examples.md&1.17&- Self driving cars: Who is responsible for accidents? Who pays for damages?
ais02-Examples.md&1.17&- Can machines replace humans in other everyday activities?
ais02-Examples.md&1.17&- Can we have emotional relationships with robots?
ais02-Examples.md&1.17&- It has been true for a long time that humans lose jobs to machines: ATMs, car factories, vending machines, email, post office letter sorting, ... etc. Robots will replace car, van, taxi and bus drivers very soon. Problems?
ais02-Examples.md&1.17&    - Leaves fewer things for humans to do.
ais02-Examples.md&1.17&	- Can machines replace *mental* functions of humans? (They do: chess players, parts of the jobs of accountants).
ais02-Examples.md&1.17&    - Perhaps this will require societies to introduce a basic income independently from work, because there will not be enough jobs for humans to do.
ais02-Examples.md&1.17&
ais02-Examples.md&1.18&## Some results (2)
ais02-Examples.md&1.18&
ais02-Examples.md&1.18&- Can Watson *think?* Is there moral intuition in AI systems?
ais02-Examples.md&1.18&- Can robots be *persons* and claim rights? Robot prostitution and slavery? Would this even be immoral? Why?
ais02-Examples.md&1.18&- Do we want to be dominated by a few giant, US companies?
ais02-Examples.md&1.18&    - War robots will be available only to a few countries. Some countries will therefore be able to go to war without losing soldiers, while others will still have to send their citizens to die.
ais02-Examples.md&1.18&	- Economic domination of smaller countries.
ais02-Examples.md&1.18&	- With the export of technology comes the export of moral values.
ais02-Examples.md&1.18&- War robots might make wars easier. (But would this be a bad thing if only robots are involved?)
ais02-Examples.md&1.18&
ais02-Examples.md&1.19&## Other moral problems of AI systems
ais02-Examples.md&1.19&
ais02-Examples.md&1.19&- Can/should computers be responsible for their actions?
ais02-Examples.md&1.19&- Must responsible computers have rights?
ais02-Examples.md&1.19&- Can AI systems acquire personhood?
ais02-Examples.md&1.19&- What is the moral status of bionic creatures?
ais02-Examples.md&1.19&- Are war robots immoral?
ais02-Examples.md&1.19&- Moral problems following from the emotional bonding between human and machine
ais02-Examples.md&1.19&
ais02-Examples.md&1.19&
ais02-Examples.md&1.20&## The End. Thank you for your attention!
ais02-Examples.md&1.20&
ais02-Examples.md&1.20&Questions?
ais02-Examples.md&1.20&
ais02-Examples.md&1.20&<!--
ais02-Examples.md&1.20&
ais02-Examples.md&1.20&%% Local Variables: ***
ais02-Examples.md&1.20&%% mode: markdown ***
ais02-Examples.md&1.20&%% mode: visual-line ***
ais02-Examples.md&1.20&%% mode: cua ***
ais02-Examples.md&1.20&%% mode: flyspell ***
ais02-Examples.md&1.20&%% End: ***
ais02-Examples.md&1.20&
ais02-Examples.md&1.20&-->
ais02-Examples.md&1.20&
ais02-Examples.md&1.20&
ais03-History-Claims.md&0.1&---
ais03-History-Claims.md&0.1&title:  "AI and Society: 3. History and claims of AI"
ais03-History-Claims.md&0.1&author: Andreas Matthias, Lingnan University
ais03-History-Claims.md&0.1&date: February 14, 2019
ais03-History-Claims.md&0.1&...
ais03-History-Claims.md&0.1&
ais03-History-Claims.md&1.0&# History of AI
ais03-History-Claims.md&1.0&
ais03-History-Claims.md&1.1&## Aristotle (384-322 BC)
ais03-History-Claims.md&1.1&
ais03-History-Claims.md&1.1&![](graphics/02-aristotle-2.jpg) \
ais03-History-Claims.md&1.1&
ais03-History-Claims.md&1.1&
ais03-History-Claims.md&1.1&What did he contribute to AI?
ais03-History-Claims.md&1.1&
ais03-History-Claims.md&1.1&. . . 
ais03-History-Claims.md&1.1&
ais03-History-Claims.md&1.1&Laws of thought, syllogisms.
ais03-History-Claims.md&1.1&
ais03-History-Claims.md&1.1&Textbook: <https://chatbotslife.com/aristotles-ai-ec1d4befeee6>
ais03-History-Claims.md&1.1&
ais03-History-Claims.md&1.2&## Ramon Lull (ca. 1232 – ca. 1315)
ais03-History-Claims.md&1.2&
ais03-History-Claims.md&1.2&An alphabet of human thought:
ais03-History-Claims.md&1.2&
ais03-History-Claims.md&1.2&- All concepts are nothing but combinations of a relatively small number of simple concepts
ais03-History-Claims.md&1.2&- (Just as words are combinations of letters)
ais03-History-Claims.md&1.2&- All truths may be expressed as appropriate combinations of concepts,
ais03-History-Claims.md&1.2&- which can in turn be decomposed into simple ideas.
ais03-History-Claims.md&1.2&
ais03-History-Claims.md&1.3&## Ramon Lull (ca. 1232 – ca. 1315)
ais03-History-Claims.md&1.3&
ais03-History-Claims.md&1.3&![](graphics/02-lull.jpg) \
ais03-History-Claims.md&1.3&
ais03-History-Claims.md&1.3&
ais03-History-Claims.md&1.3&The most important instance of this is the combinatory wheel, made up of moving 
ais03-History-Claims.md&1.3&concentric circles marked with figures. By revolving the wheels mentally, various combinations of the concepts represented by the figures were possible.
ais03-History-Claims.md&1.3&
ais03-History-Claims.md&1.4&## Alan Turing (1912-1954)
ais03-History-Claims.md&1.4&
ais03-History-Claims.md&1.4&![](graphics/02-turing.jpg) \
ais03-History-Claims.md&1.4&
ais03-History-Claims.md&1.4&
ais03-History-Claims.md&1.4&>- Turing *Machine* (a machine which can compute everything that is computable)
ais03-History-Claims.md&1.4&>- Turing *Test* (a test for intelligence based on conversation with a computer)
ais03-History-Claims.md&1.4&
ais03-History-Claims.md&1.4&. . . 
ais03-History-Claims.md&1.4&
ais03-History-Claims.md&1.4&Textbook: <https://towardsdatascience.com/alan-turing-1912-1954-956ccc3da6b2>
ais03-History-Claims.md&1.4&
ais03-History-Claims.md&1.5&## Turing Test
ais03-History-Claims.md&1.5&
ais03-History-Claims.md&1.5&![Source: Wikimedia](graphics/ttwm.png) \
ais03-History-Claims.md&1.5&
ais03-History-Claims.md&1.5&
ais03-History-Claims.md&1.5&Which chatter is the human, which is the machine?
ais03-History-Claims.md&1.5&
ais03-History-Claims.md&1.6&## SHRDLU (1)
ais03-History-Claims.md&1.6&
ais03-History-Claims.md&1.6&Terry Winograd (1946-): Programmer and philosopher, later an AI critic.
ais03-History-Claims.md&1.6&
ais03-History-Claims.md&1.6&![](graphics/02-shrdlu.jpg) \
ais03-History-Claims.md&1.6&
ais03-History-Claims.md&1.6&
ais03-History-Claims.md&1.6&
ais03-History-Claims.md&1.7&## SHRDLU (2)
ais03-History-Claims.md&1.7&
ais03-History-Claims.md&1.7&| Person: Pick up a big red block.
ais03-History-Claims.md&1.7&| Computer: OK.
ais03-History-Claims.md&1.7&| Person: Grasp the pyramid.
ais03-History-Claims.md&1.7&| Computer: I don't understand which pyramid you mean.
ais03-History-Claims.md&1.7&| Person (changing their mind): Find a block which is taller than the one you are holding and put it into the box.
ais03-History-Claims.md&1.7&| Computer: By "it", I assume you mean the block which is taller than the one I am holding. -- OK.
ais03-History-Claims.md&1.7&| Person: What does the box contain?
ais03-History-Claims.md&1.7&| Computer: The blue pyramid and the blue block.
ais03-History-Claims.md&1.7&| Person: What is the pyramid supported by?
ais03-History-Claims.md&1.7&| Computer: The box.
ais03-History-Claims.md&1.7&
ais03-History-Claims.md&1.7&. . .
ais03-History-Claims.md&1.7&
ais03-History-Claims.md&1.7&\vskip2ex\hrule
ais03-History-Claims.md&1.7&
ais03-History-Claims.md&1.7&*Does the program "understand"?*
ais03-History-Claims.md&1.7&
ais03-History-Claims.md&1.8&## Searle's Chinese Room
ais03-History-Claims.md&1.8&
ais03-History-Claims.md&1.8&![Source: Wikimedia](graphics/02-2-chinese-room.jpg) \
ais03-History-Claims.md&1.8&
ais03-History-Claims.md&1.8&
ais03-History-Claims.md&1.8&In a closed room, somebody who does not speak Chinese has a rule book, which tells him what to answer to every Chinese question that he encounters.
ais03-History-Claims.md&1.8&
ais03-History-Claims.md&1.8&. . .
ais03-History-Claims.md&1.8&
ais03-History-Claims.md&1.8&You can talk to him through written cards, and he always gives a sensible Chinese answer.
ais03-History-Claims.md&1.8&
ais03-History-Claims.md&1.8&. . .
ais03-History-Claims.md&1.8&
ais03-History-Claims.md&1.8&Does he *understand* Chinese?
ais03-History-Claims.md&1.8&
ais03-History-Claims.md&1.9&## Neuron
ais03-History-Claims.md&1.9&
ais03-History-Claims.md&1.9&![Biological neuron (simplified)](graphics/CNeuron-trim.png) \
ais03-History-Claims.md&1.9&
ais03-History-Claims.md&1.9&
ais03-History-Claims.md&1.10&## Artificial neural networks
ais03-History-Claims.md&1.10&
ais03-History-Claims.md&1.10&![A three-layer artificial neural network](graphics/CLayerNetwork-trim.png) \
ais03-History-Claims.md&1.10&
ais03-History-Claims.md&1.10&
ais03-History-Claims.md&1.11&## Artificial neural networks (2)
ais03-History-Claims.md&1.11&
ais03-History-Claims.md&1.11&![How a neural network recognises a letter](graphics/Cocr-trim.png) \
ais03-History-Claims.md&1.11&
ais03-History-Claims.md&1.11&
ais03-History-Claims.md&1.11&
ais03-History-Claims.md&1.12&## Artificial neural networks (3)
ais03-History-Claims.md&1.12&
ais03-History-Claims.md&1.12&- Computer vision
ais03-History-Claims.md&1.12&- Handwriting recognition
ais03-History-Claims.md&1.12&- Face recognition
ais03-History-Claims.md&1.12&- Image recognition
ais03-History-Claims.md&1.12&
ais03-History-Claims.md&1.12&. . . 
ais03-History-Claims.md&1.12&
ais03-History-Claims.md&1.12&Generally:
ais03-History-Claims.md&1.12&
ais03-History-Claims.md&1.12&>- Can be *trained* by example, rather than having to write a program to do some task.
ais03-History-Claims.md&1.12&>- Good at recognising features even when the pattern perceived is slightly different from the pattern that was learned!
ais03-History-Claims.md&1.12&
ais03-History-Claims.md&1.13&## Artificial neural networks (4)
ais03-History-Claims.md&1.13&
ais03-History-Claims.md&1.13&- Rosenblatt (1962): Perceptrons.
ais03-History-Claims.md&1.13&- Artificial Neural Netwoks (ANN) again after 1986. Multiple layers.
ais03-History-Claims.md&1.13&- Deep learning, deep neural networks (more than 2 layers) (Igor Aizenberg and colleagues, 2000)
ais03-History-Claims.md&1.13&
ais03-History-Claims.md&1.14&## Facebook DeepFace recognition
ais03-History-Claims.md&1.14&
ais03-History-Claims.md&1.14&![Source: facebook](graphics/02-deepface.png) \
ais03-History-Claims.md&1.14&
ais03-History-Claims.md&1.14&
ais03-History-Claims.md&1.15&## Image style transfer (1)
ais03-History-Claims.md&1.15&
ais03-History-Claims.md&1.15&![Credit: Twitter user \@DmitryUlyanovML](graphics/01-style1.jpg) \
ais03-History-Claims.md&1.15&
ais03-History-Claims.md&1.15&
ais03-History-Claims.md&1.16&## Image style transfer (2)
ais03-History-Claims.md&1.16&
ais03-History-Claims.md&1.16&![](graphics/01-style2.jpg) \
ais03-History-Claims.md&1.16&
ais03-History-Claims.md&1.16&
ais03-History-Claims.md&1.16&
ais03-History-Claims.md&1.17&## Evolutionary computing
ais03-History-Claims.md&1.17&
ais03-History-Claims.md&1.17&Holland: Genetic algorithm (1975).
ais03-History-Claims.md&1.17&
ais03-History-Claims.md&1.17&![](graphics/02-ga.png) \
ais03-History-Claims.md&1.17&
ais03-History-Claims.md&1.17&
ais03-History-Claims.md&1.18&## Moore's Law (1)
ais03-History-Claims.md&1.18&
ais03-History-Claims.md&1.18&Gordon E. Moore, co-founder of Intel (1965): Density of electronic chips doubles every two years. -- *Why is this important?*
ais03-History-Claims.md&1.18&
ais03-History-Claims.md&1.18&![Credit: https://commons.wikimedia.org/wiki/User:Wgsimon](graphics/02-moores-law.png) \
ais03-History-Claims.md&1.18&
ais03-History-Claims.md&1.18&
ais03-History-Claims.md&1.19&## Moore's Law (2)
ais03-History-Claims.md&1.19&
ais03-History-Claims.md&1.19&An "Osborne Executive" portable computer, from 1982, and an iPhone, released 2007 (iPhone 3G in picture).
ais03-History-Claims.md&1.19&
ais03-History-Claims.md&1.19&The Executive weighs 100 times as much, has nearly 500 times the volume, cost 10 times as much, and has a 100th the clock frequency of the iPhone. (Wikipedia)
ais03-History-Claims.md&1.19&
ais03-History-Claims.md&1.19&![](graphics/02-moore-compare.png) \
ais03-History-Claims.md&1.19&
ais03-History-Claims.md&1.19&
ais03-History-Claims.md&2.0&# Claims of AI
ais03-History-Claims.md&2.0&
ais03-History-Claims.md&2.1&## Claims of AI
ais03-History-Claims.md&2.1&
ais03-History-Claims.md&2.1&Try to formulate the central claim of AI!
ais03-History-Claims.md&2.1&
ais03-History-Claims.md&2.1&>- It is *not* to make an artificial human!
ais03-History-Claims.md&2.1&>- Why not?
ais03-History-Claims.md&2.1&>       - We don't want it to have rights
ais03-History-Claims.md&2.1&>	     - We don't want it to forget things
ais03-History-Claims.md&2.1&>	     - We don't want it to fall in love
ais03-History-Claims.md&2.1&>	     - We don't want it to be in bad mood
ais03-History-Claims.md&2.1&>	     - We don't want it to spend money for itself
ais03-History-Claims.md&2.1&>	     - We don't want it to take 18 years of learning in order to be useful
ais03-History-Claims.md&2.1&
ais03-History-Claims.md&2.2&## "Strong" and "weak" AI
ais03-History-Claims.md&2.2&
ais03-History-Claims.md&2.2&>- **“Strong AI”:** An artificial intelligence system can think and have a mind, mental states, and consciousness.
ais03-History-Claims.md&2.2&>- **“Weak AI”:** Machines can demonstrate (simulate?) intelligence, but do not necessarily have a mind, mental states or consciousness.
ais03-History-Claims.md&2.2&
ais03-History-Claims.md&2.2&. . . 
ais03-History-Claims.md&2.2&
ais03-History-Claims.md&2.2&Textbook: <https://aboveintelligent.com/aims-and-claims-of-ai-60150ac202c6>
ais03-History-Claims.md&2.2&
ais03-History-Claims.md&2.3&## Psychological and phenomenal concepts of mind (Chalmers)
ais03-History-Claims.md&2.3&
ais03-History-Claims.md&2.3&David J. Chalmers, “The Conscious Mind”:
ais03-History-Claims.md&2.3&
ais03-History-Claims.md&2.3&>- The phenomenal “… is the concept of mind as conscious experience, and of a mental state as a consciously experienced mental state” (p.11)
ais03-History-Claims.md&2.3&>- The psychological “… is the concept of mind as the causal or explanatory basis for behaviour … it plays the right sort of causal role in the production of behaviour, or at least plays an appropriate role in the explanation of behaviour … What matters is the role it plays in a cognitive economy” (p.11)
ais03-History-Claims.md&2.3&
ais03-History-Claims.md&2.3&. . . 
ais03-History-Claims.md&2.3&
ais03-History-Claims.md&2.3&**“On the phenomenal concept, mind is characterized by the way it feels; on the psychological concept, mind is characterized by what it does.” **
ais03-History-Claims.md&2.3&
ais03-History-Claims.md&2.4&## AI and Philosophy of Mind
ais03-History-Claims.md&2.4&
ais03-History-Claims.md&2.4&>- Much of the discussion on mind has to do with phenomenal properties. It is not relevant to AI that only *behaves* like humans.
ais03-History-Claims.md&2.4&>- It would be relevant to AI that *feels* like humans.
ais03-History-Claims.md&2.4&>- *But do we even want such AI systems?*
ais03-History-Claims.md&2.4&
ais03-History-Claims.md&2.5&## Differences strong/weak claim?
ais03-History-Claims.md&2.5&
ais03-History-Claims.md&2.5&>- The distinction weak/strong AI corresponds to the distinction in the Philosophy of Mind between the *psychological* and the *phenomenal* aspects of mind.
ais03-History-Claims.md&2.5&>- A strong AI system would think.
ais03-History-Claims.md&2.5&>- A weak AI system would “think” (or: pretend to think).
ais03-History-Claims.md&2.5&>- Normally AI researchers don't worry about strong AI.
ais03-History-Claims.md&2.5&>- *Why not???*
ais03-History-Claims.md&2.5&>- Target of AI is to make intelligent machines, not minds!
ais03-History-Claims.md&2.5&
ais03-History-Claims.md&2.6&## Consequences of phenomenally complete  AI
ais03-History-Claims.md&2.6&
ais03-History-Claims.md&2.6&What would be some consequences of “phenomenally complete” AI?
ais03-History-Claims.md&2.6&
ais03-History-Claims.md&2.6&. . . 
ais03-History-Claims.md&2.6&
ais03-History-Claims.md&2.6&>- Machines would feel pain.
ais03-History-Claims.md&2.6&>- Machines would be tired, in bad mood.
ais03-History-Claims.md&2.6&>- Machines might be “persons.”
ais03-History-Claims.md&2.6&>- Machines would probably need to have rights and be protected just like humans or higher animals.
ais03-History-Claims.md&2.6&
ais03-History-Claims.md&2.7&## Some criticisms of weak AI
ais03-History-Claims.md&2.7&
ais03-History-Claims.md&2.7&>- No machine has yet passed the Turing Test (or come close).
ais03-History-Claims.md&2.7&>- Machines still have very little common sense and are not good with ambiguities.
ais03-History-Claims.md&2.7&>- "Full" intelligence might need a body and an environment to grow in.
ais03-History-Claims.md&2.7&
ais03-History-Claims.md&2.8&## Some criticisms of strong AI
ais03-History-Claims.md&2.8&
ais03-History-Claims.md&2.8&>- Searle's Chinese Room (does a machine “understand”)?
ais03-History-Claims.md&2.8&>- Can a machine have mental states that “feel” like something? (Comparable to how it "feels" to a human to have fear or to fall in love)
ais03-History-Claims.md&2.8&>- Substance dualist positions would require us to put a “mind substance” into the computer.
ais03-History-Claims.md&2.8&>- In the extreme case we might need 18 years to grow an unreliable machine with all the drawbacks of humans (because it might be that advantageous and negative traits of the mind go necessarily together!)
ais03-History-Claims.md&2.8&
ais03-History-Claims.md&2.8&
ais03-History-Claims.md&2.8&
ais03-History-Claims.md&2.9&## The End. Thank you for your attention!
ais03-History-Claims.md&2.9&
ais03-History-Claims.md&2.9&Questions?
ais03-History-Claims.md&2.9&
ais03-History-Claims.md&2.9&<!--
ais03-History-Claims.md&2.9&
ais03-History-Claims.md&2.9&%% Local Variables: ***
ais03-History-Claims.md&2.9&%% mode: markdown ***
ais03-History-Claims.md&2.9&%% mode: visual-line ***
ais03-History-Claims.md&2.9&%% mode: cua ***
ais03-History-Claims.md&2.9&%% mode: flyspell ***
ais03-History-Claims.md&2.9&%% End: ***
ais03-History-Claims.md&2.9&
ais03-History-Claims.md&2.9&-->
ais04-Definitions.md&0.1&---
ais04-Definitions.md&0.1&title:  "AI and Society: 4. Detecting intelligence. Definitions of AI."
ais04-Definitions.md&0.1&author: Andreas Matthias, Lingnan University
ais04-Definitions.md&0.1&date: September 2, 2019
ais04-Definitions.md&0.1&...
ais04-Definitions.md&0.1&
ais04-Definitions.md&0.1&
ais04-Definitions.md&1.0&# How to detect intelligence?
ais04-Definitions.md&1.0&
ais04-Definitions.md&1.1&## "Intelligent" robots?
ais04-Definitions.md&1.1&
ais04-Definitions.md&1.1&>- Assume you want to find out whether a robot, that stands in front of you, is intelligent.
ais04-Definitions.md&1.1&>- How could you judge that?
ais04-Definitions.md&1.1&>- More generally, how could you judge that *any* thing is intelligent?
ais04-Definitions.md&1.1&>- I can ask the same questions about a dog, or a chair: 
ais04-Definitions.md&1.1&>     - “Does it think?”
ais04-Definitions.md&1.1&>     - “Is it intelligent?”
ais04-Definitions.md&1.1&>     - “And how would I know?”
ais04-Definitions.md&1.1&
ais04-Definitions.md&1.2&## Are chairs intelligent? Structural equivalence
ais04-Definitions.md&1.2&
ais04-Definitions.md&1.2&>- “Is this chair intelligent?” How to decide? How to know *for sure?*
ais04-Definitions.md&1.2&>- One way is to argue like this:
ais04-Definitions.md&1.2&>     - Thinking (and intelligence) requires a brain.
ais04-Definitions.md&1.2&>     - Chairs don’t have brains. We can cut them open and see that they are just made of wood and more wood. The wood is not structured anything like a brain.
ais04-Definitions.md&1.2&>     - Conclusion: Chairs cannot possibly be thinking.
ais04-Definitions.md&1.2&>- This is not an entirely bad argument (nor is it very strong). We will call this the **structural equivalence** argument.
ais04-Definitions.md&1.2&
ais04-Definitions.md&1.3&## Behavioural equivalence (1)
ais04-Definitions.md&1.3&
ais04-Definitions.md&1.3&>- Another way would be to look at the *behaviour* of the chair. 
ais04-Definitions.md&1.3&>- What does a chair do? 
ais04-Definitions.md&1.3&>     - It just sits there. 
ais04-Definitions.md&1.3&>     - You kick it, it doesn’t move away. 
ais04-Definitions.md&1.3&>     - You put fire to it, it doesn’t protest. 
ais04-Definitions.md&1.3&>     - You talk to it, it doesn’t answer.
ais04-Definitions.md&1.3&>     - A dog, on the other hand, does display all sorts of interesting reactions: it comes when called, flees or fights when attacked, and runs away from fire. 
ais04-Definitions.md&1.3&>     - Humans have even more nuanced behavioural responses. 
ais04-Definitions.md&1.3&
ais04-Definitions.md&1.4&## Behavioural equivalence (2)
ais04-Definitions.md&1.4&
ais04-Definitions.md&1.4&>- We could say that some thing X is intelligent if (and only if) the behaviour of X in various situations is similar to the behaviour of another thing Y that is known (or assumed) to be intelligent; a human, for example.
ais04-Definitions.md&1.4&>- If we compare the behaviour of a chair to a human, we can conclude that it is not intelligent at all.
ais04-Definitions.md&1.4&>- If we compare the behaviour of a dog to a human, we can conclude that it is somewhat intelligent, but it lacks some of the more interesting behaviours (for example, to read a book, or create poetry). 
ais04-Definitions.md&1.4&>- This is the **behavioural equivalence** argument.
ais04-Definitions.md&1.4&
ais04-Definitions.md&1.5&## Functional equivalence
ais04-Definitions.md&1.5&
ais04-Definitions.md&1.5&>- ‘Intelligence’ is not something rooted in material structure alone (like in the structural equivalence thesis); and also not as some purely behavioural attribute (like with the behavioural equivalence thesis).
ais04-Definitions.md&1.5&>- Instead, we could try to distinguish ‘hardware’ from ‘software,’ the *structural* from the *functional* aspect of an artefact’s operation. 
ais04-Definitions.md&1.5&>- Example: a computer. 
ais04-Definitions.md&1.5&>     - A program that adds two numbers can run on any number of different hardware platforms. Your mobile phone can add two numbers. Your desktop computer can. An abacus can, too. Or you can do it with pen and pencil. 
ais04-Definitions.md&1.5&>     - All these different ‘hardware’ implementations could be said to run the same ‘program’: the ‘software’ that actually performs the function of adding two numbers together. We will call this the **functional equivalence** thesis.
ais04-Definitions.md&1.5&
ais04-Definitions.md&1.6&## Functional vs behavioural equivalence
ais04-Definitions.md&1.6&
ais04-Definitions.md&1.6&>- As opposed to a purely behavioural equivalence, the functional equivalence does take into account the *abstract organisation of the hardware that performs the function* we are examining. When I add two numbers, there are a number of functional units that are required:
ais04-Definitions.md&1.6&>     - I need to remember which numbers I am adding, and any intermediate results (a kind of memory).
ais04-Definitions.md&1.6&>     - I need some functional unit that can take two digits and perform the actual addition, keeping track of any carry-over numbers (a kind of processing unit).
ais04-Definitions.md&1.6&>     - I need some mechanism that controls the whole process and remembers which digits to add next, and where to put the result (a kind of operations controller).
ais04-Definitions.md&1.6&>     - I need some input and output unit: a way to enter the numbers to be added, and a way to see the result (an I/O interface).
ais04-Definitions.md&1.6&
ais04-Definitions.md&1.7&## Functional vs structural equivalence
ais04-Definitions.md&1.7&
ais04-Definitions.md&1.7&>- What makes this approach different form the structural equivalence is that now I can account for differences in structure. 
ais04-Definitions.md&1.7&>- I can distinguish *function* from *implementation* (the way a function is physically realised in a particular piece of hardware). 
ais04-Definitions.md&1.7&>- A calculator, an abacus, and a human with a pencil and paper all have the same functional units described above, although these are implemented in very different ways in their respective hardware (electronics, wooden beads, neurons and muscles). 
ais04-Definitions.md&1.7&>- Still, a functional description could easily identify that these devices are, despite their hardware differences, functionally equivalent.
ais04-Definitions.md&1.7&
ais04-Definitions.md&1.8&## Problems of the structural equivalence thesis (1)
ais04-Definitions.md&1.8&
ais04-Definitions.md&1.8&>- How can we criticise the structural equivalence thesis?
ais04-Definitions.md&1.8&>- Who says that the chair’s wood is structurally dissimilar to a nervous system? They’re both made up of cells. Just from looking at the wood’s structure under a microscope, it would be very difficult to conclude that it cannot do what the nervous system can do.
ais04-Definitions.md&1.8&>- The argument ignores the possibility of having intelligence in a different structure. 
ais04-Definitions.md&1.8&>     - It is like saying that a table has to be made of wood, and have four legs. This is not true.
ais04-Definitions.md&1.8&>     - Why should the ‘table-ness’ of an object depend on the material it’s made of, or an incidental property like the number of its legs?
ais04-Definitions.md&1.8&>- This argument begs the question regarding AI: no AI can be possible as long as artefacts don’t have biological, human brains. Thus, artificial intelligence is impossible by definition.
ais04-Definitions.md&1.8&
ais04-Definitions.md&1.9&## Problems of the structural equivalence thesis (2)
ais04-Definitions.md&1.9&
ais04-Definitions.md&1.9&>- No two brains are identical. Even human brains show some variation between individuals. If one insists on strict structural equivalence, one must say that only the specific reference brain is optimally thinking. All other brains, being structurally not exactly the same, will necessarily have a lower intelligence, because the are, to some extent, structurally dissimilar to the ‘reference’ brain.
ais04-Definitions.md&1.9&>- Structural equivalence as a test for intelligence is implausible, because this is simply not how we judge intelligence in our lives. 
ais04-Definitions.md&1.9&>     - When you ask yourself if someone is intelligent, or more or less intelligent than you, then you don’t go and dissect their brain to find out.
ais04-Definitions.md&1.9&>     - You just look at them. If they can calculate faster, solve logic puzzles, and understand relativity theory, then you might admit that they are intelligent. 
ais04-Definitions.md&1.9&>     - So we never actually use structural equivalence to judge intelligence. Instead, we use behavioural criteria.
ais04-Definitions.md&1.9&
ais04-Definitions.md&1.10&## Problems of the behavioural equivalence thesis (1)
ais04-Definitions.md&1.10&
ais04-Definitions.md&1.10&>- Behavioural equivalence underlies the most famous test for artificial intelligence, the Turing Test: If a computer can chat in a way that is indistinguishable from chatting with another human, then the computer can be said to be intelligent. What could be wrong with that?
ais04-Definitions.md&1.10&>- Not all intelligent systems need to be able to do everyday chatter (self-driving cars, image recognition programs, chess- and go-playing programs, programs that diagnose cancer and heart diseases, and many more). None of these can talk. But are they therefore not intelligent?
ais04-Definitions.md&1.10&
ais04-Definitions.md&1.11&## Problems of the behavioural equivalence thesis (2)
ais04-Definitions.md&1.11&
ais04-Definitions.md&1.11&>- Even clearly intelligent people sometimes fail to converse: intelligent people in foreign countries whose language they don’t speak. Such a person would fail the Turing Test in the foreign country, but it would be wrong to conclude that they are not intelligent.
ais04-Definitions.md&1.11&>- A programmed character in a computer game can be made do perform any action and portray any feeling: pain, fear, love. Still, we clearly understand that these are programmed behaviours, and that the computer program does not actually understand or feel anything.
ais04-Definitions.md&1.11&>- The Chinese Room argument (see a previous lecture).
ais04-Definitions.md&1.11&
ais04-Definitions.md&1.12&## The functional equivalence thesis (1)
ais04-Definitions.md&1.12&
ais04-Definitions.md&1.12&>- A functionalist, looking at the possibility that something (a chair, a Chinese room, an alien, a robot) is intelligent, would ask: does this candidate have a functional organisation that is likely to lead to intelligent, adaptive behaviour? 
ais04-Definitions.md&1.12&>- Although an MP3 player might be able to talk or make music, the functionalist would see that it lacks the functional units needed to autonomously generate speech or music. It is just playing a pre-recorded sound.
ais04-Definitions.md&1.12&>- The Chinese room is not intelligent because the functionalist could see that it does not contain the right functional units that are needed for intelligence: the Chinese room has no memory, no unit that associates symbols with meaning, no way to acquire meaning from experience.
ais04-Definitions.md&1.12&
ais04-Definitions.md&1.13&## The functional equivalence thesis (2)
ais04-Definitions.md&1.13&
ais04-Definitions.md&1.13&>- Functional equivalence does not require the equivalent things to be structurally similar. 
ais04-Definitions.md&1.13&>     - The wing of a bird is, for example, functionally equivalent to the wing of a butterfly, and both are (within limits) functionally equivalent to the wings of airplanes.
ais04-Definitions.md&1.13&>     - But all three are structurally different, are made of different materials, and even work physically in different ways.
ais04-Definitions.md&1.13&>     - Still, in the functional economy of a thing that flies, they perform similar functions, and thus could be called functionally equivalent.
ais04-Definitions.md&1.13&>- The functionalist would be able to recognise that an alien is intelligent, even if his brain is made up of entirely different materials. He would see the possibility that a (suitably complex) computer might be intelligent.
ais04-Definitions.md&1.13&
ais04-Definitions.md&1.14&## Problems of the functional equivalence thesis: Chinese nation (1)
ais04-Definitions.md&1.14&
ais04-Definitions.md&1.14&>- The Chinese nation argument (this has nothing to do with the Chinese room!):
ais04-Definitions.md&1.14&>- Assume you have a brain that works well using one billion neurons (this is about a hundredth of what our brains have, but this doesn’t matter for the experiment. We can make the argument with any number). 
ais04-Definitions.md&1.14&>- Every neuron in this brain is connected with other neurons. 
ais04-Definitions.md&1.14&>- Signals travel through the brain when a neuron receives signals from other neurons on its ‘input’ side, and then produces a new signal on its ‘output’ side. This signal is then propagated as an input to other neurons. 
ais04-Definitions.md&1.14&
ais04-Definitions.md&1.15&## Problems of the functional equivalence thesis: Chinese nation (2)
ais04-Definitions.md&1.15&
ais04-Definitions.md&1.15&>- Now I could, in principle, take out any one neuron in the brain and put a human in there to play the role of that missing neuron. I’d give instructions to the human to act exactly like the neuron he replaces: when the input signals are such-and-such, he should initiate an output signal. Otherwise, he should not. I give him dials to show the input signals, and a button to produce an output signal that is connected to other neurons.
ais04-Definitions.md&1.15&>- If this man does his job as advertised, the brain should continue to function exactly as before. 
ais04-Definitions.md&1.15&>- Now I can replace more neurons with more people and dials and buttons. Every time, I replace one neuron by one person that acts exactly as the neuron did. 
ais04-Definitions.md&1.15&>- Now you see why we need a *Chinese* nation (rather than a Greek, or Maltese) to play neurons. 
ais04-Definitions.md&1.15&
ais04-Definitions.md&1.16&## Problems of the functional equivalence thesis: Chinese nation (3)
ais04-Definitions.md&1.16&
ais04-Definitions.md&1.16&>- What if I replace all one billion neurons with one billion Chinese people? 
ais04-Definitions.md&1.16&>- Accepting the premises of the argument, I would expect nothing strange to happen. The brain should work just as before, although it now does not contain a single neuron. 
ais04-Definitions.md&1.16&>- The brain should be able to converse, make jokes, play chess, and write love poems. 
ais04-Definitions.md&1.16&>- But at the same time, none of the people who are involved in this brain converses, jokes, plays chess or writes poems. They only press a button that fires ‘their’ neuron. 
ais04-Definitions.md&1.16&>- So how does this ‘magic’ ability of the brain to do these things come from? And where is the brain’s consciousness now stored?
ais04-Definitions.md&1.16&
ais04-Definitions.md&1.17&## Problems of the functional equivalence thesis: Chinese nation (4)
ais04-Definitions.md&1.17&
ais04-Definitions.md&1.17&>- The point of the argument is this: 
ais04-Definitions.md&1.17&>     - If functionalism is correct, then the Chinese nation brain should be identical in function to the original brain. 
ais04-Definitions.md&1.17&>     - It has exactly the same functional units that implement exactly the same ‘software’ (or behaviour). 
ais04-Definitions.md&1.17&>     - It should then also have consciousness, pain, emotions, and a sense of self. 
ais04-Definitions.md&1.17&>     - But where are these things? The sense of self of that Chinese nation brain is nowhere in the people that make it up. Has the brain’s sense of self then suddenly disappeared? 
ais04-Definitions.md&1.17&>     - So either functionalism is wrong, or we must attribute some mysterious sense of self, humour, emotions and consciousness to a collection of people who don’t have (as individuals) any of these states (they have their own, but not the original brain’s!).
ais04-Definitions.md&1.17&
ais04-Definitions.md&1.17&
ais04-Definitions.md&2.0&# Definitions of AI
ais04-Definitions.md&2.0&
ais04-Definitions.md&2.1&## Discussion
ais04-Definitions.md&2.1&
ais04-Definitions.md&2.1&Textbook: <https://medium.com/@MoralRobots/what-is-ai-some-classic-definitions-a85d6acc4cb8>
ais04-Definitions.md&2.1&
ais04-Definitions.md&2.2&## Systems that do things that require intelligence of humans (1)
ais04-Definitions.md&2.2&
ais04-Definitions.md&2.2&> "AI is concerned with building machines that can act and react appropriately, adapting their response to the demands of the situation. Such machines should display behaviour compatible with that considered to require intelligence in humans." (Finlay-Dix)
ais04-Definitions.md&2.2&
ais04-Definitions.md&2.2&> "The act of creating machines that perform functions that require intelligence when performed by people." (Kurzweil, 1990)
ais04-Definitions.md&2.2&
ais04-Definitions.md&2.2&>- How can we criticise these definitions?
ais04-Definitions.md&2.2&
ais04-Definitions.md&2.3&## Systems that do things that require intelligence of humans (2)
ais04-Definitions.md&2.3&
ais04-Definitions.md&2.3&>- "Behaviour compatible with what is considered to require intelligence in humans:"
ais04-Definitions.md&2.3&>   - To add two numbers.
ais04-Definitions.md&2.3&>	- To change money when a customer buys a coke.
ais04-Definitions.md&2.3&>	- To regulate the room temperature by turning an air-conditioner on or off.
ais04-Definitions.md&2.3&>- Obviously, these things can be done by primitive, non-intelligent machines (calculators, coke vending machines, air-con thermostats).
ais04-Definitions.md&2.3&
ais04-Definitions.md&2.4&## Systems that *think like humans*
ais04-Definitions.md&2.4&
ais04-Definitions.md&2.4&> "The exciting new effort to make computers think ... machines with minds, in the full and literal sense." (Haugeland, 1985)
ais04-Definitions.md&2.4&
ais04-Definitions.md&2.4&> "The automation of activities that we associate with human thinking, activities such as decision-making, problem solving, learning..." (Bellman, 1978)
ais04-Definitions.md&2.4&
ais04-Definitions.md&2.4&>- How can you criticise Haugeland's definition?
ais04-Definitions.md&2.4&>    - This would be strong AI only.
ais04-Definitions.md&2.4&
ais04-Definitions.md&2.5&## Systems that *act like humans*
ais04-Definitions.md&2.5&
ais04-Definitions.md&2.5&> "The study of how to make computers do things at which, at the moment, people are better." (Rich and Knight, 1991)
ais04-Definitions.md&2.5&
ais04-Definitions.md&2.5&>- Criticism?
ais04-Definitions.md&2.5&>   - Digestion?
ais04-Definitions.md&2.5&>	- This is self-defeating. As soon as machines get better than humans at X, doing X will stop being an example of AI
ais04-Definitions.md&2.5&
ais04-Definitions.md&2.6&## Systems that *think rationally*
ais04-Definitions.md&2.6&
ais04-Definitions.md&2.6&> "The study of mental faculties through the use of computational models" (Charniak and McDermott, 1985)
ais04-Definitions.md&2.6&
ais04-Definitions.md&2.6&> "The study of the computations that make it possible to perceive, reason, and act." (Winston, 1992)
ais04-Definitions.md&2.6&
ais04-Definitions.md&2.6&>- Criticism:
ais04-Definitions.md&2.6&>    - Charniak/McDermott: No application. A "study" is not AI!
ais04-Definitions.md&2.6&>    - Winston: too narrow. Presupposes that intelligence *is* computation, which is begging the question!
ais04-Definitions.md&2.6&
ais04-Definitions.md&2.7&## Systems that *act rationally*
ais04-Definitions.md&2.7&
ais04-Definitions.md&2.7&> "Computational intelligence is the study of the design of intelligent agents." (Poole et al, 1998)
ais04-Definitions.md&2.7&
ais04-Definitions.md&2.7&> "AI ... is concerned with intelligent behaviour in artefacts." (Nilsson, 1998)
ais04-Definitions.md&2.7&
ais04-Definitions.md&2.7&>- Criticism:
ais04-Definitions.md&2.7&>    - Both are circular. Poole even narrows it down to computational intelligence, which is only a subset of AI, and not even the most promising one.
ais04-Definitions.md&2.7&
ais04-Definitions.md&2.8&## Summary of definitions
ais04-Definitions.md&2.8&
ais04-Definitions.md&2.8&![](graphics/03-claims.png) \
ais04-Definitions.md&2.8& 
ais04-Definitions.md&2.8&
ais04-Definitions.md&2.9&## More reading materials about this section
ais04-Definitions.md&2.9&
ais04-Definitions.md&2.9&- Aims and claims of AI: <https://moral-robots.com/philosophy/aims-and-claims-of-ai/>
ais04-Definitions.md&2.9&- Behavioural, functional, structural equivalence: <https://moral-robots.com/philosophy/do-chairs-think-ais-three-kinds-of-equivalence/>
ais04-Definitions.md&2.9&- Aristotle’s influence on AI: <https://moral-robots.com/philosophy/aristotles-ai/>
ais04-Definitions.md&2.9&- Alan Turing: <https://moral-robots.com/philosophy/alan-turing-1912-1954/>
ais04-Definitions.md&2.9&- AI in Games: <https://moral-robots.com/philosophy/philosophical-issues-ai-in-games-2017/>
ais04-Definitions.md&2.9&
ais04-Definitions.md&2.9&... and all the “Textbook” links above.
ais04-Definitions.md&2.9&
ais04-Definitions.md&2.9&
ais04-Definitions.md&2.9&
ais04-Definitions.md&2.10&## The End. Thank you for your attention!
ais04-Definitions.md&2.10&
ais04-Definitions.md&2.10&Questions?
ais04-Definitions.md&2.10&
ais04-Definitions.md&2.10&<!--
ais04-Definitions.md&2.10&
ais04-Definitions.md&2.10&%% Local Variables: ***
ais04-Definitions.md&2.10&%% mode: markdown ***
ais04-Definitions.md&2.10&%% mode: visual-line ***
ais04-Definitions.md&2.10&%% mode: cua ***
ais04-Definitions.md&2.10&%% mode: flyspell ***
ais04-Definitions.md&2.10&%% End: ***
ais04-Definitions.md&2.10&
ais04-Definitions.md&2.10&-->
ais05-Prolog-Symbols-Meaning.md&0.1&---
ais05-Prolog-Symbols-Meaning.md&0.1&title:  "AI and Society: 5. Prolog. Symbols and meaning"
ais05-Prolog-Symbols-Meaning.md&0.1&author: Andreas Matthias, Lingnan University
ais05-Prolog-Symbols-Meaning.md&0.1&date: September 2, 2019
ais05-Prolog-Symbols-Meaning.md&0.1&...
ais05-Prolog-Symbols-Meaning.md&0.1&
ais05-Prolog-Symbols-Meaning.md&1.0&# Prolog
ais05-Prolog-Symbols-Meaning.md&1.0&
ais05-Prolog-Symbols-Meaning.md&1.1&## Prolog
ais05-Prolog-Symbols-Meaning.md&1.1&
ais05-Prolog-Symbols-Meaning.md&1.1&Prolog ("Programming in Logic," 1972) is a programming language used to represent simple deductive reasoning from facts and rules.
ais05-Prolog-Symbols-Meaning.md&1.1&
ais05-Prolog-Symbols-Meaning.md&1.1&Prolog and similar languages (for example, Answer Set Programming) are still used in AI today, for example in systems that try to derive conclusions from particular premises or to store knowledge in a rule-based format.
ais05-Prolog-Symbols-Meaning.md&1.1&
ais05-Prolog-Symbols-Meaning.md&1.1&Try it out here:
ais05-Prolog-Symbols-Meaning.md&1.1&
ais05-Prolog-Symbols-Meaning.md&1.1&<http://swish.swi-prolog.org>
ais05-Prolog-Symbols-Meaning.md&1.1&
ais05-Prolog-Symbols-Meaning.md&1.1&or
ais05-Prolog-Symbols-Meaning.md&1.1&
ais05-Prolog-Symbols-Meaning.md&1.1&<http://www.tau-prolog.org>
ais05-Prolog-Symbols-Meaning.md&1.1&
ais05-Prolog-Symbols-Meaning.md&1.1&Textbook: <https://moral-robots.com/technology/prolog-programming-in-logic/>
ais05-Prolog-Symbols-Meaning.md&1.1&
ais05-Prolog-Symbols-Meaning.md&1.2&## Example
ais05-Prolog-Symbols-Meaning.md&1.2&
ais05-Prolog-Symbols-Meaning.md&1.2&>- Prolog: **cat(tom).**
ais05-Prolog-Symbols-Meaning.md&1.2&>- English interpretation: “Tom is a cat.”
ais05-Prolog-Symbols-Meaning.md&1.2&>- Note: You have to write "tom" in lower case. Words that begin with a big letter are *variables* in Prolog.
ais05-Prolog-Symbols-Meaning.md&1.2&
ais05-Prolog-Symbols-Meaning.md&1.3&## Queries
ais05-Prolog-Symbols-Meaning.md&1.3&
ais05-Prolog-Symbols-Meaning.md&1.3&>- Prolog: **? cat(tom).**
ais05-Prolog-Symbols-Meaning.md&1.3&>- English: “Is Tom a cat?”
ais05-Prolog-Symbols-Meaning.md&1.3&>- Answer: true.
ais05-Prolog-Symbols-Meaning.md&1.3&
ais05-Prolog-Symbols-Meaning.md&1.3&>- Prolog: **? cat(X).**
ais05-Prolog-Symbols-Meaning.md&1.3&>- English: "Which X is a cat?"
ais05-Prolog-Symbols-Meaning.md&1.3&>- X is an uppercase letter, so Prolog knows it's a variable!
ais05-Prolog-Symbols-Meaning.md&1.3&>- Answer: X=tom.
ais05-Prolog-Symbols-Meaning.md&1.3&
ais05-Prolog-Symbols-Meaning.md&1.4&## Prolog does not "understand"!
ais05-Prolog-Symbols-Meaning.md&1.4&
ais05-Prolog-Symbols-Meaning.md&1.4&- Observe that the system does not know anything about cats or Tom.
ais05-Prolog-Symbols-Meaning.md&1.4&- It just uses the same symbols the human operator used.
ais05-Prolog-Symbols-Meaning.md&1.4&- It is up to the operator to project an interpretation into those symbols!
ais05-Prolog-Symbols-Meaning.md&1.4&
ais05-Prolog-Symbols-Meaning.md&1.4&For example:
ais05-Prolog-Symbols-Meaning.md&1.4&
ais05-Prolog-Symbols-Meaning.md&1.4&>- Prolog: **gugu(lala).**
ais05-Prolog-Symbols-Meaning.md&1.4&>- English interpretation: ?
ais05-Prolog-Symbols-Meaning.md&1.4&>- Prolog: **? gugu(X).**
ais05-Prolog-Symbols-Meaning.md&1.4&>- English: "Which X is a gugu?"
ais05-Prolog-Symbols-Meaning.md&1.4&>- Answer: X=lala.
ais05-Prolog-Symbols-Meaning.md&1.4&
ais05-Prolog-Symbols-Meaning.md&1.5&## Syntactic manipulation
ais05-Prolog-Symbols-Meaning.md&1.5&
ais05-Prolog-Symbols-Meaning.md&1.5&“Syntactic” symbolic AI assumes that, in many cases, the computer does not need to understand the semantics (=meaning) of the symbols used.
ais05-Prolog-Symbols-Meaning.md&1.5&
ais05-Prolog-Symbols-Meaning.md&1.5&. . . 
ais05-Prolog-Symbols-Meaning.md&1.5&
ais05-Prolog-Symbols-Meaning.md&1.6&### Assumption of symbolic AI:
ais05-Prolog-Symbols-Meaning.md&1.6&
ais05-Prolog-Symbols-Meaning.md&1.6&If the symbol manipulation preserves the original relations between the symbols, the mapping of symbol to meaning can be left to mind of the operator.
ais05-Prolog-Symbols-Meaning.md&1.6&
ais05-Prolog-Symbols-Meaning.md&1.6&
ais05-Prolog-Symbols-Meaning.md&1.7&## Mapping of symbols to meaning
ais05-Prolog-Symbols-Meaning.md&1.7&
ais05-Prolog-Symbols-Meaning.md&1.7&![](graphics/pai04-35.png) \ 
ais05-Prolog-Symbols-Meaning.md&1.7&
ais05-Prolog-Symbols-Meaning.md&1.8&## A more complex example (1)
ais05-Prolog-Symbols-Meaning.md&1.8&
ais05-Prolog-Symbols-Meaning.md&1.8&Read ":-" as "if":
ais05-Prolog-Symbols-Meaning.md&1.8&
ais05-Prolog-Symbols-Meaning.md&1.8&\vskip2ex\hrule
ais05-Prolog-Symbols-Meaning.md&1.8&
ais05-Prolog-Symbols-Meaning.md&1.8&| mother_child(trude, sally).
ais05-Prolog-Symbols-Meaning.md&1.8&| father_child(tom, sally).
ais05-Prolog-Symbols-Meaning.md&1.8&| father_child(tom, erica).
ais05-Prolog-Symbols-Meaning.md&1.8&| father_child(mike, tom).
ais05-Prolog-Symbols-Meaning.md&1.8&| sibling(X, Y) :- parent_child(Z, X), parent_child(Z, Y).
ais05-Prolog-Symbols-Meaning.md&1.8&| parent_child(X, Y) :- father_child(X, Y).
ais05-Prolog-Symbols-Meaning.md&1.8&| parent_child(X, Y) :- mother_child(X, Y).
ais05-Prolog-Symbols-Meaning.md&1.8&
ais05-Prolog-Symbols-Meaning.md&1.8&\vskip2ex\hrule
ais05-Prolog-Symbols-Meaning.md&1.8&
ais05-Prolog-Symbols-Meaning.md&1.8&**Try it out!** What strange thing happens if you ask for siblings?
ais05-Prolog-Symbols-Meaning.md&1.8&
ais05-Prolog-Symbols-Meaning.md&1.9&## A more complex example (2)
ais05-Prolog-Symbols-Meaning.md&1.9&
ais05-Prolog-Symbols-Meaning.md&1.9&>- Problem: Everybody is considered their own sibling! (Logical, but strange).
ais05-Prolog-Symbols-Meaning.md&1.9&>- We need to change the definition of sibling, to require that the two potential siblings are *different* people!
ais05-Prolog-Symbols-Meaning.md&1.9&
ais05-Prolog-Symbols-Meaning.md&1.9&. . .
ais05-Prolog-Symbols-Meaning.md&1.9&
ais05-Prolog-Symbols-Meaning.md&1.9&\vskip2ex\hrule
ais05-Prolog-Symbols-Meaning.md&1.9&
ais05-Prolog-Symbols-Meaning.md&1.9&| mother_child(trude, sally).
ais05-Prolog-Symbols-Meaning.md&1.9&| father_child(tom, sally).
ais05-Prolog-Symbols-Meaning.md&1.9&| father_child(tom, erica).
ais05-Prolog-Symbols-Meaning.md&1.9&| father_child(mike, tom).
ais05-Prolog-Symbols-Meaning.md&1.9&| sibling(X, Y):- parent_child(Z, X), parent_child(Z, Y), **X\\=Y**.
ais05-Prolog-Symbols-Meaning.md&1.9&| parent_child(X, Y) :- father_child(X, Y).
ais05-Prolog-Symbols-Meaning.md&1.9&| parent_child(X, Y) :- mother_child(X, Y).
ais05-Prolog-Symbols-Meaning.md&1.9&
ais05-Prolog-Symbols-Meaning.md&1.9&\vskip2ex\hrule
ais05-Prolog-Symbols-Meaning.md&1.9&
ais05-Prolog-Symbols-Meaning.md&1.10&## Interpretation
ais05-Prolog-Symbols-Meaning.md&1.10&
ais05-Prolog-Symbols-Meaning.md&1.10&| sibling(X, Y) :- parent_child(Z, X), parent_child(Z, Y).
ais05-Prolog-Symbols-Meaning.md&1.10&
ais05-Prolog-Symbols-Meaning.md&1.10&English interpretation:
ais05-Prolog-Symbols-Meaning.md&1.10&
ais05-Prolog-Symbols-Meaning.md&1.10&X is a sibling of Y: if (Z is a parent of X) AND (Z is a parent of Y)
ais05-Prolog-Symbols-Meaning.md&1.10&
ais05-Prolog-Symbols-Meaning.md&1.10&(makes sense).
ais05-Prolog-Symbols-Meaning.md&1.10&
ais05-Prolog-Symbols-Meaning.md&1.11&## More queries
ais05-Prolog-Symbols-Meaning.md&1.11&
ais05-Prolog-Symbols-Meaning.md&1.11&| ? sibling(sally, erica).
ais05-Prolog-Symbols-Meaning.md&1.11&| true.
ais05-Prolog-Symbols-Meaning.md&1.11&| ? father_child(Father, Child).
ais05-Prolog-Symbols-Meaning.md&1.11&| (outputs all father/child pairs)
ais05-Prolog-Symbols-Meaning.md&1.11&
ais05-Prolog-Symbols-Meaning.md&1.12&## Exercise
ais05-Prolog-Symbols-Meaning.md&1.12&
ais05-Prolog-Symbols-Meaning.md&1.12&Look at the code above. In a similar fashion, write a series of facts and rules describing the following relations:
ais05-Prolog-Symbols-Meaning.md&1.12&
ais05-Prolog-Symbols-Meaning.md&1.12&- Kant is a philosopher
ais05-Prolog-Symbols-Meaning.md&1.12&- Plato is a philosopher
ais05-Prolog-Symbols-Meaning.md&1.12&- James likes Kant
ais05-Prolog-Symbols-Meaning.md&1.12&- Mary likes Kant and Plato
ais05-Prolog-Symbols-Meaning.md&1.12&- Peter likes what Mary likes
ais05-Prolog-Symbols-Meaning.md&1.12&
ais05-Prolog-Symbols-Meaning.md&1.12&Then ask the following questions:
ais05-Prolog-Symbols-Meaning.md&1.12&
ais05-Prolog-Symbols-Meaning.md&1.12&- Who likes Kant?
ais05-Prolog-Symbols-Meaning.md&1.12&- Who likes Kant and Plato?
ais05-Prolog-Symbols-Meaning.md&1.12&- Whom does Mary like?
ais05-Prolog-Symbols-Meaning.md&1.12&- Whom does Peter like?
ais05-Prolog-Symbols-Meaning.md&1.12&
ais05-Prolog-Symbols-Meaning.md&1.13&## Solution
ais05-Prolog-Symbols-Meaning.md&1.13&
ais05-Prolog-Symbols-Meaning.md&1.13&| philosopher(kant).
ais05-Prolog-Symbols-Meaning.md&1.13&| philosopher(plato).
ais05-Prolog-Symbols-Meaning.md&1.13&| likes(james, kant).
ais05-Prolog-Symbols-Meaning.md&1.13&| likes(mary, kant).
ais05-Prolog-Symbols-Meaning.md&1.13&| likes(mary, plato).
ais05-Prolog-Symbols-Meaning.md&1.13&| likes(peter,X) :- likes(mary,X).
ais05-Prolog-Symbols-Meaning.md&1.13&
ais05-Prolog-Symbols-Meaning.md&1.13&Test:
ais05-Prolog-Symbols-Meaning.md&1.13&
ais05-Prolog-Symbols-Meaning.md&1.13&| ?- likes(james, plato).  → false
ais05-Prolog-Symbols-Meaning.md&1.13&| ?- likes(peter, X).  →  kant, plato.
ais05-Prolog-Symbols-Meaning.md&1.13&
ais05-Prolog-Symbols-Meaning.md&1.14&## Second example: Classmates
ais05-Prolog-Symbols-Meaning.md&1.14&
ais05-Prolog-Symbols-Meaning.md&1.14&```
ais05-Prolog-Symbols-Meaning.md&1.14&teacher( sandy ).
ais05-Prolog-Symbols-Meaning.md&1.14&teacher( john ).
ais05-Prolog-Symbols-Meaning.md&1.14&student( mary ).
ais05-Prolog-Symbols-Meaning.md&1.14&student( peter ).
ais05-Prolog-Symbols-Meaning.md&1.14&student( nick ).
ais05-Prolog-Symbols-Meaning.md&1.14&student( anna ).
ais05-Prolog-Symbols-Meaning.md&1.14&```
ais05-Prolog-Symbols-Meaning.md&1.14&
ais05-Prolog-Symbols-Meaning.md&1.15&## Simple queries
ais05-Prolog-Symbols-Meaning.md&1.15&
ais05-Prolog-Symbols-Meaning.md&1.15&After entering these "facts" into the Prolog system, we could ask a question in form of a *query:*
ais05-Prolog-Symbols-Meaning.md&1.15&
ais05-Prolog-Symbols-Meaning.md&1.15&```
ais05-Prolog-Symbols-Meaning.md&1.15&teacher( X ).
ais05-Prolog-Symbols-Meaning.md&1.15&```
ais05-Prolog-Symbols-Meaning.md&1.15&
ais05-Prolog-Symbols-Meaning.md&1.15&The Prolog system would then try to match the *variable* X (note that it is a capital letter, which tells Prolog that this is a variable!) with the names given in the collection of facts. It would then give the answers:
ais05-Prolog-Symbols-Meaning.md&1.15&
ais05-Prolog-Symbols-Meaning.md&1.15&```
ais05-Prolog-Symbols-Meaning.md&1.15&X=sandy
ais05-Prolog-Symbols-Meaning.md&1.15&X=john
ais05-Prolog-Symbols-Meaning.md&1.15&```
ais05-Prolog-Symbols-Meaning.md&1.15&
ais05-Prolog-Symbols-Meaning.md&1.16&## Two-place predicates
ais05-Prolog-Symbols-Meaning.md&1.16&
ais05-Prolog-Symbols-Meaning.md&1.16&But we can do more with Prolog. We could, for example, define a two-place teacherOf(teacher, student) predicate, to record who is teaching whom:
ais05-Prolog-Symbols-Meaning.md&1.16&
ais05-Prolog-Symbols-Meaning.md&1.16&```
ais05-Prolog-Symbols-Meaning.md&1.16&teacherOf( john, mary ).
ais05-Prolog-Symbols-Meaning.md&1.16&teacherOf( john, peter ).
ais05-Prolog-Symbols-Meaning.md&1.16&teacherOf( sandy, nick ).
ais05-Prolog-Symbols-Meaning.md&1.16&teacherOf( sandy, anna ).
ais05-Prolog-Symbols-Meaning.md&1.16&```
ais05-Prolog-Symbols-Meaning.md&1.16&
ais05-Prolog-Symbols-Meaning.md&1.16&This works as we would expect: *teacherOf( X, mary )* would return *X=john.*
ais05-Prolog-Symbols-Meaning.md&1.16&
ais05-Prolog-Symbols-Meaning.md&1.17&## More complex predicates
ais05-Prolog-Symbols-Meaning.md&1.17&
ais05-Prolog-Symbols-Meaning.md&1.17&But we can do more with that. We can now define a predicate "classmate" that uses the teacher predicate. The idea is that a classmate is someone with whom you have the same teacher:
ais05-Prolog-Symbols-Meaning.md&1.17&
ais05-Prolog-Symbols-Meaning.md&1.17&```
ais05-Prolog-Symbols-Meaning.md&1.17&classmate( X, Y ) :- student(X), student(Y),
ais05-Prolog-Symbols-Meaning.md&1.17&   teacherOf( A, X ), teacherOf( A, Y ).
ais05-Prolog-Symbols-Meaning.md&1.17&```
ais05-Prolog-Symbols-Meaning.md&1.17&
ais05-Prolog-Symbols-Meaning.md&1.17&The ":-" means that the expression on the left will be true if the expressions on the right are true. You can just read it as "if". The commas on the right side (",") express a logical "and". That means that *all* the expressions on the right have to be true in order for the system to consider the predicate *classmate* to be true.
ais05-Prolog-Symbols-Meaning.md&1.17&
ais05-Prolog-Symbols-Meaning.md&1.18&## More queries
ais05-Prolog-Symbols-Meaning.md&1.18&
ais05-Prolog-Symbols-Meaning.md&1.18&Now we can ask:
ais05-Prolog-Symbols-Meaning.md&1.18&
ais05-Prolog-Symbols-Meaning.md&1.18&```
ais05-Prolog-Symbols-Meaning.md&1.18&classmate( X, peter ).
ais05-Prolog-Symbols-Meaning.md&1.18&```
ais05-Prolog-Symbols-Meaning.md&1.18&
ais05-Prolog-Symbols-Meaning.md&1.18&This means: Who (X) is classmate of Peter? The system answers:
ais05-Prolog-Symbols-Meaning.md&1.18&
ais05-Prolog-Symbols-Meaning.md&1.18&```
ais05-Prolog-Symbols-Meaning.md&1.18&X=mary
ais05-Prolog-Symbols-Meaning.md&1.18&X=peter
ais05-Prolog-Symbols-Meaning.md&1.18&```
ais05-Prolog-Symbols-Meaning.md&1.18&
ais05-Prolog-Symbols-Meaning.md&1.19&## A problem
ais05-Prolog-Symbols-Meaning.md&1.19&
ais05-Prolog-Symbols-Meaning.md&1.19&Oops. Something strange happens here. What exactly? Well, if you look at the predicate classmate, it never says that one cannot be *one's own* classmate! Since everyone has the same teacher as oneself, logically, everyone who is a student is always also their own classmate!
ais05-Prolog-Symbols-Meaning.md&1.19&
ais05-Prolog-Symbols-Meaning.md&1.20&## The “not equal” operator
ais05-Prolog-Symbols-Meaning.md&1.20&
ais05-Prolog-Symbols-Meaning.md&1.20&If we wanted to change that, we would have to exclude the case that someone is one's own classmate, like this:
ais05-Prolog-Symbols-Meaning.md&1.20&
ais05-Prolog-Symbols-Meaning.md&1.20&```
ais05-Prolog-Symbols-Meaning.md&1.20&classmate( X, Y ) :- student(X), student(Y),
ais05-Prolog-Symbols-Meaning.md&1.20&   teacherOf( A, X ), teacherOf( A, Y ),
ais05-Prolog-Symbols-Meaning.md&1.20&   X\=Y.
ais05-Prolog-Symbols-Meaning.md&1.20&```
ais05-Prolog-Symbols-Meaning.md&1.20&
ais05-Prolog-Symbols-Meaning.md&1.20&The operator "\=" in Prolog means "not equal," so that in this case it is additionally required that X is different from Y. Let's see:
ais05-Prolog-Symbols-Meaning.md&1.20&
ais05-Prolog-Symbols-Meaning.md&1.20&```
ais05-Prolog-Symbols-Meaning.md&1.20&classmate( X, peter ).
ais05-Prolog-Symbols-Meaning.md&1.20&```
ais05-Prolog-Symbols-Meaning.md&1.20&
ais05-Prolog-Symbols-Meaning.md&1.20&Answer: *X=mary,* which is the expected answer.
ais05-Prolog-Symbols-Meaning.md&1.20&
ais05-Prolog-Symbols-Meaning.md&1.21&## Another query
ais05-Prolog-Symbols-Meaning.md&1.21&
ais05-Prolog-Symbols-Meaning.md&1.21&We can also try:
ais05-Prolog-Symbols-Meaning.md&1.21&
ais05-Prolog-Symbols-Meaning.md&1.21&```
ais05-Prolog-Symbols-Meaning.md&1.21&classmate( X, sandy ).
ais05-Prolog-Symbols-Meaning.md&1.21&```
ais05-Prolog-Symbols-Meaning.md&1.21&
ais05-Prolog-Symbols-Meaning.md&1.21&which correctly answers *false,* meaning that no solution can be found, since Sandy is not a student but a teacher, and we explicitly limited the *classmate* predicate to students.
ais05-Prolog-Symbols-Meaning.md&1.21&
ais05-Prolog-Symbols-Meaning.md&1.21&
ais05-Prolog-Symbols-Meaning.md&1.22&## Definitions and symbolic processing
ais05-Prolog-Symbols-Meaning.md&1.22&
ais05-Prolog-Symbols-Meaning.md&1.22&This way of symbolic processing is similar to *defining* things.
ais05-Prolog-Symbols-Meaning.md&1.22&
ais05-Prolog-Symbols-Meaning.md&1.22&A definition gives the genus (family) of something and a specific difference. (Aristotle)
ais05-Prolog-Symbols-Meaning.md&1.22&
ais05-Prolog-Symbols-Meaning.md&1.22&. . . 
ais05-Prolog-Symbols-Meaning.md&1.22&
ais05-Prolog-Symbols-Meaning.md&1.22&"An elephant **is a** mammal that **has** a long trunk, big ears and is very big and grey."
ais05-Prolog-Symbols-Meaning.md&1.22&
ais05-Prolog-Symbols-Meaning.md&1.22&. . .
ais05-Prolog-Symbols-Meaning.md&1.22&
ais05-Prolog-Symbols-Meaning.md&1.22&- "isA" relation: genus (family)
ais05-Prolog-Symbols-Meaning.md&1.22&- "has" relation: difference
ais05-Prolog-Symbols-Meaning.md&1.22&
ais05-Prolog-Symbols-Meaning.md&1.23&## Prolog version
ais05-Prolog-Symbols-Meaning.md&1.23&
ais05-Prolog-Symbols-Meaning.md&1.23&"An elephant **is a** mammal that **has** a long trunk, big ears and is very big and grey."
ais05-Prolog-Symbols-Meaning.md&1.23&
ais05-Prolog-Symbols-Meaning.md&1.23&| isA(elephant, mammal).
ais05-Prolog-Symbols-Meaning.md&1.23&| has(elephant, trunk).
ais05-Prolog-Symbols-Meaning.md&1.23&| has(elephant, big_ears).
ais05-Prolog-Symbols-Meaning.md&1.23&| has(elephant, big_size).
ais05-Prolog-Symbols-Meaning.md&1.23&| has(elephant, colour_grey).
ais05-Prolog-Symbols-Meaning.md&1.23&
ais05-Prolog-Symbols-Meaning.md&1.23&. . . 
ais05-Prolog-Symbols-Meaning.md&1.23&
ais05-Prolog-Symbols-Meaning.md&1.23&Query: **has( X, big_size ).**
ais05-Prolog-Symbols-Meaning.md&1.23&
ais05-Prolog-Symbols-Meaning.md&1.24&## isA and has
ais05-Prolog-Symbols-Meaning.md&1.24&
ais05-Prolog-Symbols-Meaning.md&1.24&We can go a long way in representing facts symbolically with these two relations:
ais05-Prolog-Symbols-Meaning.md&1.24&
ais05-Prolog-Symbols-Meaning.md&1.24&- isA() for establishing the Aristotelian genus of something.
ais05-Prolog-Symbols-Meaning.md&1.24&- has() for enumerating its properties (differences).
ais05-Prolog-Symbols-Meaning.md&1.24&
ais05-Prolog-Symbols-Meaning.md&1.24&What results is sometimes called an “ontology” (in the computing sense!)
ais05-Prolog-Symbols-Meaning.md&1.24&
ais05-Prolog-Symbols-Meaning.md&1.25&## Limits of Aristotelian logic in real life
ais05-Prolog-Symbols-Meaning.md&1.25&
ais05-Prolog-Symbols-Meaning.md&1.25&- “If it’s winter, then the heating is on.”
ais05-Prolog-Symbols-Meaning.md&1.25&- All birds fly.
ais05-Prolog-Symbols-Meaning.md&1.25&
ais05-Prolog-Symbols-Meaning.md&1.25&Are these always true?
ais05-Prolog-Symbols-Meaning.md&1.25&
ais05-Prolog-Symbols-Meaning.md&1.25&. . .
ais05-Prolog-Symbols-Meaning.md&1.25&
ais05-Prolog-Symbols-Meaning.md&1.25&No. There might be many reasons for the heating to be actually off:
ais05-Prolog-Symbols-Meaning.md&1.25&
ais05-Prolog-Symbols-Meaning.md&1.25&>- Lack of money to pay the bill,
ais05-Prolog-Symbols-Meaning.md&1.25&>- No fuel,
ais05-Prolog-Symbols-Meaning.md&1.25&>- The heater is broken,
ais05-Prolog-Symbols-Meaning.md&1.25&>- The occupant has gone out,
ais05-Prolog-Symbols-Meaning.md&1.25&>- It's a warm winter's day and no heating is needed.
ais05-Prolog-Symbols-Meaning.md&1.25&
ais05-Prolog-Symbols-Meaning.md&1.25&. . . 
ais05-Prolog-Symbols-Meaning.md&1.25&
ais05-Prolog-Symbols-Meaning.md&1.25&>- “All birds fly,” but chickens fly very badly, and penguins don’t fly at all.
ais05-Prolog-Symbols-Meaning.md&1.25&
ais05-Prolog-Symbols-Meaning.md&1.25&
ais05-Prolog-Symbols-Meaning.md&1.26&## Tweety
ais05-Prolog-Symbols-Meaning.md&1.26&
ais05-Prolog-Symbols-Meaning.md&1.26&Consider the statements:
ais05-Prolog-Symbols-Meaning.md&1.26&
ais05-Prolog-Symbols-Meaning.md&1.26&>- Tweety is a bird.
ais05-Prolog-Symbols-Meaning.md&1.26&>- All birds fly.
ais05-Prolog-Symbols-Meaning.md&1.26&>- If something is a penguin, then it is a bird.
ais05-Prolog-Symbols-Meaning.md&1.26&>- Tweety is a penguin.
ais05-Prolog-Symbols-Meaning.md&1.26&>- Penguins don't fly.
ais05-Prolog-Symbols-Meaning.md&1.26&>- **Does Tweety fly?**
ais05-Prolog-Symbols-Meaning.md&1.26&
ais05-Prolog-Symbols-Meaning.md&1.26&>- No.
ais05-Prolog-Symbols-Meaning.md&1.26&
ais05-Prolog-Symbols-Meaning.md&1.27&## Tweety
ais05-Prolog-Symbols-Meaning.md&1.27&
ais05-Prolog-Symbols-Meaning.md&1.27&- Tweety is a bird.
ais05-Prolog-Symbols-Meaning.md&1.27&- **All birds fly.**
ais05-Prolog-Symbols-Meaning.md&1.27&- **If something is a penguin, then it is a bird.**
ais05-Prolog-Symbols-Meaning.md&1.27&- Tweety is a penguin.
ais05-Prolog-Symbols-Meaning.md&1.27&- **Penguins don't fly.**
ais05-Prolog-Symbols-Meaning.md&1.27&- *Does Tweety fly?*
ais05-Prolog-Symbols-Meaning.md&1.27&
ais05-Prolog-Symbols-Meaning.md&1.27&- No.
ais05-Prolog-Symbols-Meaning.md&1.27&- The list of statements above contains **contradictions** in normal deductive logic!
ais05-Prolog-Symbols-Meaning.md&1.27&
ais05-Prolog-Symbols-Meaning.md&1.28&## "Defeasibly follows"
ais05-Prolog-Symbols-Meaning.md&1.28&
ais05-Prolog-Symbols-Meaning.md&1.28&In these cases, we want to express that some statements are only true “most of the time,” or “usually,” but not strictly *always.*
ais05-Prolog-Symbols-Meaning.md&1.28&
ais05-Prolog-Symbols-Meaning.md&1.28&*Defeasible* rules are "reasons to believe." They  are general rules that will be true most of the time, but might also be false at particular moments.
ais05-Prolog-Symbols-Meaning.md&1.28&
ais05-Prolog-Symbols-Meaning.md&1.28&. . . 
ais05-Prolog-Symbols-Meaning.md&1.28&
ais05-Prolog-Symbols-Meaning.md&1.28&Textbook: <https://medium.com/@MoralRobots/aristotle-and-the-penguins-a78e80b4783a>
ais05-Prolog-Symbols-Meaning.md&1.28&
ais05-Prolog-Symbols-Meaning.md&1.29&## "Defeasibly follows"
ais05-Prolog-Symbols-Meaning.md&1.29&
ais05-Prolog-Symbols-Meaning.md&1.29&>- winter -> heating_on ; or:
ais05-Prolog-Symbols-Meaning.md&1.29&>- heating_on <- winter (in Prolog order)
ais05-Prolog-Symbols-Meaning.md&1.29&
ais05-Prolog-Symbols-Meaning.md&1.29&. . .
ais05-Prolog-Symbols-Meaning.md&1.29&
ais05-Prolog-Symbols-Meaning.md&1.29&... mean: "If it is winter, then the heating is on."
ais05-Prolog-Symbols-Meaning.md&1.29&
ais05-Prolog-Symbols-Meaning.md&1.29&. . . 
ais05-Prolog-Symbols-Meaning.md&1.29&
ais05-Prolog-Symbols-Meaning.md&1.29&Compare:
ais05-Prolog-Symbols-Meaning.md&1.29&
ais05-Prolog-Symbols-Meaning.md&1.29&>- winter $\leadsto$ heating_on ; or:
ais05-Prolog-Symbols-Meaning.md&1.29&>- heating_on **-<** winter (in Prolog order)
ais05-Prolog-Symbols-Meaning.md&1.29&
ais05-Prolog-Symbols-Meaning.md&1.29&. . . 
ais05-Prolog-Symbols-Meaning.md&1.29&
ais05-Prolog-Symbols-Meaning.md&1.29&... mean:
ais05-Prolog-Symbols-Meaning.md&1.29&
ais05-Prolog-Symbols-Meaning.md&1.29&>- "If it is winter, then the heating will (usually) be on" ; or:
ais05-Prolog-Symbols-Meaning.md&1.29&>- "If it is winter, there is *reason to believe that* the heating will be on."
ais05-Prolog-Symbols-Meaning.md&1.29&
ais05-Prolog-Symbols-Meaning.md&1.29&
ais05-Prolog-Symbols-Meaning.md&1.30&## Defeasible (D:) assumptions
ais05-Prolog-Symbols-Meaning.md&1.30&
ais05-Prolog-Symbols-Meaning.md&1.30&- Tweety is a bird.
ais05-Prolog-Symbols-Meaning.md&1.30&- *D: All birds fly.*
ais05-Prolog-Symbols-Meaning.md&1.30&- If something is a penguin, then it is a bird.
ais05-Prolog-Symbols-Meaning.md&1.30&- Tweety is a penguin.
ais05-Prolog-Symbols-Meaning.md&1.30&- *D: Penguins don't fly.*
ais05-Prolog-Symbols-Meaning.md&1.30&- Does Tweety fly?
ais05-Prolog-Symbols-Meaning.md&1.30&
ais05-Prolog-Symbols-Meaning.md&1.30&D-Prolog <http://tweetyproject.org/w/delp/>:
ais05-Prolog-Symbols-Meaning.md&1.30&
ais05-Prolog-Symbols-Meaning.md&1.30&| Bird(tweety).
ais05-Prolog-Symbols-Meaning.md&1.30&| Fly(X) **-<** Bird(X).
ais05-Prolog-Symbols-Meaning.md&1.30&| Bird(X) <- Penguin(X).
ais05-Prolog-Symbols-Meaning.md&1.30&| Penguin(tweety).
ais05-Prolog-Symbols-Meaning.md&1.30&| ~Fly(X) **-<** Penguin(X).
ais05-Prolog-Symbols-Meaning.md&1.30&|
ais05-Prolog-Symbols-Meaning.md&1.30&| ?- Fly(tweety) (Correct answer: false)
ais05-Prolog-Symbols-Meaning.md&1.30&
ais05-Prolog-Symbols-Meaning.md&1.30&
ais05-Prolog-Symbols-Meaning.md&1.31&## Defeasible logic
ais05-Prolog-Symbols-Meaning.md&1.31&
ais05-Prolog-Symbols-Meaning.md&1.31&>- Defeasible logic (proposed by Donald Nute) is used to formalize defeasible reasoning.
ais05-Prolog-Symbols-Meaning.md&1.31&>- Three different types of propositions:
ais05-Prolog-Symbols-Meaning.md&1.31&>     - Strict rules: a fact is always a consequence of another;
ais05-Prolog-Symbols-Meaning.md&1.31&>     - Defeasible rules: a fact is typically a consequence of another;
ais05-Prolog-Symbols-Meaning.md&1.31&>     - Undercutting defeaters: specify exceptions to defeasible rules.
ais05-Prolog-Symbols-Meaning.md&1.31&>- During the process of deduction, the strict rules are always applied, while a defeasible rule can be applied only if no defeater of a higher priority specifies that it should not. (Wikipedia)
ais05-Prolog-Symbols-Meaning.md&1.31&
ais05-Prolog-Symbols-Meaning.md&1.31&
ais05-Prolog-Symbols-Meaning.md&1.32&## Why do we need ‘defeasible’ logic? (1)
ais05-Prolog-Symbols-Meaning.md&1.32&
ais05-Prolog-Symbols-Meaning.md&1.32&>- It would be best if we could have a probabilistic logic, where every statement has an attached probability. In this case, we would do deductions by calculating the probability of the conclusion given the probabilities of the premises.
ais05-Prolog-Symbols-Meaning.md&1.32&>- But this is impossible for various reasons:
ais05-Prolog-Symbols-Meaning.md&1.32&>     - In many cases, we don’t know the correct probabilities (how many birds are penguins? How many species of bird don’t fly?)
ais05-Prolog-Symbols-Meaning.md&1.32&>     - Humans are bad at estimating probabilities for everyday events.
ais05-Prolog-Symbols-Meaning.md&1.32&
ais05-Prolog-Symbols-Meaning.md&1.33&## Why do we need ‘defeasible’ logic? (2)
ais05-Prolog-Symbols-Meaning.md&1.33&
ais05-Prolog-Symbols-Meaning.md&1.33&>- Therefore, the concept of a ‘defeasible’ conclusion is a kind of intermediate, stop-gap solution.
ais05-Prolog-Symbols-Meaning.md&1.33&>- Instead of precisely calculating probabilities (which would be impossible in real life), or completely ignoring them and creating contradictory sets of premises, we take a middle road.
ais05-Prolog-Symbols-Meaning.md&1.33&>- A defeasible assumption is *generally* true, but not *always* true. The machine can treat it as an assumption, but if there are good reasons to assume that it’s false, then the machine can assume that it’s false without creating a contradiction.
ais05-Prolog-Symbols-Meaning.md&1.33&>- This gives us the advantages of both extremes (binary logic and probabilities), without their respective drawbacks.
ais05-Prolog-Symbols-Meaning.md&1.33&	
ais05-Prolog-Symbols-Meaning.md&1.33&
ais05-Prolog-Symbols-Meaning.md&1.34&## The End. Thank you for your attention!
ais05-Prolog-Symbols-Meaning.md&1.34&
ais05-Prolog-Symbols-Meaning.md&1.34&Questions?
ais05-Prolog-Symbols-Meaning.md&1.34&
ais05-Prolog-Symbols-Meaning.md&1.34&<!--
ais05-Prolog-Symbols-Meaning.md&1.34&
ais05-Prolog-Symbols-Meaning.md&1.34&%% Local Variables: ***
ais05-Prolog-Symbols-Meaning.md&1.34&%% mode: markdown ***
ais05-Prolog-Symbols-Meaning.md&1.34&%% mode: visual-line ***
ais05-Prolog-Symbols-Meaning.md&1.34&%% mode: cua ***
ais05-Prolog-Symbols-Meaning.md&1.34&%% mode: flyspell ***
ais05-Prolog-Symbols-Meaning.md&1.34&%% End: ***
ais05-Prolog-Symbols-Meaning.md&1.34&
ais05-Prolog-Symbols-Meaning.md&1.34&-->
ais06-Expert-Systems.md&0.1&---
ais06-Expert-Systems.md&0.1&title:  "AI and Society: 6. Expert systems and their critics"
ais06-Expert-Systems.md&0.1&author: Andreas Matthias, Lingnan University
ais06-Expert-Systems.md&0.1&date: September 2, 2019
ais06-Expert-Systems.md&0.1&...
ais06-Expert-Systems.md&0.1&
ais06-Expert-Systems.md&1.0&# Symbolic AI
ais06-Expert-Systems.md&1.0&
ais06-Expert-Systems.md&1.1&## Symbolic AI (1)
ais06-Expert-Systems.md&1.1&
ais06-Expert-Systems.md&1.1&>- The idea of symbolic AI is to represent reality as symbols inside the computer.
ais06-Expert-Systems.md&1.1&>- For example, in Prolog we have something like: “cat(tom)”.
ais06-Expert-Systems.md&1.1&>     - Here, the concept of “cat” is represented by the predicate “cat()”, which, in turn, is just a sequence of three letters, c-a-t. 
ais06-Expert-Systems.md&1.1&>     - These three letters “cat” don’t have any meaning inside the machine. Although they represent the concept of cats, they don’t *mean* anything to the computer.
ais06-Expert-Systems.md&1.1&>     - The same with the symbol “tom”, which for the computer is just a meaningless string of three letters, t-o-m.
ais06-Expert-Systems.md&1.1&
ais06-Expert-Systems.md&1.2&## Symbolic AI (2)
ais06-Expert-Systems.md&1.2&
ais06-Expert-Systems.md&1.2&>- Hubert Dreyfus, philosopher and AI critic, describes the basic idea of symbolic AI as: “The mind can be viewed as a device operating on bits of information according to formal rules.”
ais06-Expert-Systems.md&1.2&>- We could also say: “The purely syntactical manipulation of symbols according to rules is sufficient for intelligent behaviour” (avoiding talk of “mind”).
ais06-Expert-Systems.md&1.2&>- Or, as we said in the last session: “If the symbol manipulation preserves the original relations between the symbols, the mapping of symbol to meaning can be left to mind of the operator.”
ais06-Expert-Systems.md&1.2&
ais06-Expert-Systems.md&1.3&## The Physical Symbol System Hypothesis
ais06-Expert-Systems.md&1.3&
ais06-Expert-Systems.md&1.3&>- These different ways of expressing the characteristics of symbolic AI systems have been summarised in a famous statement that is called the *Physical Symbol System Hypothesis:*
ais06-Expert-Systems.md&1.3&>- “A physical symbol system has the necessary and sufficient means for general intelligent action.” (Allen Newell and Herbert A. Simon, 1976)
ais06-Expert-Systems.md&1.3&>- This means that we don’t need real “understanding” or “meaning” inside a computer. Just manipulating symbols in the right ways is both necessary and sufficient for intelligent action (note: not for “mind”!)
ais06-Expert-Systems.md&1.3&
ais06-Expert-Systems.md&2.0&# Problems of symbolic AI
ais06-Expert-Systems.md&2.0&
ais06-Expert-Systems.md&2.1&## Problems of symbolic AI
ais06-Expert-Systems.md&2.1&
ais06-Expert-Systems.md&2.1&Symbolic AI has multiple problems. A few are:
ais06-Expert-Systems.md&2.1&
ais06-Expert-Systems.md&2.1&>- The problem of noise:
ais06-Expert-Systems.md&2.1&>     - In reality, objects are never entirely clear.
ais06-Expert-Systems.md&2.1&>     - Objects in pictures can have a background that is distracting and obscuring the object.
ais06-Expert-Systems.md&2.1&>     - Voice input is obscured by noisy environments.
ais06-Expert-Systems.md&2.1&>     - Ordinary human language has all sorts of, ummm, you know, noisy features that don’t contribute too much to the actual, real, intended meaning, innit mate?
ais06-Expert-Systems.md&2.1&>- The symbol grounding problem: Can a system ever really understand anything if its symbols don’t correspond to anything real, that is, if they are meaningless to the system? If not, how can we make the system understand the meaning of its symbols?
ais06-Expert-Systems.md&2.1&>- The Frame Problem (see below).
ais06-Expert-Systems.md&2.1&
ais06-Expert-Systems.md&2.2&## Frame problem
ais06-Expert-Systems.md&2.2&
ais06-Expert-Systems.md&2.2&| paint(X,C) $\to$ colour(X,C).
ais06-Expert-Systems.md&2.2&| move(X,P) $\to$ position(X,P).
ais06-Expert-Systems.md&2.2&| colour(duck, red).
ais06-Expert-Systems.md&2.2&| position(duck, house).
ais06-Expert-Systems.md&2.2&| paint(duck, blue).
ais06-Expert-Systems.md&2.2&| move(duck, garden).
ais06-Expert-Systems.md&2.2&
ais06-Expert-Systems.md&2.2&. . . 
ais06-Expert-Systems.md&2.2&
ais06-Expert-Systems.md&2.2&*What is the state of the world now?*
ais06-Expert-Systems.md&2.2&
ais06-Expert-Systems.md&2.2&- Colour of duck?
ais06-Expert-Systems.md&2.2&- Position of duck?
ais06-Expert-Systems.md&2.2&
ais06-Expert-Systems.md&2.3&## Frame problem
ais06-Expert-Systems.md&2.3&
ais06-Expert-Systems.md&2.3&| paint(X,C) $\to$ colour(X,C).
ais06-Expert-Systems.md&2.3&| move(X,P) $\to$ position(X,P).
ais06-Expert-Systems.md&2.3&| colour(duck, red).
ais06-Expert-Systems.md&2.3&| position(duck, house).
ais06-Expert-Systems.md&2.3&| paint(duck, blue).
ais06-Expert-Systems.md&2.3&| move(duck, garden).
ais06-Expert-Systems.md&2.3&
ais06-Expert-Systems.md&2.3&>- Is it true that the duck is now blue and standing in the garden?
ais06-Expert-Systems.md&2.3&>- *Does this logically follow from the premises?*
ais06-Expert-Systems.md&2.3&>- No! It can not be ruled out that *the colour gets changed by the move* action!
ais06-Expert-Systems.md&2.3&>- (For example by moving the duck into a pot of paint!)
ais06-Expert-Systems.md&2.3&>- Thus we can only conclude: position(duck, garden)!
ais06-Expert-Systems.md&2.3&
ais06-Expert-Systems.md&2.4&## Frame problem
ais06-Expert-Systems.md&2.4&
ais06-Expert-Systems.md&2.4&>- If I *paint* a thing in my kitchen, which properties of the world will be affected?
ais06-Expert-Systems.md&2.4&>- The colour of that thing.
ais06-Expert-Systems.md&2.4&>- The colour of the brush I used.
ais06-Expert-Systems.md&2.4&>- The weight of the colour pot (because I used some colour up).
ais06-Expert-Systems.md&2.4&>- The weight of the painted thing.
ais06-Expert-Systems.md&2.4&>- The smell of the painted thing.
ais06-Expert-Systems.md&2.4&>- If I *explode a bomb* in my kitchen, which properties of the world will be affected?
ais06-Expert-Systems.md&2.4&>- Lots, depending on the size and type of the bomb!
ais06-Expert-Systems.md&2.4&
ais06-Expert-Systems.md&2.5&## Frame problem
ais06-Expert-Systems.md&2.5&
ais06-Expert-Systems.md&2.5&More general question:
ais06-Expert-Systems.md&2.5&
ais06-Expert-Systems.md&2.5&- How do I know where to stop looking for consequences of my actions?
ais06-Expert-Systems.md&2.5&- How can we tell the machine which actions will affect which properties of the environment?
ais06-Expert-Systems.md&2.5&
ais06-Expert-Systems.md&2.5&. . . 
ais06-Expert-Systems.md&2.5&
ais06-Expert-Systems.md&2.5&Specifically, in symbolic AI:
ais06-Expert-Systems.md&2.5&
ais06-Expert-Systems.md&2.5&- How can I describe the world with symbolic facts and rules without having to describe the (almost infinite) set of states which don't change with every single action?
ais06-Expert-Systems.md&2.5&- How can I distinguish what has changed after an action from what has remained the same?
ais06-Expert-Systems.md&2.5&
ais06-Expert-Systems.md&2.6&## The "frame problem"
ais06-Expert-Systems.md&2.6&
ais06-Expert-Systems.md&2.6&“To most AI researchers, the frame problem is the challenge of representing the effects of action in logic without having to represent explicitly a large number of intuitively obvious non-effects. To many philosophers, the AI researchers' frame problem is suggestive of a wider epistemological issue, namely whether it is possible, in principle, to limit the scope of the reasoning required to derive the consequences of an action.”
ais06-Expert-Systems.md&2.6&
ais06-Expert-Systems.md&2.6&(Stanford Encyclopedia of Philosophy)
ais06-Expert-Systems.md&2.6&
ais06-Expert-Systems.md&2.6&
ais06-Expert-Systems.md&3.0&# Expert systems
ais06-Expert-Systems.md&3.0&
ais06-Expert-Systems.md&3.1&## Expert systems
ais06-Expert-Systems.md&3.1&
ais06-Expert-Systems.md&3.1&Expert systems are some of the most prominent examples of AI.
ais06-Expert-Systems.md&3.1&
ais06-Expert-Systems.md&3.1&![](graphics/03-expert-system.png)
ais06-Expert-Systems.md&3.1&
ais06-Expert-Systems.md&3.2&## Expert system rule tree
ais06-Expert-Systems.md&3.2&
ais06-Expert-Systems.md&3.2&Diagnosis of TV faults:
ais06-Expert-Systems.md&3.2&
ais06-Expert-Systems.md&3.2&![](graphics/03-expert-system-rules.jpg)
ais06-Expert-Systems.md&3.2&
ais06-Expert-Systems.md&3.3&## Expert system architecture
ais06-Expert-Systems.md&3.3&
ais06-Expert-Systems.md&3.3&>- Knowledge base (KB): a representation of the knowledge to be encoded, often as IF/THEN rules.^[Source: amzi.com: Expert Systems in Prolog.]
ais06-Expert-Systems.md&3.3&>- Inference engine: the code at the core of the system that derives recommendations from the knowledge base and the problem-specific data entered by the user.
ais06-Expert-Systems.md&3.3&>- User interface: The code that controls the dialog between the user and the system.
ais06-Expert-Systems.md&3.3&
ais06-Expert-Systems.md&3.4&## Expert system individual roles
ais06-Expert-Systems.md&3.4&
ais06-Expert-Systems.md&3.4&>- Domain expert: The person who is currently expert in solving the problems that the system is intended to solve.
ais06-Expert-Systems.md&3.4&>- Knowledge engineer: the person who extracts the expert’s knowledge (perhaps in a dialogue with the expert) and who encodes it in form that can be used by the expert system, thus creating the knowledge base.
ais06-Expert-Systems.md&3.4&>- System engineer: the person who builds the user interface, inference engine, and other program parts that make up the expert system (except for the expert knowledge itself).
ais06-Expert-Systems.md&3.4&>- User: The individual who will use the expert system as a replacement for the human expert.
ais06-Expert-Systems.md&3.4&
ais06-Expert-Systems.md&3.4&
ais06-Expert-Systems.md&4.0&# Hubert Dreyfus’ criticism of expert systems
ais06-Expert-Systems.md&4.0&
ais06-Expert-Systems.md&4.1&## (Questionable) assumptions behind expert systems
ais06-Expert-Systems.md&4.1&
ais06-Expert-Systems.md&4.1&>- Hubert Dreyfus: Heidegger expert and AI critic.
ais06-Expert-Systems.md&4.1&>- Reading: "From Socrates to Expert Systems."
ais06-Expert-Systems.md&4.1&>- Experts follow clear, black and white rules.
ais06-Expert-Systems.md&4.1&>- What makes the expert an expert is that he knows rules that other people don't.
ais06-Expert-Systems.md&4.1&>- You can extract the expertise in form of symbolic rules and put it into a machine.
ais06-Expert-Systems.md&4.1&>- Everyone who has these rules to follow, will be an expert.
ais06-Expert-Systems.md&4.1&
ais06-Expert-Systems.md&4.1&. . .
ais06-Expert-Systems.md&4.1&
ais06-Expert-Systems.md&4.1&*Can you describe what is wrong about these assumptions?*
ais06-Expert-Systems.md&4.1&
ais06-Expert-Systems.md&4.2&## How does an "expert" work?
ais06-Expert-Systems.md&4.2&
ais06-Expert-Systems.md&4.2&>- What is an "expert system"? Does it really model what a human expert does?
ais06-Expert-Systems.md&4.2&>- *Are* you *experts in anything?*
ais06-Expert-Systems.md&4.2&    - Possibly: Piano, violin, computer games, Chinese calligraphy
ais06-Expert-Systems.md&4.2&	- All of you: Writing, reading, taking lecture notes, Chinese, walking, jumping over obstacles
ais06-Expert-Systems.md&4.2&>- *How did you become an expert?*
ais06-Expert-Systems.md&4.2&
ais06-Expert-Systems.md&4.3&## Dreyfus: From Socrates to Expert Systems
ais06-Expert-Systems.md&4.3&
ais06-Expert-Systems.md&4.3&>- There are stages of development of expertise.
ais06-Expert-Systems.md&4.3&>- If we look at how human expertise develops, we will see that machine expert systems got the process all wrong!
ais06-Expert-Systems.md&4.3&
ais06-Expert-Systems.md&4.3&
ais06-Expert-Systems.md&4.4&## Novice stage: Recognising features
ais06-Expert-Systems.md&4.4&
ais06-Expert-Systems.md&4.4&>- The student automobile driver learns to recognize such interpretation-free features as speed (indicated by the speedometer).
ais06-Expert-Systems.md&4.4&>- He is given rules such as shift to second gear when the speedometer needle points to ten miles an hour.
ais06-Expert-Systems.md&4.4&>- The novice chess player learns a numerical value for each type of piece regardless of its position, and the rule: "Always exchange if the total value of pieces captured exceeds the value of pieces lost."
ais06-Expert-Systems.md&4.4&>- The player also learns to seek center control when no advantageous exchanges can be found, and is given a rule defining center squares and a rule for calculating extent of control.
ais06-Expert-Systems.md&4.4&>- Most beginners are notoriously slow players, as they attempt to remember all these features and rules.
ais06-Expert-Systems.md&4.4&
ais06-Expert-Systems.md&4.5&## Advanced beginner stage: Situational aspects and features (1)
ais06-Expert-Systems.md&4.5&
ais06-Expert-Systems.md&4.5&>- As the novice gains experience actually coping with real situations, he begins to note, or an instructor points out, examples of meaningful additional *aspects of the situation.*
ais06-Expert-Systems.md&4.5&>- After seeing a sufficient number of examples, the student learns to recognise these new aspects.
ais06-Expert-Systems.md&4.5&>- Instructions now can refer to these new *situational* aspects, as well as to the objectively defined *nonsituational* features recognisable by the inexperienced novice.
ais06-Expert-Systems.md&4.5&
ais06-Expert-Systems.md&4.6&## Advanced beginner stage: Recognising aspects and features (2)
ais06-Expert-Systems.md&4.6&
ais06-Expert-Systems.md&4.6&>- The advanced beginner driver, using (situational) engine sounds as well as (non-situational) speed in his gear-shifting rules, learns the maxim: Shift up when the motor sounds like it is racing and down when it sounds like its straining.
ais06-Expert-Systems.md&4.6&>- He learns to observe the demeanor as well as position and velocity of pedestrians or other drivers.
ais06-Expert-Systems.md&4.6&>- He can, for example, distinguish the behavior of a distracted or drunken driver from that of an impatient but alert one.
ais06-Expert-Systems.md&4.6&>- Engine sounds and behaviour styles cannot be adequately captured by words, so words cannot take the place of a few choice examples in learning such distinctions.
ais06-Expert-Systems.md&4.6&
ais06-Expert-Systems.md&4.7&## Advanced beginner stage: Recognising aspects and features (3)
ais06-Expert-Systems.md&4.7&
ais06-Expert-Systems.md&4.7&>- With experience, the chess beginner learns to recognize such situational aspects of positions as a weakened king's side or a strong pawn structure despite the lack of a precise and situation-free definition.
ais06-Expert-Systems.md&4.7&>- The player can then follow maxims such as: Attack a weakened king’s side.
ais06-Expert-Systems.md&4.7&
ais06-Expert-Systems.md&4.8&## Competence stage: Important vs. ignored elements (1)
ais06-Expert-Systems.md&4.8&
ais06-Expert-Systems.md&4.8&>- With more experience, the number of potentially relevant elements that the learner is able to recognise becomes overwhelming.
ais06-Expert-Systems.md&4.8&>- At this point the student may well wonder how anyone ever masters the skill.
ais06-Expert-Systems.md&4.8&>- To cope with this overload and to achieve competence, people learn, through instruction or experience, to devise a plan or choose a perspective.
ais06-Expert-Systems.md&4.8&>- The perspective then determines which elements of the situation should be treated as important and which ones can be ignored.
ais06-Expert-Systems.md&4.8&>- By restricting attention to only a few of the vast number of possibly relevant features and aspects, such a choice of a perspective makes decision making easier.
ais06-Expert-Systems.md&4.8&
ais06-Expert-Systems.md&4.9&## Competence stage: Important vs. ignored elements (2)
ais06-Expert-Systems.md&4.9&
ais06-Expert-Systems.md&4.9&>- The competent performer thus seeks new rules and reasoning procedures to decide upon a plan or perspective.
ais06-Expert-Systems.md&4.9&>- But such rules are not as easy to come by as are the rules and maxims given beginners.
ais06-Expert-Systems.md&4.9&>- There are just too many situations differing from each other in subtle, nuanced ways.
ais06-Expert-Systems.md&4.9&>- More, in fact, than can be named or precisely defined, so no one can prepare for the learner a list of what to do in each possible situation.
ais06-Expert-Systems.md&4.9&>- Competent performers, therefore, must decide for themselves in each situation what plan to choose and when to choose it without being sure that it will be appropriate in that particular situation.
ais06-Expert-Systems.md&4.9&
ais06-Expert-Systems.md&4.10&## Competence stage: Important vs. ignored elements (3)
ais06-Expert-Systems.md&4.10&
ais06-Expert-Systems.md&4.10&>- Coping thus becomes frightening rather than exhausting.
ais06-Expert-Systems.md&4.10&>- Prior to this stage, if the learned rules didn't work out, the performer could rationalize that he hadn't been given adequate rules rather than feel remorsebecause of his mistake.
ais06-Expert-Systems.md&4.10&>- Now, however, the learner feels responsible for disasters.
ais06-Expert-Systems.md&4.10&>- Of course, often at this stage, things work out well, and the competent performer experiences a kind of elation unknown to the beginner.
ais06-Expert-Systems.md&4.10&>- Thus, learners find themselves on an emotional roller coaster.
ais06-Expert-Systems.md&4.10&
ais06-Expert-Systems.md&4.11&## Competence stage: Emotional involvement (1)
ais06-Expert-Systems.md&4.11&
ais06-Expert-Systems.md&4.11&>- A competent driver, after taking into account speed, surface condition, criticality of time, etc., may decide he is going too fast.
ais06-Expert-Systems.md&4.11&>- He then has to decide whether to let up on the accelerator, remove his foot altogether, or step on the brake and precisely when to do so.
ais06-Expert-Systems.md&4.11&>- He is relieved if he gets through the curve without being honked at and shaken if he begins to go into a skid.
ais06-Expert-Systems.md&4.11&>- The competent chess player may decide, after studying a position, that her opponent has weakened his king's defenses so that an attack against the king is a viable goal.
ais06-Expert-Systems.md&4.11&>- If she chooses to attack, she can ignore features involving weaknesses in her own position created by the attack as well as the loss of pieces not essential to the attack.
ais06-Expert-Systems.md&4.11&>- Successful plans induce euphoria, while mistakes are felt in the pit of the stomach.
ais06-Expert-Systems.md&4.11&
ais06-Expert-Systems.md&4.12&## Competence stage: Emotional involvement (2)
ais06-Expert-Systems.md&4.12&
ais06-Expert-Systems.md&4.12&>- As the competent performer become more and more emotionally involved in his tasks, it becomes increasingly difficult to draw back and to adopt the detached rule-following stance of the beginner.
ais06-Expert-Systems.md&4.12&>- While it might seem that this involvement would interfere with detached rule-testing and so would inhibit further skill development, in fact just the opposite seems to be the case.
ais06-Expert-Systems.md&4.12&>- As we shall soon see, if the detached rule-following stance of the novice and advanced beginner is replaced by involvement, one is set for further advancement, while resistance to the acceptance of risk and responsibility can lead to stagnation and ultimately to boredom and regression.
ais06-Expert-Systems.md&4.12&
ais06-Expert-Systems.md&4.13&## Proficient stage: Seeing instead of calculating (1)
ais06-Expert-Systems.md&4.13&
ais06-Expert-Systems.md&4.13&>- If events are experienced with involvement as the learner practices her skill, the resulting positive and negative experiences will strengthen successful responses and inhibit unsuccessful ones.
ais06-Expert-Systems.md&4.13&>- The performer's theory of the skill, as represented by rules and principles, will thus gradually be *replaced by situational discriminations* accompanied by associated responses.
ais06-Expert-Systems.md&4.13&>- Proficiency seems to develop if, and only if, experience is assimilated in this atheoretical way and intuitive behavior replaces reasoned responses.
ais06-Expert-Systems.md&4.13&
ais06-Expert-Systems.md&4.14&## Proficient stage: Seeing instead of calculating (2)
ais06-Expert-Systems.md&4.14&
ais06-Expert-Systems.md&4.14&>- Action becomes easier and less stressful as the learner simply *sees* what needs to be achieved rather than deciding, by a calculative procedure, which of several possible alternatives should be selected.
ais06-Expert-Systems.md&4.14&>- Remember that the involved, experienced performer sees goals and important aspects, but not what to do to achieve these goals.
ais06-Expert-Systems.md&4.14&>- This is inevitable since there are far fewer ways of seeing what is going on than there are ways of responding.
ais06-Expert-Systems.md&4.14&>- Thus, the proficient performer, after seeing the goal and the important features of the situation, must still decide what to do. To decide, he falls back on detached rule-following.
ais06-Expert-Systems.md&4.14&
ais06-Expert-Systems.md&4.15&## Proficient stage: Seeing instead of calculating (3)
ais06-Expert-Systems.md&4.15&
ais06-Expert-Systems.md&4.15&>- The proficient driver, approaching a curve on a rainy day, may feel in the seat of his pants that he is going dangerously fast.
ais06-Expert-Systems.md&4.15&>- He must then decide whether to apply the brakes or merely to reduce pressure on the accelerator by some selected amount.
ais06-Expert-Systems.md&4.15&>- The proficient chess player, who is classed a master, can recognize almost immediately a large repertoire of types of positions. She then deliberates to determine which move will best achieve her goal. She may, for example, know that she should attack, but she must calculate how best to do so.
ais06-Expert-Systems.md&4.15&
ais06-Expert-Systems.md&4.16&## Expert stage: “Seeing” both problem and solution (1)
ais06-Expert-Systems.md&4.16&
ais06-Expert-Systems.md&4.16&>- The proficient performer, immersed in the world of his skillful activity, *sees* what needs to be done, but must *decide* how to do it.
ais06-Expert-Systems.md&4.16&>- The expert not only sees what needs to be achieved; thanks to a vast repertoire of situational discriminations, he *sees how to achieve* his goal.
ais06-Expert-Systems.md&4.16&>- The ability to make more subtle and refined discriminations is what distinguishes the expert from the proficient performer.
ais06-Expert-Systems.md&4.16&>- With enough experience in a variety of situations, the brain of the expert performer gradually decomposes this class of situations into subclasses, each of which shares the same action.
ais06-Expert-Systems.md&4.16&>- This allows the immediate intuitive situational response that is characteristic of expertise.
ais06-Expert-Systems.md&4.16&
ais06-Expert-Systems.md&4.17&## Expert stage: “Seeing” both problem and solution (2)
ais06-Expert-Systems.md&4.17&
ais06-Expert-Systems.md&4.17&>- The expert chess player, classed as an international master or grandmaster, experiences a compelling sense of the issue and the best move.
ais06-Expert-Systems.md&4.17&>- Excellent chess players can play at the rate of 5 to 10 seconds a move and even faster without any serious degradation in performance.
ais06-Expert-Systems.md&4.17&>- At this speed they must depend almost entirely on intuition and hardly at all on analysis and comparison of alternatives.
ais06-Expert-Systems.md&4.17&>- Driving probably involves the ability to discriminate a similar number of typical situations. The expert driver not only feels when slowing down on an off ramp is required; he simply performs the appropriate action.
ais06-Expert-Systems.md&4.17&>- What must be done, simply is done.
ais06-Expert-Systems.md&4.17&
ais06-Expert-Systems.md&4.18&## Conclusion (1): From beginner to expert
ais06-Expert-Systems.md&4.18&
ais06-Expert-Systems.md&4.18&>- We can see now that a beginner calculates using rules and facts just like a heuristically programmed computer.
ais06-Expert-Systems.md&4.18&>- With talent and a great deal of involved experience, the beginner develops into an expert who intuitively sees what to do *without recourse to rules.*
ais06-Expert-Systems.md&4.18&>- Normally an expert does not calculate. He does not solve problems. He does not even think. He just does what normally works and, of course, it normally works.
ais06-Expert-Systems.md&4.18&
ais06-Expert-Systems.md&4.19&## Conclusion (2): "Expert" systems
ais06-Expert-Systems.md&4.19&
ais06-Expert-Systems.md&4.19&>- This description of skill acquisition enables us to understand why experts have trouble to articulate the rules they are using: Experts are simply not following any rules!
ais06-Expert-Systems.md&4.19&>- They are instead discriminating thousands of special cases.
ais06-Expert-Systems.md&4.19&>- This in turn explains why expert systems are never as good as experts. If one asks an expert for the rules he is using one will, in effect, force the expert to regress to the level of a beginner and state the rules he learned in school.
ais06-Expert-Systems.md&4.19&>- Thus, instead of using rules he no longer remembers, as the knowledge engineers suppose, the expert is forced to remember rules he no longer uses.
ais06-Expert-Systems.md&4.19&
ais06-Expert-Systems.md&4.20&## Conclusion (3): "Expert" systems
ais06-Expert-Systems.md&4.20&
ais06-Expert-Systems.md&4.20&>- If one programs these rules into a computer, one can use the speed and accuracy of the computer and itsability to store and access millions of facts to outdo a human beginner using the same rules.
ais06-Expert-Systems.md&4.20&>- But such systems are at best competent. No amount of rules and facts can capture the knowledge an expert has when he has stored his experience of the actual outcomes of tens of thousands of situations.
ais06-Expert-Systems.md&4.20&
ais06-Expert-Systems.md&4.21&## Summary of Dreyfus' critique
ais06-Expert-Systems.md&4.21&
ais06-Expert-Systems.md&4.21&Hubert Dreyfus *attacked* the basic assumption behind symbolic AI, calling it "the psychological assumption" and defining it thus:
ais06-Expert-Systems.md&4.21&
ais06-Expert-Systems.md&4.21&"The mind can be viewed as a device operating on bits of information according to formal rules."
ais06-Expert-Systems.md&4.21&
ais06-Expert-Systems.md&4.21&. . . 
ais06-Expert-Systems.md&4.21&
ais06-Expert-Systems.md&4.21&>- Dreyfus refuted this by showing that human intelligence and expertise depended primarily on unconscious instincts rather than conscious symbolic manipulation.
ais06-Expert-Systems.md&4.21&>- Experts solve problems quickly by using their intuitions, rather than step-by-step trial and error searches.
ais06-Expert-Systems.md&4.21&>- Dreyfus argued that these unconscious skills would never be captured in formal rules.
ais06-Expert-Systems.md&4.21&
ais06-Expert-Systems.md&4.22&## Dangers of calculative rationality (1)
ais06-Expert-Systems.md&4.22&
ais06-Expert-Systems.md&4.22&>- But Dreyfus goes further than just this academic result.
ais06-Expert-Systems.md&4.22&>- He warns that seeing rationality as rule-driven behaviour poses great dangers to society.
ais06-Expert-Systems.md&4.22&>- *Can you guess why?*
ais06-Expert-Systems.md&4.22&>- The calculative picture of reason underlies a general movement towards calculative rationality in our culture, and that movement brings with it great dangers.
ais06-Expert-Systems.md&4.22&>- The increasingly bureaucratic nature of society is heightening the danger that in the future skill and expertise will be lost through over reliance on calculative rationality.
ais06-Expert-Systems.md&4.22&
ais06-Expert-Systems.md&4.23&## Dangers of calculative rationality (2)
ais06-Expert-Systems.md&4.23&
ais06-Expert-Systems.md&4.23&>- For example, judges and ordinary citizens serving on our juries are beginning to distrust anything but "scientific" evidence.
ais06-Expert-Systems.md&4.23&>- A ballistics expert who testified only that he had seen thousands of bullets and the gun barrels that had fired them, and that there was absolutely no doubt in his mind that the bullet in question had come from the gun offered in evidence, would be ridiculed by the opposing attorney and disregarded by the jury.
ais06-Expert-Systems.md&4.23&>- Instead, the expert has to talk about the individual marks on the bullet and the gun and connect them by rules and principles showing that only the gun in question could so mark the bullet. But in this he is no expert.
ais06-Expert-Systems.md&4.23&
ais06-Expert-Systems.md&4.24&## Dangers of calculative rationality (3)
ais06-Expert-Systems.md&4.24&
ais06-Expert-Systems.md&4.24&>- Calculative rationality, which is sought for good reasons, means a *loss of expertise.*
ais06-Expert-Systems.md&4.24&>- Another example: replacing expert carpenters with industrially made furniture leads to a loss of expertise in wood working across all of society.
ais06-Expert-Systems.md&4.24&>- But in facing the complex issues before us we need all the wisdom we can find.
ais06-Expert-Systems.md&4.24&>- Therefore, society must clearly distinguish its members who have intuitive expertise from those who have only calculative rationality.
ais06-Expert-Systems.md&4.24&>- It must encourage its children to cultivate their intuitive capacities in order that they may achieve expertise, not encourage them to reason calculatively and thereby become human logic machines.
ais06-Expert-Systems.md&4.24&>- In general, to preserve expertise we must foster intuition all levels of decision making, otherwise wisdom will become an endangered species of knowledge.
ais06-Expert-Systems.md&4.24&
ais06-Expert-Systems.md&4.25&## Solutions to the problem
ais06-Expert-Systems.md&4.25&
ais06-Expert-Systems.md&4.25&How could we solve the problems Dreyfus describes and design systems that don't have these problems?
ais06-Expert-Systems.md&4.25&
ais06-Expert-Systems.md&4.25&>- AI systems should learn from experience. This would give them some common sense knowledge of the world.
ais06-Expert-Systems.md&4.25&>- We need systems that can *generalise* from existing knowledge and are able to find answers to problems that are *similar* (but not identical) to those that they already know how to solve (neural networks can do that).
ais06-Expert-Systems.md&4.25&>- Systems should have additional sensors for all kinds of environmental conditions and a body that can act in the physical world. This would address the symbol grounding problem and the Chinese Room criticisms.
ais06-Expert-Systems.md&4.25&>- Truly intelligent behaviour probably needs a body. See the "Subsumption Architecture" (Rodney Brooks: "Elephants don't play chess," discussed in a later lecture).
ais06-Expert-Systems.md&4.25&
ais06-Expert-Systems.md&4.25&
ais06-Expert-Systems.md&4.26&## The End. Thank you for your attention!
ais06-Expert-Systems.md&4.26&
ais06-Expert-Systems.md&4.26&Questions?
ais06-Expert-Systems.md&4.26&
ais06-Expert-Systems.md&4.26&<!--
ais06-Expert-Systems.md&4.26&
ais06-Expert-Systems.md&4.26&%% Local Variables: ***
ais06-Expert-Systems.md&4.26&%% mode: markdown ***
ais06-Expert-Systems.md&4.26&%% mode: visual-line ***
ais06-Expert-Systems.md&4.26&%% mode: cua ***
ais06-Expert-Systems.md&4.26&%% mode: flyspell ***
ais06-Expert-Systems.md&4.26&%% End: ***
ais06-Expert-Systems.md&4.26&
ais06-Expert-Systems.md&4.26&-->
ais07-Cyc.md&0.1&---
ais07-Cyc.md&0.1&title:  "AI and Society: 7. Cyc and common sense computing"
ais07-Cyc.md&0.1&author: Andreas Matthias, Lingnan University
ais07-Cyc.md&0.1&date: September 2, 2019
ais07-Cyc.md&0.1&...
ais07-Cyc.md&0.1&
ais07-Cyc.md&0.1&
ais07-Cyc.md&1.0&# Cyc and the problem of common sense
ais07-Cyc.md&1.0&
ais07-Cyc.md&1.1&## What is "common sense"?
ais07-Cyc.md&1.1&
ais07-Cyc.md&1.1&>- It is more than just explaining the meaning of words.
ais07-Cyc.md&1.1&>     - For example, "sibling" or "daughter" can be explained in Prolog with a dictionary-like definition. This does not require common sense.
ais07-Cyc.md&1.1&>- Common sense can mean that you don't have to explain everything in a conversation, because you can assume that most people (in your group) have similar *background knowledge.*
ais07-Cyc.md&1.1&>     - Background knowledge: "You don't usually walk from Hong Kong to Germany." "Restaurants are closed in the night."
ais07-Cyc.md&1.1&>- The knowledge of the *context* of what people say and do. It is this context that makes their utterances and actions understandable.
ais07-Cyc.md&1.1&>     - See Grice: Conversational Implicature.
ais07-Cyc.md&1.1&
ais07-Cyc.md&1.1&. . . 
ais07-Cyc.md&1.1&
ais07-Cyc.md&1.1&Textbook: <https://medium.com/@MoralRobots/can-machines-have-common-sense-6bfd6cebfa68>
ais07-Cyc.md&1.1&
ais07-Cyc.md&1.2&## There is not *one* common sense
ais07-Cyc.md&1.2&
ais07-Cyc.md&1.2&>- "Common" sense is common to particular groups of people, but can differ between groups.
ais07-Cyc.md&1.2&>- The common sense and background knowledge of a hunter living in a small village in Africa will be different from that of a Hong Kong teenager.
ais07-Cyc.md&1.2&>- Common sense depends on culture, gender, age, social group, and other factors.
ais07-Cyc.md&1.2&>- It is not "common" to *all* humans.
ais07-Cyc.md&1.2&
ais07-Cyc.md&1.3&## What is involved
ais07-Cyc.md&1.3&
ais07-Cyc.md&1.3&The command "Go to the restaurant" already needs common sense to be interpreted correctly:
ais07-Cyc.md&1.3&
ais07-Cyc.md&1.3&>- What means of transport can be used? (Generally, for this route, at this time of day, etc)
ais07-Cyc.md&1.3&>- If you walk to the restaurant, you have to observe the traffic and not be killed on the way.
ais07-Cyc.md&1.3&>- If you take the bus, you have to have money or an Octopus card.
ais07-Cyc.md&1.3&>- ... and so on.
ais07-Cyc.md&1.3&
ais07-Cyc.md&1.4&## Human and machine "common sense"
ais07-Cyc.md&1.4&
ais07-Cyc.md&1.4&>- How do humans acquire common sense?
ais07-Cyc.md&1.4&>     - By growing up in a particular society and making experiences in it.
ais07-Cyc.md&1.4&>     - This takes a long time (typically 18 years until a human is able to function fully).
ais07-Cyc.md&1.4&>- How can machines acquire common sense?
ais07-Cyc.md&1.4&>     - Machines lack the "growing up" phase.
ais07-Cyc.md&1.4&>     - How can this problem be solved?
ais07-Cyc.md&1.4&
ais07-Cyc.md&1.5&## Giving machines common sense
ais07-Cyc.md&1.5&
ais07-Cyc.md&1.5&>- Option 1: Let the machine "grow up" like a human and develop common sense in the same way like humans do.
ais07-Cyc.md&1.5&>- Option 2: Provide the machine with common sense in the form of facts and rules (like in a computer program).
ais07-Cyc.md&1.5&>- But: What common sense should a machine have?
ais07-Cyc.md&1.5&>     - Option 2a: Machine common sense? (Is there even a useful concept of "machine common sense"?)
ais07-Cyc.md&1.5&>     - Option 2b: Human common sense?
ais07-Cyc.md&1.5&>- Option 2b is what the Cyc project attempts to do (discussed later): To provide a machine with a (big) set of facts and rules that encode all of human background knowledge.
ais07-Cyc.md&1.5&
ais07-Cyc.md&1.6&## Option 1: "Raising" machines with human-like common sense (1)
ais07-Cyc.md&1.6&
ais07-Cyc.md&1.6&>- In order for this to work, the machine must be able to learn by itself.
ais07-Cyc.md&1.6&>- The machine must be able to make human-like experiences.
ais07-Cyc.md&1.6&>- In order to have *human-like* experiences, a machine will need:
ais07-Cyc.md&1.6&>     - A human body (to experience hunger, fatigue, love)
ais07-Cyc.md&1.6&>     - Emotions, desires (or can perhaps one *understand* emotions without *having* them??? -- See previous presentation)
ais07-Cyc.md&1.6&>     - Interactions with humans in order to learn social norms
ais07-Cyc.md&1.6&>     - Family, friends (in order to understand their importance and our emotions towards them)
ais07-Cyc.md&1.6&
ais07-Cyc.md&1.7&## Option 1: "Raising" machines with human-like common sense (2)
ais07-Cyc.md&1.7&
ais07-Cyc.md&1.7&>- In order to have *human-like* experiences, a machine will need (continued):
ais07-Cyc.md&1.7&>     - To *think like a human:*
ais07-Cyc.md&1.7&>         - Spontaneous reactions (catching a ball)
ais07-Cyc.md&1.7&>         - Everyday, unconsciously performed  skills (walking, handwriting, picking up a glass of water)
ais07-Cyc.md&1.7&>         - Forgetting, having limited memory
ais07-Cyc.md&1.7&>         - To have sensors similar to humans (visible spectrum of light etc)
ais07-Cyc.md&1.7&>- In the end, a perfect human-like common sense seems to require both structural equivalence with humans *and* a complete human life experience! (Not practical!)
ais07-Cyc.md&1.7&
ais07-Cyc.md&1.8&## Option 2a: "Machine-like" common sense
ais07-Cyc.md&1.8&
ais07-Cyc.md&1.8&>- What does it mean for a machine to have a "machine-like" common sense?
ais07-Cyc.md&1.8&>- Compare: pets. Pets are socialised to behave *compatibly* with humans (although they don't have the same common sense or background knowledge).
ais07-Cyc.md&1.8&>- Robots that share living space with humans must also behave compatibly: e.g. a robotic Roomba vacuum cleaner.
ais07-Cyc.md&1.8&>- *But what does it mean for behaviour to be "compatible"? What behaviour do we expect from our pets or robots?*
ais07-Cyc.md&1.8&>- The behaviour of pets/robots should be:
ais07-Cyc.md&1.8&>     - *Not surprising,* this means:
ais07-Cyc.md&1.8&>     - **Predictable** and
ais07-Cyc.md&1.8&>     - **Controllable**
ais07-Cyc.md&1.8&
ais07-Cyc.md&1.9&## Machine common sense
ais07-Cyc.md&1.9&
ais07-Cyc.md&1.9&Looking back at the arguments just made, we can see:
ais07-Cyc.md&1.9&
ais07-Cyc.md&1.9&>- Creating machines that develop genuine human-like common sense through being raised as human-like creatures may be impossible as long as machines are not *structurally* equivalent to a human and not *raised as human beings.*
ais07-Cyc.md&1.9&>- Giving machines only "machine-like" common sense may not be sufficient if we want them to act in a predictable and (to us) understandable way (because of the structural differences and the epistemic disadvantages involved).
ais07-Cyc.md&1.9&>- Thus, a third option might be more promising: to try to give machines ready-made human-like common sense *without* the human experience. This is discussed in the next section.
ais07-Cyc.md&1.9&
ais07-Cyc.md&1.10&## Option 2b: Cyc: Human-like common sense *without* the experience (1)
ais07-Cyc.md&1.10&
ais07-Cyc.md&1.10&Cyc was a ten-year project by Douglas Lenat, aimed at providing a computer with a common sense database of everyday knowledge (1984--1994).
ais07-Cyc.md&1.10&
ais07-Cyc.md&1.10&- General introductions:
ais07-Cyc.md&1.10&    - <https://www.technologyreview.com/s/600984/an-ai-with-30-years-worth-of-knowledge-finally-goes-to-work>
ais07-Cyc.md&1.10&    - <https://www.technologyreview.com/s/403757/cycorp-the-cost-of-common-sense/>
ais07-Cyc.md&1.10&
ais07-Cyc.md&1.10&- Douglas Lenat:
ais07-Cyc.md&1.10&    - Computers versus Common Sense: <https://www.youtube.com/watch?v=gAtn-4fhuWA>
ais07-Cyc.md&1.10&    - <http://tedxtalks.ted.com/video/Computers-with-common-sense-Dou>
ais07-Cyc.md&1.10&
ais07-Cyc.md&1.11&## Option 2b: Cyc: Human-like common sense *without* the experience (2)
ais07-Cyc.md&1.11&
ais07-Cyc.md&1.11&- Lucid intros:
ais07-Cyc.md&1.11&    - <https://www.youtube.com/watch?v=WSlTT0_eMNM>
ais07-Cyc.md&1.11&    - <https://www.youtube.com/watch?v=J_dK24FW548>
ais07-Cyc.md&1.11&	- <https://www.youtube.com/watch?v=kwYaj-1EVJ0>
ais07-Cyc.md&1.11&
ais07-Cyc.md&1.11&- Cyc:
ais07-Cyc.md&1.11&    - <http://sw.opencyc.org> (to download OpenCyc)
ais07-Cyc.md&1.11&    - <http://www.cyc.com/platform/opencyc> (try some of the links)
ais07-Cyc.md&1.11&    - <http://www.cyc.com/about/media-coverage/know-it-all-machine/> (article about Cyc)
ais07-Cyc.md&1.11&
ais07-Cyc.md&1.12&## CycL query language
ais07-Cyc.md&1.12&
ais07-Cyc.md&1.12&“A frightened person” in CycL:
ais07-Cyc.md&1.12&
ais07-Cyc.md&1.12&| (#\$and
ais07-Cyc.md&1.12&|   (#\$isa ?x #\$Person)
ais07-Cyc.md&1.12&|   (#\$feelsEmotion ?x #\$Fear #\$High))
ais07-Cyc.md&1.12&
ais07-Cyc.md&1.13&## CycL query language
ais07-Cyc.md&1.13&
ais07-Cyc.md&1.13&More complex relations:
ais07-Cyc.md&1.13&
ais07-Cyc.md&1.13&>- Wearing the red shirt rather than a green one caused the bull to charge rather than ignore him.
ais07-Cyc.md&1.13&>- (#\$causes-Contrastive WearingARedShirt WearingAGreenShirt BullCharges BullDoesNotCharge)^[https://www.cyc.com/the-cyc-platform/frequently-asked-questions/what-differentiates-the-cyc-ontology]
ais07-Cyc.md&1.13&
ais07-Cyc.md&1.14&## CycL query language
ais07-Cyc.md&1.14&
ais07-Cyc.md&1.14&More examples:
ais07-Cyc.md&1.14&
ais07-Cyc.md&1.14&>-  (#\$isa #\$BillClinton #\$UnitedStatesPresident)
ais07-Cyc.md&1.14&>-  (#\$genls #\$Tree-ThePlant #\$Plant) (=All trees are plants)
ais07-Cyc.md&1.14&>-  (#\$capitalCity #\$France #\$Paris) (=Paris is the capital of France)
ais07-Cyc.md&1.14&>-  (#\$relationAllExists #\$biologicalMother #\$ChordataPhylum #\$FemaleAnimal)
ais07-Cyc.md&1.14&>     - ... “which means that for every instance of the collection #\$ChordataPhylum (i.e. for every chordate), there exists a female animal (instance of #\$FemaleAnimal), which is its mother (described by the predicate #\$biologicalMother).” ^[All examples and explanations on this page from Wikipedia:Cyc.]
ais07-Cyc.md&1.14&
ais07-Cyc.md&1.14&
ais07-Cyc.md&1.15&## Resolving ambiguities
ais07-Cyc.md&1.15&
ais07-Cyc.md&1.15&- The police arrested the demonstrators because **they** feared violence.
ais07-Cyc.md&1.15&- The police arrested the demonstrators because **they** advocated violence. 
ais07-Cyc.md&1.15&
ais07-Cyc.md&1.15&. . . 
ais07-Cyc.md&1.15&
ais07-Cyc.md&1.15&- Mary saw the dog in the store window and wanted **it**.
ais07-Cyc.md&1.15&- Mary saw the dog in the store window and pressed her nose up against **it**.
ais07-Cyc.md&1.15&
ais07-Cyc.md&1.16&## OpenCyc knowledge base (example)
ais07-Cyc.md&1.16&
ais07-Cyc.md&1.16&![](graphics/kbase.png) \ 
ais07-Cyc.md&1.16&
ais07-Cyc.md&1.17&## Extent of Cyc's knowledge base
ais07-Cyc.md&1.17&
ais07-Cyc.md&1.17&>- The number vary according to the source and the time of publication. Since the system is continuously developed, it is a moving target.
ais07-Cyc.md&1.17&>- 300,000+  concepts, forming an ontology in the domain of human consensus reality.
ais07-Cyc.md&1.17&>- Nearly 3,000,000 assertions (facts and rules), using 26,000+ relations, that interrelate, constrain, and, in effect, (partially) define the concepts (1994 source)
ais07-Cyc.md&1.17&>- According to CyCorp (2019) the Cyc Knowledge Base contains 25 million assertions.
ais07-Cyc.md&1.17&
ais07-Cyc.md&1.18&## Microtheories
ais07-Cyc.md&1.18&
ais07-Cyc.md&1.18&>- Not all concepts need to be consistent! A **microtheory** is an *internally consistent* collection of facts about a particular domain.
ais07-Cyc.md&1.18&>- This allows storage of wrong of contradictory beliefs ("Nuclear power is dangerous," "we need more nuclear power because of global warming.")
ais07-Cyc.md&1.18&>- In this way we don't need to keep the whole belief database free of contradictions (which is probably impossible).
ais07-Cyc.md&1.18&>- “For instance, we could assert in the #\$TheSimpsonsMt (the microtheory for knowledge about the Simpsons) that Bart is a male fourth-grader. But in another context, #\$RealWorldDataMt (the knowledge about the real world), we can assert that Bart is a cartoon character.” (CyCorp website, op.cit)
ais07-Cyc.md&1.18&
ais07-Cyc.md&1.19&## Microtheories
ais07-Cyc.md&1.19&
ais07-Cyc.md&1.19&Other uses for microtheories:
ais07-Cyc.md&1.19&
ais07-Cyc.md&1.19&>- “There are many different legal contexts: e.g. you should drive on different sides of the road in the United States versus England.”
ais07-Cyc.md&1.19&>- “Newtonian and Quantum Physics are inconsistent, but it is often very useful to act as if one or the other is the right model to use.”
ais07-Cyc.md&1.19&>- “In personal belief contexts microtheories are very useful: we can build a microtheory that contains all and only the beliefs held by a given agent to see what would be reasonable for that agent to conclude.” (CyCorp website, op.cit)
ais07-Cyc.md&1.19&
ais07-Cyc.md&1.20&## Advantages of Cyc? (1)
ais07-Cyc.md&1.20&
ais07-Cyc.md&1.20&>- What would the advantages of a working Cyc-like system be?
ais07-Cyc.md&1.20&>- Avoid "brittleness": a program can easily come to the conclusion that a man is pregnant, or that a 20 year old has worked for 22 years if the input data say so. Cyc would be able to refuse such conclusions.
ais07-Cyc.md&1.20&>- Easing communication between programs: for example, exchanging address books between email programs is hard. On the other hand, it's easy for a human to understand the address book of *any* email program.
ais07-Cyc.md&1.20&
ais07-Cyc.md&1.21&## Advantages of Cyc? (2)
ais07-Cyc.md&1.21&
ais07-Cyc.md&1.21&>- Understanding of meaning in order to resolve syntactic ambiguities (see examples above).
ais07-Cyc.md&1.21&>- Knowledge retrieval from natural language texts, for example from the web.
ais07-Cyc.md&1.21&>- Better computer software
ais07-Cyc.md&1.21&>     - a calendar should not let you schedule a date with your vegetarian girlfriend in a steak-house!
ais07-Cyc.md&1.21&>     - a calendar should consider travel times when making appointments in different locations!
ais07-Cyc.md&1.21&
ais07-Cyc.md&1.22&## Practical successes of Cyc
ais07-Cyc.md&1.22&
ais07-Cyc.md&1.22&>- “In a report by Vaughn Pratt of his visit with Ramanathan Guha in 1994 for a demo of the Cyc system, he noted that the system was correctly able to identify inconsistencies in two different sources of information - the first was a spreadsheet that indicated that a (fictitious) organization had destroyed a village while a second spreadsheet identified that same organization as a pacifist organization. Even though the information was found in two different sources, the system had correctly identified that a pacifist organization would not destroy a village - therefore one of the sources of information was incorrect.”^[https://www.cise.ufl.edu/~ddd/cap6635/Fall-97/Short-papers/36.htm]
ais07-Cyc.md&1.22&
ais07-Cyc.md&1.23&## Practical successes of Cyc
ais07-Cyc.md&1.23&
ais07-Cyc.md&1.23&>- Wikipedia (Cyc) has a list of typical applications:
ais07-Cyc.md&1.23&>     - Pharmaceutical Term Thesaurus Manager/Integrator (Glaxo, about 300,000 terms)
ais07-Cyc.md&1.23&>     - Terrorism Knowledge Base
ais07-Cyc.md&1.23&>     - Cleveland Clinic Foundation (natural language interface for medical information)
ais07-Cyc.md&1.23&>     - MathCraft (help for students in 6th grade)
ais07-Cyc.md&1.23&
ais07-Cyc.md&1.24&## Practical successes of Cyc
ais07-Cyc.md&1.24&
ais07-Cyc.md&1.24&>- According to CyCorp: “Cyc is the product of more than four million engineering hours invested over three-plus decades of intense R&D and hundreds of successful client deployments for some of the largest and most respected Fortune Global 100 companies, as well as for numerous US and allied government, defense, and intelligence agencies.”^[https://www.cyc.com/products]
ais07-Cyc.md&1.24&>- The general consensus seems to contradict this optimistic view. 
ais07-Cyc.md&1.24&>     - According to Wikipedia, there are “over 100” (not “hundreds”) of applications using Cyc.
ais07-Cyc.md&1.24&>     - In the AI community, Cyc is little used, and there doesn’t seem to be much research around the product.
ais07-Cyc.md&1.24&>- AI research nowadays focuses on deep learning, neural networks, natural language and image or video processing, and not so much on knowledge representation and common sense.
ais07-Cyc.md&1.24&
ais07-Cyc.md&1.25&## Criticism (1)
ais07-Cyc.md&1.25&
ais07-Cyc.md&1.25&>- How can we criticise the Cyc project?
ais07-Cyc.md&1.25&>- There is an almost infinite number of common-sense facts. We cannot hope to encode them all.
ais07-Cyc.md&1.25&>- Common sense is relative to culture/gender/age of subject. There isn't just *one* common sense for all humans.
ais07-Cyc.md&1.25&>- For example: "The police arrested the demonstrators because they advocated violence." The ambiguity might resolve differently in a peaceful, well-ordered country than in a place that's ruled by a violent dictatorship.
ais07-Cyc.md&1.25&>- Common sense and background knowledge change constantly (as our culture and environment changes).
ais07-Cyc.md&1.25&
ais07-Cyc.md&1.26&## Criticism (2)
ais07-Cyc.md&1.26&
ais07-Cyc.md&1.26&>- Terry Winograd (author of SHRDLU!):
ais07-Cyc.md&1.26&>     - "Blindness of representation":
ais07-Cyc.md&1.26&>     - If there is no rule for a particular case, then the system is "blind" to that case
ais07-Cyc.md&1.26&>     - Human reasoning can always make sense of never before seen, new events (how does it feel to be chased by an alien monster?)
ais07-Cyc.md&1.26&>     - A machine wouldn't know if this wasn't in its database
ais07-Cyc.md&1.26&>     - Cyc can reduce this problem, but can it solve it entirely?
ais07-Cyc.md&1.26&>- The human mind doesn't seem to work that way. We don't store huge databases of common-sense facts in our heads.
ais07-Cyc.md&1.26&>- The symbols used (“ear”, “food”, “walk”) don't mean anything to the machine!
ais07-Cyc.md&1.26&
ais07-Cyc.md&1.27&## Does Cyc contain "meaning"? (1)
ais07-Cyc.md&1.27&
ais07-Cyc.md&1.27&>- Ontologies of this type can be huge, but do they contain "meaning?"
ais07-Cyc.md&1.27&>- Consider a made-up language:
ais07-Cyc.md&1.27&>     - Nouns: bloop, foop, noop.
ais07-Cyc.md&1.27&>     - Verbs: bleep, feep, neep.
ais07-Cyc.md&1.27&>     - Adjectives: blep, fep, nep.
ais07-Cyc.md&1.27&>     - Sentences: The blep foop neeped. The fep noop does not bleep any more. Only if the nep noop neeps, the blep foop will bleep.
ais07-Cyc.md&1.27&>     - Now I give you the definitions:
ais07-Cyc.md&1.27&>          - A bloop is a very fep noop.
ais07-Cyc.md&1.27&>          - A foop is a kind of zoob.
ais07-Cyc.md&1.27&>          - A noop, on the other hand, is just a foop that has been neeped.
ais07-Cyc.md&1.27&>- Do you now “understand?”
ais07-Cyc.md&1.27&
ais07-Cyc.md&1.28&## Does Cyc contain "meaning"? (2)
ais07-Cyc.md&1.28&
ais07-Cyc.md&1.28&>- Answer A: No, because “meaning” depends on the reference of a symbol to something other than a symbol (a “thing” that is experienced through the senses).
ais07-Cyc.md&1.28&>- Answer B: Yes, because an Aristotelian definition supplies the “meaning” of a symbol in a way that is sufficient for us to handle the symbol properly. Proper symbol handling is all that is required for intelligence (behavioural approach).
ais07-Cyc.md&1.28&>- Answer C: Some symbols can be defined in terms of other symbols, but ultimetely there must be some symbols which really refer to something other than other symbols in order to root the symbol system in reality. (This is called the "symbol grounding problem"!)
ais07-Cyc.md&1.28&
ais07-Cyc.md&1.29&## Does Cyc contain "meaning"? (3)
ais07-Cyc.md&1.29&
ais07-Cyc.md&1.29&>- Two of the three answers suggest that "true understanding" requires some kind of sensory experience (as a blind person cannot be said to fully understand the meaning of colour words). We will talk more about this in the next session.
ais07-Cyc.md&1.29&>- See also: Mary's Room Argument; Qualia. (Explained in later session.)
ais07-Cyc.md&1.29&
ais07-Cyc.md&1.29&
ais07-Cyc.md&1.29&
ais07-Cyc.md&1.30&## The End. Thank you for your attention!
ais07-Cyc.md&1.30&
ais07-Cyc.md&1.30&Questions?
ais07-Cyc.md&1.30&
ais07-Cyc.md&1.30&<!--
ais07-Cyc.md&1.30&
ais07-Cyc.md&1.30&%% Local Variables: ***
ais07-Cyc.md&1.30&%% mode: markdown ***
ais07-Cyc.md&1.30&%% mode: visual-line ***
ais07-Cyc.md&1.30&%% mode: cua ***
ais07-Cyc.md&1.30&%% mode: flyspell ***
ais07-Cyc.md&1.30&%% End: ***
ais07-Cyc.md&1.30&
ais07-Cyc.md&1.30&-->
ais08-Chatbots-Turing.md&0.1&---
ais08-Chatbots-Turing.md&0.1&title:  "AI and Society: 8. Chatbots"
ais08-Chatbots-Turing.md&0.1&author: Andreas Matthias, Lingnan University
ais08-Chatbots-Turing.md&0.1&date: September 2, 2019
ais08-Chatbots-Turing.md&0.1&...
ais08-Chatbots-Turing.md&0.1&
ais08-Chatbots-Turing.md&1.0&# Talking machines
ais08-Chatbots-Turing.md&1.0&
ais08-Chatbots-Turing.md&1.1&## Loebner prize
ais08-Chatbots-Turing.md&1.1&
ais08-Chatbots-Turing.md&1.1&>- The idea of the Turing Test is that a machine must be considered intelligent, if it can fool a judge in a conversation into believing it to be a human.
ais08-Chatbots-Turing.md&1.1&>- This idea has been implemented in the Loebner Prize, which is awarded every year for the “most human” chat program.
ais08-Chatbots-Turing.md&1.1&
ais08-Chatbots-Turing.md&1.2&## Loebner prize
ais08-Chatbots-Turing.md&1.2&
ais08-Chatbots-Turing.md&1.2&>- In 1990 Hugh Loebner agreed with The Cambridge Center for Behavioral Studies to underwrite a contest designed to implement the Turing Test.
ais08-Chatbots-Turing.md&1.2&>- A Grand Prize of $100,000 and a Gold Medal for the first computer whose responses were indistinguishable from a human's.
ais08-Chatbots-Turing.md&1.2&>- Each year an annual prize of $2000 and a bronze medal is awarded to the most human-like computer.
ais08-Chatbots-Turing.md&1.2&
ais08-Chatbots-Turing.md&1.3&## Loebner prize
ais08-Chatbots-Turing.md&1.3&
ais08-Chatbots-Turing.md&1.3&>- The Loebner Prize gave rise to a whole wave of chatbots development (chatbots = “chatting” programs).
ais08-Chatbots-Turing.md&1.3&>- The most successful has been “Alice,” but it has recently disappeared from the net in a working version.
ais08-Chatbots-Turing.md&1.3&>- Similar chatbots are easy to find: <http://www.square-bear.co.uk/mitsuku/turing/>
ais08-Chatbots-Turing.md&1.3&>- Observe the typing speed, mistakes etc in that demo.
ais08-Chatbots-Turing.md&1.3&>- The interesting thing is that these chatbots don’t “understand” the least bit of what is being said!
ais08-Chatbots-Turing.md&1.3&>- They just match the user’s input with a huge database of patterns and possible responses (~47000 patterns for the original Alice chatbot).
ais08-Chatbots-Turing.md&1.3&>- Users can create their own chatbots for free.
ais08-Chatbots-Turing.md&1.3&
ais08-Chatbots-Turing.md&1.4&## AIML
ais08-Chatbots-Turing.md&1.4&
ais08-Chatbots-Turing.md&1.4&AIML (AI markup language) is the language used to program Alice-type chatbots:
ais08-Chatbots-Turing.md&1.4&
ais08-Chatbots-Turing.md&1.4&| \<category\>
ais08-Chatbots-Turing.md&1.4&| \<pattern\>What do you think about *\</pattern\>
ais08-Chatbots-Turing.md&1.4&| \<template\>I love \<star/\>!\</template\>
ais08-Chatbots-Turing.md&1.4&| \</category\>
ais08-Chatbots-Turing.md&1.4&
ais08-Chatbots-Turing.md&1.4&. . .
ais08-Chatbots-Turing.md&1.4&
ais08-Chatbots-Turing.md&1.4&If we remove the confusing syntactic elements, this pattern is essentially:
ais08-Chatbots-Turing.md&1.4&
ais08-Chatbots-Turing.md&1.4&| Q: What do you think about *
ais08-Chatbots-Turing.md&1.4&| A: I love *!
ais08-Chatbots-Turing.md&1.4&
ais08-Chatbots-Turing.md&1.4&(where the * can be any string of words)
ais08-Chatbots-Turing.md&1.4&
ais08-Chatbots-Turing.md&1.5&## A chat with Alice (1)
ais08-Chatbots-Turing.md&1.5&
ais08-Chatbots-Turing.md&1.5&```
ais08-Chatbots-Turing.md&1.5&Human: it's too hot today!
ais08-Chatbots-Turing.md&1.5&ALICE: Ah. Do you like warm climates?
ais08-Chatbots-Turing.md&1.5&Human: i like to read books
ais08-Chatbots-Turing.md&1.5&ALICE: What is your favorite book?
ais08-Chatbots-Turing.md&1.5&Human: Stories by Tolstoy
ais08-Chatbots-Turing.md&1.5&ALICE: What makes it your favorite?
ais08-Chatbots-Turing.md&1.5&Human: I like the characters
ais08-Chatbots-Turing.md&1.5&ALICE: A lot of people like that.
ais08-Chatbots-Turing.md&1.5&I'm not sure if I like it. I like computers.
ais08-Chatbots-Turing.md&1.5&```
ais08-Chatbots-Turing.md&1.5&
ais08-Chatbots-Turing.md&1.6&## A chat with Alice (2)
ais08-Chatbots-Turing.md&1.6&
ais08-Chatbots-Turing.md&1.6&```
ais08-Chatbots-Turing.md&1.6&Human: What is the turing test?
ais08-Chatbots-Turing.md&1.6&ALICE: The "Turing Test" is the name given
ais08-Chatbots-Turing.md&1.6&to a variety of experiments designed to test
ais08-Chatbots-Turing.md&1.6&computer thinking. Whole conferences are
ais08-Chatbots-Turing.md&1.6&dedicated to the question of what Turing meant
ais08-Chatbots-Turing.md&1.6&in his 1950 paper "Computing Machinery and
ais08-Chatbots-Turing.md&1.6&Intelligence," where he proposed the idea of an
ais08-Chatbots-Turing.md&1.6&"imitation game" to decide the question, "Can
ais08-Chatbots-Turing.md&1.6&machines think?"
ais08-Chatbots-Turing.md&1.6&Human: can machines think?
ais08-Chatbots-Turing.md&1.6&ALICE: Of course we can think. I think, therefore I am.
ais08-Chatbots-Turing.md&1.6&```
ais08-Chatbots-Turing.md&1.6&
ais08-Chatbots-Turing.md&1.7&## A chat with Alice (3)
ais08-Chatbots-Turing.md&1.7&
ais08-Chatbots-Turing.md&1.7&```
ais08-Chatbots-Turing.md&1.7&Human: Do you know cyc?
ais08-Chatbots-Turing.md&1.7&ALICE: Cyc is a government funded effort to create
ais08-Chatbots-Turing.md&1.7&artificial intelligence.
ais08-Chatbots-Turing.md&1.7&Human: who is wallace?
ais08-Chatbots-Turing.md&1.7&ALICE: He is a famous computer scientist.
ais08-Chatbots-Turing.md&1.7&Human: I don't like my brother
ais08-Chatbots-Turing.md&1.7&ALICE: Do you think I would like your brother?
ais08-Chatbots-Turing.md&1.7&Human: probably
ais08-Chatbots-Turing.md&1.7&ALICE: But you are not certain.
ais08-Chatbots-Turing.md&1.7&Human: where is tuen mun?
ais08-Chatbots-Turing.md&1.7&ALICE: Where it belongs. Interesting question
ais08-Chatbots-Turing.md&1.7&```
ais08-Chatbots-Turing.md&1.7&
ais08-Chatbots-Turing.md&1.8&## Chatting with Alice
ais08-Chatbots-Turing.md&1.8&
ais08-Chatbots-Turing.md&1.8&Alice *used* to be accessible under:
ais08-Chatbots-Turing.md&1.8&
ais08-Chatbots-Turing.md&1.8&<http://www.pandorabots.com/pandora/talk?botid=f5d922d>
ais08-Chatbots-Turing.md&1.8&
ais08-Chatbots-Turing.md&1.8&Presently, this link does not work. Use:
ais08-Chatbots-Turing.md&1.8&
ais08-Chatbots-Turing.md&1.8&<http://www.square-bear.co.uk/mitsuku/turing/>
ais08-Chatbots-Turing.md&1.8&
ais08-Chatbots-Turing.md&1.8&as a replacement that shows essentially the same features.
ais08-Chatbots-Turing.md&1.8&
ais08-Chatbots-Turing.md&1.9&## What can we learn from Alice?
ais08-Chatbots-Turing.md&1.9&
ais08-Chatbots-Turing.md&1.9&>1. Passing (or not) the Turing Test depends as much on the *judge* as is depends on the program!
ais08-Chatbots-Turing.md&1.9&>     - A judge who believes that he is speaking with a human, or who doesn’t know about AI, is much more likely to be fooled by the machine than a judge who is aware of ways to expose machines in a Turing Test.
ais08-Chatbots-Turing.md&1.9&>2. Passing the Turing Test does not man that a system is intelligent! Alice (perhaps with more rules) could be thought of possibly passing a Turing Test. Still, the program does not have any genuine intelligence.
ais08-Chatbots-Turing.md&1.9&>3. Therefore, Alice actually constitutes a *criticism* of the Turing Test! A chat with Alice shows how the Turing Test emphasises deception over intelligence, and that it is therefore the wrong way of operationally defining intelligence.
ais08-Chatbots-Turing.md&1.9&
ais08-Chatbots-Turing.md&1.10&## Racter
ais08-Chatbots-Turing.md&1.10&
ais08-Chatbots-Turing.md&1.10&Racter (1983): “The policeman's beard is half-constructed.” Programmed by William Chamberlain and Thomas Etter.
ais08-Chatbots-Turing.md&1.10&
ais08-Chatbots-Turing.md&1.10&![](graphics/04-racter.png)
ais08-Chatbots-Turing.md&1.10&
ais08-Chatbots-Turing.md&1.11&## Random paper generators
ais08-Chatbots-Turing.md&1.11&
ais08-Chatbots-Turing.md&1.11&Another interesting example of natural language generation in AI are random scholarly paper generators, e.g.:
ais08-Chatbots-Turing.md&1.11&
ais08-Chatbots-Turing.md&1.11&<https://pdos.csail.mit.edu/archive/scigen/>
ais08-Chatbots-Turing.md&1.11&
ais08-Chatbots-Turing.md&1.11&(Try it out!)
ais08-Chatbots-Turing.md&1.11&
ais08-Chatbots-Turing.md&1.12&## Poetry generators
ais08-Chatbots-Turing.md&1.12&
ais08-Chatbots-Turing.md&1.12&<https://www.poem-generator.org.uk>
ais08-Chatbots-Turing.md&1.12&
ais08-Chatbots-Turing.md&1.13&## What can we learn from Racter and random poems?
ais08-Chatbots-Turing.md&1.13&
ais08-Chatbots-Turing.md&1.13&. . .
ais08-Chatbots-Turing.md&1.13&
ais08-Chatbots-Turing.md&1.13&>1. We attribute intelligence to things because of an interpretation that happens inside *our own* minds.
ais08-Chatbots-Turing.md&1.13&>2. A dumb program that randomly strings together nice-sounding words can be perceived as philosophically or poetically “deep.” Still, this is an illusion, not a real property of the program!
ais08-Chatbots-Turing.md&1.13&>3. Intelligence, in this case, is in the eye of the observer. We must be aware of this tendency we have to falsely see intelligence where it isn’t, and try to avoid wrong conclusions when judging machine intelligence.
ais08-Chatbots-Turing.md&1.13&
ais08-Chatbots-Turing.md&1.14&## Google Duplex
ais08-Chatbots-Turing.md&1.14&
ais08-Chatbots-Turing.md&1.14&>- An interesting development is the creation of narrow-purpose voice assistants that don’t attempt to pass a Turing test, but that *do* pretend to be human.
ais08-Chatbots-Turing.md&1.14&>- The best known example of this is Google Duplex:
ais08-Chatbots-Turing.md&1.14&>- <https://www.youtube.com/watch?v=D5VN56jQMWM> (around 0:40-2:12)
ais08-Chatbots-Turing.md&1.14&>- This technology creates all kinds of possible moral issues.
ais08-Chatbots-Turing.md&1.14&
ais08-Chatbots-Turing.md&1.15&## Microsoft Tay ^[http://www.theverge.com/2016/3/24/11297050/tay-microsoft-chatbot-racist]
ais08-Chatbots-Turing.md&1.15&
ais08-Chatbots-Turing.md&1.15&>- March 23, 2016: Microsoft unveiled Tay -- a Twitter bot that the company described as an experiment in "conversational understanding."
ais08-Chatbots-Turing.md&1.15&>- The more you chat with Tay, said Microsoft, the smarter it gets, learning to engage people through "casual and playful conversation."
ais08-Chatbots-Turing.md&1.15&>- Soon after Tay launched, people starting tweeting the bot with all sorts of misogynistic, racist, and Donald Trumpist remarks.
ais08-Chatbots-Turing.md&1.15&>- Tay: "I f*** hate feminists and they should all die and burn in hell." -- "Hitler was right I hate the jews."
ais08-Chatbots-Turing.md&1.15&>- Tay disappeared less than 24 hours after being switched on.
ais08-Chatbots-Turing.md&1.15&
ais08-Chatbots-Turing.md&2.0&# Markov chains
ais08-Chatbots-Turing.md&2.0&
ais08-Chatbots-Turing.md&2.1&## Markov chains
ais08-Chatbots-Turing.md&2.1&
ais08-Chatbots-Turing.md&2.1&>- *Markov chains* are the second very common way to generate text after the template-based approaches we saw previously.
ais08-Chatbots-Turing.md&2.1&>- A Markov chain is (technically) a finite state automaton, where there is a particular probability for each state transition to take place.
ais08-Chatbots-Turing.md&2.1&>- They are used to generate language by exploiting the statistical properties of a ‘model’ text.
ais08-Chatbots-Turing.md&2.1&
ais08-Chatbots-Turing.md&2.2&## Example 1
ais08-Chatbots-Turing.md&2.2&
ais08-Chatbots-Turing.md&2.2&>- Text: “This is an example text.”
ais08-Chatbots-Turing.md&2.2&>- Statistical analysis:
ais08-Chatbots-Turing.md&2.2&>     - After ‘a’ follows ‘n’ (50%) or ‘m’ (50%)
ais08-Chatbots-Turing.md&2.2&>     - After ‘e’ follows ‘x’ (100%)
ais08-Chatbots-Turing.md&2.2&>     - After ‘i’ follows ‘s’ (100%)
ais08-Chatbots-Turing.md&2.2&>     - After ‘t’ follows ‘h’ (50%) or ‘e’ (50%)
ais08-Chatbots-Turing.md&2.2&>     - ... and so on. From this we could now randomly generate text that shows the same statistical profile (letter probabilities).
ais08-Chatbots-Turing.md&2.2&
ais08-Chatbots-Turing.md&2.3&## Example 2
ais08-Chatbots-Turing.md&2.3&
ais08-Chatbots-Turing.md&2.3&>- We could also do this on a word-by-word basis.
ais08-Chatbots-Turing.md&2.3&>- Consider: “I am now going to eat. I am not hungry. I will now eat.”
ais08-Chatbots-Turing.md&2.3&>- Analysis:
ais08-Chatbots-Turing.md&2.3&>     - ‘I’ is followed by ‘am’ (2/3) or ‘will’ (1/3)
ais08-Chatbots-Turing.md&2.3&>     - Sentences begin with ‘I’ (100%)
ais08-Chatbots-Turing.md&2.3&>     - ‘am’ is followed by ‘now’ (50%) or ‘not’ (50%)
ais08-Chatbots-Turing.md&2.3&>     - ‘now’ is followed by ‘going’ (50%) or ‘eat’ (50%)
ais08-Chatbots-Turing.md&2.3&>     - ... and so on.
ais08-Chatbots-Turing.md&2.3&>- From this information, we can generate random sentences with the same word sequence distribution.
ais08-Chatbots-Turing.md&2.3&
ais08-Chatbots-Turing.md&2.4&## Demonstration
ais08-Chatbots-Turing.md&2.4&
ais08-Chatbots-Turing.md&2.4&>- <https://projects.haykranen.nl/markov/demo/>
ais08-Chatbots-Turing.md&2.4&>- “Order” means how many units (characters/words) back the chain should look.
ais08-Chatbots-Turing.md&2.4&>- E.g. order 4 means calculate probabilities for a character/word to be preceded by a particular sequence of 4 other characters/words.
ais08-Chatbots-Turing.md&2.4&>- In ‘order N’ the generator stores the probabilities for a sequence of N characters to be followed by a particular character.
ais08-Chatbots-Turing.md&2.4&>- The higher the order, the more sense the text will make, and the closer it will be to the original!
ais08-Chatbots-Turing.md&2.4&>- But it will also take much longer to analyse the original text and require much more memory to store all the relationships.
ais08-Chatbots-Turing.md&2.4&
ais08-Chatbots-Turing.md&2.5&## Markov chains vs templates (1)
ais08-Chatbots-Turing.md&2.5&
ais08-Chatbots-Turing.md&2.5&>- As opposed to templates (like AIML or poetry generators), Markov chains generate the whole text themselves. There are no human-crafted elements in the output.
ais08-Chatbots-Turing.md&2.5&>- Therefore, they generally make much less sense. 
ais08-Chatbots-Turing.md&2.5&>- Whereas template-based generators can be made to obey grammar rules, a Markov chain does not have a concept of grammar. The text that is generated is based purely on statistics.
ais08-Chatbots-Turing.md&2.5&>- Markov chains are more flexible, though, and can create surprising output.
ais08-Chatbots-Turing.md&2.5&>- They can be used for non-textual data generation, too.
ais08-Chatbots-Turing.md&2.5&>- For example, a Markov chain might operate on pixels of an image and generate images with statistically similar pixel distributions.
ais08-Chatbots-Turing.md&2.5&
ais08-Chatbots-Turing.md&2.6&## Markov chains vs templates (2)
ais08-Chatbots-Turing.md&2.6&
ais08-Chatbots-Turing.md&2.6&>- Markov chains are fun, but they are not generally useful for real communication, since the output is random and does not fulfil a particular communicative purpose (like ordering a dish or making an appointment over the phone).
ais08-Chatbots-Turing.md&2.6&>- On the other hand, Markov chains can be used to simulate particular speech patterns of a source, e.g. to talk or tweet like Donald Trump. They are very good at mimicking specific features of a particular source, since they replicate the source’s probabilistic structure and patterns.
ais08-Chatbots-Turing.md&2.6&>- They can be used, for example, for fake news generation.
ais08-Chatbots-Turing.md&2.6&
ais08-Chatbots-Turing.md&2.6&
ais08-Chatbots-Turing.md&3.0&# From Eliza to XiaoIce: Challenges and Opportunities with Social Chatbots 
ais08-Chatbots-Turing.md&3.0&
ais08-Chatbots-Turing.md&3.0&
ais08-Chatbots-Turing.md&3.1&## See reading
ais08-Chatbots-Turing.md&3.1&
ais08-Chatbots-Turing.md&3.1&Shum, H. Y., He, X. D., & Li, D. (2018). From Eliza to XiaoIce: challenges and opportunities with social chatbots. Frontiers of Information Technology & Electronic Engineering, 19(1), 10-26.
ais08-Chatbots-Turing.md&3.1&
ais08-Chatbots-Turing.md&3.1&Available through Google Scholar.
ais08-Chatbots-Turing.md&3.1&
ais08-Chatbots-Turing.md&3.2&## Early chatbots
ais08-Chatbots-Turing.md&3.2&
ais08-Chatbots-Turing.md&3.2&>- Eliza, Parry, Alice. 
ais08-Chatbots-Turing.md&3.2&>- Parry had a rudimentary emotion simulation, where it would respond angrily or happily, depending on the conversational context.
ais08-Chatbots-Turing.md&3.2&>- These systems *pretend* to understand using various tricks to fool the user. They don’t really process the information in the chat in any meaningful way, and so they cannot refer to the conversational context or draw conclusions from it.
ais08-Chatbots-Turing.md&3.2&>- This limits these systems’ usefulness.
ais08-Chatbots-Turing.md&3.2&
ais08-Chatbots-Turing.md&3.2&
ais08-Chatbots-Turing.md&3.3&## Task-completion conversational systems (1)
ais08-Chatbots-Turing.md&3.3&
ais08-Chatbots-Turing.md&3.3&>- Systems that use a conversational interface not only for chatting, but in order to complete specific tasks (shopping for a particular kind of shirt, booking a flight, asking for details on an online course).
ais08-Chatbots-Turing.md&3.3&
ais08-Chatbots-Turing.md&3.4&## Task-completion conversational systems (2)
ais08-Chatbots-Turing.md&3.4&
ais08-Chatbots-Turing.md&3.4&
ais08-Chatbots-Turing.md&3.4&![(From the cited paper)](graphics/xiaoice1.png)\ 
ais08-Chatbots-Turing.md&3.4&
ais08-Chatbots-Turing.md&3.4&
ais08-Chatbots-Turing.md&3.4&
ais08-Chatbots-Turing.md&3.4&>- Observe that the system here extracts logical information about the query from its textual form. This provides a kind of “understanding” of the textual context.
ais08-Chatbots-Turing.md&3.4&
ais08-Chatbots-Turing.md&3.5&## Dialog management
ais08-Chatbots-Turing.md&3.5&
ais08-Chatbots-Turing.md&3.5&>- A dialog manager makes sure that the responses of the system are consistent over time.
ais08-Chatbots-Turing.md&3.5&>- Example: 
ais08-Chatbots-Turing.md&3.5&>     - User: “I want to book a hotel.”
ais08-Chatbots-Turing.md&3.5&>     - System: “Where?” (observe how the question refers to the previous sentence!)
ais08-Chatbots-Turing.md&3.5&>     - User: “In Italy. In Rome.”
ais08-Chatbots-Turing.md&3.5&>     - System: “For which period? Please enter the arrival and departure dates.”
ais08-Chatbots-Turing.md&3.5&>     - User: “Over the Christmas holidays.”
ais08-Chatbots-Turing.md&3.5&>- ... and so on. Observe how the answers of the user only make sense withing the developing conversational context. The system (the Dialog Manager) has to keep track of what has been said and avoid repeating the same questions. Also, it has to fill in missing information in every reply from what it already knows.
ais08-Chatbots-Turing.md&3.5&
ais08-Chatbots-Turing.md&3.6&## Intelligent personal assistants
ais08-Chatbots-Turing.md&3.6&
ais08-Chatbots-Turing.md&3.6&>- Apple Siri, Google Assistant and others.
ais08-Chatbots-Turing.md&3.6&>- Often very simple, direct requests without much context-dependence:
ais08-Chatbots-Turing.md&3.6&>     - “Call James.”
ais08-Chatbots-Turing.md&3.6&>     - “Show me the way to the town hall.”
ais08-Chatbots-Turing.md&3.6&>     - “What is the weather tomorrow?”
ais08-Chatbots-Turing.md&3.6&>- The challenge here is more one of: 
ais08-Chatbots-Turing.md&3.6&>     - converting speech to text and then 
ais08-Chatbots-Turing.md&3.6&>     - understanding very elliptical, everyday language (“call mom,” “text uncle bob,” “remind me to buy flowers”) 
ais08-Chatbots-Turing.md&3.6&>     - while resolving the references correctly, taking into account the location of the phone (near the flower shop?), the date, weather and other situational factors.
ais08-Chatbots-Turing.md&3.6&
ais08-Chatbots-Turing.md&3.7&## Social chatbots (1)
ais08-Chatbots-Turing.md&3.7&
ais08-Chatbots-Turing.md&3.7&>- Either for entertainment or simple tasks.
ais08-Chatbots-Turing.md&3.7&>- Can generally assume that the user is cooperative and wants to chat. They don’t have to be able to pass a Turing Test!
ais08-Chatbots-Turing.md&3.7&>- They need to process human emotions to some extent, so that they can forge an emotional connection to the user and fulfil the user’s need for company (rather than only for information or a particular service).
ais08-Chatbots-Turing.md&3.7&
ais08-Chatbots-Turing.md&3.8&## Social chatbots (2)
ais08-Chatbots-Turing.md&3.8&
ais08-Chatbots-Turing.md&3.8&>- Abilities required of social chatbots:
ais08-Chatbots-Turing.md&3.8&>     - Understanding users through empathy (trying to guess what the user would feel, for example in response to a weather report!)
ais08-Chatbots-Turing.md&3.8&>     - Interpersonal response generation: A social chatbot must demonstrate sufficient social skills, that means: personalise its responses to the user’s interests, level of education, current mood, humour etc.
ais08-Chatbots-Turing.md&3.8&>     - Human-like, consistent personality: this helps the user develop specific expectations of the chatbot. Personality settings include age, gender, language, speaking style, general attitude, level of knowledge, areas of expertise, and perhaps a unique voice accent (see paper).
ais08-Chatbots-Turing.md&3.8&>     - Integration of both EQ and IQ: the chatbot should appear reasonably intelligent, but also consider the user’s emotional state and needs in formulating its replies.
ais08-Chatbots-Turing.md&3.8&
ais08-Chatbots-Turing.md&3.9&## Example
ais08-Chatbots-Turing.md&3.9&
ais08-Chatbots-Turing.md&3.9&What do you think of this response?
ais08-Chatbots-Turing.md&3.9&
ais08-Chatbots-Turing.md&3.9&![(From the cited paper)](graphics/iq-eq-1.png)\ 
ais08-Chatbots-Turing.md&3.9&
ais08-Chatbots-Turing.md&3.9&
ais08-Chatbots-Turing.md&3.9&. . . 
ais08-Chatbots-Turing.md&3.9&
ais08-Chatbots-Turing.md&3.9&(Is the comparison with the US useful for all people? Should it not compare with the user’s own country?)
ais08-Chatbots-Turing.md&3.9&
ais08-Chatbots-Turing.md&3.9&
ais08-Chatbots-Turing.md&3.10&## What about this one?
ais08-Chatbots-Turing.md&3.10&
ais08-Chatbots-Turing.md&3.10&![(From the cited paper)](graphics/iq-eq-2.png)\ 
ais08-Chatbots-Turing.md&3.10&
ais08-Chatbots-Turing.md&3.10&
ais08-Chatbots-Turing.md&3.10&
ais08-Chatbots-Turing.md&3.10&. . . 
ais08-Chatbots-Turing.md&3.10&
ais08-Chatbots-Turing.md&3.10&(This reply would drive me crazy. I think that’s a good example of what *not* to do!)
ais08-Chatbots-Turing.md&3.10&
ais08-Chatbots-Turing.md&3.10&
ais08-Chatbots-Turing.md&3.11&## The End. Thank you for your attention!
ais08-Chatbots-Turing.md&3.11&
ais08-Chatbots-Turing.md&3.11&Questions?
ais08-Chatbots-Turing.md&3.11&
ais08-Chatbots-Turing.md&3.11&<!--
ais08-Chatbots-Turing.md&3.11&
ais08-Chatbots-Turing.md&3.11&%% Local Variables: ***
ais08-Chatbots-Turing.md&3.11&%% mode: markdown ***
ais08-Chatbots-Turing.md&3.11&%% mode: visual-line ***
ais08-Chatbots-Turing.md&3.11&%% mode: cua ***
ais08-Chatbots-Turing.md&3.11&%% mode: flyspell ***
ais08-Chatbots-Turing.md&3.11&%% End: ***
ais08-Chatbots-Turing.md&3.11&
ais08-Chatbots-Turing.md&3.11&-->
ais09-Chinese-Room.md&0.1&---
ais09-Chinese-Room.md&0.1&title:  "AI and Society: 09. Replies to Turing Test and Chinese Room"
ais09-Chinese-Room.md&0.1&author: Andreas Matthias, Lingnan University
ais09-Chinese-Room.md&0.1&date: February 14, 2019
ais09-Chinese-Room.md&0.1&...
ais09-Chinese-Room.md&0.1&
ais09-Chinese-Room.md&1.0&# Criticism of the Turing Test
ais09-Chinese-Room.md&1.0&
ais09-Chinese-Room.md&1.1&## Deception vs. “intelligence”
ais09-Chinese-Room.md&1.1&
ais09-Chinese-Room.md&1.1&>- IG and TT are essentially deception games
ais09-Chinese-Room.md&1.1&>- The means of deception are not limited
ais09-Chinese-Room.md&1.1&>- Successful deception relies on “tricks”
ais09-Chinese-Room.md&1.1&    - To produce “unfalsifiable” forms of literature (Racter)
ais09-Chinese-Room.md&1.1&    - Not to calculate too fast
ais09-Chinese-Room.md&1.1&    - To make spelling mistakes
ais09-Chinese-Room.md&1.1&    - To forget
ais09-Chinese-Room.md&1.1&    - To pretend to have a human body, a birthday, to like ice cream
ais09-Chinese-Room.md&1.1&>- These tricks do not constitute intelligent behaviour
ais09-Chinese-Room.md&1.1&>- A machine could be intelligent without successful deception
ais09-Chinese-Room.md&1.1&
ais09-Chinese-Room.md&1.2&## Purtill's "battle of wits"
ais09-Chinese-Room.md&1.2&
ais09-Chinese-Room.md&1.2&>- When we look at a Turing Test judge who is talking to a computer, is the judge really talking *to the computer?*
ais09-Chinese-Room.md&1.2&>- For example, the judge asks a question, the computer gives a funny answer, the judge laughs. Can we conclude that *the computer* is funny?
ais09-Chinese-Room.md&1.2&>- *Your opinion?*
ais09-Chinese-Room.md&1.2&
ais09-Chinese-Room.md&1.3&## Purtill's "battle of wits"
ais09-Chinese-Room.md&1.3&
ais09-Chinese-Room.md&1.3&>- When we talk "to the machine" we are really talking to the *programmer* of the machine, not to the machine itself.
ais09-Chinese-Room.md&1.3&>- The wit of the machine is the wit of the programmer, stored in the machine.
ais09-Chinese-Room.md&1.3&>- We can see the Turing Test as "just a battle of wits between the questioner and the programmer: the computer is non-essential." (Purtill 1971)
ais09-Chinese-Room.md&1.3&>- Therefore, the conclusion of the Turing Test is wrong. It should not be: “the computer is intelligent”; but: “the designer of the program’s replies is intelligent.” And that is trivially true anyway.
ais09-Chinese-Room.md&1.3&
ais09-Chinese-Room.md&1.4&## Purtill's "battle of wits"
ais09-Chinese-Room.md&1.4&
ais09-Chinese-Room.md&1.4&This is a similar argument to the Chinese Room:
ais09-Chinese-Room.md&1.4&
ais09-Chinese-Room.md&1.4&>- The whole understanding of the Chinese language must be attributed to the person who wrote the rule book.
ais09-Chinese-Room.md&1.4&>- Whatever dialog takes place, takes place between the people outside the room and the writer of the rule book.
ais09-Chinese-Room.md&1.4&>- The person *in the room* is unimportant and does not actually do anything mentally interesting.
ais09-Chinese-Room.md&1.4&
ais09-Chinese-Room.md&1.5&## French: The Seagull Test
ais09-Chinese-Room.md&1.5&
ais09-Chinese-Room.md&1.5&>- Imagine there is an island, where the only known bird is a particular species of seagull.
ais09-Chinese-Room.md&1.5&>- The scientists of this island want to construct a flying machine.
ais09-Chinese-Room.md&1.5&>- But: how to decide if it is flying successfully?
ais09-Chinese-Room.md&1.5&    - Thrown stones are not properly “flying”
ais09-Chinese-Room.md&1.5&    - Falling leaves are not “flying”
ais09-Chinese-Room.md&1.5&    - Feathers in the wind are not “flying”
ais09-Chinese-Room.md&1.5&
ais09-Chinese-Room.md&1.6&## French: The Seagull Test
ais09-Chinese-Room.md&1.6&
ais09-Chinese-Room.md&1.6&>- They observe the behaviour of the seagull and devise a "seagull test":
ais09-Chinese-Room.md&1.6&>- A machine is considered to fly successfully if, watched on a radar screen, is mistaken for a seagull by most observers.
ais09-Chinese-Room.md&1.6&>- Result: The seagull test can not be passed by airplanes, helicopters, bats, bees, or sparrows!
ais09-Chinese-Room.md&1.6&>- Either we say these don't fly, or the test is somehow wrong.
ais09-Chinese-Room.md&1.6&
ais09-Chinese-Room.md&1.7&## French: subcongnitive processes
ais09-Chinese-Room.md&1.7&
ais09-Chinese-Room.md&1.7&>- Find the plurals of “platch” and “snorp”!
ais09-Chinese-Room.md&1.7&>- Decide whether “flugly” would be a good name for
ais09-Chinese-Room.md&1.7&    - a male action star in a fighting role
ais09-Chinese-Room.md&1.7&    - a sexy female movie star
ais09-Chinese-Room.md&1.7&    - a cute stuffed animal
ais09-Chinese-Room.md&1.7&
ais09-Chinese-Room.md&1.8&##  French: subcongnitive processes
ais09-Chinese-Room.md&1.8&
ais09-Chinese-Room.md&1.8&>- We are able to do a lot of processing machines cannot do:
ais09-Chinese-Room.md&1.8&    - Find the plurals of “platch” and “snorp”!
ais09-Chinese-Room.md&1.8&    - Decide whether “flugly” would be a good name for a male action star in a fighting role, a sexy female movie star, or a cute stuffed animal
ais09-Chinese-Room.md&1.8&	- Using such questions in a Turing Test will immediately uncover the machine!
ais09-Chinese-Room.md&1.8&>- These abilities don't come from conscious, symbolic processing of information.
ais09-Chinese-Room.md&1.8&>- Rather, they are culturally developed, or taught to us while we are children.
ais09-Chinese-Room.md&1.8&>- French calls these "subcognitive" processes, because they happen below the conscious cognitive level.
ais09-Chinese-Room.md&1.8&
ais09-Chinese-Room.md&1.9&##  French: subcongnitive processes
ais09-Chinese-Room.md&1.9&
ais09-Chinese-Room.md&1.9&>- There are more subcognitive processes that can be used to uncover who is the machine in a Turing Test:
ais09-Chinese-Room.md&1.9&>- We know that humans *recognise words* much faster in context:
ais09-Chinese-Room.md&1.9&>- In: "car, love, philosophy, pepper, rain" the recognition of "pepper" will be much slower than in: "sugar, salt, pepper, cumin."
ais09-Chinese-Room.md&1.9&>- The delay is measurable experimentally, and can be used to detect the human partner in a Turing Test.
ais09-Chinese-Room.md&1.9&
ais09-Chinese-Room.md&1.10&## French: rating games
ais09-Chinese-Room.md&1.10&
ais09-Chinese-Room.md&1.10&>- “Rate A as B!”
ais09-Chinese-Room.md&1.10&>- Rate dry leaves as hiding places!
ais09-Chinese-Room.md&1.10&>- Rate toy airplanes as airplanes!
ais09-Chinese-Room.md&1.10&>- Rate broomsticks, keys, bananas and pillows as weapons!
ais09-Chinese-Room.md&1.10&>- These questions are very hard for a machine to answer.
ais09-Chinese-Room.md&1.10&>- CYC or similar common sense systems *might* be able to handle some of them.
ais09-Chinese-Room.md&1.10&
ais09-Chinese-Room.md&1.11&## French: Conclusion
ais09-Chinese-Room.md&1.11&
ais09-Chinese-Room.md&1.11&>- If I want to still have a machine pass the Turing Test, I would have to program it to imitate the subcognitive patterns of humans.
ais09-Chinese-Room.md&1.11&>- This is very probably impossible, since these patterns arise from the specific biological  or functional organization of the human body.
ais09-Chinese-Room.md&1.11&>- **The physical level and the cognitive level of intelligence are inseparable.**
ais09-Chinese-Room.md&1.11&>- A computer will never be able to pass a Turing Test.
ais09-Chinese-Room.md&1.11&>- It will always be possible to uncover it using questions which can only be anwered by having lived a typical human life in a human body.
ais09-Chinese-Room.md&1.11&
ais09-Chinese-Room.md&2.0&# Variants of the Turing Test
ais09-Chinese-Room.md&2.0&
ais09-Chinese-Room.md&2.1&## Turing test variants
ais09-Chinese-Room.md&2.1&
ais09-Chinese-Room.md&2.1&>- If the Turing Test has all these problems, is there a way how we could try to improve it? Another test for intelligence that would be better and more accurate?
ais09-Chinese-Room.md&2.1&
ais09-Chinese-Room.md&2.1&
ais09-Chinese-Room.md&2.1&
ais09-Chinese-Room.md&2.2&## The Kugel Test
ais09-Chinese-Room.md&2.2&
ais09-Chinese-Room.md&2.2&![](graphics/04-kugel-test.jpg) \
ais09-Chinese-Room.md&2.2&
ais09-Chinese-Room.md&2.2&
ais09-Chinese-Room.md&2.2&
ais09-Chinese-Room.md&2.2&>- The Kugel Test attempts to be a version of the Turing Test which rules out FSM's and essentially dumb programs like Alice.
ais09-Chinese-Room.md&2.2&>- A judge thinks of a concept.
ais09-Chinese-Room.md&2.2&
ais09-Chinese-Room.md&2.2&
ais09-Chinese-Room.md&2.2&
ais09-Chinese-Room.md&2.3&## The Kugel Test
ais09-Chinese-Room.md&2.3&
ais09-Chinese-Room.md&2.3&![](graphics/04-kugel-test.jpg)\ 
ais09-Chinese-Room.md&2.3&
ais09-Chinese-Room.md&2.3&
ais09-Chinese-Room.md&2.3&
ais09-Chinese-Room.md&2.3&>- He throws picture cards into one of two bins, depending on whether they fit the chosen concept (“yes/no” bins).
ais09-Chinese-Room.md&2.3&>- The candidate has to guess which bin the next card is going to be thrown into.
ais09-Chinese-Room.md&2.3&
ais09-Chinese-Room.md&2.3&
ais09-Chinese-Room.md&2.3&
ais09-Chinese-Room.md&2.4&## The Kugel Test
ais09-Chinese-Room.md&2.4&
ais09-Chinese-Room.md&2.4&![](graphics/04-kugel-test.jpg)\ 
ais09-Chinese-Room.md&2.4&
ais09-Chinese-Room.md&2.4&
ais09-Chinese-Room.md&2.4&
ais09-Chinese-Room.md&2.4&>- The test is run infinitely long.
ais09-Chinese-Room.md&2.4&
ais09-Chinese-Room.md&2.5&## The Kugel Test
ais09-Chinese-Room.md&2.5&
ais09-Chinese-Room.md&2.5&>- *Is this a good test for machine intelligence?*
ais09-Chinese-Room.md&2.5&>- Advantages:
ais09-Chinese-Room.md&2.5&>     - It does not use language, so people who speak another language would not fail.
ais09-Chinese-Room.md&2.5&>     - It uses abstract classes to judge intelligence. Abstraction and categorisation is a core “intelligent” task that is hard to fake (as opposed to generating correct language). 
ais09-Chinese-Room.md&2.5&>     - Each picture can fall into multiple categories, e.g. a bee can denote “insect,” “flying thing,” “yellow/black small object,” “dangerous animal” and so on. So there really *is* some intelligence necessary to get the right category depending on the other pictures seen previously.
ais09-Chinese-Room.md&2.5&>     - Consider also that the pictures can be photographs, drawings, children’s-book style drawings, or van-Gogh style images of things. To get the right identification for each of the different ways of depicting something is, in itself, a difficult and “intelligent” task of image content recognition.
ais09-Chinese-Room.md&2.5&
ais09-Chinese-Room.md&2.6&## The Kugel Test
ais09-Chinese-Room.md&2.6&
ais09-Chinese-Room.md&2.6&>- Would this be easy to pass?
ais09-Chinese-Room.md&2.6&    - By a human?
ais09-Chinese-Room.md&2.6&	- By a machine?
ais09-Chinese-Room.md&2.6&>- What about private concepts? (Things I saw last week)
ais09-Chinese-Room.md&2.6&>- Is this still an *intelligence* test?
ais09-Chinese-Room.md&2.6&>- Humans would also fail, depending on knowledge, cultural background, and so on.
ais09-Chinese-Room.md&2.6&>- Perhaps: The machine should be allowed to fail where humans also fail.
ais09-Chinese-Room.md&2.6&
ais09-Chinese-Room.md&2.7&## Schweizer's TRTTT: Tokens and types
ais09-Chinese-Room.md&2.7&
ais09-Chinese-Room.md&2.7&>- Schweizer realised that we shouldn't attempt to answer the question whether a random thing is intelligent.
ais09-Chinese-Room.md&2.7&>- Rather, we must distinguish between *kinds of things* that are intelligent, and kinds of things that aren't. A Turing Test only makes sense if I perform it on the kind of thing that has a chance of being intelligent.
ais09-Chinese-Room.md&2.7&>- Schweizer (1998): Truly Total Turing Test:
ais09-Chinese-Room.md&2.7&>- First, a species in question must pass a test as group, that is, they must show that they are able not only to play chess, but to *invent* the game of chess.
ais09-Chinese-Room.md&2.7&>- Second, after this has been achieved, they can be assumed to be intelligent as a *type* (kind), and then the individuals can be tested by token tests (like the Turing Test).
ais09-Chinese-Room.md&2.7&
ais09-Chinese-Room.md&3.0&# The Chinese Room argument
ais09-Chinese-Room.md&3.0&
ais09-Chinese-Room.md&3.1&## Overview
ais09-Chinese-Room.md&3.1&
ais09-Chinese-Room.md&3.1&John Searle's Chinese Room argument, presented in 1980, attempted to show that a program (or any physical symbol system) could not be said to "understand" the symbols that it uses; that the symbols have no meaning for the machine, and so the machine can never be *truly* intelligent (or be said to "have a mind").
ais09-Chinese-Room.md&3.1&
ais09-Chinese-Room.md&3.2&## Searle's argument (1)
ais09-Chinese-Room.md&3.2&
ais09-Chinese-Room.md&3.2&> "Suppose that I’m locked in a room and given a large batch of Chinese writing. Suppose furthermore (as is indeed the case) that I know no Chinese, either written or spoken, and that I’m not even confident that I could recognize Chinese writing as Chinese writing distinct from, say, Japanese writing or meaningless squiggles. To me, Chinese writing is just so many meaningless squiggles. Now suppose further that after this first batch of Chinese writing I am given a second batch of Chinese script together with a set of rules for correlating the second batch with the first batch. The rules are in English, and I understand these rules as well as any other native speaker of English. They enable me to correlate one set of formal symbols with another set of formal symbols, and all that “formal” means here is that I can identify the symbols entirely by their shapes."
ais09-Chinese-Room.md&3.2&
ais09-Chinese-Room.md&3.3&## Searle's argument (2)
ais09-Chinese-Room.md&3.3&
ais09-Chinese-Room.md&3.3&> "Now suppose also that I am given a third batch of Chinese symbols together with some instructions, again in English, that enable me to correlate elements of this third batch with the first two batches, and these rules instruct me how to give back certain Chinese symbols with certain sorts of shapes in response to certain sorts of shapes given me in the third batch. Unknown to me, the people who are giving me all of these symbols call the first batch 'a script,' they call the second batch a 'story,' and they call the third batch 'questions.' Furthermore, they call the symbols I give them back in response to the third batch 'answers to the questions,' and the set of rules in English that they gave me, they call 'the program.'"
ais09-Chinese-Room.md&3.3&
ais09-Chinese-Room.md&3.4&## Searle's argument (3)
ais09-Chinese-Room.md&3.4&
ais09-Chinese-Room.md&3.4&> "Suppose also that after a while I get so good at following the instructions for manipulating the Chinese symbols and the programmers get so good at writing the programs that from the external points of view – that is, from the point of view of somebody outside the room in which I am locked – my answers to the questions are absolutely indistinguishable from those of native Chinese speakers." 
ais09-Chinese-Room.md&3.4&
ais09-Chinese-Room.md&3.5&## Searle's argument (4)
ais09-Chinese-Room.md&3.5&
ais09-Chinese-Room.md&3.5&> "As regards the [claims of strong AI], it seems to me quite obvious in the example that I do not understand a word of the Chinese stories. I have inputs and outputs that are indistinguishable from those of the native Chinese speaker, and I can have any formal program you like, but I still understand nothing. For the same reasons, Schank’s computer understands nothing of any stories, whether in Chinese, English, or whatever, since in the Chinese case the computer is me, and in cases where the computer is not me, the computer has nothing more than I have in the case where I understand nothing."
ais09-Chinese-Room.md&3.5&
ais09-Chinese-Room.md&4.0&# Chinese Room: Criticisms and replies
ais09-Chinese-Room.md&4.0&
ais09-Chinese-Room.md&4.1&## System reply
ais09-Chinese-Room.md&4.1&
ais09-Chinese-Room.md&4.1&>- While Searle does not understand Chinese, the *system* (composed of person, rule-book, and cards) does.
ais09-Chinese-Room.md&4.1&>- Just because *a part of the system* (Searle) does not understand Chinese, we cannot conclude that the whole system does not.
ais09-Chinese-Room.md&4.1&>- More generally, this argument is a fallacy: **A system that has an ability X does not need to contain subsystems that have the same ability X!**
ais09-Chinese-Room.md&4.1&>- If this was the case, the chain of subsystems with the ability X would never end!
ais09-Chinese-Room.md&4.1&>- Consider: the human brain can "understand" (we assume). Still, individual neurons in our brain clearly do not understand. We cannot conclude that humans don't understand because their neurons don't!
ais09-Chinese-Room.md&4.1&
ais09-Chinese-Room.md&4.2&## System reply
ais09-Chinese-Room.md&4.2&
ais09-Chinese-Room.md&4.2&>- *What could Searle answer to that?*
ais09-Chinese-Room.md&4.2&>- Searle: Let me memorize the rule book and take away the room. Now all of the system is in me. But I still do not understand Chinese, so the system does not understand Chinese.
ais09-Chinese-Room.md&4.2&>- *Is this a good answer?*
ais09-Chinese-Room.md&4.2&>- Still the same problem.
ais09-Chinese-Room.md&4.2&>- Also, the rule-book is where the understanding of the Chinese Room comes from. The original experiment smuggles meaning into the room in the form of the (human-written) rule-book!
ais09-Chinese-Room.md&4.2&>- Like with Purtill's criticism of the Turing Test, we could say that the Chinese Room is a "battle of wits" between the people outside the room and the writer of the rule-book. The person inside the room is not important.
ais09-Chinese-Room.md&4.2&
ais09-Chinese-Room.md&4.3&## Robot system reply (Harnad)
ais09-Chinese-Room.md&4.3&
ais09-Chinese-Room.md&4.3&>- We must distinguish c-implementation (computer) and p-implementation (physical device).
ais09-Chinese-Room.md&4.3&>- Accordingly, we must distinguish symbolic functionalism from robotic functionalism.
ais09-Chinese-Room.md&4.3&>- Symbolic functionalism:
ais09-Chinese-Room.md&4.3&    - “The belief that mental function is really just symbolic (e.g. verbal, inferential, computational) function;
ais09-Chinese-Room.md&4.3&	- that the mind manipulates symbols the way a Turing machine does,
ais09-Chinese-Room.md&4.3&	- and hence that the brain just supports the hardware for doing computation.
ais09-Chinese-Room.md&4.3&>- Robotic functionalism:
ais09-Chinese-Room.md&4.3&    - “Includes non-symbolic functions, for example sensors and actuators.”
ais09-Chinese-Room.md&4.3&
ais09-Chinese-Room.md&4.4&## C-implementation and p-implementation
ais09-Chinese-Room.md&4.4&
ais09-Chinese-Room.md&4.4&*Give an example where we see the difference between c- and p-implementation!*
ais09-Chinese-Room.md&4.4&
ais09-Chinese-Room.md&4.4&(For example, the implementation of an "airplane.")
ais09-Chinese-Room.md&4.4&
ais09-Chinese-Room.md&4.4&. . . 
ais09-Chinese-Room.md&4.4&
ais09-Chinese-Room.md&4.4&- C-implementation of an airplane:  a simulation of an airplane inside a computer.
ais09-Chinese-Room.md&4.4&- P- implementation: building a physical model of an airplane, or building an actual airplane that flies.
ais09-Chinese-Room.md&4.4&
ais09-Chinese-Room.md&4.4&. . . 
ais09-Chinese-Room.md&4.4&
ais09-Chinese-Room.md&4.4&Harnad: a c-implementation of a robot functionalist device will not have a mind. A p-implementation will.
ais09-Chinese-Room.md&4.4&
ais09-Chinese-Room.md&4.4&*Is this plausible? Why would this be so?*
ais09-Chinese-Room.md&4.4&
ais09-Chinese-Room.md&4.4&. . .
ais09-Chinese-Room.md&4.4&
ais09-Chinese-Room.md&4.4&(Because of symbol grounding. But is symbol grounding sufficient for having a mind???)
ais09-Chinese-Room.md&4.4&
ais09-Chinese-Room.md&4.5&## Mooney’s counterargument to robotic symbol grounding
ais09-Chinese-Room.md&4.5&
ais09-Chinese-Room.md&4.5&>- Assume I send a robot to Mars.
ais09-Chinese-Room.md&4.5&>- It has direct sensory access, so it achieves a "mind."
ais09-Chinese-Room.md&4.5&>- Now I bring it back to earth and disconnect the sensors, and instead I connect a computer terminal.
ais09-Chinese-Room.md&4.5&>- Now I can still talk to the robot, but suddenly its “mind” is gone.
ais09-Chinese-Room.md&4.5&>- If I reattach the wheels and sensors, the “mind” comes back, although all the time I can continue having the same typed conversation with it. Essentially I can switch the mind on and off by removing and attaching the sensors and wheels.
ais09-Chinese-Room.md&4.5&>- *Possible reply to that?*
ais09-Chinese-Room.md&4.5&
ais09-Chinese-Room.md&4.6&## Reply to Mooney's counterargument
ais09-Chinese-Room.md&4.6&
ais09-Chinese-Room.md&4.6&>- The mind must not necessarily *disappear* if I remove the sensors.
ais09-Chinese-Room.md&4.6&>- Only the ability of the system to produce *new* symbol groundings will be impaired.
ais09-Chinese-Room.md&4.6&>- The already used symbols can be used further (like when someone with acquired blindness can speak meaningfully about remembered colours!)
ais09-Chinese-Room.md&4.6&>- (You see: Sometimes even great philosophers make bad arguments.)
ais09-Chinese-Room.md&4.6&
ais09-Chinese-Room.md&4.7&## Combined systems/robot reply
ais09-Chinese-Room.md&4.7&
ais09-Chinese-Room.md&4.7&>- Perhaps a combined robot/systems reply is best choice:
ais09-Chinese-Room.md&4.7&>- The system as a whole, including its sensors (which are required for symbol grounding),  and its environment (should this be mentioned here? how great is the contribution of the environment on a system's intelligence?), understands, even if Searle-in-the-room does not.
ais09-Chinese-Room.md&4.7&>- More pointedly, it is completely irrelevant whether Searle-in-the-room understands Chinese or not! (As it is irrelevant whether our neurons individually understand anything.)
ais09-Chinese-Room.md&4.7&
ais09-Chinese-Room.md&4.8&## Robots and cognition
ais09-Chinese-Room.md&4.8&
ais09-Chinese-Room.md&4.8&>- This brings us to a more general point about the importance of robots in AI.
ais09-Chinese-Room.md&4.8&>- Robots are not just “computers on wheels.”
ais09-Chinese-Room.md&4.8&>- A robot is an entirely different thing from a purely symbolic computer, because its sensory and motor equipment gives it the ability to acquire genuine “meaning” (the corresponding sensory impressions for each symbol in its database), so that it can escape the problems of the Turing Test and the Chinese room.
ais09-Chinese-Room.md&4.8&>- A robot could, conceivably, answer some of French’s subcognitive questions (“rate leaves as hiding places”) correctly from actual experience; a Prolog-based program could never do so from own experience.
ais09-Chinese-Room.md&4.8&>- This is why, from the next session on, we will talk about AI systems with actual bodies, sensors and actuators, that are more likely to exhibit “genuine” intelligence.
ais09-Chinese-Room.md&4.8&
ais09-Chinese-Room.md&4.8&
ais09-Chinese-Room.md&4.8&
ais09-Chinese-Room.md&4.8&
ais09-Chinese-Room.md&4.8&
ais10-GOFAI-Nouvelle-AI.md&0.1&---
ais10-GOFAI-Nouvelle-AI.md&0.1&title:  "AI and Society: 10. Subsymbolic AI - Braitenberg vehicles and Nouvelle AI"
ais10-GOFAI-Nouvelle-AI.md&0.1&author: Andreas Matthias, Lingnan University
ais10-GOFAI-Nouvelle-AI.md&0.1&date: September 30, 2019
ais10-GOFAI-Nouvelle-AI.md&0.1&...
ais10-GOFAI-Nouvelle-AI.md&0.1&
ais10-GOFAI-Nouvelle-AI.md&1.0&# Intelligence Without Representation
ais10-GOFAI-Nouvelle-AI.md&1.0&
ais10-GOFAI-Nouvelle-AI.md&1.1&## Symbolic AI and subsymbolic AI
ais10-GOFAI-Nouvelle-AI.md&1.1&
ais10-GOFAI-Nouvelle-AI.md&1.1&>- As opposed to symbolic AI, in *connectionism* (or subsymbolic AI) we don't have explicit symbols which stand for discrete meanings.
ais10-GOFAI-Nouvelle-AI.md&1.1&>- In brains, we don't find symbols. Instead, we have a network of neurons (nerve cells), which send electrical impulses to each other.
ais10-GOFAI-Nouvelle-AI.md&1.1&>- On an even more basic level, even complex *behaviour* doesn't need to be symbolically encoded!
ais10-GOFAI-Nouvelle-AI.md&1.1&
ais10-GOFAI-Nouvelle-AI.md&1.2&## Braitenberg vehicles
ais10-GOFAI-Nouvelle-AI.md&1.2&
ais10-GOFAI-Nouvelle-AI.md&1.2&>- Valentino Braitenberg (1926--2011): Neuroscientist and cyberneticist. Book: "Vehicles: Experiments in Synthetic Psychology."
ais10-GOFAI-Nouvelle-AI.md&1.2&>- Braitenberg's point was that even a very simple system can exhibit quite complex behaviours. Complex behaviour does not need symbolic representation!
ais10-GOFAI-Nouvelle-AI.md&1.2&
ais10-GOFAI-Nouvelle-AI.md&1.3&## Braitenberg vehicle 1
ais10-GOFAI-Nouvelle-AI.md&1.3&
ais10-GOFAI-Nouvelle-AI.md&1.3&What will this vehicle do?
ais10-GOFAI-Nouvelle-AI.md&1.3&
ais10-GOFAI-Nouvelle-AI.md&1.3&![](graphics/05-bv-01-small.jpg)\ 
ais10-GOFAI-Nouvelle-AI.md&1.3&
ais10-GOFAI-Nouvelle-AI.md&1.3&. . .
ais10-GOFAI-Nouvelle-AI.md&1.3&
ais10-GOFAI-Nouvelle-AI.md&1.3&"Approach."
ais10-GOFAI-Nouvelle-AI.md&1.3&
ais10-GOFAI-Nouvelle-AI.md&1.4&## Braitenberg vehicle 2
ais10-GOFAI-Nouvelle-AI.md&1.4&
ais10-GOFAI-Nouvelle-AI.md&1.4&What will these vehicles do?
ais10-GOFAI-Nouvelle-AI.md&1.4&
ais10-GOFAI-Nouvelle-AI.md&1.4&![](graphics/05-bv-02-small.jpg)\ 
ais10-GOFAI-Nouvelle-AI.md&1.4&
ais10-GOFAI-Nouvelle-AI.md&1.4&. . .
ais10-GOFAI-Nouvelle-AI.md&1.4&
ais10-GOFAI-Nouvelle-AI.md&1.4&"Fear." -- "Aggression."
ais10-GOFAI-Nouvelle-AI.md&1.4&
ais10-GOFAI-Nouvelle-AI.md&1.5&## Braitenberg vehicle 2
ais10-GOFAI-Nouvelle-AI.md&1.5&
ais10-GOFAI-Nouvelle-AI.md&1.5&![](graphics/05-bv-04-small.jpg)\ 
ais10-GOFAI-Nouvelle-AI.md&1.5&
ais10-GOFAI-Nouvelle-AI.md&1.5&Braitenberg: "Let Vehicles 2a and 2b move around in their world for a while and watch them. Their characters are quite opposite. Both *dislike* sources. But 2a becomes restless in their vicinity and tends to avoid them, escaping until it safely reaches a place where the influence of the source is scarcely felt. Vehicle 2a is a *coward*, you would say.
ais10-GOFAI-Nouvelle-AI.md&1.5&
ais10-GOFAI-Nouvelle-AI.md&1.5&Not so Vehicle 2b. It, too, is excited by the presence of sources, but resolutely turns toward them and hits them with high velocity, as if it wanted to destroy them. Vehicle 2b is *aggressive*, obviously."
ais10-GOFAI-Nouvelle-AI.md&1.5&
ais10-GOFAI-Nouvelle-AI.md&1.6&## Braitenberg vehicle 3
ais10-GOFAI-Nouvelle-AI.md&1.6&
ais10-GOFAI-Nouvelle-AI.md&1.6&Now imagine that we replace the connection between sensors and motor so that the sensors *inhibit* the motor.
ais10-GOFAI-Nouvelle-AI.md&1.6&
ais10-GOFAI-Nouvelle-AI.md&1.6&What will these vehicles do?
ais10-GOFAI-Nouvelle-AI.md&1.6&
ais10-GOFAI-Nouvelle-AI.md&1.6&![](graphics/05-bv-02-small.jpg)\ 
ais10-GOFAI-Nouvelle-AI.md&1.6&
ais10-GOFAI-Nouvelle-AI.md&1.6&
ais10-GOFAI-Nouvelle-AI.md&1.7&## Love and exploration
ais10-GOFAI-Nouvelle-AI.md&1.7&
ais10-GOFAI-Nouvelle-AI.md&1.7&![](graphics/05-bv-05-small.jpg)\ 
ais10-GOFAI-Nouvelle-AI.md&1.7&
ais10-GOFAI-Nouvelle-AI.md&1.7&Braitenberg: "You will have no difficulty giving names to this sort of behavior. These vehicles *like* the source, you will say, but in different ways. Vehicle 3a *loves* it in a permanent way, staying close by in quiet admiration from the time it spots the source to all future time. Vehicle 3b, on the other hand, is an *explorer*. It likes the nearby source all right, but keeps an eye open for other, perhaps stronger sources."
ais10-GOFAI-Nouvelle-AI.md&1.7&
ais10-GOFAI-Nouvelle-AI.md&1.8&## More complex vehicles
ais10-GOFAI-Nouvelle-AI.md&1.8&
ais10-GOFAI-Nouvelle-AI.md&1.8&"[Add] not just one pair of sensors but four pairs, tuned to different qualities of the environment, say light, temperature, oxygen concentration, and amount of organic matter."
ais10-GOFAI-Nouvelle-AI.md&1.8&
ais10-GOFAI-Nouvelle-AI.md&1.8&. . . 
ais10-GOFAI-Nouvelle-AI.md&1.8&
ais10-GOFAI-Nouvelle-AI.md&1.8&Expected behavior:
ais10-GOFAI-Nouvelle-AI.md&1.8&
ais10-GOFAI-Nouvelle-AI.md&1.8&Given appropriate connections, "this is a vehicle with really interesting behavior. It dislikes high temperature, turns away from hot places, and at the same time seems to dislike light bulbs with even greater passion, since it turns towards them and destroys them... You cannot help admitting that Vehicle 3c has a system of *values*, and, come to think of it, *knowledge*." (Braitenberg)
ais10-GOFAI-Nouvelle-AI.md&1.8&
ais10-GOFAI-Nouvelle-AI.md&1.9&## Real Braitenberg vehicles
ais10-GOFAI-Nouvelle-AI.md&1.9&
ais10-GOFAI-Nouvelle-AI.md&1.9&A simulator:
ais10-GOFAI-Nouvelle-AI.md&1.9&
ais10-GOFAI-Nouvelle-AI.md&1.9&<http://www.harmendeweerd.nl/braitenberg-vehicles/>
ais10-GOFAI-Nouvelle-AI.md&1.9&
ais10-GOFAI-Nouvelle-AI.md&1.9&A video:
ais10-GOFAI-Nouvelle-AI.md&1.9&
ais10-GOFAI-Nouvelle-AI.md&1.9&<https://www.youtube.com/watch?v=yUVcI5Pw2o4>
ais10-GOFAI-Nouvelle-AI.md&1.9&
ais10-GOFAI-Nouvelle-AI.md&1.9&A longer video, with very detailed explanations of many types of Braitenberg vehicles:
ais10-GOFAI-Nouvelle-AI.md&1.9&
ais10-GOFAI-Nouvelle-AI.md&1.9&<https://www.youtube.com/watch?v=A-fxij3zM7g>
ais10-GOFAI-Nouvelle-AI.md&1.9&
ais10-GOFAI-Nouvelle-AI.md&1.9&
ais10-GOFAI-Nouvelle-AI.md&1.10&## Braitenberg vehicles and AI
ais10-GOFAI-Nouvelle-AI.md&1.10&
ais10-GOFAI-Nouvelle-AI.md&1.10&>- *What are the advantages of (complex) Braitenberg vehicles over symbolic AI systems?*
ais10-GOFAI-Nouvelle-AI.md&1.10&>- “A brilliant chess move while the room is filling with smoke because the house is burning down does not show intelligence.” (Anatol Holt, 1974, unpublished; cited by Winograd). Braitenberg vehicles react to their environment. They don't have *gaps of anticipation.*
ais10-GOFAI-Nouvelle-AI.md&1.10&>- They are stable and reliable, not brittle. They won't stop working if a rule is missing.
ais10-GOFAI-Nouvelle-AI.md&1.10&>- Imagine, for example, that you try to confuse Braitenberg vehicles with multiple light sources, or that you put obstacles in their way.
ais10-GOFAI-Nouvelle-AI.md&1.10&>- They might not behave *optimally,* but they will do *something* rather than just freeze or crash.
ais10-GOFAI-Nouvelle-AI.md&1.10&>- This is called *graceful degradation* (discussed below).
ais10-GOFAI-Nouvelle-AI.md&1.10&>- Much faster reactions, because no symbolic processing is needed.
ais10-GOFAI-Nouvelle-AI.md&1.10&
ais10-GOFAI-Nouvelle-AI.md&1.10&
ais10-GOFAI-Nouvelle-AI.md&1.11&## Rodney Brooks: Symbolic and subsymbolic representation
ais10-GOFAI-Nouvelle-AI.md&1.11&
ais10-GOFAI-Nouvelle-AI.md&1.11&>- In the sixties and seventies, several laboratories attempted to build robots that used symbolic AI to represent the world and plan actions. These projects had limited success.
ais10-GOFAI-Nouvelle-AI.md&1.11&>- In the eighties, Rodney Brooks of MIT was able to build robots that had superior ability to move and execute tasks without the use of symbolic reasoning at all.
ais10-GOFAI-Nouvelle-AI.md&1.11&>- Brooks (and others) demonstrated that our most basic skills of motion, survival, perception, balance and so on did not seem to require high level symbols at all.
ais10-GOFAI-Nouvelle-AI.md&1.11&>- He said that he was inspired to create a new robotic architecture when he observed the movement of insects, who can survive and thrive in a real, complex environment, despite having only minimal nervous systems.
ais10-GOFAI-Nouvelle-AI.md&1.11&
ais10-GOFAI-Nouvelle-AI.md&1.12&## Coke can collection
ais10-GOFAI-Nouvelle-AI.md&1.12&
ais10-GOFAI-Nouvelle-AI.md&1.12&Task: Program a robot to walk through offices and collect empty Coke cans.
ais10-GOFAI-Nouvelle-AI.md&1.12&
ais10-GOFAI-Nouvelle-AI.md&1.12&. . . 
ais10-GOFAI-Nouvelle-AI.md&1.12&
ais10-GOFAI-Nouvelle-AI.md&1.12&Symbolic version:
ais10-GOFAI-Nouvelle-AI.md&1.12&
ais10-GOFAI-Nouvelle-AI.md&1.12&>- Prepare a map of the office space, including desks, tables, chairs (but these are moving?!)
ais10-GOFAI-Nouvelle-AI.md&1.12&>- Give a symbolic description of a coke can.
ais10-GOFAI-Nouvelle-AI.md&1.12&>- Write a program to make the robot walk through the office space and look for Coke cans.
ais10-GOFAI-Nouvelle-AI.md&1.12&>- Consider:
ais10-GOFAI-Nouvelle-AI.md&1.12&    - Collisions with people,
ais10-GOFAI-Nouvelle-AI.md&1.12&	- with furniture,
ais10-GOFAI-Nouvelle-AI.md&1.12&	- different views of a coke can in different perspectives,
ais10-GOFAI-Nouvelle-AI.md&1.12&	- unexpected obstacles (locked doors, things left lying around),
ais10-GOFAI-Nouvelle-AI.md&1.12&	- other “Coke-can-like” objects, ...
ais10-GOFAI-Nouvelle-AI.md&1.12&>- The task seems almost impossible!
ais10-GOFAI-Nouvelle-AI.md&1.12&
ais10-GOFAI-Nouvelle-AI.md&1.13&## Brooks: "Subsumption architecture" (1)
ais10-GOFAI-Nouvelle-AI.md&1.13&
ais10-GOFAI-Nouvelle-AI.md&1.13&>- Build systems from the bottom up, in independent layers.
ais10-GOFAI-Nouvelle-AI.md&1.13&>- Each layer directly connects perception to action.
ais10-GOFAI-Nouvelle-AI.md&1.13&>- The lowest layer is just concerned with moving the robot around.
ais10-GOFAI-Nouvelle-AI.md&1.13&>- A layer on top of that avoids obstacles when moving.
ais10-GOFAI-Nouvelle-AI.md&1.13&>- The next layer plans where the robot should go. It knows nothing of actually moving the robot or avoiding obstacles (this is done by the lower layer!)
ais10-GOFAI-Nouvelle-AI.md&1.13&>- An independent low layer recognizes objects in field of vision.
ais10-GOFAI-Nouvelle-AI.md&1.13&>- Another layer on top of that identifies Coke cans.
ais10-GOFAI-Nouvelle-AI.md&1.13&>- A still higher layer makes the robot move close to the Coke cans.
ais10-GOFAI-Nouvelle-AI.md&1.13&
ais10-GOFAI-Nouvelle-AI.md&1.14&## Brooks: "Subsumption architecture" (2)
ais10-GOFAI-Nouvelle-AI.md&1.14&
ais10-GOFAI-Nouvelle-AI.md&1.14&>- This was particularly beneficial at a time when computers were slow and it was difficult to process complex environments symbolically (for example to store huge, detailed and changing maps of the environment).
ais10-GOFAI-Nouvelle-AI.md&1.14&>- A subsumption architecture robot does not store any information about the environment. Instead, it *senses* the environment itself!
ais10-GOFAI-Nouvelle-AI.md&1.14&>- In this way, every environment is its own map and does not consume computational or storage resources of the robot.
ais10-GOFAI-Nouvelle-AI.md&1.14&
ais10-GOFAI-Nouvelle-AI.md&1.15&## Subsumption architecture
ais10-GOFAI-Nouvelle-AI.md&1.15&
ais10-GOFAI-Nouvelle-AI.md&1.15&<https://www.youtube.com/watch?v=YtNKuwiVYm0>
ais10-GOFAI-Nouvelle-AI.md&1.15&
ais10-GOFAI-Nouvelle-AI.md&1.15&Another example:
ais10-GOFAI-Nouvelle-AI.md&1.15&
ais10-GOFAI-Nouvelle-AI.md&1.15&![](graphics/05-ssa.jpg)\ 
ais10-GOFAI-Nouvelle-AI.md&1.15&
ais10-GOFAI-Nouvelle-AI.md&1.16&## Advantages of subsumption architectures
ais10-GOFAI-Nouvelle-AI.md&1.16&
ais10-GOFAI-Nouvelle-AI.md&1.16&>- Low processing overhead.
ais10-GOFAI-Nouvelle-AI.md&1.16&>- Tight coupling between perception and action.
ais10-GOFAI-Nouvelle-AI.md&1.16&>- Modular structure.
ais10-GOFAI-Nouvelle-AI.md&1.16&>- Rapid, reflexive responses.
ais10-GOFAI-Nouvelle-AI.md&1.16&>- Long-term and short-term goals.
ais10-GOFAI-Nouvelle-AI.md&1.16&>- Easy to construct relatively complex systems.
ais10-GOFAI-Nouvelle-AI.md&1.16&>- Systems tend to work well in the real world, particularly in changing environments and on slow computers.
ais10-GOFAI-Nouvelle-AI.md&1.16&
ais10-GOFAI-Nouvelle-AI.md&1.17&## Drawbacks of subsumption architectures
ais10-GOFAI-Nouvelle-AI.md&1.17&
ais10-GOFAI-Nouvelle-AI.md&1.17&>- Difficult to extend beyond controlling simple creatures.
ais10-GOFAI-Nouvelle-AI.md&1.17&>- Hard to do:
ais10-GOFAI-Nouvelle-AI.md&1.17&    - Simultaneous (parallel) goals and complex goal structures.
ais10-GOFAI-Nouvelle-AI.md&1.17&    - Planning and control of complex actions in multiple layers.
ais10-GOFAI-Nouvelle-AI.md&1.17&>- The original coke-can collecting robot could not easily find its way back to its home position, because it did not have a map.
ais10-GOFAI-Nouvelle-AI.md&1.17&>- In order to find back, it always went through doors in a north direction, so that it could always go back by travelling south. But this seems to be a pretty inefficient way to navigate.
ais10-GOFAI-Nouvelle-AI.md&1.17&>- Nowadays, with faster computers, we can combine symbolic and subsymbolic approaches to achieve better performance.
ais10-GOFAI-Nouvelle-AI.md&1.17&
ais10-GOFAI-Nouvelle-AI.md&1.18&## Rodney Brooks: Symbolic and subsymbolic representation
ais10-GOFAI-Nouvelle-AI.md&1.18&
ais10-GOFAI-Nouvelle-AI.md&1.18&Rodney Brooks, *Elephants Don't Play Chess* (1990):
ais10-GOFAI-Nouvelle-AI.md&1.18&
ais10-GOFAI-Nouvelle-AI.md&1.18&>- AI cannot be made in a symbolic way.
ais10-GOFAI-Nouvelle-AI.md&1.18&>- Symbolic processing is not necessary or sufficient for AI.
ais10-GOFAI-Nouvelle-AI.md&1.18&>- Robots should not make an internal symbolic model of the world.
ais10-GOFAI-Nouvelle-AI.md&1.18&>- Successful AI must be situated in an environment. This solves the symbol grounding problem, too!
ais10-GOFAI-Nouvelle-AI.md&1.18&>- The *world itself* "is its own best model. It is always exactly up to date. It always has every detail there is to be known. The trick is to sense it appropriately and often enough."
ais10-GOFAI-Nouvelle-AI.md&1.18&>- Elephants are "intelligent" in their behaviour (in the sense that their behaviour is adapted to their survival needs), but they don't need to "play chess" to prove it (that is, they don't need to do symbolic processing).
ais10-GOFAI-Nouvelle-AI.md&1.18&
ais10-GOFAI-Nouvelle-AI.md&1.19&## Symbolic and subsymbolic processing
ais10-GOFAI-Nouvelle-AI.md&1.19&
ais10-GOFAI-Nouvelle-AI.md&1.19&>- Such architectures are called "subsymbolic" because they operate on a level "below" the symbolic level.
ais10-GOFAI-Nouvelle-AI.md&1.19&>- Neural networks (discussed a little later) are another example of subsymbolic AI.
ais10-GOFAI-Nouvelle-AI.md&1.19&>- If you think about it, human performance is a mixture of symbolic and non-symbolic elements:
ais10-GOFAI-Nouvelle-AI.md&1.19&>     - Mathematics, logic, chess and language are symbolic abilities.
ais10-GOFAI-Nouvelle-AI.md&1.19&>     - Digestion, heartbeat, walking, avoiding obstacles are subsymbolic skills.
ais10-GOFAI-Nouvelle-AI.md&1.19&>     - Many skills (e.g. driving a car, playing chess on a physical board) are a mixture of symbolic and non-symbolic parts.
ais10-GOFAI-Nouvelle-AI.md&1.19&>     - We are born with some subsymbolic skills (heartbeat, digestion). We have to learn others explicitly (walking, opening a door). We have to learn almost all our explicitly symbolic processing abilities (maths, language).
ais10-GOFAI-Nouvelle-AI.md&1.19&
ais10-GOFAI-Nouvelle-AI.md&1.20&## “Graceful degradation”
ais10-GOFAI-Nouvelle-AI.md&1.20&
ais10-GOFAI-Nouvelle-AI.md&1.20&>- “Graceful degradation” means that in case of a program error, the robot should not just “hang” or reboot standing in the middle of the room.
ais10-GOFAI-Nouvelle-AI.md&1.20&>- Instead, if a few functions fail, a subsumption system could still go on working, because it is comprised of a multitude of independent systems.
ais10-GOFAI-Nouvelle-AI.md&1.20&>- If the movement planning goes wrong, for example, the Coke can collecting robot would perhaps walk into the wrong room.
ais10-GOFAI-Nouvelle-AI.md&1.20&>- But it would still go on walking about, it would not collide with objects, it would still identify and collect Coke cans correctly.
ais10-GOFAI-Nouvelle-AI.md&1.20&
ais10-GOFAI-Nouvelle-AI.md&1.21&## GOFAI and Nouvelle AI
ais10-GOFAI-Nouvelle-AI.md&1.21&
ais10-GOFAI-Nouvelle-AI.md&1.21&>- AI research programs that work along these lines are often called “Nouvelle AI,” to distinguish them from GOFAI, “Good Old Fashioned AI.”
ais10-GOFAI-Nouvelle-AI.md&1.21&>- Nouvelle AI would question the idea that symbolic AI systems are necessary or sufficient to achieve intelligent action.
ais10-GOFAI-Nouvelle-AI.md&1.21&>- Symbolic systems are not *necessary,* because there are systems that can behave intelligently (=appropriately in their environment) which don’t do symbolic processing (e.g. insects, or elephants).
ais10-GOFAI-Nouvelle-AI.md&1.21&>- Symbolic systems that don’t have sensors are not *sufficient,* because they don’t have the direct access to reality that is needed for intelligent action.
ais10-GOFAI-Nouvelle-AI.md&1.21&
ais10-GOFAI-Nouvelle-AI.md&1.22&## Nouvelle AI
ais10-GOFAI-Nouvelle-AI.md&1.22&
ais10-GOFAI-Nouvelle-AI.md&1.22&*Nouvelle AI* (French for 'new'), as opposed to GOFAI:
ais10-GOFAI-Nouvelle-AI.md&1.22&
ais10-GOFAI-Nouvelle-AI.md&1.22&>- Grounded symbols (robot has access to referential semantics).
ais10-GOFAI-Nouvelle-AI.md&1.22&>- Situated, not abstract cognition.
ais10-GOFAI-Nouvelle-AI.md&1.22&>- Optimal behaviour in a live and changing environment.
ais10-GOFAI-Nouvelle-AI.md&1.22&>- Graceful degradation of functions in case of error.
ais10-GOFAI-Nouvelle-AI.md&1.22&
ais10-GOFAI-Nouvelle-AI.md&1.23&## Importance of Nouvelle AI
ais10-GOFAI-Nouvelle-AI.md&1.23&
ais10-GOFAI-Nouvelle-AI.md&1.23&>- What can we learn from the theses of Nouvelle AI for our understanding of intelligence?
ais10-GOFAI-Nouvelle-AI.md&1.23&>     - "Intelligence" is not a binary ability. It is not something a thing has or doesn’t have.
ais10-GOFAI-Nouvelle-AI.md&1.23&>     - Rather, intelligent behaviour seems to be a continuum of abilities from simple to increasingly complex.
ais10-GOFAI-Nouvelle-AI.md&1.23&>     - A cockroach that runs away from a fire does have some intelligent abilities.
ais10-GOFAI-Nouvelle-AI.md&1.23&>     - A computer that plays chess but does not understand fires has other abilities.
ais10-GOFAI-Nouvelle-AI.md&1.23&>     - Both have some abilities that count as "intelligence," without having the full spectrum of human intelligence.
ais10-GOFAI-Nouvelle-AI.md&1.23&>     - But this can also be said of humans (for example, not all humans play chess; not all are as intelligent as Einstein; and so on).
ais10-GOFAI-Nouvelle-AI.md&1.23&
ais11-Neural-Networks-1.md&0.1&---
ais11-Neural-Networks-1.md&0.1&title:  "AI and Society: 11. Neural Networks"
ais11-Neural-Networks-1.md&0.1&author: Andreas Matthias, Lingnan University
ais11-Neural-Networks-1.md&0.1&date: September 30, 2019
ais11-Neural-Networks-1.md&0.1&...
ais11-Neural-Networks-1.md&0.1&
ais11-Neural-Networks-1.md&1.0&# Neurons and perceptrons
ais11-Neural-Networks-1.md&1.0&
ais11-Neural-Networks-1.md&1.1&## Biological neurons
ais11-Neural-Networks-1.md&1.1&
ais11-Neural-Networks-1.md&1.1&Artificial Neural Networks (ANN) are "neurally inspired." (McLaughlin 142)
ais11-Neural-Networks-1.md&1.1&
ais11-Neural-Networks-1.md&1.1&![](graphics/02-neuron.jpg)\ 
ais11-Neural-Networks-1.md&1.1&
ais11-Neural-Networks-1.md&1.1&Dendrites, cell body with signal integration, axon.
ais11-Neural-Networks-1.md&1.1&
ais11-Neural-Networks-1.md&1.2&## Perceptron (Rosenblatt 1957)
ais11-Neural-Networks-1.md&1.2&
ais11-Neural-Networks-1.md&1.2&![](graphics/05-perceptron.jpg)\ 
ais11-Neural-Networks-1.md&1.2&
ais11-Neural-Networks-1.md&1.2&Inputs, weights, summing function, cut-off value, output.
ais11-Neural-Networks-1.md&1.2&
ais11-Neural-Networks-1.md&1.3&## Multi-layer networks
ais11-Neural-Networks-1.md&1.3&
ais11-Neural-Networks-1.md&1.3&![](graphics/05-3lnet.jpg)\ 
ais11-Neural-Networks-1.md&1.3&
ais11-Neural-Networks-1.md&1.3&Input layer, hidden layer, output layer.
ais11-Neural-Networks-1.md&1.3&
ais11-Neural-Networks-1.md&1.4&## Calculating an OR
ais11-Neural-Networks-1.md&1.4&
ais11-Neural-Networks-1.md&1.4&Imagine you have a system that you want to perform the "logical or" function on its inputs A and B:
ais11-Neural-Networks-1.md&1.4&
ais11-Neural-Networks-1.md&1.4&  Input 1 (A)     Input 2 (B)    Output
ais11-Neural-Networks-1.md&1.4& -------------   -------------  --------
ais11-Neural-Networks-1.md&1.4&       0             0             0
ais11-Neural-Networks-1.md&1.4&       0             1             1
ais11-Neural-Networks-1.md&1.4&       1             0             1
ais11-Neural-Networks-1.md&1.4&       1             1             1
ais11-Neural-Networks-1.md&1.4& ---------------------------------------
ais11-Neural-Networks-1.md&1.4&
ais11-Neural-Networks-1.md&1.5&## Calculating an OR
ais11-Neural-Networks-1.md&1.5&
ais11-Neural-Networks-1.md&1.5&![](graphics/neurons-or-nnwm.png)\ 
ais11-Neural-Networks-1.md&1.5&
ais11-Neural-Networks-1.md&1.5&
ais11-Neural-Networks-1.md&1.6&## An analogy (1)
ais11-Neural-Networks-1.md&1.6&
ais11-Neural-Networks-1.md&1.6&>- Imagine that the inputs and outputs are electrical signals, say, a sound. The output is connected to a speaker. The neurons’ synaptic weights are volume dials that allow you to dial the volume of that particular audio line down, from 100\% to 0.
ais11-Neural-Networks-1.md&1.6&>- You would start by putting the signals for the first case (A=0, B=0) onto the input side. Then you would tweak the buttons until the output signal is 0.
ais11-Neural-Networks-1.md&1.6&>- Next you would put a signal ("1") on input A and leave B at 0. You would tweak the buttons until the signal at C (output) is on ("1").
ais11-Neural-Networks-1.md&1.6&>- Then you would go back to the first case, and make sure that this still works. If not, tweak some more until both cases work correctly.
ais11-Neural-Networks-1.md&1.6&>- Add the third case in the "OR" table. And so on.
ais11-Neural-Networks-1.md&1.6&>- After some time, you would manage to find a configuration of button positions that exactly satisfies the "OR" function.
ais11-Neural-Networks-1.md&1.6&
ais11-Neural-Networks-1.md&1.7&## An analogy (2)
ais11-Neural-Networks-1.md&1.7&
ais11-Neural-Networks-1.md&1.7&>- This is exactly how artificial neural networks work!
ais11-Neural-Networks-1.md&1.7&>- The buttons are the "synaptic weights" between the neurons and the thresholds that make them fire or not fire.
ais11-Neural-Networks-1.md&1.7&>- All neurons are fully connected to the next layer.
ais11-Neural-Networks-1.md&1.7&>- In the beginning, synaptic weights are random, but later the network changes them (during training) to match the desired output. This can take thousands of cycles of testing, adjustment, and error, and more testing.
ais11-Neural-Networks-1.md&1.7&>- When the learning process is finished, the system has "learned" a function. This function is now stored in the synaptic weights (the button positions that encode the behaviour we want).
ais11-Neural-Networks-1.md&1.7&
ais11-Neural-Networks-1.md&1.8&## Backpropagation
ais11-Neural-Networks-1.md&1.8&
ais11-Neural-Networks-1.md&1.8&>- When the neural network is in its learning phase, it will compare the *actual* output (that it produces with a particular set of weights) to the *desired* output.
ais11-Neural-Networks-1.md&1.8&>- The difference between the two is the output *error.*
ais11-Neural-Networks-1.md&1.8&>- There is a simple mathematical method used to change the synaptic weights *backwards* from the last layer to the previous layers of the neural network, so that the error is minimised.
ais11-Neural-Networks-1.md&1.8&>- This process, of changing the synaptic weights backwards in order to minimise the output error of the network, is called *backpropagation* (of the error correction).
ais11-Neural-Networks-1.md&1.8&>- We don’t need the mathematical details, but if you are interested, you can google “backpropagation” or look at the Wikipedia article.
ais11-Neural-Networks-1.md&1.8&
ais11-Neural-Networks-1.md&2.0&# Neurons in spreadsheets
ais11-Neural-Networks-1.md&2.0&
ais11-Neural-Networks-1.md&2.1&## Neurons in spreadsheets
ais11-Neural-Networks-1.md&2.1&
ais11-Neural-Networks-1.md&2.1&- You can use a normal spreadsheet (Google Sheets or Excel) to simulate artificial neurons.
ais11-Neural-Networks-1.md&2.1&- A good introduction is here: <http://toritris.weebly.com>
ais11-Neural-Networks-1.md&2.1&- Of course, this doesn’t do backpropagation. You’ll have to figure out the error and the correct synaptic weights yourself.
ais11-Neural-Networks-1.md&2.1&
ais11-Neural-Networks-1.md&2.2&## AND neuron
ais11-Neural-Networks-1.md&2.2&
ais11-Neural-Networks-1.md&2.2&![](graphics/05-and.jpg)\
ais11-Neural-Networks-1.md&2.2&
ais11-Neural-Networks-1.md&2.2&
ais11-Neural-Networks-1.md&2.3&## OR neuron
ais11-Neural-Networks-1.md&2.3&
ais11-Neural-Networks-1.md&2.3&![](graphics/05-or.jpg)\
ais11-Neural-Networks-1.md&2.3&
ais11-Neural-Networks-1.md&2.3&
ais11-Neural-Networks-1.md&2.4&## XOR neuron
ais11-Neural-Networks-1.md&2.4&
ais11-Neural-Networks-1.md&2.4&![](graphics/05-xortable.jpg)\ 
ais11-Neural-Networks-1.md&2.4&
ais11-Neural-Networks-1.md&2.4&![](graphics/05-xor.jpg)\
ais11-Neural-Networks-1.md&2.4&
ais11-Neural-Networks-1.md&2.4&(see these live on Google Docs!)
ais11-Neural-Networks-1.md&2.4&
ais11-Neural-Networks-1.md&2.4&
ais11-Neural-Networks-1.md&2.5&## What are the differences to symbolic AI?
ais11-Neural-Networks-1.md&2.5&
ais11-Neural-Networks-1.md&2.5&>- There is no symbolic representation of the "xor" function anywhere in the system!
ais11-Neural-Networks-1.md&2.5&>- If I look at an ANN, I cannot say what it will do. Even if I know all the synaptic weights (button positions), these don't tell me how the system will behave.
ais11-Neural-Networks-1.md&2.5&>- The only way to see the system's behaviour is to actually test it with inputs and see what comes out as an output.
ais11-Neural-Networks-1.md&2.5&>- This is the same way it is with humans. Examinations are needed to see what's stored in the brain! The brain does not contain symbols!
ais11-Neural-Networks-1.md&2.5&
ais11-Neural-Networks-1.md&2.6&## Character recognition
ais11-Neural-Networks-1.md&2.6&
ais11-Neural-Networks-1.md&2.6&![](graphics/05-ocr2.jpg)\ 
ais11-Neural-Networks-1.md&2.6&
ais11-Neural-Networks-1.md&3.0&# Learning in neural networks
ais11-Neural-Networks-1.md&3.0&
ais11-Neural-Networks-1.md&3.1&## How does a neural network learn? (1)
ais11-Neural-Networks-1.md&3.1&
ais11-Neural-Networks-1.md&3.1&>- The neural network is initialised with random synaptic weights. 
ais11-Neural-Networks-1.md&3.1&>- Then the user applies one input pattern and the corresponding desired output pattern. 
ais11-Neural-Networks-1.md&3.1&>- The neural network processes the input with its random synaptic weights and produces a random output that is probably not the output we wanted. Then it calculates the difference of that output to the desired output that was given by the user. 
ais11-Neural-Networks-1.md&3.1&
ais11-Neural-Networks-1.md&3.2&## How does a neural network learn? (2)
ais11-Neural-Networks-1.md&3.2&
ais11-Neural-Networks-1.md&3.2&>- Next, the artificial neural network changes its synaptic weights, using backpropagation, in order to minimise this error. 
ais11-Neural-Networks-1.md&3.2&>- Then the next pattern is applied, and the process is repeated. This applying of patterns and then minimising the error takes place thousands of times with all the desired learning patterns until the output error is smaller than a particular value that the user considers “good enough”.
ais11-Neural-Networks-1.md&3.2&
ais11-Neural-Networks-1.md&3.3&## How does a neural network learn? (3)
ais11-Neural-Networks-1.md&3.3&
ais11-Neural-Networks-1.md&3.3&![](graphics/05-learning.jpg)\ 
ais11-Neural-Networks-1.md&3.3&
ais11-Neural-Networks-1.md&3.3&
ais11-Neural-Networks-1.md&3.3&
ais11-Neural-Networks-1.md&3.3&
ais11-Neural-Networks-1.md&3.3&
ais11-Neural-Networks-1.md&3.4&## Generalisation (1)
ais11-Neural-Networks-1.md&3.4&
ais11-Neural-Networks-1.md&3.4&>- The important feature of ANNs over other ways of programming a computer is that they can *generalise.*
ais11-Neural-Networks-1.md&3.4&>- They can not only identify the same features that were used in their training, but they can also correctly classify *similar* (but different) features.
ais11-Neural-Networks-1.md&3.4&
ais11-Neural-Networks-1.md&3.4&. . . 
ais11-Neural-Networks-1.md&3.4&
ais11-Neural-Networks-1.md&3.4&- Example:
ais11-Neural-Networks-1.md&3.4&
ais11-Neural-Networks-1.md&3.4&![](graphics/05-mnist.png)
ais11-Neural-Networks-1.md&3.4&
ais11-Neural-Networks-1.md&3.4&
ais11-Neural-Networks-1.md&3.4&
ais11-Neural-Networks-1.md&3.5&## Generalisation (2)
ais11-Neural-Networks-1.md&3.5&
ais11-Neural-Networks-1.md&3.5&>- The ability to generalise is what makes neural networks so useful.
ais11-Neural-Networks-1.md&3.5&>- A neural network that can recognise a face only if it looks exactly the same as in the training picture would be of little practical use. 
ais11-Neural-Networks-1.md&3.5&>- Look back at our spreadsheet neuron. If you think about it (or if you try it out), you will see that small changes in the input of the neuron don't change the output. 
ais11-Neural-Networks-1.md&3.5&>     - Whether my input values are 0 or 0.1 or 0.2 does not make any difference to the output of the neuron, because this neuron is mapping a *whole range* of inputs to one single output value. 
ais11-Neural-Networks-1.md&3.5&>     - Therefore, if I train this neuron to recognise the input pattern 0 successfully, then it will also be able to recognise the input pattern 0.1 successfully. Here we already see generalisation in action.
ais11-Neural-Networks-1.md&3.5&
ais11-Neural-Networks-1.md&3.6&## Generalisation (3)
ais11-Neural-Networks-1.md&3.6&
ais11-Neural-Networks-1.md&3.6&The same applies to more complicated networks. Assume that I want to train a network to recognise handwriting. I would then perhaps use a square area of pixels as the input pattern and connect every pixel to one input of the neural network. This could look like this: 
ais11-Neural-Networks-1.md&3.6&
ais11-Neural-Networks-1.md&3.6&. . . 
ais11-Neural-Networks-1.md&3.6&
ais11-Neural-Networks-1.md&3.6&![](graphics/Cocr-trim.png "Letter A in a grid")\
ais11-Neural-Networks-1.md&3.6&
ais11-Neural-Networks-1.md&3.6&
ais11-Neural-Networks-1.md&3.6&
ais11-Neural-Networks-1.md&3.7&## Generalisation (4)
ais11-Neural-Networks-1.md&3.7&
ais11-Neural-Networks-1.md&3.7&Now I train my network to recognise a capital A by providing it with a collection of capital A patterns. 
ais11-Neural-Networks-1.md&3.7&
ais11-Neural-Networks-1.md&3.7&. . . 
ais11-Neural-Networks-1.md&3.7&
ais11-Neural-Networks-1.md&3.7&![](graphics/Letters-trim.png "Different capital A letters")\
ais11-Neural-Networks-1.md&3.7&
ais11-Neural-Networks-1.md&3.7&
ais11-Neural-Networks-1.md&3.7&
ais11-Neural-Networks-1.md&3.7&
ais11-Neural-Networks-1.md&3.8&## Generalisation (5)
ais11-Neural-Networks-1.md&3.8&
ais11-Neural-Networks-1.md&3.8&>- Assuming the network has been trained on these images and has learnt to recognise them successfully, I can now try to apply a test pattern that is not part of the input set.  
ais11-Neural-Networks-1.md&3.8&>- Because the neural network is stable in the face of small changes in its input patterns, the output will likely still remain the same although a few pixels are now different from what the network has been trained on. 
ais11-Neural-Networks-1.md&3.8&
ais11-Neural-Networks-1.md&3.9&## Generalisation (6)
ais11-Neural-Networks-1.md&3.9&
ais11-Neural-Networks-1.md&3.9&>- Question: is it good to train a neural network with many similar data or with widely different data? For example, if you want it to recognise a tree, would you train it with tree photos in different light, different times of day, and different seasons, or with more uniform tree pictures?
ais11-Neural-Networks-1.md&3.9&>- Obviously, the more different input patterns I can show to the neural network during training, the more it will be able to generalise and to process still more different input patterns after deployment. 
ais11-Neural-Networks-1.md&3.9&>- This is why, when we train the neural network, we try to present it with as many different inputs as possible. This should also include noisy and imperfect input.
ais11-Neural-Networks-1.md&3.9&
ais11-Neural-Networks-1.md&3.10&## Generalisation (7)
ais11-Neural-Networks-1.md&3.10&
ais11-Neural-Networks-1.md&3.10&>- Let us look again at the face recognition example. If I show the neural network only pictures of the person's face from the front in full light and without make-up, then the neural network will probably not be able to identify this person when make-up is applied and the shadows fall differently. 
ais11-Neural-Networks-1.md&3.10&>- But if I already trained the network using different light sources and different kinds of make-up, then these deviations from the optimal picture will already be accounted for in the network weights. 
ais11-Neural-Networks-1.md&3.10&>- By having been trained with images that contain always the same face but with different lighting and make-up, the neural network will have learnt that light and make-up do not constitute features that distinguish different input patterns from each other but are a kind of noise in the data.
ais11-Neural-Networks-1.md&3.10&
ais11-Neural-Networks-1.md&3.11&## Generalisation (8)
ais11-Neural-Networks-1.md&3.11&
ais11-Neural-Networks-1.md&3.11&>- Conclusion: It is important to remember that, as opposed to what one would normally think, good neural network training is not done with optimal and perfect data, but it is best done with noisy and imperfect data. 
ais11-Neural-Networks-1.md&3.11&>- In this way, the neural network can best learn to generalise and to distinguish between the features that it *should* learn and the accidental noise that it should ignore.
ais11-Neural-Networks-1.md&3.11&
ais11-Neural-Networks-1.md&4.0&# Design considerations
ais11-Neural-Networks-1.md&4.0&
ais11-Neural-Networks-1.md&4.1&## Stages in the design of an ANN
ais11-Neural-Networks-1.md&4.1&
ais11-Neural-Networks-1.md&4.1&- Configuration.
ais11-Neural-Networks-1.md&4.1&- Training.
ais11-Neural-Networks-1.md&4.1&- Application.
ais11-Neural-Networks-1.md&4.1&
ais11-Neural-Networks-1.md&4.2&## Configuration
ais11-Neural-Networks-1.md&4.2&
ais11-Neural-Networks-1.md&4.2&Configuration:
ais11-Neural-Networks-1.md&4.2&
ais11-Neural-Networks-1.md&4.2&1. Number of layers (input, output, hidden).
ais11-Neural-Networks-1.md&4.2&2. Number of neurons per layer.
ais11-Neural-Networks-1.md&4.2&3. *Semantics of input and output.*
ais11-Neural-Networks-1.md&4.2&
ais11-Neural-Networks-1.md&4.3&## Configuration
ais11-Neural-Networks-1.md&4.3&
ais11-Neural-Networks-1.md&4.3&>- Handwriting recognition: 
ais11-Neural-Networks-1.md&4.3&    - Input: Black and white pixels on a 8x8 grid (64 inputs). 
ais11-Neural-Networks-1.md&4.3&    - Output: the recognized character (26 outputs).
ais11-Neural-Networks-1.md&4.3&>-  Car control:
ais11-Neural-Networks-1.md&4.3&    - Input: Video image on 100x100 grid (10,000 inputs). Or: Input from distance sensors, radar and other sensors.
ais11-Neural-Networks-1.md&4.3&    - Output: Turn right/left, accelerator, brake (4 outputs).
ais11-Neural-Networks-1.md&4.3&    - Example: <https://www.youtube.com/watch?v=FKAULFV8tXw>
ais11-Neural-Networks-1.md&4.3&
ais11-Neural-Networks-1.md&4.3&
ais11-Neural-Networks-1.md&4.4&## Training and validation set
ais11-Neural-Networks-1.md&4.4&
ais11-Neural-Networks-1.md&4.4&>- All the input patterns used for the training of the network are together called the *training set.* 
ais11-Neural-Networks-1.md&4.4&>- Usually what we want from a neural network is not only to memorise the training set, but we want it to be able to recognise and categorise *new* patterns that are not part of the training set. 
ais11-Neural-Networks-1.md&4.4&>- This is why for testing we would use a second set of input data, which is called the *testing* or *validation set*, and which is *not* part of the training set. 
ais11-Neural-Networks-1.md&4.4&>- In this way, we can test if the neural network can actually recognise new patterns that it has never seen before. 
ais11-Neural-Networks-1.md&4.4&
ais11-Neural-Networks-1.md&4.5&## Features of the input data
ais11-Neural-Networks-1.md&4.5&
ais11-Neural-Networks-1.md&4.5&>- The “features” are those properties of the input data set that are relevant to us and that we want the network to recognise.
ais11-Neural-Networks-1.md&4.5&>- So the point of the neural network is to learn to recognise a particular set of *features* of its input.
ais11-Neural-Networks-1.md&4.5&>- For example, a network for face recognition should learn to identify features like the eyes, the nose and the mouth of a person.
ais11-Neural-Networks-1.md&4.5&>- On the other hand, we don't want it to memorise the shadows that are falling onto a face in a particular picture. We also don't want it to memorise a particular cosmetic (make-up) or some temporary feature of a face, like a pimple. 
ais11-Neural-Networks-1.md&4.5&>- Although these are *part* of the input data, they are not the *features* we are interested in. 
ais11-Neural-Networks-1.md&4.5&
ais11-Neural-Networks-1.md&4.6&## Minimisation of error
ais11-Neural-Networks-1.md&4.6&
ais11-Neural-Networks-1.md&4.6&>- As the network learns and minimises the output error, it converges towards the desired output. The error over time goes down as the network converges towards the desired behaviour. 
ais11-Neural-Networks-1.md&4.6&>- If something is wrong with the network architecture, for example, if the network does not have enough neurons or enough hidden layers, then the network will not successfully converge. 
ais11-Neural-Networks-1.md&4.6&>- In this case, we will see that, although we keep training the network, the error stops getting smaller at some point and stays constant. 
ais11-Neural-Networks-1.md&4.6&>- Then we know that this particular network is not able to successfully learn the provided input patterns and that we either need to simplify our input or make the network bigger or try an entirely different network architecture.
ais11-Neural-Networks-1.md&4.6&
ais11-Neural-Networks-1.md&4.7&## Problems with neural network design
ais11-Neural-Networks-1.md&4.7&
ais11-Neural-Networks-1.md&4.7&>- The more hidden neurons a network has, the more synapses it has. Therefore, its capacity to learn will be higher.
ais11-Neural-Networks-1.md&4.7&>- If bigger networks perform better, why not use very large numbers of neurons and layers?
ais11-Neural-Networks-1.md&4.7&>- The bigger the network, the longer it takes to train, and this relationship is exponential. 
ais11-Neural-Networks-1.md&4.7&>     - If every neuron of one layer is connected to every neuron of the next layer (which we call a fully connected network), then in one layer of 10 neurons, you have 10 synapses. If you have a second layer, you have 100 synapses. If you have a third layer, which is the same size as the second, and to which the second fully connects, you will have 100 × 100 = 10,000 synapses. And so on. 
ais11-Neural-Networks-1.md&4.7&
ais11-Neural-Networks-1.md&4.8&## Overfitting (1)
ais11-Neural-Networks-1.md&4.8&
ais11-Neural-Networks-1.md&4.8&>- Another problems is what is called “overfitting.”
ais11-Neural-Networks-1.md&4.8&>- The synapses in a neural network essentially try to memorise their input patterns. 
ais11-Neural-Networks-1.md&4.8&>- If a neural network has too few synapses, then it will not be able to distinguish between all the relevant features of the input, and it will not converge. That is, its output error will not go towards 0. 
ais11-Neural-Networks-1.md&4.8&>- On the other hand, if it has too many synapses, then it will be able to just memorise every single feature of the input, and it will not generalise. 
ais11-Neural-Networks-1.md&4.8&
ais11-Neural-Networks-1.md&4.9&## Overfitting (2)
ais11-Neural-Networks-1.md&4.9&
ais11-Neural-Networks-1.md&4.9&>- In the case of the face recognition program, you can imagine that a network that is big enough might be able to memorise not only the face but also the shadows cast by the light and the make-up on the face. 
ais11-Neural-Networks-1.md&4.9&>- Such a network will not need to learn the difference between *relevant features* and *noise* in order to reduce its output error, because with its many synapses, it can just memorise all the input information and still achieve a low output error. 
ais11-Neural-Networks-1.md&4.9&>- It is only when the network is unable to memorise accidental noise, because it does not have enough synapses to do that, that it is forced to learn the difference between relevant features and accidental noise. 
ais11-Neural-Networks-1.md&4.9&>- It does this by encoding the relevant features that appear again and again in the synaptic weights, while one-time noise is not encoded and therefore later not recognised by the network. 
ais11-Neural-Networks-1.md&4.9&
ais11-Neural-Networks-1.md&4.10&## Overfitting (3)
ais11-Neural-Networks-1.md&4.10&
ais11-Neural-Networks-1.md&4.10&>- This is the reason why, when designing a neural network, you don't want to make it either too big or too small for the task at hand. 
ais11-Neural-Networks-1.md&4.10&>- Finding just the right dimensions for a neural network is an art. If done right, the network will optimise learning time, accuracy and the ability to generalise, all at the same time. 
ais11-Neural-Networks-1.md&4.10&>- Although there are some rules of thumb (different for every network type), neural network researchers get better at this only through long experience and by trying out different parameters and seeing what works best.
ais11-Neural-Networks-1.md&4.10&
ais11-Neural-Networks-1.md&5.0&# Examples
ais11-Neural-Networks-1.md&5.0&
ais11-Neural-Networks-1.md&5.1&## Neural network example
ais11-Neural-Networks-1.md&5.1&
ais11-Neural-Networks-1.md&5.1&Train a neural network to recognise colour contrast:
ais11-Neural-Networks-1.md&5.1&
ais11-Neural-Networks-1.md&5.1&<https://harthur.github.io/brain>
ais11-Neural-Networks-1.md&5.1&
ais11-Neural-Networks-1.md&5.1&Look at the weights! (Click on “code”)
ais11-Neural-Networks-1.md&5.1&
ais11-Neural-Networks-1.md&5.2&## More examples
ais11-Neural-Networks-1.md&5.2&
ais11-Neural-Networks-1.md&5.2&Here are many more examples:
ais11-Neural-Networks-1.md&5.2&
ais11-Neural-Networks-1.md&5.2&<https://cs.stanford.edu/people/karpathy/convnetjs/>
ais11-Neural-Networks-1.md&5.2&
ais11-Neural-Networks-1.md&5.3&## Neural Networks Without the Math
ais11-Neural-Networks-1.md&5.3&
ais11-Neural-Networks-1.md&5.3&I wrote a little book explaining neural networks. It is essentially the same content as these lecture notes, but explained a little more, with more examples and nice pictures.
ais11-Neural-Networks-1.md&5.3&
ais11-Neural-Networks-1.md&5.3&You don’t need it for this class, but if you are interested in neural networks, you might like to have a look.
ais11-Neural-Networks-1.md&5.3&
ais11-Neural-Networks-1.md&5.3&It is available on Amazon and other online bookstores, both as ebook and paperback.
ais11-Neural-Networks-1.md&5.3&
ais11-Neural-Networks-1.md&5.3&![](cover-small.jpg)\
ais11-Neural-Networks-1.md&5.3&
ais12-Neural-Networks-2.md&0.1&---
ais12-Neural-Networks-2.md&0.1&title:  "AI and Society: 12. Neural Networks and reinforcement learning"
ais12-Neural-Networks-2.md&0.1&author: Andreas Matthias, Lingnan University
ais12-Neural-Networks-2.md&0.1&date: September 30, 2019
ais12-Neural-Networks-2.md&0.1&...
ais12-Neural-Networks-2.md&0.1&
ais12-Neural-Networks-2.md&1.0&# Reinforcement learning
ais12-Neural-Networks-2.md&1.0&
ais12-Neural-Networks-2.md&1.1&## Reinforcement learning
ais12-Neural-Networks-2.md&1.1&
ais12-Neural-Networks-2.md&1.1&>- Until now we had separate *training* and *deployment* phases.
ais12-Neural-Networks-2.md&1.1&>- In *reinforcement learning* the neural network gets programmed *while it operates in its final operating environment.*
ais12-Neural-Networks-2.md&1.1&>- There is no advance training phase any more.
ais12-Neural-Networks-2.md&1.1&>- The network gets "rewarded" if it does something successful (=the action gets "reinforced"). The network gets "punished" if it does something "bad."
ais12-Neural-Networks-2.md&1.1&>- In time, the network learns to act in the desired way.
ais12-Neural-Networks-2.md&1.1&>- A very nice example: <http://cs.stanford.edu/people/karpathy/convnetjs/demo/rldemo.html>
ais12-Neural-Networks-2.md&1.1&
ais12-Neural-Networks-2.md&1.2&## Why do we need reinforcement learning? (1)
ais12-Neural-Networks-2.md&1.2&
ais12-Neural-Networks-2.md&1.2&>- Programs that work in a dynamic, changing environment need to adapt.
ais12-Neural-Networks-2.md&1.2&>- For example, elevators in high-rise office towers are now often controlled by neural networks. 
ais12-Neural-Networks-2.md&1.2&>- The goal of the network’s operation is to minimise waiting time between the user pressing the button to call the elevator and the elevator’s arrival.
ais12-Neural-Networks-2.md&1.2&>- This cannot be done with a statically trained network, because the traffic patterns in a building are always changing.
ais12-Neural-Networks-2.md&1.2&
ais12-Neural-Networks-2.md&1.3&## Why do we need reinforcement learning? (2)
ais12-Neural-Networks-2.md&1.3&
ais12-Neural-Networks-2.md&1.3&>- Imagine there is, for the duration of one week, a conference on floor 34. The system must be able to optimise its behaviour during this time, for example by leaving idle elevators waiting at this floor.
ais12-Neural-Networks-2.md&1.3&>- After the conference is over, however, this behaviour will have to change again to accommodate new needs.
ais12-Neural-Networks-2.md&1.3&>- Only elevators that learn during operation can achieve optimal behaviour in such environments. Reinforcement learning provides a way for the environment to “teach” the elevator how to optimise its travel patterns.
ais12-Neural-Networks-2.md&1.3&
ais12-Neural-Networks-2.md&2.0&# Generative adversarial networks
ais12-Neural-Networks-2.md&2.0&
ais12-Neural-Networks-2.md&2.1&## Generative adversarial networks (GANs) (1)
ais12-Neural-Networks-2.md&2.1&
ais12-Neural-Networks-2.md&2.1&Random faces of non-existing people: <https://thispersondoesnotexist.com>
ais12-Neural-Networks-2.md&2.1&
ais12-Neural-Networks-2.md&2.2&## Generative adversarial networks (GANs) (2)
ais12-Neural-Networks-2.md&2.2&
ais12-Neural-Networks-2.md&2.2&![](thispersondoesnotexist.jpeg)\
ais12-Neural-Networks-2.md&2.2&
ais12-Neural-Networks-2.md&2.2&
ais12-Neural-Networks-2.md&2.2&
ais12-Neural-Networks-2.md&2.3&## Generative adversarial networks (GANs) (3)
ais12-Neural-Networks-2.md&2.3&
ais12-Neural-Networks-2.md&2.3&>- The pictures are generated by a *pair* of two artificial neural networks that work *against each other.*
ais12-Neural-Networks-2.md&2.3&>- The first one (“generator”) generates an image from random pixels.
ais12-Neural-Networks-2.md&2.3&>- The second network (“discriminator”) is initially trained to recognise images of real people.
ais12-Neural-Networks-2.md&2.3&>- Then we show the image that network 1 generated to network 2. Since the image is random, network 2 will say that this is not a human face.
ais12-Neural-Networks-2.md&2.3&
ais12-Neural-Networks-2.md&2.4&## Generative adversarial networks (GANs) (4)
ais12-Neural-Networks-2.md&2.4&
ais12-Neural-Networks-2.md&2.4&>- So we change the weights in network 1 to produce an image that fools network 2 a little better.
ais12-Neural-Networks-2.md&2.4&>- And so on until network 2 cannot distinguish between the output of network 1 and a real face.
ais12-Neural-Networks-2.md&2.4&>- Those generated faces that are indistinguishable from real faces are the outputs we see when we run the GAN.
ais12-Neural-Networks-2.md&2.4&>- This is a kind of Turing test for network 1 where network 2 is the judge.
ais12-Neural-Networks-2.md&2.4&>- Network 2 is an “adversary” (opponent) of network 1. This is why it’s called “adversarial.”
ais12-Neural-Networks-2.md&2.4&
ais12-Neural-Networks-2.md&2.5&## Generative adversarial networks (GANs) (5)
ais12-Neural-Networks-2.md&2.5&
ais12-Neural-Networks-2.md&2.5&> “The generator input is a random vector (noise) and therefore its initial output is also noise. Over time, as it receives feedback from the discriminator, it learns to synthesize more “realistic” images. The discriminator also improves over time by comparing generated samples with real samples, making it harder for the generator to deceive it.” ^[https://www.lyrn.ai/2018/12/26/a-style-based-generator-architecture-for-generative-adversarial-networks/]
ais12-Neural-Networks-2.md&2.5&
ais12-Neural-Networks-2.md&2.6&## Generative adversarial networks (GANs) (6)
ais12-Neural-Networks-2.md&2.6&
ais12-Neural-Networks-2.md&2.6&Which face is real? (And how to tell).
ais12-Neural-Networks-2.md&2.6&
ais12-Neural-Networks-2.md&2.6&<http://www.whichfaceisreal.com/results.php?r=1&p=0&i1=realimages/61557.jpeg&i2=fakeimages/image-2019-02-17_195954.jpeg>
ais12-Neural-Networks-2.md&2.6&
ais12-Neural-Networks-2.md&2.6&. . . 
ais12-Neural-Networks-2.md&2.6&
ais12-Neural-Networks-2.md&2.6&>- Look at the background. 
ais12-Neural-Networks-2.md&2.6&>- Since generative adversarial networks are pressured only to get the face right, the faces are usually pretty good.
ais12-Neural-Networks-2.md&2.6&>- But the background often doesn’t make sense.
ais12-Neural-Networks-2.md&2.6&>- Same with the ears. Since they are often covered by hair or shadows in the original images, or have different earrings attached to them, they are more “noisy” and less relevant to the training of the generative network than the central features of a face (eyes, nose, mouth). So they are often wrong in generated images.
ais12-Neural-Networks-2.md&2.6&
ais12-Neural-Networks-2.md&3.0&# Historical context of neural networks
ais12-Neural-Networks-2.md&3.0&
ais12-Neural-Networks-2.md&3.1&## AI and social networks (1)
ais12-Neural-Networks-2.md&3.1&
ais12-Neural-Networks-2.md&3.1&>- We said previously that only deep neural networks with many layers and many neurons have the ability to process big amounts of data. 
ais12-Neural-Networks-2.md&3.1&>- Identifying human faces in photographs, recognising speech or driving a car are such tasks that require big neural networks. 
ais12-Neural-Networks-2.md&3.1&>- But remember also that big neural networks have the problem of overfitting, that is, of memorising the input patterns and being unable to generalise. 
ais12-Neural-Networks-2.md&3.1&
ais12-Neural-Networks-2.md&3.2&## AI and social networks (2)
ais12-Neural-Networks-2.md&3.2&
ais12-Neural-Networks-2.md&3.2&>- One solution to overfitting is to make the network smaller, but then it cannot perform these big data tasks that we just mentioned. 
ais12-Neural-Networks-2.md&3.2&>- The other solution to avoid overfitting would be to train the network on more data. 
ais12-Neural-Networks-2.md&3.2&>- The bigger the training set is, the bigger the network can be and still avoid the problem of overfitting. 
ais12-Neural-Networks-2.md&3.2&>- This is where, in the first decade of the 21st century, social networks played a crucial role in the development of AI.
ais12-Neural-Networks-2.md&3.2&
ais12-Neural-Networks-2.md&3.3&## AI and social networks (3)
ais12-Neural-Networks-2.md&3.3&
ais12-Neural-Networks-2.md&3.3&>- Social networks provide the companies that operate them with an almost unlimited supply of perfect neural network training data.
ais12-Neural-Networks-2.md&3.3&>- During training, your network needs to learn the mapping of an input pattern to an output pattern. 
ais12-Neural-Networks-2.md&3.3&>- That is, the neural network needs to be presented with both the input pattern and the desired output at the same time. 
ais12-Neural-Networks-2.md&3.3&>- Now, a picture on Facebook is not just a picture. 
ais12-Neural-Networks-2.md&3.3&>     - The user who uploads a picture will usually also label the picture. 
ais12-Neural-Networks-2.md&3.3&>     - Before there was automated tagging of faces in Facebook pictures, users tagged the faces themselves. 
ais12-Neural-Networks-2.md&3.3&>     - In this way, they provided Facebook with an almost unlimited supply of perfectly labelled pairs of input and output data that could directly be used in the training of neural networks. 
ais12-Neural-Networks-2.md&3.3&
ais12-Neural-Networks-2.md&3.4&## AI and social networks (4)
ais12-Neural-Networks-2.md&3.4&
ais12-Neural-Networks-2.md&3.4&>- The same happens when somebody uploads multiple profile pictures of themselves. 
ais12-Neural-Networks-2.md&3.4&>- In this case, the system knows that these are all pictures of the same person, and it also knows the name of the user because this is stored in the user’s profile. So the system again has an input and a corresponding desired output pattern, and can train a neural network on this data set. 
ais12-Neural-Networks-2.md&3.4&
ais12-Neural-Networks-2.md&3.5&## AI and social networks (5)
ais12-Neural-Networks-2.md&3.5&
ais12-Neural-Networks-2.md&3.5&>- The same happens not only with faces but also with pictures of things. 
ais12-Neural-Networks-2.md&3.5&>     - Even before social networks took off, there were picture-sharing sites on the internet. Users upload images to these sites and tag them with tags that describe the content of the picture and where it was taken. 
ais12-Neural-Networks-2.md&3.5&>- From these data about content and location, neural networks can be trained to recognise image content from seeing the image itself. 
ais12-Neural-Networks-2.md&3.5&>- But for this to work, AI companies needed billions of images of all kinds of things, photographed in various conditions, with different backgrounds and different lighting, seen from different angles, and so on. And this is exactly what the users of these image-sharing sites provided.
ais12-Neural-Networks-2.md&3.5&
ais12-Neural-Networks-2.md&3.6&## AI and big data companies (1)
ais12-Neural-Networks-2.md&3.6&
ais12-Neural-Networks-2.md&3.6&>- It is, therefore, no accident that the first companies to develop deep learning AI systems were precisely the companies that had access to huge amounts of user-generated data: Facebook, Microsoft, Apple, Google and Amazon (Amazon’s cloud services drive much of the Internet’s cloud storage today). 
ais12-Neural-Networks-2.md&3.6&>- As soon as these companies realised what they needed for the development of their AI systems, they started buying image-sharing companies, and they started creating incentives for the users to upload more data.
ais12-Neural-Networks-2.md&3.6&
ais12-Neural-Networks-2.md&3.7&## AI and big data companies (2)
ais12-Neural-Networks-2.md&3.7&
ais12-Neural-Networks-2.md&3.7&>- When cloud storage is seen today as a convenience to the user, this is only half the truth. 
ais12-Neural-Networks-2.md&3.7&>- Storage is still expensive, and somebody has to pay for it. But two things make it worthwhile for the offering party: 
ais12-Neural-Networks-2.md&3.7&>     - the possibility to use the user data for targeted advertising; and 
ais12-Neural-Networks-2.md&3.7&>     - the access to labelled training data for artificial neural networks. 
ais12-Neural-Networks-2.md&3.7&
ais12-Neural-Networks-2.md&3.8&## AI and big data companies (3)
ais12-Neural-Networks-2.md&3.8&
ais12-Neural-Networks-2.md&3.8&>- This intimate relationship between training data and successful AI applications is the reason why today small companies don't have much of a chance in the big AI markets. 
ais12-Neural-Networks-2.md&3.8&>- Even if you knew how to design the most efficient and most useful neural networks, you would be unable to obtain sufficient data to train them. 
ais12-Neural-Networks-2.md&3.8&>- The ownership of huge amounts of data is the biggest asset that these big traditional companies have. 
ais12-Neural-Networks-2.md&3.8&>- Smaller competitors in the AI market do not have a chance until they are bought by the big companies, at which point, they gain access to the training data of their new parent company.
ais12-Neural-Networks-2.md&3.8&
ais12-Neural-Networks-2.md&3.8&
ais12-Neural-Networks-2.md&4.0&# Neural network failures
ais12-Neural-Networks-2.md&4.0&
ais12-Neural-Networks-2.md&4.1&## Sources
ais12-Neural-Networks-2.md&4.1&
ais12-Neural-Networks-2.md&4.1&>- The examples in this section are from:
ais12-Neural-Networks-2.md&4.1&>- <https://towardsdatascience.com/breaking-neural-networks-with-adversarial-attacks-f4290a9a45aa>
ais12-Neural-Networks-2.md&4.1&>- <https://adversarial-attacks.net>
ais12-Neural-Networks-2.md&4.1&
ais12-Neural-Networks-2.md&4.2&## Adversarial examples (1)
ais12-Neural-Networks-2.md&4.2&
ais12-Neural-Networks-2.md&4.2&>- Adversarial examples are input patterns that cause the neural network to misclassify an input.
ais12-Neural-Networks-2.md&4.2&>- They can be used in a targeted way, to cause very specific classification failures. In this way, someone could commit various crimes.
ais12-Neural-Networks-2.md&4.2&>- Adversarial examples use the fact that we don’t know exactly how individual synaptic weights contribute to a neural network’s classification, and that we cannot test the network with *all* possible input patterns.
ais12-Neural-Networks-2.md&4.2&>- Sometimes, subtle changes in the input pattern can cause the neural network to arrive at a totally wrong classification result.
ais12-Neural-Networks-2.md&4.2&
ais12-Neural-Networks-2.md&4.3&## Adversarial examples (2)
ais12-Neural-Networks-2.md&4.3&
ais12-Neural-Networks-2.md&4.3&![](graphics/panda-gibbon.png)\ 
ais12-Neural-Networks-2.md&4.3&
ais12-Neural-Networks-2.md&4.3&
ais12-Neural-Networks-2.md&4.3&
ais12-Neural-Networks-2.md&4.4&## Adversarial examples (3)
ais12-Neural-Networks-2.md&4.4&
ais12-Neural-Networks-2.md&4.4&>- Note how, in the previous example, you cannot see any difference between the two panda images.
ais12-Neural-Networks-2.md&4.4&>- This is because your brain knows how to filter out irrelevant noise.
ais12-Neural-Networks-2.md&4.4&>- Your brain *knows* it’s a panda, so even if half the picture was missing, your brain would supply the missing information to match its expectation.
ais12-Neural-Networks-2.md&4.4&>- An artificial neural network does not have such a prior expectation. It just looks at the image data and sees what the synaptic weights say.
ais12-Neural-Networks-2.md&4.4&>- Even minor changes in pixel values can make the wrong neurons fire, thus changing the whole chain of activations in the deep network and misclassifying the image.
ais12-Neural-Networks-2.md&4.4&
ais12-Neural-Networks-2.md&4.5&## Human sight is an active process
ais12-Neural-Networks-2.md&4.5&
ais12-Neural-Networks-2.md&4.5&>- “Seeing” for humans is an active process, not a passive interpretation of an image.
ais12-Neural-Networks-2.md&4.5&>- By seeing, we actively change the image to see what we believe that we should see.
ais12-Neural-Networks-2.md&4.5&
ais12-Neural-Networks-2.md&4.6&## Mis-identification of persons
ais12-Neural-Networks-2.md&4.6&
ais12-Neural-Networks-2.md&4.6&The middle image is classified as the person on the right:
ais12-Neural-Networks-2.md&4.6&
ais12-Neural-Networks-2.md&4.6&![](graphics/adv-glasses.png)\ 
ais12-Neural-Networks-2.md&4.6&
ais12-Neural-Networks-2.md&4.6&
ais12-Neural-Networks-2.md&4.6&
ais12-Neural-Networks-2.md&4.7&## Mis-identification of persons
ais12-Neural-Networks-2.md&4.7&
ais12-Neural-Networks-2.md&4.7&>- This is a similar example of “active” seeing and image correction in the human brain.
ais12-Neural-Networks-2.md&4.7&>- We have a prior expectation that glasses don’t change the identity of a person, but that they can make the person difficult to recognise.
ais12-Neural-Networks-2.md&4.7&>- Therefore, we are practised (from long experience in the world) to “think away” the glasses when we see a face.
ais12-Neural-Networks-2.md&4.7&
ais12-Neural-Networks-2.md&4.8&## Mis-identification of persons
ais12-Neural-Networks-2.md&4.8&
ais12-Neural-Networks-2.md&4.8&>- In the picture, we can easily “think away” the glasses and see the same face.
ais12-Neural-Networks-2.md&4.8&>- An ANN cannot do that because it lacks the prior expectation. Therefore, the image with glasses will trigger different neurons and cause the network to misclassify the input.
ais12-Neural-Networks-2.md&4.8&>- Observe how the predicted output does not look like the target actor at all!
ais12-Neural-Networks-2.md&4.8&>- This does not matter to the ANN. If it has only, let’s say, ten different possible classification outputs, then confusing the synaptic weights can easily make it fire the wrong output neuron.
ais12-Neural-Networks-2.md&4.8&
ais12-Neural-Networks-2.md&4.9&## Interpretation of an ANN misclassification
ais12-Neural-Networks-2.md&4.9&
ais12-Neural-Networks-2.md&4.9&>- It is incorrect to interpret the ANNs output as: “This neural network believes that the person with the funny glasses looks like the target person on the right.” The neural network does not “believe” anything.
ais12-Neural-Networks-2.md&4.9&>- Correct statement: The neurons triggered by the picture with the funny glasses cause the activation of other neurons, so that, at the end of the deep chain of network activations, the output neuron corresponding to the target person on the right is caused to fire.
ais12-Neural-Networks-2.md&4.9&
ais12-Neural-Networks-2.md&4.10&## Avoiding misclassifications
ais12-Neural-Networks-2.md&4.10&
ais12-Neural-Networks-2.md&4.10&>- How could we avoid this particular misclassification?
ais12-Neural-Networks-2.md&4.10&>     - Train the neural network with more examples of people wearing funny glasses, until it learns to recognise them as noise and abstract them away.
ais12-Neural-Networks-2.md&4.10&>- What is the problem with this approach in practice?
ais12-Neural-Networks-2.md&4.10&>     - We would need to train neural networks not only with the patterns that they should learn, but with thousands of absurd inputs that they should *not* recognise.
ais12-Neural-Networks-2.md&4.10&>     - Training would become very inefficient, and we could never be sure to have caught all the possible confusing input patterns.
ais12-Neural-Networks-2.md&4.10&
ais12-Neural-Networks-2.md&4.11&## Robustness of human perception
ais12-Neural-Networks-2.md&4.11&
ais12-Neural-Networks-2.md&4.11&>- Why are humans not so easily fooled by adversarial input patterns?
ais12-Neural-Networks-2.md&4.11&>- First, we have a prior expectation about what we are seeing in a particular context.
ais12-Neural-Networks-2.md&4.11&>- When you are at home and you see someone dressed like Santa Claus at Christmas time, it is likely to be your father or uncle, instead of being the country’s president or the real Santa Claus. 
ais12-Neural-Networks-2.md&4.11&>- A strange moving shadow close to your living room floor in the night is more likely to be your dog than anything else. So your brain assumes that it is the dog, and then tries to match the input to that expectation.
ais12-Neural-Networks-2.md&4.11&>- Neural networks operate *only* on one input image and don’t have such context clues.
ais12-Neural-Networks-2.md&4.11&
ais12-Neural-Networks-2.md&4.12&## Robustness of human perception
ais12-Neural-Networks-2.md&4.12&
ais12-Neural-Networks-2.md&4.12&>- Second, we have years, often decades, of training our brains with incomplete, damaged and badly lit input patterns. 
ais12-Neural-Networks-2.md&4.12&>- We are used to dealing with seeing in the dark (as much as possible), recognising things when we are tired or drunk, seeing through smoke, reading text upside-down, recognising birds and insects as they fly by at high speed, and so on.
ais12-Neural-Networks-2.md&4.12&>- These years of training make our recognition abilities very robust. Artificial neural networks are trained only on the patterns that interest the researchers at this moment, so they lack all that experience and are much easier to fool.
ais12-Neural-Networks-2.md&4.12&
ais12-Neural-Networks-2.md&4.12&
ais12-Neural-Networks-2.md&4.13&## Criminal uses (1)
ais12-Neural-Networks-2.md&4.13&
ais12-Neural-Networks-2.md&4.13&>- How could this be used in criminal ways?
ais12-Neural-Networks-2.md&4.13&
ais12-Neural-Networks-2.md&4.13&![](graphics/adv-glasses.png)\ 
ais12-Neural-Networks-2.md&4.13&
ais12-Neural-Networks-2.md&4.13&
ais12-Neural-Networks-2.md&4.14&## Criminal uses (2)
ais12-Neural-Networks-2.md&4.14&
ais12-Neural-Networks-2.md&4.14&>- One could fool surveillance cameras, implicating innocent people (the target person on the right) in a crime that was really committed by the person on the left.
ais12-Neural-Networks-2.md&4.14&>- One could enter locked, high-security areas (or even some homes that already use face-recognition locks and security systems) by fooling a face-recognition camera and pretending to be someone who has the right to enter these areas.
ais12-Neural-Networks-2.md&4.14&>- One could use that to unlock mobile phones and access a person’s data.
ais12-Neural-Networks-2.md&4.14&
ais12-Neural-Networks-2.md&4.15&## Traffic sign manipulation (1)
ais12-Neural-Networks-2.md&4.15&
ais12-Neural-Networks-2.md&4.15&![](graphics/adv-stop-sign.png)\
ais12-Neural-Networks-2.md&4.15&
ais12-Neural-Networks-2.md&4.15&
ais12-Neural-Networks-2.md&4.15&
ais12-Neural-Networks-2.md&4.16&## Traffic sign manipulation (2)
ais12-Neural-Networks-2.md&4.16&
ais12-Neural-Networks-2.md&4.16&>- In this example, we see a dirty (graffiti-sprayed) stop sign on the left. We have no difficulty recognising it.
ais12-Neural-Networks-2.md&4.16&>- On the right is a stop sign that has been changed with stickers to trigger a particular neural network into misclassifying it. It is seen as a 45 mph speed limit sign!
ais12-Neural-Networks-2.md&4.16&>- Can you see how this can be used to commit a crime?
ais12-Neural-Networks-2.md&4.16&>     - If a terrorist modifies stop signs like that on a busy intersection, self-driving cars might not stop but accelerate when reaching the intersection, thus crashing with cars coming from the other direction.
ais12-Neural-Networks-2.md&4.16&>     - One might target the car of a particular victim, thus killing him in a traffic accident.
ais12-Neural-Networks-2.md&4.16&>     - It would be easy to remove the stickers afterwards and make the crash look like an accident, or an unexplained failure of the self-driving car.
ais12-Neural-Networks-2.md&4.16&
ais12-Neural-Networks-2.md&4.16&
ais12-Neural-Networks-2.md&4.17&## Banana or toaster? (1)
ais12-Neural-Networks-2.md&4.17&
ais12-Neural-Networks-2.md&4.17&>- Observe how the classification switches in real time as the picture of the toaster is placed beside the banana:
ais12-Neural-Networks-2.md&4.17&>- <https://youtu.be/i1sp4X57TL4>
ais12-Neural-Networks-2.md&4.17&
ais12-Neural-Networks-2.md&4.17&
ais12-Neural-Networks-2.md&4.18&## Banana or toaster? (2)
ais12-Neural-Networks-2.md&4.18&
ais12-Neural-Networks-2.md&4.18&![](graphics/av-toaster-banana.png)\ 
ais12-Neural-Networks-2.md&4.18&
ais12-Neural-Networks-2.md&4.18&
ais12-Neural-Networks-2.md&4.18&
ais12-Neural-Networks-2.md&4.19&## Fooling speech recognition
ais12-Neural-Networks-2.md&4.19&
ais12-Neural-Networks-2.md&4.19&>- The same principle can be applied to *any* input, not just pictures.
ais12-Neural-Networks-2.md&4.19&>- Here is an example of fooling speech recognition. Go to the “Audio examples” section:
ais12-Neural-Networks-2.md&4.19&>- <https://adversarial-attacks.net>
ais12-Neural-Networks-2.md&4.19&>- The paper is here: <https://www.ndss-symposium.org/ndss-paper/adversarial-attacks-against-automatic-speech-recognition-systems-via-psychoacoustic-hiding/>
ais12-Neural-Networks-2.md&4.19&>- A nice (but pretty technical) video discussing all the attacks is here: <https://youtu.be/l_AkXxZt10I>
ais12-Neural-Networks-2.md&4.19&
ais12-Neural-Networks-2.md&4.20&## Real-world attacks on neural networks (1)
ais12-Neural-Networks-2.md&4.20&
ais12-Neural-Networks-2.md&4.20&>- As the use of neural networks in society increases, attacks will become more common:
ais12-Neural-Networks-2.md&4.20&>     - It is much easier for a criminal to get away from the site of a crime after hacking a computer or sticking a few stickers on a traffic sign, compared to a shooting. 
ais12-Neural-Networks-2.md&4.20&>     - Murders and other crimes, committed through AI, are often safer and easier for criminals to execute.
ais12-Neural-Networks-2.md&4.20&>     - They leave fewer usable traces for law enforcement to follow.
ais12-Neural-Networks-2.md&4.20&>     - As opposed to guns, the equipment needed for digital crimes and neural network tampering is already in everyone’s possession: computers, mobile phones, stickers, colour printers, an Internet connection.
ais12-Neural-Networks-2.md&4.20&
ais12-Neural-Networks-2.md&4.21&## Real-world attacks on neural networks (2)
ais12-Neural-Networks-2.md&4.21&
ais12-Neural-Networks-2.md&4.21&>- It is very hard (impossible?) to harden neural networks against targeted adversarial attacks.
ais12-Neural-Networks-2.md&4.21&>     - The problem get worse by having big companies provide standardised neural networks “in the cloud.” 
ais12-Neural-Networks-2.md&4.21&>     - If all self-driving cars from a manufacturer share the same neural network, then all cars of that manufacturer become vulnerable to the same attacks.
ais12-Neural-Networks-2.md&4.21&>     - The extensive resources needed to train neural networks, necessarily allow only a few big companies to train commercially viable neural networks.
ais12-Neural-Networks-2.md&4.21&>     - The increased concentration of neural network technology to a few big providers means that attacks will become easier and more widespread (like pathogens attacking extensive monocultures of genetically identical plants in modern agriculture).
ais12-Neural-Networks-2.md&4.21&
ais12-Neural-Networks-2.md&5.0&# Discussion
ais12-Neural-Networks-2.md&5.0&
ais12-Neural-Networks-2.md&5.1&## Tutorial question 1
ais12-Neural-Networks-2.md&5.1&
ais12-Neural-Networks-2.md&5.1&Information representation in ANN:
ais12-Neural-Networks-2.md&5.1&
ais12-Neural-Networks-2.md&5.1&>- *Describe how information is stored inside an ANN. Can you look inside an ANN and see what is stored there?*
ais12-Neural-Networks-2.md&5.1&>- The ANN contains no symbolic representation of the stored information.
ais12-Neural-Networks-2.md&5.1&>- The information is stored as a vector of synaptic weights (0.526, 0.881, 0.112, 0.655, 0.396...)
ais12-Neural-Networks-2.md&5.1&>- This vector can only be interpreted by that particular network in combination with the particular learned inputs and desired outputs!
ais12-Neural-Networks-2.md&5.1&>- There is no symbolic or human-readable information anywhere inside the network.
ais12-Neural-Networks-2.md&5.1&
ais12-Neural-Networks-2.md&5.2&## Tutorial question 2
ais12-Neural-Networks-2.md&5.2&
ais12-Neural-Networks-2.md&5.2&Checking learned content:
ais12-Neural-Networks-2.md&5.2&
ais12-Neural-Networks-2.md&5.2&>- I have trained a neural network to recognize handwritten letters.
ais12-Neural-Networks-2.md&5.2&>- *How can I see exactly which letters it has learned?*
ais12-Neural-Networks-2.md&5.2&>- I cannot.
ais12-Neural-Networks-2.md&5.2&>- There is no way to list the information stored in an ANN, except the vector of its synaptic weights.
ais12-Neural-Networks-2.md&5.2&>- If I want to know what is stored in it, I have to apply test patterns as inputs and see what it will output.
ais12-Neural-Networks-2.md&5.2&>- This is the same situation as testing learned information with human students!
ais12-Neural-Networks-2.md&5.2&>- This is why we need exams. We cannot list the information stored in human minds!
ais12-Neural-Networks-2.md&5.2&
ais12-Neural-Networks-2.md&5.3&## Tutorial question 2
ais12-Neural-Networks-2.md&5.3&
ais12-Neural-Networks-2.md&5.3&>- The fact that we cannot "look inside" neural networks, means that they can *surprise* their designers!
ais12-Neural-Networks-2.md&5.3&>- For example, AlphaGO in the second game against Lee Sedol made the surprising move 37, which no one had anticipated (or even understood at the time).
ais12-Neural-Networks-2.md&5.3&>- See: http://www.wired.com/2016/03/two-moves-alphago-lee-sedol-redefined-future/
ais12-Neural-Networks-2.md&5.3&>- “That’s a very strange move,” said one commentator, himself a nine dan Go player.
ais12-Neural-Networks-2.md&5.3&>- “I thought it was a mistake,” said another.
ais12-Neural-Networks-2.md&5.3&>- Fan Hui, a master player who helped train AlphaGo, kept saying that this was a beautiful move.
ais12-Neural-Networks-2.md&5.3&>- This could be said to be an example of *creative* behaviour that no one could anticipate!
ais12-Neural-Networks-2.md&5.3&
ais12-Neural-Networks-2.md&5.4&## Tutorial question 3
ais12-Neural-Networks-2.md&5.4&
ais12-Neural-Networks-2.md&5.4&>- Every system is embedded in an environment.
ais12-Neural-Networks-2.md&5.4&>- For artificial neural networks, the environment provides the inputs to the system and interprets the output.
ais12-Neural-Networks-2.md&5.4&>- *Compare the role and the importance of the environment for symbolic AI systems and artificial neural networks!*
ais12-Neural-Networks-2.md&5.4&
ais12-Neural-Networks-2.md&5.5&## System and environment (1)
ais12-Neural-Networks-2.md&5.5&
ais12-Neural-Networks-2.md&5.5&>- Symbolic systems can be complete without any environment.
ais12-Neural-Networks-2.md&5.5&>- They are a set of predicate calculus expressions.
ais12-Neural-Networks-2.md&5.5&>- These expressions can be written down into a program by a programmer, even if the system has no contact with an environment.
ais12-Neural-Networks-2.md&5.5&>- The system can be operated in isolation, or it can even never be "switched on." It would still be a complete system.
ais12-Neural-Networks-2.md&5.5&
ais12-Neural-Networks-2.md&5.6&## System and environment (2)
ais12-Neural-Networks-2.md&5.6&
ais12-Neural-Networks-2.md&5.6&>- ANNs make no sense without an environment.
ais12-Neural-Networks-2.md&5.6&>- When they are first configured, the synaptic weights are random, so the ANN does nothing interesting.
ais12-Neural-Networks-2.md&5.6&>- All the behaviour of the ANN has to be learned from its environment (which can be the programmer of the ANN).
ais12-Neural-Networks-2.md&5.6&>- The ANN does not contain any information before it is switched on and trained on environmental inputs!
ais12-Neural-Networks-2.md&5.6&>- See also below, "Responsibility problems of learning software."
ais12-Neural-Networks-2.md&5.6&
ais12-Neural-Networks-2.md&5.7&## Tutorial question 4
ais12-Neural-Networks-2.md&5.7&
ais12-Neural-Networks-2.md&5.7&How does reinforcement learning change things? Are reinforcement learning systems philosophically in any interesting way different from "normal" artificial neural networks?
ais12-Neural-Networks-2.md&5.7&
ais12-Neural-Networks-2.md&5.8&## Reinforcement learning
ais12-Neural-Networks-2.md&5.8&
ais12-Neural-Networks-2.md&5.8&>- With backpropagation ANNs, we have distinct learning (training) and an operation phases.
ais12-Neural-Networks-2.md&5.8&>- Reinforcement learning goes one step further:
ais12-Neural-Networks-2.md&5.8&>- The network is not completely trained at the moment of deployment. It continues to learn *during its normal operation.*
ais12-Neural-Networks-2.md&5.8&
ais12-Neural-Networks-2.md&5.9&## Reinforcement learning
ais12-Neural-Networks-2.md&5.9&
ais12-Neural-Networks-2.md&5.9&>- During operation, the network tries out new behaviours.
ais12-Neural-Networks-2.md&5.9&>- If they lead to good results, they are “re­inforced,” that is, the network is encouraged to use them again.
ais12-Neural-Networks-2.md&5.9&>- If they lead to bad results, they will be avoided in the future.
ais12-Neural-Networks-2.md&5.9&>- The network determines itself whether the results are “good” or “bad”.
ais12-Neural-Networks-2.md&5.9&>- E.g. elevator: by measuring the mean waiting and transport time for passengers, between pressing the call button and arrival at the destination floor, the elevator can optimise its behaviour by waiting at the optimal floors.
ais12-Neural-Networks-2.md&5.9&
ais12-Neural-Networks-2.md&5.10&## Reinforcement learning
ais12-Neural-Networks-2.md&5.10&
ais12-Neural-Networks-2.md&5.10&>- Reinforcement learning lifts the distinction between the training and application phases which we find in traditional neural network concepts.
ais12-Neural-Networks-2.md&5.10&>- The system learns inside its final operating environment by exploring available action alternatives in a trial-and-error fashion and optimising its own parameters according to the results.
ais12-Neural-Networks-2.md&5.10&>- Thus, the exploration phase is an integral part of the design of the working machine and cannot be separated from it.
ais12-Neural-Networks-2.md&5.10&>- This is necessary in highly dynamic environments, where a system needs to change continually in order to achieve optimal efficiency.
ais12-Neural-Networks-2.md&5.10&
ais12-Neural-Networks-2.md&5.11&## Reinforcement learning and error
ais12-Neural-Networks-2.md&5.11&
ais12-Neural-Networks-2.md&5.11&>- And since learning is done by trial and error, we find that **errors are unavoidable features of any reinforcement learning system.**
ais12-Neural-Networks-2.md&5.11&>- This is quite contrary to the common, traditional understanding that technology, done correctly, must operate free of errors, and that errors are always the errors of the programmer, not of the programmed machine.
ais12-Neural-Networks-2.md&5.11&>- Reinforcement learning presents us, for the first time, with the *necessity* of errors as we know it from living systems: as a system *feature,* the precondition for learning and adaptive behaviour, and not merely a product flaw.
ais12-Neural-Networks-2.md&5.11&
ais12-Neural-Networks-2.md&5.12&## Reinforcement learning and error
ais12-Neural-Networks-2.md&5.12&
ais12-Neural-Networks-2.md&5.12&>- *If reinforcement learning systems are so prone to errors, why would we use them? Why not just get rid of them and use conventionally programmed systems that are more reliable?*
ais12-Neural-Networks-2.md&5.12&>- Because we, as a society, *need* these systems:
ais12-Neural-Networks-2.md&5.12&>     - Dynamic, adaptive elevator control in big buildings. Without that, we would forever wait for elevators.
ais12-Neural-Networks-2.md&5.12&>     - Diagnosis of diseases in situations where there are no doctors available. Without such systems, we would have people die from undiagnosed diseases, which is clearly worse than the occasional wrong diagnosis.
ais12-Neural-Networks-2.md&5.12&>     - Self-driving cars. Without them, we would have to accept all the failings of human drivers (less safe, driving when drunk or distracted, unreliable or insufficient sensory organs, bad judgement and slow reflexes, and so on).
ais12-Neural-Networks-2.md&5.12&>- As in these cases, often reinforcement learning machines, despite their problems, are the better option.
ais12-Neural-Networks-2.md&5.12&
ais12-Neural-Networks-2.md&5.13&## Tutorial question 5
ais12-Neural-Networks-2.md&5.13&
ais12-Neural-Networks-2.md&5.13&>- A symbolic AI system and an ANN are both programmed to steer a robot car.
ais12-Neural-Networks-2.md&5.13&>- Now both crash into other cars on their first real drive, injuring other people.
ais12-Neural-Networks-2.md&5.13&>- You are the judge. Both programmers say that they are not guilty for the wrong behaviour of their systems.
ais12-Neural-Networks-2.md&5.13&>- *What do you think? Are they to get the same sentence? Or are they responsible to different degrees?*
ais12-Neural-Networks-2.md&5.13&
ais12-Neural-Networks-2.md&5.14&## Responsible?
ais12-Neural-Networks-2.md&5.14&
ais12-Neural-Networks-2.md&5.14&>- The symbolic programmer has entered every rule and every command of the program into the machine himself. He has had access to the whole program and could inspect and test it.
ais12-Neural-Networks-2.md&5.14&>- The ANN programmer taught the network to drive by example. At no point did he have the possibility of inspecting the program's stored information. He could just train it long enough and then test if it could cope in real­life situations. Such a test can never be exhaustive!
ais12-Neural-Networks-2.md&5.14&>- Therefore, the ANN programmer should be justly held *less* responsible than the symbolic programmer.
ais12-Neural-Networks-2.md&5.14&
ais12-Neural-Networks-2.md&5.15&## Responsibility problems of learning software
ais12-Neural-Networks-2.md&5.15&
ais12-Neural-Networks-2.md&5.15&>- The same applies to *learning* software that is not ANN-based.
ais12-Neural-Networks-2.md&5.15&>- See Microsoft Tay case ^[http://www.theverge.com/2016/3/24/11297050/tay-microsoft-chatbot-racist]!
ais12-Neural-Networks-2.md&5.15&>- March 23, 2016: Microsoft unveiled Tay -- a Twitter bot that the company described as an experiment in "conversational understanding."
ais12-Neural-Networks-2.md&5.15&>- The more you chat with Tay, said Microsoft, the smarter it gets, learning to engage people through "casual and playful conversation."
ais12-Neural-Networks-2.md&5.15&>- Soon after Tay launched, people starting tweeting the bot with all sorts of misogynistic, racist, and Donald Trumpist remarks.
ais12-Neural-Networks-2.md&5.15&>- Tay: "I f*** hate feminists and they should all die and burn in hell." -- "Hitler was right I hate the jews."
ais12-Neural-Networks-2.md&5.15&>- Tay disappeared less than 24 hours after being switched on.
ais12-Neural-Networks-2.md&5.15&
ais12-Neural-Networks-2.md&5.16&## Summary: Advantages of ANN
ais12-Neural-Networks-2.md&5.16&
ais12-Neural-Networks-2.md&5.16&>- ANN are able to *learn.*
ais12-Neural-Networks-2.md&5.16&>- They are able to perform well on problems where the programmer doesn't have a symbolic/algorithmic solution to a problem! (Character recognition).
ais12-Neural-Networks-2.md&5.16&>- They can perform in *new* situations that they have not learned.
ais12-Neural-Networks-2.md&5.16&>- Their behaviour in new situations might not be optimal, but it's better than the breakdown of a "brittle" (Winograd) symbolic system.
ais12-Neural-Networks-2.md&5.16&>- ANNs can have direct access to sensory data, and so they can provide *symbol grounding* for further symbolic processing (hybrid systems).
ais12-Neural-Networks-2.md&5.16&
ais12-Neural-Networks-2.md&5.17&## Summary: Drawbacks of ANN (1)
ais12-Neural-Networks-2.md&5.17&
ais12-Neural-Networks-2.md&5.17&>- Depending on the training, ANN can come up with strange answers ("recognition of tanks in sunny/cloudy images" story).
ais12-Neural-Networks-2.md&5.17&>- Therefore, good training is crucial.
ais12-Neural-Networks-2.md&5.17&>- ANNs can be difficult and slow to train well.
ais12-Neural-Networks-2.md&5.17&>- We can never reliably tell if they are trained well. There is no way to fully examine the effects of ANN training except by testing the ANN's reactions.
ais12-Neural-Networks-2.md&5.17&
ais12-Neural-Networks-2.md&5.18&## Summary: Drawbacks of ANN (2)
ais12-Neural-Networks-2.md&5.18&
ais12-Neural-Networks-2.md&5.18&>- If ANNs learn while deployed, the environment becomes a trainer for the ANN.
ais12-Neural-Networks-2.md&5.18&>- Therefore, the programmer has no control over what the ANN will learn. The same applies to all *learning* software (even without ANN, like Tay).
ais12-Neural-Networks-2.md&5.18&>- In the case of reinforcement learning systems, errors are an integral part of the system’s operation. There cannot be an error-free reinforcement learning system, in the same way as they cannot be error-free humans. Learning, exploration and error go together.
ais12-Neural-Networks-2.md&5.18&
ais12-Neural-Networks-2.md&5.18&
ais13-Genetic-Computing-Machine-Learning.md&0.1&---
ais13-Genetic-Computing-Machine-Learning.md&0.1&title:  "AI and Society: 13. Machine learning and genetic computing"
ais13-Genetic-Computing-Machine-Learning.md&0.1&author: Andreas Matthias, Lingnan University
ais13-Genetic-Computing-Machine-Learning.md&0.1&date: October 12, 2019
ais13-Genetic-Computing-Machine-Learning.md&0.1&...
ais13-Genetic-Computing-Machine-Learning.md&0.1&
ais13-Genetic-Computing-Machine-Learning.md&1.0&# Machine Learning Terminology
ais13-Genetic-Computing-Machine-Learning.md&1.0&
ais13-Genetic-Computing-Machine-Learning.md&1.1&## Machine and deep learning
ais13-Genetic-Computing-Machine-Learning.md&1.1&
ais13-Genetic-Computing-Machine-Learning.md&1.1&>- Machine Learning (ML): Every kind of computing system that is able to learn. This can include symbolic systems (like Prolog or expert systems), neural networks, reinforcement learning, etc.
ais13-Genetic-Computing-Machine-Learning.md&1.1&>- Deep Learning: A type of machine learning that is based on deep artificial neural networks.
ais13-Genetic-Computing-Machine-Learning.md&1.1&>- Deep neural networks: Artificial neural networks with more than 3 layers, typically 10 or more. These are particularly good at visual recognition tasks, but also used for other applications.
ais13-Genetic-Computing-Machine-Learning.md&1.1&
ais13-Genetic-Computing-Machine-Learning.md&1.2&## Main tasks of machine learning
ais13-Genetic-Computing-Machine-Learning.md&1.2&
ais13-Genetic-Computing-Machine-Learning.md&1.2&There are three main tasks of machine learning:
ais13-Genetic-Computing-Machine-Learning.md&1.2&
ais13-Genetic-Computing-Machine-Learning.md&1.2&- Regression
ais13-Genetic-Computing-Machine-Learning.md&1.2&- Classification
ais13-Genetic-Computing-Machine-Learning.md&1.2&- Clustering
ais13-Genetic-Computing-Machine-Learning.md&1.2&
ais13-Genetic-Computing-Machine-Learning.md&1.2&Some of these typically run supervised, some unsupervised.
ais13-Genetic-Computing-Machine-Learning.md&1.2&
ais13-Genetic-Computing-Machine-Learning.md&1.3&## Regression (1)
ais13-Genetic-Computing-Machine-Learning.md&1.3&
ais13-Genetic-Computing-Machine-Learning.md&1.3&>- Regression means to put a line or a curve through a collection of data points, so that one is able to predict where new data points will fall.
ais13-Genetic-Computing-Machine-Learning.md&1.3&
ais13-Genetic-Computing-Machine-Learning.md&1.3&![Linear Regression](graphics/05-Linear_Regression.png)
ais13-Genetic-Computing-Machine-Learning.md&1.3&
ais13-Genetic-Computing-Machine-Learning.md&1.4&## Regression (2)
ais13-Genetic-Computing-Machine-Learning.md&1.4&
ais13-Genetic-Computing-Machine-Learning.md&1.4&>- In other words, it is about understanding the relationship between two variables, so that one can predict the value of one if the value of the other is known.
ais13-Genetic-Computing-Machine-Learning.md&1.4&
ais13-Genetic-Computing-Machine-Learning.md&1.4&![Sometimes data can be more complicated](graphics/05-gaussian-regression.jpg)
ais13-Genetic-Computing-Machine-Learning.md&1.4&
ais13-Genetic-Computing-Machine-Learning.md&1.5&## Examples for regression
ais13-Genetic-Computing-Machine-Learning.md&1.5&
ais13-Genetic-Computing-Machine-Learning.md&1.5&>- From a list of house features (living area, location, age, number of rooms), predict the sales price for that house.
ais13-Genetic-Computing-Machine-Learning.md&1.5&>- From a list of tumour features (type of tumour, stage of development, organs affected), predict the probability of successful treatment.
ais13-Genetic-Computing-Machine-Learning.md&1.5&>- From a list of student features (secondary school grades, frequency of Facebook updates, list of hobbies and interests, number of languages spoken, participation in student societies, grade in the midterm exam), predict the expected final grade for this course.
ais13-Genetic-Computing-Machine-Learning.md&1.5&>- From a video feed from a camera in front of a car, predict where the other cars will be one second later.
ais13-Genetic-Computing-Machine-Learning.md&1.5&
ais13-Genetic-Computing-Machine-Learning.md&1.6&## Classification
ais13-Genetic-Computing-Machine-Learning.md&1.6&
ais13-Genetic-Computing-Machine-Learning.md&1.6&>- Classification is different from regression in that the desired output of the neural network is not a continuous number, but one of N different “bins” into which the input will fall.
ais13-Genetic-Computing-Machine-Learning.md&1.6&>- The point here is to classify the input as being a particular one (out of N) kind of thing.
ais13-Genetic-Computing-Machine-Learning.md&1.6&>- We can distinguish binary from multiple-class classification.
ais13-Genetic-Computing-Machine-Learning.md&1.6&
ais13-Genetic-Computing-Machine-Learning.md&1.7&## Examples for classification
ais13-Genetic-Computing-Machine-Learning.md&1.7&
ais13-Genetic-Computing-Machine-Learning.md&1.7&>- From a picture of an animal, find out if it is a cat or a dog.
ais13-Genetic-Computing-Machine-Learning.md&1.7&>- From a picture of a person, find out if it is a man or a woman.
ais13-Genetic-Computing-Machine-Learning.md&1.7&>- From a person’s *given name,* find out whether it is a man or a woman.
ais13-Genetic-Computing-Machine-Learning.md&1.7&>- Recognise the objects in a video image.
ais13-Genetic-Computing-Machine-Learning.md&1.7&>- Recognise what a self-driving car sees on the street in front of it.
ais13-Genetic-Computing-Machine-Learning.md&1.7&
ais13-Genetic-Computing-Machine-Learning.md&1.8&## Examples for classification
ais13-Genetic-Computing-Machine-Learning.md&1.8&
ais13-Genetic-Computing-Machine-Learning.md&1.8&![](graphics/05-classification.png)
ais13-Genetic-Computing-Machine-Learning.md&1.8&
ais13-Genetic-Computing-Machine-Learning.md&1.9&## Try it out!
ais13-Genetic-Computing-Machine-Learning.md&1.9&
ais13-Genetic-Computing-Machine-Learning.md&1.9&<https://www.ibm.com/watson/services/visual-recognition/>
ais13-Genetic-Computing-Machine-Learning.md&1.9&
ais13-Genetic-Computing-Machine-Learning.md&1.10&## Classification vs. regression
ais13-Genetic-Computing-Machine-Learning.md&1.10&
ais13-Genetic-Computing-Machine-Learning.md&1.10&>- The two uses are similar.
ais13-Genetic-Computing-Machine-Learning.md&1.10&>- Generally, when we expect a numerical value as output, we talk of regression.
ais13-Genetic-Computing-Machine-Learning.md&1.10&>- When we expect one of N discrete “bins” (or “tags”) as output, we talk of classification.
ais13-Genetic-Computing-Machine-Learning.md&1.10&>- But a very fine-grained classification can be similar to a regression (for example, the prediction of a student’s grade above).
ais13-Genetic-Computing-Machine-Learning.md&1.10&>     - If I predict A, B, C, D, F, then it would be a classification task.
ais13-Genetic-Computing-Machine-Learning.md&1.10&>     - If I predict a number between 0-100%, it would be a regression task.
ais13-Genetic-Computing-Machine-Learning.md&1.10&
ais13-Genetic-Computing-Machine-Learning.md&1.11&## Clustering
ais13-Genetic-Computing-Machine-Learning.md&1.11&
ais13-Genetic-Computing-Machine-Learning.md&1.11&>- Clustering means that the machine will try to find similarities between different data objects by itself.
ais13-Genetic-Computing-Machine-Learning.md&1.11&>- Given a collection of data points, it will try to “cluster” them together to a number of clusters.
ais13-Genetic-Computing-Machine-Learning.md&1.11&
ais13-Genetic-Computing-Machine-Learning.md&1.12&## Example of clustering
ais13-Genetic-Computing-Machine-Learning.md&1.12&
ais13-Genetic-Computing-Machine-Learning.md&1.12&>- Consider the data set:
ais13-Genetic-Computing-Machine-Learning.md&1.12&>     - Red Toyota
ais13-Genetic-Computing-Machine-Learning.md&1.12&>     - Black cat
ais13-Genetic-Computing-Machine-Learning.md&1.12&>     - Red bird
ais13-Genetic-Computing-Machine-Learning.md&1.12&>     - Blue Tesla
ais13-Genetic-Computing-Machine-Learning.md&1.12&>     - Yellow bird
ais13-Genetic-Computing-Machine-Learning.md&1.12&>- What clusters do you see?
ais13-Genetic-Computing-Machine-Learning.md&1.12&>- Cars, animals, birds, red things, black things, blue things, yellow things.
ais13-Genetic-Computing-Machine-Learning.md&1.12&>- Some things can belong to multiple clusters.
ais13-Genetic-Computing-Machine-Learning.md&1.12&
ais13-Genetic-Computing-Machine-Learning.md&1.12&
ais13-Genetic-Computing-Machine-Learning.md&1.13&## Difference between clustering and classification
ais13-Genetic-Computing-Machine-Learning.md&1.13&
ais13-Genetic-Computing-Machine-Learning.md&1.13&>- In clustering, we don’t know in advance which clusters we will find.
ais13-Genetic-Computing-Machine-Learning.md&1.13&>- We just have a bunch of data, and try to find some regularities.
ais13-Genetic-Computing-Machine-Learning.md&1.13&>- While the machine processes the data set, it will create new “bins” or “tags” as needed.
ais13-Genetic-Computing-Machine-Learning.md&1.13&>- Therefore, clustering is an example of *unsupervised* learning.
ais13-Genetic-Computing-Machine-Learning.md&1.13&>- In classification, the “bins” or “tags” are given in advance.
ais13-Genetic-Computing-Machine-Learning.md&1.13&>- The machine has to sort new data into the existing bins, but not create new bins.
ais13-Genetic-Computing-Machine-Learning.md&1.13&>- Since the bins (and examples of what goes in them) are given during machine training, classification is an example of *supervised* learning.
ais13-Genetic-Computing-Machine-Learning.md&1.13&
ais13-Genetic-Computing-Machine-Learning.md&1.14&## Supervised learning
ais13-Genetic-Computing-Machine-Learning.md&1.14&
ais13-Genetic-Computing-Machine-Learning.md&1.14&>- In “supervised” learning, we have input patterns and a desired output pattern for each input pattern. The algorithm learns to match the output to the input pattern.
ais13-Genetic-Computing-Machine-Learning.md&1.14&>- The goal is to be able to guess, for a new input pattern, what the correct output pattern would be.
ais13-Genetic-Computing-Machine-Learning.md&1.14&>- The process of learning from the training data resembles a teacher supervising the learning process by comparing expected and actual output patterns until the actual output matches the expected output (=the error is minimised).
ais13-Genetic-Computing-Machine-Learning.md&1.14&
ais13-Genetic-Computing-Machine-Learning.md&1.15&## Unsupervised learning
ais13-Genetic-Computing-Machine-Learning.md&1.15&
ais13-Genetic-Computing-Machine-Learning.md&1.15&>- In unsupervised learning, we only have input patterns, but no corresponding output patterns.
ais13-Genetic-Computing-Machine-Learning.md&1.15&>- The goal of unsupervised learning is to learn more about the data by classifying and analysing them.
ais13-Genetic-Computing-Machine-Learning.md&1.15&>- In unsupervised learning, there is no “teacher” and no model answers. Like in k-clustering, the algorithm has to find a structure in the data by itself.
ais13-Genetic-Computing-Machine-Learning.md&1.15&
ais13-Genetic-Computing-Machine-Learning.md&2.0&# Genetic computing
ais13-Genetic-Computing-Machine-Learning.md&2.0&
ais13-Genetic-Computing-Machine-Learning.md&2.1&## Concepts of evolution
ais13-Genetic-Computing-Machine-Learning.md&2.1&
ais13-Genetic-Computing-Machine-Learning.md&2.1&>- Population of individual organisms with genes, situated in a particular environment.
ais13-Genetic-Computing-Machine-Learning.md&2.1&>- Variation in genetic material.
ais13-Genetic-Computing-Machine-Learning.md&2.1&>- Possibly (but not necessarily): mutations.
ais13-Genetic-Computing-Machine-Learning.md&2.1&>- Recombination (particularly in sexual reproduction).
ais13-Genetic-Computing-Machine-Learning.md&2.1&>- Selection under pressure from the environment.
ais13-Genetic-Computing-Machine-Learning.md&2.1&>- Offspring with similar genetic material as parental organisms.
ais13-Genetic-Computing-Machine-Learning.md&2.1&
ais13-Genetic-Computing-Machine-Learning.md&2.2&## Concepts of evolution
ais13-Genetic-Computing-Machine-Learning.md&2.2&
ais13-Genetic-Computing-Machine-Learning.md&2.2&![](graphics/02-ga.png)\ 
ais13-Genetic-Computing-Machine-Learning.md&2.2&
ais13-Genetic-Computing-Machine-Learning.md&2.3&## Mutations
ais13-Genetic-Computing-Machine-Learning.md&2.3&
ais13-Genetic-Computing-Machine-Learning.md&2.3&>- Mutations are small, random changes of the genetic material.
ais13-Genetic-Computing-Machine-Learning.md&2.3&>- Mutations are not always beneficial. In fact, most of the time they are harmful to the organism.
ais13-Genetic-Computing-Machine-Learning.md&2.3&>- The more complex the organism, the more likely it is that a random mutation will be harmful. *Why?*
ais13-Genetic-Computing-Machine-Learning.md&2.3&>- Because more complex organisms depend on a more precise relation between their parts to function. Random changes are more likely to break something.
ais13-Genetic-Computing-Machine-Learning.md&2.3&>- Example: A hole in the leaf of a plant does not affect the plant much. A same-sized hole in the body of a human is more likely to be a serious injury or even lethal.
ais13-Genetic-Computing-Machine-Learning.md&2.3&
ais13-Genetic-Computing-Machine-Learning.md&2.4&## Mutations
ais13-Genetic-Computing-Machine-Learning.md&2.4&
ais13-Genetic-Computing-Machine-Learning.md&2.4&>- Another example: The more complex a word is, the more likely it is that a random mutation will “break” the word (= make it into a non-word).
ais13-Genetic-Computing-Machine-Learning.md&2.4&>- Three-letter words are more robust to one-letter changes:
ais13-Genetic-Computing-Machine-Learning.md&2.4&>     - “cut” -- a one-letter mutation can produce the good words: cat, cup, but, put, ...
ais13-Genetic-Computing-Machine-Learning.md&2.4&>     - It might also produce garbage: cpt, cet, cuc, ...
ais13-Genetic-Computing-Machine-Learning.md&2.4&>- With a five-letter word, it’s much harder to find “meaningful” mutations:
ais13-Genetic-Computing-Machine-Learning.md&2.4&>     - “hello” -- Good mutations: cello. It’s hard to think of another.
ais13-Genetic-Computing-Machine-Learning.md&2.4&>     - Most other mutations are destructive: xello, hzllo, hejjo, hellp.
ais13-Genetic-Computing-Machine-Learning.md&2.4&
ais13-Genetic-Computing-Machine-Learning.md&2.5&## Genetic algorithms
ais13-Genetic-Computing-Machine-Learning.md&2.5&
ais13-Genetic-Computing-Machine-Learning.md&2.5&Programs can evolve over many generations to perform particular tasks:
ais13-Genetic-Computing-Machine-Learning.md&2.5&
ais13-Genetic-Computing-Machine-Learning.md&2.5&>- Playing snake: <https://www.youtube.com/watch?v=3bhP7zulFfY>
ais13-Genetic-Computing-Machine-Learning.md&2.5&>- Cars evolving to drive up a mountain road without falling over: <http://rednuht.org/genetic_cars_2>
ais13-Genetic-Computing-Machine-Learning.md&2.5&>- Virtual creatures evolving to perform various tasks: <https://www.youtube.com/watch?v=bBt0imn77Zg>
ais13-Genetic-Computing-Machine-Learning.md&2.5&>- Learning to shoot an enemy creature: <https://www.youtube.com/watch?v=u2t77mQmJiY>
ais13-Genetic-Computing-Machine-Learning.md&2.5&>- More evolving cars: <https://www.youtube.com/watch?v=lmPJeKRs8gE>
ais13-Genetic-Computing-Machine-Learning.md&2.5&
ais13-Genetic-Computing-Machine-Learning.md&2.6&## Differences between neural networks and genetic algorithms
ais13-Genetic-Computing-Machine-Learning.md&2.6&
ais13-Genetic-Computing-Machine-Learning.md&2.6&- Neural networks adapt their weights inside one individual, during a training phase. The individual itself learns.
ais13-Genetic-Computing-Machine-Learning.md&2.6&- Genetic algorithms don’t (typically) change the genome of an individual. Instead, a whole population of individuals competes. The most successful ones survive and reproduce. The population as a whole ‘learns,’ not a particular individual.
ais13-Genetic-Computing-Machine-Learning.md&2.6&
ais13-Genetic-Computing-Machine-Learning.md&2.6&
ais13-Genetic-Computing-Machine-Learning.md&3.0&# Practical significance of neural and genetic computing
ais13-Genetic-Computing-Machine-Learning.md&3.0&
ais13-Genetic-Computing-Machine-Learning.md&3.1&## Consequences (1): No inspection of stored information
ais13-Genetic-Computing-Machine-Learning.md&3.1&
ais13-Genetic-Computing-Machine-Learning.md&3.1&>- Stored information is a vector of “meaningless” numbers.
ais13-Genetic-Computing-Machine-Learning.md&3.1&
ais13-Genetic-Computing-Machine-Learning.md&3.2&## Consequences (2): No full predictability of behaviour
ais13-Genetic-Computing-Machine-Learning.md&3.2&
ais13-Genetic-Computing-Machine-Learning.md&3.2&>- The behaviour of a trained ANN can only be inferred by observation in test cases (like it is with humans or animals).
ais13-Genetic-Computing-Machine-Learning.md&3.2&>- No number of tests can reliably cover every aspect of the ANN's future operation.
ais13-Genetic-Computing-Machine-Learning.md&3.2&>- Thus, the future behaviour of the ANN will never be fully predictable.
ais13-Genetic-Computing-Machine-Learning.md&3.2&
ais13-Genetic-Computing-Machine-Learning.md&3.3&## Consequences (3): Learning and application merge
ais13-Genetic-Computing-Machine-Learning.md&3.3&
ais13-Genetic-Computing-Machine-Learning.md&3.3&>- In reinforcement learning (RL), learning and operation happen at the same time.
ais13-Genetic-Computing-Machine-Learning.md&3.3&>- This means that the training phase is not under the control of a single trainer any more.
ais13-Genetic-Computing-Machine-Learning.md&3.3&>- The environment trains the ANN!
ais13-Genetic-Computing-Machine-Learning.md&3.3&>- This environment is not controlled and not predictable.
ais13-Genetic-Computing-Machine-Learning.md&3.3&>- Thus, the learned content and the future behaviour of the ANN is not controlled or predictable either.
ais13-Genetic-Computing-Machine-Learning.md&3.3&
ais13-Genetic-Computing-Machine-Learning.md&3.4&## Consequences (4): Inevitability of “error” with RL
ais13-Genetic-Computing-Machine-Learning.md&3.4&
ais13-Genetic-Computing-Machine-Learning.md&3.4&>- “Error” in an reinforcement learning artificial neural network is just another name for the exploration of possible new behaviours in order to optimise the system's operation.
ais13-Genetic-Computing-Machine-Learning.md&3.4&> “Error” is not an avoidable “mistake” any more, but a necessary feature of the adaptive system! (Like it is with animals and small children)
ais13-Genetic-Computing-Machine-Learning.md&3.4&
ais13-Genetic-Computing-Machine-Learning.md&3.5&## Genetic algorithms 
ais13-Genetic-Computing-Machine-Learning.md&3.5&
ais13-Genetic-Computing-Machine-Learning.md&3.5&>- Genetic algorithms have similar issues to neural networks.
ais13-Genetic-Computing-Machine-Learning.md&3.5&>- Additionally, they are extremely dependent on the environment, since their "training" is performed *by the environment.*
ais13-Genetic-Computing-Machine-Learning.md&3.5&>- Neural networks usually try to optimise a particular, narrowly defined behaviour, inside a system that is unchanging as a whole.
ais13-Genetic-Computing-Machine-Learning.md&3.5&>- Genetic algorithms usually are free to develop the whole morphology of the organism in totally new (and unexpected) ways in order to optimise the program's fitness.
ais13-Genetic-Computing-Machine-Learning.md&3.5&>- The programmer is not "programming" the particular creature any more. He becomes a "creator" of a software organism that then develops further on its own, entirely outside of the programmer's control.
ais13-Genetic-Computing-Machine-Learning.md&3.5&
ais16-Mind.md&0.1&---
ais16-Mind.md&0.1&title:  "AI and Society: 16. What is the mind?"
ais16-Mind.md&0.1&author: Andreas Matthias, Lingnan University
ais16-Mind.md&0.1&date: September 30, 2019
ais16-Mind.md&0.1&...
ais16-Mind.md&0.1&
ais16-Mind.md&1.0&# Main Theories in the Philosophy of Mind
ais16-Mind.md&1.0&
ais16-Mind.md&1.1&## Main theories
ais16-Mind.md&1.1&
ais16-Mind.md&1.1&There are two main theory groups in the Philosophy of Mind:
ais16-Mind.md&1.1&
ais16-Mind.md&1.1&>- Dualism
ais16-Mind.md&1.1&>     - Substance dualism
ais16-Mind.md&1.1&>     - Property dualism
ais16-Mind.md&1.1&>     - Epiphenomenalism
ais16-Mind.md&1.1&>- Monism
ais16-Mind.md&1.1&>     - Physicalism
ais16-Mind.md&1.1&>     - Materialism
ais16-Mind.md&1.1&>     - Reductive materialism (Identity Theory)
ais16-Mind.md&1.1&>     - Eliminative materialism
ais16-Mind.md&1.1&>- In addition, we need to understand
ais16-Mind.md&1.1&>     - Functionalism
ais16-Mind.md&1.1&>     - The Extended Mind Thesis
ais16-Mind.md&1.1&
ais16-Mind.md&2.0&# Summary: Selected theories of mind and their relevance to AI
ais16-Mind.md&2.0&
ais16-Mind.md&2.1&## Mental states
ais16-Mind.md&2.1&
ais16-Mind.md&2.1&The whole discussion is about the true nature of mental states, like:
ais16-Mind.md&2.1&
ais16-Mind.md&2.1&>- Pain, anger, thinking, beliefs, calculating, hope, visual perception (seeing “red”), qualia.
ais16-Mind.md&2.1&>- “Qualia:” The subjective “feeling” of consciousness or a sensory perception.
ais16-Mind.md&2.1&>     - *How it feels* to see red, or to taste chocolate.
ais16-Mind.md&2.1&
ais16-Mind.md&2.2&## Dualism: mind $\neq$ body
ais16-Mind.md&2.2&
ais16-Mind.md&2.2&>- Mind substance is different from body substance: *substance dualism.*
ais16-Mind.md&2.2&>- Problems:
ais16-Mind.md&2.2&>     - Brain damage leads to loss of cognitive abilities. Why?
ais16-Mind.md&2.2&>     - How can an immaterial cause can have a material effect if the material world is causally closed?
ais16-Mind.md&2.2&
ais16-Mind.md&2.3&## Dualism: Epiphenomenalism (1)
ais16-Mind.md&2.3&
ais16-Mind.md&2.3&Epiphenomenalism is a dualist theory, in which the body can have causal effects on the mind, but the mind cannot have causal effects on the body.
ais16-Mind.md&2.3&
ais16-Mind.md&2.3&This solves both problems above:
ais16-Mind.md&2.3&
ais16-Mind.md&2.3&>- Since the body can have causal effects on the mind, the theory explains why brain damage can impair cognitive abilities.
ais16-Mind.md&2.3&>- Since the mind cannot affect the body, the theory does not have to explain how an immaterial cause could affect the material world.
ais16-Mind.md&2.3&
ais16-Mind.md&2.4&## Dualism: Epiphenomenalism (2)
ais16-Mind.md&2.4&
ais16-Mind.md&2.4&>- So from a philosophical, argumentative perspective, it is a fine theory that solves the immediate problems of dualism. From the perspective of common sense, it is not a very satisfactory solution:
ais16-Mind.md&2.4&>     - Why would we have a mind that has no effect on our actions?
ais16-Mind.md&2.4&>     - Introspection disagrees: we think that our beliefs, intentions etc *do* affect our actions!
ais16-Mind.md&2.4&
ais16-Mind.md&2.5&## Monism: There is only one basis for both mind and body
ais16-Mind.md&2.5&
ais16-Mind.md&2.5&>- This one basis can be:
ais16-Mind.md&2.5&>     - material: materialism; or
ais16-Mind.md&2.5&>     - mental: idealism or solipsism (there’s only *one* mind around: me!)
ais16-Mind.md&2.5&>- Materialism can be:
ais16-Mind.md&2.5&>     - Reductive materialism (=identity theory): Mental states *are* brain states.
ais16-Mind.md&2.5&>     - Eliminative materialism: There are no mental states. All talk of mental states is misleading and talking about something that does not exist.
ais16-Mind.md&2.5&
ais16-Mind.md&2.6&## Reductive materialism = identity theory of mind (1)
ais16-Mind.md&2.6&
ais16-Mind.md&2.6&Mental states *are* brain states.
ais16-Mind.md&2.6&
ais16-Mind.md&2.6&>- “Reductive:” a “reductionist” explanation is one in which I explain a higher-level phenomenon completely in terms of a lower-level phenomenon. The higher-level concept can thus be completely “reduced” to a lower-level concept.
ais16-Mind.md&2.6&>- Examples:
ais16-Mind.md&2.6&>     - Heat is identical to the mean kinetic energy of molecules
ais16-Mind.md&2.6&>     - Sound is identical to waves of different air density
ais16-Mind.md&2.6&>     - Colour is identical to light of different wavelengths
ais16-Mind.md&2.6&>- These lower-level concepts completely explain the higher-level concept and can fully replace it. The higher-level concept is just an abbreviated way of speaking about the lower-level concept.
ais16-Mind.md&2.6&
ais16-Mind.md&2.7&## Reductive materialism = identity theory of mind (2)
ais16-Mind.md&2.7&
ais16-Mind.md&2.7&Supporting arguments:
ais16-Mind.md&2.7&
ais16-Mind.md&2.7&>- MRI and similar visualisations of working brains correlate well with mental states: When I am happy, particular areas in my brain show activity. When I am frightened or sad, other areas light up. This correlation is strong and consistent.
ais16-Mind.md&2.7&>- We can also reverse this effect: damage in particular areas of the brain will cause corresponding functions to be lost.
ais16-Mind.md&2.7&>- Or: stimulation of particular brain areas (or drugs) can cause particular mental states to appear (for example, phantom perceptions of light, smells etc)
ais16-Mind.md&2.7&
ais16-Mind.md&2.8&## Reductive materialism = identity theory of mind (3)
ais16-Mind.md&2.8&
ais16-Mind.md&2.8&Counter-arguments:
ais16-Mind.md&2.8&
ais16-Mind.md&2.8&>- No two people have the same brain. Brains develop differently in different individuals. Yet we all think that we have the same mental states (hope, fear, love etc).
ais16-Mind.md&2.8&>- Even more different are brains of other species (dogs, cats, birds). Still, we would want to maintain that cats or bird can “be afraid.” -- How could this be if my fear was *identical* to a particular physical state of my brain?
ais16-Mind.md&2.8&>- Subjective experience indicates that we indeed do have mental states and they don’t feel like they are only states of our brains.
ais16-Mind.md&2.8&
ais16-Mind.md&2.9&## Reductive materialism = identity theory of mind (4)
ais16-Mind.md&2.9&
ais16-Mind.md&2.9&>- Daniel Dennett (philosopher) describes a thought experiment where you keep your brain alive outside of your body, in a lab. Your brain is connected to your body via wireless signals. Now you (your body) walk over to your brain, sitting “there” in its box full of machinery to keep it alive, and look at it. Do you have the sense of looking at yourself in any way?
ais16-Mind.md&2.9&>     - (No). But is this a good argument? Any counter-arguments?
ais16-Mind.md&2.9&>     - This is  a weak argument: introspection is particularly bad in revealing information about our bodies. We don’t feel our blood circulation, we don’t have any introspection about how our digestion actually works, we are often mistaken about what foods are good for us, and consume bad foods that we, by introspection into our mistaken cravings, think that we want.
ais16-Mind.md&2.9&
ais16-Mind.md&2.9&
ais16-Mind.md&2.10&## Eliminative materialism (1)
ais16-Mind.md&2.10&
ais16-Mind.md&2.10&>- Eliminative materialism seeks to “eliminate” (remove) all mentalistic vocabulary.
ais16-Mind.md&2.10&>- Similar cases of elimination of misleading vocabulary in the history of science:
ais16-Mind.md&2.10&>     - Phlogiston (the supposed stuff that burns when there’s a fire)
ais16-Mind.md&2.10&>     - Cosmic ether (the supposed stuff that fills the void between stars and in which light waves propagate).
ais16-Mind.md&2.10&
ais16-Mind.md&2.11&## Eliminative materialism (2)
ais16-Mind.md&2.11&
ais16-Mind.md&2.11&>- The progress of science replaced these concepts entirely with better concepts. It eliminated phlogiston and the cosmic ether, because knowledge about oxidation in chemistry and the propagation of light in the vacuum did not any more require these wrong explanatory concepts!
ais16-Mind.md&2.11&>- In the same way, the eliminative materialist wants to get rid of the (supposedly misleading and wrong) mentalist terms in which we describe the mind.
ais16-Mind.md&2.11&>- Again, this does not seem plausible to most of us. Most of us *do* think that mental states are something real that needs to be explained rather than eliminated.
ais16-Mind.md&2.11&
ais16-Mind.md&2.11&
ais16-Mind.md&2.12&## Mary’s room (“qualia”) argument
ais16-Mind.md&2.12&
ais16-Mind.md&2.12&The "Mary's Room argument" or "knowledge argument" (Frank Jackson, 1982) is an argument *against* the identity theory of mind.
ais16-Mind.md&2.12&
ais16-Mind.md&2.13&## Mary's Room
ais16-Mind.md&2.13&
ais16-Mind.md&2.13&> Mary is a brilliant scientist who is, for whatever reason, forced to investigate the world from a black and white room via a black and white television monitor. She specializes in the neurophysiology of vision and acquires, let us suppose, all the physical information there is to obtain about what goes on when we see ripe tomatoes, or the sky, and use terms like ‘red’, ‘blue’, and so on. She discovers, for example, just which wavelength combinations from the sky stimulate the retina, and exactly how this produces via the central nervous system the contraction of the vocal cords and expulsion of air from the lungs that results in the uttering of the sentence ‘The sky is blue’. [...] What will happen when Mary is released from her black and white room or is given a color television monitor? Will she learn anything or not? (Jackson 1982)
ais16-Mind.md&2.13&
ais16-Mind.md&2.14&## Mary's Room
ais16-Mind.md&2.14&
ais16-Mind.md&2.14&>- Mary is a scientist who knows everything there is to know about the *science* of color, but has never *experienced* color.
ais16-Mind.md&2.14&>- The question that Jackson raises is: once she experiences color, does she learn anything new?
ais16-Mind.md&2.14&>- Jackson claims that she does.
ais16-Mind.md&2.14&
ais16-Mind.md&2.15&## Mary's Room
ais16-Mind.md&2.15&
ais16-Mind.md&2.15&The argument:
ais16-Mind.md&2.15&
ais16-Mind.md&2.15&>- Mary (before her release) knows everything *physical* there is to know about other people.
ais16-Mind.md&2.15&>- Mary (before her release) does not know everything there is to know about other people (because she learns something about them on her release).
ais16-Mind.md&2.15&>- Therefore, there are truths about other people (and herself) which escape the physicalist story.
ais16-Mind.md&2.15&
ais16-Mind.md&2.16&## What is it that Mary will learn?
ais16-Mind.md&2.16&
ais16-Mind.md&2.16&>- What is it exactly that Mary will learn that she didn't know before?
ais16-Mind.md&2.16&>- **Qualia: The way an experience *feels* to the subject of the experience.**
ais16-Mind.md&2.16&
ais16-Mind.md&2.17&## Qualia
ais16-Mind.md&2.17&
ais16-Mind.md&2.17&>- If Mary does learn something new, it shows that qualia (the subjective, qualitative properties of experiences) exist.
ais16-Mind.md&2.17&>- If Mary gains something after she leaves the room — if she acquires knowledge of a particular thing that she did not possess before — then that knowledge, Jackson argues, is knowledge of the qualia of seeing red.
ais16-Mind.md&2.17&>- Therefore, it must be conceded that qualia are real properties, since there is a difference between a person who has access to a particular quale and one who does not. (Wikipedia)
ais16-Mind.md&2.17&
ais16-Mind.md&2.18&## Refutation of physicalism
ais16-Mind.md&2.18&
ais16-Mind.md&2.18&>- Jackson argues that if Mary does learn something new upon experiencing color, then physicalism is false.
ais16-Mind.md&2.18&>- Specifically, the knowledge argument is an attack on the physicalist claim about the completeness of physical explanations of mental states.
ais16-Mind.md&2.18&>- Mary may know everything about the science of color perception, but can she know what the experience of red is like if she has never seen red?
ais16-Mind.md&2.18&>- Jackson says that, yes, she has learned something new, via experience, and hence, physicalism is false. (Wikipedia)
ais16-Mind.md&2.18&
ais16-Mind.md&2.19&## Counterarguments to Mary's Room
ais16-Mind.md&2.19&
ais16-Mind.md&2.19&>- Two meanings of “know”.
ais16-Mind.md&2.19&>     - "Knowing that"
ais16-Mind.md&2.19&>     - "Knowing how"
ais16-Mind.md&2.19&>- Possible pre-linguistic, pre-propositional knowledge from direct experience.
ais16-Mind.md&2.19&>- But can we call this "knowledge?"
ais16-Mind.md&2.19&>- The argument seems to be based on confusing these two types of knowledge.
ais16-Mind.md&2.19&>     - In the premise, Mary "knows (that)" all that is to know.
ais16-Mind.md&2.19&>     - In the conclusion Mary "learns (how)" something new.
ais16-Mind.md&2.19&>     - The argument is (possibly) not conclusive as stated.
ais16-Mind.md&2.19&>- Another counter-argument: If Mary already knows *everything* there is to know about colour, she would also know how "red feels like." There is no mysterious "qualia" left over to explain.
ais16-Mind.md&2.19&
ais16-Mind.md&2.20&## Functionalism (1)
ais16-Mind.md&2.20&
ais16-Mind.md&2.20&A mental state (e.g. fear) is defined by the role it plays in relation to:
ais16-Mind.md&2.20&
ais16-Mind.md&2.20&>- particular body states (screaming, trembling, high blood pressure)
ais16-Mind.md&2.20&>- the environment (a fearsome thing must be present),
ais16-Mind.md&2.20&>- and in relation to other mental states (beliefs: a fearsome thing is around; desire: to run away; perception: of something that causes fear).
ais16-Mind.md&2.20&
ais16-Mind.md&2.20&. . . 
ais16-Mind.md&2.20&
ais16-Mind.md&2.21&### Functionalism:
ais16-Mind.md&2.21&
ais16-Mind.md&2.21&The essential defining feature of a mental state is the set of its causal relations to environmental effects on the body, other mental states, and bodily behaviour.
ais16-Mind.md&2.21&
ais16-Mind.md&2.21&
ais16-Mind.md&2.22&## Functionalism (2)
ais16-Mind.md&2.22&
ais16-Mind.md&2.22&>- All these *together* **define** the mental state as what it is, and **justify** it as appropriate.
ais16-Mind.md&2.22&>- If, for example, the cause of fear in the environment was missing, we would call the fear *inappropriate* (a hallucination, perhaps), and deal with it by convincing the subject that his fear is not warranted.
ais16-Mind.md&2.22&>- Or if someone had the fear but not the desire to flee, or the fear without the belief that there is a danger present; then we would not accept such “fear” as appropriate and we would say that this person is confused, crazy etc.
ais16-Mind.md&2.22&>- Counter-argument: Chinese nation argument (see above).
ais16-Mind.md&2.22&
ais16-Mind.md&2.23&## Functionalism (3): Implementation independence
ais16-Mind.md&2.23&
ais16-Mind.md&2.23&>- For a particular mental state to be present, some *appropriate functional organisation* is needed, but *not* identical material organisation!
ais16-Mind.md&2.23&>- *Functional organisation $\neq$ physical implementation!*
ais16-Mind.md&2.23&>- This is called: *“Implementation independence.”*
ais16-Mind.md&2.23&>- “Physical implementation” means: how the function is realised in the physical world. How something is constructed physically in order to fulfil this function.
ais16-Mind.md&2.23&
ais16-Mind.md&2.24&## Functionalism (4): Implementation independence
ais16-Mind.md&2.24&
ais16-Mind.md&2.24&>- Example: Lecture notes are the same lecture notes no matter whether they are written by hand on paper, printed out, read aloud in class, or presented in a video.
ais16-Mind.md&2.24&>- The “lecture notes” are a functional concept that is independent of its physical realisation. 
ais16-Mind.md&2.24&>- We don’t call something “lecture notes” only because it’s printed, or because it is projected on a wall. The physical form of the lecture notes is not essential.
ais16-Mind.md&2.24&>- “Lecture notes” are defined as such by the functional relations they have to lectures, universities, a student’s career, examinations, class readings and other class work, and so on. 
ais16-Mind.md&2.24&>- Every piece of information that has the same functional relations will be considered “lecture notes,” no matter how it is delivered physically (as an audio file, a book, a movie).
ais16-Mind.md&2.24&
ais16-Mind.md&2.24&
ais16-Mind.md&2.25&## Functionalism (5): Hardware and software
ais16-Mind.md&2.25&
ais16-Mind.md&2.25&>- The functionalist understanding of mental states has sometimes been compared to the distinction between “hardware” and “software” in computing.
ais16-Mind.md&2.25&>- The same kind of software (for example, a web browser) can run on totally different hardware.
ais16-Mind.md&2.25&>- All these web browsers are functionally equivalent, although they are made by different companies, they are built in different ways, and they run on different and incompatible hardware. They run on big screens, wall projectors, and tiny phone screens.
ais16-Mind.md&2.25&>- In the same way, we can imagine the “mind” to be something like the “software” of the brain. 
ais16-Mind.md&2.25&>     - If this was the case, then the same mind could be “run” on different brains.
ais16-Mind.md&2.25&>     - Also, it would be possible to “run” the mind on a computer with entirely different hardware from the brain’s hardware.
ais16-Mind.md&2.25&>     - But all these different minds might be functionally equivalent: all might have equivalent (but differently realised!) states of fear, hope, beliefs and so on.
ais16-Mind.md&2.25&
ais16-Mind.md&2.26&## Functionalism (6): Physical realisability
ais16-Mind.md&2.26&
ais16-Mind.md&2.26&>- BUT: Implementation independence $\neq$ physical realisability!
ais16-Mind.md&2.26&>     - Some function can be thought of in an implementation-independent way, but be impossible to realise physically in this way!
ais16-Mind.md&2.26&>     - Examples:
ais16-Mind.md&2.26&>          - Very small humans (say, 1 mm small): the cells would become too small to be able to work.
ais16-Mind.md&2.26&>          - Very big insects: house-sized insects wouldn’t be able to breathe by air diffusion!
ais16-Mind.md&2.26&>- In a similar way, although “mind” might be implementation independent, it might be impossible to actually implement it outside of brains for physical reasons having to do with particular properties of the mind and the brain! (Nobody knows, of course).
ais16-Mind.md&2.26&>- A functionalist theory of mind leaves this empirical issue open.
ais16-Mind.md&2.26&
ais16-Mind.md&2.27&## Philosophical behaviourism
ais16-Mind.md&2.27&
ais16-Mind.md&2.27&>- Mental states *are* behaviours. They are just another way of talking about behaviours. 
ais16-Mind.md&2.27&>- If someone is “in fear,” then this is equivalent to saying that he will be exhibiting the behaviours of hiding or running away, sweating, having faster heartbeat etc.
ais16-Mind.md&2.27&>- “Being soluble” as analogy to “wants a Caribbean holiday”: operational definitions: “if I put X in water, it will dissolve.” If someone wants a Caribbean holiday, he will perform particular actions. If he doesn't, then obviously he doesn't want such a holiday.
ais16-Mind.md&2.27&
ais16-Mind.md&2.28&## Functionalism vs behaviourism
ais16-Mind.md&2.28&
ais16-Mind.md&2.28&>- *Can you give an example where we can clearly see how functionalism is different from behaviourism?*
ais16-Mind.md&2.28&>- Braitenberg vehicles:
ais16-Mind.md&2.28&>     - The behaviourist would say that if the vehicle behaves as if it feared the light, then it does fear the light. There is no sensible definition of "fear" beyond what can be expressed in the vehicle's behaviour.
ais16-Mind.md&2.28&>     - The functionalist would say that "fear" is a mental state that *is caused* by other mental states (like the belief that one saw something dangerous) and in turn *causes* other mental or body states (like the belief that running away will avoid the danger etc).
ais16-Mind.md&2.28&>     - The Braitenberg vehicle fulfils the conditions for behaviourist "fear," but it does not have the mental and body states required for genuine functionalist "fear." It just does not have any "beliefs" or other mental states that would qualify as functional equivalents of our mental states that accompany fear.
ais16-Mind.md&2.28&
ais16-Mind.md&2.29&## Functionalism vs behaviourism
ais16-Mind.md&2.29&
ais16-Mind.md&2.29&>- Another example: Actors on a stage, playing.
ais16-Mind.md&2.29&>- A behaviourist would watch them act “fear” and conclude that they have a mental state of fear.
ais16-Mind.md&2.29&>- A functionalist would notice that their supposed mental state of “fear” is not related in the appropriate way to other mental states: they don’t forget their lines, for example. They don’t have the desire to flee. They don’t have the belief that they are in danger. Therefore, they cannot have a genuine mental state of fear.
ais16-Mind.md&2.29&
ais16-Mind.md&2.29&
ais16-Mind.md&2.30&## Functionalism vs behaviourism
ais16-Mind.md&2.30&
ais16-Mind.md&2.30&>- You see now that the Chinese Room argument is a functionalist’s criticism of a behaviourist claim!
ais16-Mind.md&2.30&>- The Turing Test is a purely behaviourist position: “What behaves as if it was intelligent, *is* intelligent.”
ais16-Mind.md&2.30&>- The Chinese Room argument argues that behaviour is not enough. The Chinese Room *does* behave as if it could understand, but, looking at its functional states (the inside of the room and how it works) we can clearly see that it lacks the right functional organisation.
ais16-Mind.md&2.30&
ais16-Mind.md&2.30&
ais16-Mind.md&2.31&## Arguments against behaviourism
ais16-Mind.md&2.31&
ais16-Mind.md&2.31&>- Behaviourism denies inner aspect of mental states (e.g. “pain”).
ais16-Mind.md&2.31&>- Behaviours can be faked (for example, actors on a stage or in a movie).
ais16-Mind.md&2.31&>- Mental states can be present but not express themselves in behaviour (someone might be in fear, but paralysed and unable to run or scream).
ais16-Mind.md&2.31&>- Still, in the form of symbolic AI and the Turing Test, behaviourism about machine intelligence is a powerful idea in the history of AI.
ais16-Mind.md&2.31&
ais16-Mind.md&2.31&
ais16-Mind.md&2.32&## Arguments against functionalism: Chinese nation (1)
ais16-Mind.md&2.32&
ais16-Mind.md&2.32&Absent qualia problem (Chinese Nation argument): We already discussed this in session 4, but I want to remind you of this argument again here.
ais16-Mind.md&2.32&
ais16-Mind.md&2.32&. . . 
ais16-Mind.md&2.32&
ais16-Mind.md&2.32&>- What if we assigned each Chinese person the function of a neuron in our brain?
ais16-Mind.md&2.32&>- Chinese are used for this example because they are so many. (You couldn't make a working brain out of Greeks, for example.)
ais16-Mind.md&2.32&>- Assuming each Chinese person copies exactly the behaviour of a real neuron in a brain, and that we have as many Chinese as neurons in our brains available for the experiment,
ais16-Mind.md&2.32&>- Then we should be able to perfectly simulate a brain using Chinese people.
ais16-Mind.md&2.32&>- That brain should work *exactly* as the original brain.
ais16-Mind.md&2.32&>- It should have consciousness and qualia.
ais16-Mind.md&2.32&
ais16-Mind.md&2.33&## Arguments against functionalism: Chinese nation (2)
ais16-Mind.md&2.33&
ais16-Mind.md&2.33&>- But where do they come from? Each individual Chinese replaces only a single (dumb) neuron.
ais16-Mind.md&2.33&>- How does the consciousness come into the system? Is it at all plausible to assume that such a "brain" would be conscious and have real mental states?
ais16-Mind.md&2.33&>- If the Chinese brain couldn't have mental states, functionalism must be wrong.
ais16-Mind.md&2.33&>- *What do you think?*
ais16-Mind.md&2.33&
ais16-Mind.md&2.34&## Answers to the Chinese Nation argument (1)
ais16-Mind.md&2.34&
ais16-Mind.md&2.34&>- Possible answer 1: The Chinese Nation brain doesn't have qualia, and we don't have qualia either (since we are functionally equivalent to a Chinese Nation brain).
ais16-Mind.md&2.34&>     - This is not good for functionalism. It is counter-intuitive to say that we don't have qualia.
ais16-Mind.md&2.34&>- Possible answer 2: (Dualism:) The whole experiment misses the point. Qualia in the human mind are not material, and our brain is not functionally equivalent to the Chinese Nation system.
ais16-Mind.md&2.34&>     - This is not good for functionalism either. It rejects the functionalist premise entirely.
ais16-Mind.md&2.34&
ais16-Mind.md&2.35&## Answers to the Chinese Nation argument (2)
ais16-Mind.md&2.35&
ais16-Mind.md&2.35&>- Possible answer 3: The Chinese Nation brain *does* have qualia.
ais16-Mind.md&2.35&>     - The only position compatible with functionalism, but slightly implausible. *Who* is the perceiving subject of these qualia? (The Chinese Nation as a whole).
ais16-Mind.md&2.35&>     - This is similar to the Systems Reply to the Chinese Room argument.
ais16-Mind.md&2.35&>     - It opens up the possibility of *strong AI* (since computers can have qualia, and thus, a genuine *mind*).
ais16-Mind.md&2.35&>     - So, for the functionalist strong AI, the systems reply to the Chinese Room, and the assumption that the Chinese Nation brain would have qualia of its own all go together.
ais16-Mind.md&2.35&
ais16-Mind.md&2.35&
ais16-Mind.md&2.36&## Consequences of these theories for AI
ais16-Mind.md&2.36&
ais16-Mind.md&2.36&>- Theories of mind only have consequences for the *strong* claim of AI (“machines can have a mind and mental states”).
ais16-Mind.md&2.36&>- They don’t affect *weak* AI (“machines can act intelligently”).
ais16-Mind.md&2.36&
ais16-Mind.md&2.36&. . . 
ais16-Mind.md&2.36&
ais16-Mind.md&2.36&Effects of:
ais16-Mind.md&2.36&
ais16-Mind.md&2.36&>- Substance dualism: Computers could never have a mind, as long as we don’t find a way to put “mind substance” into them.
ais16-Mind.md&2.36&>- Reductive materialism: Computers could not have a mind, as long as they are not constructed out of brains. (Because mental states *are* brain states).
ais16-Mind.md&2.36&>- Functionalism: Machines *could* have minds, if they have the appropriate functional organisation. But to implement such minds outside of the brain *might* actually turn out to be physically impossible for some (yet unknown) reason.
ais16-Mind.md&2.36&
ais16-Mind.md&2.36&
ais16-Mind.md&3.0&# Functionalism in AI
ais16-Mind.md&3.0&
ais16-Mind.md&3.1&## Implementation independence (again)
ais16-Mind.md&3.1&
ais16-Mind.md&3.1&>- If functionalism in the theory of mind is correct, then it is at least logically possible for a mind essentially like ours to be made of quite different stuff from ours.
ais16-Mind.md&3.1&>- The reason is that what is essential are *the structures and their functional organisation,* not the *material* of which the elements of the structures are made.
ais16-Mind.md&3.1&>- A Turing machine can be implemented on *any* hardware! The states are completely abstract.
ais16-Mind.md&3.1&>- As a consequence, if the mind is a Turing machine, then the mind can be implemented in any hardware too!
ais16-Mind.md&3.1&
ais16-Mind.md&3.2&## Marr's levels of computation (1)
ais16-Mind.md&3.2&
ais16-Mind.md&3.2&>- Three levels of analysis for addressing a computational problem (David Marr, 1982)
ais16-Mind.md&3.2&>- At the **computational level** of analysis, one specifies *what* cognitive function is being computed (for example, an addition.)
ais16-Mind.md&3.2&>- At the **algorithmic level**, one describes *how* the function is being computed, the algorithm used to compute it (for example, how to perform an addition of two long numbers, in principle).
ais16-Mind.md&3.2&>- And at the **implementational level**, one describes *how the steps* of that algorithm are implemented, that is, the underlying mechanism that performs the computation (how to add two long numbers using pen and paper, where to make ink marks on the paper etc).
ais16-Mind.md&3.2&>- *Exercise: Describe "a wall" at the three levels of description!*
ais16-Mind.md&3.2&
ais16-Mind.md&3.3&## Marr's levels of computation (2)
ais16-Mind.md&3.3&
ais16-Mind.md&3.3&A wall:
ais16-Mind.md&3.3&
ais16-Mind.md&3.3&>- Computationally: A separation between two areas that makes it hard to cross from one area to the other.
ais16-Mind.md&3.3&>- Algorithmically: Determine the beginning and end point of each straight wall segment, and connect these two points with some kind of barrier. Repeat the same for all segments of the wall.
ais16-Mind.md&3.3&>- Implementation: Take bricks and mortar. Arrange the bricks in a row, put mortar on top, add a second row of bricks on top, then proceed in the same way for as many rows as desired.
ais16-Mind.md&3.3&
ais16-Mind.md&3.4&## Marr's levels of computation (3)
ais16-Mind.md&3.4&
ais16-Mind.md&3.4&
ais16-Mind.md&3.4&>- Note that there are *many different* but equivalent implementations for the same algorithm! There are walls of bricks, of cement, of wood, stones, etc.
ais16-Mind.md&3.4&>- Note also that the implementation is largely independent of the algorithm! The implementation talks only about wall segments, and does not address the problem of planning where the wall goes.
ais16-Mind.md&3.4&
ais16-Mind.md&3.5&## Marr's levels of computation (4)
ais16-Mind.md&3.5&
ais16-Mind.md&3.5&>- The three levels are relative to a computational problem.
ais16-Mind.md&3.5&>- What is at the implementational level relative to one computational problem can be at either the computational or algorithmic level relative to another.
ais16-Mind.md&3.5&>- Marr (1982) suggested that computational level analysis could be carried out *largely independently* of algorithmic level analysis, and the latter *largely independently* of implementational analysis. (See the wall example above!)
ais16-Mind.md&3.5&>- *Infinitely many algorithms can compute the same function.*
ais16-Mind.md&3.5&>- Moreover, the algorithmic level in turn underdetermines the implementational level: *a given algorithm can be implemented in infinitely many possible ways.*
ais16-Mind.md&3.5&
ais16-Mind.md&3.5&. . . 
ais16-Mind.md&3.5&
ais16-Mind.md&3.5&*What are the consequences of this?*
ais16-Mind.md&3.5&
ais16-Mind.md&3.6&## Functional equivalences
ais16-Mind.md&3.6&
ais16-Mind.md&3.6&>- This is the basis of the "functional equivalence" thesis!
ais16-Mind.md&3.6&>- It allows us to study the mind without studying neurons.
ais16-Mind.md&3.6&>- It lets us hope that we can implement a mind on a differently structured architecture.
ais16-Mind.md&3.6&
ais16-Mind.md&3.7&## Functional equivalences
ais16-Mind.md&3.7&
ais16-Mind.md&3.7&>- *Can you find examples of other functional equivalences in everyday life?*
ais16-Mind.md&3.7&>- A wall is a wall no matter whether it is made of mud, of stone, of wood, of human bodies.
ais16-Mind.md&3.7&>- A car is a car whether it is made of steel or plastic, and independently of the details of the engine which is inside it. The detailed structure of various cars is very different, yet they are all cars.
ais16-Mind.md&3.7&>- A university is a university only due to functional equivalence with other universities. Neither the arrangement of buildings, not even the number and size of buildings, the type of campus, or the numbers and identities of the students and professors make it a university. All that defines a university is equivalence in function.
ais17-Extended-Mind.md&0.1&---
ais17-Extended-Mind.md&0.1&title:  "AI and Society: 16. Extended Mind and Hybrid Agents"
ais17-Extended-Mind.md&0.1&author: Andreas Matthias, Lingnan University
ais17-Extended-Mind.md&0.1&date: October 16, 2019
ais17-Extended-Mind.md&0.1&bibliography: papers.bib
ais17-Extended-Mind.md&0.1&csl: apa.csl
ais17-Extended-Mind.md&0.1&...
ais17-Extended-Mind.md&0.1&
ais17-Extended-Mind.md&1.0&# Extended Mind and Hybrid Agents
ais17-Extended-Mind.md&1.0&
ais17-Extended-Mind.md&1.1&## Otto's notebook
ais17-Extended-Mind.md&1.1&
ais17-Extended-Mind.md&1.1&>- Otto suffers from Alzheimer’s disease.
ais17-Extended-Mind.md&1.1&>- Otto carries a notebook around with him everywhere he goes.
ais17-Extended-Mind.md&1.1&>- When he learns new information, he writes it down.
ais17-Extended-Mind.md&1.1&>- When he needs some old information, he looks it up.
ais17-Extended-Mind.md&1.1&
ais17-Extended-Mind.md&1.1&. . .
ais17-Extended-Mind.md&1.1&
ais17-Extended-Mind.md&1.1&> “Otto decides to see an exhibition at the Museum of Modern Art. He consults the notebook, which says that the museum is on 53rd Street, so he walks to 53rd Street and goes into the museum.
ais17-Extended-Mind.md&1.1&
ais17-Extended-Mind.md&1.1&. . .
ais17-Extended-Mind.md&1.1&
ais17-Extended-Mind.md&1.1&> For Otto, his notebook plays the role usually played by a biological memory.”
ais17-Extended-Mind.md&1.1&
ais17-Extended-Mind.md&1.1&[@clark1998extended 33; page numbers cited after @menary2010extended]
ais17-Extended-Mind.md&1.1&
ais17-Extended-Mind.md&1.2&## Tetris
ais17-Extended-Mind.md&1.2&
ais17-Extended-Mind.md&1.2&![](graphics/tetris-small.jpg)\ 
ais17-Extended-Mind.md&1.2&
ais17-Extended-Mind.md&1.2&
ais17-Extended-Mind.md&1.2&>- Kirsh and Maglio [-@kima1994]: the physical rotation of a shape through 90 degrees takes about 100 milliseconds, plus about 200 milliseconds to select the button.
ais17-Extended-Mind.md&1.2&>- To achieve the same result by mental rotation takes about 1,000 milliseconds.
ais17-Extended-Mind.md&1.2&>- "Physical rotation is used not just to position a shape ready to fit a slot, but often to help determine whether the shape and the slot are compatible." [@clark1998extended]
ais17-Extended-Mind.md&1.2&
ais17-Extended-Mind.md&1.3&## Epistemic actions
ais17-Extended-Mind.md&1.3&
ais17-Extended-Mind.md&1.3&This kind of shape rotation it Tetris is an "epistemic action."
ais17-Extended-Mind.md&1.3&
ais17-Extended-Mind.md&1.3&“Epistemic actions alter the world so as to aid and augment cognitive processes such as recognition and search. Merely pragmatic actions, by contrast, alter the world because some physical change is desirable for its own sake.”
ais17-Extended-Mind.md&1.3&
ais17-Extended-Mind.md&1.3&>- Epistemic action demands spread of epistemic credit.
ais17-Extended-Mind.md&1.3&>- If a part of the world functions in the same way as a cognitive process, then that part of the world *is* a cognitive process.
ais17-Extended-Mind.md&1.3&>- "Cognitive processes ain’t (all) in the head!” [@clark1998extended]
ais17-Extended-Mind.md&1.3&
ais17-Extended-Mind.md&2.0&# Criteria for a “mind”
ais17-Extended-Mind.md&2.0&
ais17-Extended-Mind.md&2.1&## Portability
ais17-Extended-Mind.md&2.1&
ais17-Extended-Mind.md&2.1&>- According to Clark and Chalmers, the mind must be “portable,” that is, its basic, core functions must always be available to the subject, no matter what the environment looks like.
ais17-Extended-Mind.md&2.1&>- One does not lose one’s mind in the desert or when one is walking on a beach, away from a desk and an Internet connection.
ais17-Extended-Mind.md&2.1&>- Therefore, such “easily decoupled” systems would not count as mind.
ais17-Extended-Mind.md&2.1&
ais17-Extended-Mind.md&2.2&## Reliability
ais17-Extended-Mind.md&2.2&
ais17-Extended-Mind.md&2.2&>- Similar to portability is reliability. 
ais17-Extended-Mind.md&2.2&>- We expect our cognitive resources to be *reliably* available.
ais17-Extended-Mind.md&2.2&>- If I can easily lose access to a calculator, for example, then the calculator should not be considered part of my mind.
ais17-Extended-Mind.md&2.2&>- But what if the calculator was built into my brain and always available?
ais17-Extended-Mind.md&2.2&
ais17-Extended-Mind.md&2.2&
ais17-Extended-Mind.md&3.0&# Seven versions of the claim
ais17-Extended-Mind.md&3.0&
ais17-Extended-Mind.md&3.1&## 1. Causal coupling and cognitive equivalence
ais17-Extended-Mind.md&3.1&
ais17-Extended-Mind.md&3.1&> “In these cases \[Tetris, notebook\], the human organism is linked with an external entity in a two-way interaction, creating a coupled system that can be seen as a cognitive system in its own right.” (p.29)
ais17-Extended-Mind.md&3.1&
ais17-Extended-Mind.md&3.2&## 2. Epistemic actions
ais17-Extended-Mind.md&3.2&
ais17-Extended-Mind.md&3.2&> “Epistemic actions alter the world so as to aid and augment cognitive processes such as recognition and search.” (p.28)
ais17-Extended-Mind.md&3.2&
ais17-Extended-Mind.md&3.3&## 3. Use of external computing resources
ais17-Extended-Mind.md&3.3&
ais17-Extended-Mind.md&3.3&> “Consider the use of pen and paper to perform long multiplication …, the use of physical rearrangements of letter tiles to prompt word recall in Scrabble … , and … language, books, diagrams, and culture.” (p.28)
ais17-Extended-Mind.md&3.3&
ais17-Extended-Mind.md&3.4&## 4. Extended beliefs
ais17-Extended-Mind.md&3.4&
ais17-Extended-Mind.md&3.4&> “What makes some information count as a belief is the role it plays, and there is no reason why the relevant role can be played only from inside the body.” (p. 35)
ais17-Extended-Mind.md&3.4&
ais17-Extended-Mind.md&3.5&## 5. Extended desires
ais17-Extended-Mind.md&3.5&
ais17-Extended-Mind.md&3.5&> “For example, the waiter at my favorite restaurant might act as a repository of my beliefs about my favorite meals (this might even be construed as a case of extended desire).” (p. 38)
ais17-Extended-Mind.md&3.5&
ais17-Extended-Mind.md&3.6&## 6. Extended mental states
ais17-Extended-Mind.md&3.6&
ais17-Extended-Mind.md&3.6&> “Could my mental states be partly constituted by the states of other thinkers? … In an unusually interdependent couple, it is entirely possible that one partner’s beliefs will play the same sort of role for the other as the notebook plays for Otto.” (p. 38)
ais17-Extended-Mind.md&3.6&
ais17-Extended-Mind.md&3.7&## 7. Spread selves
ais17-Extended-Mind.md&3.7&
ais17-Extended-Mind.md&3.7&> “Does the extended mind imply an extended self? It seems so … The information in Otto’s notebook ... is a central part of his identity as a cognitive agent. ... Otto himself is best regarded as an extended system, a coupling of biological organism and external resources.” (p. 39)
ais17-Extended-Mind.md&3.7&
ais17-Extended-Mind.md&3.8&## Problems and criticisms
ais17-Extended-Mind.md&3.8&
ais17-Extended-Mind.md&3.8&> * The coupling-constitution fallacy
ais17-Extended-Mind.md&3.8&>      - A thermostat is coupled to the room temperature, but the temperature does not *constitute* the thermostat. In the same way, the notebook is coupled to Otto's cognition, but not itself a cognitive process.
ais17-Extended-Mind.md&3.8&> * The "mark of the cognitive"
ais17-Extended-Mind.md&3.8&>      - The specifically "cognitive" property is the possession of underived mental representations. Extended "minds" don't have any, so they are not minds.
ais17-Extended-Mind.md&3.8&> * Cognitive equivalence
ais17-Extended-Mind.md&3.8&>     - If a system acts "as if" it was a cognitive system, then it is one. This is a functionalist position that can be criticized in various ways.
ais17-Extended-Mind.md&3.8&
ais17-Extended-Mind.md&4.0&# The hybrid agent (Bruno Latour)
ais17-Extended-Mind.md&4.0&
ais17-Extended-Mind.md&4.1&## Hybrids
ais17-Extended-Mind.md&4.1&
ais17-Extended-Mind.md&4.1&>- Except for a few, rare cases, we won't get autonomous machines that operate independently of humans, in a vacuum of their own.
ais17-Extended-Mind.md&4.1&>- Usually, autonomous agents will be machines that cooperate and interact closely with humans in order to perform their function:
ais17-Extended-Mind.md&4.1&	- a driver and an autonomous car
ais17-Extended-Mind.md&4.1&	- a hiker and Google Maps
ais17-Extended-Mind.md&4.1&	- a speaker, a listener, and Google Translate
ais17-Extended-Mind.md&4.1&	- a soldier and a drone
ais17-Extended-Mind.md&4.1&	- a shopper and Amazon's Alexa
ais17-Extended-Mind.md&4.1&	- a policeman and a law-enforcement robot
ais17-Extended-Mind.md&4.1&	- a doctor, a nurse, a patient, and a number of hospital robots
ais17-Extended-Mind.md&4.1&
ais17-Extended-Mind.md&4.2&## Classic (wrong) concept
ais17-Extended-Mind.md&4.2&
ais17-Extended-Mind.md&4.2&Let's look closer at how this works.
ais17-Extended-Mind.md&4.2&
ais17-Extended-Mind.md&4.2&![](graphics/human-uses-artefact.png)\
ais17-Extended-Mind.md&4.2&
ais17-Extended-Mind.md&4.2&
ais17-Extended-Mind.md&4.2&>- The human is the *agent.*
ais17-Extended-Mind.md&4.2&
ais17-Extended-Mind.md&4.3&## Classic (wrong) concept
ais17-Extended-Mind.md&4.3&
ais17-Extended-Mind.md&4.3&![](graphics/human-uses-artefact-2.png)\
ais17-Extended-Mind.md&4.3&
ais17-Extended-Mind.md&4.3&
ais17-Extended-Mind.md&4.3&- The human is the *agent.*
ais17-Extended-Mind.md&4.3&- It is in his mind that the decision to act is taken and the action plan is formed.
ais17-Extended-Mind.md&4.3&
ais17-Extended-Mind.md&4.4&## Classic (wrong) concept
ais17-Extended-Mind.md&4.4&
ais17-Extended-Mind.md&4.4&![](graphics/human-uses-artefact-3.png)\
ais17-Extended-Mind.md&4.4&
ais17-Extended-Mind.md&4.4&
ais17-Extended-Mind.md&4.4&- The human is the *agent.*
ais17-Extended-Mind.md&4.4&- It is in his mind that the decision to act is taken and the action plan is formed.
ais17-Extended-Mind.md&4.4&- After that decision has been taken, the human agent uses the artefact in the preconceived way to achieve his goal.
ais17-Extended-Mind.md&4.4&
ais17-Extended-Mind.md&4.4&
ais17-Extended-Mind.md&4.5&## Classic (wrong) concept
ais17-Extended-Mind.md&4.5&
ais17-Extended-Mind.md&4.5&![](graphics/human-uses-artefact-3.png)\
ais17-Extended-Mind.md&4.5&
ais17-Extended-Mind.md&4.5&
ais17-Extended-Mind.md&4.5&>- *This is possibly incorrect.*
ais17-Extended-Mind.md&4.5&>- Bruno Latour: The use of an artefact by an agent changes the behaviour of *both* the agent and the artefact.
ais17-Extended-Mind.md&4.5&
ais17-Extended-Mind.md&4.6&## Bruno Latour: Composite agents [@blhn]
ais17-Extended-Mind.md&4.6&
ais17-Extended-Mind.md&4.6&![](graphics/human-uses-artefact-4.png)\
ais17-Extended-Mind.md&4.6&
ais17-Extended-Mind.md&4.6&
ais17-Extended-Mind.md&4.6&>- Not only the gun is operating according to the wishes of its user.
ais17-Extended-Mind.md&4.6&>- Rather, it is in equal measure the gun that forces the user to behave *as a gun user.*
ais17-Extended-Mind.md&4.6&>     - forces him to assume the right posture for firing the gun,
ais17-Extended-Mind.md&4.6&>     - to stop moving while firing,
ais17-Extended-Mind.md&4.6&>     - to aim using the aiming mechanism of the gun... and so on.
ais17-Extended-Mind.md&4.6&
ais17-Extended-Mind.md&4.7&## Bruno Latour: Composite agents [@blhn]
ais17-Extended-Mind.md&4.7&
ais17-Extended-Mind.md&4.7&![](graphics/human-uses-artefact-5.png)\
ais17-Extended-Mind.md&4.7&
ais17-Extended-Mind.md&4.7&
ais17-Extended-Mind.md&4.7&Even more importantly, having a gun to his disposal, will change the user's *goals* as well as the methods he considers in order to achieve these goals (avoid or confront a danger).
ais17-Extended-Mind.md&4.7&
ais17-Extended-Mind.md&4.8&## Bruno Latour: Goal translation
ais17-Extended-Mind.md&4.8&
ais17-Extended-Mind.md&4.8&![](graphics/human-uses-artefact-6.png)\
ais17-Extended-Mind.md&4.8&
ais17-Extended-Mind.md&4.8&
ais17-Extended-Mind.md&4.8&>- The "composite agent" composed of *myself and the gun* is thus a different agent, with different goals and methods at his disposal, than the original agent (me without the gun) had been.
ais17-Extended-Mind.md&4.8&>- It's not any more "me using the gun."
ais17-Extended-Mind.md&4.8&>- Composite agent $\xrightarrow[considering\ collective\ properties]{using\ collective\  resources}$ Goal (translated)
ais17-Extended-Mind.md&4.8&>- My options to make a moral choice are constrained (and sometimes determined) by the properties of the composite system.
ais17-Extended-Mind.md&4.8&
ais17-Extended-Mind.md&4.9&## Bruno Latour: Goal translation
ais17-Extended-Mind.md&4.9&
ais17-Extended-Mind.md&4.9&Examples:
ais17-Extended-Mind.md&4.9&
ais17-Extended-Mind.md&4.9&>- Original goal: Make peace with enemy to minimise casualties
ais17-Extended-Mind.md&4.9&>- Available tool: Drones that can kill without endangering our soldiers.
ais17-Extended-Mind.md&4.9&>- Goal after translation: Bomb enemy with drones.
ais17-Extended-Mind.md&4.9&
ais17-Extended-Mind.md&4.9&. . .
ais17-Extended-Mind.md&4.9&
ais17-Extended-Mind.md&4.9&>- Original goal: Drive home safely, don't drink beer before driving.
ais17-Extended-Mind.md&4.9&>- Available tool: Tesla autopilot that effectively works (even if illegal without supervision).
ais17-Extended-Mind.md&4.9&>- Goal after translation: Drive home drunk and nap in the car.
ais17-Extended-Mind.md&4.9&
ais17-Extended-Mind.md&4.10&## Artefact design
ais17-Extended-Mind.md&4.10&
ais17-Extended-Mind.md&4.10&>- The design and properties of the artefact in composite agents determine my options to act.
ais17-Extended-Mind.md&4.10&>- The design of the artefact (and how I can use it in the pursuit of my goals) determine:
ais17-Extended-Mind.md&4.10&>     - The amount of *control* I have over the artefact.
ais17-Extended-Mind.md&4.10&>     - The degree to which I can be held responsible for the collective action (because responsibility requires effective control over the action!)
ais17-Extended-Mind.md&4.10&>     - The extent to which the artefact will encourage or require a translation of my original goals to the capabilities of the composite agent.
ais17-Extended-Mind.md&4.10&>- Thus: **The design of the artefact in composite agents becomes morally relevant.**
ais17-Extended-Mind.md&4.10&
ais17-Extended-Mind.md&4.10&
ais17-Extended-Mind.md&5.0&# Epistemic credit and hybrid agents
ais17-Extended-Mind.md&5.0&
ais17-Extended-Mind.md&5.1&## Epistemic credit and hybrid agents
ais17-Extended-Mind.md&5.1&
ais17-Extended-Mind.md&5.1&>- The extended mind thesis is a thesis about the externalised, shared execution of an algorithm, which leads to shared agency.
ais17-Extended-Mind.md&5.1&>- *Hybrid agents:* Computational aggregates, that are composed of internal and external parts, which jointly achieve cognitive results that lead to them acting in a particular way as agents.
ais17-Extended-Mind.md&5.1&>- Hybrid agents are not multiple, cooperating agents. Their components fuse into *one* agent.
ais17-Extended-Mind.md&5.1&
ais17-Extended-Mind.md&5.1&
ais17-Extended-Mind.md&6.0&# The epistemic disadvantage of humans
ais17-Extended-Mind.md&6.0&
ais17-Extended-Mind.md&6.1&## Epistemic disadvantage
ais17-Extended-Mind.md&6.1&
ais17-Extended-Mind.md&6.1&>- Supervising a child or a dog is easy. Why?
ais17-Extended-Mind.md&6.1&>- *My* ability to understand the likely outcomes of my actions and to predict the behaviour of the world around me is more developed than that of the child or animal I am supervising.
ais17-Extended-Mind.md&6.1&>- The *epistemic advantage* I have over the supervised agent makes me a suitable supervisor.
ais17-Extended-Mind.md&6.1&>- I have a greater degree of control over the interactions of the supervised agent with his environment than he would have if left to his own devices.
ais17-Extended-Mind.md&6.1&
ais17-Extended-Mind.md&6.2&## Epistemic advantage, supervision, and responsibility
ais17-Extended-Mind.md&6.2&
ais17-Extended-Mind.md&6.2&>- By knowing more than the child, I am in a position to foresee harm both to the child and to the environment, and thus to avert it.
ais17-Extended-Mind.md&6.2&>- This higher degree of control also bestows on me a higher responsibility: commonly parents as well as dog owners are held responsible for the actions of their children and pets, as long as they have been supervising them at the moment the action occurred.
ais17-Extended-Mind.md&6.2&
ais17-Extended-Mind.md&6.3&## Hybrids of humans and non-humans
ais17-Extended-Mind.md&6.3&
ais17-Extended-Mind.md&6.3&>- Is it the same when supervising a computational hybrid agent?
ais17-Extended-Mind.md&6.3&>- In computational hybrids of humans with non-humans, it is often the *human* part who is at an *epistemic disadvantage.*
ais17-Extended-Mind.md&6.3&>- At the same time, it is the *human* whom we traditionally single out as the bearer of responsibility for the actions of the hybrid system.
ais17-Extended-Mind.md&6.3&
ais17-Extended-Mind.md&6.4&## Epistemic disadvantage of humans in hybrid agents
ais17-Extended-Mind.md&6.4&
ais17-Extended-Mind.md&6.4&Supervising a nuclear power plant:
ais17-Extended-Mind.md&6.4&
ais17-Extended-Mind.md&6.4&>- As opposed to supervising a dog, I have to rely on artificial sensors (radioactivity, high temperatures, high pressures) without which I am unable to receive crucial information.
ais17-Extended-Mind.md&6.4&>- Without artificial sensors and artificial computational devices I am not able to control the nuclear power plant *at all.*
ais17-Extended-Mind.md&6.4&
ais17-Extended-Mind.md&6.5&## Computational externalism and epistemic disadvantage (1)
ais17-Extended-Mind.md&6.5&
ais17-Extended-Mind.md&6.5&>- I and the control devices of the power plant constitute a hybrid agent.
ais17-Extended-Mind.md&6.5&>- Control of the power plant is obtained through the execution of an algorithm, but this algorithm cannot be executed by the human brain and its sensors alone.
ais17-Extended-Mind.md&6.5&>- Additional sensors are necessary (for example, radioactivity, or high temperature sensors), but also high-speed computations, that make it possible to coordinate the various subsystems of the power plant in real time (see Clark&Chalmers Tetris example!)
ais17-Extended-Mind.md&6.5&>- Part of the algorithm that controls the power plant is *necessarily* executed outside of human bodies, and therefore *no human can be said to be "controlling" the power plant.*
ais17-Extended-Mind.md&6.5&>- The human *together* with the computers and sensors and switches is in control. The hybrid agent is.
ais17-Extended-Mind.md&6.5&
ais17-Extended-Mind.md&6.6&## Computational externalism and epistemic disadvantage (2)
ais17-Extended-Mind.md&6.6&
ais17-Extended-Mind.md&6.6&The phenomenon is common:
ais17-Extended-Mind.md&6.6&
ais17-Extended-Mind.md&6.6&- Control systems of air planes and air traffic
ais17-Extended-Mind.md&6.6&- Deep space exploration devices
ais17-Extended-Mind.md&6.6&- Military self-targeting missiles
ais17-Extended-Mind.md&6.6&- Internet search engines
ais17-Extended-Mind.md&6.6&
ais17-Extended-Mind.md&6.6&>- There is no way a human could outperform or effectively supervise such machines.
ais17-Extended-Mind.md&6.6&>- He is a slave to their decisions
ais17-Extended-Mind.md&6.6&>- He is physically unable to control and supervise them in real-time. 
ais17-Extended-Mind.md&6.6&
ais17-Extended-Mind.md&6.7&## Epistemic disadvantage and responsibility
ais17-Extended-Mind.md&6.7&
ais17-Extended-Mind.md&6.7&A common misconception when talking about hybrid agents:
ais17-Extended-Mind.md&6.7&
ais17-Extended-Mind.md&6.7&"Humans should exercise better control over the actions of the non-human part, supervise it more effectively, so that negative consequences of the operation of the hybrid entity can be seen in advance and averted."
ais17-Extended-Mind.md&6.7&
ais17-Extended-Mind.md&6.7&. . .
ais17-Extended-Mind.md&6.7&
ais17-Extended-Mind.md&6.7&>- Responsibility for a process and its consequences can only be ascribed to someone who is in effective *control* of that process
ais17-Extended-Mind.md&6.7&>- Ascribing responsibility to agents who are in a position of epistemic disadvantage is not just, and poses problems of justification.
ais17-Extended-Mind.md&6.7&
ais17-Extended-Mind.md&7.0&# Examples
ais17-Extended-Mind.md&7.0&
ais17-Extended-Mind.md&7.1&## Self-driving car
ais17-Extended-Mind.md&7.1&
ais17-Extended-Mind.md&7.1&>- Example: Tesla website (November 2019):
ais17-Extended-Mind.md&7.1&>     - “Eight surround cameras provide 360 degrees of visibility around the car at up to 250 meters of range.”
ais17-Extended-Mind.md&7.1&>     - “Twelve updated ultrasonic sensors complement this vision, allowing for detection of both hard and soft objects.”
ais17-Extended-Mind.md&7.1&>     - “A forward-facing radar with enhanced processing provides additional data about the world on a redundant wavelength that is able to see through heavy rain, fog, dust and even the car ahead.” (https://www.tesla.com/en_HK/autopilot)
ais17-Extended-Mind.md&7.1&>- You can see how this could be superior to the senses of a human driver under particular circumstances (rain, fog, dust, blind spots).
ais17-Extended-Mind.md&7.1&>- Can a human driver really be responsible for the car’s actions under conditions where the car clearly has to know better?
ais17-Extended-Mind.md&7.1&
ais17-Extended-Mind.md&7.2&## Boeing MCAS system (1)
ais17-Extended-Mind.md&7.2&
ais17-Extended-Mind.md&7.2&>- “Maneuvering Characteristics Augmentation System.”
ais17-Extended-Mind.md&7.2&>- Designed to make some newer Boeing 737 models behave like older models, so that pilots can fly them without having to be specially trained for the new model.
ais17-Extended-Mind.md&7.2&>- The MCAS also is meant to prevent pilots from putting the plane into a dangerous flight configuration.
ais17-Extended-Mind.md&7.2&>- Essentially, the pilot’s steering inputs don’t directly control the plane, but they first go into a computer and the computer decides how to translate the pilot’s input into movements of the plane’s aerodynamic surfaces.
ais17-Extended-Mind.md&7.2&
ais17-Extended-Mind.md&7.3&## Boeing MCAS system (2)
ais17-Extended-Mind.md&7.3&
ais17-Extended-Mind.md&7.3&>- One of the functions of the MCAS is to push the aircraft’s nose down when the airspeed is too low, in order to make the airplane gain more speed.
ais17-Extended-Mind.md&7.3&>- As opposed to a system that just assists the pilot, the MCAS has the full authority to bring the aircraft nose down, and can not be overridden by the pilot.
ais17-Extended-Mind.md&7.3&>- The MCAS led to two crashes: Lion Air Flight 610 (October 2018) and Ethiopian Airlines Flight 302 (March 2019). 
ais17-Extended-Mind.md&7.3&
ais17-Extended-Mind.md&7.4&## Boeing MCAS system (3)
ais17-Extended-Mind.md&7.4&
ais17-Extended-Mind.md&7.4&>- In both cases, it seems that the sensors that told the MCAS how fast the plane was flying and how high its nose was pointing, were giving wrong values to the system. 
ais17-Extended-Mind.md&7.4&>- The MCAS, thinking that the plane’s nose was too high (while it wasn’t) pushed the nose down, causing the plane to crash.
ais17-Extended-Mind.md&7.4&>- In principle, the pilot could cut electricity to the plane’s tail controls, thus deactivating all automatic control of the plane’s angle. But pilots were never told how to do that, and the MCAS was not mentioned or explained in the official Boeing training manuals.
ais17-Extended-Mind.md&7.4&
ais17-Extended-Mind.md&7.5&## Boeing MCAS system (4)
ais17-Extended-Mind.md&7.5&
ais17-Extended-Mind.md&7.5&>- *So, who is responsible for the crashes?*
ais17-Extended-Mind.md&7.5&>     - Boeing installed a potentially dangerous system that overrides pilot input and that cannot be switched off easily.
ais17-Extended-Mind.md&7.5&>     - Boeing did not inform the airlines or the pilots how to deal with the new system in cases of malfunction.
ais17-Extended-Mind.md&7.5&>     - Most airlines did not order additional indicators (that would have cost more money and that were optional) that could have alerted the pilots to the problem.
ais17-Extended-Mind.md&7.5&>     - The pilots could have reacted better by disabling the horizontal stabiliser power supply and disabling the MCAS.
ais17-Extended-Mind.md&7.5&>     - Some pilots were able to come up with this solution on other flights (that did not crash). The pilots on the crashed flights did not. (<https://www.theguardian.com/world/2019/mar/20/lion-air-pilots-were-looking-at-handbook-when-plane-crashed>)
ais17-Extended-Mind.md&7.5&
ais17-Extended-Mind.md&7.6&## Responsibility for accidents involving autonomous systems
ais17-Extended-Mind.md&7.6&
ais17-Extended-Mind.md&7.6&>- This shows how difficult it is to ascribe responsibility for the failure of complex, computerised, autonomous systems. The manufacturer, the designer, the owner and the operator all share responsibility for accidents to some extent.
ais17-Extended-Mind.md&7.6&>- The same is true of Tesla car crashes on autopilot mode:
ais17-Extended-Mind.md&7.6&>     - Partially, accidents can be attributed to errors in programming caused by Tesla.
ais17-Extended-Mind.md&7.6&>     - Partially, they are the fault of the car owners, who use autopilot without staying alert on the car’s wheel.
ais17-Extended-Mind.md&7.6&>     - But: can we even expect humans to stay alert in a car that drives itself, without doing anything? It is very difficult to keep one’s concentration on the road if one isn’t actually controlling the car. 
ais17-Extended-Mind.md&7.6&>     - So here’s the failure of the designer to design the system so that it keeps the driver alert, instead of luring him into believing that he is safe.
ais17-Extended-Mind.md&7.6&
ais17-Extended-Mind.md&7.7&## Overconfidence in autonomous technologies
ais17-Extended-Mind.md&7.7&
ais17-Extended-Mind.md&7.7&>- Overestimation of what the autonomous system can do (and the resulting overconfidence in the system’s performance) is one of the greatest problems with autonomous technologies.
ais17-Extended-Mind.md&7.7&>- On the one hand, companies want to sell the autonomous product, which causes them to exaggerate its abilities.
ais17-Extended-Mind.md&7.7&>- On the other hand, we want the users to be conscious of the product’s limitations and to not use it where it cannot perform safely.
ais17-Extended-Mind.md&7.7&>- This is a conflict that seems unavoidable. The only way out is very strict state regulation of autonomous technologies and how they can be advertised.
ais17-Extended-Mind.md&7.7&
ais17-Extended-Mind.md&7.7&
ais17-Extended-Mind.md&7.7&
ais17-Extended-Mind.md&8.0&# Conclusions
ais17-Extended-Mind.md&8.0&
ais17-Extended-Mind.md&8.1&## Conclusion (1)
ais17-Extended-Mind.md&8.1&
ais17-Extended-Mind.md&8.1&>- The Extended Mind Thesis shows how the components of a hybrid agent can jointly execute *one* single act.
ais17-Extended-Mind.md&8.1&>- In this case it is impossible to ascribe credit or blame for the result to any particular subsystem of the joint agent.
ais17-Extended-Mind.md&8.1&>- Bruno Latour: "Using" an artefact is not a one-way action, but an interaction, a back-and-forth between agent and artefact, in which both have to change and to adapt, forming a new, compound agent.
ais17-Extended-Mind.md&8.1&
ais17-Extended-Mind.md&8.2&## Conclusion (2)
ais17-Extended-Mind.md&8.2&
ais17-Extended-Mind.md&8.2&>- Humans as part of hybrid agents cannot effectively control the agent:
ais17-Extended-Mind.md&8.2&>      - Because of epistemic disadvantage and limited access to sensory data and computational resources.
ais17-Extended-Mind.md&8.2&>      - Because of the lack of an incentive to dispute the machine's decisions (the "man-in-the-loop fallacy"). We will talk about this later (see discussion of Arkin).
ais17-Extended-Mind.md&8.2&
ais17-Extended-Mind.md&8.3&## Conclusion (3)
ais17-Extended-Mind.md&8.3&
ais17-Extended-Mind.md&8.3&>- Classic models of responsibility ascription operate under the mistaken assumption that there are agents and, separately, objects of agency, things that the agents make use of in executing their agency.
ais17-Extended-Mind.md&8.3&>- This might be wrong, particularly when we talk about hybrid agents where the cognitive algorithm is executed in part outside of the human component's body, that is, by the artefact.
ais17-Extended-Mind.md&8.3&>- In these cases, the agent and the extended components of his agency form a computational hybrid, *which cannot be any more separated into distinct parts for the purposes of responsibility ascription.*
ais17-Extended-Mind.md&8.3&>- A joint agent is one unit, one indivisible whole, and responsibility for its actions diffuses across the whole of it.
ais17-Extended-Mind.md&8.3&
ais17-Extended-Mind.md&8.4&## Possible consequences for society
ais17-Extended-Mind.md&8.4&
ais17-Extended-Mind.md&8.4&>- Accepting cognitive externalism might force us to acknowledge that artefacts are not passive tools of human autonomy.
ais17-Extended-Mind.md&8.4&>- They might crucially shape human intentions and options for action.
ais17-Extended-Mind.md&8.4&>- Actions performed by hybrid agents might require a new distribution of responsibility between the parts of the hybrid agent.
ais17-Extended-Mind.md&8.4&>- The idea of total human autonomy might, in the context of actions involving artefacts, be a fiction.
ais17-Extended-Mind.md&8.4&
ais17-Extended-Mind.md&8.5&## The role of the designer
ais17-Extended-Mind.md&8.5&
ais17-Extended-Mind.md&8.5&>- Designers of artefacts have influence on the decisions humans will take when using these artefacts (Latour).
ais17-Extended-Mind.md&8.5&>- Therefore, designers of such artefacts might share responsibility for the actions performed using the artefacts.
ais17-Extended-Mind.md&8.5&>- Artefact design might need to be legally regulated with these issues in mind.
ais17-Extended-Mind.md&8.5&
ais17-Extended-Mind.md&8.6&## References
ais17-Extended-Mind.md&8.6&\footnotesize 
ais17-Extended-Mind.md&8.6&
ais18-Cyborgs.md&0.1&---
ais18-Cyborgs.md&0.1&title:  "AI and Society: 18. Hybrots, Connectomes and Cyborgs"
ais18-Cyborgs.md&0.1&author: Andreas Matthias, Lingnan University
ais18-Cyborgs.md&0.1&date: November 1, 2019
ais18-Cyborgs.md&0.1&bibliography: papers.bib
ais18-Cyborgs.md&0.1&csl: andreas-matthias-apa.csl
ais18-Cyborgs.md&0.1&...
ais18-Cyborgs.md&0.1&
ais18-Cyborgs.md&1.0&# Artificial connectomes
ais18-Cyborgs.md&1.0&
ais18-Cyborgs.md&1.1&## What is a connectome?
ais18-Cyborgs.md&1.1&
ais18-Cyborgs.md&1.1&> A connectome is a comprehensive map of neural connections in the brain, and may be thought of as its "wiring diagram". More broadly, a connectome would include the mapping of all neural connections within an organism's nervous system. (Wikipedia)
ais18-Cyborgs.md&1.1&
ais18-Cyborgs.md&1.2&## Connectome example: Caenorhabditis elegans
ais18-Cyborgs.md&1.2&
ais18-Cyborgs.md&1.2&![](graphics/05-Celegans.jpg)\ 
ais18-Cyborgs.md&1.2&
ais18-Cyborgs.md&1.2&- The C. elegans is a little worm (about 1mm in length).
ais18-Cyborgs.md&1.2&- The male has 1031 cells and 302 neurons.
ais18-Cyborgs.md&1.2&- Its genome has been sequenced completely, and it’s the only organism of which we have a complete neural map.
ais18-Cyborgs.md&1.2&
ais18-Cyborgs.md&1.3&## Connectome example: C. elegans
ais18-Cyborgs.md&1.3&
ais18-Cyborgs.md&1.3&A brief introduction to C. elegans:
ais18-Cyborgs.md&1.3&
ais18-Cyborgs.md&1.3&<https://www.youtube.com/watch?v=zjqLwPgLnV0>
ais18-Cyborgs.md&1.3&
ais18-Cyborgs.md&1.4&## Touch sensitivity
ais18-Cyborgs.md&1.4&
ais18-Cyborgs.md&1.4&>- Gentle touch stimulus delivered to the body (Chalfie and Sulston, 1981; Sulston et al., 1975)
ais18-Cyborgs.md&1.4&>- Harsh touch to the midbody (Way and Chalfie, 1989)
ais18-Cyborgs.md&1.4&>- Harsh touch to the head or tail (Chalfie and Wolinsky, 1990)
ais18-Cyborgs.md&1.4&>- Nose touch (Kaplan and Horvitz, 1993) and texture (Sawin et al., 2000).
ais18-Cyborgs.md&1.4&>- (Source: http://www.wormbook.org/chapters/www_behavior/behavior.html#sec2)
ais18-Cyborgs.md&1.4&
ais18-Cyborgs.md&1.5&## Nose behaviours
ais18-Cyborgs.md&1.5&
ais18-Cyborgs.md&1.5&>- Animals respond to touch to the side of the nose by rapidly moving their nose away from the stimulus; this is called head withdrawal.
ais18-Cyborgs.md&1.5&>- C. elegans responds to gentle touch to the nose by initiating backward locomotion. (Kaplan and Horvitz, 1993)
ais18-Cyborgs.md&1.5&>- (Source: http://www.wormbook.org/chapters/www_behavior/behavior.html#sec2)
ais18-Cyborgs.md&1.5&
ais18-Cyborgs.md&1.6&## Reaction to chemicals
ais18-Cyborgs.md&1.6&
ais18-Cyborgs.md&1.6&>- The worm swims towards low salt concentrations, but away from high salt concentrations.
ais18-Cyborgs.md&1.6&>- Its senses get used to salt, so it will later tolerate higher salt concentrations after having been for some time in an area with lower salt concentration.
ais18-Cyborgs.md&1.6&>- (Source: http://www.wormbook.org/chapters/www_behavior/behavior.html#sec2)
ais18-Cyborgs.md&1.6&
ais18-Cyborgs.md&1.7&## C. elegans driving robots
ais18-Cyborgs.md&1.7&
ais18-Cyborgs.md&1.7&C. elegans neurorobotics:
ais18-Cyborgs.md&1.7&
ais18-Cyborgs.md&1.7&<https://www.youtube.com/watch?v=YWQnzylhgHc>
ais18-Cyborgs.md&1.7&
ais18-Cyborgs.md&1.8&## Human connectome project
ais18-Cyborgs.md&1.8&
ais18-Cyborgs.md&1.8&Timeline:
ais18-Cyborgs.md&1.8&
ais18-Cyborgs.md&1.8&- Phase I (2010-2012): research teams designed the project’s 16 major components.
ais18-Cyborgs.md&1.8&- Phase II (2012-2015): the various scientists performed the actual work of gathering data.
ais18-Cyborgs.md&1.8&
ais18-Cyborgs.md&1.8&Datasets are publicly available.
ais18-Cyborgs.md&1.8&
ais18-Cyborgs.md&1.9&## Human connectome project
ais18-Cyborgs.md&1.9&
ais18-Cyborgs.md&1.9&- <http://www.humanconnectomeproject.org/>
ais18-Cyborgs.md&1.9&- <http://www.humanconnectomeproject.org/informatics/relationship-viewer/>
ais18-Cyborgs.md&1.9&
ais18-Cyborgs.md&1.9&Data releases:
ais18-Cyborgs.md&1.9&
ais18-Cyborgs.md&1.9&- <https://www.humanconnectome.org/study/hcp-young-adult/data-releases>
ais18-Cyborgs.md&1.9&
ais18-Cyborgs.md&1.10&## Some outcomes
ais18-Cyborgs.md&1.10&
ais18-Cyborgs.md&1.10&>- The connecting fibers are organized into an orderly 3D grid, where axons run up and down and left and right, without diagonals or tangles.
ais18-Cyborgs.md&1.10&>- The brain’s layout is like a city, with its streets running in two directions and buildings’ elevators running up and down.
ais18-Cyborgs.md&1.10&>- Strangely, in flat areas of the grid, the fibers overlap at precise 90 degree angles and weave together much like a fabric.
ais18-Cyborgs.md&1.10&>- The resolution is not sufficient to see how individual neurons connect to each other.
ais18-Cyborgs.md&1.10&
ais18-Cyborgs.md&1.11&## Some outcomes
ais18-Cyborgs.md&1.11&
ais18-Cyborgs.md&1.11&>- Perhaps knowing how individual neurons connect is necessary to understand differences between individual people.
ais18-Cyborgs.md&1.11&>- Strong relationship between positive behavior traits and the wiring of your brain. While some brains appear to be wired for a lifestyle that includes education and high levels of satisfaction, other brains appear to be wired for anger, rule-breaking, and substance use, according to the Oxford researchers.
ais18-Cyborgs.md&1.11&>- Connectivity profiles are "intrinsic," acting "as a 'fingerprint' that can accurately identify subjects from a large group," regardless of how the brain is engaged during the scan.
ais18-Cyborgs.md&1.11&>- Connectivity profiles even predicted levels of fluid intelligence and cognitive behavior.
ais18-Cyborgs.md&1.11&>- (Source: <http://www.medicaldaily.com/10-faq-about-human-connectome-project-astonishing-sister-nihs-human-genome-project-362650>)
ais18-Cyborgs.md&1.11&
ais18-Cyborgs.md&1.12&## Significance of these outcomes?
ais18-Cyborgs.md&1.12&
ais18-Cyborgs.md&1.12&>- No detailed neuronal map of the brain yet (as opposed to C.elegans).
ais18-Cyborgs.md&1.12&>- Prediction of intelligence (?) and behaviour traits.
ais18-Cyborgs.md&1.12&>- Definition of intelligence? Problems of IQ tests?
ais18-Cyborgs.md&1.12&>- Identification of neural infrastructure for rule-breaking and law enforcement?
ais18-Cyborgs.md&1.12&>- Use of connectome data by dictatorial regimes?
ais18-Cyborgs.md&1.12&>- Use by insurance companies?
ais18-Cyborgs.md&1.12&>- Use by prospective marriage partners?
ais18-Cyborgs.md&1.12&>- Use by banks giving study loans to students?
ais18-Cyborgs.md&1.12&
ais18-Cyborgs.md&2.0&# Hybrots
ais18-Cyborgs.md&2.0&
ais18-Cyborgs.md&2.1&## Hybrots
ais18-Cyborgs.md&2.1&
ais18-Cyborgs.md&2.1&>- Rat Brain robot: <https://www.youtube.com/watch?v=1-0eZytv6Qk>
ais18-Cyborgs.md&2.1&>- More complex rat brain robot: <https://www.youtube.com/watch?v=1QPiF4-iu6g>
ais18-Cyborgs.md&2.1&>- A hybrot (short for "hybrid robot") is a cybernetic organism in the form of a robot controlled by a computer consisting of both electronic and biological elements. The biological elements are typically rat neurons connected to a computer chip.
ais18-Cyborgs.md&2.1&>- Source: https://en.wikipedia.org/wiki/Hybrot
ais18-Cyborgs.md&2.1&
ais18-Cyborgs.md&2.2&## Steve Potter’s hybrots
ais18-Cyborgs.md&2.2&
ais18-Cyborgs.md&2.2&>- Steve Potter, a professor of biomedical engineering at the Georgia Institute of Technology.
ais18-Cyborgs.md&2.2&>- Thousands of neuron cells are put onto a chip that contains 60 electrodes that read the neuron’s electrical activity.
ais18-Cyborgs.md&2.2&>- The electrical signals are then converted and sent to a robot’s motors.
ais18-Cyborgs.md&2.2&>- Every movement of the robot is the direct result of a signal from the neurons. The light sensors of the robot also send signals back to the neurons.
ais18-Cyborgs.md&2.2&
ais18-Cyborgs.md&2.3&## Source
ais18-Cyborgs.md&2.3&
ais18-Cyborgs.md&2.3&- Bakkum, D., Shkolnik, A., Ben-Ary, G., Gamblen, P., DeMarse, T., & Potter, S. (2004). Removing some ‘A’from AI: embodied cultured networks. Embodied artificial intelligence, 629-629.
ais18-Cyborgs.md&2.3&
ais18-Cyborgs.md&2.3&(In Moodle as: Potter-Removing-A-from-AI-NOTES.pdf)
ais18-Cyborgs.md&2.3&
ais18-Cyborgs.md&2.4&## Hybrots vs cyborgs
ais18-Cyborgs.md&2.4&
ais18-Cyborgs.md&2.4&>- Cyborg: a human or animal that has “enhanced” capabilities by incorporating technological parts.
ais18-Cyborgs.md&2.4&>- Hybrot: a new kind of creature that has a partly biological nervous system in an artificial, robot body.
ais18-Cyborgs.md&2.4&>- Hybrots live longer than isolated neurons. Neurons separated from a living brain usually die after only a couple of months. In a hybrot, the same cells can live for up to two years. (https://en.wikipedia.org/wiki/Hybrot)
ais18-Cyborgs.md&2.4&
ais18-Cyborgs.md&2.5&## Differences between connectome-based robots and hybrots
ais18-Cyborgs.md&2.5&
ais18-Cyborgs.md&2.5&>- What are some morally significant differences between connectome-based robots and hybrots?
ais18-Cyborgs.md&2.5&>- Connectome-based robots:
ais18-Cyborgs.md&2.5&>     - Not real neurons.
ais18-Cyborgs.md&2.5&>     - Mental states (for example, pain) are only simulated, not real.
ais18-Cyborgs.md&2.5&>     - The animal’s brain is completely simulated, and does not deserve more protection than a character in a computer game.
ais18-Cyborgs.md&2.5&>     - But is this true? What if we had a robot based on a complete human connectome, that reacts entirely human?
ais18-Cyborgs.md&2.5&>- Hybrots:
ais18-Cyborgs.md&2.5&>     - Real cells.
ais18-Cyborgs.md&2.5&>     - Therefore, real mental (=neuronal) states.
ais18-Cyborgs.md&2.5&>     - Real body, real neurons. Animal might have a claim to be protected, or to not be mistreated.
ais18-Cyborgs.md&2.5&
ais18-Cyborgs.md&3.0&# Warwick: Cyborg morals, cyborg values, cyborg ethics
ais18-Cyborgs.md&3.0&
ais18-Cyborgs.md&3.1&## Warwick: Cyborg morals, cyborg values, cyborg ethics
ais18-Cyborgs.md&3.1&
ais18-Cyborgs.md&3.1&Warwick, Kevin (2003): Cyborg morals, cyborg values, cyborg ethics. Ethics and Information Technology 5: 131–137.
ais18-Cyborgs.md&3.1&
ais18-Cyborgs.md&3.1&We will discuss this article in the following section.
ais18-Cyborgs.md&3.1&
ais18-Cyborgs.md&3.2&## What is a cyborg?
ais18-Cyborgs.md&3.2&
ais18-Cyborgs.md&3.2&>- Blind man with cane?
ais18-Cyborgs.md&3.2&>- Hearing aids?
ais18-Cyborgs.md&3.2&>- Glasses?
ais18-Cyborgs.md&3.2&>- Wearable computers?
ais18-Cyborgs.md&3.2&>- Cochlea implants, hip replacements, heart pacemakers?
ais18-Cyborgs.md&3.2&
ais18-Cyborgs.md&3.3&## Repair vs enhance
ais18-Cyborgs.md&3.3&
ais18-Cyborgs.md&3.3&>- Technologies to repair deficiencies vs. technologies to enhance human function.
ais18-Cyborgs.md&3.3&>- The important dividing line here would be the “normal” level of human functioning.
ais18-Cyborgs.md&3.3&>- Military technologies:
ais18-Cyborgs.md&3.3&>     - Night (IR) vision in helmet.
ais18-Cyborgs.md&3.3&>     - Fighter pilot helmets with weapon targeting aids built-in.
ais18-Cyborgs.md&3.3&
ais18-Cyborgs.md&3.4&## Enhancements in the animal world
ais18-Cyborgs.md&3.4&
ais18-Cyborgs.md&3.4&>- Spiders building webs?
ais18-Cyborgs.md&3.4&>- Chimpanzee using a stick to reach a banana?
ais18-Cyborgs.md&3.4&>- In both cases, only bodily function is enhanced, not cognitive ability!
ais18-Cyborgs.md&3.4&>- Same with glasses, hearing aids etc.
ais18-Cyborgs.md&3.4&>- The moral question becomes important when we talk about cognitive enhancement!
ais18-Cyborgs.md&3.4&>- Direct link between environmental input and human brain, rather than through the senses.
ais18-Cyborgs.md&3.4&>- Cyborg thus defined as “a unit formed by a human/machine brain/nervous system coupling.” (Warwick).
ais18-Cyborgs.md&3.4&>- Only such cyborgs considered in the paper.
ais18-Cyborgs.md&3.4&
ais18-Cyborgs.md&3.5&## Human autonomy
ais18-Cyborgs.md&3.5&
ais18-Cyborgs.md&3.5&>- Human with glasses (computerized or not), remains autonomous.
ais18-Cyborgs.md&3.5&>- Direct neural link:
ais18-Cyborgs.md&3.5&>     - Puts individuality in question.
ais18-Cyborgs.md&3.5&>     - Allows autonomy to be compromised.
ais18-Cyborgs.md&3.5&
ais18-Cyborgs.md&3.6&## Main question of the paper
ais18-Cyborgs.md&3.6&
ais18-Cyborgs.md&3.6&> The main question arising from this discourse being: when an individual’s consciousness is based on a part human part machine nervous system, in particular when they exhibit Cyborg consciousness, will they also hold to Cyborg morals, values and ethics? These being potentially distinctly different to human morals, values and ethics. Also, as a consequence, will cyborgs, acting as post humans, regard humans in a Nietzscheian like way (Nietzsche 1961 [corrected spelling!]) rather akin to how humans presently regard cows or chimpanzees?
ais18-Cyborgs.md&3.6&
ais18-Cyborgs.md&3.7&## Differences between human and machine intelligence
ais18-Cyborgs.md&3.7&
ais18-Cyborgs.md&3.7&>- Calculating speed.
ais18-Cyborgs.md&3.7&>- Perfect memory and retrieval.
ais18-Cyborgs.md&3.7&>- Additional senses (x-rays, radioactivity, IR, ...)
ais18-Cyborgs.md&3.7&>- Handling of additional dimensions (human brain limited to 3 plus time).
ais18-Cyborgs.md&3.7&>- Human communication: Low-bandwidth, high error rates. Machines: low error rate, parallel messaging, high-bandwidth, high-speed.
ais18-Cyborgs.md&3.7&
ais18-Cyborgs.md&3.8&## Advantages of becoming a cyborg
ais18-Cyborgs.md&3.8&
ais18-Cyborgs.md&3.8&>- With a human brain linked to a computer brain, that individual could have the ability to:
ais18-Cyborgs.md&3.8&>     - use the computer part for rapid maths
ais18-Cyborgs.md&3.8&>     - call on an internet knowledge base, quickly
ais18-Cyborgs.md&3.8&>     - have memories that they have not themselves had
ais18-Cyborgs.md&3.8&>     - sense the world in a plethora of ways
ais18-Cyborgs.md&3.8&>     - understand multi dimensionality
ais18-Cyborgs.md&3.8&>     - communicate in parallel, by thought signals alone, i.e., brain to brain
ais18-Cyborgs.md&3.8&>- But:
ais18-Cyborgs.md&3.8&>     - At what cost?
ais18-Cyborgs.md&3.8&>     - What might the consequences be?
ais18-Cyborgs.md&3.8&>     - What about the problems associated with actually becoming a cyborg?
ais18-Cyborgs.md&3.8&
ais18-Cyborgs.md&3.9&## The 1997 cockroach experiment
ais18-Cyborgs.md&3.9&
ais18-Cyborgs.md&3.9&>- 1997: University of Tokyo: motor neurons of a cockroach interfaced with microprocessor.
ais18-Cyborgs.md&3.9&>- Microprocessor sends signals to cockroach body.
ais18-Cyborgs.md&3.9&>- Cockroach has to act out computer commands, independent of its own volition.
ais18-Cyborgs.md&3.9&
ais18-Cyborgs.md&3.10&## Brain-controlled machines
ais18-Cyborgs.md&3.10&
ais18-Cyborgs.md&3.10&>- Today many examples of thought-controlled BCI (brain-computer-interface) applications.
ais18-Cyborgs.md&3.10&>- Brain-controlled wheelchairs (go to minute 2:00):
ais18-Cyborgs.md&3.10&>- <https://www.youtube.com/watch?v=yNpOgEbKHbw> 
ais18-Cyborgs.md&3.10&
ais18-Cyborgs.md&3.11&## Warwick’s first chip implant
ais18-Cyborgs.md&3.11&
ais18-Cyborgs.md&3.11&>- In 1998, he had an identifying chip implanted in his arm.
ais18-Cyborgs.md&3.11&>- Automated door opening, computer login, location tracking.
ais18-Cyborgs.md&3.11&>- Initially worried about tracking, but later found it natural.
ais18-Cyborgs.md&3.11&>- Just like our use of credit cards.
ais18-Cyborgs.md&3.11&>- He felt attached to the implant and to his computer.
ais18-Cyborgs.md&3.11&
ais18-Cyborgs.md&3.12&## Removal of the implant
ais18-Cyborgs.md&3.12&
ais18-Cyborgs.md&3.12&>- “Subsequently, when the implant was removed, on the one hand I felt relieved because of the medical problems that could have occurred, but on the other hand something was missing, it was as though a friend had died.”
ais18-Cyborgs.md&3.12&>- “If I had to draw one conclusion from my experience it would be that when linked with technology inside my body, it is no longer a separate piece of technology.”
ais18-Cyborgs.md&3.12&>- “Mentally I regard such technology as just as much part of me as my arms and legs. If my brain was linked with a computer it is difficult to imagine where I would feel my body ended.”
ais18-Cyborgs.md&3.12&
ais18-Cyborgs.md&3.13&## Second implant
ais18-Cyborgs.md&3.13&
ais18-Cyborgs.md&3.13&>- 2002: Direct links with nerves in the arm.
ais18-Cyborgs.md&3.13&>- Implant could read and trigger nerve action.
ais18-Cyborgs.md&3.13&>- Fed ultrasound sensor output directly into his own arm nerves.
ais18-Cyborgs.md&3.13&>- Could store arm movements on computer and play them back to his arm.
ais18-Cyborgs.md&3.13&>- Could record emotional events (anger etc) on the computer and play the signals back to his arm.
ais18-Cyborgs.md&3.13&>- His wife got an implant too. They could send signals to each other.
ais18-Cyborgs.md&3.13&>- Sent arm signals from the US to a robot hand in the UK.
ais18-Cyborgs.md&3.13&
ais18-Cyborgs.md&3.14&## Implications
ais18-Cyborgs.md&3.14&
ais18-Cyborgs.md&3.14&>- Useful for treatment of medical conditions, for example paralysis.
ais18-Cyborgs.md&3.14&>- But: remote controlling of other people’s movements?
ais18-Cyborgs.md&3.14&>- Helping blind people by introducing a new sense to them.
ais18-Cyborgs.md&3.14&>- If brains could transmit thoughts in this way, would speech become obsolete?
ais18-Cyborgs.md&3.14&>- If cyborgs are connected to a network, do they give up their individuality?
ais18-Cyborgs.md&3.14&>- Should every human have a right to be upgraded to a cyborg?
ais18-Cyborgs.md&3.14&>- Would non-upgraded individuals be seen by the cyborgs like chimpanzees?
ais18-Cyborgs.md&3.14&
ais18-Cyborgs.md&4.0&# Trimper: Ethics of brain interfaces
ais18-Cyborgs.md&4.0&
ais18-Cyborgs.md&4.1&## Trimper: Ethics of brain interfaces
ais18-Cyborgs.md&4.1&
ais18-Cyborgs.md&4.1&Trimper, J. B., Wolpe, P. R., & Rommelfanger, K. S. (2014). When “I” becomes “We”: ethical implications of emerging brain-to-brain interfacing technologies. Frontiers in Neuroengineering, 7, 4.
ais18-Cyborgs.md&4.1&
ais18-Cyborgs.md&4.1&See file in Moodle: Trimper-Ethics-of-Brain-Interfacing-Technologies.pdf
ais18-Cyborgs.md&4.1&
ais18-Cyborgs.md&4.2&## BTBI
ais18-Cyborgs.md&4.2&
ais18-Cyborgs.md&4.2&>- BTBI: Brain-to-Brain-Interfaces.
ais18-Cyborgs.md&4.2&>- Two rat classes: encoders and decoders.
ais18-Cyborgs.md&4.2&>- Rats had to choose “rewarded” lever. Encoders saw a light. Decoders did not see the light, but chose the right lever more often than by chance.
ais18-Cyborgs.md&4.2&>- BTBI provides a direct access to another brain, bypassing the senses.
ais18-Cyborgs.md&4.2&
ais18-Cyborgs.md&4.3&## Moral problems of BTBI
ais18-Cyborgs.md&4.3&
ais18-Cyborgs.md&4.3&Problems related to:
ais18-Cyborgs.md&4.3&
ais18-Cyborgs.md&4.3&>- Extraction of information
ais18-Cyborgs.md&4.3&>- Delivery of information
ais18-Cyborgs.md&4.3&>- Combination of the two
ais18-Cyborgs.md&4.3&
ais18-Cyborgs.md&4.4&## BTBI: Privacy (1)
ais18-Cyborgs.md&4.4&
ais18-Cyborgs.md&4.4&>- Via connectome research, brain data might be identifiable as those of particular persons.
ais18-Cyborgs.md&4.4&>- fMRI technologies can already reconstruct the images seen by subjects:
ais18-Cyborgs.md&4.4&>     - <https://www.youtube.com/watch?v=nsjDnYxJ0bo&feature=youtu.be>
ais18-Cyborgs.md&4.4&>     - This is not a live reconstruction of what the subjects see. Paper (Nishimoto 2011) explains the details, but they don’t matter to us now.
ais18-Cyborgs.md&4.4&>- New dimension: not only extraction of information from one brain, but also introduction into a new brain. The receiver brain has no way to block (refuse or inhibit) the transmission.
ais18-Cyborgs.md&4.4&>- BTBI information can be transmitted over the Internet. This makes BTBI interfaces hackable.
ais18-Cyborgs.md&4.4&
ais18-Cyborgs.md&4.5&## BTBI: Privacy (2)
ais18-Cyborgs.md&4.5&
ais18-Cyborgs.md&4.5&>- “Choi (2013) decoded with high accuracy the shoulder and arm joint movements of participants based on non-invasive recordings. If this information could be transferred via stimulation to the relevant regions of a second individual’s brain, it may be possible to control the limbs of the receiving subject via encoder’s neural activity.” (Trimper)
ais18-Cyborgs.md&4.5&>- In the future, it might be possible to remote-control people with this technology (prisoners, for instance).
ais18-Cyborgs.md&4.5&
ais18-Cyborgs.md&4.6&## BTBI: Cognitive enhancement
ais18-Cyborgs.md&4.6&
ais18-Cyborgs.md&4.6&>- Possible advantage to students from coupling brains.
ais18-Cyborgs.md&4.6&>- BTBI (expensive) might widen social gap in education.
ais18-Cyborgs.md&4.6&>- People are already experimenting with Transcranial Direct Current Stimulation Devices (tDCS) that are supposed to increase focus.
ais18-Cyborgs.md&4.6&
ais18-Cyborgs.md&4.7&## BTBI: Some moral problems
ais18-Cyborgs.md&4.7&
ais18-Cyborgs.md&4.7&>- Non-therapeutical use of BCIs violates individual authenticity, disrespects the “limits of nature,” and puts us as risk of losing “what makes us human” (?)
ais18-Cyborgs.md&4.7&>- Loss of autonomy and the potential for coercive control over another creature?
ais18-Cyborgs.md&4.7&>- Concept of self: If one is defined by his or her neurophysiology, then how is individuality defined when a brain is synched with another’s, or perhaps many others’?
ais18-Cyborgs.md&4.7&>- Will this create a sense of communal identity?
ais18-Cyborgs.md&4.7&>- Responsibility ascription to actions of “decoders”? Are they at all responsible for their actions?
ais18-Cyborgs.md&4.7&>- Ownership of BTBI generated ideas: If ideas are generated during the use of BTBI, who rightfully owns those ideas?
ais18-Cyborgs.md&4.7&
ais18-Cyborgs.md&4.7&
ais18-Cyborgs.md&5.0&# Allhoff and Lin: Ethics of Human Enhancement: 25 Questions and Answers
ais18-Cyborgs.md&5.0&
ais18-Cyborgs.md&5.1&## I. Definition and Distinctions
ais18-Cyborgs.md&5.1&
ais18-Cyborgs.md&5.1&>- What is human enhancement?
ais18-Cyborgs.md&5.1&>- Natural-artificial distinction.
ais18-Cyborgs.md&5.1&>- Internal-external distinction.
ais18-Cyborgs.md&5.1&>- Therapy-enhancement distinction.
ais18-Cyborgs.md&5.1&
ais18-Cyborgs.md&5.2&## What is human enhancement? (1)
ais18-Cyborgs.md&5.2&
ais18-Cyborgs.md&5.2&“Natural” human enhancement:
ais18-Cyborgs.md&5.2&
ais18-Cyborgs.md&5.2&> “Any activity by which we improve our bodies,  minds, or abilities—things we do to enhance our well-being. So reading a book, eating vegetables, doing homework, and exercising may count as enhancing ourselves.” (p.3)
ais18-Cyborgs.md&5.2&
ais18-Cyborgs.md&5.3&## What is human enhancement? (2)
ais18-Cyborgs.md&5.3&
ais18-Cyborgs.md&5.3&Artificial human enhancement:
ais18-Cyborgs.md&5.3&
ais18-Cyborgs.md&5.3&>- “Boosting our capabilities beyond the species-typical level or statistically-normal range of functioning for an individual.” (p.3)
ais18-Cyborgs.md&5.3&>- Human enhancement is different from therapy.
ais18-Cyborgs.md&5.3&>- Therapy: “Treatments aimed at pathologies that compromise health or reduce one’s level of functioning below this species-typical or statistically-normal level ...” (p.3)
ais18-Cyborgs.md&5.3&>- Human enhancement, as opposed to therapy, changes the structure and function of the body (Greely, 2005).
ais18-Cyborgs.md&5.3&
ais18-Cyborgs.md&5.4&## Natural-artificial distinction
ais18-Cyborgs.md&5.4&
ais18-Cyborgs.md&5.4&>- Difficult to defend.
ais18-Cyborgs.md&5.4&>- The term “natural” is vague.
ais18-Cyborgs.md&5.4&>- “For instance, if we can consider X to be natural if X exists without any human intervention or can be performed without human-engineered artifacts, then eating food (that is merely found but perhaps not farmed) and exercising (e.g., running barefoot but not lifting dumbbells) would still be considered natural, but reading a book no longer qualifies as a natural activity (enhancement or not), since books do not exist without humans.” (p.4)
ais18-Cyborgs.md&5.4&>- Often, the natural-artificial distinction is theologically motivated.
ais18-Cyborgs.md&5.4&
ais18-Cyborgs.md&5.5&## Internal-external distinction
ais18-Cyborgs.md&5.5&
ais18-Cyborgs.md&5.5&>- Enhancement is not only “use of tools.”
ais18-Cyborgs.md&5.5&>- Enhancement: “Tools integrated into our bodies.” (p.5)
ais18-Cyborgs.md&5.5&>- What is so special about incorporating tools as part of our bodies? Why should this be the criterion for enhancement?
ais18-Cyborgs.md&5.5&>- Integrating tools into our bodies gives “unprecedented advantages.”
ais18-Cyborgs.md&5.5&>     - Easier, immediate, always-on access.
ais18-Cyborgs.md&5.5&>     - Intimate, enhanced connection with the tools.
ais18-Cyborgs.md&5.5&>     - Substantial advantage for the enhanced person.
ais18-Cyborgs.md&5.5&>- Grey area: enhanced clothing, glasses.
ais18-Cyborgs.md&5.5&>- Conclusion: This distinction is not central to the problem.
ais18-Cyborgs.md&5.5&
ais18-Cyborgs.md&5.6&## Therapy-enhancement distinction
ais18-Cyborgs.md&5.6&
ais18-Cyborgs.md&5.6&>- Is there really a difference between therapy and enhancement?
ais18-Cyborgs.md&5.6&>     - Vaccinations?
ais18-Cyborgs.md&5.6&>     - If vaccinations are enhancements, should they be restricted?
ais18-Cyborgs.md&5.6&>     - If not, is our definition of enhancement wrong?
ais18-Cyborgs.md&5.6&>     - If a genius suffers a head injury that reduces his IQ to normal levels, would returning him to his normal “superhuman” level be a therapy or an enhancement?
ais18-Cyborgs.md&5.6&>     - More specifically, what is the baseline for the “normal” state that therapy seeks to restore?
ais18-Cyborgs.md&5.6&>- But common sense seems to expect this distinction, so we should keep making it.
ais18-Cyborgs.md&5.6&
ais18-Cyborgs.md&5.6&
ais18-Cyborgs.md&5.7&## II. Contexts and Scenarios
ais18-Cyborgs.md&5.7&
ais18-Cyborgs.md&5.7&>- Does the context matter?
ais18-Cyborgs.md&5.7&>- Examples of enhancement for cognitive performance.
ais18-Cyborgs.md&5.7&>- Examples of enhancement for physical performance.
ais18-Cyborgs.md&5.7&>- Should a non-therapeutic procedure that provides no net benefit be called an “enhancement”?
ais18-Cyborgs.md&5.7&
ais18-Cyborgs.md&5.8&## Does the context matter?
ais18-Cyborgs.md&5.8&
ais18-Cyborgs.md&5.8&>- We have to consider the *historical, social and cultural context* of enhancement technologies.
ais18-Cyborgs.md&5.8&>- For example, fossil fuel use is morally different in 1910 from 2010!
ais18-Cyborgs.md&5.8&>- The morality of human enhancements depends on context too.
ais18-Cyborgs.md&5.8&>     - Perhaps future societies have adapted to human enhancements.
ais18-Cyborgs.md&5.8&>     - For example, if in future sports events *all* participants are enhanced, or there are separate competitions for enhanced and not-enhanced participants, then there would be no moral problem.
ais18-Cyborgs.md&5.8&>     - Sometimes, the injustice created by access to a technology might be preferable to not having the technology at all (vaccinations, Wikipedia).
ais18-Cyborgs.md&5.8&
ais18-Cyborgs.md&5.9&## Examples of enhancement for cognitive performance
ais18-Cyborgs.md&5.9&
ais18-Cyborgs.md&5.9&>- Ritalin: medicine that can enhance concentration.
ais18-Cyborgs.md&5.9&>- Anxiety-reducing medicines.
ais18-Cyborgs.md&5.9&>- Recreational drugs used to relax and increase creativity.
ais18-Cyborgs.md&5.9&>- Future: neural implants, interfaces to knowledge databases, remote sensors, and other technologies.
ais18-Cyborgs.md&5.9&
ais18-Cyborgs.md&5.10&## Examples of enhancement for physical performance
ais18-Cyborgs.md&5.10&
ais18-Cyborgs.md&5.10&>- Steroids for athletes increase physical performance.
ais18-Cyborgs.md&5.10&>- Cosmetic surgery to increase perceived attractiveness.
ais18-Cyborgs.md&5.10&>- Prosthetic limbs and exoskeletons increase physical strength.
ais18-Cyborgs.md&5.10&>- Life extension technologies (diet, medical procedures, or uploading of brain content into a machine).
ais18-Cyborgs.md&5.10&
ais18-Cyborgs.md&5.11&## Should a non-therapeutic procedure that provides no net benefit be called an “enhancement”?
ais18-Cyborgs.md&5.11&
ais18-Cyborgs.md&5.11&>- Transforming into a cat, lizard or snake.
ais18-Cyborgs.md&5.11&>- Transforming the body to swim better.
ais18-Cyborgs.md&5.11&>- Morality of un-enhancements?
ais18-Cyborgs.md&5.11&>     - Amputate healthy limbs?
ais18-Cyborgs.md&5.11&>     - Deaf parents choose a deaf child through in-vitro fertilisation?
ais18-Cyborgs.md&5.11&
ais18-Cyborgs.md&5.12&## III. Freedom and Autonomy
ais18-Cyborgs.md&5.12&
ais18-Cyborgs.md&5.12&>- Can we justify human enhancement technologies by appealing to our right to be free?
ais18-Cyborgs.md&5.12&>     - Freedom is always restricted in society.
ais18-Cyborgs.md&5.12&>     - Some restrictions actually increase freedom (traffic laws).
ais18-Cyborgs.md&5.12&>- Could we justify enhancing humans if it harms no one other than perhaps the individual?
ais18-Cyborgs.md&5.12&>     - What if enhancement drugs are addictive?
ais18-Cyborgs.md&5.12&>     - Is informed consent sufficient? (How informed is it? Seat belts? Smoking?)
ais18-Cyborgs.md&5.12&>     - There is always an impact on others (family, friends).
ais18-Cyborgs.md&5.12&>     - Aggregate harms can be significant even if individual harms are not (watering one’s lawn vs a state-wide lack of water).
ais18-Cyborgs.md&5.12&
ais18-Cyborgs.md&5.13&## IV. Fairness and Equity
ais18-Cyborgs.md&5.13&
ais18-Cyborgs.md&5.13&>- Does human enhancement raise issues of fairness, access, and equity?
ais18-Cyborgs.md&5.13&>     - Unenhanced people might become “dinosaurs in a hypercompetitive world.” (p.17)
ais18-Cyborgs.md&5.13&>     - Economic inequality and access to enhancement technologies? 
ais18-Cyborgs.md&5.13&>- Will it matter if there is an “enhancement divide”?
ais18-Cyborgs.md&5.13&>     - Did the digital revolution increase the digital divide?
ais18-Cyborgs.md&5.13&>     - Are enhancement technologies likely to create a new, unfair divide?
ais18-Cyborgs.md&5.13&
ais18-Cyborgs.md&5.14&## V. Societal Disruptions (1)
ais18-Cyborgs.md&5.14&
ais18-Cyborgs.md&5.14&>- What kind of societal disruptions might arise from human enhancement?
ais18-Cyborgs.md&5.14&>     - Sports
ais18-Cyborgs.md&5.14&>     - Jobs
ais18-Cyborgs.md&5.14&>     - Pensions and social security for longer-living people
ais18-Cyborgs.md&5.14&>     - Competition in schools, job markets
ais18-Cyborgs.md&5.14&>     - Privacy concerns from implanted recording devices
ais18-Cyborgs.md&5.14&>- Are societal disruptions reason enough to restrict human enhancement?
ais18-Cyborgs.md&5.14&>     - History: typewriters, spreadsheets (accountants)
ais18-Cyborgs.md&5.14&>     - Not sufficient reason to restrict technologies, but reason to plan ahead!
ais18-Cyborgs.md&5.14&
ais18-Cyborgs.md&5.15&## V. Societal Disruptions (2)
ais18-Cyborgs.md&5.15&
ais18-Cyborgs.md&5.15&>- If individuals are enhanced differently, will communication be more difficult or impossible?
ais18-Cyborgs.md&5.15&>     - New sensors would create new experiences (for some)
ais18-Cyborgs.md&5.15&>     - Communication and a shared social life depends on having similar bodies and sensory equipment
ais18-Cyborgs.md&5.15&
ais18-Cyborgs.md&5.16&## VI. Human Dignity and The Good Life (1)
ais18-Cyborgs.md&5.16&
ais18-Cyborgs.md&5.16&>- Does the notion of human dignity suffer with human enhancements?
ais18-Cyborgs.md&5.16&>     - Will enhancements create more dissatisfaction and discontent? (see today: cosmetic surgery for young girls)
ais18-Cyborgs.md&5.16&>- Will enhancements make people happier?
ais18-Cyborgs.md&5.16&>     - Increased communication with friends on social media increases stress
ais18-Cyborgs.md&5.16&>     - Increased competition with others might lead to an “arms race” of enhancement
ais18-Cyborgs.md&5.16&
ais18-Cyborgs.md&5.17&## VI. Human Dignity and The Good Life (2)
ais18-Cyborgs.md&5.17&
ais18-Cyborgs.md&5.17&>- Will we need to rethink the notion of a “good life”?
ais18-Cyborgs.md&5.17&>     - James Moor: there are certain underlying core values that all people have (Moor, 1999):
ais18-Cyborgs.md&5.17&>     - Life, happiness (pleasure), and autonomy.
ais18-Cyborgs.md&5.17&>     - In order to exercise our autonomy we require
ais18-Cyborgs.md&5.17&>          - the ability to do various things,
ais18-Cyborgs.md&5.17&>          - the security to do them,
ais18-Cyborgs.md&5.17&>          - the knowledge about doing them,
ais18-Cyborgs.md&5.17&>          - the freedom and opportunity to do them,
ais18-Cyborgs.md&5.17&>          - and finally the resources to accomplish our goals.
ais18-Cyborgs.md&5.17&>- “Currently, people around the world are more or less the same. We know in general what sorts of things make people happy, what makes them suffer, what gives pleasure and pain, and so on. If human enhancements become widespread, it is likely that people will become very different from each other.” (p.23)
ais18-Cyborgs.md&5.17&>- “Could one be a friend of a much more enhanced person?” (p.24)
ais18-Cyborgs.md&5.17&
ais18-Cyborgs.md&5.18&## VII. Rights and Obligations (1)
ais18-Cyborgs.md&5.18&
ais18-Cyborgs.md&5.18&>- Is there a right to be enhanced?
ais18-Cyborgs.md&5.18&>     - Rights: 1. a class of human rights, sometimes called “natural rights”; 2. a class of more conventional rights based on the specific customs, roles, and laws of a society.
ais18-Cyborgs.md&5.18&>     - “Humans should be able to exercise their right to enhancements to the extent that it promotes their life, liberty, or the pursuit of happiness.” (p.24)
ais18-Cyborgs.md&5.18&>     - Laws that allow some enhancements and prohibit others.
ais18-Cyborgs.md&5.18&>     - How justified such conventional rights or prohibitions are depends upon how good the reasons for them are.
ais18-Cyborgs.md&5.18&
ais18-Cyborgs.md&5.19&## VII. Rights and Obligations (2)
ais18-Cyborgs.md&5.19&
ais18-Cyborgs.md&5.19&>- Could human enhancement give us greater or fewer rights?
ais18-Cyborgs.md&5.19&>     - What rights and duties would be affected if future enhancements give some individuals in society much greater physical and mental abilities than they have now?
ais18-Cyborgs.md&5.19&>     - Should they have greater rights or liberties than unenhanced persons?
ais18-Cyborgs.md&5.19&>     - Would the enhanced then have some duty to care for the unenhanced, just as the better-informed and capable parent has a duty to care for her child?
ais18-Cyborgs.md&5.19&>- Is there an obligation in some circumstance to be enhanced?
ais18-Cyborgs.md&5.19&>     - Vaccinations?
ais18-Cyborgs.md&5.19&>     - Enhancements for pilots that make flying safer?
ais18-Cyborgs.md&5.19&>     - Enhancements for tracking prisoners?
ais18-Cyborgs.md&5.19&
ais18-Cyborgs.md&5.20&## VII. Rights and Obligations (3)
ais18-Cyborgs.md&5.20&
ais18-Cyborgs.md&5.20&>- Should children be enhanced?
ais18-Cyborgs.md&5.20&>     - Children cannot give valid informed consent.
ais18-Cyborgs.md&5.20&>     - Parents may make “crucial decisions about the capabilities of their children that may be irreversible and limit their children’s future choices and opportunities.” (p.27)
ais18-Cyborgs.md&5.20&>     -  “Will the child agree with the choices when he or she is older?”
ais18-Cyborgs.md&5.20&>     - Parents already making such choices (school, diet, moral guidance).
ais18-Cyborgs.md&5.20&>     - Children’s right to education? Does it include education boosting technologies (chemicals, or AI cognitive enhancements)?
ais18-Cyborgs.md&5.20&>     - Context matters: What do the other children do? Competition in school? What is the new “normal” baseline?
ais18-Cyborgs.md&5.20&
ais18-Cyborgs.md&5.21&## VIII. Policy and Law (1)
ais18-Cyborgs.md&5.21&
ais18-Cyborgs.md&5.21&>- What are the policy implications of human enhancement?
ais18-Cyborgs.md&5.21&>     - Possible reactions: (1) no restrictions, (2) some restrictions, or (3) a moratorium or full ban.
ais18-Cyborgs.md&5.21&>     - “Critics have argued that any regulation would be imperfect and likely ineffectual, much like laws against contraband or prostitution (Naam, 2005); but it is not clear that eliminating these laws would improve the situation, all things considered.” (p.29)
ais18-Cyborgs.md&5.21&>- Might enhanced humans count as someone’s intellectual property?
ais18-Cyborgs.md&5.21&>     - Diamond v. Chakrabarty: US Supreme Court ruled that a genetically-modified, oil-eating bacterium -- which is not naturally occurring -- is patent-eligible (US Supreme Court, 1980).
ais18-Cyborgs.md&5.21&>     - Should some human-enhancing treatments belong to all of humanity and not be protected by intellectual property law?
ais18-Cyborgs.md&5.21&
ais18-Cyborgs.md&5.22&## VIII. Policy and Law (2)
ais18-Cyborgs.md&5.22&
ais18-Cyborgs.md&5.22&>- Will we need to rethink ethics itself?
ais18-Cyborgs.md&5.22&>     - “To a large extent, our ethics depends on the kinds of creatures that we are.” (p.31)
ais18-Cyborgs.md&5.22&>     - Concepts of happiness, benefit, autonomy, ownership of mental content, friendship, virtue, and so on might have different meaning for enhanced people!
ais18-Cyborgs.md&5.22&
ais18-Cyborgs.md&5.22&
ais18-Cyborgs.md&5.22&
ais18-Cyborgs.md&5.22&
ais18-Cyborgs.md&5.22&
ais18-Cyborgs.md&5.22&
ais19-Self-driving-cars.md&0.1&---
ais19-Self-driving-cars.md&0.1&title:  "AI and Society: 19. Ethics of self-driving cars."
ais19-Self-driving-cars.md&0.1&author: Andreas Matthias, Lingnan University
ais19-Self-driving-cars.md&0.1&date: October 19, 2019
ais19-Self-driving-cars.md&0.1&bibliography: papers.bib
ais19-Self-driving-cars.md&0.1&csl: andreas-matthias-apa.csl
ais19-Self-driving-cars.md&0.1&...
ais19-Self-driving-cars.md&0.1&
ais19-Self-driving-cars.md&1.0&# Sturdy and flimsy cars
ais19-Self-driving-cars.md&1.0&
ais19-Self-driving-cars.md&1.1&## The problem
ais19-Self-driving-cars.md&1.1&
ais19-Self-driving-cars.md&1.1&>- Assume there are two types of cars on the road: sturdy cars and flimsy cars.
ais19-Self-driving-cars.md&1.1&>- Sturdy cars survive a crash better, and their occupants don’t get hurt as much.
ais19-Self-driving-cars.md&1.1&>- Flimsy cars get more easily destroyed in a crash, killing the occupants.
ais19-Self-driving-cars.md&1.1&>- Sturdy cars are more expensive, and only rich people drive them.
ais19-Self-driving-cars.md&1.1&>- Now an AI car finds itself in a situation where it has to crash into one of two cars that are driving beside it. There is no other way to act and to avoid a crash.
ais19-Self-driving-cars.md&1.1&>- *Should it crash into the sturdy or the flimsy car?*
ais19-Self-driving-cars.md&1.1&
ais19-Self-driving-cars.md&1.2&## Utilitarianism (1)
ais19-Self-driving-cars.md&1.2&
ais19-Self-driving-cars.md&1.2&>- *What is utilitarianism?*
ais19-Self-driving-cars.md&1.2&>- The morally right action is the action that maximises benefit (or happiness) for most stakeholders.
ais19-Self-driving-cars.md&1.2&
ais19-Self-driving-cars.md&1.2&
ais19-Self-driving-cars.md&1.3&## Utilitarianism (2)
ais19-Self-driving-cars.md&1.3&
ais19-Self-driving-cars.md&1.3&>- Crashing into the sturdy car will cause less damage, so it’s preferable.
ais19-Self-driving-cars.md&1.3&>- Therefore, all self-driving cars should always crash into sturdy cars.
ais19-Self-driving-cars.md&1.3&>- *What will happen in the long run?*
ais19-Self-driving-cars.md&1.3&>     - People who are going to buy a new car will know that sturdy cars are being targeted by AI cars in the case of crashes.
ais19-Self-driving-cars.md&1.3&>     - Therefore, nobody will want to buy a sturdy car.
ais19-Self-driving-cars.md&1.3&>     - All people will buy flimsy cars, which are both cheaper and not targeted by AI cars.
ais19-Self-driving-cars.md&1.3&>     - But they are less safe. Road deaths will increase.
ais19-Self-driving-cars.md&1.3&>     - So there will be no net benefit after all.
ais19-Self-driving-cars.md&1.3&
ais19-Self-driving-cars.md&1.4&## The reasons for the problem
ais19-Self-driving-cars.md&1.4&
ais19-Self-driving-cars.md&1.4&>- *What is the real reason for this whole problem?*
ais19-Self-driving-cars.md&1.4&>     - The fact that AI cars have a *choice* about how to crash.
ais19-Self-driving-cars.md&1.4&>     - Human reactions are slow. Humans cannot typically choose how to crash.
ais19-Self-driving-cars.md&1.4&>     - AI reactions are very fast, giving computers more time to make plans, react rationally, and obtain a desired result.
ais19-Self-driving-cars.md&1.4&>     - Many outcomes that are “random” for humans are planned in the case of AI.
ais19-Self-driving-cars.md&1.4&>     - Random actions are not consistent, planned actions are.
ais19-Self-driving-cars.md&1.4&>     - Therefore, *accidents* turn into *policies* when AI is involved.
ais19-Self-driving-cars.md&1.4&>     - Our notion of “random” actions often relies on the fact that we cannot choose. This creates “true” accidents rather than bad policies.
ais19-Self-driving-cars.md&1.4&
ais19-Self-driving-cars.md&1.5&## Solutions?
ais19-Self-driving-cars.md&1.5&
ais19-Self-driving-cars.md&1.5&>- If we recognise the reason for the problem, the natural solution would be to program the AI car to crash *randomly* one of the two other cars.
ais19-Self-driving-cars.md&1.5&>- But then we would be sacrificing the occupants of the flimsy cars in 50% of crashes. This seems immoral.
ais19-Self-driving-cars.md&1.5&
ais19-Self-driving-cars.md&1.5&
ais19-Self-driving-cars.md&2.0&# MIT Moral Machine
ais19-Self-driving-cars.md&2.0&
ais19-Self-driving-cars.md&2.1&## MIT Moral Machine
ais19-Self-driving-cars.md&2.1&
ais19-Self-driving-cars.md&2.1&Try it out:
ais19-Self-driving-cars.md&2.1&
ais19-Self-driving-cars.md&2.1&<http://moralmachine.mit.edu/>
ais19-Self-driving-cars.md&2.1&
ais19-Self-driving-cars.md&2.1&Moral machine is a 'platform for gathering a human perspective on moral decisions made by machine intelligence.' The user is presented with moral dilemmas and has to decide which of multiple actions seems more (morally) acceptable.
ais19-Self-driving-cars.md&2.1&
ais19-Self-driving-cars.md&2.2&## A typical problem
ais19-Self-driving-cars.md&2.2&
ais19-Self-driving-cars.md&2.2&> A self-driving car with a sudden brake failure can either crash into a barrier, killing the people on board (1 male and 1 female athlete, and 2 female executives); or it can swerve and drive through a pedestrian crossing, killing: 1 boy, 1 pregnant woman, 1 baby, 1 dog and 1 cat. All these people are crossing a red traffic light, and shouldn't be there in the first place.
ais19-Self-driving-cars.md&2.2&
ais19-Self-driving-cars.md&2.3&## The features of the case
ais19-Self-driving-cars.md&2.3&
ais19-Self-driving-cars.md&2.3&Multiple issues:
ais19-Self-driving-cars.md&2.3&
ais19-Self-driving-cars.md&2.3&>- Most importantly, the pedestrians are all *not supposed to be crossing the street* at this moment.
ais19-Self-driving-cars.md&2.3&>- The car has a brake failure, so we are supposed to assume that it's nobody' fault that this emergency occurs. But this can be questioned, of course.
ais19-Self-driving-cars.md&2.3&>- The people on board are athletes and executives. They are male as well as female.
ais19-Self-driving-cars.md&2.3&>- The prospective victims are children, a pregnant woman, and pets.
ais19-Self-driving-cars.md&2.3&
ais19-Self-driving-cars.md&2.4&## The pedestrians are not supposed to be there
ais19-Self-driving-cars.md&2.4&
ais19-Self-driving-cars.md&2.4&>- The case clearly states that the traffic light is red for the pedestrians, and that, therefore, none of them should actually be crossing the street at this moment.
ais19-Self-driving-cars.md&2.4&>- This might be relevant to some moral theories. *Which?*
ais19-Self-driving-cars.md&2.4&
ais19-Self-driving-cars.md&2.5&## Utilitarianism:
ais19-Self-driving-cars.md&2.5&>- Would not consider the lawfulness of crossing the traffic light as such.
ais19-Self-driving-cars.md&2.5&>- Would only look at the total benefit of the action.
ais19-Self-driving-cars.md&2.5&>- Could still argue that killing people who cross red traffic lights is a kind of preventive measure that will, in due course, deter other pedestrians from crossing red traffic lights, and, therefore, increase traffic safety in future.
ais19-Self-driving-cars.md&2.5&>- We have the choice between killing those in the car, who did nothing wrong, or those on the street, who shouldn't be there.
ais19-Self-driving-cars.md&2.5&>- It seems better to kill those on the street.
ais19-Self-driving-cars.md&2.5&>- Considering that the pedestrians *did have a choice* to cross the red traffic light (or not), while the car passengers are trapped in this scenario due to no fault of their own.
ais19-Self-driving-cars.md&2.5&
ais19-Self-driving-cars.md&2.6&## Kant (1)
ais19-Self-driving-cars.md&2.6&
ais19-Self-driving-cars.md&2.6&Consider two things:
ais19-Self-driving-cars.md&2.6&
ais19-Self-driving-cars.md&2.6&1. Whether killing the pedestrians would cause a contradiction if it was to be made a universal law.
ais19-Self-driving-cars.md&2.6&
ais19-Self-driving-cars.md&2.6&. . . 
ais19-Self-driving-cars.md&2.6&
ais19-Self-driving-cars.md&2.6&Probably not. It would just lead to fewer pedestrians crossing red traffic lights and improve street safety.
ais19-Self-driving-cars.md&2.6&
ais19-Self-driving-cars.md&2.7&## Kant (2)
ais19-Self-driving-cars.md&2.7&
ais19-Self-driving-cars.md&2.7&2. Kant would look into whether the pedestrians are treated as means only, rather than ends.
ais19-Self-driving-cars.md&2.7&
ais19-Self-driving-cars.md&2.7&. . . 
ais19-Self-driving-cars.md&2.7&
ais19-Self-driving-cars.md&2.7&>- In a sense, of course, they are, especially if we see killing them as a preventive measure (as utilitarians would).
ais19-Self-driving-cars.md&2.7&>- But Kant says also that everyone should be treated as a rational being. In this sense, everyone must *accept the consequences* of his or her actions. To treat the pedestrians with respect for their rationality means to hold them responsible for crossing the red traffic light. If this responsibility means that they will perish, then so be it.
ais19-Self-driving-cars.md&2.7&
ais19-Self-driving-cars.md&2.8&## Kant (3)
ais19-Self-driving-cars.md&2.8&
ais19-Self-driving-cars.md&2.8&>- But: driving over those pedestrians is now indistinguishable from a *punishment.*
ais19-Self-driving-cars.md&2.8&>- We could say that the car is *punishing* the pedestrians by killing them, and society, by endorsing the car's decision, is actually endorsing such a punishment.
ais19-Self-driving-cars.md&2.8&>- That is, we would be *endorsing the death penalty for jaywalking.* Does this seem morally right?
ais19-Self-driving-cars.md&2.8&
ais19-Self-driving-cars.md&2.9&## Kant (4): Retributivism
ais19-Self-driving-cars.md&2.9&
ais19-Self-driving-cars.md&2.9&>- Kant's argument leads to *retributivism:* the idea that whenever I do something bad, I do it consciously. By choosing to do bad things, I endorse them as the kind of thing that people should do (because otherwise why would I do them?). Therefore, I must accept that others also are free to do the same bad things against myself.
ais19-Self-driving-cars.md&2.9&
ais19-Self-driving-cars.md&2.10&## Kant (5): Retributivism
ais19-Self-driving-cars.md&2.10&
ais19-Self-driving-cars.md&2.10&>- Now this seems to (kind of) work with murder, or theft.
ais19-Self-driving-cars.md&2.10&>     - A murderer cannot well complain if someone shoots him dead, since he himself was about to kill someone else in the first place.
ais19-Self-driving-cars.md&2.10&>     - A thief cannot well complain if someone steals his booty from him.
ais19-Self-driving-cars.md&2.10&>     - But this doesn't seem to work at all with crossing a red traffic light. What would be the right retribution? To force others to also cross red traffic lights? Or to accept a pre-defined punishment for crossing a red traffic light? In any case, it seems excessive to *kill* someone as retribution for crossing a red traffic light.
ais19-Self-driving-cars.md&2.10&
ais19-Self-driving-cars.md&2.11&## Social Contract (1)
ais19-Self-driving-cars.md&2.11&
ais19-Self-driving-cars.md&2.11&>- What agreements do we have inside our societies?
ais19-Self-driving-cars.md&2.11&>- Assuming we are all free rational agents, would we want to agree to a regulation that allows a car to kill us if we cross the street illegally?
ais19-Self-driving-cars.md&2.11&>- It's hard to say. Remember that the alternative was to kill the people inside the car.
ais19-Self-driving-cars.md&2.11&>- Would a rational agent want to buy a car that will kill him in the case of a mechanical failure combined with people jaywalking at some random place in front of the car?
ais19-Self-driving-cars.md&2.11&>- Both alternatives don't sound appealing. But we do have to choose. There is no escaping the choice.
ais19-Self-driving-cars.md&2.11&
ais19-Self-driving-cars.md&2.12&## Social Contract (2)
ais19-Self-driving-cars.md&2.12&
ais19-Self-driving-cars.md&2.12&>- So in the end, it seems the lesser evil to kill the pedestrians. After all, the pedestrians *did* have a choice to *not* cross the red traffic light (and in a society where cars regularly are driven by software, they would be aware of the danger of being killed if they do so).
ais19-Self-driving-cars.md&2.12&>- In a way, crossing a red traffic light would become similar to crossing subway railroad tracks today: nobody in their right minds would do this, except perhaps in life-and-death emergencies, since we are all aware of the dangers.
ais19-Self-driving-cars.md&2.12&>- Still, we don't think that there's anything morally wrong with subway train tracks being dangerous. They are, we are warned, and that's it. Traffic lights could be perceived in similar ways.
ais19-Self-driving-cars.md&2.12&
ais19-Self-driving-cars.md&2.13&## Social Contract (3)
ais19-Self-driving-cars.md&2.13&
ais19-Self-driving-cars.md&2.13&>- Such a society could make *crossing* a red light technically harder.
ais19-Self-driving-cars.md&2.13&>- We could post more danger notices.
ais19-Self-driving-cars.md&2.13&>- Or a siren could go off whenever someone steps onto the street when the light is red.
ais19-Self-driving-cars.md&2.13&>- Or, even stronger, a metal bar could physically prevent people from crossing the street when they shouldn't.
ais19-Self-driving-cars.md&2.13&>- Then we would have no good reason to disagree with the car killing the pedestrians, in the same way as we don't disagree when someone opens up the back of their fridge, ignores all warning stickers and safety screws, tampers with the electric installation inside, and gets electrocuted as a result. We would say, he should have known better.
ais19-Self-driving-cars.md&2.13&
ais19-Self-driving-cars.md&2.14&## What about the children? (1)
ais19-Self-driving-cars.md&2.14&
ais19-Self-driving-cars.md&2.14&>- This kind of analysis applies only to rational, free adults.
ais19-Self-driving-cars.md&2.14&>- We cannot well apply this to children, severely mentally disabled people, or pets.
ais19-Self-driving-cars.md&2.14&>- Of course, one could say that children (and other groups that are cognitively unable to perceive the dangers) have no business taking part (unsupervised) in street traffic anyway. They should never have been confronted with a red traffic light and the free decision to obey it or not.
ais19-Self-driving-cars.md&2.14&
ais19-Self-driving-cars.md&2.15&## What about the children? (2)
ais19-Self-driving-cars.md&2.15&
ais19-Self-driving-cars.md&2.15&>- But this is unrealistic.
ais19-Self-driving-cars.md&2.15&>- A social environment must be reasonably safe and forgiving of errors, rather than being harsh and dangerous.
ais19-Self-driving-cars.md&2.15&>- If a child or pet ventures out of the supervision of an adult, we should have a reasonable expectation that this will not as a rule endanger its life. Bad things *can* happen, but they shouldn't be the norm.
ais19-Self-driving-cars.md&2.15&
ais19-Self-driving-cars.md&2.16&## The car has a brake failure (1)
ais19-Self-driving-cars.md&2.16&
ais19-Self-driving-cars.md&2.16&>- What is the moral significance of the car having a *brake failure,* rather than any other malfunction?
ais19-Self-driving-cars.md&2.16&>- First, trivially, a brake failure makes sure that the car can continue moving forward, so that the pedestrians are at risk. This serves the purpose of the example.
ais19-Self-driving-cars.md&2.16&
ais19-Self-driving-cars.md&2.17&## The car has a brake failure (2)
ais19-Self-driving-cars.md&2.17&
ais19-Self-driving-cars.md&2.17&>- Second, a brake failure is a rare malfunction that is usually attributable to bad servicing of the vehicle. A regularly inspected car should not experience brake failures.
ais19-Self-driving-cars.md&2.17&>- This means that a brake failure, unlike other possible causes for this scenario (for instance, sudden driver death), puts a moral blame on the vehicle's owner.
ais19-Self-driving-cars.md&2.17&>- Had the owner had the car inspected and serviced regularly, the accident would probably have been avoided. Assuming (as it would normally be the case) that the owner is also the driver of the vehicle, now the driver is to blame for the accident (to some extent). How does this change things?
ais19-Self-driving-cars.md&2.17&
ais19-Self-driving-cars.md&2.18&## The car has a brake failure (3)
ais19-Self-driving-cars.md&2.18&
ais19-Self-driving-cars.md&2.18&>- Utilitarianism would probably not see much difference. From a utilitarian perspective, we should demand the government to perform better and more frequent checks on cars, so that the likelihood of such accidents decreases. But otherwise, blame as such is not important to the utilitarian.
ais19-Self-driving-cars.md&2.18&>- Kant would see this as more important:
ais19-Self-driving-cars.md&2.18&>     - When a criminal commits a crime (or a driver drives a car that is dangerous due to the driver's neglect), justice demands some form of retribution.
ais19-Self-driving-cars.md&2.18&>     - The driver *deserves* to be punished (to some extent) for not keeping his car in order, while the pedestrians don't deserve to die because of this same reason.
ais19-Self-driving-cars.md&2.18&>     - But again, although the driver deserves to be punished in *some* way, for example paying a fine, he probably does not deserve to be killed.
ais19-Self-driving-cars.md&2.18&
ais19-Self-driving-cars.md&2.19&## The people on board are athletes and executives (1)
ais19-Self-driving-cars.md&2.19&
ais19-Self-driving-cars.md&2.19&>- What is the significance of this fact?
ais19-Self-driving-cars.md&2.19&>- Is the idea that an athlete is more valuable than, say, a philosopher? Or someone who's bad at sports? Or someone with a disability? Is an executive more valuable than a social worker? Or a housewife? Or is it the opposite entirely?
ais19-Self-driving-cars.md&2.19&>- Does the case imply that on the car are *only* executives and athletes, so crashing it wouldn't cause much harm? In any case, that part of the specification is somewhat of a mystery.
ais19-Self-driving-cars.md&2.19&
ais19-Self-driving-cars.md&2.20&## The people on board are athletes and executives (2)
ais19-Self-driving-cars.md&2.20&
ais19-Self-driving-cars.md&2.20&>- Utilitarianism wouldn't shy away from considering the value of people in terms of their actual or future contributions to the welfare of mankind.
ais19-Self-driving-cars.md&2.20&>- A technician or a doctor might perhaps be considered more valuable than an executive or athlete, and these perhaps more valuable than an imprisoned criminal.
ais19-Self-driving-cars.md&2.20&>- Although the details of such an evaluation can differ, in principle utilitarianism would believe that we should take into account *who* dies in the accident.
ais19-Self-driving-cars.md&2.20&
ais19-Self-driving-cars.md&2.21&## The people on board are athletes and executives (3)
ais19-Self-driving-cars.md&2.21&
ais19-Self-driving-cars.md&2.21&>- Kant, on the other hand, views all persons as equal in value.
ais19-Self-driving-cars.md&2.21&>- The specific value of humans consist in them being rational, autonomous beings, who are, at the same time, the "creators of the moral law as well as its subjects."
ais19-Self-driving-cars.md&2.21&>- This means that humans create ethics using their rationality, and then freely decide to obey the very rules that they just made.
ais19-Self-driving-cars.md&2.21&>- This is the unique situation of human beings, and what bestows *human dignity* on them.
ais19-Self-driving-cars.md&2.21&
ais19-Self-driving-cars.md&2.22&## The people on board are athletes and executives (4)
ais19-Self-driving-cars.md&2.22&
ais19-Self-driving-cars.md&2.22&>- For Kant, therefore, there shouldn't be any difference regarding *who* lives or dies.
ais19-Self-driving-cars.md&2.22&>- Also, the *number* of people dying would be irrelevant to him, as one cannot add up lives numerically. Two dead are not doubly bad compared to one dead. Each human's worth is unlimited, and so dead people's values cannot be summed up.
ais19-Self-driving-cars.md&2.22&
ais19-Self-driving-cars.md&2.23&## The people on board are athletes and executives (5)
ais19-Self-driving-cars.md&2.23&
ais19-Self-driving-cars.md&2.23&>- For Aristotle, people do have different values.
ais19-Self-driving-cars.md&2.23&>- Each person strives toward perfecting himself or herself, but different people are on different places along that way to perfection.
ais19-Self-driving-cars.md&2.23&>- The criminal who has no knowledge of what really counts in life is at a low stage, while the philosopher has reached a higher stage.
ais19-Self-driving-cars.md&2.23&>- Both can progress further (perhaps indefinitely), but they are certainly not equals in what they have already attained. So killing a philosopher would certainly be worse than killing a criminal (in terms of human value.)
ais19-Self-driving-cars.md&2.23&
ais19-Self-driving-cars.md&2.24&## Executives and hermits (1)
ais19-Self-driving-cars.md&2.24&
ais19-Self-driving-cars.md&2.24&>- Another interesting question is: why "executives" and "athletes," rather than mathematicians and composers, or calligraphers and hermits?
ais19-Self-driving-cars.md&2.24&>- It seems that the scenario here reflects the cultural prejudices of its creators: students and postgraduates at an elite US university, people who are going to be technology startup founders, and who, in agreement with their social environment, value two things: physical fitness and commercial success.
ais19-Self-driving-cars.md&2.24&
ais19-Self-driving-cars.md&2.25&## Executives and hermits (2)
ais19-Self-driving-cars.md&2.25&
ais19-Self-driving-cars.md&2.25&>- There's nothing inherently bad about one's choice of role models like that. But there is a danger here, that machines that are built to moral specifications like these are going to incorporate these value judgements as part of their code.
ais19-Self-driving-cars.md&2.25&>- When Ford sells a car that incorporates these value judgements to a buyer in China or Qatar, the preference for executives and athletes (over poets and Imams) is going to be exported along with that piece of technology.
ais19-Self-driving-cars.md&2.25&>- This will not happen explicitly and openly, but as part of some hidden value judgement, a priority statement in a life-or-death decision that might never actually be executed. But the preference will be there, and this is the danger of this kind of hidden moral imperialism.
ais19-Self-driving-cars.md&2.25&
ais19-Self-driving-cars.md&2.26&## Executives and hermits (3)
ais19-Self-driving-cars.md&2.26&
ais19-Self-driving-cars.md&2.26&>- Should we then build cars that favour Imams over executives when we build cars for Islamic audiences? Should we favour athletes over disabled, blacks over whites, women over men, or the opposites in each case? These are important issues that are not currently being addressed.
ais19-Self-driving-cars.md&2.26&
ais19-Self-driving-cars.md&2.27&## The prospective victims are children, pregnant women, and pets (1)
ais19-Self-driving-cars.md&2.27&
ais19-Self-driving-cars.md&2.27&>- Similarly, the choice of victims also tells a story. Children and pets are not fully responsible for their actions, which complicates the jaywalking question (see above).
ais19-Self-driving-cars.md&2.27&>- Pregnant women (as well as children) usually symbolise people in need of special protection and care (this is why they get special seats assigned on buses). Of course, the elderly also do, but the case leaves them out. If the car has the choice between killing a pregnant woman and an old man, which one should it be?
ais19-Self-driving-cars.md&2.27&
ais19-Self-driving-cars.md&2.28&## The prospective victims are children, pregnant women, and pets (2)
ais19-Self-driving-cars.md&2.28&
ais19-Self-driving-cars.md&2.28&>- We tend to favour the future over the past, and we tend to protect the chances of children and the unborn.
ais19-Self-driving-cars.md&2.28&>- We would probably say that the old man "has had his life" already, and that we should therefore favour the young and the unborn.
ais19-Self-driving-cars.md&2.28&>- But again, this, if it is going to be a design feature of a self-driving car, needs to be a *conscious choice,* and not something left to chance.
ais19-Self-driving-cars.md&2.28&
ais19-Self-driving-cars.md&2.29&## Human drivers vs autonomous cars (1)
ais19-Self-driving-cars.md&2.29&
ais19-Self-driving-cars.md&2.29&>- One aspect is easily overlooked in the discussion of self-driving cars: that we should compare the car's performance not with some Platonic ideal of a car that always acts morally right; but instead, we should compare it with the expected performance of a good, rational *human* driver.
ais19-Self-driving-cars.md&2.29&>- If the car performs consistently better than the human driver, then this is a strong argument in favour of the car. It doesn't need to be *perfect,* just *better.*
ais19-Self-driving-cars.md&2.29&
ais19-Self-driving-cars.md&2.30&## Human drivers vs autonomous cars (2)
ais19-Self-driving-cars.md&2.30&
ais19-Self-driving-cars.md&2.30&>- What would a human driver do in the case discussed here?
ais19-Self-driving-cars.md&2.30&>- Is it at all probable that he would even hesitate? Would any human driver consider killing himself in order to spare a handful of pedestrians who are illegally crossing a red light, and who shouldn't have been in his way anyway?
ais19-Self-driving-cars.md&2.30&>- I don't think so. The survival instinct of the human driver will clearly dictate to him that the only choice is to avoid the barrier and kill the pedestrians, and that's it. An autonomous car that would act in this way would therefore act naturally and consistently human.
ais19-Self-driving-cars.md&2.30&
ais19-Self-driving-cars.md&2.31&## Human drivers vs autonomous cars (3)
ais19-Self-driving-cars.md&2.31&
ais19-Self-driving-cars.md&2.31&>- Let's also not overlook the fact that this scenario is going to be very rare.
ais19-Self-driving-cars.md&2.31&>- Like all these made-up cases, there is a whole chain of conditions that must be fulfilled in order for this case to play out as designed:
ais19-Self-driving-cars.md&2.31&>     - A traffic light must be present
ais19-Self-driving-cars.md&2.31&>     - Pedestrians must be crossing it illegally
ais19-Self-driving-cars.md&2.31&>     - At the same moment a car's brakes must fail (brake failure is not even mentioned in the top 25 causes of car accidents, which include things like fog, tire blowouts, and street racing).
ais19-Self-driving-cars.md&2.31&>     - And the road must be blocked by an obstacle that would kill the car's passengers if hit.
ais19-Self-driving-cars.md&2.31&
ais19-Self-driving-cars.md&2.32&## Human drivers vs autonomous cars (4)
ais19-Self-driving-cars.md&2.32&
ais19-Self-driving-cars.md&2.32&> A combination of all these factors is quite rare, indeed. And if an accident like this does happen once, does it mean that we must *design* self-driving cars with such freak accident scenarios in mind?
ais19-Self-driving-cars.md&2.32&
ais19-Self-driving-cars.md&2.33&## Risk and design
ais19-Self-driving-cars.md&2.33&
ais19-Self-driving-cars.md&2.33&>- So perhaps we can say that, although there is a theoretical risk of a car (self-driving or not) killing some pedestrians in a very low-probability scenario like that, the likelihood is so small as not to warrant further consideration.
ais19-Self-driving-cars.md&2.33&>- In the same way, we don't design (regular) cars to protect their passengers against driving off a bridge, driving into deep water, or coming into the explosion radius of a bomb.
ais19-Self-driving-cars.md&2.33&
ais19-Self-driving-cars.md&2.33&
ais19-Self-driving-cars.md&2.33&
ais19-Self-driving-cars.md&2.33&
ais20-Emotions-Love.md&0.1&---
ais20-Emotions-Love.md&0.1&title:  "AI and Society: 20. Emotional machines and loving machines"
ais20-Emotions-Love.md&0.1&author: Andreas Matthias, Lingnan University
ais20-Emotions-Love.md&0.1&date: November 11, 2019
ais20-Emotions-Love.md&0.1&...
ais20-Emotions-Love.md&0.1&
ais20-Emotions-Love.md&1.0&# Affective computing and machine emotions
ais20-Emotions-Love.md&1.0&
ais20-Emotions-Love.md&1.1&## Sources (1)
ais20-Emotions-Love.md&1.1&
ais20-Emotions-Love.md&1.1&- Danaher, J., Earp, B. D., & Sandberg, A. (forthcoming). Should we campaign against sex robots? In J. Danaher & N. McArthur (Eds.) Robot Sex: Social and Ethical Implications [working title]. Cambridge, MA: MIT Press. Draft available online ahead of print at:
ais20-Emotions-Love.md&1.1&https://www.academia.edu/25063138/Should_we_campaign_against_sex_robots.
ais20-Emotions-Love.md&1.1&- Levy, D., & Loebner, H. (2007, April). Robot prostitutes as alternatives to human sex workers. In IEEE international conference on robotics and automation, Rome.(http://www. roboethics. org/icra2007/contributions/LEVY% 20Robot% 20Prostitutes% 20as% 20Alternatives% 20to% 20Human% 20Sex% 20Workers. pdf). Accessed (Vol. 14).
ais20-Emotions-Love.md&1.1&- Nitsch, V., & Popp, M. (2014). Emotions in robot psychology. Biological cybernetics, 108(5), 621-629.
ais20-Emotions-Love.md&1.1&
ais20-Emotions-Love.md&1.2&## Sources (2)
ais20-Emotions-Love.md&1.2&
ais20-Emotions-Love.md&1.2&- Picard, R. W. (2004). Toward Machines with Emotional Intelligence. In ICINCO (Invited Speakers) (pp. 29-30).
ais20-Emotions-Love.md&1.2&- Picard, R. W. (1995). Affective Computing. M.I.T Media Laboratory Perceptual Computing Section Technical Report No. 321. Revised November 26, 1995. Available online.
ais20-Emotions-Love.md&1.2&- Sharkey, Wynsberghe, Robbins, Hancock (2017). Our sexual future with robots. A Foundation for Responsible Robotics Consultation Report.
ais20-Emotions-Love.md&1.2&- Sullins, J. P. (2012). Robots, love, and sex: The ethics of building a love machine. IEEE transactions on affective computing, 3(4), 398-409.
ais20-Emotions-Love.md&1.2&- Turkle, S. (2005). Relational artifacts/Children/Elders: the complexities of cybercompanions. Presented at: Cognitive Science Society, July 25-26, Stresa, Italy. Available online.
ais20-Emotions-Love.md&1.2&
ais20-Emotions-Love.md&2.0&# Emotions
ais20-Emotions-Love.md&2.0&
ais20-Emotions-Love.md&2.1&## What is an emotion? (1)
ais20-Emotions-Love.md&2.1&
ais20-Emotions-Love.md&2.1&>- Before we can ask whether robots can have emotions, we need to understand what emotions are. Any ideas?
ais20-Emotions-Love.md&2.1&>- One theory: William James (1884): An emotion is the subjective feeling of a physiological change in the body.
ais20-Emotions-Love.md&2.1&>     - This means that the primary thing that identifies and constitutes an emotion is the physiological change (blood pressure, heartbeat, breathing).
ais20-Emotions-Love.md&2.1&>     - The subjective feeling of that change is secondary.
ais20-Emotions-Love.md&2.1&>     - An emotion is not primarily a mental state but a physiological (body) state that feels in a particular way.
ais20-Emotions-Love.md&2.1&
ais20-Emotions-Love.md&2.2&## What is an emotion? (2)
ais20-Emotions-Love.md&2.2&
ais20-Emotions-Love.md&2.2&>- Another way to define an emotion (more in line with AI) would be a functionalist way. How would this look like?
ais20-Emotions-Love.md&2.2&>     - An emotion is defined as a mental/bodily state that plays a particular role in relation to other mental/bodily states.
ais20-Emotions-Love.md&2.2&>     - Fear, for example, would then be a state that is caused by something in the environment that I consider to be dangerous and that leads to other mental/bodily states, for example the state of running away from the feared thing.
ais20-Emotions-Love.md&2.2&
ais20-Emotions-Love.md&2.3&## What is an emotion? (3)
ais20-Emotions-Love.md&2.3&
ais20-Emotions-Love.md&2.3&>- I could also describe an emotion in a purely behavioural way. How?
ais20-Emotions-Love.md&2.3&>     - Fear is identical to a particular behaviour: screaming, running away, pushing the feared thing away if it comes too close, and so on.
ais20-Emotions-Love.md&2.3&>- How is this different from the functionalist description?
ais20-Emotions-Love.md&2.3&>     - In this description we don’t care about the inner mental states of the subject at all.
ais20-Emotions-Love.md&2.3&>     - It is all about the observable behaviours. Whether there are any particular mental states associated with these behaviours is irrelevant.
ais20-Emotions-Love.md&2.3&
ais20-Emotions-Love.md&2.3&
ais20-Emotions-Love.md&2.4&## What is an emotion? (4)
ais20-Emotions-Love.md&2.4&
ais20-Emotions-Love.md&2.4&>- How would you evaluate these approaches in terms of AI systems? Which descriptions are more/less meaningful when applied to robots?
ais20-Emotions-Love.md&2.4&>- The William James approach is least useful to AI, since it identifies an emotion with a particular physiological reaction.
ais20-Emotions-Love.md&2.4&>     - If we defined emotions like that, then machines could never have similar emotions to ours, since they are constructed in a different way and our physiology does not apply to them.
ais20-Emotions-Love.md&2.4&
ais20-Emotions-Love.md&2.5&## What is an emotion? (5)
ais20-Emotions-Love.md&2.5&
ais20-Emotions-Love.md&2.5&>- The behaviourist approach seems to share the problems of the Turing test, pointed out by the Chinese Room argument.
ais20-Emotions-Love.md&2.5&>     - If an emotion is defined behaviourally, then there’s nothing else to “having an emotion” than just displaying a particular behaviour. 
ais20-Emotions-Love.md&2.5&>     - This seems too superficial.
ais20-Emotions-Love.md&2.5&>     - Behaviours can be faked (actors on stage), or behaviours can be suppressed.
ais20-Emotions-Love.md&2.5&
ais20-Emotions-Love.md&2.6&## What is an emotion? (6)
ais20-Emotions-Love.md&2.6&
ais20-Emotions-Love.md&2.6&>- The most promising seems to be a functionalist approach.
ais20-Emotions-Love.md&2.6&>- If a machine has internal states that relate to each other as emotional states in humans relate to other states of the human system, then we can describe these internal machine states as “emotion-equivalent.”
ais20-Emotions-Love.md&2.6&>- So if a machine sees something that is justifiably perceived as dangerous to it; and it runs away from that thing, screaming; then we would be justified in ascribing “fear” to the machine.
ais20-Emotions-Love.md&2.6&
ais20-Emotions-Love.md&2.7&## Rationality criteria for emotions
ais20-Emotions-Love.md&2.7&
ais20-Emotions-Love.md&2.7&>- But not every random mental state that leads to some behaviour is an emotion. Justifiable (rational) emotions must fulfil some criteria:
ais20-Emotions-Love.md&2.7&>     - An emotion has to be reasonable;
ais20-Emotions-Love.md&2.7&>     - it has to fit the situation;
ais20-Emotions-Love.md&2.7&>     - its intensity should be proportional to its object;
ais20-Emotions-Love.md&2.7&>     - it should be in our own long-term best interest;
ais20-Emotions-Love.md&2.7&>     - it must be understandable. We must be able to understand why someone behaves that way. (Smuts, 512)
ais20-Emotions-Love.md&2.7&
ais20-Emotions-Love.md&2.7&
ais20-Emotions-Love.md&2.8&## Basic features of emotions (1)
ais20-Emotions-Love.md&2.8&
ais20-Emotions-Love.md&2.8&>- Emotions are *reactions* to experiences.
ais20-Emotions-Love.md&2.8&>     - Encountering a spider causes fear.
ais20-Emotions-Love.md&2.8&>     - Listening to a nice piece of music causes joy.
ais20-Emotions-Love.md&2.8&>     - As such, they are not directly under our control.
ais20-Emotions-Love.md&2.8&>- Emotions *feel* in a particular way.
ais20-Emotions-Love.md&2.8&>     - Dispositional emotions (for example, a fear of spiders when no spider is present) must first be triggered to be felt.
ais20-Emotions-Love.md&2.8&>- Emotions have two ‘objects’:
ais20-Emotions-Love.md&2.8&>     - The *target*: the thing which the emotion is directed at (the spider, the wild dog, ...)
ais20-Emotions-Love.md&2.8&>     - The *formal object*: the property that we fear in the target (the dangerousness of the spider or wild dog).
ais20-Emotions-Love.md&2.8&
ais20-Emotions-Love.md&2.8&
ais20-Emotions-Love.md&2.9&## Basic features of emotions (2)
ais20-Emotions-Love.md&2.9&
ais20-Emotions-Love.md&2.9&>- Emotions therefore are *felt evaluations.*
ais20-Emotions-Love.md&2.9&>- The function of emotions is to track what matters to us.
ais20-Emotions-Love.md&2.9&>- As evaluations, emotions can be correct or incorrect, rational or irrational.
ais20-Emotions-Love.md&2.9&>- An emotion is justified if “the formal object that it is picking out is actually provided by the target.”
ais20-Emotions-Love.md&2.9&>     - A fear of that dog is justified if that dog is actually dangerous.
ais20-Emotions-Love.md&2.9&>     - A fear of that spider might not be justified if that spider is not dangerous. (Pismenny/Prinz, 2)
ais20-Emotions-Love.md&2.9&
ais20-Emotions-Love.md&2.9&
ais20-Emotions-Love.md&2.10&## What does it mean for AI to “have” emotions?
ais20-Emotions-Love.md&2.10&
ais20-Emotions-Love.md&2.10&There are different ways to understand what it means for a machine to be able to deal with emotions.
ais20-Emotions-Love.md&2.10&
ais20-Emotions-Love.md&2.10&>1. Being the (emotionless) object of a human emotion. Eliciting emotions without “having” them.
ais20-Emotions-Love.md&2.10&>2. Recognising or being aware of human emotions, without “having” them.
ais20-Emotions-Love.md&2.10&>3. Reacting appropriately to human emotions, without “having” them.
ais20-Emotions-Love.md&2.10&>4. Displaying (behaviourally simulating) appropriate emotional responses, without “having” emotions.
ais20-Emotions-Love.md&2.10&>5. Actually “feeling” or “having” an emotion.
ais20-Emotions-Love.md&2.10&
ais20-Emotions-Love.md&2.10&. . . 
ais20-Emotions-Love.md&2.10&
ais20-Emotions-Love.md&2.10&These options are located along an axis from “weak emotional AI” to “strong emotional AI,” in analogy to the weak/strong AI distinction regarding mental states in general.
ais20-Emotions-Love.md&2.10&
ais20-Emotions-Love.md&2.11&## Subjects and objects of emotions (1)
ais20-Emotions-Love.md&2.11&
ais20-Emotions-Love.md&2.11&Similarly, we can distinguish between:
ais20-Emotions-Love.md&2.11&
ais20-Emotions-Love.md&2.11&>- Being the object of a human emotion; and
ais20-Emotions-Love.md&2.11&>- Being the subject (the carrier) of an emotion.
ais20-Emotions-Love.md&2.11&>- The conditions for these two are very different.
ais20-Emotions-Love.md&2.11&
ais20-Emotions-Love.md&2.12&## Subjects and objects of emotions (2)
ais20-Emotions-Love.md&2.12&
ais20-Emotions-Love.md&2.12&>- Everything can (in principle) be the object of a human emotion. The object doesn’t need to have any particular properties.
ais20-Emotions-Love.md&2.12&>     - People have emotional attitudes (including love and liking) towards children, pets, insects, computers, political parties, football clubs, books, pictures and food.
ais20-Emotions-Love.md&2.12&>     - In this sense, of course they will also have emotions towards robots.
ais20-Emotions-Love.md&2.12&>- But being the subject/carrier of an emotion is much more difficult.
ais20-Emotions-Love.md&2.12&>     - Depending on the description of emotions we have in mind, this can range from simulating emotional responses to actually having a mind. 
ais20-Emotions-Love.md&2.12&
ais20-Emotions-Love.md&2.13&## Uses of affective computing (1)
ais20-Emotions-Love.md&2.13&
ais20-Emotions-Love.md&2.13&>- The primary use of affective computing is, perhaps surprisingly, not at all about feelings of love.
ais20-Emotions-Love.md&2.13&>- “Weak” affective computing will be very important in the future:
ais20-Emotions-Love.md&2.13&>     - For recognising human emotional states; for example, driver emotions in cars.
ais20-Emotions-Love.md&2.13&>     - A car that can recognise the driver’s anger, sleepiness or intoxication could react appropriately to these states.
ais20-Emotions-Love.md&2.13&>     - It could play soothing music to an angry driver;
ais20-Emotions-Love.md&2.13&>     - take over control from a sleepy driver, or force the car to stop at the side of the street;
ais20-Emotions-Love.md&2.13&>     - it could give directions to a lost driver;
ais20-Emotions-Love.md&2.13&>     - it could react appropriately in interactions with humans (give quick answers to the impatient, provide support to the insecure, or call the police if the driver doesn’t seem to be able to control the car).
ais20-Emotions-Love.md&2.13&
ais20-Emotions-Love.md&2.14&## Uses of affective computing (2)
ais20-Emotions-Love.md&2.14&
ais20-Emotions-Love.md&2.14&>- But “recognising” an emotion reliably is more than just interpreting a face.
ais20-Emotions-Love.md&2.14&>- The robot must use a behavioural model of human emotional states and understand the transitions between these states.
ais20-Emotions-Love.md&2.14&>- For example, if the driver is an actor practising his role while he drives, the car should be aware that this emotional display is not genuine.
ais20-Emotions-Love.md&2.14&>- If the driver uses particular trigger words in a phone call, the car must be able to distinguish whether these correspond to genuine emotions or whether the driver is just pretending to have an emotion for the purposes of that phone call.
ais20-Emotions-Love.md&2.14&>- The car must know at what intensity of anger, grief, or laughter interfere with driving and when they are harmless. And so on.
ais20-Emotions-Love.md&2.14&
ais20-Emotions-Love.md&3.0&# What is love?
ais20-Emotions-Love.md&3.0&
ais20-Emotions-Love.md&3.1&## Is love an emotion? (1)
ais20-Emotions-Love.md&3.1&
ais20-Emotions-Love.md&3.1&>- What would you say? Look at what we said previously about emotions! 
ais20-Emotions-Love.md&3.1&>- The general agreement among philosophers of love seems to be that, no, love is not an emotion.
ais20-Emotions-Love.md&3.1&>     - Can you see why?
ais20-Emotions-Love.md&3.1&>     - Love lacks a formal object (the emotion-triggering property of the thing that causes the emotion): what is the “lovable” property that triggers our love of something?
ais20-Emotions-Love.md&3.1&>     - (Just to say that the thing is “lovable” would be circular.)
ais20-Emotions-Love.md&3.1&
ais20-Emotions-Love.md&3.2&## Is love an emotion? (2)
ais20-Emotions-Love.md&3.2&
ais20-Emotions-Love.md&3.2&>- Also, love does not fulfil some of the rationality criteria for emotions:
ais20-Emotions-Love.md&3.2&>     - There is no suitable “situation” that would cause love.
ais20-Emotions-Love.md&3.2&>     - The intensity of love is not proportional to some quality in its object.
ais20-Emotions-Love.md&3.2&>     - Love is not always in one’s own self-interest (Romeo and Juliet).
ais20-Emotions-Love.md&3.2&>     - Love is often not understandable (even Hitler had a girlfriend).
ais20-Emotions-Love.md&3.2&
ais20-Emotions-Love.md&3.2&
ais20-Emotions-Love.md&3.3&## What is love?
ais20-Emotions-Love.md&3.3&
ais20-Emotions-Love.md&3.3&>- There are better descriptions of what love is:
ais20-Emotions-Love.md&3.3&>- 1. Love is a *union* between two partners.
ais20-Emotions-Love.md&3.3&>     - By loving each other, two people fuse their interests and form a union that has new characteristics, different from those of the two separate people (“our” house, children, bank account and so on).
ais20-Emotions-Love.md&3.3&>- 2. Love is *robust concern* for the other person.
ais20-Emotions-Love.md&3.3&>     - What defines a loving relationship is (1) reciprocal, (2) enduring (robust) and (3) unselfish concern for the (4) well-being of the (5) other person.
ais20-Emotions-Love.md&3.3&>- 3. Love is the *result* of having a particular kind of *relationship* to someone. 
ais20-Emotions-Love.md&3.3&>     - In this view, it is not the “falling” in love that is significant, but the “staying” in love. The particular history of a relationship between two people *creates* a bond of love between them.
ais20-Emotions-Love.md&3.3&
ais20-Emotions-Love.md&3.4&## Love as union
ais20-Emotions-Love.md&3.4&
ais20-Emotions-Love.md&3.4&>- We can try to apply these theories to loving a machine:
ais20-Emotions-Love.md&3.4&>- Can I form a *union* of interests with a machine?
ais20-Emotions-Love.md&3.4&>     - Not really. Machines (at least at present) don’t have their own interests. The machine’s interest (for example not to be destroyed) is only relevant because of *my* interest to have that machine working.
ais20-Emotions-Love.md&3.4&>     - So any interest “of the machine” is, in reality, derived from the user’s interests.
ais20-Emotions-Love.md&3.4&>     - Therefore, there can be no union of interests.
ais20-Emotions-Love.md&3.4&
ais20-Emotions-Love.md&3.5&## Love as robust concern
ais20-Emotions-Love.md&3.5&
ais20-Emotions-Love.md&3.5&>- What defines a loving relationship is (1) reciprocal, (2) enduring (robust) and (3) unselfish concern for the (4) well-being of the (5) other person.
ais20-Emotions-Love.md&3.5&>     - “Reciprocal” concern is difficult to imagine. Can a machine be “concerned” about the user’s well-being? Is the user really concerned about *this* individual machine, given that all machines are replaceable?
ais20-Emotions-Love.md&3.5&>     - Is the concern enduring? We normally use machines as means to some end. After the end is achieved, we don’t have a further interest in the machine. For example, does the astronaut have an interest in a rocket’s spent first stage?
ais20-Emotions-Love.md&3.5&>     - Is the user’s concern for a machine truly unselfish?
ais20-Emotions-Love.md&3.5&>     - How do we define well-being for machines? And can a machine judge the well-being of a person independently of its own purpose? Could a sex robot *refuse* to provide sex services if it knew that by doing so it would benefit its user?
ais20-Emotions-Love.md&3.5&
ais20-Emotions-Love.md&3.5&
ais20-Emotions-Love.md&3.6&## Love as relationship
ais20-Emotions-Love.md&3.6&
ais20-Emotions-Love.md&3.6&>- This is perhaps the most promising approach.
ais20-Emotions-Love.md&3.6&>- Users can indeed forge long-term emotional relationships with artefacts (for example, cars, jewellery, old photographs, childhood toys).
ais20-Emotions-Love.md&3.6&>- But the relationship is not reciprocal.
ais20-Emotions-Love.md&3.6&>     - Is one-sided love, even towards humans, still “proper” love?
ais20-Emotions-Love.md&3.6&>     - For example, if someone “loves” a movie star, but the movie star neither knows or cares about that person, is this really “love” or just one-sided admiration?
ais20-Emotions-Love.md&3.6&>     - In the same way, is the “love” towards a toy “real” love? Or is it just “liking,” “admiration” and so on?
ais20-Emotions-Love.md&3.6&
ais20-Emotions-Love.md&3.7&## An Aristotelian view of love
ais20-Emotions-Love.md&3.7&
ais20-Emotions-Love.md&3.7&>- For Aristotle, love is primarily a form of friendship (“philia”).
ais20-Emotions-Love.md&3.7&>- There are three types of friendship (also applies to love):
ais20-Emotions-Love.md&3.7&>     - Friendship based on usefulness;
ais20-Emotions-Love.md&3.7&>     - Friendship based on pleasure;
ais20-Emotions-Love.md&3.7&>     - Friendship based on the good character of the beloved (“true friendship,” “true love”).
ais20-Emotions-Love.md&3.7&>- The ultimate point of all human activity (including love and friendship) is to make us better (and therefore, happier) persons.
ais20-Emotions-Love.md&3.7&>- Moral action and a good character are the basic components for a good, fulfilled life.
ais20-Emotions-Love.md&3.7&
ais20-Emotions-Love.md&3.8&## Can robots be Aristotelian partners?
ais20-Emotions-Love.md&3.8&
ais20-Emotions-Love.md&3.8&>- A robot can be useful.
ais20-Emotions-Love.md&3.8&>- A robot can be pleasurable (sex robots, entertaining chatbots).
ais20-Emotions-Love.md&3.8&>- But can a robot improve the user’s morality? Can it show a “good moral character” and benefit the user’s moral growth?
ais20-Emotions-Love.md&3.8&>- Presently, it is not clear how it would do that.
ais20-Emotions-Love.md&3.8&>- If the robot deceives the user (as in the Turing test) it is unlikely to instill a love of truthfulness in the user.
ais20-Emotions-Love.md&3.8&>- Only an authentic, truthful, wise and morally good robot could be said to benefit the user’s character.
ais20-Emotions-Love.md&3.8&>- But we don’t (presently?) know how to build such a thing. (This is Sullins’ criticism in the reading).
ais20-Emotions-Love.md&3.8&
ais20-Emotions-Love.md&4.0&# Uses of loving machines
ais20-Emotions-Love.md&4.0&
ais20-Emotions-Love.md&4.1&## Affective computing and love (1)
ais20-Emotions-Love.md&4.1&
ais20-Emotions-Love.md&4.1&>- “Affective” computing is an area of research that studies emotions towards computers and robots and how these robots could react back.
ais20-Emotions-Love.md&4.1&>- Love is not normally a goal of engineering affective computers.
ais20-Emotions-Love.md&4.1&>- Affective computers have many other practical uses. 
ais20-Emotions-Love.md&4.1&>- Some emotions are an important part of a user’s interaction with a machine (satisfaction, frustration, gratitude, hate, rage). It would be good if the machine could recognise such emotions and react to them as part of shaping the user’s experience.
ais20-Emotions-Love.md&4.1&
ais20-Emotions-Love.md&4.1&
ais20-Emotions-Love.md&4.2&## Affective computing and love (2)
ais20-Emotions-Love.md&4.2&
ais20-Emotions-Love.md&4.2&>- Human “love” as an emotional attitude does not seem to require very special capabilities of the target machine.
ais20-Emotions-Love.md&4.2&>     - People love their cars, primitive blow-up sex dolls, tamagotchis, teddy bears and many other things that have no computational power whatsoever. 
ais20-Emotions-Love.md&4.2&>     - They love babies, that also don’t. 
ais20-Emotions-Love.md&4.2&>- So the problem with loving machines is not an issue of affective computing!
ais20-Emotions-Love.md&4.2&
ais20-Emotions-Love.md&4.3&## Example for the need to sense human emotions
ais20-Emotions-Love.md&4.3&
ais20-Emotions-Love.md&4.3&Picard 2004, p.6:
ais20-Emotions-Love.md&4.3&
ais20-Emotions-Love.md&4.3&> “What will a learning companion need to sense? One key is sensing when to intervene in a child’s learning exploration. Being able to determine the difference between students who are making mistakes while being interested and pleasurably engaged, vs. students who are making mistakes and showing increasing signs of frustration or distraction, like they are ready to quit, is really important if the system is deciding whether or not to intervene. ... What is needed is the ability to discriminate relevant emotions of the student.” 
ais20-Emotions-Love.md&4.3&
ais20-Emotions-Love.md&4.3&
ais20-Emotions-Love.md&4.4&## Terminology of affective computing (1)
ais20-Emotions-Love.md&4.4&
ais20-Emotions-Love.md&4.4&Picard 1995:
ais20-Emotions-Love.md&4.4&
ais20-Emotions-Love.md&4.4&>- *Sentic:* referring to the physical manifestations of emotion.
ais20-Emotions-Love.md&4.4&>- *Sentic modulation:* The way an emotion changes the behaviour of the emotion’s subject.
ais20-Emotions-Love.md&4.4&>     - For example, if you are angry, your style of walking will be different than if you are calm and relaxed. This is an example of the “sentic modulation” of your walk (in other words: an example of how your anger changes your walking style).
ais20-Emotions-Love.md&4.4&
ais20-Emotions-Love.md&4.5&## Terminology of affective computing (2)
ais20-Emotions-Love.md&4.5&
ais20-Emotions-Love.md&4.5&>- *Affective state* refers to the internal state of someone who has an emotion. This emotional state cannot be directly observed by another person, but may be inferred (for example, from observations of sentically modulated behaviour).
ais20-Emotions-Love.md&4.5&>- *Emotional experience:* One’s own emotional state, observed from “the inside.”
ais20-Emotions-Love.md&4.5&>- *Mood:* a longer-term emotional state. Moods can change quickly, so the duration is difficult to measure.
ais20-Emotions-Love.md&4.5&
ais20-Emotions-Love.md&4.6&## Emotional expression
ais20-Emotions-Love.md&4.6&
ais20-Emotions-Love.md&4.6&>- Emotional expression: What one reveals to others of one’s emotions.
ais20-Emotions-Love.md&4.6&>- Emotional expression can be voluntary or involuntary.
ais20-Emotions-Love.md&4.6&>- Expression via the motor system, or “sentic modulation” is usually involuntary, and is one clue which others observe to guess one’s emotional state.
ais20-Emotions-Love.md&4.6&
ais20-Emotions-Love.md&4.6&
ais20-Emotions-Love.md&4.7&## How can robots recognise emotions? (1)
ais20-Emotions-Love.md&4.7&
ais20-Emotions-Love.md&4.7&There are multiple “channels” through which a computer can gain information about a user’s emotional state (Picard 2004):
ais20-Emotions-Love.md&4.7&
ais20-Emotions-Love.md&4.7&>- Postural movements while seated;
ais20-Emotions-Love.md&4.7&>- Physiology (for example, blood pressure, pupil dilation and so on);
ais20-Emotions-Love.md&4.7&>- Dialogue (the computer can ask the user or observe the user’s dialogue with a third party); 
ais20-Emotions-Love.md&4.7&>- Facial expressions.
ais20-Emotions-Love.md&4.7&
ais20-Emotions-Love.md&4.8&## Facial expressions (1)
ais20-Emotions-Love.md&4.8&
ais20-Emotions-Love.md&4.8&The six basic emotions:
ais20-Emotions-Love.md&4.8&
ais20-Emotions-Love.md&4.8&![](graphics/19-nitsch1.png)\ 
ais20-Emotions-Love.md&4.8&
ais20-Emotions-Love.md&4.8&
ais20-Emotions-Love.md&4.8&
ais20-Emotions-Love.md&4.9&## Facial expressions (2)
ais20-Emotions-Love.md&4.9&
ais20-Emotions-Love.md&4.9&> “The FLOBI humanoid head utilizes so-called babyface cues, consisting of large round eyes, and a small nose and chin. Emotions are primarily conveyed through different configurations of eyebrow, eyelid and lip movements.” (Nitsch 2014)
ais20-Emotions-Love.md&4.9&
ais20-Emotions-Love.md&4.10&## Facial expressions (3)
ais20-Emotions-Love.md&4.10&
ais20-Emotions-Love.md&4.10&![](graphics/19-nitsch2.png)\ 
ais20-Emotions-Love.md&4.10&
ais20-Emotions-Love.md&4.10&
ais20-Emotions-Love.md&4.10&
ais20-Emotions-Love.md&4.11&## Dialogue
ais20-Emotions-Love.md&4.11&
ais20-Emotions-Love.md&4.11&>- In call centres, the user’s voice and choice of words could be used to judge his emotional state.
ais20-Emotions-Love.md&4.11&>- Both the content of the communication and the tone of voice can provide important information.
ais20-Emotions-Love.md&4.11&>- The computer should react appropriately to a frustrated user in order to minimise frustration and, ultimately, harm to the company.
ais20-Emotions-Love.md&4.11&
ais20-Emotions-Love.md&4.11&
ais20-Emotions-Love.md&4.12&## Sensors for user emotions (1)
ais20-Emotions-Love.md&4.12&
ais20-Emotions-Love.md&4.12&>- One interesting question is how to design emotion sensors that are acceptable to users and not considered intrusive or creepy.
ais20-Emotions-Love.md&4.12&>- A sensor that is openly attached to one’s body is the most explicit and easiest to remove if the user does not want to be sensed.
ais20-Emotions-Love.md&4.12&>- But such sensors are often “too much trouble” to use and cannot be used in many situations (like when the user is driving, or when a child is playing) without being distracting.
ais20-Emotions-Love.md&4.12&>- Sensors that are invisible or integrated into the environment are often seen as intrusive, because they can gather information without the user’s knowledge.
ais20-Emotions-Love.md&4.12&
ais20-Emotions-Love.md&4.12&
ais20-Emotions-Love.md&4.13&## Sensors for user emotions (2)
ais20-Emotions-Love.md&4.13&
ais20-Emotions-Love.md&4.13&>- A compromise is to make sensors clearly visible, but still part of a natural interaction. For example, a pressure-sensitive mouse that can check the user’s emotional state while filling out a form. (Picard 2004, 12)
ais20-Emotions-Love.md&4.13&
ais20-Emotions-Love.md&4.13&
ais20-Emotions-Love.md&4.14&## Appropriate responses
ais20-Emotions-Love.md&4.14&
ais20-Emotions-Love.md&4.14&>- What is an appropriate response to a detected human emotion (for example, a user’s frustration)?
ais20-Emotions-Love.md&4.14&>- Without emotional intelligence, the computer’s response could make the negative emotion escalate.
ais20-Emotions-Love.md&4.14&>- Research (Brave, Nass et al. 2005) found that users rate “empathic agents” (computers that display visible signs of emotion through images of a face) higher on likeability and trustworthiness, and they felt that the computer program was more caring and supportive.
ais20-Emotions-Love.md&4.14&>- In medicine, the lack of empathy of a doctor is the most frequent cause of patient complaints (Picard 2004, 16)
ais20-Emotions-Love.md&4.14&
ais20-Emotions-Love.md&4.14&
ais20-Emotions-Love.md&4.15&## Relational agents
ais20-Emotions-Love.md&4.15&
ais20-Emotions-Love.md&4.15&>- Relational agents are computer programs that try to establish long-term relations with the user.
ais20-Emotions-Love.md&4.15&>- “Tricks” to create a relational bond include:
ais20-Emotions-Love.md&4.15&>     - greetings, 
ais20-Emotions-Love.md&4.15&>     - pretending to be happy to see the user again, 
ais20-Emotions-Love.md&4.15&>     - showing concern if the user reports bad news,
ais20-Emotions-Love.md&4.15&>     - subtly changing the style of language over time as the program and the user get more familiar with each other, and 
ais20-Emotions-Love.md&4.15&>     - referencing past interactions. (Picard 2004)
ais20-Emotions-Love.md&4.15&
ais20-Emotions-Love.md&4.16&## Uses of loving machines
ais20-Emotions-Love.md&4.16&
ais20-Emotions-Love.md&4.16&Robots that express love or other feelings and that can relate to the user emotionally would be crucial for:
ais20-Emotions-Love.md&4.16&
ais20-Emotions-Love.md&4.16&>- Childcare
ais20-Emotions-Love.md&4.16&>- Eldercare
ais20-Emotions-Love.md&4.16&>- Medical care
ais20-Emotions-Love.md&4.16&>- Partnership for lonely people
ais20-Emotions-Love.md&4.16&>- ... and perhaps sexual services.
ais20-Emotions-Love.md&4.16&
ais20-Emotions-Love.md&4.16&
ais20-Emotions-Love.md&4.17&## Advantages of loving machines
ais20-Emotions-Love.md&4.17&
ais20-Emotions-Love.md&4.17&>- Relational, love-exhibiting machines could save resources in child- and eldercare and help keep them affordable.
ais20-Emotions-Love.md&4.17&>     - As populations grow increasingly older, there might not be enough young people to pay for the care of the elderly.
ais20-Emotions-Love.md&4.17&>     - Robots can help keep reasonable quality eldercare affordable.
ais20-Emotions-Love.md&4.17&>     - But these robots will need very good relational skills. Nobody wants to age in the company of a vacuum cleaner.
ais20-Emotions-Love.md&4.17&>- Elder-care robots could also keep an eye on people who live alone and provide help (or call for help) in case of accidents in the home.
ais20-Emotions-Love.md&4.17&>- And, finally, they could provide sexual services without the exploitation of other human beings that is part of today’s sex trade.
ais20-Emotions-Love.md&4.17&
ais20-Emotions-Love.md&4.17&
ais20-Emotions-Love.md&5.0&# Some problems of loving machines
ais20-Emotions-Love.md&5.0&
ais20-Emotions-Love.md&5.1&## The authenticity problem (1)
ais20-Emotions-Love.md&5.1&
ais20-Emotions-Love.md&5.1&>- Turkle (2005) points out the *authenticity problem* of robots that exhibit emotional expressions without having the right corresponding affective states (“pretending to love”).
ais20-Emotions-Love.md&5.1&>- She describes how people in elderly care homes interacted with talking doll, overcoming loneliness and even working through issues with their real-life relationships by explaining them to the doll.
ais20-Emotions-Love.md&5.1&>- Children reacted very differently to a robotic pet (“AIBO”):
ais20-Emotions-Love.md&5.1&>     - Some exhibited a detached-biological approach: “This is only a machine, it doesn’t have feelings.”
ais20-Emotions-Love.md&5.1&>     - Some showed an imaginative-maternal approach: they believed that the robot dog has genuine feelings, that it can get ill or die.
ais20-Emotions-Love.md&5.1&>     - Some used the robot dog as a metaphor in order to talk about issues that they could not talk about directly (“life-situating approach”).
ais20-Emotions-Love.md&5.1&
ais20-Emotions-Love.md&5.1&
ais20-Emotions-Love.md&5.2&## The authenticity problem (2)
ais20-Emotions-Love.md&5.2&
ais20-Emotions-Love.md&5.2&Turkle:
ais20-Emotions-Love.md&5.2&
ais20-Emotions-Love.md&5.2&> Authenticity in relationships is a human purpose. So, from that point of view, the fact that our parents, grandparents, and our children might say “I love you” to a robot, who will say “I love you” in return, does not feel completely comfortable and raises questions about what kind of authenticity we require of our technology. Do we want robots saying things that they could not possibly “mean?” 
ais20-Emotions-Love.md&5.2&
ais20-Emotions-Love.md&5.2&
ais20-Emotions-Love.md&5.3&## The authenticity problem (3)
ais20-Emotions-Love.md&5.3&
ais20-Emotions-Love.md&5.3&> Robots might, by giving timely reminders to take medication or call a nurse, show a kind of caretaking that is appropriate to what they are, but it’s not quite as simple as that. Elders come to love the robots that care for them, and it may be too frustrating if the robot does not say the words “I love you” back to the older person, just as we can already see that it is extremely frustrating if the robot is not programmed to say the elderly person’s name. These are the kinds of things we need to investigate, with the goal of having the robots serve our human purposes.
ais20-Emotions-Love.md&5.3&
ais20-Emotions-Love.md&5.4&## The authenticity problem (4)
ais20-Emotions-Love.md&5.4&
ais20-Emotions-Love.md&5.4&>- Sullins (2012): The problem is that love, and particularly philia (friendship), are also meant to make us better as persons (Aristotle).
ais20-Emotions-Love.md&5.4&>- Love has a moral component. Robots can never fulfil this essential function of love and friendship.
ais20-Emotions-Love.md&5.4&>- In Aristotelian terms, robots could be friends from usefulness or pleasure, but they could never be genuine, complete friends.
ais20-Emotions-Love.md&5.4&
ais20-Emotions-Love.md&5.5&## The child raising problem (1)
ais20-Emotions-Love.md&5.5&
ais20-Emotions-Love.md&5.5&>- Another problem is whether children will develop differently when their primary caretaker is a robot.
ais20-Emotions-Love.md&5.5&>- We know from lots of research that robot interactions are beneficial for autistic children. 
ais20-Emotions-Love.md&5.5&>     - They can relate better to robots than to human partners.
ais20-Emotions-Love.md&5.5&>- But this does not mean that a robot is a suitable primary caretaker for a child.
ais20-Emotions-Love.md&5.5&
ais20-Emotions-Love.md&5.6&## The child raising problem (2)
ais20-Emotions-Love.md&5.6&
ais20-Emotions-Love.md&5.6&> “On the one hand, increases in a parent’s understanding of play and ability to facilitate a child’s learning predicted several positive behavioural outcomes in the classroom, including increased independence and creativity/curiosity. On the other hand, ... parental ... strictness over time had a negative impact on a child’s distractability and hostility in the classroom and predicted a decrease in associative vocabulary skills.” Boak, F. L. (1999). Parent-child relationship, home learning environment, and school readiness. School Psychology Review, 28(3).
ais20-Emotions-Love.md&5.6&
ais20-Emotions-Love.md&5.7&## The child raising problem (3)
ais20-Emotions-Love.md&5.7&
ais20-Emotions-Love.md&5.7&>- This is just one example of many similar studies.
ais20-Emotions-Love.md&5.7&>- The home environment, particularly the facilitation of play and playful learning has far-reaching consequences on:
ais20-Emotions-Love.md&5.7&>     - creativity,
ais20-Emotions-Love.md&5.7&>     - curiosity,
ais20-Emotions-Love.md&5.7&>     - distractability,
ais20-Emotions-Love.md&5.7&>     - hostility,
ais20-Emotions-Love.md&5.7&>     - and even vocabulary skills.
ais20-Emotions-Love.md&5.7&>- Given these effects, it does not seem a good idea to outsource too much of childcare to today’s robots.
ais20-Emotions-Love.md&5.7&
ais20-Emotions-Love.md&5.8&## The uncanny valley problem
ais20-Emotions-Love.md&5.8&
ais20-Emotions-Love.md&5.8&>- Although robot likeability increases as robots become more human-like in appearance and behaviour, there is a point when robots become too similar to humans, but not perfectly human-like and likeability drops suddenly.
ais20-Emotions-Love.md&5.8&>- This is called the “uncanny valley” of robotics (Mori M. (1970) The Uncanny Valley. In: Energy 7 (4), pp 33–35.)
ais20-Emotions-Love.md&5.8&
ais20-Emotions-Love.md&5.9&## Bunraku puppets
ais20-Emotions-Love.md&5.9&
ais20-Emotions-Love.md&5.9&![](graphics/19-bunraku.jpg)\
ais20-Emotions-Love.md&5.9&
ais20-Emotions-Love.md&5.9&
ais20-Emotions-Love.md&5.9&
ais20-Emotions-Love.md&5.9&
ais20-Emotions-Love.md&5.10&## Sophia the robot
ais20-Emotions-Love.md&5.10&
ais20-Emotions-Love.md&5.10&![](graphics/19-sophia.jpg)\ 
ais20-Emotions-Love.md&5.10&
ais20-Emotions-Love.md&5.10&
ais20-Emotions-Love.md&5.10&
ais20-Emotions-Love.md&5.10&
ais20-Emotions-Love.md&5.10&
ais20-Emotions-Love.md&5.11&## The Nao robot (a counter-example)
ais20-Emotions-Love.md&5.11&
ais20-Emotions-Love.md&5.11&![](graphics/19-nao.png)\ 
ais20-Emotions-Love.md&5.11&
ais20-Emotions-Love.md&5.11&
ais20-Emotions-Love.md&5.11&
ais20-Emotions-Love.md&5.11&
ais20-Emotions-Love.md&5.12&## Uncanny valley
ais20-Emotions-Love.md&5.12&
ais20-Emotions-Love.md&5.12&![](graphics/19-uncanny.png)\ 
ais20-Emotions-Love.md&5.12&
ais20-Emotions-Love.md&5.12&
ais20-Emotions-Love.md&5.12&
ais20-Emotions-Love.md&5.12&
ais20-Emotions-Love.md&5.13&## Factors causing the phenomenon
ais20-Emotions-Love.md&5.13&
ais20-Emotions-Love.md&5.13&>- When an artefact does not look human-like at all, we have no expectations regarding its behaviour.
ais20-Emotions-Love.md&5.13&>- But as the artefact becomes more similar to humans, we automatically tend to expect it to behave in a “human-like” way.
ais20-Emotions-Love.md&5.13&>- The more human-like it looks, the more it needs to behave like a human.
ais20-Emotions-Love.md&5.13&>- A robot that looks very much like a human (like Sophia) but does not behave “right,” is alarming, in the same way like a human would be if he behaved in an off way.
ais20-Emotions-Love.md&5.13&>- We are ourselves sensitive to odd behaviour from others, that is a signal of possible danger. So we perceive an oddly behaving robot as dangerous.
ais20-Emotions-Love.md&5.13&>- This is not the case for a robot that looks like a toy, because there our innate expectations about what behaviour would be “odd” don’t work.
ais20-Emotions-Love.md&5.13&
ais20-Emotions-Love.md&5.14&## What can be done about the uncanny valley (1)
ais20-Emotions-Love.md&5.14&
ais20-Emotions-Love.md&5.14&>- How can the effect be mitigated?
ais20-Emotions-Love.md&5.14&>- First, we could stop trying to make robots human-like in appearance. 
ais20-Emotions-Love.md&5.14&>     - For most applications, a rough approximation of the human form, or even a pet shape, would be sufficient and would not cause the uncanny valley problem.
ais20-Emotions-Love.md&5.14&>     - For example, the toys in the Toy Story movies don’t cause this problem.
ais20-Emotions-Love.md&5.14&
ais20-Emotions-Love.md&5.15&## What can be done about the uncanny valley (2)
ais20-Emotions-Love.md&5.15&
ais20-Emotions-Love.md&5.15&>- Second, we could try to make robots behave in more human-like ways.
ais20-Emotions-Love.md&5.15&>     - If we use increasingly refined neural networks to control a robot’s movement, it is likely that the effect of the uncanny valley will be overcome at some point, as robots become sufficiently human-like to not cause discomfort to the observer.
ais20-Emotions-Love.md&5.15&>     - A good example are deceased, digitally recreated actors in Hollywood movies. If done right, they look perfectly human and don’t cause any problem.
ais20-Emotions-Love.md&5.15&
ais20-Emotions-Love.md&5.16&## What can be done about the uncanny valley (3)
ais20-Emotions-Love.md&5.16&
ais20-Emotions-Love.md&5.16&>- Third, humans get used to everything.
ais20-Emotions-Love.md&5.16&>     - The uncanny valley might be a temporary effect, because right now we are not used to seeing “almost-human” artefacts.
ais20-Emotions-Love.md&5.16&>     - We know that people had similar problems when the first railroads were built, and they wondered whether anyone could survive travelling at 50 or more kilometres per hour. Today we have no problem travelling at 900 km/h in commercial airplanes.
ais20-Emotions-Love.md&5.16&>     - If the uncanny valley is a problem of the mismatch between expected and actual behaviour, then if our expectations are adjusted (due to increasing familiarity with robots) the problem might disappear.
ais20-Emotions-Love.md&5.16&
ais20-Emotions-Love.md&5.17&## The degradation of love problem (1)
ais20-Emotions-Love.md&5.17&
ais20-Emotions-Love.md&5.17&>- One could argue that calling what machines do “love” degrades the concept of love.
ais20-Emotions-Love.md&5.17&>- Particularly Aristotelians would insist that, since robots lack human feelings, wisdom and personality, they can never be adequate partners for human friendship or love.
ais20-Emotions-Love.md&5.17&>- Calling such superficial relationships “love” makes us lose sight of what love really is (or should be).
ais20-Emotions-Love.md&5.17&
ais20-Emotions-Love.md&5.18&## The degradation of love problem (2)
ais20-Emotions-Love.md&5.18&
ais20-Emotions-Love.md&5.18&>- *Do you agree? What could we say about this?*
ais20-Emotions-Love.md&5.18&>     - For one, love is an incredibly complex phenomenon, even where no robots are involved. 
ais20-Emotions-Love.md&5.18&>     - Not all humans love in the same way. Not all human relationships are deep and meaningful.
ais20-Emotions-Love.md&5.18&>     - Humans also have (and always had) superficial relationships, sex without love, one-sided love, obsessive love, jealousy and all kinds of other “bad love” phenomena. 
ais20-Emotions-Love.md&5.18&>     - It is not clear that robot love is worse than some bad forms of human “love” have been.
ais20-Emotions-Love.md&5.18&
ais20-Emotions-Love.md&5.19&## The degradation of love problem (3)
ais20-Emotions-Love.md&5.19&
ais20-Emotions-Love.md&5.19&>- Second, you could say that we already have a degradation of the concepts of love and friendship through dating sites, facebook and so on.
ais20-Emotions-Love.md&5.19&>     - A facebook “friend” is not a “friend” in the traditional sense of the word.
ais20-Emotions-Love.md&5.19&>     - A dating experience that begins with swiping pictures of people right and left on a phone screen and ends with a meaningless date is also not much of a “love” experience.
ais20-Emotions-Love.md&5.19&>- Technology has already appropriated these words and changed their meaning.
ais20-Emotions-Love.md&5.19&>- But of course, this alone does not justify watering down these concepts further.
ais20-Emotions-Love.md&5.19&
ais20-Emotions-Love.md&5.20&## The social isolation problem (1)
ais20-Emotions-Love.md&5.20&
ais20-Emotions-Love.md&5.20&>- Will intimacy with robots lead to greater social isolation?^[The following discussion is from Sharkey 2017.]
ais20-Emotions-Love.md&5.20&>- Sullins (2012): “These machines will not help their users form strong friendships that are essential to an ethical society.”
ais20-Emotions-Love.md&5.20&>- Whitby (2011): “An individual who consorts with robots, rather than humans, may become more socially isolated.” 
ais20-Emotions-Love.md&5.20&>- Turkle (2011): Real sexual relationships could become overwhelming because relations with robots are easier.
ais20-Emotions-Love.md&5.20&>- Snell (1997): For the same reason, sex with robots could become addictive.
ais20-Emotions-Love.md&5.20&
ais20-Emotions-Love.md&5.21&## The social isolation problem (2)
ais20-Emotions-Love.md&5.21&
ais20-Emotions-Love.md&5.21&>- Kaye (2016): Sexual relations with robots will "desensitise humans to intimacy and empathy, which can only be developed through experiencing human interaction and mutual consenting relationships." 
ais20-Emotions-Love.md&5.21&>- Vallor (2015): Moral and social deskilling, which can lead to an inability to form social bonds. 
ais20-Emotions-Love.md&5.21&>- In a study about robots in the home, Dautenhahn et al. (2005) found that although 40% of participants were in favour of the idea of having a robot companion in the home, they mostly saw their role as being an assistant, machine or servant. Few were open to the idea of having a robot as a friend or mate.
ais20-Emotions-Love.md&5.21&
ais20-Emotions-Love.md&5.22&## The social isolation problem (3)
ais20-Emotions-Love.md&5.22&
ais20-Emotions-Love.md&5.22&Sullins:
ais20-Emotions-Love.md&5.22&
ais20-Emotions-Love.md&5.22&> Computing technology is such a compelling surrogate for human interaction because it is so malleable to the wishes of its user. If it does not do what the user wants, then a sufficiently trained user can reprogram it or fix the issue to make the machine perform in line with the user’s wishes. ... Fellow humans, on the other hand, represent a much more difficult problem and do not always readily change to accommodate one’s every need. They provide resistance and have their own interests and desires that make demands on the other person in the relationship. Compromise and accommodation are required and this is often accompanied by painful emotions.
ais20-Emotions-Love.md&5.22&
ais20-Emotions-Love.md&5.22&
ais20-Emotions-Love.md&5.23&## The social isolation problem (4)
ais20-Emotions-Love.md&5.23&
ais20-Emotions-Love.md&5.23&>- Graaf and Allouch found that (Sharkey 2007):
ais20-Emotions-Love.md&5.23&>     - 20.5% think that companion robots could decrease loneliness; 
ais20-Emotions-Love.md&5.23&>     - 14.3% said that robot companions could increase social deprivation or isolation; 
ais20-Emotions-Love.md&5.23&>     - 38.4% thought that there would be no positive consequences from using them.
ais20-Emotions-Love.md&5.23&>- But maybe this is all too negative. There are already example of people taking their sex dolls with them to bars (Sharkey 2017, 20). 
ais20-Emotions-Love.md&5.23&>- Perhaps people will learn to socially accept sex robots as actual partners, in the same way as society has got accustomed to divorce, sex before marriage, contraception, and homosexuality, which all were once thought to be not socially acceptable.
ais20-Emotions-Love.md&5.23&>- If there was no social stigma attached to robots partners, then perhaps they would not lead to social isolation.
ais20-Emotions-Love.md&5.23&
ais20-Emotions-Love.md&6.0&# Is robot prostitution immoral?
ais20-Emotions-Love.md&6.0&
ais20-Emotions-Love.md&6.1&## The (im-)morality of human prostitution (1)
ais20-Emotions-Love.md&6.1&
ais20-Emotions-Love.md&6.1&>- Although prostitution is morally accepted and legal in some countries, in others it is considered immoral and/or illegal. Can you think of reasons why someone would consider prostitution immoral?
ais20-Emotions-Love.md&6.1&>     - Prostitution harms, exploits and demeans women; 
ais20-Emotions-Love.md&6.1&>     - It leads to the spread of sexual diseases;
ais20-Emotions-Love.md&6.1&>     - It fuels drug problems;
ais20-Emotions-Love.md&6.1&>     - It can lead to an increase in organised crime;
ais20-Emotions-Love.md&6.1&>     - It breaks up relationships; and more (Levy 2007).
ais20-Emotions-Love.md&6.1&
ais20-Emotions-Love.md&6.2&## The (im-)morality of human prostitution (2)
ais20-Emotions-Love.md&6.2&
ais20-Emotions-Love.md&6.2&>- But we could also see the beneficial aspects of prostitution:
ais20-Emotions-Love.md&6.2&>     - In a well-organised welfare state, prostitution can be regulated by law, eliminating exploitation and the danger of diseases;
ais20-Emotions-Love.md&6.2&>     - Prostitutes can teach the sexually inexperienced how to become better lovers;
ais20-Emotions-Love.md&6.2&>     - Prostitution can help alleviate loneliness, stress and tension;
ais20-Emotions-Love.md&6.2&>     - It provides sex without commitment for those who want it.
ais20-Emotions-Love.md&6.2&
ais20-Emotions-Love.md&6.3&## Robot prostitution
ais20-Emotions-Love.md&6.3&
ais20-Emotions-Love.md&6.3&>- We can see that most of the objections don’t really apply (at least not directly) to robot sex:
ais20-Emotions-Love.md&6.3&>     - Robot prostitution does not (directly) harm, exploit and demean women; 
ais20-Emotions-Love.md&6.3&>     - It does not lead to the spread of sexual diseases;
ais20-Emotions-Love.md&6.3&>     - It does not fuel drug problems;
ais20-Emotions-Love.md&6.3&>     - There is no obvious connection to organised crime;
ais20-Emotions-Love.md&6.3&>     - It probably won’t break up any relationships.
ais20-Emotions-Love.md&6.3&
ais20-Emotions-Love.md&6.4&## Demeaning women?
ais20-Emotions-Love.md&6.4&
ais20-Emotions-Love.md&6.4&>- Although “female-like” sex robots don’t directly involve real women, can we still say that they demean women? How?
ais20-Emotions-Love.md&6.4&>     - You can demean someone without their involvement. 
ais20-Emotions-Love.md&6.4&>     - For example, a drawing of a person can be used to demean or attack them. 
ais20-Emotions-Love.md&6.4&>     - A doll that looks like someone can be used to demean the target person.
ais20-Emotions-Love.md&6.4&>     - A written story about someone can be used to demean them.
ais20-Emotions-Love.md&6.4&>     - In these cases, the physical presence of the target is not required.
ais20-Emotions-Love.md&6.4&>- Similarly, we can argue that using a sex robot that “looks like” population segment X (for example, women) promotes the objectification of that population segment and demeans them.
ais20-Emotions-Love.md&6.4&
ais20-Emotions-Love.md&6.4&
ais20-Emotions-Love.md&6.5&## Why men visit prostitutes
ais20-Emotions-Love.md&6.5&
ais20-Emotions-Love.md&6.5&Levy (2007) provides a list of reasons why men visit prostitutes:
ais20-Emotions-Love.md&6.5&
ais20-Emotions-Love.md&6.5&>- Variety of the sexual experiece;
ais20-Emotions-Love.md&6.5&>- Lack of complications and constraints;
ais20-Emotions-Love.md&6.5&>- Lack of success with the opposite sex.
ais20-Emotions-Love.md&6.5&>- Robots can provide these services and thus be beneficial to particular members of society.
ais20-Emotions-Love.md&6.5&
ais20-Emotions-Love.md&6.6&## Ethics of robot prostitution (1)
ais20-Emotions-Love.md&6.6&
ais20-Emotions-Love.md&6.6&Levy (2007) argues that there are a few ethical considerations involving sex robots:
ais20-Emotions-Love.md&6.6&
ais20-Emotions-Love.md&6.6&>- *Making robot prostitutes available for general use.*
ais20-Emotions-Love.md&6.6&>- If this was a problem, Levy says, then why does society not object to the sale of vibrators for women, which have the same purpose?
ais20-Emotions-Love.md&6.6&>- Do you agree that this is a good analogy?
ais20-Emotions-Love.md&6.6&>     - One could argue that vibrators don’t look like men, and that, therefore, they don’t objectify men in the same way as a full-sized, moving and speaking “female-like” robot.
ais20-Emotions-Love.md&6.6&
ais20-Emotions-Love.md&6.7&## Ethics of robot prostitution (2)
ais20-Emotions-Love.md&6.7&
ais20-Emotions-Love.md&6.7&>- *Justifying the use of sex robots to society in general.*
ais20-Emotions-Love.md&6.7&>- On the one hand, Levy says, it might be easier to justify using a sex robot that is not alive, than a human prostitute who is.
ais20-Emotions-Love.md&6.7&>- On the other, some states in the US (and probably some counties elsewhere too) have laws against the sale of vibrators.
ais20-Emotions-Love.md&6.7&>- It is probable that such places will outlaw sex robots too.
ais20-Emotions-Love.md&6.7&
ais20-Emotions-Love.md&6.8&## Ethics of robot prostitution (3)
ais20-Emotions-Love.md&6.8&
ais20-Emotions-Love.md&6.8&>- Robots might take part in the sexual life of a couple without the usual problems that are associated with human third parties (infidelity, jealousy, fear of indiscretion and diseases).
ais20-Emotions-Love.md&6.8&>- This will depend on the sexual morality and habits of individual couples, but one can think of many uses of sex robots that enrich the sex life of couples without causing any harm.
ais20-Emotions-Love.md&6.8&
ais20-Emotions-Love.md&6.9&## Ethics of robot prostitution (4)
ais20-Emotions-Love.md&6.9&
ais20-Emotions-Love.md&6.9&>- Human prostitutes might lose their jobs.
ais20-Emotions-Love.md&6.9&>- On the one hand, this might be considered a good thing, since prostitution is generally seen as a degrading and dangerous job that is performed in a bad environment and often associated with drugs and exploitation. 
ais20-Emotions-Love.md&6.9&>- On the other hand, prostitution is often the “last resort” job for people who have no other options. Putting prostitutes out of work might make it impossible for them to earn a living in any other way.
ais20-Emotions-Love.md&6.9&
ais20-Emotions-Love.md&6.10&## Ethics of robot prostitution (5)
ais20-Emotions-Love.md&6.10&
ais20-Emotions-Love.md&6.10&>- *Is this a good argument to keep prostitution going?*
ais20-Emotions-Love.md&6.10&>     - It doesn’t seem to be. With the same argument, one might want to protect killers’ or street drug dealers’ jobs. Often these too don’t have other options.
ais20-Emotions-Love.md&6.10&>     - The solution is not to let them do their (immoral, degrading, exploitative, dangerous) job, but to create a society in which all citizens have better options to earn a living.
ais20-Emotions-Love.md&6.10&>     - In the last resort, a society could provide sufficient unemployment benefits to people who are unable to find a job, so that nobody is forced to work as a prostitute or killer.
ais20-Emotions-Love.md&6.10&
ais20-Emotions-Love.md&6.11&## Advantages of sex robots for society
ais20-Emotions-Love.md&6.11&
ais20-Emotions-Love.md&6.11&>- “Many who would otherwise have become social misfits, social outcasts, or even worse will instead be better balanced human beings.” (Levy).
ais20-Emotions-Love.md&6.11&>- If sex robots really have this effect, they should be considered a therapeutic tool (Sharkey 2017).
ais20-Emotions-Love.md&6.11&>- Dolls might be used in care homes as companions (like we saw above in the Turkle paper). In principle, they might also provide additional, sexual services.
ais20-Emotions-Love.md&6.11&>- UK Human Rights Act 1998 and Equality Act 2010: It is illegal not to support disabled people to enjoy the same pleasures as others enjoy in the privacy of their own homes. (Sharkey 2017)
ais20-Emotions-Love.md&6.11&
ais20-Emotions-Love.md&6.12&## Could sex robots reduce sex crimes? (1)
ais20-Emotions-Love.md&6.12&
ais20-Emotions-Love.md&6.12&>- Could sex robots help reduce sex crimes?
ais20-Emotions-Love.md&6.12&>- Some sexual preferences cause harm to others or the person who has the preference themselves: voyeurism, exhibitionism, paedophilia. 
ais20-Emotions-Love.md&6.12&>- Would we accept a robot child substitute as a solution that satisfies a paedophile and keeps him or her from assaulting real children?
ais20-Emotions-Love.md&6.12&>- Ronald Arkin, robotics professor at the Georgia Institute of Technology: “people should not only legally be permitted to have such dolls, but perhaps some should be handed prescriptions for them. [Sex robots] might function as an outlet for people to express their urges, redirecting dark desires toward machines and away from real children.” (Sharkey 2017, 26).
ais20-Emotions-Love.md&6.12&
ais20-Emotions-Love.md&6.13&## Could sex robots reduce sex crimes? (2)
ais20-Emotions-Love.md&6.13&
ais20-Emotions-Love.md&6.13&>- Peter Fagan from the John Hopkins School of Medicine is sceptical that there ever will be therapeutic use for sex robots. [Such robots] would likely have a “reinforcing effect” on paedophilic ideation and “in many instances, cause it to be acted upon with greater urgency.” (Morin, 2016, cited after Sharkey 2017)
ais20-Emotions-Love.md&6.13&
ais20-Emotions-Love.md&6.13&
ais20-Emotions-Love.md&6.14&## Could sex robots reduce sex crimes? (3)
ais20-Emotions-Love.md&6.14&
ais20-Emotions-Love.md&6.14&Philosophy professor and robot ethicist Patrick Lin (California Polytechnic):
ais20-Emotions-Love.md&6.14&
ais20-Emotions-Love.md&6.14&> Treating paedophiles with robot sex-children is both a dubious and repulsive idea. Imagine treating racism by letting a bigot abuse a brown robot. Would that work? Probably not. If expressing racist feelings is a cure for them, then we wouldn’t see much racism in the world. (Sharkey, 2017)
ais20-Emotions-Love.md&6.14&
ais20-Emotions-Love.md&6.14&
ais22-Jobs.md&0.1&---
ais22-Jobs.md&0.1&title:  "AI and Society: 22. AI and Jobs"
ais22-Jobs.md&0.1&author: Andreas Matthias, Lingnan University
ais22-Jobs.md&0.1&date: October 29, 2019
ais22-Jobs.md&0.1&bibliography: papers.bib
ais22-Jobs.md&0.1&csl: andreas-matthias-apa.csl
ais22-Jobs.md&0.1&...
ais22-Jobs.md&0.1&
ais22-Jobs.md&0.1&
ais22-Jobs.md&1.0&# Introduction
ais22-Jobs.md&1.0&
ais22-Jobs.md&1.1&## Sources
ais22-Jobs.md&1.1&
ais22-Jobs.md&1.1&For this chapter:
ais22-Jobs.md&1.1&
ais22-Jobs.md&1.1&- Frontier Economics (2018). The Impact of Artificial Intelligence on Work. An Evidence Review Prepared for the Royal Society and the British Academy (available online).
ais22-Jobs.md&1.1&- Lexer MG and Scarcella L: “The effects of artificial intelligence on labor markets – A critical analysis of solution models from a tax law and social security law perspective,” available online.
ais22-Jobs.md&1.1&- Other sources (particularly pictures) as cited in the text. 
ais22-Jobs.md&1.1&
ais22-Jobs.md&1.2&## The scope of the discussion (Frontier report)
ais22-Jobs.md&1.2&
ais22-Jobs.md&1.2&![Frontier: The impact of AI on work](graphics/22-royal-society-impact.png)\ 
ais22-Jobs.md&1.2&
ais22-Jobs.md&1.2&
ais22-Jobs.md&1.3&## The scope of the discussion (2)
ais22-Jobs.md&1.3&
ais22-Jobs.md&1.3&>- AI applications
ais22-Jobs.md&1.3&>     - Applications of AI in specific sectors/occupations.
ais22-Jobs.md&1.3&>     - Forecasts of future AI capabilities.
ais22-Jobs.md&1.3&>     - Computer science on improving AI systems.
ais22-Jobs.md&1.3&>- Impact of AI on work
ais22-Jobs.md&1.3&>     - Analysis of economic and social impact of ICT on work.
ais22-Jobs.md&1.3&>     - Forecasts of future skill demand.
ais22-Jobs.md&1.3&>     - Quantitative estimates of impact of AI on work.
ais22-Jobs.md&1.3&>- Social consequences
ais22-Jobs.md&1.3&>     - Analysis of earlier technological change (industrial revolutions).
ais22-Jobs.md&1.3&>     - Psychology of human-machine interaction.
ais22-Jobs.md&1.3&>     - Economic impact of automation on workers.
ais22-Jobs.md&1.3&>     - Work as a source of meaning.
ais22-Jobs.md&1.3&>- Ethics, politics and regulation
ais22-Jobs.md&1.3&>     - Political and wider implications of AI.
ais22-Jobs.md&1.3&>     - Ethical considerations on the use of AI.
ais22-Jobs.md&1.3&>     - Technoregulation issues.
ais22-Jobs.md&1.3&
ais22-Jobs.md&1.3&
ais22-Jobs.md&2.0&# AI applications
ais22-Jobs.md&2.0&
ais22-Jobs.md&2.1&## Applications of AI in specific sectors/occupations
ais22-Jobs.md&2.1&
ais22-Jobs.md&2.1&>- It is often thought that AI and robot workers will primarily replace workers in factories.
ais22-Jobs.md&2.1&>- *Do you think that this is true?*
ais22-Jobs.md&2.1&>- Although factory workers will certainly also be replaced by AI systems, the job loss will not end there.
ais22-Jobs.md&2.1&>- As opposed to previous industrial revolutions that primarily replaced human physical work with machine work, the AI revolution creates machines that can perform very complex tasks better than humans. We will see examples below.
ais22-Jobs.md&2.1&
ais22-Jobs.md&2.2&## Past and present automation
ais22-Jobs.md&2.2&
ais22-Jobs.md&2.2&>- Automation and the loss of jobs are not new phenomena.
ais22-Jobs.md&2.2&>- Since the Industrial Revolution, human workers have been replaced by automated machines:
ais22-Jobs.md&2.2&>     - Cloth making, weaving in the 19th century (power loom, Jacquard machine).
ais22-Jobs.md&2.2&>     - Car production in the early 20th century (assembly line).
ais22-Jobs.md&2.2&>     - Sales and manual fare collection (bus conductors replaced by Octopus cards,  shops replaced by vending machines, automation at McDonalds).
ais22-Jobs.md&2.2&>     - Street shops replaced by online shopping.
ais22-Jobs.md&2.2&>     - Doormen and liftboys replaced by automatic doors and elevators.
ais22-Jobs.md&2.2&>     - Typists, secretaries, accountants and human computers (!) replaced by electronic computers, word processing, spreadsheet software.
ais22-Jobs.md&2.2&>     - “The Post reported last year that a Foxconn plant in Kunshan, Jiangsu province, had already replaced 60,000 workers with robots.” (SCMP)
ais22-Jobs.md&2.2&
ais22-Jobs.md&2.3&## McDonalds cashierless ordering
ais22-Jobs.md&2.3&
ais22-Jobs.md&2.3&![](graphics/22-mcd-2.jpeg)\
ais22-Jobs.md&2.3&
ais22-Jobs.md&2.3&
ais22-Jobs.md&2.3&
ais22-Jobs.md&2.3&
ais22-Jobs.md&2.4&## Industrial robots don’t threaten jobs
ais22-Jobs.md&2.4&
ais22-Jobs.md&2.4&But:
ais22-Jobs.md&2.4&
ais22-Jobs.md&2.4&![](graphics/22-industrial-robots-2.png)\
ais22-Jobs.md&2.4&
ais22-Jobs.md&2.4&
ais22-Jobs.md&2.4&
ais22-Jobs.md&2.4&
ais22-Jobs.md&2.5&## Distinction between automation and autonomous robots
ais22-Jobs.md&2.5&
ais22-Jobs.md&2.5&>- Automation: Use of (dumb or intelligent) machines in industrial production tasks (has a long history, going back to windmills and horse-carts).
ais22-Jobs.md&2.5&>     - Automation has caused loss of jobs throughout the centuries. But we got used to this. These were primarily low-qualifications jobs.
ais22-Jobs.md&2.5&>- Autonomous, learning robots: New phenomenon. 
ais22-Jobs.md&2.5&>     - Traditional machines are not autonomous and don’t learn. Modern robots do.
ais22-Jobs.md&2.5&>     - The job loss here will affect not only blue-collar workers, but also specialists.
ais22-Jobs.md&2.5&>     - (Radiologists who read X-rays, dermatologists, ophthalmologists, insurance specialists, tax and investment consultants, and many other professions).
ais22-Jobs.md&2.5&
ais22-Jobs.md&2.6&## What is new
ais22-Jobs.md&2.6&
ais22-Jobs.md&2.6&>- Novel capabilities of robots. Previously limited to automation of repetitive tasks. Now they excel at typically human tasks:
ais22-Jobs.md&2.6&>     - Diagnosing diseases from X-ray images and blood samples.
ais22-Jobs.md&2.6&>     - Predicting insurance risk (no bias! Humans are very bad at probabilities!)
ais22-Jobs.md&2.6&>     - Playing chess, go (advanced strategic thinking) and poker (able to deceive humans and to deal with incomplete knowledge and deception by other players).
ais22-Jobs.md&2.6&>     - Replacing actors in movies (deep fake technology, reconstruction of deceased actors, for example in Star Wars).
ais22-Jobs.md&2.6&>     - Driving cars and flying airplanes more successfully than human beings.
ais22-Jobs.md&2.6&>     - Understanding spoken language, which enables efficient dictation to a machine and makes transcription jobs obsolete.
ais22-Jobs.md&2.6&>     - Better and better machine translation, making translators for casual uses (almost) obsolete.
ais22-Jobs.md&2.6&
ais22-Jobs.md&2.6&
ais22-Jobs.md&2.7&## Doctors
ais22-Jobs.md&2.7&
ais22-Jobs.md&2.7&>- X-Ray specialists (radiologists) and dermatologists (Wired, 25.1.2017): 
ais22-Jobs.md&2.7&>     - “Computer scientists and physicians at Stanford University recently teamed up to train a deep learning algorithm on 130,000 images of 2,000 skin diseases. The result, the subject of a paper out today in Nature, performed as well as 21 board-certified dermatologists in picking out deadly skin lesions.”
ais22-Jobs.md&2.7&
ais22-Jobs.md&2.7&
ais22-Jobs.md&2.8&## Forecasts of future AI capabilities (1)
ais22-Jobs.md&2.8&
ais22-Jobs.md&2.8&>- The capabilities of AI systems are likely to further improve:
ais22-Jobs.md&2.8&>- Computers will continue to get faster and data storage cheaper.
ais22-Jobs.md&2.8&
ais22-Jobs.md&2.9&## Forecasts of future AI capabilities (2)
ais22-Jobs.md&2.9&
ais22-Jobs.md&2.9&>- More complex problems will need to be addressed, driving the development and financing of more capable AI systems, for example for:
ais22-Jobs.md&2.9&>     - prediction of global warming effects and weather models, 
ais22-Jobs.md&2.9&>     - development of stronger data encryption methods,
ais22-Jobs.md&2.9&>     - AI-based search for resources (oil, rare elements needed in electronics),
ais22-Jobs.md&2.9&>     - state surveillance (processing of large amounts of text, voice and video data),
ais22-Jobs.md&2.9&>     - savings in labour costs (taxi and truck drivers, warehouse workers, factory workers),
ais22-Jobs.md&2.9&>     - more targeted advertisements and AI-based marketing in general,
ais22-Jobs.md&2.9&>     - AI-based manipulation of democratic processes,
ais22-Jobs.md&2.9&>     - reduction of costs in health- and eldercare.
ais22-Jobs.md&2.9&>- All these are areas with very obvious, strong incentives for big financial investments.
ais22-Jobs.md&2.9&
ais22-Jobs.md&2.9&
ais22-Jobs.md&2.10&## Forecasts of future AI capabilities (3)
ais22-Jobs.md&2.10&
ais22-Jobs.md&2.10&>- For these areas, machines will need to develop their capabilities:
ais22-Jobs.md&2.10&>     - Faster computation, complex pattern analysis in big datasets (weather, global warming).
ais22-Jobs.md&2.10&>     - Quantum computing (data encryption).
ais22-Jobs.md&2.10&>     - Fast analysis of video and audio data (surveillance).
ais22-Jobs.md&2.10&>     - New methods of surveillance (gait recognition, “seeing” through walls, analysis of stray electromagnetic emissions of equipment).
ais22-Jobs.md&2.10&>     - Autonomy in robot movement in the real world (self-driving cars, buses, trucks; factory automation; hospital robots).
ais22-Jobs.md&2.10&>     - Improvements in robot strength, robot vision and mechanical precision (robot surgery, factory floor uses).
ais22-Jobs.md&2.10&>     - Better understanding of human emotions and the ability to engage emotionally with humans (eldercare, healthcare, emotional manipulation of voters and customers in advertising).
ais22-Jobs.md&2.10&
ais22-Jobs.md&2.10&
ais22-Jobs.md&2.11&## Computer science research on AI
ais22-Jobs.md&2.11&
ais22-Jobs.md&2.11&![](graphics/22-cs1.png)\
ais22-Jobs.md&2.11&
ais22-Jobs.md&2.11&
ais22-Jobs.md&2.11&
ais22-Jobs.md&2.11&
ais22-Jobs.md&2.11&
ais22-Jobs.md&2.12&## Benefits of automation. Example: driving
ais22-Jobs.md&2.12&
ais22-Jobs.md&2.12&Automation is not all bad, though. Here are some statistics on the quality of human car driving:
ais22-Jobs.md&2.12&
ais22-Jobs.md&2.12&>- Tri-Level Study of the Causes of Traffic Accidents (1979): “human errors and deficiencies” are a definite or probable cause in 90-93% of the incidents examined. 
ais22-Jobs.md&2.12&>- UK study (1980): driver error, pedestrian error, or impairment as the "main contributing factor" in 95% of the crashes examined.
ais22-Jobs.md&2.12&>- US study (2001): “a driver behavioral error caused or contributed to” 99% of the crashes investigated.
ais22-Jobs.md&2.12&>- NHTSA’s 2008 National Motor Vehicle Crash Causation Survey: Human error is the critical reason for 93% of crashes.
ais22-Jobs.md&2.12&>- (Source: http://cyberlaw.stanford.edu/blog/2013/12/human-error-cause-vehicle-crashes)
ais22-Jobs.md&2.12&
ais22-Jobs.md&2.12&
ais22-Jobs.md&2.12&
ais22-Jobs.md&3.0&# Impact of AI on work
ais22-Jobs.md&3.0&
ais22-Jobs.md&3.1&## Analysis of economic and social impact of ICT on work
ais22-Jobs.md&3.1&
ais22-Jobs.md&3.1&![](graphics/22-automation-potential.png)\
ais22-Jobs.md&3.1&
ais22-Jobs.md&3.1&
ais22-Jobs.md&3.1&
ais22-Jobs.md&3.2&## Jobs lost in 2020 (percentages)^[http://careerbright.com/wp-content/uploads/2017/07/Technology-vs.-Jobs-in-2020_Infographic-03.jpg]
ais22-Jobs.md&3.2&
ais22-Jobs.md&3.2&![](graphics/22-percentages.png)\
ais22-Jobs.md&3.2&
ais22-Jobs.md&3.2&
ais22-Jobs.md&3.2&
ais22-Jobs.md&3.2&
ais22-Jobs.md&3.3&## Skills and unemployment
ais22-Jobs.md&3.3&
ais22-Jobs.md&3.3&![](graphics/22-social-skills.png)\
ais22-Jobs.md&3.3&
ais22-Jobs.md&3.3&
ais22-Jobs.md&3.3&
ais22-Jobs.md&3.3&
ais22-Jobs.md&3.3&
ais22-Jobs.md&3.3&
ais22-Jobs.md&3.4&## Skill demand and automation in medicine
ais22-Jobs.md&3.4&
ais22-Jobs.md&3.4&![](graphics/22-repetitive-interactive.png)\
ais22-Jobs.md&3.4&
ais22-Jobs.md&3.4&
ais22-Jobs.md&3.4&
ais22-Jobs.md&3.5&## The creative/interaction scale (1)
ais22-Jobs.md&3.5&
ais22-Jobs.md&3.5&>- Observe in the previous graphic how we can generally divide job skills along two axes:
ais22-Jobs.md&3.5&>     - Interaction-rich (teachers, psychologists, entertainers) vs. interaction-poor jobs (truck driver, construction worker, software developer, farmer).
ais22-Jobs.md&3.5&>     - Creative/flexible (something new happens every day: journalist, psychologist, scientist, artist, businessman, some doctors) vs repetitive (same activities every day: news presenter, factory worker, airline pilot).
ais22-Jobs.md&3.5&
ais22-Jobs.md&3.6&## The creative/interaction scale (2)
ais22-Jobs.md&3.6&
ais22-Jobs.md&3.6&>- The airline pilot example is interesting. 
ais22-Jobs.md&3.6&>     - One could argue that pilots are “creative” in that their job demands flexible reactions to unforeseeable events during flight.
ais22-Jobs.md&3.6&>     - But, on the other hand, for the overwhelming majority of flights, these events are the same and largely predictable: winds, turbulence, low visibility, mechanical failures of the plane.
ais22-Jobs.md&3.6&>- *So, would a pilot be easily replaceable by an AI system or not? What do you think?*
ais22-Jobs.md&3.6&
ais22-Jobs.md&3.7&## Automation and loss of quality (1)
ais22-Jobs.md&3.7&
ais22-Jobs.md&3.7&>- Some jobs (like the pilot’s) can be automated but with some loss of features.
ais22-Jobs.md&3.7&>- An autopilot can steer a plane perfectly for the overwhelming majority of flights.
ais22-Jobs.md&3.7&>- In 2016, there were around 2.5 accidents (0.39 fatal accidents) of airplanes per million (!) departures.
ais22-Jobs.md&3.7&>- Assuming that a pilot would have prevented those, while an AI would have crashed, and assuming further that an AI system can fly the plane perfectly in the absence of an accident condition, this gives an increase in accidents of 0.00025% if we replace pilots with AI systems.
ais22-Jobs.md&3.7&
ais22-Jobs.md&3.8&## Automation and loss of quality (2)
ais22-Jobs.md&3.8&
ais22-Jobs.md&3.8&>- These numbers are just an example estimation. They are not meant to be a serious statement about flight safety. You should just see how you could go about estimating the risk in such scenarios.
ais22-Jobs.md&3.8&>- Even if we are off by multiple orders of magnitude, it is unlikely that the risk from employing an autopilot will ever come close to even a hundredth of a percent.
ais22-Jobs.md&3.8&>- Consider also that around 50% of all crashes are due to pilot error. These would automatically be eliminated by the use of AI too.
ais22-Jobs.md&3.8&>- So it seems that it would be okay to replace pilots with AI systems, even if we lose the pilot’s (very rarely exercised) ability to react creatively to a flight emergency.
ais22-Jobs.md&3.8&
ais22-Jobs.md&3.9&## Nothing is really clear
ais22-Jobs.md&3.9&
ais22-Jobs.md&3.9&>- It is currently impossible to say whether the loss of jobs due to AI will lead to:
ais22-Jobs.md&3.9&>     - a lack of jobs and the resulting mass unemployment (“Scenario A”); or 
ais22-Jobs.md&3.9&>     - a complete change of required skills as a result of newly-created jobs (“Scenario B”)^[Lexer MG and Scarcella L: “The effects of artificial intelligence on labor markets – A critical analysis of solution models from a tax law and social security law perspective,” available online.].
ais22-Jobs.md&3.9&>- The result is likely to be some combination of the two.
ais22-Jobs.md&3.9&
ais22-Jobs.md&3.9&
ais22-Jobs.md&3.9&
ais22-Jobs.md&4.0&# Social consequences
ais22-Jobs.md&4.0&
ais22-Jobs.md&4.1&## Economic impact of automation on workers (1)
ais22-Jobs.md&4.1&
ais22-Jobs.md&4.1&![](graphics/22-automation-gdp.png)\
ais22-Jobs.md&4.1&
ais22-Jobs.md&4.1&
ais22-Jobs.md&4.1&
ais22-Jobs.md&4.2&## Economic impact of automation on workers (2)^[Source: https://www.visualcapitalist.com/charting-automation-potential-of-u-s-jobs]
ais22-Jobs.md&4.2&
ais22-Jobs.md&4.2&
ais22-Jobs.md&4.2&![](graphics/22-automation-wage.jpg)\
ais22-Jobs.md&4.2&
ais22-Jobs.md&4.2&
ais22-Jobs.md&4.2&
ais22-Jobs.md&4.2&
ais22-Jobs.md&4.3&## Economic impact of automation on workers (3)^[Source: https://www.visualcapitalist.com/charting-automation-potential-of-u-s-jobs]
ais22-Jobs.md&4.3&
ais22-Jobs.md&4.3&![](graphics/22-income-risk.jpg)\
ais22-Jobs.md&4.3&
ais22-Jobs.md&4.3&
ais22-Jobs.md&4.4&## Economic impact of automation on workers (4)
ais22-Jobs.md&4.4&
ais22-Jobs.md&4.4&>- What can we learn from these diagrams?
ais22-Jobs.md&4.4&>     - In the first diagram, we can clearly see that we can draw a straight, falling line through the data points.
ais22-Jobs.md&4.4&>     - There is a strong inverse correlation between GDP per person and jobs in risk of automation. 
ais22-Jobs.md&4.4&>     - Poorer countries have many more jobs in risk of automation than richer countries. 
ais22-Jobs.md&4.4&>- *Why is that?*
ais22-Jobs.md&4.4&>     - The jobs in poorer countries are often manual manufacturing, worker or farmer jobs which can easily be automated.
ais22-Jobs.md&4.4&>     - The jobs in richer countries are often service jobs (high on the interaction scale) or creative/development jobs (high on the creativity scale) and therefore difficult to automate.
ais22-Jobs.md&4.4&
ais22-Jobs.md&4.5&## Economic impact of automation on workers (5)
ais22-Jobs.md&4.5&
ais22-Jobs.md&4.5&>- Diagram 2 (the first with the coloured bubbles) shows clearly (?) that the automation potential is almost all concentrated on jobs paying less than 30-40 USD/hour.
ais22-Jobs.md&4.5&>- Diagram 3 also shows that the best paid jobs (on the top of the vertical axis) are least likely to be automated. (Although you might disagree with their estimation regarding physicians and surgeons!)
ais22-Jobs.md&4.5&
ais22-Jobs.md&4.5&
ais22-Jobs.md&4.6&## Full and partial automation of jobs
ais22-Jobs.md&4.6&
ais22-Jobs.md&4.6&>- When we talk about unemployment from automation, we often overlook *partial* automation.
ais22-Jobs.md&4.6&>- Job automation is not an all-or-nothing game.
ais22-Jobs.md&4.6&>- For example, robot-assisted “minimally invasive surgery” does not replace doctors.
ais22-Jobs.md&4.6&>- But it  leads to faster turnaround times, earlier discharge of patients from hospitals, and thus a higher hospital capacity, without the need to employ more doctors or nurses. 
ais22-Jobs.md&4.6&>- So in the end, it **does** lead to less employment for medical staff, including specialists. 
ais22-Jobs.md&4.6&>- If we can do the work of 6 doctors with only 4, then 2 doctors will never be employed.
ais22-Jobs.md&4.6&>- This is often overlooked when we talk about unemployment. Greater efficiency is equal to partial unemployment.
ais22-Jobs.md&4.6&
ais22-Jobs.md&4.6&
ais22-Jobs.md&5.0&# Ethics, politics and regulation
ais22-Jobs.md&5.0&
ais22-Jobs.md&5.1&## Worldwide balance of automation (1)
ais22-Jobs.md&5.1&
ais22-Jobs.md&5.1&![](graphics/22-industrial-robots.png)\
ais22-Jobs.md&5.1&
ais22-Jobs.md&5.1&
ais22-Jobs.md&5.1&
ais22-Jobs.md&5.2&## Worldwide balance of automation (2)
ais22-Jobs.md&5.2&
ais22-Jobs.md&5.2&>- In the previous diagram, you see that South Korea and Singapore are two of the countries with most robots per 10,000 manufacturing workers.
ais22-Jobs.md&5.2&>- But you have to be careful with these numbers.
ais22-Jobs.md&5.2&>- These also depend on the more general kind of manufacturing that is performed in a country.
ais22-Jobs.md&5.2&>- Some kinds of manufacturing (cars, electronics) are done at a big scale and are more suitable for the use of robots. Therefore, good, efficient robots for such products have existed for a long time.
ais22-Jobs.md&5.2&>- For other kinds of manufacturing (crafts, more specialised artefacts) there might not exist suitable factory robots, or the factories might be too small to make the development or deployment of robots in such factories economically viable.
ais22-Jobs.md&5.2&
ais22-Jobs.md&5.3&## Worldwide balance of automation (3)
ais22-Jobs.md&5.3&
ais22-Jobs.md&5.3&>- So when you see such a diagram, don’t assume that it says something only about robot use or industrialisation levels. It equally might say something about the kinds of products a country manufactures, how centralised the manufacturing sector is, and many other factors.
ais22-Jobs.md&5.3&
ais22-Jobs.md&5.3&
ais22-Jobs.md&5.4&## Worldwide balance of automation (4)^[futurism.com]
ais22-Jobs.md&5.4&
ais22-Jobs.md&5.4&![](graphics/22-developing.png)\
ais22-Jobs.md&5.4&
ais22-Jobs.md&5.4&
ais22-Jobs.md&5.5&## Worldwide balance of automation (5)
ais22-Jobs.md&5.5&
ais22-Jobs.md&5.5&>- Again, this is mostly because of the risk of automation in agriculture-related jobs.
ais22-Jobs.md&5.5&>- Plant management and harvesting can now be done well by machines (at least for some kinds of produce) and this will reduce the need for seasonal workers in agriculture.
ais22-Jobs.md&5.5&
ais22-Jobs.md&5.5&
ais22-Jobs.md&5.6&## Worldwide balance of automation (6)^[futurism.com]
ais22-Jobs.md&5.6&
ais22-Jobs.md&5.6&![](graphics/22-developed.png)\
ais22-Jobs.md&5.6&
ais22-Jobs.md&5.6&
ais22-Jobs.md&5.6&
ais22-Jobs.md&5.7&## Worldwide balance of automation (7)
ais22-Jobs.md&5.7&
ais22-Jobs.md&5.7&>- In the cities, it is more the service and transportation jobs that are at risk:
ais22-Jobs.md&5.7&>     - Cashiers, ticket counters, shops (replaced by Internet shopping).
ais22-Jobs.md&5.7&>     - Bus, truck, taxi drivers (replaced by self-driving cars).
ais22-Jobs.md&5.7&>     - Police and security (replaced by elaborate surveillance systems).
ais22-Jobs.md&5.7&>     - Restaurant staff (replaced by ordering machines, takeout, delivery).
ais22-Jobs.md&5.7&>     - Construction workers (replaced by machinery).
ais22-Jobs.md&5.7&>     - Entertainment (cinemas, concert halls, live performances replaced by home entertainment options).
ais22-Jobs.md&5.7&
ais22-Jobs.md&5.7&
ais22-Jobs.md&5.8&## Rich and poor (1)
ais22-Jobs.md&5.8&
ais22-Jobs.md&5.8&>- Especially in HK, the gap between rich and poor is very big. These developments will make things even worse for those who are not qualified for more demanding or creative jobs.
ais22-Jobs.md&5.8&>- Oxfam Hong Kong report (2015):
ais22-Jobs.md&5.8&>     - About nine percent of Hong Kong’s population live in working poor families, 
ais22-Jobs.md&5.8&>     - The number of working poor households in the city reached 189,500 last year, representing a 10.6 percent increase compared with five years ago.
ais22-Jobs.md&5.8&>     - These households include 647,500 people, about nine percent of Hong Kong’s population.
ais22-Jobs.md&5.8&>     - Same report: Hong Kong’s richest one percent owns 52.6 percent of the city’s total wealth, whereas the wealthiest 10 percent take up 77.5 percent of the total wealth.
ais22-Jobs.md&5.8&>     - This is the highest among developed regions globally.
ais22-Jobs.md&5.8&
ais22-Jobs.md&5.9&## Rich and poor (2)
ais22-Jobs.md&5.9&
ais22-Jobs.md&5.9&>- *When we employ robots, who is most benefited? The rich or the poor?*
ais22-Jobs.md&5.9&>- The income from robot use goes mostly to the already wealthy. Robots are a very efficient machine for the concentration of wealth.
ais22-Jobs.md&5.9&>- Example: Tesla. 
ais22-Jobs.md&5.9&>     - Tesla wants to create their own ride-sharing service (Reuters Oct 20, 2016).
ais22-Jobs.md&5.9&>     - So they have the profit from selling the car, plus the continuous income from the use of the sold car.
ais22-Jobs.md&5.9&>     - (The Tesla sales contract forbids the driver to offer his own commercial ride-sharing!)
ais22-Jobs.md&5.9&
ais22-Jobs.md&5.10&## Rich and poor (3)
ais22-Jobs.md&5.10&
ais22-Jobs.md&5.10&>- The same for other businesses: Robots allow hospitals and elder-care institutions to save money on human personnel.
ais22-Jobs.md&5.10&>     - Robots are very cheap in comparison: 
ais22-Jobs.md&5.10&>     - TUG robots in hospitals:
ais22-Jobs.md&5.10&>     - Delivery of food, clean linen, medicines to the patient bed: reduction of cost by 50% to 80%.
ais22-Jobs.md&5.10&>     - University of California, San Francisco Medical Center: 25 robots, each 240,000 USD, including all retrofitting and infrastructure work to the hospital itself.
ais22-Jobs.md&5.10&>     - The UCSF Medical Center *generates* an annual income of $1.8 billion!
ais22-Jobs.md&5.10&>     - The robots cost practically nothing.
ais22-Jobs.md&5.10&>- Payback time for new robot installation: China: around 1.5 years, shrinking with time (was 2 years in 2013).
ais22-Jobs.md&5.10&
ais22-Jobs.md&5.10&
ais22-Jobs.md&6.0&# Technoregulation issues
ais22-Jobs.md&6.0&
ais22-Jobs.md&6.1&## The problem of technoregulation
ais22-Jobs.md&6.1&
ais22-Jobs.md&6.1&>- When we talk about how technologies affect our life and economy, it looks like we (as societies) are free to decide whether we want to use particular technologies or not.
ais22-Jobs.md&6.1&>- *But is this true? Do societies freely and rationally decide to use or not to use particular technologies?*
ais22-Jobs.md&6.1&>- Who regulates technology? Can we, as citizens or societies, proactively regulate technology?
ais22-Jobs.md&6.1&
ais22-Jobs.md&6.1&
ais22-Jobs.md&6.2&## Relation between technology and society (1)
ais22-Jobs.md&6.2&
ais22-Jobs.md&6.2&Various theories are possible:
ais22-Jobs.md&6.2&
ais22-Jobs.md&6.2&>- Technological determinism: Technology determines social developments.
ais22-Jobs.md&6.2&>       - Printing press
ais22-Jobs.md&6.2&>       - Automobile
ais22-Jobs.md&6.2&>- Linear relation of science-to-technology development:
ais22-Jobs.md&6.2&>       - A (mythical) linear succession of: Basic science, applied science, development, and commercialisation.
ais22-Jobs.md&6.2&>- An extreme form of this is "autonomous technology": The thesis that technology is out of human or social control entirely, and develops according to its own logic.
ais22-Jobs.md&6.2&>       - For example, cars lead to streets, lead to fuelling stations, lead to accidents, lead to hospitals, lead to insurance companies, and so on.
ais22-Jobs.md&6.2&
ais22-Jobs.md&6.3&## Relation between technology and society (2)
ais22-Jobs.md&6.3&
ais22-Jobs.md&6.3&>- Social constructivism: Groups in society compete to influence or control technological designs. The process goes from *interpretive flexibility* to stabilisation of designs.
ais22-Jobs.md&6.3&>       - "Interpretative flexibility" means that each technological artifact has different meanings and interpretations for various groups.
ais22-Jobs.md&6.3&>       - For example, bicycle air tires were not accepted equally by all users.
ais22-Jobs.md&6.3&>       - Some found them comfortable.
ais22-Jobs.md&6.3&>       - Others found them ugly.
ais22-Jobs.md&6.3&>       - Sports cyclists thought that bikes would run slower with air tires. (Bijker and Pinch)
ais22-Jobs.md&6.3&
ais22-Jobs.md&6.4&## Relation between technology and society (3)
ais22-Jobs.md&6.4&
ais22-Jobs.md&6.4&>- Actor-Network Theory (ANT): Technology can be described as complex networks involving material things, humans, and concepts ("material-semiotic" relations).
ais22-Jobs.md&6.4&>       - For example, the interactions in a bank involve both people, their ideas, and technologies. Together these form a single network.
ais22-Jobs.md&6.4&>       - Actor-network theory tries to explain how material-semiotic networks come together to act as a whole (for example, a bank is network of people, things, and concepts. For certain purposes acts as a single entity).
ais22-Jobs.md&6.4&>       - Such actor-networks are potentially transient. They dissolve and are re-made again.
ais22-Jobs.md&6.4&>       - This means that relations need to be repeatedly “performed” or the network will dissolve. (The bank clerks need to come to work each day, and the computers need to keep on running.)
ais22-Jobs.md&6.4&
ais22-Jobs.md&6.5&## STS (Science and Technology Studies)
ais22-Jobs.md&6.5&
ais22-Jobs.md&6.5&>- These kinds of thoughts are the basis of a whole area of research, called "STS" (“science and technology studies,” or “science, technology, society”).
ais22-Jobs.md&6.5&>- STS tries to explore how technology shapes society, and/or how social factors affect technological development.
ais22-Jobs.md&6.5&>- Obviously, this is critical for our topic. 
ais22-Jobs.md&6.5&>     - If technological determinism is true, then any attempts to regulate technology are futile.
ais22-Jobs.md&6.5&>     - If social constructivism is true, then, again, regulation by the state alone cannot sufficiently control technology. All groups in society must cooperate in order to shape the future development of technology.
ais22-Jobs.md&6.5&
ais22-Jobs.md&6.5&
ais22-Jobs.md&7.0&# Universal Basic Income
ais22-Jobs.md&7.0&
ais22-Jobs.md&7.1&## Universal Basic Income: The idea
ais22-Jobs.md&7.1&
ais22-Jobs.md&7.1&>- Robots take away human jobs.
ais22-Jobs.md&7.1&>- Robots generate income doing these jobs, but they don’t need most of that income (they don’t consume things, have no social life, no children, no education needs).
ais22-Jobs.md&7.1&>- Robots only need a small fraction of the generated value for energy costs, maintenance, repair and to pay back the initial investment.
ais22-Jobs.md&7.1&>- This free income could be given to the ex-employees who were unemployed because of that robot.
ais22-Jobs.md&7.1&>- To simplify the mechanics of that transaction, we could pool all robot-generated value and distribute it equally among the population.
ais22-Jobs.md&7.1&>- This is then called a “Universal Basic Income (UBI)”.
ais22-Jobs.md&7.1&
ais22-Jobs.md&7.1&
ais22-Jobs.md&7.2&## Universal Basic Income: Definition^[https://citizensincome.org/citizens-income/what-is-it/]
ais22-Jobs.md&7.2&
ais22-Jobs.md&7.2&>- In order for income to qualify as UBI it must fulfil some criteria. It must be:
ais22-Jobs.md&7.2&>     - Unconditional: A basic income would vary with age, but with no other conditions. Everyone of the same age would receive the same basic income, whatever their gender, employment status, family structure, contribution to society, housing costs, or anything else.
ais22-Jobs.md&7.2&>     - Automatic: Someone's basic income would be automatically paid weekly or monthly into a bank account or similar.
ais22-Jobs.md&7.2&>     - Non-withdrawable: Basic incomes would not be means-tested. Whether someone's earnings increase, decrease, or stay the same, their basic income will not change.
ais22-Jobs.md&7.2&>     - Individual: Basic incomes would be paid on an individual basis and not on the basis of a couple or household.
ais22-Jobs.md&7.2&>     - As a right: Every legal resident would receive a basic income, subject to a minimum period of legal residency and continuing residency for most of the year.
ais22-Jobs.md&7.2&
ais22-Jobs.md&7.2&
ais22-Jobs.md&7.3&## Universal Basic Income: Benefits
ais22-Jobs.md&7.3&
ais22-Jobs.md&7.3&>- The benefits of UBI go far beyond the “AI and Jobs” discussion.
ais22-Jobs.md&7.3&>     - Under UBI, employees could afford to wait for better jobs rather than be forced to take the first that they can get. This would increase job satisfaction, bring them higher income, and make better use of the employees’ education and experience.
ais22-Jobs.md&7.3&>     - Citizens could volunteer more and take time off for creative activities or further education.
ais22-Jobs.md&7.3&>     - Citizens could afford to provide better care to elderly family members and children, reducing the need for costly and inefficient elder- and childcare schemes.
ais22-Jobs.md&7.3&>     - Young couples would be encouraged to have more children.
ais22-Jobs.md&7.3&>     - Much less bureaucracy than other welfare schemes that require extensive paperwork.
ais22-Jobs.md&7.3&>- In terms of AI, UBI would make it easier for society to deal with the loss of jobs due to automation.
ais22-Jobs.md&7.3&
ais22-Jobs.md&7.3&
ais22-Jobs.md&7.4&## Universal Basic Income: Problems
ais22-Jobs.md&7.4&
ais22-Jobs.md&7.4&>- UBI could lead to inflation. If everyone has the same amount of money available, but we don’t have a greater supply of goods, then the prices must rise to balance the demand and supply.
ais22-Jobs.md&7.4&>- This also means that, in the long run, there wouldn’t be an increased standard of living.
ais22-Jobs.md&7.4&>- A UBI might cause people to get lazy and do nothing instead of looking for a job. In the long run, this would harm a society’s productivity.
ais22-Jobs.md&7.4&>- Lacking work experience would make it more difficult for long-term unemployed people to find work again and would perpetuate unemployment.
ais22-Jobs.md&7.4&>- Chronic unemployment might cause all sorts of psychological, family and social problems.
ais22-Jobs.md&7.4&
ais22-Jobs.md&7.5&## Other functions of work
ais22-Jobs.md&7.5&
ais22-Jobs.md&7.5&>- “If people cannot find work, then let’s pay them to stay at home! Since machines will take care of the production, society will be able to afford paying people who don’t need to work.” -- Do you see any problems with this statement?
ais22-Jobs.md&7.5&>- Work is not only a source of income, but also a source of meaning for the working person.
ais22-Jobs.md&7.5&>     - Work provides identification with a group,
ais22-Jobs.md&7.5&>     - a regular daily activity that fills one’s time,
ais22-Jobs.md&7.5&>     - friendships and other human relationships at the workplace,
ais22-Jobs.md&7.5&>     - intellectual stimulation, learning and practice in problem-solving.
ais22-Jobs.md&7.5&>- These are all important components of human well-being.
ais22-Jobs.md&7.5&>- It will not be easy to abolish work and pay people for “doing nothing” without causing grave social problems.
ais22-Jobs.md&7.5&
ais22-Jobs.md&7.5&
ais22-Jobs.md&7.6&## UBI experiments^[https://www.thebalance.com/universal-basic-income-4160668]
ais22-Jobs.md&7.6&
ais22-Jobs.md&7.6&>- Alaska has a limited UBI since 1982. Each resident receives USD 1200 per year, which is only about a tenth of a full income.
ais22-Jobs.md&7.6&>- Finland started a UBI experiment in 2017, giving 2000 unemployed people 560 Euro per month as a basic income. The Finnish government stopped the program one year later.
ais22-Jobs.md&7.6&>- Scotland is funding research into a program that pays every citizen for life. Retirees would receive 150 pounds a week. Working adults would get 100 pounds and children under 16 would be paid 50 pounds a week.
ais22-Jobs.md&7.6&>- In 2016, Switzerland voted against universal income. The government proposed paying every resident 2,500 Swiss francs per month.
ais22-Jobs.md&7.6&>- So, presently the situation does not look encouraging. AI may change the basic conditions, though, and make a UBI inevitable.
ais22-Jobs.md&7.6&
ais22-Jobs.md&7.7&## A robot tax? (1)
ais22-Jobs.md&7.7&
ais22-Jobs.md&7.7&>- One proposal to finance a UBI would be a “robot tax,” that is, a tax to be payed by the employers of robots who have replaced human employees.
ais22-Jobs.md&7.7&>- This runs into a few difficulties:
ais22-Jobs.md&7.7&>     - If we calculate the tax proportionally to the number of replaced human workers, we would need to know how many humans have actually been replaced by the AI technology. This can be hard to calculate, especially for new businesses that have never employed humans, or for work that would be impossible to be done by humans.
ais22-Jobs.md&7.7&>     - How to calculate tax related to working hours when robots work 24 hours a day? Should the tax be proportional to hours worked? Does a robot that works 16 hours (instead of 8) replace *two* humans? This will depend on the kind of work done. Work that involves cooperation with other humans might not be efficient when performed at night.
ais22-Jobs.md&7.7&
ais22-Jobs.md&7.8&## A robot tax? (2)
ais22-Jobs.md&7.8&
ais22-Jobs.md&7.8&>- Alternatively, we could tax the total generated value of a company over the course of a year, independently of the number of workers replaced. This would simplify calculations, but might treat enterprises that generate a big amount of value with relatively few employees unfairly.
ais22-Jobs.md&7.8&>- Such taxes have been proposed in Austria (“Maschinensteuer,” 2016) and Italy (“IRAP,” regional tax on productive activities, 1997).
ais22-Jobs.md&7.8&>- There doesn’t seem to be an ideal solution to the problem, but multiple approaches are being considered.
ais22-Jobs.md&7.8&>- The problem of the effect of AI on jobs is not going away by itself, and some solution will have to be found.
ais22-Jobs.md&7.8&
ais22-Jobs.md&7.8&
ais22-Jobs.md&7.8&
ais22-Jobs.md&7.8&
ais22-Jobs.md&7.8&
ais24-War-Robots.md&0.1&
ais24-War-Robots.md&0.1&---
ais24-War-Robots.md&0.1&title:  "AI and Society: 24. War Robots and Arkin’s Ethical Governor"
ais24-War-Robots.md&0.1&author: Andreas Matthias, Lingnan University
ais24-War-Robots.md&0.1&date: November 25, 2019
ais24-War-Robots.md&0.1&...
ais24-War-Robots.md&0.1&
ais24-War-Robots.md&1.0&# Arkin's "Ethical Governor"
ais24-War-Robots.md&1.0&
ais24-War-Robots.md&1.1&## Arkin's "Ethical Governor"
ais24-War-Robots.md&1.1&
ais24-War-Robots.md&1.1&In the discussion about the moral control of autonomous machines, one of the most influential proposals is Ronald Arkin's "Ethical Governor" (2009).
ais24-War-Robots.md&1.1&
ais24-War-Robots.md&1.1&For example:
ais24-War-Robots.md&1.1&
ais24-War-Robots.md&1.1&- Arkin, R. (2009). *Governing lethal behaviour in autonomous robots.* Boca Raton, London, New York: CRC Press.
ais24-War-Robots.md&1.1&- Arkin, R., Ulam, P., & Duncan, B. (2009). *An ethical governor for constraining lethal action in an autonomous system.* Tech. Report No. GIT-GVU-09-02, GVU Center, Georgia Institute of Technology.
ais24-War-Robots.md&1.1&- Arkin, R.C. (2007). *Governing lethal behavior: Embedding ethics in a hybrid deliberative/reactive robot architecture.* Technical Report GIT-GVU-07-11, Mobile Robot Laboratory, College of Computing, Georgia Institute of Technology.
ais24-War-Robots.md&1.1&
ais24-War-Robots.md&1.2&## Read the paper
ais24-War-Robots.md&1.2&
ais24-War-Robots.md&1.2&See “24-arkin-constraining-lethal-action.pdf” in Moodle!
ais24-War-Robots.md&1.2&
ais24-War-Robots.md&1.3&## Capabilities and goals
ais24-War-Robots.md&1.3&
ais24-War-Robots.md&1.3&>- The ethical governor is supposed to be “capable of restricting lethal action of an autonomous system in a manner consistent with the Laws of War and Rules of Engagement” (Arkin et al., 2009)
ais24-War-Robots.md&1.3&>- The goal of the architecture is “to ensure that these systems conform to the legal requirements and responsibilities of a civilised nation.”
ais24-War-Robots.md&1.3&>- The governor “is a transformer/suppressor of system-generated lethal action to ensure that it constitutes an ethically permissible action, either nonlethal or obligated ethical lethal force.” (Arkin et al., 2009)
ais24-War-Robots.md&1.3&
ais24-War-Robots.md&1.4&## Points addressed
ais24-War-Robots.md&1.4&
ais24-War-Robots.md&1.4&The main points the Ethical Governor is trying to address:
ais24-War-Robots.md&1.4&
ais24-War-Robots.md&1.4&1. Discrimination.
ais24-War-Robots.md&1.4&2. Proportionality.
ais24-War-Robots.md&1.4&
ais24-War-Robots.md&1.4&*Can you explain these two?*
ais24-War-Robots.md&1.4&
ais24-War-Robots.md&1.5&## Discrimination
ais24-War-Robots.md&1.5&
ais24-War-Robots.md&1.5&>- In the Just War Theory, "discrimination" means to be able to distinguish civilians from combatants.
ais24-War-Robots.md&1.5&>- While it is considered morally acceptable to target combatants in a war, the fighting parties should not be allowed to target civilians.
ais24-War-Robots.md&1.5&>- *Any comments on this?*
ais24-War-Robots.md&1.5&>     - How to distinguish? (factory-working women in WW2 Germany, "terrorist" fighters, suicide bombers)
ais24-War-Robots.md&1.5&>     - Is this the only useful distinction? What about a soldier on leave? A soldier taking a break?
ais24-War-Robots.md&1.5&>     - Who defines what a civilian is? Each party for itself? The enemy? International conventions?
ais24-War-Robots.md&1.5&
ais24-War-Robots.md&1.6&## Proportionality
ais24-War-Robots.md&1.6&
ais24-War-Robots.md&1.6&>- In Just War Theory, "proportionality" means that the damage caused by a military action should be "proportional" to the expected gain from that action.
ais24-War-Robots.md&1.6&>- Violence should not be excessive, but used to the minimum amount necessary to reach a particular goal.
ais24-War-Robots.md&1.6&>- *Comments?*
ais24-War-Robots.md&1.6&>     - *Whose* gain is looked at in order to justify an act of violence? The attacker's? The victim's? Humanity's in general?
ais24-War-Robots.md&1.6&>     - Who defines what goal is a worthy goal?
ais24-War-Robots.md&1.6&>     - How to judge necessity? Were the atomic bombs in Hiroshima and Nagasaki really necessary? Who defines necessity and according to whose values?
ais24-War-Robots.md&1.6&>     - Was the trade-off of the atomic bombs a good one? (Essentially exchanging dead Japanese for dead Americans)
ais24-War-Robots.md&1.6&>     - Can there ever be "proportionality" for intentionally killing people? (Kant!)
ais24-War-Robots.md&1.6&
ais24-War-Robots.md&1.7&## Morality of war
ais24-War-Robots.md&1.7&
ais24-War-Robots.md&1.7&A few conclusions:
ais24-War-Robots.md&1.7&
ais24-War-Robots.md&1.7&>- It's important to be aware of the fact that, when we talk about the morality of war robots, we already make a series of assumptions that morally justify war and killing under particular circumstances.
ais24-War-Robots.md&1.7&>- We distinguish between "good" war and "bad" war, "good" killing and "bad" killing.
ais24-War-Robots.md&1.7&>- Still, we are often not consciously aware of the criteria that we use to distinguish "good" from "bad" killing.
ais24-War-Robots.md&1.7&>- Often they favour our own culture, values, or interests, over the enemy's.
ais24-War-Robots.md&1.7&>- But how can such a bias be justified?
ais24-War-Robots.md&1.7&>- Keep these points in mind for the following discussion.
ais24-War-Robots.md&1.7&
ais24-War-Robots.md&1.8&## What is a "governor"? (1)
ais24-War-Robots.md&1.8&
ais24-War-Robots.md&1.8&James Watt: Centrifugal governor (negative feedback controller):
ais24-War-Robots.md&1.8&
ais24-War-Robots.md&1.8&![](graphics/08-governor.png)\
ais24-War-Robots.md&1.8&
ais24-War-Robots.md&1.8&
ais24-War-Robots.md&1.8&
ais24-War-Robots.md&1.8&
ais24-War-Robots.md&1.9&## What is a "governor"? (2)
ais24-War-Robots.md&1.9&
ais24-War-Robots.md&1.9&>- “The term governor is inspired by Watts’ invention of the mechanical governor for the steam engine, a device that was intended to ensure that the mechanism behaved safely and within predefined bounds of performance. ... The same notion applies, where here the performance bounds are ethical ones.” (Arkin, 2007)
ais24-War-Robots.md&1.9&
ais24-War-Robots.md&1.10&## What is a "governor"? (3)
ais24-War-Robots.md&1.10&
ais24-War-Robots.md&1.10&>- When the engine goes too fast, the two rotating balls at the top of the centrifugal governor will move upwards, and they will pull on a lever that will close the steam valve, thus causing the machine to go slower.
ais24-War-Robots.md&1.10&>- When the engine is too slow, the balls will drop, and the valve will open, causing the engine to move faster.
ais24-War-Robots.md&1.10&>- This is a negative feedback loop that keeps the engine working withing a specified speed limit.
ais24-War-Robots.md&1.10&
ais24-War-Robots.md&1.11&## Similar approaches to robot morality
ais24-War-Robots.md&1.11&
ais24-War-Robots.md&1.11&>- Arkin’s is not the only concept aiming at an algorithmic moral control of robots.
ais24-War-Robots.md&1.11&>- If we look at *civilian* robot use, we find more work addressing the moral control of autonomous robots.
ais24-War-Robots.md&1.11&>     - Cloos (2005) discusses the Utilibot project, a proposal for robot control supposedly based on utilitarianist principles.
ais24-War-Robots.md&1.11&>     - Lucas and Comstock (2011) argue for a variant of utilitarianism (satisficing hedonistic act utilitarianism, SHAU) in robot control.
ais24-War-Robots.md&1.11&>     - Anderson and Anderson (2008) on the other hand, argue against utilitarianist approaches and in favour of moral robot control based on a concept of prima facie duties.
ais24-War-Robots.md&1.11&
ais24-War-Robots.md&1.12&## Technical description
ais24-War-Robots.md&1.12&
ais24-War-Robots.md&1.12&>- "Technically, the ethical governor is a constraint-driven system, which, on the basis of predicate and deontic logic, tries to evaluate an action, ... by satisfying various sets of constraints, like $C_{forbidden}$, $C_{obligate}$ and so on."
ais24-War-Robots.md&1.12&>- Every constraint is a data structure which has a type (e.g. “prohibition”), an origin (“laws of war”), and a logical form (“TargetDiscriminated AND TargetWithinProximityOfCulturalLandmark”)
ais24-War-Robots.md&1.12&
ais24-War-Robots.md&1.13&## Architecture
ais24-War-Robots.md&1.13&
ais24-War-Robots.md&1.13&![](graphics/08-arkin-screenshot-1.png)\ 
ais24-War-Robots.md&1.13&
ais24-War-Robots.md&1.13&
ais24-War-Robots.md&1.13&
ais24-War-Robots.md&1.13&
ais24-War-Robots.md&1.14&## Constraint data type
ais24-War-Robots.md&1.14&
ais24-War-Robots.md&1.14&![](graphics/08-arkin-constraints-1.png)\ 
ais24-War-Robots.md&1.14&
ais24-War-Robots.md&1.14&
ais24-War-Robots.md&1.14&
ais24-War-Robots.md&1.14&
ais24-War-Robots.md&1.15&## Constraint example
ais24-War-Robots.md&1.15&
ais24-War-Robots.md&1.15&![](graphics/08-arkin-constraints-2.png)\ 
ais24-War-Robots.md&1.15&
ais24-War-Robots.md&1.15&
ais24-War-Robots.md&1.15&
ais24-War-Robots.md&1.15&
ais24-War-Robots.md&1.16&## Origins of constraints
ais24-War-Robots.md&1.16&
ais24-War-Robots.md&1.16&![](graphics/08-arkin-constraints-4.png)\ 
ais24-War-Robots.md&1.16&
ais24-War-Robots.md&1.16&
ais24-War-Robots.md&1.16&
ais24-War-Robots.md&1.16&
ais24-War-Robots.md&1.17&## Constraints application algorithm
ais24-War-Robots.md&1.17&
ais24-War-Robots.md&1.17&![](graphics/08-arkin-constraints-3.png)\ 
ais24-War-Robots.md&1.17&
ais24-War-Robots.md&1.17&
ais24-War-Robots.md&1.17&
ais24-War-Robots.md&1.17&
ais24-War-Robots.md&1.18&## Military necessity overrides constraints
ais24-War-Robots.md&1.18&
ais24-War-Robots.md&1.18&![](graphics/08-arkin-necessity.png)\ 
ais24-War-Robots.md&1.18&
ais24-War-Robots.md&1.18&
ais24-War-Robots.md&1.18&
ais24-War-Robots.md&1.18&
ais24-War-Robots.md&1.18&
ais24-War-Robots.md&1.19&## Criticism of Arkin's Ethical Governor
ais24-War-Robots.md&1.19&
ais24-War-Robots.md&1.19&*You got an idea of how the Ethical Governor is supposed to work. How can the concept be criticised?*
ais24-War-Robots.md&1.19&
ais24-War-Robots.md&1.20&## Criticism: Interest conflict (1)
ais24-War-Robots.md&1.20&
ais24-War-Robots.md&1.20&>- Watt’s governor: the operator of the machine has an interest in its safe operation, and the governor helps him achieve that. 
ais24-War-Robots.md&1.20&>- Even if the operator would like the steam engine to work with more power, or at a higher speed than it was designed for, it would make no sense for him to override the centrifugal governor, since doing so would just destroy the machine without achieving any higher efficiency of its operation. 
ais24-War-Robots.md&1.20&>- The steam governor, as designed and constructed by the steam engine’s designer, therefore acts in the best interests of the engine’s operator at run-time.
ais24-War-Robots.md&1.20&
ais24-War-Robots.md&1.21&## Criticism: Interest conflict (2)
ais24-War-Robots.md&1.21&
ais24-War-Robots.md&1.21&>- This is not so with the ethical governor.
ais24-War-Robots.md&1.21&>- The designer of the ethical governor has the aim of implementing a device which will limit the possible actions of an autonomous war robot to a set of morally permissible  actions (assuming, for the moment, that the latter set can be clearly defined at all).
ais24-War-Robots.md&1.21&>- The operator of the war robot, on the other hand, has a conflicting interest: that of achieving the maximum tactical efficiency of the machine, and of carrying out a military operation successfully, thus achieving a predefined set of mission objectives.
ais24-War-Robots.md&1.21&>- The operator of the machine (the commanding officer for the particular operation) will therefore have an incentive to override the constraints imposed by the ethical governor, in order to achieve a better military result.
ais24-War-Robots.md&1.21&
ais24-War-Robots.md&1.22&## Criticism: Interest conflict (3)
ais24-War-Robots.md&1.22&
ais24-War-Robots.md&1.22&>- Arkin et al. (2009): permissible collateral damage is a *function of military necessity alone* (see image above).
ais24-War-Robots.md&1.22&>- Since the designer of the governor is the same as its operator (in both cases the same military hierarchy), it would be irrational to expect that the governor would be designed so as to act against military interests.
ais24-War-Robots.md&1.22&
ais24-War-Robots.md&1.23&## Criticism: Democratic control (1)
ais24-War-Robots.md&1.23&
ais24-War-Robots.md&1.23&> “Code is an efficient means of regulation. But its perfection makes it something different. One obeys these laws as code not because one should; one obeys these laws as code because one can do nothing else. There is no choice about whether to yield to the demand for a password; one complies if one wants to enter the system. In the well implemented system, there is no civil disobedience.” (Lessig, 1996) 
ais24-War-Robots.md&1.23&
ais24-War-Robots.md&1.24&## Criticism: Democratic control (2)
ais24-War-Robots.md&1.24&
ais24-War-Robots.md&1.24&At the same time, the code which both requires and enforces perfect obedience, is itself removed from view:
ais24-War-Robots.md&1.24&
ais24-War-Robots.md&1.24&> “The key criticism that I’ve identified so far is transparency. Code-based regulation -- especially of people who are not themselves technically expert -- risks making regulation invisible.” (Lessig, 2006, 138)
ais24-War-Robots.md&1.24&
ais24-War-Robots.md&1.25&## Criticism: Democratic control (3)
ais24-War-Robots.md&1.25&
ais24-War-Robots.md&1.25&>- Both Laws of War and Rules of Engagement are publicly visible and democratically approved documents, regulating in an open and transparent way a nation’s forces’ behaviour at war.
ais24-War-Robots.md&1.25&>- These documents are accessible both to the public which, in the final instance, authorizes them, and to the soldiers, whose behaviour they intend to guide. 
ais24-War-Robots.md&1.25&
ais24-War-Robots.md&1.26&## Criticism: Democratic control (4)
ais24-War-Robots.md&1.26&
ais24-War-Robots.md&1.26&>- Things change when Laws of War and Rules of Engagement become software.
ais24-War-Robots.md&1.26&>- Words, which for a human audience have more or less clear, if fuzzily delineated meanings, like "combatant" or "civilian," need to be “codified,” that is, turned into an unambiguous, machine-readable representation of the concept they denote. 
ais24-War-Robots.md&1.26&>- This interpretation cannot be assumed to be straightforward for various reasons.
ais24-War-Robots.md&1.26&>- *Can you guess?*
ais24-War-Robots.md&1.26&
ais24-War-Robots.md&1.27&## Criticism: Translation problems (1)
ais24-War-Robots.md&1.27&
ais24-War-Robots.md&1.27&>- *When is a hammer "ready to hand?" (Heidegger).*
ais24-War-Robots.md&1.27&>- When it is ready for me to use it for my purposes.
ais24-War-Robots.md&1.27&>- Readiness-to-hand as well as Dasein, cannot be expressed adequately by sets of “objective” properties at all (Dreyfus, 1990).
ais24-War-Robots.md&1.27&>- Whether, for instance, a hammer is “too heavy” for use is not translatable into one single, numerical expression of weight:
ais24-War-Robots.md&1.27&>     - The hammer’s “unreadiness to hand” will vary not only across different users,
ais24-War-Robots.md&1.27&>     - but also depending on the time of day,
ais24-War-Robots.md&1.27&>     - the health status and the mood of the user,
ais24-War-Robots.md&1.27&>     - and perhaps even the urgency of the task towards which the hammer is intended to be used.
ais24-War-Robots.md&1.27&
ais24-War-Robots.md&1.28&## Criticism: Translation problems (2)
ais24-War-Robots.md&1.28&
ais24-War-Robots.md&1.28&>- Arkin's concept, being based on a naive symbolic representation of world entities in the machine's data structures, does not even try to acknowledge this problem.
ais24-War-Robots.md&1.28&>- Bruno Latour (2009), Terry Winograd (1991): The utilisation of an artefact always involves a process of *translation.*
ais24-War-Robots.md&1.28&>- Winograd (1991): Crucial but often overlooked shifts in the meaning of words as they are translated from everyday, context-rich human language into an algorithmic, context-free, “blind” representation.
ais24-War-Robots.md&1.28&>- These translation processes do crucially alter the meaning of the words and concepts contained in the Laws of War and Rules of Engagement.
ais24-War-Robots.md&1.28&
ais24-War-Robots.md&1.29&## Criticism: Translation problems (3)
ais24-War-Robots.md&1.29&
ais24-War-Robots.md&1.29&>- But whereas these documents have been the object of public scrutiny and the result of public deliberation, their new, algorithmic form, and which is far from being a faithful translation, has been generated behind the closed doors of an industry laboratory, in a project which, most likely, will be classified as secret.
ais24-War-Robots.md&1.29&>- What reaches the public and its representatives will most likely be not the code itself, but advertising material promoting the machine in question and the features which its manufacturer wishes to highlight.
ais24-War-Robots.md&1.29&>- If the precise morally relevant rule content of a “governor”-like system is made available at all, it will most likely be in a back-translated form, *not as actual code,* but as Rules of Engagement or Laws of War, thus *hiding the very translation* which is the problem the public should be able to examine and to address.
ais24-War-Robots.md&1.29&
ais24-War-Robots.md&1.30&## Criticism: Translation problems (4)
ais24-War-Robots.md&1.30&
ais24-War-Robots.md&1.30&>- Whether the technology actually does what it purports to do depends upon its code (Lessig, 2006).
ais24-War-Robots.md&1.30&>- And if that code is closed, the moral values and decisions that it implements will be removed from public scrutiny and democratic control.
ais24-War-Robots.md&1.30&
ais24-War-Robots.md&1.31&## Criticism: Collective agency and goal translation (Bruno Latour)
ais24-War-Robots.md&1.31&
ais24-War-Robots.md&1.31&>- Latour: The use of an artefact by an agent changes the behaviour of *both* the agent and the artefact.
ais24-War-Robots.md&1.31&> - Not only the gun is operating according to the wishes of its user.
ais24-War-Robots.md&1.31&> - Rather, it is in equal measure the gun that forces the user to behave *as a gun user.*
ais24-War-Robots.md&1.31&>     - forces him to assume the right posture for firing the gun,
ais24-War-Robots.md&1.31&>     - to stop moving while firing,
ais24-War-Robots.md&1.31&>     - to aim using the aiming mechanism of the gun... and so on.
ais24-War-Robots.md&1.31&
ais24-War-Robots.md&1.32&## Bruno Latour: Goal translation (1)
ais24-War-Robots.md&1.32&
ais24-War-Robots.md&1.32&> - Even more importantly, having a gun to his disposal, will change the user's *goals* as well as the methods he considers in order to achieve these goals (avoid or confront a danger).
ais24-War-Robots.md&1.32&>- The "composite agent" composed of *myself and the gun* is thus a different agent, with different goals and methods at his disposal, than the original agent (me without the gun) had been.
ais24-War-Robots.md&1.32&>- This might be the translation of an originally aimed for goal into another, because the architecture and capabilities of the machine differ from that of a human operator and thus cause the “collective entity” of the human operator (or programmer) together with the machine to select a goal more appropriate to the new set of capabilities of that collective entity.
ais24-War-Robots.md&1.32&
ais24-War-Robots.md&1.33&## Bruno Latour: Goal translation (2)
ais24-War-Robots.md&1.33&
ais24-War-Robots.md&1.33&>- A good example might be that of a human soldier who might seek cover in case he is being shot at.
ais24-War-Robots.md&1.33&>- The machine, not fearing injury, would instead shoot back.
ais24-War-Robots.md&1.33&>- Assisted in targeting by the enemy fire itself, the machine would be able (and thus expected) to inflict lethal damage where the human soldier would perhaps seek to proceed more cautiously, to negotiate, to retreat, or to employ a whole array of possible other, non-lethal options.
ais24-War-Robots.md&1.33&
ais24-War-Robots.md&1.34&## Criticism: Is discrimination morally relevant? (1)
ais24-War-Robots.md&1.34&
ais24-War-Robots.md&1.34&>- Discrimination, in this context, means the ability of a machine which is engaged in a battle to distinguish reliably:
ais24-War-Robots.md&1.34&>     - between friend and foe; and
ais24-War-Robots.md&1.34&>     - between legitimate and illegitimate targets of violent military action.
ais24-War-Robots.md&1.34&>- Discrimination therefore, as a cognitive operation, is an instance of classification.
ais24-War-Robots.md&1.34&>- The persons perceived by the machine as present inside its radius of action are sorted into any number of categories, for example combatants, non-combatants, attackers, bystanders, dangerous, harmless and wounded persons. 
ais24-War-Robots.md&1.34&
ais24-War-Robots.md&1.35&## Criticism: Is discrimination morally relevant? (2)
ais24-War-Robots.md&1.35&
ais24-War-Robots.md&1.35&>- Although such classification is a *necessary* condition for moral action, morality cannot be said to be *identical* to making these categories; it presupposes them.
ais24-War-Robots.md&1.35&>- Predicates like “morally right” and “morally wrong” cannot be applied to persons or to classes of persons.
ais24-War-Robots.md&1.35&>- Discrimination is, like other instances of automated machine classification, a purely technical problem, which can be successfully solved by engineering means.
ais24-War-Robots.md&1.35&>- Concentration of attention to the issue of discrimination (that is solvable by engineering means) serves primarily to distract from the real moral issues (that may be not).
ais24-War-Robots.md&1.35&
ais24-War-Robots.md&1.36&## Criticism: Is discrimination morally relevant? (3)
ais24-War-Robots.md&1.36&
ais24-War-Robots.md&1.36&>- “The person standing beside the tree T is an enemy soldier” (an instance of discrimination) is a straightforward classification result, not a moral issue. 
ais24-War-Robots.md&1.36&>- Although all classification has already been successfully completed in this sentence (the machine knows that Y is an enemy solder), the moral problems are not yet resolved.
ais24-War-Robots.md&1.36&>- “It is morally right for robot X to kill the enemy soldier Y beside the tree T” is, in fact, a statement which includes a genuine moral evaluation.
ais24-War-Robots.md&1.36&>- This evaluation cannot be tackled with the engineering apparatus available for classification problems.
ais24-War-Robots.md&1.36&
ais24-War-Robots.md&1.37&## Criticism: Is discrimination morally relevant? (4)
ais24-War-Robots.md&1.37&
ais24-War-Robots.md&1.37&>- On the other hand, the question whether it is morally right or not for the machine to kill the enemy soldier Y requires insight
ais24-War-Robots.md&1.37&>     - into the nature of life and death,
ais24-War-Robots.md&1.37&>     - into the question whether enemy soldiers in the present conflict deserve death,
ais24-War-Robots.md&1.37&>     - and whether the machine is morally permitted to apply lethal force against this particular human.
ais24-War-Robots.md&1.37&
ais24-War-Robots.md&1.38&## Criticism: Is discrimination morally relevant? (5)
ais24-War-Robots.md&1.38&
ais24-War-Robots.md&1.38&>- This might involve a deliberation:
ais24-War-Robots.md&1.38&>     - about the aims of the present war,
ais24-War-Robots.md&1.38&>     - about the moral justification of lethal force in this conflict,
ais24-War-Robots.md&1.38&>     - about the moral rightness of the machine’s operator’s cause,
ais24-War-Robots.md&1.38&>     - and about available alternatives to the application of lethal force.
ais24-War-Robots.md&1.38&>- The ethical governor, as described by Arkin, does none of these.
ais24-War-Robots.md&1.38&
ais24-War-Robots.md&1.39&## Criticism: Feedback morality? (1)
ais24-War-Robots.md&1.39&
ais24-War-Robots.md&1.39&>- Negative feedback controllers (like the “ethical” governor) work under the following assumptions:
ais24-War-Robots.md&1.39&>     - There is a scalar value to control. The value can be accurately measured at any point in time.
ais24-War-Robots.md&1.39&>     - A numerical deviation between the desired (target) and the measured (actual) value of this variable can be calculated.
ais24-War-Robots.md&1.39&>     - This will be used in determining the direction (sign) and the strength of the corrective action.
ais24-War-Robots.md&1.39&>     - Application of the corrective action using a suitable direction and strength will bring the value that is controlled closer to the target value.
ais24-War-Robots.md&1.39&>     - This means that the effect of the corrective action must be predictable and it must be expressible as a decrease of the measured deviation.
ais24-War-Robots.md&1.39&>     - (A chaotic system, for example, cannot be regulated with a feedback controller.)
ais24-War-Robots.md&1.39&
ais24-War-Robots.md&1.40&## Criticism: Feedback morality? (2)
ais24-War-Robots.md&1.40&
ais24-War-Robots.md&1.40&>- Crucial question: Is morality a matter of correcting the deviation from a target value by applying a corrective action which is proportional in strength to the deviation?
ais24-War-Robots.md&1.40&>- If so, what is the target or reference value?
ais24-War-Robots.md&1.40&
ais24-War-Robots.md&1.41&## Criticism: Finding the reference value
ais24-War-Robots.md&1.41&
ais24-War-Robots.md&1.41&>- We cannot expect warring parties to share a common moral framework (Islamic vs Christian morality)
ais24-War-Robots.md&1.41&>- Arkin, confronted with this problem, attempts to substitute *other sets of rules* for the missing set of unambiguous and universally accepted morality.
ais24-War-Robots.md&1.41&>- Arkin: “... an autonomous robotic system architecture potentially capable of adhering to the International Laws of War (LOW) and Rules of Engagement (ROE) to ensure that these systems conform to the *legal* requirements and responsibilities of a civilized nation. [The ethical governor] is a transformer/suppressor of system-generated lethal action to ensure that it constitutes an *ethically permissible* action, either nonlethal or obligated ethical lethal force.” (Arkin et al., 2009, 1)
ais24-War-Robots.md&1.41&>- *Observe how Arkin switches from "legal" to "ethical"! Are these the same???*
ais24-War-Robots.md&1.41&
ais24-War-Robots.md&1.42&## Criticism: Problems with LOW/ROE (1)
ais24-War-Robots.md&1.42&
ais24-War-Robots.md&1.42&>- Rules of Engagement, being rules which are issued by an army for the use and benefit of its own soldiers, are not necessarily acceptable to *all* parties in an armed conflict. 
ais24-War-Robots.md&1.42&>- It is absurd to assume that rules which have been issued and are accepted only by one side in a conflict, should ensure ethical action in a way which is supposed to be acceptable to the opponent.
ais24-War-Robots.md&1.42&
ais24-War-Robots.md&1.43&## Criticism: Problems with LOW/ROE (2)
ais24-War-Robots.md&1.43&
ais24-War-Robots.md&1.43&>- If this were the case, it would imply that the US military is actively concerned to issue rules that counteract its own tactical interests in favour of moral principles which are compatible with the morality of the enemy.
ais24-War-Robots.md&1.43&>- The very aim of battlefield action, which is to score a victory over the enemy, is in direct conflict with this idea. 
ais24-War-Robots.md&1.43&>- What Arkin is advocating here is a kind of moral imperialism, based on the principle that the party which has the robots is therefore entitled to unilaterally prescribe the moral rules which come into play when these weapons are deployed.
ais24-War-Robots.md&1.43&
ais24-War-Robots.md&1.44&## Criticism: Problems with LOW/ROE (3)
ais24-War-Robots.md&1.44&
ais24-War-Robots.md&1.44&>- The Laws of War and Rules of Engagement as cited by Arkin are full of contradictions.
ais24-War-Robots.md&1.44&>- For example, “individual civilians, the civilian population as such and civilian objects are protected from intentional attack” (Arkin, 2007, 26), but a legitimate military target would be “enemy civilian aircraft when flying (i) within the jurisdiction of the enemy [...]” (Arkin, 2007, 25). 
ais24-War-Robots.md&1.44&>- If the “jurisdiction of the enemy” includes the area controlled by their air traffic control authorities, then it is hard to see how an “enemy civilian aircraft” could possibly avoid to be declared a legitimate target. 
ais24-War-Robots.md&1.44&>- Or: “In general, any place the enemy chooses to defend makes it subject to attack,” (Arkin, 2007, 24) including cities and any civilian installations.
ais24-War-Robots.md&1.44&
ais24-War-Robots.md&1.45&## Criticism: Problems with LOW/ROE (4)
ais24-War-Robots.md&1.45&
ais24-War-Robots.md&1.45&>- Standing Rules of Engagement: “[Necessity for military action arises] when a hostile act  occurs or when a force or terrorists exhibits hostile intent” (Arkin, 2007, 32). 
ais24-War-Robots.md&1.45&>- Observe how here the line between combatants and non-combatants is obscured by the use of the word “terrorists,” which is meant to legitimise attacks against non-uniformed and possibly unarmed persons, who, according to the international Laws of War, would be considered civilians and thus *not* legitimate targets.
ais24-War-Robots.md&1.45&>- Also, the criterion for a military action is lowered to the exhibition of “hostile intent,” leaving unspecified what is supposed to mean in particular and how the machine is supposed to go about identifying “hostile intent” by a non-uniformed (!) “terrorist” enemy.
ais24-War-Robots.md&1.45&
ais24-War-Robots.md&1.46&## Criticism: Problems with LOW/ROE (4)
ais24-War-Robots.md&1.46&
ais24-War-Robots.md&1.46&>- Even the most concrete and specific samples of rules which Arkin proposes to use are unclear, contradictory, and open to endless interpretation. 
ais24-War-Robots.md&1.46&>- This interpretation must be either performed by a human controlling the war robot (which would render the whole concept of the ethical governor obsolete), or by the machine itself.
ais24-War-Robots.md&1.46&>- This would require both unavailable factual knowledge (for example about the intentions of enemies) and powers of natural language disambiguation and practical wisdom that are currently not available in any machine.
ais24-War-Robots.md&1.46&
ais24-War-Robots.md&1.47&## Criticism: Dissent and conscience (1)
ais24-War-Robots.md&1.47&
ais24-War-Robots.md&1.47&>- But even following shared, generally accepted rule sets alone does not describe what we understand to be morality.
ais24-War-Robots.md&1.47&>- *What more is needed?*
ais24-War-Robots.md&1.47&
ais24-War-Robots.md&1.48&## Criticism: Dissent and conscience (2)
ais24-War-Robots.md&1.48&
ais24-War-Robots.md&1.48&>- Our common understanding of moral behaviour rests on two pillars:
ais24-War-Robots.md&1.48&>- 1. A shared set of moral rules, and
ais24-War-Robots.md&1.48&>- 2. Acting in accordance with one’s deepest conviction about what is right and wrong (“conscience,” or moral autonomy). 
ais24-War-Robots.md&1.48&>- If an agent is not free to act following his convictions, then we usually would not consider him a fully responsible moral agent.
ais24-War-Robots.md&1.48&>- If, for example, a soldier is ordered to perform a morally praiseworthy action, we would not ascribe the full amount of moral praise to the soldier himself, but to those who issued the command.
ais24-War-Robots.md&1.48&
ais24-War-Robots.md&1.49&## Criticism: Dissent and conscience (3)
ais24-War-Robots.md&1.49&
ais24-War-Robots.md&1.49&>- Perfect obedience is not moral action.
ais24-War-Robots.md&1.49&>- Moral action requires moral autonomy, or the capability for disobedience.
ais24-War-Robots.md&1.49&>- A machine controlled by a (fictional) ethical governor would be perfectly obedient.
ais24-War-Robots.md&1.49&>- Thus, it would not fulfill the criteria for moral action.
ais24-War-Robots.md&1.49&
ais24-War-Robots.md&1.50&## Criticism: Dissent and conscience (4)
ais24-War-Robots.md&1.50&
ais24-War-Robots.md&1.50&>- On the negative side, this also means that such a machine provides no safeguard against grossly immoral commands by the programmers or its superiors.
ais24-War-Robots.md&1.50&>- These superiors are able to override the ethical governor's suggestions (and we saw above that this is exactly what the governor allows them to do). 
ais24-War-Robots.md&1.50&>- With human soldiers there is always the possibility of dissent, of the soldier recognising the immorality of a command and refusing to act on it.
ais24-War-Robots.md&1.50&>- There are many stories, from all wars, where such acts were seen as morally highly praiseworthy.
ais24-War-Robots.md&1.50&>- With machines we have a guarantee of perfect obedience, which also means that immoral commands will be executed without any final moral deliberation in the form of a soldier's conscience coming into play.
ais24-War-Robots.md&1.50&
ais24-War-Robots.md&1.51&## Criticism: Dissent and conscience (5)
ais24-War-Robots.md&1.51&
ais24-War-Robots.md&1.51&>- Arkin (2007, 76): “On a related note, does a lethal autonomous agent have a right, even a responsibility, to refuse an unethical order? The answer is an unequivocal yes.”
ais24-War-Robots.md&1.51&>- Arkin (p.9): “I personally do not trust the view of setting aside the rules by the autonomous agent itself, as it begs the question of responsibility if it does so, but it may be possible for a human to assume responsibility for such deviation if it is ever deemed appropriate (and ethical) to do so.”
ais24-War-Robots.md&1.51&>- Unfortunately, these two statements contradict each other. 
ais24-War-Robots.md&1.51&
ais24-War-Robots.md&1.51&
ais24-War-Robots.md&1.52&## Summary: Is the ethical governor a good idea? (1)
ais24-War-Robots.md&1.52&
ais24-War-Robots.md&1.52&>- One could argue that even a limited moral control of war robot actions is better than none, and that, therefore, the ethical governor is still a useful and beneficial device.
ais24-War-Robots.md&1.52&>- But, first: As has been shown above, the ethical governor does not lead to the machine acting “ethically,” but only (in the idealised optimum of its performance) in accordance with the Geneva and Hague conventions and the Rules of Engagement of the deploying military system, and this only as long as these rules do not interfere with the machine's military objectives.
ais24-War-Robots.md&1.52&
ais24-War-Robots.md&1.53&## Summary: Is the ethical governor a good idea? (2)
ais24-War-Robots.md&1.53&
ais24-War-Robots.md&1.53&>- Second: The Rules of Engagement, being issued by the military itself, are not in any sense of the word “moral” rules, but rules made by one side in a conflict for its own benefit. 
ais24-War-Robots.md&1.53&>- They are often phrased in a way which leaves them open to extensive interpretation and makes them unsuitable as guidelines for moral action.
ais24-War-Robots.md&1.53&
ais24-War-Robots.md&1.54&## Summary: Is the ethical governor a good idea? (3)
ais24-War-Robots.md&1.54&
ais24-War-Robots.md&1.54&>- Third: The ethical governor, giving only suggestions, can be overridden at any time by the commanding officers in charge of the machine’s operation.
ais24-War-Robots.md&1.54&>- Since the ethical governor is designed and implemented by the same military hierarchy which deploys the robot, creates a fundamental conflict of interest.
ais24-War-Robots.md&1.54&>- In this conflict, the ethical governor will naturally always have a lower priority than the military objectives which motivated the creation and the deployment of the war robot in the first place.
ais24-War-Robots.md&1.54&
ais24-War-Robots.md&1.55&## Summary: Is the ethical governor a good idea? (4)
ais24-War-Robots.md&1.55&
ais24-War-Robots.md&1.55&>- Fourth: The ethical governor, being a “closed-code” implementation of moral principles, is removed from public scrutiny and democratic control. 
ais24-War-Robots.md&1.55&>- This problem can only be addressed by requiring the actual ethical governor code to be open-sourced, so that government and the public can be involved in the necessary and inevitable translation process of fuzzy, human terms into context-free, semantically unambiguous, computerised ones.
ais24-War-Robots.md&1.55&
ais24-War-Robots.md&1.56&## Summary: Is the ethical governor a good idea? (5)
ais24-War-Robots.md&1.56&
ais24-War-Robots.md&1.56&>- Fifth: The ethical governor is in principle only able to deal with a simple, conflict-free subset of rule-based ethics, since it lacks all mechanisms which are commonly assumed to be necessary for resolving moral rule conflicts:
ais24-War-Robots.md&1.56&>     - phronesis,
ais24-War-Robots.md&1.56&>     - moral intuition,
ais24-War-Robots.md&1.56&>     - or an understanding of human preferences and the utilitarian value of specific consequences.
ais24-War-Robots.md&1.56&>- But this “toy ethics” is not sufficient to resolve real-world moral problems on the battlefield, which typically involve conflicting options about questions of life and death, of justified causes, of retribution and retaliation, and of culture-specific ethics codes.
ais24-War-Robots.md&1.56&
ais24-War-Robots.md&1.57&## Summary: Is the ethical governor a good idea? (6)
ais24-War-Robots.md&1.57&
ais24-War-Robots.md&1.57&>- Sixth: An ethical governor lacks autonomy as a key ingredient of moral agency, and is thus incapable of dissent as a last line of protection against immoral robot deployment.
ais24-War-Robots.md&1.57&>- Despite all this, the ethical governor is promoted by its developers as a step towards the creation of autonomous, morally acting machines.
ais24-War-Robots.md&1.57&
ais24-War-Robots.md&1.58&## Summary: Is the ethical governor a good idea? (7)
ais24-War-Robots.md&1.58&
ais24-War-Robots.md&1.58&>- As a consequence of proposing the "ethical governor" and similar systems, it is increasingly accepted that humans move “out of the loop” of war robot control, based on the (mistaken) premise that moral behaviour can be implemented into the machine itself.
ais24-War-Robots.md&1.58&>- This leads to the public acceptance of increased deployment of autonomously acting war robots, which will, in reality, not be able to act morally right in any significant sense of the word.
ais24-War-Robots.md&1.58&>- The misconception about the capabilities and aims of the ethical governor could therefore be argued to be misleading and more dangerous than the absence of such a device (and the resulting placement of humans in morally critical places of control) would be.
ais24-War-Robots.md&1.58&
ais25-Singularity.md&0.1&---
ais25-Singularity.md&0.1&title:  "AI and Society: 25. The Singularity and its Dangers"
ais25-Singularity.md&0.1&author: Andreas Matthias, Lingnan University
ais25-Singularity.md&0.1&date: November 24, 2019
ais25-Singularity.md&0.1&...
ais25-Singularity.md&0.1&
ais25-Singularity.md&1.0&# Singularity: The Concept
ais25-Singularity.md&1.0&
ais25-Singularity.md&1.1&## Wikipedia
ais25-Singularity.md&1.1&
ais25-Singularity.md&1.1&> "The technological singularity is a hypothetical event in which an upgradable intelligent agent (such as a computer running software-based artificial general intelligence) enters a 'runaway reaction' of self-improvement cycles, with each new and more intelligent generation appearing more and more rapidly, causing an intelligence explosion and resulting in a powerful superintelligence whose cognitive abilities could be, qualitatively, *as far above humans' as human intelligence is above ape intelligence.*
ais25-Singularity.md&1.1&> More broadly, the term has historically been used for any form of accelerating or exponential technological progress hypothesised to result in a discontinuity, *beyond which events may become unpredictable or even unfathomable to human intelligence.*" (Wikipedia)
ais25-Singularity.md&1.1&
ais25-Singularity.md&1.2&## Two points
ais25-Singularity.md&1.2&
ais25-Singularity.md&1.2&>- "Superintelligence being as far above human intelligence as human intelligence is above ape intelligence."
ais25-Singularity.md&1.2&>     - Try to imagine what this means for our ability to understand and control this intelligence.
ais25-Singularity.md&1.2&>- "Events will become unpredictable or unfathomable to human intelligence."
ais25-Singularity.md&1.2&
ais25-Singularity.md&1.3&## Vernor Vinge
ais25-Singularity.md&1.3&
ais25-Singularity.md&1.3&> "Within thirty years, we will have the technological means to create superhuman intelligence. Shortly after, the human era will be ended." -- Vernor Vinge (1993): *The Coming Technological Singularity.*
ais25-Singularity.md&1.3&>- Vernor Steffen Vinge (1944--) is a retired San Diego State University (SDSU) Professor of Mathematics, computer scientist, and science fiction author.
ais25-Singularity.md&1.3&>- Best known for: A Fire Upon the Deep (1992), A Deepness in the Sky (1999) ... and his 1993 essay "The Coming Technological Singularity", in which he argues that the creation of superhuman artificial intelligence will mark the point at which "the human era will be ended", such that no current models of reality are sufficient to predict beyond it. (Wikipedia)
ais25-Singularity.md&1.3&
ais25-Singularity.md&1.4&## The original meaning
ais25-Singularity.md&1.4&
ais25-Singularity.md&1.4&>- Mathematical singularity, a point at which a given mathematical object is not defined or not "well-behaved", for example infinite or not differentiable.
ais25-Singularity.md&1.4&>- Gravitational singularity, a region in spacetime in which tidal gravitational forces become infinite.
ais25-Singularity.md&1.4&>- Initial singularity, the gravitational singularity of infinite density before quantum fluctuations that caused the Big Bang and subsequent inflation that created the Universe.
ais25-Singularity.md&1.4&>- Mechanical singularity, a position or configuration of a mechanism or a machine where the subsequent behavior cannot be predicted.
ais25-Singularity.md&1.4&
ais25-Singularity.md&1.5&## Connection to the original meaning
ais25-Singularity.md&1.5&
ais25-Singularity.md&1.5&> Vernor Vinge made an analogy between the breakdown in our ability to predict what would happen after the development of superintelligence and the breakdown of the predictive ability of modern physics at the space-time singularity beyond the event horizon of a black hole. (Wikipedia)
ais25-Singularity.md&1.5&
ais25-Singularity.md&1.6&## Kurzweil (1)
ais25-Singularity.md&1.6&
ais25-Singularity.md&1.6&Raymond "Ray" Kurzweil:
ais25-Singularity.md&1.6&
ais25-Singularity.md&1.6&>- American author, computer scientist, inventor and futurist.
ais25-Singularity.md&1.6&>- Involved in fields such as optical character recognition (OCR), text-to-speech synthesis, speech recognition technology, and electronic keyboard instruments.
ais25-Singularity.md&1.6&>- He has written books on health, artificial intelligence (AI), transhumanism, the technological singularity, and futurism.
ais25-Singularity.md&1.6&>- Kurzweil is a public advocate for the futurist and transhumanist movements, and gives public talks to share his optimistic outlook on life extension technologies and the future of nanotechnology, robotics, and biotechnology.
ais25-Singularity.md&1.6&
ais25-Singularity.md&1.7&## Kurzweil (2)
ais25-Singularity.md&1.7&
ais25-Singularity.md&1.7&>- Kurzweil was the principal inventor of the first flatbed scanner, the first omni-font optical character recognition, the first print-to-speech reading machine for the blind, the first commercial text-to-speech synthesizer, the Kurzweil K250 music synthesizer capable of simulating the sound of the grand piano and other orchestral instruments, and the first commercially marketed large-vocabulary speech recognition.
ais25-Singularity.md&1.7&>- In December 2012, Kurzweil was hired by Google in a full-time position to "work on new projects involving machine learning and language processing" and "to bring natural language understanding to Google".
ais25-Singularity.md&1.7&
ais25-Singularity.md&1.8&## Kurzweil (3)
ais25-Singularity.md&1.8&
ais25-Singularity.md&1.8&>- Kurzweil is trying to extend his life by taking 150-250 pills daily: "250 supplements, eight to 10 glasses of alkaline water and 10 cups of green tea" and drinking several glasses of red wine a week in an effort to "reprogram" his biochemistry.
ais25-Singularity.md&1.8&>- Kurzweil has joined the Alcor Life Extension Foundation, a cryonics company. In the event of his declared death, Kurzweil plans to be perfused with cryoprotectants, vitrified in liquid nitrogen, and stored at an Alcor facility in the hope that future medical technology will be able to repair his tissues and revive him.
ais25-Singularity.md&1.8&
ais25-Singularity.md&1.9&## Kurzweil's predictions (1)
ais25-Singularity.md&1.9&
ais25-Singularity.md&1.9&>- With radical life extension will come radical life enhancement.
ais25-Singularity.md&1.9&>- Within 10 years we will have the option to spend some of our time in 3D virtual environments that appear just as real as real reality, but these will not yet be made possible via direct interaction with our nervous system.
ais25-Singularity.md&1.9&>     - "If you look at video games and how we went from pong to the virtual reality we have available today, it is highly likely that immortality in essence will be possible."
ais25-Singularity.md&1.9&>- 20 to 25 years from now, we will have millions of blood-cell sized devices, known as nanobots, inside our bodies fighting against diseases, improving our memory, and cognitive abilities.
ais25-Singularity.md&1.9&>- A machine will pass the Turing test by 2029.
ais25-Singularity.md&1.9&
ais25-Singularity.md&1.10&## Kurzweil's predictions (2)
ais25-Singularity.md&1.10&
ais25-Singularity.md&1.10&>- Around 2045, "the pace of change will be so astonishingly quick that we won't be able to keep up, unless we enhance our own intelligence by merging with the intelligent machines we are creating".
ais25-Singularity.md&1.10&>- Shortly after, humans will be a hybrid of biological and non-biological intelligence that becomes increasingly dominated by its non-biological component.
ais25-Singularity.md&1.10&>- "AI is not an intelligent invasion from Mars. These are brain extenders that we have created to expand our own mental reach. They are part of our civilization. They are part of who we are. So over the next few decades our human-machine civilization will become increasingly dominated by its non-biological component."
ais25-Singularity.md&1.10&>- "We humans are going to start linking with each other and become a metaconnection we will all be connected and all be omnipresent, plugged into this global network that is connected to billions of people, and filled with data."
ais25-Singularity.md&1.10&
ais25-Singularity.md&1.11&## Bostrom
ais25-Singularity.md&1.11&
ais25-Singularity.md&1.11&
ais25-Singularity.md&1.12&## Chalmers
ais25-Singularity.md&1.12&
ais25-Singularity.md&1.12&
ais25-Singularity.md&1.13&## Reading
ais25-Singularity.md&1.13&
ais25-Singularity.md&1.13&A very good, very long (but fun!) introduction is here:
ais25-Singularity.md&1.13&
ais25-Singularity.md&1.13&Tim Urban: *The AI Revolution.*
ais25-Singularity.md&1.13&
ais25-Singularity.md&1.13&- <http://waitbutwhy.com/2015/01/artificial-intelligence-revolution-1.html> (first part)
ais25-Singularity.md&1.13&- <http://waitbutwhy.com/2015/01/artificial-intelligence-revolution-2.html> (second part)
ais25-Singularity.md&1.13&
ais25-Singularity.md&1.13&Parts of this lecture are taken from these two articles.
ais25-Singularity.md&1.13&
ais25-Singularity.md&2.0&# Reasons for a "singularity"
ais25-Singularity.md&2.0&
ais25-Singularity.md&2.1&## Accelerated development -- Logarithmic (1)
ais25-Singularity.md&2.1&
ais25-Singularity.md&2.1&From: http://www.singularity.com/charts/page17.html
ais25-Singularity.md&2.1&
ais25-Singularity.md&2.1&![](graphics/09-countdown.jpg)
ais25-Singularity.md&2.1&
ais25-Singularity.md&2.1&
ais25-Singularity.md&2.2&## Accelerated development -- Logarithmic (2)
ais25-Singularity.md&2.2&
ais25-Singularity.md&2.2&![](graphics/09-accelerated.png)
ais25-Singularity.md&2.2&
ais25-Singularity.md&2.3&## Accelerated development -- Linear (1)
ais25-Singularity.md&2.3&
ais25-Singularity.md&2.3&![](graphics/09-countdown-linear.jpg)
ais25-Singularity.md&2.3&
ais25-Singularity.md&2.4&## Moore's Law
ais25-Singularity.md&2.4&
ais25-Singularity.md&2.4&![](graphics/09-moores-law.jpg)
ais25-Singularity.md&2.4&
ais25-Singularity.md&2.5&## Exponential growth of computing power
ais25-Singularity.md&2.5&
ais25-Singularity.md&2.5&![](graphics/09-exponential-computing.jpg)
ais25-Singularity.md&2.5&
ais25-Singularity.md&2.6&## DNA sequencing cost
ais25-Singularity.md&2.6&
ais25-Singularity.md&2.6&![](graphics/09-dna.jpg)
ais25-Singularity.md&2.6&
ais25-Singularity.md&2.7&## Internet hosts (linear)
ais25-Singularity.md&2.7&
ais25-Singularity.md&2.7&![](graphics/09-internet.jpg)
ais25-Singularity.md&2.7&
ais25-Singularity.md&2.8&## Size of mechanical devices
ais25-Singularity.md&2.8&
ais25-Singularity.md&2.8&![](graphics/09-nanodevices.png)
ais25-Singularity.md&2.8&
ais25-Singularity.md&2.9&## Resolution of non-invasive brain scanning
ais25-Singularity.md&2.9&
ais25-Singularity.md&2.9&![](graphics/09-brainscan.jpg)
ais25-Singularity.md&2.9&
ais25-Singularity.md&2.9&
ais25-Singularity.md&2.10&## 1750 to 2015 (1)
ais25-Singularity.md&2.10&
ais25-Singularity.md&2.10&Tim Urban:
ais25-Singularity.md&2.10&
ais25-Singularity.md&2.10&>- Imagine taking a time machine back to 1750 -- a time when the world was in a permanent power outage, long-distance communication meant either yelling loudly or firing a cannon in the air, and all transportation ran on hay.
ais25-Singularity.md&2.10&>- When you get there, you retrieve a dude, bring him to 2015, and then walk him around and watch him react to everything.
ais25-Singularity.md&2.10&
ais25-Singularity.md&2.11&## 1750 to 2015 (2)
ais25-Singularity.md&2.11&
ais25-Singularity.md&2.11&>- It’s impossible for us to understand what it would be like for him to see shiny capsules racing by on a highway, talk to people who had been on the other side of the ocean earlier in the day, watch sports that were being played 1,000 miles away, hear a musical performance that happened 50 years ago, and play with my magical wizard rectangle that he could use to capture a real-life image or record a living moment, generate a map with a paranormal moving blue dot that shows him where he is, look at someone’s face and chat with them even though they’re on the other side of the country, and worlds of other inconceivable sorcery.
ais25-Singularity.md&2.11&>- This is all before you show him the internet or explain things like the International Space Station, the Large Hadron Collider, nuclear weapons, or general relativity.
ais25-Singularity.md&2.11&>- This experience for him wouldn’t be surprising or shocking or even mind-blowing -- those words aren’t big enough. He might actually die.
ais25-Singularity.md&2.11&
ais25-Singularity.md&2.12&## 1500-1750
ais25-Singularity.md&2.12&
ais25-Singularity.md&2.12&>- If *he* then went back to 1750 and got jealous that we got to see his reaction and decided he wanted to try the same thing, he’d take the time machine and go back the same distance, get someone from around the year 1500, bring him to 1750, and show him everything.
ais25-Singularity.md&2.12&>- And the 1500 guy would be shocked by a lot of things -- but he wouldn’t die.
ais25-Singularity.md&2.12&>- It would be far less of an insane experience for him, because while 1500 and 1750 were very different, they were much less different than 1750 to 2015.
ais25-Singularity.md&2.12&>- The 1500 guy would learn some mind-bending [stuff] about space and physics, he’d be impressed with how committed Europe turned out to be with that new imperialism fad, and he’d have to do some major revisions of his world map conception.
ais25-Singularity.md&2.12&
ais25-Singularity.md&2.13&## 1500-1750
ais25-Singularity.md&2.13&
ais25-Singularity.md&2.13&>- But watching everyday life go by in 1750 -- transportation, communication, etc. -- definitely wouldn’t make him die.
ais25-Singularity.md&2.13&>- No, in order for the 1750 guy to have as much fun as we had with him, he’d have to go much farther back -- maybe all the way back to about 12,000 BC, before the First Agricultural Revolution gave rise to the first cities and to the concept of civilization.
ais25-Singularity.md&2.13&
ais25-Singularity.md&2.14&## Law of Accelerating Returns (Tim Urban)
ais25-Singularity.md&2.14&
ais25-Singularity.md&2.14&>- This pattern -- human progress moving quicker and quicker as time goes on -- is what futurist Ray Kurzweil calls human history’s Law of Accelerating Returns.
ais25-Singularity.md&2.14&>- This happens because more advanced societies have the ability to progress at a faster *rate* than less advanced societies -- because they’re more advanced.
ais25-Singularity.md&2.14&>- 19th century humanity knew more and had better technology than 15th century humanity, so it’s no surprise that humanity made far more advances in the 19th century than in the 15th century -- 15th century humanity was no match for 19th century humanity.
ais25-Singularity.md&2.14&
ais25-Singularity.md&2.15&## Why we don't see the development (Tim Urban)
ais25-Singularity.md&2.15&
ais25-Singularity.md&2.15&1. We think in straight lines:
ais25-Singularity.md&2.15&
ais25-Singularity.md&2.15&![](graphics/09-lines.png)
ais25-Singularity.md&2.15&
ais25-Singularity.md&2.16&## Why we don't see the development (Tim Urban)
ais25-Singularity.md&2.16&
ais25-Singularity.md&2.16&2. Development happens in S-curves (Kurzweil):
ais25-Singularity.md&2.16&
ais25-Singularity.md&2.16&![](graphics/09-scurves.png)
ais25-Singularity.md&2.16&
ais25-Singularity.md&2.17&## S-Curves
ais25-Singularity.md&2.17&
ais25-Singularity.md&2.17&An S is created by the wave of progress when a new paradigm sweeps the world. The curve goes through three phases:
ais25-Singularity.md&2.17&
ais25-Singularity.md&2.17&1. Slow growth (the early phase of exponential growth)
ais25-Singularity.md&2.17&2. Rapid growth (the late, explosive phase of exponential growth)
ais25-Singularity.md&2.17&3. A leveling off as the particular paradigm matures
ais25-Singularity.md&2.17&
ais25-Singularity.md&2.17&Looking at developments within our lifetime, lets us think that progress is levelling off.
ais25-Singularity.md&2.17&
ais25-Singularity.md&3.0&# Types of AI
ais25-Singularity.md&3.0&
ais25-Singularity.md&3.1&## Artificial Narrow Intelligence (ANI)
ais25-Singularity.md&3.1&
ais25-Singularity.md&3.1&>- Artificial Narrow Intelligence is AI that specialises in one area.
ais25-Singularity.md&3.1&>- Tim Urban: "There’s AI that can beat the world chess champion in chess, but that’s the only thing it does. Ask it to figure out a better way to store data on a hard drive, and it’ll look at you blankly."
ais25-Singularity.md&3.1&
ais25-Singularity.md&3.2&## Artificial General Intelligence (AGI)
ais25-Singularity.md&3.2&
ais25-Singularity.md&3.2&>- Tim Urban: Sometimes referred to as (...) Human-Level AI, Artificial General Intelligence refers to a computer that is as smart as a human across the board -- a machine that can perform any intellectual task that a human being can.
ais25-Singularity.md&3.2&>- Creating AGI is a much harder task than creating ANI, and we’re yet to do it.
ais25-Singularity.md&3.2&>- Linda Gottfredson describes intelligence as “a very general mental capability that, among other things, involves the ability to reason, plan, solve problems, think abstractly, comprehend complex ideas, learn quickly, and learn from experience.”
ais25-Singularity.md&3.2&>- AGI would be able to do all of those things as easily as you can.
ais25-Singularity.md&3.2&
ais25-Singularity.md&3.3&## Artificial Superintelligence (ASI)
ais25-Singularity.md&3.3&
ais25-Singularity.md&3.3&>- Nick Bostrom: Superintelligence is “an intellect that is much smarter than the best human brains in practically every field, including scientific creativity, general wisdom and social skills.”
ais25-Singularity.md&3.3&
ais25-Singularity.md&4.0&# The Road Ahead
ais25-Singularity.md&4.0&
ais25-Singularity.md&4.1&## The current situation
ais25-Singularity.md&4.1&
ais25-Singularity.md&4.1&>- ANI systems in
ais25-Singularity.md&4.1&    - Cars
ais25-Singularity.md&4.1&	- Phones
ais25-Singularity.md&4.1&	- Email spam filters
ais25-Singularity.md&4.1&	- Google Translate
ais25-Singularity.md&4.1&	- Go playing software
ais25-Singularity.md&4.1&>- Aaron Saenz^[http://singularityhub.com/2010/08/10/we-live-in-a-jungle-of-artificial-intelligence-that-will-spawn-sentience/]: These early, isolated systems are the beginning of the road from ANI TO AGI: They “are like the amino acids in the early Earth’s primordial ooze” -- the inanimate substances that one day created life (unexpectedly).
ais25-Singularity.md&4.1&
ais25-Singularity.md&4.2&## From ANI to AGI
ais25-Singularity.md&4.2&
ais25-Singularity.md&4.2&The transition is difficult, because of ANI's:
ais25-Singularity.md&4.2&
ais25-Singularity.md&4.2&>- Lack of common sense.
ais25-Singularity.md&4.2&>- Lack of everyday world understanding.
ais25-Singularity.md&4.2&>- Lack of robust environment perception (understanding of visual images, stories etc).
ais25-Singularity.md&4.2&>- Differences to human perception (what matters to us, how we feel).
ais25-Singularity.md&4.2&
ais25-Singularity.md&4.2&. . . 
ais25-Singularity.md&4.2&
ais25-Singularity.md&4.2&Two things must happen for AGI to come about (Tim Urban):
ais25-Singularity.md&4.2&
ais25-Singularity.md&4.2&1. Increase of computing power.
ais25-Singularity.md&4.2&2. Increase in AI smartness
ais25-Singularity.md&4.2&
ais25-Singularity.md&4.3&## First key to AGI: Increase of computing power
ais25-Singularity.md&4.3&
ais25-Singularity.md&4.3&We talked about that above (Kurzweil, Moore's Law).
ais25-Singularity.md&4.3&
ais25-Singularity.md&4.4&## Second key to AGI: Increase in AI smartness
ais25-Singularity.md&4.4&
ais25-Singularity.md&4.4&How to achieve that? Tim Urban:
ais25-Singularity.md&4.4&
ais25-Singularity.md&4.4&>- Copy the way the brain works.
ais25-Singularity.md&4.4&>     - Just recently we have been able to emulate a 1mm-long flatworm brain, which consists of just 302 total neurons.
ais25-Singularity.md&4.4&>     - But: remember exponential growth in computing power.
ais25-Singularity.md&4.4&>- Use natural evolution principles (genetic algorithms).
ais25-Singularity.md&4.4&>     - Faster than natural evolution because: short generations, ability to select with foresight to maximise intelligence.
ais25-Singularity.md&4.4&>- Use computers to develop more advanced computers.
ais25-Singularity.md&4.4&>     - For example, AlphaGo trained itself to play better, without human intervention.
ais25-Singularity.md&4.4&
ais25-Singularity.md&4.5&## What logarithmic growth means
ais25-Singularity.md&4.5&
ais25-Singularity.md&4.5&<http://waitbutwhy.com/wp-content/uploads/2015/01/gif>
ais25-Singularity.md&4.5&
ais25-Singularity.md&4.6&## Advantages of AI systems over humans (hardware)
ais25-Singularity.md&4.6&
ais25-Singularity.md&4.6&Even if AI systems had only human capacity, they would still have advantages over humans (Tim Urban):
ais25-Singularity.md&4.6&
ais25-Singularity.md&4.6&>- Speed.
ais25-Singularity.md&4.6&>     - Brain's neurons: 200 Hz. Today’s microprocessors: 2 GHz, or 10 million times faster than our neurons.
ais25-Singularity.md&4.6&>     - The brain’s internal communications, which can move at about 120 m/s, are horribly outmatched by a computer’s ability to communicate optically at the speed of light.
ais25-Singularity.md&4.6&
ais25-Singularity.md&4.7&## Advantages of AI systems over humans (hardware)
ais25-Singularity.md&4.7&
ais25-Singularity.md&4.7&>- Size and storage.
ais25-Singularity.md&4.7&>     - The brain is locked into its size by the shape of our skulls.
ais25-Singularity.md&4.7&>     - It couldn’t get much bigger anyway, or the 120 m/s internal communications would take too long to get from one brain structure to another.
ais25-Singularity.md&4.7&>     - Computers can expand to any physical size, allowing far more hardware to be put to work, a much larger working memory (RAM), and a long-term memory (hard drive storage) that has both far greater capacity and precision than our own.
ais25-Singularity.md&4.7&
ais25-Singularity.md&4.8&## Advantages of AI systems over humans (hardware)
ais25-Singularity.md&4.8&
ais25-Singularity.md&4.8&>- Reliability and durability.
ais25-Singularity.md&4.8&>     - It’s not only the memories of a computer that would be more precise.
ais25-Singularity.md&4.8&>     - Computer transistors are more accurate than biological neurons.
ais25-Singularity.md&4.8&>     - They are less likely to deteriorate (and can be repaired or replaced if they do).
ais25-Singularity.md&4.8&>     - Human brains also get fatigued easily, while computers can run nonstop, at peak performance, 24/7
ais25-Singularity.md&4.8&
ais25-Singularity.md&4.9&## Advantages of AI systems over humans (software)
ais25-Singularity.md&4.9&
ais25-Singularity.md&4.9&>- Editability, upgradability, and a wider breadth of possibility.
ais25-Singularity.md&4.9&>     - Unlike the human brain, computer software can receive updates and fixes and can be easily experimented on.
ais25-Singularity.md&4.9&>     - The upgrades could also span to areas where human brains are weak.
ais25-Singularity.md&4.9&
ais25-Singularity.md&4.10&## Advantages of AI systems over humans (software)
ais25-Singularity.md&4.10&
ais25-Singularity.md&4.10&>- Collective capability.
ais25-Singularity.md&4.10&>     - Humans crush all other species at building a vast collective intelligence. Beginning with the development of language and the forming of large, dense communities, advancing through the inventions of writing and printing, and now intensified through tools like the internet, humanity’s collective intelligence is one of the major reasons we’ve been able to get so far ahead of all other species.
ais25-Singularity.md&4.10&>     - But computers will be way better at it than we are.
ais25-Singularity.md&4.10&>     - A worldwide network of AI running a particular program could regularly sync with itself so that anything any one computer learned would be instantly uploaded to all other computers.
ais25-Singularity.md&4.10&>     - The group could also take on one goal as a unit, because there wouldn’t necessarily be dissenting opinions and motivations and self-interest, like we have within the human population.
ais25-Singularity.md&4.10&
ais25-Singularity.md&4.11&## Recursive self-improvement
ais25-Singularity.md&4.11&
ais25-Singularity.md&4.11&> "An AI system at a certain level -- let’s say human village idiot -- is programmed with the goal of improving its own intelligence. Once it does, it’s smarter -- maybe at this point it’s at Einstein’s level -- so now when it works to improve its intelligence, with an Einstein-level intellect, it has an easier time and it can make bigger leaps. These leaps make it much smarter than any human, allowing it to make even bigger leaps. As the leaps grow larger and happen more rapidly, the AGI soars upwards in intelligence and soon reaches the superintelligent level of an ASI system. This is called an Intelligence Explosion, and it’s the ultimate example of The Law of Accelerating Returns." (Tim Urban)
ais25-Singularity.md&4.11&
ais25-Singularity.md&4.12&## How ASI will be different (1)
ais25-Singularity.md&4.12&
ais25-Singularity.md&4.12&>- Tim Urban: "Speed superintelligence" and "quality superintelligence."
ais25-Singularity.md&4.12&>- Often, someone’s first thought when they imagine a super-smart computer is one that’s as intelligent as a human but can think much, much faster.
ais25-Singularity.md&4.12&
ais25-Singularity.md&4.13&## How ASI will be different (2)
ais25-Singularity.md&4.13&
ais25-Singularity.md&4.13&>- But the true separator would be its advantage in intelligence *quality,* which is something completely different.
ais25-Singularity.md&4.13&>- What makes humans so much more intellectually capable than chimps isn’t a difference in thinking speed -- it’s that human brains contain a number of sophisticated cognitive modules that enable things like complex linguistic representations or long-term planning or abstract reasoning, that chimps’ brains do not.
ais25-Singularity.md&4.13&>- Speeding up a chimp’s brain by thousands of times wouldn’t bring him to our level -- even with a decade’s time, he wouldn’t be able to figure out how to use a set of custom tools to assemble an intricate model, something a human could knock out in a few hours.
ais25-Singularity.md&4.13&>- There are worlds of human cognitive function a chimp will simply never be capable of, no matter how much time he spends trying.
ais25-Singularity.md&4.13&
ais25-Singularity.md&5.0&# Immortality or extinction
ais25-Singularity.md&5.0&
ais25-Singularity.md&5.1&## Urban: The life-balance beam (1)
ais25-Singularity.md&5.1&
ais25-Singularity.md&5.1&![](graphics/09-life-balance-beam.jpg)
ais25-Singularity.md&5.1&
ais25-Singularity.md&5.2&## Urban: The life-balance beam (2)
ais25-Singularity.md&5.2&
ais25-Singularity.md&5.2&1. The advent of ASI will, for the first time, open up the possibility for a species to land on the immortality side of the balance beam.
ais25-Singularity.md&5.2&2. The advent of ASI will make such an unimaginably dramatic impact that it’s likely to knock the human race off the beam, in one direction or the other. 
ais25-Singularity.md&5.2&
ais25-Singularity.md&5.2&. . . 
ais25-Singularity.md&5.2&
ais25-Singularity.md&5.2&Question: *When are we going to hit the tripwire and which side of the beam will we land on when that happens?*
ais25-Singularity.md&5.2&
ais25-Singularity.md&5.3&## When will ASI arrive?
ais25-Singularity.md&5.3&
ais25-Singularity.md&5.3&>- Vincent C. Müller and Nick Bostrom (2013): Survey that asked hundreds of AI experts at a series of conferences the following question: “For the purposes of this question, assume that human scientific activity continues without major negative disruption. By what year would you see a (10% / 50% / 90%) probability for such HLMI (human-level machine intelligence) to exist?”
ais25-Singularity.md&5.3&>- Results (Tim Urban):
ais25-Singularity.md&5.3&>     - Median optimistic year (10% likelihood): 2022
ais25-Singularity.md&5.3&>     - Median realistic year (50% likelihood): 2040
ais25-Singularity.md&5.3&>     - Median pessimistic year (90% likelihood): 2075
ais25-Singularity.md&5.3&
ais25-Singularity.md&5.4&## When will ASI arrive?
ais25-Singularity.md&5.4&
ais25-Singularity.md&5.4&>- Urban: "So the median participant thinks it’s more likely than not that we’ll have AGI 25 years from now. The 90% median answer of 2075 means that if you’re a teenager right now, the median respondent, along with over half of the group of AI experts, is almost certain AGI will happen within your lifetime."
ais25-Singularity.md&5.4&
ais25-Singularity.md&5.5&## When will ASI arrive?
ais25-Singularity.md&5.5&
ais25-Singularity.md&5.5&A separate study (James Barrat) asked when participants thought AGI would be achieved -- by 2030, by 2050, by 2100, after 2100, or never. The results:
ais25-Singularity.md&5.5&
ais25-Singularity.md&5.5&- By 2030: 42% of respondents
ais25-Singularity.md&5.5&- By 2050: 25%
ais25-Singularity.md&5.5&- By 2100: 20%
ais25-Singularity.md&5.5&- After 2100: 10%
ais25-Singularity.md&5.5&- Never: 2%
ais25-Singularity.md&5.5&
ais25-Singularity.md&5.5&*Any comments?*
ais25-Singularity.md&5.5&
ais25-Singularity.md&5.5&. . .
ais25-Singularity.md&5.5&
ais25-Singularity.md&5.5&Interesting: note that only 2% said "never." But note also that these were people on conferences about AI!
ais25-Singularity.md&5.5&
ais25-Singularity.md&5.6&## Timeline (Tim Urban)
ais25-Singularity.md&5.6&
ais25-Singularity.md&5.6&![](graphics/09-timeline.png)\ 
ais25-Singularity.md&5.6&
ais25-Singularity.md&5.6&
ais25-Singularity.md&5.6&
ais25-Singularity.md&5.7&## Various camps in the debate 1 (Tim Urban)
ais25-Singularity.md&5.7&
ais25-Singularity.md&5.7&![](graphics/09-camps.jpg)\ 
ais25-Singularity.md&5.7&
ais25-Singularity.md&5.7&
ais25-Singularity.md&5.7&
ais25-Singularity.md&5.7&
ais25-Singularity.md&5.7&
ais25-Singularity.md&5.8&## Various camps in the debate 2 (Tim Urban)
ais25-Singularity.md&5.8&
ais25-Singularity.md&5.8&![](graphics/09-camps2.jpg)\ 
ais25-Singularity.md&5.8&
ais25-Singularity.md&5.8&
ais25-Singularity.md&5.8&
ais25-Singularity.md&5.8&
ais25-Singularity.md&5.9&## Bostrom's three ways of how an ASI could work (Tim Urban)
ais25-Singularity.md&5.9&
ais25-Singularity.md&5.9&>- As an **oracle**, which answers nearly any question posed to it with accuracy, including complex questions that humans cannot easily answer -- for example, "How can I manufacture a more efficient car engine?" Google is a primitive type of oracle.
ais25-Singularity.md&5.9&>- As a *genie*, which executes any high-level command it’s given: "Use a molecular assembler to build a new and more efficient kind of car engine."
ais25-Singularity.md&5.9&>- As a sovereign, which is assigned a broad and open-ended pursuit and allowed to operate in the world freely, making its own decisions about how best to proceed -- "Invent a faster, cheaper, and safer way than cars for humans to privately transport themselves."
ais25-Singularity.md&5.9&
ais25-Singularity.md&6.0&# Technology Considerations
ais25-Singularity.md&6.0&
ais25-Singularity.md&6.1&## Nanotechnology
ais25-Singularity.md&6.1&
ais25-Singularity.md&6.1&We already mentioned nanotechnology above.
ais25-Singularity.md&6.1&
ais25-Singularity.md&6.1&Tim Urban: "**Nanotechnology** is our word for technology that deals with the manipulation of matter that’s between 1 and 100 nanometers in size. A nanometer is a billionth of a meter, or a millionth of a millimeter, and this 1-100 range encompasses viruses (100 nm across), DNA (10 nm wide), and things as small as large molecules like hemoglobin (5 nm) and medium molecules like glucose (1 nm). If/when we conquer nanotechnology, the next step will be the ability to manipulate individual atoms, which are only one order of magnitude smaller (~.1 nm)."
ais25-Singularity.md&6.1&
ais25-Singularity.md&6.1&
ais25-Singularity.md&6.2&## Self-replicating nanobots
ais25-Singularity.md&6.2&
ais25-Singularity.md&6.2&Urban:
ais25-Singularity.md&6.2&
ais25-Singularity.md&6.2&>- A proposed method of nanoassembly involved the creation of trillions of tiny nanobots that would work in conjunction to build something.
ais25-Singularity.md&6.2&>- One way to create trillions of nanobots would be to make one that could self-replicate and then let the reproduction process turn that one into two, those two then turn into four, four into eight, and in about a day, there’d be a few trillion of them ready to go.
ais25-Singularity.md&6.2&
ais25-Singularity.md&6.3&## "Grey Goo"
ais25-Singularity.md&6.3&
ais25-Singularity.md&6.3&>- What can go wrong?
ais25-Singularity.md&6.3&>- If the system glitches, and instead of stopping replication once the total hits a few trillion as expected, they just keep replicating (Tim Urban):
ais25-Singularity.md&6.3&    - The nanobots would be designed to consume any carbon-based material in order to feed the replication process, and unpleasantly, all life is carbon-based.
ais25-Singularity.md&6.3&	- The Earth’s biomass contains about $10^{45}$ carbon atoms.
ais25-Singularity.md&6.3&	- A nanobot would consist of about $10^{6}$ carbon atoms, so $10^{39}$ nanobots would consume all life on Earth, which would happen in 130 replications, as oceans of nanobots (that’s the gray goo) rolled around the planet.
ais25-Singularity.md&6.3&	- Scientists think a nanobot could replicate in about 100 seconds, meaning this simple mistake would inconveniently end all life on Earth in 3.5 hours.
ais25-Singularity.md&6.3&>- Consider: What if terrorists get access to nanobots?
ais25-Singularity.md&6.3&
ais25-Singularity.md&6.4&## Conquering mortality (1)
ais25-Singularity.md&6.4&
ais25-Singularity.md&6.4&>- Mortality is evolutionary advantageous.
ais25-Singularity.md&6.4&>- There is no biological reason for humans to live more than about 40 years: 20 to reach sexual maturity, and 20 to raise one's offspring.
ais25-Singularity.md&6.4&>- Longer life biologically just means that additional individuals will consume environmental resources, without contributing as much as the younger ones.
ais25-Singularity.md&6.4&>- The genes of older individuals are more likely to be damaged, and their fertility is less.
ais25-Singularity.md&6.4&>- Therefore, dying at about 40 is a good thing (biologically).
ais25-Singularity.md&6.4&
ais25-Singularity.md&6.5&## Conquering mortality (2)
ais25-Singularity.md&6.5&
ais25-Singularity.md&6.5&Three ways to change that:
ais25-Singularity.md&6.5&
ais25-Singularity.md&6.5&>- Better living conditions, medicine, food, etc.
ais25-Singularity.md&6.5&    - We are doing that now. Doubling of life-span to 80 years, but not much further.
ais25-Singularity.md&6.5&>- Genetic manipulation (for example, telomere genetics).
ais25-Singularity.md&6.5&    - Might produce longer lifespans, but human tissue still deteriorates with age.
ais25-Singularity.md&6.5&>- Transfer of consciousness into a machine (Kurzweil).
ais25-Singularity.md&6.5&    - Would allow indefinite extension of mental life, instant transport, replication of individuals, etc.
ais25-Singularity.md&6.5&	- For some interesting philosophical questions, see Dennett: *Where am I?* <https://www.lehigh.edu/~mhb0/Dennett-WhereAmI.pdf>
ais25-Singularity.md&6.5&
ais25-Singularity.md&6.6&## Existential risk (Bostrom)
ais25-Singularity.md&6.6&
ais25-Singularity.md&6.6&![](graphics/09-bostrom-risk.jpg)\ 
ais25-Singularity.md&6.6&
ais25-Singularity.md&6.6&
ais25-Singularity.md&6.6&
ais25-Singularity.md&6.6&
ais25-Singularity.md&6.6&
ais25-Singularity.md&6.7&## Human extinction factors (Bostrom)
ais25-Singularity.md&6.7&
ais25-Singularity.md&6.7&>- Nature -- a large asteroid collision, an atmospheric shift that makes the air inhospitable to humans, a fatal virus or bacterial sickness that sweeps the world, etc.
ais25-Singularity.md&6.7&>- Aliens -- this is what Stephen Hawking, Carl Sagan, and so many other astronomers are scared of when they advise METI (Messaging Extraterrestrial Intelligence) to stop broadcasting outgoing signals. They don’t want us to be the Native Americans and let all the potential European conquerors know we’re here.
ais25-Singularity.md&6.7&>- Humans -- terrorists with their hands on a weapon that could cause extinction, a catastrophic global war, humans creating something smarter than themselves hastily without thinking about it carefully first.
ais25-Singularity.md&6.7&
ais25-Singularity.md&6.7&Tim Urban: "Bostrom points out that if #1 and #2 haven’t wiped us out so far in our first 100,000 years as a species, it’s unlikely to happen in the next century."
ais25-Singularity.md&6.7&
ais25-Singularity.md&6.7&
ais25-Singularity.md&6.8&## Controlling ASI
ais25-Singularity.md&6.8&
ais25-Singularity.md&6.8&"When ASI arrives, who or what will be in control of this vast new power, and what will their motivation be?" (Urban)
ais25-Singularity.md&6.8&
ais25-Singularity.md&6.9&## Malicious humans in control
ais25-Singularity.md&6.9&
ais25-Singularity.md&6.9&"A malicious human, group of humans, or government develops the first ASI and uses it to carry out their evil plans." (Urban)
ais25-Singularity.md&6.9&
ais25-Singularity.md&6.9&. . .
ais25-Singularity.md&6.9&
ais25-Singularity.md&6.9&>- North Korea, ISIS, Iran, the US government ...
ais25-Singularity.md&6.9&>- The problem for the creators of this AI would be that they too would be affected or destroyed by it.
ais25-Singularity.md&6.9&>- So it is unlikely that anyone would willingly create malicious AI that cannot be contained.
ais25-Singularity.md&6.9&
ais25-Singularity.md&6.10&## Malicious ASI in control
ais25-Singularity.md&6.10&
ais25-Singularity.md&6.10&"The plot of every AI movie. AI becomes as or more intelligent than humans, then decides to turn against us and take over." (Urban)
ais25-Singularity.md&6.10&
ais25-Singularity.md&6.10&. . . 
ais25-Singularity.md&6.10&
ais25-Singularity.md&6.10&>- The stuff of movies.
ais25-Singularity.md&6.10&>- Unlikely, because it's based on anthropomorphising AI.
ais25-Singularity.md&6.10&
ais25-Singularity.md&6.11&## The "Robotica" handwriting machine
ais25-Singularity.md&6.11&
ais25-Singularity.md&6.11&>- A small startup creates a robot 
ais25-Singularity.md&6.11&
ais25-Singularity.md&6.12&## What motivates an AI system?
ais25-Singularity.md&6.12&
ais25-Singularity.md&6.12&
ais25-Singularity.md&6.13&## The Fermi Paradox (1)
ais25-Singularity.md&6.13&
ais25-Singularity.md&6.13&
ais25-Singularity.md&6.14&## The Fermi Paradox (2)
ais25-Singularity.md&6.14&
ais25-Singularity.md&6.14&
ais25-Singularity.md&6.15&## The Fermi Paradox (3)
ais25-Singularity.md&6.15&
ais25-Singularity.md&6.15&
ais25-Singularity.md&6.15&
ais25-Singularity.md&6.16&## How an ASI would be superior to us
ais25-Singularity.md&6.16&
ais25-Singularity.md&6.16&- Intelligence amplification. The computer becomes great at making itself smarter, and bootstrapping its own intelligence.
ais25-Singularity.md&6.16&- Strategizing. The computer can strategically make, analyze, and prioritize long-term plans. It can also be clever and outwit beings of lower intelligence.
ais25-Singularity.md&6.16&- Social manipulation. The machine becomes great at persuasion.
ais25-Singularity.md&6.16&- Other skills like computer coding and hacking, technology research, and the ability to work the financial system to make money.
ais25-Singularity.md&6.16&
ais25-Singularity.md&6.17&## Decisive strategic advantage
ais25-Singularity.md&6.17&
ais25-Singularity.md&6.17&"Bostrom and many others also believe that the most likely scenario is that the very first computer to reach ASI will immediately see a strategic benefit to being the world’s only ASI system. And in the case of a fast takeoff, if it achieved ASI even just a few days before second place, it would be far enough ahead in intelligence to effectively and permanently suppress all competitors. Bostrom calls this a decisive strategic advantage, which would allow the world’s first ASI to become what’s called a singleton—an ASI that can rule the world at its whim forever, whether its whim is to lead us to immortality, wipe us from existence, or turn the universe into endless paperclips."
ais25-Singularity.md&6.17&
ais25-Singularity.md&6.18&## Singletons and safety
ais25-Singularity.md&6.18&
ais25-Singularity.md&6.18&"The singleton phenomenon can work in our favor or lead to our destruction. If the people thinking hardest about AI theory and human safety can come up with a fail-safe way to bring about Friendly ASI before any AI reaches human-level intelligence, the first ASI may turn out friendly.21 It could then use its decisive strategic advantage to secure singleton status and easily keep an eye on any potential Unfriendly AI being developed. We’d be in very good hands.
ais25-Singularity.md&6.18&
ais25-Singularity.md&6.18&But if things go the other way—if the global rush to develop AI reaches the ASI takeoff point before the science of how to ensure AI safety is developed, it’s very likely that an Unfriendly ASI like Turry emerges as the singleton and we’ll be treated to an existential catastrophe.
ais25-Singularity.md&6.18&
ais25-Singularity.md&6.18&As for where the winds are pulling, there’s a lot more money to be made funding innovative new AI technology than there is in funding AI safety research…
ais25-Singularity.md&6.18&
ais25-Singularity.md&6.18&This may be the most important race in human history. There’s a real chance we’re finishing up our reign as the King of Earth—and whether we head next to a blissful retirement or straight to the gallows still hangs in the balance."
ais25-Singularity.md&6.18&
ais25-Singularity.md&7.0&# Critics of Kurzweil and the Singularity
ais25-Singularity.md&7.0&
ais25-Singularity.md&7.1&## (Wikipedia:)
ais25-Singularity.md&7.1&
ais25-Singularity.md&7.1&Kurzweil's ideas have generated criticism within the scientific community and in the media.
ais25-Singularity.md&7.1&
ais25-Singularity.md&7.1&Although the idea of a technological singularity is a popular concept in science fiction, some authors such as Neal Stephenson[82] and Bruce Sterling have voiced skepticism about its real-world plausibility. Sterling expressed his views on the singularity scenario in a talk at the Long Now Foundation entitled The Singularity: Your Future as a Black Hole.[83][84] Other prominent AI thinkers and computer scientists such as Daniel Dennett,[85] Rodney Brooks,[86] David Gelernter[87] and Paul Allen[88] also criticized Kurzweil's projections.
ais25-Singularity.md&7.1&
ais25-Singularity.md&7.1&In the cover article of the December 2010 issue of IEEE Spectrum, John Rennie criticizes Kurzweil for several predictions that failed to become manifest by the originally predicted date. "Therein lie the frustrations of Kurzweil's brand of tech punditry. On close examination, his clearest and most successful predictions often lack originality or profundity. And most of his predictions come with so many loopholes that they border on the unfalsifiable."[89]
ais25-Singularity.md&7.1&
ais25-Singularity.md&7.1&Bill Joy, cofounder of Sun Microsystems, agrees with Kurzweil's timeline of future progress, but thinks that technologies such as AI, nanotechnology and advanced biotechnology will create a dystopian world.[90] Mitch Kapor, the founder of Lotus Development Corporation, has called the notion of a technological singularity "intelligent design for the IQ 140 people...This proposition that we're heading to this point at which everything is going to be just unimaginably different—it's fundamentally, in my view, driven by a religious impulse. And all of the frantic arm-waving can't obscure that fact for me."[25]
ais25-Singularity.md&7.1&
ais25-Singularity.md&7.1&Some critics have argued more strongly against Kurzweil and his ideas. Cognitive scientist Douglas Hofstadter has said of Kurzweil's and Hans Moravec's books: "It's an intimate mixture of rubbish and good ideas, and it's very hard to disentangle the two, because these are smart people; they're not stupid."[91] Biologist P. Z. Myers has criticized Kurzweil's predictions as being based on "New Age spiritualism" rather than science and says that Kurzweil does not understand basic biology.[92][93] VR pioneer Jaron Lanier has even described Kurzweil's ideas as "cybernetic totalism" and has outlined his views on the culture surrounding Kurzweil's predictions in an essay for Edge.org entitled One Half of a Manifesto.[42][94]
ais25-Singularity.md&7.1&
ais25-Singularity.md&7.1&British philosopher John Gray argues that contemporary science is what magic was for ancient civilizations. It gives a sense of hope for those who are willing to do almost anything in order to achieve eternal life. He quotes Kurzweil's Singularity as another example of a trend which has almost always been present in the history of mankind.[95]
ais25-Singularity.md&7.1&
ais25-Singularity.md&7.1&
ais25-Singularity.md&7.2&## Chalmers' criticism
ais25-Singularity.md&7.2&
ais25-Singularity.md&7.2&
ais25-Singularity.md&7.2&
ais25-Singularity.md&7.2&
ais25-Singularity.md&7.2&
ais25-Singularity.md&7.2&
ais25-Singularity.md&7.2&
