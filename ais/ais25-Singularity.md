---
title:  "AI and Society: 25. The Singularity and its Dangers"
author: Andreas Matthias, Lingnan University
date: November 24, 2019
...

# Singularity: The Concept

## Wikipedia

> "The technological singularity is a hypothetical event in which an upgradable intelligent agent (such as a computer running software-based artificial general intelligence) enters a 'runaway reaction' of self-improvement cycles, with each new and more intelligent generation appearing more and more rapidly, causing an intelligence explosion and resulting in a powerful superintelligence whose cognitive abilities could be, qualitatively, *as far above humans' as human intelligence is above ape intelligence.*
> More broadly, the term has historically been used for any form of accelerating or exponential technological progress hypothesised to result in a discontinuity, *beyond which events may become unpredictable or even unfathomable to human intelligence.*" (Wikipedia)

## Two points

>- "Superintelligence being as far above human intelligence as human intelligence is above ape intelligence."
>     - Try to imagine what this means for our ability to understand and control this intelligence.
>- "Events will become unpredictable or unfathomable to human intelligence."

## Vernor Vinge

> "Within thirty years, we will have the technological means to create superhuman intelligence. Shortly after, the human era will be ended." -- Vernor Vinge (1993): *The Coming Technological Singularity.*
>- Vernor Steffen Vinge (1944--) is a retired San Diego State University (SDSU) Professor of Mathematics, computer scientist, and science fiction author.
>- Best known for: A Fire Upon the Deep (1992), A Deepness in the Sky (1999) ... and his 1993 essay "The Coming Technological Singularity", in which he argues that the creation of superhuman artificial intelligence will mark the point at which "the human era will be ended", such that no current models of reality are sufficient to predict beyond it. (Wikipedia)

## The original meaning

>- Mathematical singularity, a point at which a given mathematical object is not defined or not "well-behaved", for example infinite or not differentiable.
>- Gravitational singularity, a region in spacetime in which tidal gravitational forces become infinite.
>- Initial singularity, the gravitational singularity of infinite density before quantum fluctuations that caused the Big Bang and subsequent inflation that created the Universe.
>- Mechanical singularity, a position or configuration of a mechanism or a machine where the subsequent behavior cannot be predicted.

## Connection to the original meaning

> Vernor Vinge made an analogy between the breakdown in our ability to predict what would happen after the development of superintelligence and the breakdown of the predictive ability of modern physics at the space-time singularity beyond the event horizon of a black hole. (Wikipedia)

## Kurzweil (1)

Raymond "Ray" Kurzweil:

>- American author, computer scientist, inventor and futurist.
>- Involved in fields such as optical character recognition (OCR), text-to-speech synthesis, speech recognition technology, and electronic keyboard instruments.
>- He has written books on health, artificial intelligence (AI), transhumanism, the technological singularity, and futurism.
>- Kurzweil is a public advocate for the futurist and transhumanist movements, and gives public talks to share his optimistic outlook on life extension technologies and the future of nanotechnology, robotics, and biotechnology.

## Kurzweil (2)

>- Kurzweil was the principal inventor of the first flatbed scanner, the first omni-font optical character recognition, the first print-to-speech reading machine for the blind, the first commercial text-to-speech synthesizer, the Kurzweil K250 music synthesizer capable of simulating the sound of the grand piano and other orchestral instruments, and the first commercially marketed large-vocabulary speech recognition.
>- In December 2012, Kurzweil was hired by Google in a full-time position to "work on new projects involving machine learning and language processing" and "to bring natural language understanding to Google".

## Kurzweil (3)

>- Kurzweil is trying to extend his life by taking 150-250 pills daily: "250 supplements, eight to 10 glasses of alkaline water and 10 cups of green tea" and drinking several glasses of red wine a week in an effort to "reprogram" his biochemistry.
>- Kurzweil has joined the Alcor Life Extension Foundation, a cryonics company. In the event of his declared death, Kurzweil plans to be perfused with cryoprotectants, vitrified in liquid nitrogen, and stored at an Alcor facility in the hope that future medical technology will be able to repair his tissues and revive him.

## Kurzweil's predictions (1)

>- With radical life extension will come radical life enhancement.
>- Within 10 years we will have the option to spend some of our time in 3D virtual environments that appear just as real as real reality, but these will not yet be made possible via direct interaction with our nervous system.
>     - "If you look at video games and how we went from pong to the virtual reality we have available today, it is highly likely that immortality in essence will be possible."
>- 20 to 25 years from now, we will have millions of blood-cell sized devices, known as nanobots, inside our bodies fighting against diseases, improving our memory, and cognitive abilities.
>- A machine will pass the Turing test by 2029.

## Kurzweil's predictions (2)

>- Around 2045, "the pace of change will be so astonishingly quick that we won't be able to keep up, unless we enhance our own intelligence by merging with the intelligent machines we are creating".
>- Shortly after, humans will be a hybrid of biological and non-biological intelligence that becomes increasingly dominated by its non-biological component.
>- "AI is not an intelligent invasion from Mars. These are brain extenders that we have created to expand our own mental reach. They are part of our civilization. They are part of who we are. So over the next few decades our human-machine civilization will become increasingly dominated by its non-biological component."
>- "We humans are going to start linking with each other and become a metaconnection we will all be connected and all be omnipresent, plugged into this global network that is connected to billions of people, and filled with data."

## Bostrom


## Chalmers


## Reading

A very good, very long (but fun!) introduction is here:

Tim Urban: *The AI Revolution.*

- <http://waitbutwhy.com/2015/01/artificial-intelligence-revolution-1.html> (first part)
- <http://waitbutwhy.com/2015/01/artificial-intelligence-revolution-2.html> (second part)

Parts of this lecture are taken from these two articles.

# Reasons for a "singularity"

## Accelerated development -- Logarithmic (1)

From: http://www.singularity.com/charts/page17.html

![](graphics/09-countdown.jpg)


## Accelerated development -- Logarithmic (2)

![](graphics/09-accelerated.png)

## Accelerated development -- Linear (1)

![](graphics/09-countdown-linear.jpg)

## Moore's Law

![](graphics/09-moores-law.jpg)

## Exponential growth of computing power

![](graphics/09-exponential-computing.jpg)

## DNA sequencing cost

![](graphics/09-dna.jpg)

## Internet hosts (linear)

![](graphics/09-internet.jpg)

## Size of mechanical devices

![](graphics/09-nanodevices.png)

## Resolution of non-invasive brain scanning

![](graphics/09-brainscan.jpg)


## 1750 to 2015 (1)

Tim Urban:

>- Imagine taking a time machine back to 1750 -- a time when the world was in a permanent power outage, long-distance communication meant either yelling loudly or firing a cannon in the air, and all transportation ran on hay.
>- When you get there, you retrieve a dude, bring him to 2015, and then walk him around and watch him react to everything.

## 1750 to 2015 (2)

>- It’s impossible for us to understand what it would be like for him to see shiny capsules racing by on a highway, talk to people who had been on the other side of the ocean earlier in the day, watch sports that were being played 1,000 miles away, hear a musical performance that happened 50 years ago, and play with my magical wizard rectangle that he could use to capture a real-life image or record a living moment, generate a map with a paranormal moving blue dot that shows him where he is, look at someone’s face and chat with them even though they’re on the other side of the country, and worlds of other inconceivable sorcery.
>- This is all before you show him the internet or explain things like the International Space Station, the Large Hadron Collider, nuclear weapons, or general relativity.
>- This experience for him wouldn’t be surprising or shocking or even mind-blowing -- those words aren’t big enough. He might actually die.

## 1500-1750

>- If *he* then went back to 1750 and got jealous that we got to see his reaction and decided he wanted to try the same thing, he’d take the time machine and go back the same distance, get someone from around the year 1500, bring him to 1750, and show him everything.
>- And the 1500 guy would be shocked by a lot of things -- but he wouldn’t die.
>- It would be far less of an insane experience for him, because while 1500 and 1750 were very different, they were much less different than 1750 to 2015.
>- The 1500 guy would learn some mind-bending [stuff] about space and physics, he’d be impressed with how committed Europe turned out to be with that new imperialism fad, and he’d have to do some major revisions of his world map conception.

## 1500-1750

>- But watching everyday life go by in 1750 -- transportation, communication, etc. -- definitely wouldn’t make him die.
>- No, in order for the 1750 guy to have as much fun as we had with him, he’d have to go much farther back -- maybe all the way back to about 12,000 BC, before the First Agricultural Revolution gave rise to the first cities and to the concept of civilization.

## Law of Accelerating Returns (Tim Urban)

>- This pattern -- human progress moving quicker and quicker as time goes on -- is what futurist Ray Kurzweil calls human history’s Law of Accelerating Returns.
>- This happens because more advanced societies have the ability to progress at a faster *rate* than less advanced societies -- because they’re more advanced.
>- 19th century humanity knew more and had better technology than 15th century humanity, so it’s no surprise that humanity made far more advances in the 19th century than in the 15th century -- 15th century humanity was no match for 19th century humanity.

## Why we don't see the development (Tim Urban)

1. We think in straight lines:

![](graphics/09-lines.png)

## Why we don't see the development (Tim Urban)

2. Development happens in S-curves (Kurzweil):

![](graphics/09-scurves.png)

## S-Curves

An S is created by the wave of progress when a new paradigm sweeps the world. The curve goes through three phases:

1. Slow growth (the early phase of exponential growth)
2. Rapid growth (the late, explosive phase of exponential growth)
3. A leveling off as the particular paradigm matures

Looking at developments within our lifetime, lets us think that progress is levelling off.

# Types of AI

## Artificial Narrow Intelligence (ANI)

>- Artificial Narrow Intelligence is AI that specialises in one area.
>- Tim Urban: "There’s AI that can beat the world chess champion in chess, but that’s the only thing it does. Ask it to figure out a better way to store data on a hard drive, and it’ll look at you blankly."

## Artificial General Intelligence (AGI)

>- Tim Urban: Sometimes referred to as (...) Human-Level AI, Artificial General Intelligence refers to a computer that is as smart as a human across the board -- a machine that can perform any intellectual task that a human being can.
>- Creating AGI is a much harder task than creating ANI, and we’re yet to do it.
>- Linda Gottfredson describes intelligence as “a very general mental capability that, among other things, involves the ability to reason, plan, solve problems, think abstractly, comprehend complex ideas, learn quickly, and learn from experience.”
>- AGI would be able to do all of those things as easily as you can.

## Artificial Superintelligence (ASI)

>- Nick Bostrom: Superintelligence is “an intellect that is much smarter than the best human brains in practically every field, including scientific creativity, general wisdom and social skills.”

# The Road Ahead

## The current situation

>- ANI systems in
    - Cars
	- Phones
	- Email spam filters
	- Google Translate
	- Go playing software
>- Aaron Saenz^[http://singularityhub.com/2010/08/10/we-live-in-a-jungle-of-artificial-intelligence-that-will-spawn-sentience/]: These early, isolated systems are the beginning of the road from ANI TO AGI: They “are like the amino acids in the early Earth’s primordial ooze” -- the inanimate substances that one day created life (unexpectedly).

## From ANI to AGI

The transition is difficult, because of ANI's:

>- Lack of common sense.
>- Lack of everyday world understanding.
>- Lack of robust environment perception (understanding of visual images, stories etc).
>- Differences to human perception (what matters to us, how we feel).

. . . 

Two things must happen for AGI to come about (Tim Urban):

1. Increase of computing power.
2. Increase in AI smartness

## First key to AGI: Increase of computing power

We talked about that above (Kurzweil, Moore's Law).

## Second key to AGI: Increase in AI smartness

How to achieve that? Tim Urban:

>- Copy the way the brain works.
>     - Just recently we have been able to emulate a 1mm-long flatworm brain, which consists of just 302 total neurons.
>     - But: remember exponential growth in computing power.
>- Use natural evolution principles (genetic algorithms).
>     - Faster than natural evolution because: short generations, ability to select with foresight to maximise intelligence.
>- Use computers to develop more advanced computers.
>     - For example, AlphaGo trained itself to play better, without human intervention.

## What logarithmic growth means

<http://waitbutwhy.com/wp-content/uploads/2015/01/gif>

## Advantages of AI systems over humans (hardware)

Even if AI systems had only human capacity, they would still have advantages over humans (Tim Urban):

>- Speed.
>     - Brain's neurons: 200 Hz. Today’s microprocessors: 2 GHz, or 10 million times faster than our neurons.
>     - The brain’s internal communications, which can move at about 120 m/s, are horribly outmatched by a computer’s ability to communicate optically at the speed of light.

## Advantages of AI systems over humans (hardware)

>- Size and storage.
>     - The brain is locked into its size by the shape of our skulls.
>     - It couldn’t get much bigger anyway, or the 120 m/s internal communications would take too long to get from one brain structure to another.
>     - Computers can expand to any physical size, allowing far more hardware to be put to work, a much larger working memory (RAM), and a long-term memory (hard drive storage) that has both far greater capacity and precision than our own.

## Advantages of AI systems over humans (hardware)

>- Reliability and durability.
>     - It’s not only the memories of a computer that would be more precise.
>     - Computer transistors are more accurate than biological neurons.
>     - They are less likely to deteriorate (and can be repaired or replaced if they do).
>     - Human brains also get fatigued easily, while computers can run nonstop, at peak performance, 24/7

## Advantages of AI systems over humans (software)

>- Editability, upgradability, and a wider breadth of possibility.
>     - Unlike the human brain, computer software can receive updates and fixes and can be easily experimented on.
>     - The upgrades could also span to areas where human brains are weak.

## Advantages of AI systems over humans (software)

>- Collective capability.
>     - Humans crush all other species at building a vast collective intelligence. Beginning with the development of language and the forming of large, dense communities, advancing through the inventions of writing and printing, and now intensified through tools like the internet, humanity’s collective intelligence is one of the major reasons we’ve been able to get so far ahead of all other species.
>     - But computers will be way better at it than we are.
>     - A worldwide network of AI running a particular program could regularly sync with itself so that anything any one computer learned would be instantly uploaded to all other computers.
>     - The group could also take on one goal as a unit, because there wouldn’t necessarily be dissenting opinions and motivations and self-interest, like we have within the human population.

## Recursive self-improvement

> "An AI system at a certain level -- let’s say human village idiot -- is programmed with the goal of improving its own intelligence. Once it does, it’s smarter -- maybe at this point it’s at Einstein’s level -- so now when it works to improve its intelligence, with an Einstein-level intellect, it has an easier time and it can make bigger leaps. These leaps make it much smarter than any human, allowing it to make even bigger leaps. As the leaps grow larger and happen more rapidly, the AGI soars upwards in intelligence and soon reaches the superintelligent level of an ASI system. This is called an Intelligence Explosion, and it’s the ultimate example of The Law of Accelerating Returns." (Tim Urban)

## How ASI will be different (1)

>- Tim Urban: "Speed superintelligence" and "quality superintelligence."
>- Often, someone’s first thought when they imagine a super-smart computer is one that’s as intelligent as a human but can think much, much faster.

## How ASI will be different (2)

>- But the true separator would be its advantage in intelligence *quality,* which is something completely different.
>- What makes humans so much more intellectually capable than chimps isn’t a difference in thinking speed -- it’s that human brains contain a number of sophisticated cognitive modules that enable things like complex linguistic representations or long-term planning or abstract reasoning, that chimps’ brains do not.
>- Speeding up a chimp’s brain by thousands of times wouldn’t bring him to our level -- even with a decade’s time, he wouldn’t be able to figure out how to use a set of custom tools to assemble an intricate model, something a human could knock out in a few hours.
>- There are worlds of human cognitive function a chimp will simply never be capable of, no matter how much time he spends trying.

# Immortality or extinction

## Urban: The life-balance beam (1)

![](graphics/09-life-balance-beam.jpg)

## Urban: The life-balance beam (2)

1. The advent of ASI will, for the first time, open up the possibility for a species to land on the immortality side of the balance beam.
2. The advent of ASI will make such an unimaginably dramatic impact that it’s likely to knock the human race off the beam, in one direction or the other. 

. . . 

Question: *When are we going to hit the tripwire and which side of the beam will we land on when that happens?*

## When will ASI arrive?

>- Vincent C. Müller and Nick Bostrom (2013): Survey that asked hundreds of AI experts at a series of conferences the following question: “For the purposes of this question, assume that human scientific activity continues without major negative disruption. By what year would you see a (10% / 50% / 90%) probability for such HLMI (human-level machine intelligence) to exist?”
>- Results (Tim Urban):
>     - Median optimistic year (10% likelihood): 2022
>     - Median realistic year (50% likelihood): 2040
>     - Median pessimistic year (90% likelihood): 2075

## When will ASI arrive?

>- Urban: "So the median participant thinks it’s more likely than not that we’ll have AGI 25 years from now. The 90% median answer of 2075 means that if you’re a teenager right now, the median respondent, along with over half of the group of AI experts, is almost certain AGI will happen within your lifetime."

## When will ASI arrive?

A separate study (James Barrat) asked when participants thought AGI would be achieved -- by 2030, by 2050, by 2100, after 2100, or never. The results:

- By 2030: 42% of respondents
- By 2050: 25%
- By 2100: 20%
- After 2100: 10%
- Never: 2%

*Any comments?*

. . .

Interesting: note that only 2% said "never." But note also that these were people on conferences about AI!

## Timeline (Tim Urban)

![](graphics/09-timeline.png)\ 



## Various camps in the debate 1 (Tim Urban)

![](graphics/09-camps.jpg)\ 





## Various camps in the debate 2 (Tim Urban)

![](graphics/09-camps2.jpg)\ 




## Bostrom's three ways of how an ASI could work (Tim Urban)

>- As an **oracle**, which answers nearly any question posed to it with accuracy, including complex questions that humans cannot easily answer -- for example, "How can I manufacture a more efficient car engine?" Google is a primitive type of oracle.
>- As a *genie*, which executes any high-level command it’s given: "Use a molecular assembler to build a new and more efficient kind of car engine."
>- As a sovereign, which is assigned a broad and open-ended pursuit and allowed to operate in the world freely, making its own decisions about how best to proceed -- "Invent a faster, cheaper, and safer way than cars for humans to privately transport themselves."

# Technology Considerations

## Nanotechnology

We already mentioned nanotechnology above.

Tim Urban: "**Nanotechnology** is our word for technology that deals with the manipulation of matter that’s between 1 and 100 nanometers in size. A nanometer is a billionth of a meter, or a millionth of a millimeter, and this 1-100 range encompasses viruses (100 nm across), DNA (10 nm wide), and things as small as large molecules like hemoglobin (5 nm) and medium molecules like glucose (1 nm). If/when we conquer nanotechnology, the next step will be the ability to manipulate individual atoms, which are only one order of magnitude smaller (~.1 nm)."


## Self-replicating nanobots

Urban:

>- A proposed method of nanoassembly involved the creation of trillions of tiny nanobots that would work in conjunction to build something.
>- One way to create trillions of nanobots would be to make one that could self-replicate and then let the reproduction process turn that one into two, those two then turn into four, four into eight, and in about a day, there’d be a few trillion of them ready to go.

## "Grey Goo"

>- What can go wrong?
>- If the system glitches, and instead of stopping replication once the total hits a few trillion as expected, they just keep replicating (Tim Urban):
    - The nanobots would be designed to consume any carbon-based material in order to feed the replication process, and unpleasantly, all life is carbon-based.
	- The Earth’s biomass contains about $10^{45}$ carbon atoms.
	- A nanobot would consist of about $10^{6}$ carbon atoms, so $10^{39}$ nanobots would consume all life on Earth, which would happen in 130 replications, as oceans of nanobots (that’s the gray goo) rolled around the planet.
	- Scientists think a nanobot could replicate in about 100 seconds, meaning this simple mistake would inconveniently end all life on Earth in 3.5 hours.
>- Consider: What if terrorists get access to nanobots?

## Conquering mortality (1)

>- Mortality is evolutionary advantageous.
>- There is no biological reason for humans to live more than about 40 years: 20 to reach sexual maturity, and 20 to raise one's offspring.
>- Longer life biologically just means that additional individuals will consume environmental resources, without contributing as much as the younger ones.
>- The genes of older individuals are more likely to be damaged, and their fertility is less.
>- Therefore, dying at about 40 is a good thing (biologically).

## Conquering mortality (2)

Three ways to change that:

>- Better living conditions, medicine, food, etc.
    - We are doing that now. Doubling of life-span to 80 years, but not much further.
>- Genetic manipulation (for example, telomere genetics).
    - Might produce longer lifespans, but human tissue still deteriorates with age.
>- Transfer of consciousness into a machine (Kurzweil).
    - Would allow indefinite extension of mental life, instant transport, replication of individuals, etc.
	- For some interesting philosophical questions, see Dennett: *Where am I?* <https://www.lehigh.edu/~mhb0/Dennett-WhereAmI.pdf>

## Existential risk (Bostrom)

![](graphics/09-bostrom-risk.jpg)\ 





## Human extinction factors (Bostrom)

>- Nature -- a large asteroid collision, an atmospheric shift that makes the air inhospitable to humans, a fatal virus or bacterial sickness that sweeps the world, etc.
>- Aliens -- this is what Stephen Hawking, Carl Sagan, and so many other astronomers are scared of when they advise METI (Messaging Extraterrestrial Intelligence) to stop broadcasting outgoing signals. They don’t want us to be the Native Americans and let all the potential European conquerors know we’re here.
>- Humans -- terrorists with their hands on a weapon that could cause extinction, a catastrophic global war, humans creating something smarter than themselves hastily without thinking about it carefully first.

Tim Urban: "Bostrom points out that if #1 and #2 haven’t wiped us out so far in our first 100,000 years as a species, it’s unlikely to happen in the next century."


## Controlling ASI

"When ASI arrives, who or what will be in control of this vast new power, and what will their motivation be?" (Urban)

## Malicious humans in control

"A malicious human, group of humans, or government develops the first ASI and uses it to carry out their evil plans." (Urban)

. . .

>- North Korea, ISIS, Iran, the US government ...
>- The problem for the creators of this AI would be that they too would be affected or destroyed by it.
>- So it is unlikely that anyone would willingly create malicious AI that cannot be contained.

## Malicious ASI in control

"The plot of every AI movie. AI becomes as or more intelligent than humans, then decides to turn against us and take over." (Urban)

. . . 

>- The stuff of movies.
>- Unlikely, because it's based on anthropomorphising AI.

## The "Robotica" handwriting machine

>- A small startup creates a robot 

## What motivates an AI system?


## The Fermi Paradox (1)


## The Fermi Paradox (2)


## The Fermi Paradox (3)



## How an ASI would be superior to us

- Intelligence amplification. The computer becomes great at making itself smarter, and bootstrapping its own intelligence.
- Strategizing. The computer can strategically make, analyze, and prioritize long-term plans. It can also be clever and outwit beings of lower intelligence.
- Social manipulation. The machine becomes great at persuasion.
- Other skills like computer coding and hacking, technology research, and the ability to work the financial system to make money.

## Decisive strategic advantage

"Bostrom and many others also believe that the most likely scenario is that the very first computer to reach ASI will immediately see a strategic benefit to being the world’s only ASI system. And in the case of a fast takeoff, if it achieved ASI even just a few days before second place, it would be far enough ahead in intelligence to effectively and permanently suppress all competitors. Bostrom calls this a decisive strategic advantage, which would allow the world’s first ASI to become what’s called a singleton—an ASI that can rule the world at its whim forever, whether its whim is to lead us to immortality, wipe us from existence, or turn the universe into endless paperclips."

## Singletons and safety

"The singleton phenomenon can work in our favor or lead to our destruction. If the people thinking hardest about AI theory and human safety can come up with a fail-safe way to bring about Friendly ASI before any AI reaches human-level intelligence, the first ASI may turn out friendly.21 It could then use its decisive strategic advantage to secure singleton status and easily keep an eye on any potential Unfriendly AI being developed. We’d be in very good hands.

But if things go the other way—if the global rush to develop AI reaches the ASI takeoff point before the science of how to ensure AI safety is developed, it’s very likely that an Unfriendly ASI like Turry emerges as the singleton and we’ll be treated to an existential catastrophe.

As for where the winds are pulling, there’s a lot more money to be made funding innovative new AI technology than there is in funding AI safety research…

This may be the most important race in human history. There’s a real chance we’re finishing up our reign as the King of Earth—and whether we head next to a blissful retirement or straight to the gallows still hangs in the balance."

# Critics of Kurzweil and the Singularity

## (Wikipedia:)

Kurzweil's ideas have generated criticism within the scientific community and in the media.

Although the idea of a technological singularity is a popular concept in science fiction, some authors such as Neal Stephenson[82] and Bruce Sterling have voiced skepticism about its real-world plausibility. Sterling expressed his views on the singularity scenario in a talk at the Long Now Foundation entitled The Singularity: Your Future as a Black Hole.[83][84] Other prominent AI thinkers and computer scientists such as Daniel Dennett,[85] Rodney Brooks,[86] David Gelernter[87] and Paul Allen[88] also criticized Kurzweil's projections.

In the cover article of the December 2010 issue of IEEE Spectrum, John Rennie criticizes Kurzweil for several predictions that failed to become manifest by the originally predicted date. "Therein lie the frustrations of Kurzweil's brand of tech punditry. On close examination, his clearest and most successful predictions often lack originality or profundity. And most of his predictions come with so many loopholes that they border on the unfalsifiable."[89]

Bill Joy, cofounder of Sun Microsystems, agrees with Kurzweil's timeline of future progress, but thinks that technologies such as AI, nanotechnology and advanced biotechnology will create a dystopian world.[90] Mitch Kapor, the founder of Lotus Development Corporation, has called the notion of a technological singularity "intelligent design for the IQ 140 people...This proposition that we're heading to this point at which everything is going to be just unimaginably different—it's fundamentally, in my view, driven by a religious impulse. And all of the frantic arm-waving can't obscure that fact for me."[25]

Some critics have argued more strongly against Kurzweil and his ideas. Cognitive scientist Douglas Hofstadter has said of Kurzweil's and Hans Moravec's books: "It's an intimate mixture of rubbish and good ideas, and it's very hard to disentangle the two, because these are smart people; they're not stupid."[91] Biologist P. Z. Myers has criticized Kurzweil's predictions as being based on "New Age spiritualism" rather than science and says that Kurzweil does not understand basic biology.[92][93] VR pioneer Jaron Lanier has even described Kurzweil's ideas as "cybernetic totalism" and has outlined his views on the culture surrounding Kurzweil's predictions in an essay for Edge.org entitled One Half of a Manifesto.[42][94]

British philosopher John Gray argues that contemporary science is what magic was for ancient civilizations. It gives a sense of hope for those who are willing to do almost anything in order to achieve eternal life. He quotes Kurzweil's Singularity as another example of a trend which has almost always been present in the history of mankind.[95]


## Chalmers' criticism







