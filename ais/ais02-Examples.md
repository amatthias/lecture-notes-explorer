---
title:  "AI and Society: 2. Examples of AI and philosophical issues"
author: Andreas Matthias, Lingnan University
date: September 4, 2019
...

# Some examples of applied AI systems

## Deep Blue (IBM) vs. Kasparov, 1997

![](graphics/01-chess.jpg) \

 
## Stanford University's “Stanley”, 2005

![](graphics/01-stanley.jpg) \


## Google driverless car (1)

![](graphics/01-google-car-1.jpg) \


## Google driverless car (2)

![](graphics/01-google-car-2.jpg) \


## Tesla autopilot

<https://www.youtube.com/watch?v=3yCAZWdqX_Y>

## Face recognition

![](graphics/01-face.jpg) \


Automated tagging of faces in images: Facebook: June 2011, Google+: December 2011.

## IBM Watson

Image recognition:

<http://visual-recognition-demo.mybluemix.net/>

## IBM Watson wins Jeopardy (Feb 14, 2011)

<https://www.youtube.com/watch?v=P18EdAKuC1U>

## AlphaGo wins against Lee Sedol at Go (Weiqi)

![9.3.2016: AlphaGo. 10.3.: AlphaGo. 12.3.: AlphaGo. 13.3: Lee Sedol. 15.3.: AlphaGo.](graphics/01-alphago-1.jpg) \


## AlphaGo wins against Ke Jie at Go (Weiqi)

![23/25/27.5.2017: AlphaGo wins all three games.](graphics/01-kejie.jpg) \


## AlphaGo wins against Ke Jie at Go (Weiqi)

AlphaGo also won in a team game against five top-ranking players (Chen Yaoye, Zhou Ruiyang, Mi Yuting, Shi Yue, and Tang Weixing).

Right now, AlphaGo is clearly much better than the best human Go players in the world.

This is a significant milestone, because Go was the last game that was considered to be so complex that machines would not be able to play it well (chess was already successfully ‘solved’ in 1997!)

See: The Future of Go Summit (2017): <https://events.google.com/alphago2017/>

## Robotics: Honda Asimo

![](graphics/01-asimo.jpg) \


## Robotics: Aibo

![](graphics/01-aibo.jpg) \


## Robotics: Pepper

![](graphics/01-pepper.jpg) \


## Robotics videos

Honda Asimo: <https://www.youtube.com/watch?v=QdQL11uWWcI>

Boston Dynamics "Atlas" lifting robot: <https://www.youtube.com/watch?v=rVlhMGQgDkY>

Boston Dynamics "Wild Cat" soldier assisting robot: <https://www.youtube.com/watch?v=wE3fmFTtP9g>

## Philosophical issues

What are the specific philosophical issues with 

1. Chess and Go playing machines?
2. Self­driving cars?
3. Automated face recognition?
4. IBM Watson?
5. Autonomous toy robots for children?
6. The Boston Dynamics robots?

Make a list of philosophical questions each technology poses! Then we will discuss your ideas.

. . . 

Textbook: <https://chatbotslife.com/philosophical-issues-ai-in-games-2017-207aff30dbb5>

## Some results (1)

- Chess and Go playing machines make the game less enjoyable to humans and threaten professional human players.
- Self driving cars: Who is responsible for accidents? Who pays for damages?
- Can machines replace humans in other everyday activities?
- Can we have emotional relationships with robots?
- It has been true for a long time that humans lose jobs to machines: ATMs, car factories, vending machines, email, post office letter sorting, ... etc. Robots will replace car, van, taxi and bus drivers very soon. Problems?
    - Leaves fewer things for humans to do.
	- Can machines replace *mental* functions of humans? (They do: chess players, parts of the jobs of accountants).
    - Perhaps this will require societies to introduce a basic income independently from work, because there will not be enough jobs for humans to do.

## Some results (2)

- Can Watson *think?* Is there moral intuition in AI systems?
- Can robots be *persons* and claim rights? Robot prostitution and slavery? Would this even be immoral? Why?
- Do we want to be dominated by a few giant, US companies?
    - War robots will be available only to a few countries. Some countries will therefore be able to go to war without losing soldiers, while others will still have to send their citizens to die.
	- Economic domination of smaller countries.
	- With the export of technology comes the export of moral values.
- War robots might make wars easier. (But would this be a bad thing if only robots are involved?)

## Other moral problems of AI systems

- Can/should computers be responsible for their actions?
- Must responsible computers have rights?
- Can AI systems acquire personhood?
- What is the moral status of bionic creatures?
- Are war robots immoral?
- Moral problems following from the emotional bonding between human and machine


## The End. Thank you for your attention!

Questions?

<!--

%% Local Variables: ***
%% mode: markdown ***
%% mode: visual-line ***
%% mode: cua ***
%% mode: flyspell ***
%% End: ***

-->


