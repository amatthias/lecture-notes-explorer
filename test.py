def de_total_tries( user ):
    count=0
    for x in stats:
        if (x[0]==user):
            count+=1;
    return count

def de_this_tries( user, word ):
    count=0
    for x in stats:
        if (x[0]==user and x[1]==word):
            count+=1;
    return count


def de_this_errors( user, word ):
    count=0
    for x in stats:
        if (x[0]==user and x[1]==word and x[2]==0):
            count+=1;
    return count


def de_feedback( user, word, correct ):
    # Correct is an integer, 1: true, 0: false
    t = [user, word, correct]
    stats.append( t )


stats=[]

de_feedback( "A", "word1", 1 );
de_feedback( "B", "word1", 1 );
de_feedback( "A", "word1", 1 );
de_feedback( "A", "word2", 0 );
de_feedback( "A", "word3", 0 );

print(de_total_tries( "A" ));
print(de_this_tries( "A", "word2" ));
print(de_this_errors( "A", "word1" ));
