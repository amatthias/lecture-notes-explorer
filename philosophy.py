import flask, random, re, pickle
from flask import request, jsonify
from flask_cors import CORS
from urllib.parse import unquote

# TODO: Deleting points and resetting user records doesn't work yet



app = flask.Flask(__name__)
CORS(app)
app.config["DEBUG"] = True

entries = []
id=0
de_entries = []
stats=[]


def getcontext( filename, line ):
    context = ""
    with open(filename) as f:
        for cline in f:
            if ((cline.split("&")[0]==line.split("&")[0]) and (cline.split("&")[1]==line.split("&")[1])):
                context+=cline.split("&")[2];
    return context;


#              (line.split("&")[2].lower().find(unquote(pattern.lower())) != -1):        


def grep( cl, pattern ):
    entries = []
    id=0
    filename=cl+"/bigfile-"+cl+".md"
        
    with open( filename ) as f:
        for line in f:
            oneword = {}                   
            if re.search(r'\b'+pattern.lower()+r'\b', line.split("&")[2].lower(), re.IGNORECASE):
                oneword['id'] = id
                oneword['file'] = line.split("&")[0]
                oneword['section'] = line.split("&")[1]
                oneword['line'] = line.split("&")[2]
                oneword['p'] = getcontext(filename, line)
                id=id+1
                entries.append(oneword)
        
    return( entries )



def de_maybe_load_files():
    filename="school-de/german-words.txt"

    try:
        with open('./article-stats.pickle', 'rb') as f:
            stats=pickle.load(f)
    except:
        pass
    
    if ( len(de_entries) == 0 ):
        with open( filename ) as f:
            for line in f:
                de_entries.append(line.strip())
        return True;

    return False;


def de_get_spaced_article_exercise( user ):
    did_load = de_maybe_load_files()
    id=0
    word = random.choice(de_entries)
    this_tries = de_this_tries( user, word );
    this_errors = de_this_tries( user, word );
    total_tries = de_total_tries( user );
    total_errors = de_total_errors( user );
    return( word +" "+user+" "+str(total_tries)+" "+str(total_errors)+" "+str(this_tries)+" "+str(this_errors) )


def de_total_tries( user ):
    count=0
    for x in stats:
        if (x[0]==user):
            count+=1;
    return count

def de_this_tries( user, word ):
    count=0
    for x in stats:
        if (x[0]==user and x[1]==word):
            count+=1;
    return count


def de_this_errors( user, word ):
    count=0
    for x in stats:
        if (x[0]==user and x[1]==word and x[2]==0):
            count+=1;
    return count

def de_total_errors( user ):
    count=0
    for x in stats:
        if (x[0]==user and x[2]==0):
            count+=1;
    return count


def de_feedback( user, word, correct ):
    # Correct is an integer, 1: true, 0: false
    did_load = de_maybe_load_files()
    t = [user, word, correct]
    stats.append( t )
    with open('./article-stats.pickle', 'wb') as f:
        pickle.dump(stats, f, pickle.HIGHEST_PROTOCOL)
    

def de_reset_user( user ):
    did_load = de_maybe_load_files()
    for x in stats:
        if (x[0]==user):
            stats.remove( x );
    with open('./article-stats.pickle', 'wb') as f:
        pickle.dump(stats, f, pickle.HIGHEST_PROTOCOL)


    
@app.route('/', methods=['GET'])
def home():
    return "<h1>Lecture notes search</h1><p>Error: No search term. <a href='https://amatthias.pythonanywhere.com/find'>Click here</a> to start a new search!</p>The German articles test is <a href='https://amatthias.pythonanywhere.com/german-articles'>here</a>."



@app.route('/api/v1/classes/love', methods=['GET'])
def api_search_love():
    # Check if a search term was provided as part of the URL.
    if 'search' in request.args:
        search = request.args['search']
    else:
        # Return
        return jsonify("No search term")
        
    results = grep( "love", search )
    return jsonify(results)


@app.route('/api/v1/classes/lct', methods=['GET'])
def api_search_lct():
    # Check if a search term was provided as part of the URL.
    if 'search' in request.args:
        search = request.args['search']
    else:
        # Return
        return jsonify("No search term")
        
    results = grep( "lct", search )
    return jsonify(results)


@app.route('/api/v1/classes/ais', methods=['GET'])
def api_search_ais():
    # Check if a search term was provided as part of the URL.
    if 'search' in request.args:
        search = request.args['search']
    else:
        # Return
        return jsonify("No search term")
        
    results = grep( "ais", search )
    return jsonify(results)



@app.route('/api/v1/school/de/articles/reset/<user>', methods=['GET'])
def api_de_reset_user( user ):
    de_reset_user( user );
    return jsonify("ok");
    

@app.route('/api/v1/school/de/articles', methods=['GET'])
def api_de_get_spaced_article_exercise():
    # Check if a user name was provided as part of the URL.
    if 'user' in request.args:
        user = request.args['user']
    else:
        user = "default-user"
        
    results = de_get_spaced_article_exercise( user )

    return jsonify(results)


@app.route('/api/v1/school/de/articles/<user>/<word>/<int:correct>', methods=['GET'])
def api_de_feedback(user, word, correct):
    de_feedback( user, word, correct );
    return jsonify(user+" "+word+" "+str(correct))




if __name__ == "__main__":
    de_entries = []
    app.run()
