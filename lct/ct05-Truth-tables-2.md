% Critical Thinking
% 5. Truth tables, tautologies, contradictions
% A. Matthias

# Where we are

## What did we learn last time?

- Logical operations can be described by truth tables

## What are we going to learn today?

- We will practice using truth tables on everyday language
- We will talk about tautologies, contradictions, contingent, consistent and inconsistent statements

# Translations

## Translate into symbolic form:

"If and only if this tree gets enough water and sunshine and it’s not too cold, it will grow well and give fruit."

. . .

w: this tree gets enough water  
s: this tree gets enough sunshine  
c: it’s too cold  
g: this tree will grow well  
f: this tree will give fruit  

. . .

\f{(w$\land$s$\land$$\lnot$c)$\leftrightarrow$(g$\land$f)}

## Translate into a formal expression!
The school has collapsed, but it is not true that five students died.

. . .

\f{S$\land$$\lnot$D}

## Translate into a formal expression!

If you want to go, then I will go with you if it is sunny.

. . .

Y: You want to go.
I: I will go with you
S: It is sunny.

. . . 

\f{Y$\to$(S$\to$I)}

or

\f{S$\land$Y$\to$I}

## Translate into a formal expression!

Whether Peter is coming to the party or not, Mary is not going to come.

. . . 

\f{(P$\lor$$\lnot$P)$\to$$\lnot$M}

or

\f{(P$\to$$\lnot$M)$\land$($\lnot$P$\to$$\lnot$M)}

or simply

\f{$\lnot$M}

(but this is not a good translation!)

## Make a truth table

Whether Peter is coming to the party or not, Mary is not going to come.

\f{(P$\to$$\lnot$M)$\land$($\lnot$P$\to$$\lnot$M)}

or simply:

\f{$\lnot$M}

Check with a truth table that these two solutions have the same truth values!

. . .

*Make sure to write the two expressions into the same truth table!*

## Translate into a formal expression!

Unless you try to improve yourself, and unless you improve your attitude, you are not going to succeed.

. . . 

Y: You try to improve yourself.
A: You improve your attitude.
S: You are going to succeed.

. . . 

\f{$\lnot$(Y$\land$A)$\to$$\lnot$S} or \f{(Y$\land$A)$\lor$$\lnot$S} or \f{S$\to$(Y$\land$A)}

. . . 

Be careful:

\f{$\lnot$(Y$\land$A)$\to$$\lnot$S} is equivalent to: \f{S$\to$(Y$\land$A)}

but *not* equivalent to:

\f{(Y$\land$A)$\to$S}

(S might need additional reasons in order to be true!)

## “Unless ... not”

For example:

In order to stay alive, you need to breathe, to eat and to drink. So it is correct to say:

“Unless you breathe and eat you are not going to stay alive.”

But this can *not* be converted into:

“If you breathe and eat you will stay alive” (because I also need to drink!)

# Truth tables

## Truth tables for the basic logical operators (summary)

![](graphics/07-operators.png)\ 

## How to build a truth table

Example:

If the weather is fine and I don't have to work, I will go to the park or to the beach.

. . . 

>- 1st step: translate into symbolic form: \f{(F$\land\lnot$W)$\to$(P$\lor$B)}


## How to build a truth table

![](graphics/07-tt1.png)\ 

## How to build a truth table

![](graphics/07-tt2.png)\ 

## How to build a truth table

![](graphics/07-tt3.png)\ 

## How to build a truth table

![](graphics/07-tt4.png)\ 

## How many lines do I need?

>- Non-maths answer: Insert “T” and “F” until all your variables go from true, true, true ... to false, false, false ...
>- Maths answer: $2^n$ where $n$ is the number of variables (propositions) in your expression.
>- That is:
>      - 2 variables = 4 lines
>      - 3 variables = 8 lines
>      - 4 variables = 16 lines

## How to build a truth table

![](graphics/07-tt5.png)\ 

## How to build a truth table

![](graphics/07-tt6.png)\ 

## How to build a truth table

![](graphics/07-tt7.png)\ 

## How to build a truth table

![](graphics/07-tt8.png)\ 

## How to build a truth table

![](graphics/07-tt9.png)\ 

## How to build a truth table

![](graphics/07-tt10.png)\ 

## How to build a truth table

![](graphics/07-tt11.png)\ 

## How to build a truth table

![](graphics/07-tt12.png)\ 

## How to build a truth table

![](graphics/07-tt13.png)\ 

## How to build a truth table

![](graphics/07-tt14.png)\ 

## How to build a truth table

![](graphics/07-tt15.png)\ 

## How to build a truth table

![](graphics/07-tt16.png)\ 

## How to build a truth table

![](graphics/07-tt17.png)\ 

## How to build a truth table

![](graphics/07-tt18.png)\ 

## How to build a truth table

![](graphics/07-tt19.png)\ 

## How to build a truth table

![](graphics/07-tt20.png)\ 

## How to build a truth table

![](graphics/07-tt21.png)\ 

## How to build a truth table

![“If the weather is fine and I don't have to work, I'll go to the park or to the beach”: (F$\land\lnot$W)$\to$(P$\lor$B)](graphics/07-tt22.png)

## Truth table practice

Now calculate the truth table for:

"If it is hot, I will go to the beach and swim or surf."

To help you, here are again the truth tables for the basic logical operators:

![](graphics/07-operators.png)\ 

## Solution

"If it is hot, I will go to the beach and swim or surf."

\f{H$\to$B$\land$(S$\lor$F)}

11 true, 5 false.

## Summary: How to calculate a truth table

![](graphics/07-tt-how.png)\ 

# Tautologies and contradictions

## Logical tautologies (\cn{重言式})

>- All daughters have a mother
>- All ships are ships
>- Bananas are either sweet or not sweet
>- These sentences are not only true.
>- They are *necessarily* true. They could never be false.
>- They are called *logical tautologies.*

## Logical tautologies (\cn{重言式})

A logical tautology is a sentence that cannot be anything but true for logical reasons.

e.g.: “Bananas are sweet or not sweet”

S: Bananas are sweet

\f{S$\lor$$\lnot$S}

The truth table of this shows that it is always true!

## Contradictions (\cn{矛盾})

The opposite of the tautology is the *contradiction.*

A logical contradiction is a sentence which, for logical reasons, is always false.

## Contradictions (\cn{矛盾})

e.g.: “My car is big and small (=not big)”

\f{B$\land$$\lnot$B}

## Contingent statements (\cn{適然句})

Every statement which is neither a tautology nor a contradiction is said to be a *contingent statement.*

This means that contingent statements can be either true or false, depending on the truth values of the propositions which compose them.

## Tautologies/contradictions

We can use truth tables to show that some statements are tautologies (always true), contradictions (always false), or contingent statements (sometimes true, sometimes false).

## Tautologies/contradictions

*Show whether the following statement is a tautology, a contradiction, or a contingent statement:*

“If John marries me I will be happy, and if he doesn’t marry me then I will not be happy.”

. . . 

\f{(m$\to$h)$\land$($\lnot$m$\to\lnot$h)}

. . .

Contingent statement! (Some lines in the truth table are true, some are false).

This is actually equivalent to:

\f{m$\leftrightarrow$h}

# Consistent and inconsistent

## Consistent statements

Multiple statements are *consistent* with each other if they can be true at the same time. That is, if it is possible for them to be all true together.

A set of statements is *inconsistent* if it would be impossible for them all to be true at the same time.

*Don’t confuse consistent and contingent!*

## Consistent statements

C: I am in class now  
H: I am hungry now  
P: I am at Pacific Place now  

>- C and H are consistent (I can be in class and hungry at the same time, although this doesn’t *have* to be true!)
>- H and P are also consistent.
>- But C and P are not consistent (they are inconsistent), because I cannot be in two places at the same time.

## Are these statements consistent?

1. I will go the cinema tonight.
2. If I don’t go to the cinema tonight, I will not eat rice tonight.
3. I don’t eat rice tonight.

*Use a truth table to show whether these statements are consistent!*

## Are these statements consistent?

1. I will go the cinema tonight:  
\f{c}
2. If I don’t go to the cinema tonight, I will not eat rice tonight:  
\f{$\lnot$c$\to\lnot$r}
3. I don’t eat rice tonight:  
\f{$\lnot$r}

. . . 

![Yes, they are consistent.](graphics/07-tt-consistent.png)\ 

## Are these statements consistent?

1. I will go the cinema tonight
2. *If and only if* I don’t go to the cinema tonight, I will not eat rice tonight
3. I don’t eat rice tonight

*Use a truth table to show whether these statements are consistent!*

## Are these statements consistent?

1. I will go the cinema tonight:  
\f{c}
2. If and only if I don’t go to the cinema tonight, I will not eat rice tonight:  
\f{$\lnot$c$\leftrightarrow\lnot$r}
3. I don’t eat rice tonight:  
\f{$\lnot$r}

. . . 

![No, they are inconsistent.](graphics/07-tt-inconsistent.png)\ 

# Exercises

## Translate into symbolic form:

"For lunch, I want a sandwich with cheese or egg. Or I might have a soup or a steak, but the soup without chicken meat or fish balls."

. . .

- w: sandwich, c: cheese, e: egg
- s: soup, t: steak
- m: chicken meat, f: fish balls

\f{\LARGE{(w$\land$(c$\lor$e)) $\lor$ (s$\land\lnot$(m$\lor$f)) $\lor$ t}}

. . . 

Second version (not so good):

\f{\textcolor{red}{(c$\lor$e)}$\lor$(s$\land\lnot$(m$\lor$f))$\lor$t}

This is “not so good” because it does not mirror the original sentence exactly 
(the “sandwich with” part is gone)

## Why is this one wrong?

"For lunch, I want a sandwich with cheese or egg. Or I might have a soup or a steak, but the soup without chicken meat or fish balls."

- w: sandwich, c: cheese, e: egg
- s: soup, t: steak
- m: chicken meat, f: fish balls

**Wrong:** \f{(w$\land$(c$\lor$e))$\lor$((s$\lor$t)$\land$\textcolor{red}{$\lnot$(m$\lor$f)})}

. . .

Because it doesn't connect the chicken meat and the fish balls with the soup!

(So this one would forbid the serving of a chicken steak. But the customer didn't exclude it.)

## Why is this one wrong?

"For lunch, I want a sandwich with cheese or egg. Or I might have a soup or a steak, but the soup without chicken meat or fish balls."

- w: sandwich, c: cheese, e: egg
- s: soup, t: steak
- m: chicken meat, f: fish balls

**Wrong:** \f{(w$\land$(c$\lor$e))$\lor$(s$\land\lnot$\textcolor{red}{(m$\land$f)})$\lor$t}

. . .

This would mean that I don't want meat *and* fishballs at the same time. But if I had *only* meat, I would be happy (which is not what the speaker meant). So it must be “not (meat *or* fishballs)” instead:

\f{(w$\land$(c$\lor$e))$\lor$(s$\land\lnot$\textcolor{green}{(m$\lor$f)})$\lor$t}

## Meat and/or fishballs

!["Without meat and/or fishballs." We will discuss this more next time!](graphics/07-meat-fishballs.png)

## Translate into symbolic form:

"I'd like a green tea with peach flavour or a jasmine tea without any flavour. Into the green tea with peach flavour I like sugar, but put sugar into the jasmine tea only if it is very hot. Otherwise please serve it with some milk. Thank you!"

- g: I like green tea, p: I like peach flavour
- j: I like jasmine tea, f: I like any flavour
- s: put sugar into the tea, h: tea is very hot
- m: serve it with some milk

. . .

\f{\LARGE{(g$\land$p$\land$s)$\lor$(j$\land\lnot$f$\land$h$\land$s)$\lor$(j$\land\lnot$f$\land\lnot$h$\land$m)}}

or

\f{\LARGE{(g$\land$p$\land$s)$\lor$((j$\land\lnot$f)$\land$((h$\leftrightarrow$s)$\land$($\lnot$h$\leftrightarrow$m)))}}

## Common mistake

A common mistake in the previous expression would be to write something like:

- A: expression for the one tea
- B: expression for the other tea
- M: I want the tea with milk

Wrong: \f{A$\lor$B$\lor$M}

Because now we could have the milk (M) without any tea!

You must make sure that the milk is connected to some tea.

## Comparing truth tables

By comparing the truth tables of sentences, we can see exactly the logical differences between sentences.

Compare:

1. Give me a tea with milk and no sugar
2. If you give me a tea with milk, it should have no sugar.

*Write down and compare the truth tables!*

. . .

1. \f{t$\land$m$\land\lnot$s}
2. \f{(t$\land$m)$\to\lnot$s}

## Solution

![](graphics/07-tea-milk-sugar.png)

## What did we learn today?

- We practiced using truth tables on everyday language.
- We talked about tautologies, contradictions, contingent, consistent and inconsistent statements.

## Any questions?

![](graphics/questions.jpg)\ 

## Thank you for your attention!

Email: <matthias@ln.edu.hk>

If you have questions, please come to my office hours (see course outline).
