% Critical Thinking
% 20. Debate 3: Surveillance and freedom
% A. Matthias

# Where we are

## What did we learn last time?

- Climate change ethics

## What are we going to learn today?

- Surveillance and freedom

# How to deal with an applied question

*Not necessarily in that order:*

- Understand the problem
- Identify the stakeholders
- Make relevant distinctions and clarify the issues
- Find and quote relevant theory (from research)
- Find the main arguments for each side (from research)
- Find places where more research (facts) is needed
- Identify benefits and costs and weigh them against each other
- Avoid yes/no answers. Try to see the complexities of the problem
- Try to find original alternatives that solve the problem better than just saying that one side is right (for example: meat eating might be morally right if animals are treated well, if we reduce the amount of meat consumed, if we eat only wild animals or insects etc)



# Social contract

## Hobbes, Locke, Rousseau

Three thinkers:

. . . 

- Thomas Hobbes (1588-1679)
- John Locke (1632-1704)
- Jean-Jacques Rousseau (1712-1778)

. . .

We will mainly talk about Hobbes.

# The State of Nature

## The State of Nature (1)

Imagine there is no God, no society, no government. Just a few people living each for themselves in nature.

How would life be?

- Hunting, gathering food
- Gathered food can be taken away by others
- No trust between people, therefore no friendships
- A state of war of all against all

. . .

Hobbes calls this the *"State of Nature."*

## The State of Nature (2)

What would *not* exist in this world?

- Schools, Universities
- Industry
- Markets
- Division of labour
- Culture, books, art
- Air-conditioning, proper buildings, roads, electricity, water pipes, metals, iPhones
- No money and no way to accumulate wealth (nature's wealth rots away easily!)

. . .

... because all these things require division of labour and personal safety!

## The State of Nature (3)

Is it true that the stronger people would be happy with this life, while only the weak would suffer?

. . . 

No, the strong would suffer too:

1. Every strong person has once been weak (as a child)
2. Every strong person will be weak again (in sleep, in illness, in old age)
3. Except for one person, every strong person has at least one who is even stronger than him. If he meets those, he will be the relatively weaker one
4. Many weak people together can always defeat one strong person

## The State of Nature (4)

Even if one could be undisputed and strong in one's community, would he enjoy his life?

>- There are no luxuries, no culture, no comfortable buildings, no electricity
>- No accumulation of wealth possible. Natural products rot and go bad
>- The life of the strong is almost as bad as the life of the poor, and much worse than the poorest life in *our* society

## The State of Nature (5)

### Hobbes:

The life of man in the State of Nature would be: **"solitary, poor, nasty, brutish, and short."**

. . . 

>- Solitary: lonely, because there is no trust between men
>- Poor: because there is no wealth to be had
>- Nasty: bad, annoying, dangerous
>- Brutish: animal-like, because there is no culture, no education
>- Short: This seems like a blessing, considering the other attributes


## The core idea of Hobbes' Social Contract

We can agree to:

>- Give power away from each one individual,
>- and to concentrate this power in one, extremely powerful entity.
>- This entity will, in return, force all the citizens to cooperate, so that they can escape the State of Nature.

## Locke’s State of Nature

>- For Locke, things are a bit different.
>- The State of Nature is not such a bad state.
>- Humans have share all natural resources. They own their own lives and the fruits of their own work.
>- People *naturally* have the right of life, liberty and possessions.
>- Sometimes, particular “bad” people disturb the peaceful working of primitive society and threaten the rights of others.
>- Everyone is entitled to protect their own rights, stop those who threaten the order, and restore the peace.
>     - In fact, people are not even *allowed* to voluntarily give away their rights. The rights to life and freedom are given by God and therefore cannot be given away, even with the consent of the individual. 
>     - Therefore, nobody *can* agree to a dictatorship that would take away these rights.

## Locke’s government

>- In time, people realise that it is easier to keep society safe if they form a special group of people (the government) whose job it becomes to protect people’s rights.
>- So for Locke, the government has a particular task to fulfil. But it is not allowed to interfere with the rights of the citizens.
>- The separation of legislative and executive power is necessary to make sure that the government does not misuse its power.
>- The invention of *money* makes it possible to store wealth and gives an incentive for people to produce more than they can consume, so that the wealth of the society increases. This is a good thing for all citizens. Eventually, the additional wealth will benefit everyone.

## Rousseau’s State of Nature (1)

>- Rousseau had quite the opposite idea from Hobbes.
>- He thought that the State of Nature was an environment in which people were originally free, equal and peaceful.
>- When people began to claim property and take things for themselves, the result was inequality, conflicts and war.
>- The social contract, for Rousseau, mainly has the function of protecting the original freedom of men.

## Rousseau’s State of Nature (2)

>- Therefore, the society should not be ruled by a king or political party, but by a system where all citizens directly form the government. There should be no separation of powers.
>- All decisions should be taken by direct vote of the citizens. So everyone would be “forced to be free.”
>- The government should be “mere officials” who administer society, but who don’t take decisions of their own. They just execute the will of the people.
>- Rousseau was very negative about the governments of his time. He thought that they did not properly implement a social contract and that they did not protect citizens’ interests.


## The Social Contract

### The core idea:

The Social Contract is a fictional contract that people create so that power is taken away from the individuals and transferred to a Third Party. In return, the Third Party will provide the guarantees that are necessary in order for the individuals to be able to escape the State of Nature.

### A more general definition:

"[A set of rules that] rational people will agree to accept, for their mutual benefit, on the condition that others follow those rules as well." (Rachels, 11.3)

## The three thinkers

>- Hobbes: Strong government keeps everyone out of the state of nature.
>- Locke: Weaker government only protects original freedoms.
>- Rousseau: Government takes no own decisions, only executes the will of the people.


## Government power

When you create a powerful entity like that, it is better not to give it too much power, else it may misuse its power to suppress you and take away your freedom.

>- So, how much power do we need to give to the government?
>- And what exactly should its functions be?

## Government power: Hobbes

>- The government is the only protection from the state of nature. Since the state of nature is the worst possible life, whatever a government does will be better. 
>- Therefore, governments should even have the right to take away personal liberties if this is necessary in order to secure the well-functioning of society.
>- Free speech, freedom of movement and religion, these are all less important than being out of the state of nature, and can therefore be given up to a strong government.
>- But, in the end, the purpose of the government should always be to work *for* its citizens, not against them.
>- So a government that clearly puts its citizens in a worse condition than the state of nature would not be legitimate (treatment of Jews in WW2 Germany, slavery).

## Government power: Locke (1)

>- “A morally legitimate government coerces [=forces people to do something] only those who freely and voluntarily consent to its authority.” (Section 95 in the Second Treatise.)
>- A government that claims absolute, unlimited authority to coerce its subjects cannot be morally legitimate.
>- No one could rationally consent to an absolutist government. 
>- The rational basis for establishing government and submitting to it is to bring it about that fundamental moral rights specified by natural law are upheld (chapter 9). 
>- Submitting to absolutist government would make it far more likely that fundamental moral rights, one’s own and those of other people, would be violated (sections 131, 139, and others). 
>- Hence, no absolutist government could arise by consent.

## Government power: Locke (2)

>- One does not have an absolute property right over oneself, one’s own body. Therefore, one does not have a right to commit suicide or to damage oneself.
>- Why?
>     - Because one’s rights are given by nature, or God, and thus can be limited.
>	 - We are not born free, in full possession of all possible rights over ourselves.
>- Since we don’t have unlimited rights over our own lives, we are not morally at liberty to transfer such rights to a political ruler by free and voluntary consent.

## Government power: Locke (3)

>- A morally legitimate government, 
>     - (=which coerces by right and which those coerced are obligated to support and obey), is one that both 
>	 - (a) coerces only those who freely and voluntarily consent to its authority and 
>	 - (b) protects their fundamental moral rights. ^[(http://philosophyfaculty.ucsd.edu/faculty/rarneson/Courses/LockeLimited.pdf)]

## Government power: Locke (4)

>- Locke advocates limited government, and limits to the authority of government. 
>- If the political rulers are violating people’s fundamental moral rights, it is right to rebel against them.


## Government power: Rousseau (1)

>- Rousseau rejects the idea that the government should rule over the people.
>- For him, to hand over one’s general right of ruling oneself to another person or body constitutes a form a slavery. 
>- A periodically elected  government would necessarily be legislating on a range of topics on which citizens have not given their opinion. 
>- Laws passed by such assemblies would therefore bind citizens in terms that they have not themselves agreed upon.
>- Elections and government by representation is therefore a symptom of moral decline and the loss of virtue.

## Government power: Rousseau (2)

>- But realistically, not everyone can be directly involved in day-to-day government operations.
>- Therefore, Rousseau distinguishes between sovereign and government.
>- The sovereign, composed of the people as a whole, makes laws as an expression of its general will. 
>- The government is a more limited body that administers the state within the bounds set by the laws, and which applies the laws to particular cases.

## How much power should the government have? (Summary)

>- For Hobbes, the state of nature is so bad that even a bad government is better than none. So the citizens should accept is as it is, even if it is bad. But it still must be better than the state of nature.
>- For Locke, the government cannot *create* any rights, including the right to ownership of things. Everyone naturally already has rights, and the government has to respect and protect these rights.
>- For Locke, if the citizens agree that the government is not doing its job, they should always be able to choose a new government and replace the old one.
>- For Locke, dividing the government into two parts (legislative and executive) is a way of restricting and controlling the government’s power.

## The state violating the Contract

Can the state violate the Social Contract?

Yes:

>- When the state itself harms its citizens more than the state of nature would do; or
>- When it is unable to protect them; or
>- When it violates their natural rights (Locke)

## What is the consequence of a state violating the contract?

Different philosophers disagreed on this:

. . . 

>- Hobbes thought that once citizens have given their power away, they have to obey their government unconditionally.
>- Locke thought that citizens do have the right to revolt against the government if it does not protect their (naturally given) rights.
>- Rousseau did not think that a government was a good (or necessary thing) at all. Citizens should directly decide about matters together. Whether a particular state action violates the contract or not depends on the actual agreements between the contract members.


# Surveillance

## Forms of surveillance

>- When you want to talk about surveillance, you need to distinguish different types of it:
>     - companies monitoring employees’ working times and performance;
>     - parents monitoring their children (babyphones);
>     - the MTR monitoring stations and trains with security cameras; 
>     - the government monitoring streets with face recognition cameras, reading citizens’ social media posts, reading private correspondence, tapping citizens’ phones (NSA);
>- Potentially, surveillance may lead to other measures: social credit, restrictions on movement and so on.
>- All these have to be distinguished. They are different in many respects (for example, misuse potential, who profits from the surveillance, what the ultimate goal is, whether it respects individual freedom, ...)


# Surveillance technology

## Different types of surveillance

>- Research different types of surveillance and technologies used:
>     - Cameras with face recognition
>     - Work clocking systems
>     - GPS bracelets for quarantine purposes
>     - Gait recognition
>     - Phone line tapping
>     - Internet and social media observation
>     - Observation through walls
>     - Household garbage analysis
>- How are they different? Are they all equally effective? Are they equally dangerous? Are some better than others? What are their moral implications?


# Ethics of surveillance

## Utilitarianism

Different approaches. Utilitarianism: 

>- Does surveillance improve the society’s happiness? Does it provide a benefit to most people that is higher than its drawbacks?
>- In order to judge that, you need to consider:
>     - Effectiveness of surveillance in preventing crime? (research!)
>     - How many people share the burden vs how many are benefited?
>     - Are there other ways to improve safety without the drawbacks?
>     - Other drawbacks? For example, extensive surveillance may endanger trade secrets, free speech, freedom of academic research, the ability to remove bad governments in the future and so on.


## Kant

>- Kant: all humans are equal. They must be treated as ends, not only as means to some other end. Their own ends must be respected.
>- Does surveillance treat people “as ends”?
>- Or do people become just means to the end of lower crime rates?
>- Compare: forcing drivers and car passengers to wear seat belts. Is this morally right? What if the government enforces it by monitoring seat belt use with sensors inside the car? Would it then be morally right or wrong? Why exactly?
>- How far does personal freedom extend? Does it include the right to harm others? Does it include the right to harm oneself? Does the government need to protect citizens from their own mistakes or ignorance? (e.g. by certifying particular products as safe, or by outlawing unsafe products).

## Social contract

>- What kinds of surveillance would rational citizens consent to? Why exactly?
>- What kinds of surveillance are necessary for a society to stay out of the state of nature? What kinds promote or threaten the essential goals of the social contract? (The answer will be different for Hobbes/Locke/Rousseau!)
>- How can the government be prevented from misusing its powers? What legal and procedural safeguards need to be in place to prevent misuse and guarantee that the citizens keep their maximum freedom, while at the same time protecting society?


## Effect on moral standards

>- Another argument against total surveillance is that it endangers the moral fabric of society.
>- If people are unable to do anything bad, then acting morally is not a choice anymore, but becomes inevitable and enforced. So people are not any more choosing to be good (or bad). They give up their moral freedom.
>- As a result, being “good” is not morally valuable any more, and people lose the ability to make moral choices. In the end, this leads to a population that is obedient like a flock of sheep, but that doesn’t have any understanding of moral values.
>- Such populations are dangerous, because this total obedience can be misused by bad governments for bad ends (for example, to start wars).
>- It is always better to have a population that is able to make moral decisions, the argument goes, even if this means tolerating a few crimes.


# Final remarks

## Conclusion about creating your arguments / writing a paper

- Think through the different aspects of the problem
- Show that you thought about the different aspects, but then pick a small part of the question that you can answer well for the main part of your argument or paper
- Avoid yes/no answers. Appreciate the complexity of the problem
- Do your research, both about theory and about facts
- Use your research, and quote it correctly if you write a paper
- Try to find original solutions to the problem that go beyond saying that one side is right or wrong


## Any questions?

![](graphics/questions.jpg)\ 

## Thank you for your attention!

Email: <matthias@ln.edu.hk>

If you have questions, please come to my office hours (see course outline).
