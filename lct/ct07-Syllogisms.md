% Critical Thinking
% 7. Syllogisms
% A. Matthias

# Where we are

## What did we learn last time?

- Conditional expressions and conditional arguments
- Antecedent and consequent
- Unstated (hidden) conditions
- Valid and invalid argument forms of conditional arguments

## What are we going to learn today?

- Necessary and sufficient conditions
- Categorical syllogisms
- What can go wrong with them
- How Venn diagrams help us analyze syllogisms

# Necessary and sufficient conditions

## Necessary and sufficient
A necessary condition (\cn{必要條件}) is one which must be true in order for the consequent to happen.

But still, even if it is true, the consequent might also need other conditions to be fulfilled in order to happen.

## Necessary conditions

“If I study, then I will pass my examination.”

>- “If I study” is a necessary condition.
>- It must be true, otherwise I will not pass my examination.
>- But even if I study, I might fail (e.g. because I am sick or drunk at the date of the examination).
>- So this condition is *necessary* but not *sufficient.*

## Sufficient conditions

“If I eat ten cheeseburgers, then I will be sick.”

>- “If I eat ten cheeseburgers” is a sufficient condition (\cn{充分條件}) for getting sick.
>- If this is true, then the consequence will also be true.
>- Nothing else besides this condition is needed to bring about the consequence.

## Necessary conditions

![](graphics/08-necessary.png)\ 

## Sufficient conditions

![](graphics/08-sufficient.png)\ 

## Necessary and sufficient

Look at the result first:

>- If you cannot have the result without the condition, then the condition is necessary.
>- If the condition *alone* is sure bring about the result, then the condition is sufficient.

## Necessary and sufficient

We can express this as a conditional statement:

>- If A is *necessary* for B, then:
>- If not A then not B; or
>- if B then A

>- If A is *sufficient* for B, then:
>- If A then B; or
>- if not B then not A

## Equivalent?

We just said:

>- If A is sufficient for B, then:
>- If A then B; or
>- if not B then not A

>- It seems then, that A$\to$B and $\lnot$B$\to\lnot$A are equivalent.
>- But is this true? *Show with a truth table that these two expressions are equivalent!*

## Now try it yourself!

Which are necessary, which are sufficient?

1. If you eat a lot of rice, you will not be hungry.
2. You have to take a General Education course in order to complete your studies.
3. If you walk in the rain without any protection, you will get wet.
4. If you print out that email, you can read it at home.
5. If you have a tea bag, you can make some tea.

## Necessary and sufficient

>1. If you eat a lot of rice, you will not be hungry: **sufficient.**
>2. You have to take some common core courses in order to complete your studies: **necessary.**
>3. If you walk in the rain without any protection, you will get wet: **sufficient.**
>4. If you print out that email, you can read it at home: **sufficient.**
>5. If you have a tea bag, you can make some tea: **neither necessary nor sufficient**:
>      1. I could have tea leaves instead of a tea bag.
>      2. I still need hot water.

## “If” and “only” (further explanation) (1)

>- "Betty will eat the fruit if it is an apple."
>- This is equivalent to: "Only if Betty will eat the fruit, is it an apple."
>- Or: "If the fruit is an apple, then Betty will eat it."

. . . 

>- This states simply that Betty will eat fruits that are apples.
>- It does not, however, exclude the possibility that Betty might also eat bananas or other types of fruit.
>- All that is known for certain is that she will eat any and all apples that she happens upon.
>- That the fruit is an apple is a sufficient condition for Betty to eat the fruit.

## “If” and “only” (2)

>- "Betty will eat the fruit *only* if it is an apple."
>- This is equivalent to: "If Betty will eat the fruit, then it is an apple."
>- Or: "Betty will eat the fruit $\to$ fruit is an apple."

. . .

>- This states that the only fruit Betty will eat is an apple.
>- It does not, however, exclude the possibility that Betty will refuse an apple if it is made available, in contrast with (1), which requires Betty to eat any available apple.
>- In this case, that a given fruit is an apple is a *necessary* condition for Betty to be eating it.
>- It is not a *sufficient* condition since Betty might not eat all the apples she is given.

## “If” and “only” (3)

"Betty will eat the fruit *if and only if* it is an apple."

>- This is equivalent to: "Betty will eat the fruit $\leftrightarrow$ fruit is an apple."
>- This statement makes it clear that Betty will eat all and only those fruits that are apples.
>- She will not leave any apple uneaten, and she will not eat any other type of fruit.
>- That a given fruit is an apple is *both a necessary and a sufficient condition* for Betty to eat the fruit.^[From: https://en.wikipedia.org/wiki/If_and_only_if]

# Possibility and necessity

## How is it possible? (1)

Compare different types of impossibility here^[from: https://philosophy.hku.hk/think/meaning/possibility.php]:

- It is impossible to be a tall man without being tall.
- It is impossible to dissolve gold in pure water.
- It is impossible to travel from Hong Kong to New York in less than ten minutes.
- It is impossible to visit the doctor without an appointment.

## How is it possible? (2)

>- It is LOGICALLY impossible to be tall without being tall. There is no possible universe in which this might be the case.
>- It is EMPIRICALLY (physically, causally) impossible to dissolve gold in water. But there might be another universe, with different physics, in which this is possible.
>- To travel very fast is TECHNOLOGICALLY impossible today, but it might become possible in two hundred years. It is neither logically nor empirically (physically) impossible.
>- The last example is possible in all the above senses. It is only LEGALLY impossible (=not permitted), but one could certainly force it to make it happen.

## Different kinds of necessity

Necessary/sufficient also mean different things depending on what kind of possibility we have in mind (examples from HKU Think):

>- Having four sides is *logically necessary* for being a square.
>- Being a father is *logically sufficient* for being a parent.
>- The presence of oxygen is *causally necessary* for the proper functioning of the brain.
>- Passing current through a resistor is *causally sufficient* for the generation of heat.
>- Being an adult of over 18 years old is *legally necessary* for having the right to vote.
>- The presence of a witness is *legally necessary* for a valid marriage.
>- Other types of necessity are also possible, for example, necessity from morality, politeness, prudence and so on.

## Which kind of necessity/sufficiency to you see here?

>- A recent photograph is _____ _____ for a new ID card.
>- Eating twenty mooncakes is _____ sufficient for me to be sick.
>- Having committed a serious crime is _____ necessary for going to jail.
>- Owning a cat is _____ _____ for being a pet owner.
>- Fuel is _____ necessary for my car to drive.
>- Being a father or a mother is _____ _____ for being a parent.


## Which kind of necessity/sufficiency to you see here?

>- A recent photograph is legally necessary for a new ID card.
>- Eating twenty mooncakes is causally sufficient for me to be sick.
>- Having committed a serious crime is legally necessary for going to jail.
>- Owning a cat is logically sufficient for being a pet owner.
>- Fuel is causally necessary for my car to drive.
>- Being a father or a mother is logically necessary and sufficient for being a parent. (Conditions can be both!)


# Categorical syllogisms

## Categorical syllogisms

*Why do chickens have feathers?*

. . . 

Because they are birds and all birds have feathers.

\vspace{3ex}

. . . 

Premise 1: All birds have feathers   
Premise 2: Chickens are birds   
 --------------------------------------------  
Conclusion: Chickens have feathers.

## Categorical syllogisms

It doesn't help very much to represent this symbolically using propositional calculus:

\vspace{3ex}

All birds have feathers = p  
Chickens are birds = q  
 ---------------------------------------------  
Therefore, chickens have feathers = r

 p  
 q  
 ---  
 r

## Categorical syllogisms

 Every lion is a mammal.  
 No fish is  a mammal.  
 -----------------------------  
 No fish is a lion.

\vspace{3ex}

. . .

 All students have a student ID.  
 Some people have no student ID.  
 -----------------------------------------  
 Some people are not students.
 
## Categorical syllogisms

In categorical syllogisms we talk about the *categories* things belong to.

We can recognize them by the use of the words “every”, “all”, “some”.

## Premises and conclusion

Categorical syllogisms have two premises and a conclusion:

\vspace{3ex}

(P1) Every lion is a mammal.  
(P2) No fish is a mammal.  
 -----------------------------------  
(C) No fish is a lion.

## Middle term

The two premises are connected by a *middle term,* which appears in both, but not in the conclusion:

\vspace{3ex}

All students have a *student ID.*  
Some people have no *student ID.*  
 -----------------------------------------  
Some people are not students.

\vspace{3ex}

. . . 

Every lion is a *mammal.*  
No fish is a *mammal.*  
 ------------------------------  
No fish is a lion.

## Major and minor term

The conclusion has a *subject* and a **predicate** ( = a property which applies to the subject).

\vspace{3ex}

All students have a student ID.  
Some people have no student ID.  
 -----------------------------------------  
*Some people* are **not students.**

\vspace{3ex}

. . . 

Every lion is a mammal.  
No fish is a mammal.  
 ------------------------------  
*No fish* is **a lion.**

## Major and minor term

The conclusion has a subject and a predicate (=a property which applies to the subject).

>- The subject of the conclusion is called the *minor term* of the syllogism.
>- The predicate of the conclusion is called the **major term.**

. . .

*No fish* is **a lion.**
*Some people* are **not students.**

## Find the major, minor and middle terms!

P1: Every dog is a mammal.  
P2: Some pets are dogs.  
 ------------------------------------  
C:  Some pets are mammals.  

\vspace{3ex}

P1: Some cakes are sweet.  
P2: Some presents are cakes.  
 ------------------------------------  
C: Some presents are sweet.  

## Find the major, minor and middle terms!

**Major**, *minor*, and \underline{middle} terms:

\vspace{3ex}

P1: Every \underline{dog} is a **mammal**.  
P2: Some *pets* are \underline{dogs}.  
 ------------------------------------  
C:  Some *pets* are **mammals**.  

\vspace{3ex}

P1: Some \underline{cakes} are **sweet**.  
P2: Some *presents* are \underline{cakes}.  
 ------------------------------------  
C: Some *presents* are **sweet**.  

# Venn diagrams

## Venn diagrams

Another way of testing the validity of syllogisms are the so-called Venn diagrams.

A Venn diagram represents a syllogism graphically.

## Basic Venn diagram

\begin{venndiagram3sets}[labelA={Subject}, labelB={Pred.}, labelC={Middle}]
\end{venndiagram3sets}

We always begin with a diagram like this. Each circle represents one of: subject, predicate, middle term.

## Basic Venn diagram

\begin{venndiagram3sets}[labelA={S}, labelB={P}, labelC={M}, labelOnlyBC={x}]
\fillOnlyA
\end{venndiagram3sets}

>- If a circle is empty, we don’t know anything about it.
>- If it is shaded, then there are *no* elements in it ("S" in the picture).
>- If it contains an X, then there is at least one thing in that category.

## “All S are P”

Example: All lions are mammals.

\begin{venndiagram3sets}[labelA={S}, labelB={P}, labelC={M}]
\fillANotB
\end{venndiagram3sets}

## “Some S are P”

Example: Some cats are black.

![](graphics/09-venn-4.png)\ 

Since we don’t know where the X goes (area 1 or 2), we can just put it onto the line. Often, you can avoid this by shading the “all” areas first!

## “Some S are not P”

Example: Some cats are not black.

![](graphics/09-venn-5.png)\ 

Again: by putting the “X” onto the line, you can avoid the need for two “X.”

## “No S is P”

Example: No taxi drivers are bus drivers.

\begin{venndiagram3sets}[labelA={S}, labelB={P}, labelC={M}]
\fillACapB
\end{venndiagram3sets}


## Venn diagram principles

1. “All/no” premises result in shaded areas.
2. “Some” premises result in “x”.
3. You expect one shading or “x” per premise.
4. Therefore, an argument cannot possibly be valid if it has only “all” premises but a “some” conclusion: where would the “x” for the conclusion come from?
5. You don’t draw the conclusion. You just check for it after you have finished drawing the premises.
6. Remember: You shade what does *not* have any elements in it!
7. Don’t forget to clearly answer the question: is the argument valid, and why? (Or why not?). A diagram alone is not a sufficient answer.


## Venn diagram numbering

\begin{venndiagram3sets}[labelA={S}, labelB={P}, labelC={M}, labelOnlyA={1}, labelOnlyAB={2}, labelOnlyB={3}, labelOnlyAC={4}, labelABC={5}, labelOnlyBC={6}, labelOnlyC={7}]
\end{venndiagram3sets}

A good example answer for an exam question would be (if you cannot submit the drawing itself, e.g. online): “Areas 4 and 5 are shaded. There is an x in 6. For the conclusion to be valid, I would expect an x in 6. Since it is there, the argument is valid.” (Or something like that).


## Prove that this is correct

All lions (M) are mammals (P)  
Some rare animals (S) are lions (M)  
 -----------------------------------------------  
Some rare animals (S) are mammals (P)  

. . . 

>- In order to prove this with a Venn diagram, we would first shade the circles according to the premises.
>- Then we would see whether the resulting shading is consistent with the conclusion.
>- *Always shade “all” sentences first!*


## Prove that this is correct

\begin{venndiagram3sets}[labelA={S}, labelB={P}, labelC={M}]
\end{venndiagram3sets}

All lions (M) are mammals (P)  
Some rare animals (S) are lions (M)  
 -----------------------------------------------  
Some rare animals (S) are mammals (P)  

## Prove that this is correct

\begin{venndiagram3sets}[labelA={S}, labelB={P}, labelC={M}]
\fillCNotB
\end{venndiagram3sets}

**All lions (M) are mammals (P)**  
Some rare animals (S) are lions (M)  
 -----------------------------------------------  
Some rare animals (S) are mammals (P)  

## Prove that this is correct

\begin{venndiagram3sets}[labelA={S}, labelB={P}, labelC={M},labelABC={x}]
\fillCNotB
\end{venndiagram3sets}

All lions (M) are mammals (P)  
**Some rare animals (S) are lions (M)**  
 -----------------------------------------------  
Some rare animals (S) are mammals (P)  

## Prove that this is correct

\begin{venndiagram3sets}[labelA={S}, labelB={P}, labelC={M},labelABC={x}]
\fillCNotB
\setpostvennhook{
\draw[<-] (labelABC) -- ++(0:3cm)
node[right,text width=4cm,align=flush left]
{This area is the solution, and it does have an x (something inside) = valid!};}
\end{venndiagram3sets}

All lions (M) are mammals (P)  
Some rare animals (S) are lions (M)  
 -----------------------------------------------  
**Some rare animals (S) are mammals (P)**  

## No do it yourself!

Is this correct?

\vspace{3ex}

No fish is a mammal  
Some mammals are animals  
 -----------------------------------------  
Some animals are not fish  

## Correct?

\begin{venndiagram3sets}[labelA={S}, labelB={P}, labelC={M}, labelOnlyAC={x}]
\fillBCapC
\setpostvennhook{
\draw[<-] (labelOnlyAC) -- ++(0:3.5cm)
node[right,text width=4cm,align=flush left]
{This area is the solution, and it does have an x (something inside) = valid!};}
\end{venndiagram3sets}

No fish (P) is a mammal (M)  
Some mammals (M) are animals (S)  
 -----------------------------------------  
Some animals (S) are not fish (P)  

- First premise: shaded area.
- Second premise: "x."

## Why is the left diagram correct and not the right?

![](graphics/09-venn-11.png)\ 

"Some animals (S) are not fish (P)."

Because we know that there are elements where the “X” is, but we don’t know anything about the red area on the right outside of the area with the “X”. So we cannot assume that there will be things in that area.

## Venn diagrams

Use a Venn diagram to show that this syllogism is *incorrect!*

\vspace{3ex}

All cats are animals  
Some cats are pets  
 -----------------------  
All animals are pets

## Invalid

\begin{venndiagram3sets}[labelA={S}, labelB={P}, labelC={M},labelABC={x}]
\fillCNotA
\setpostvennhook{
\draw[<-] (labelOnlyA) -- ++(0:3.5cm)
node[right,text width=4cm,align=flush left]
{If the syllogism was correct, this area should be shaded!};}
\end{venndiagram3sets}

All cats (M) are animals (S) [shaded area]  
Some cats (M) are pets (P) ['x']  
 --------------------------------------------------  
All animals (S) are pets (P)

*The conclusion does not follow from the premises.*

## Venn diagrams for sets

>- Venn diagrams are not only useful for syllogisms.
>- They can also be used to solve mathematical problems involving sets.

. . .

A teacher is planning schedules for 30 students.  Sixteen students say they want to take French, 16 want to take Spanish, and 11 want to take Latin.  Five say they want to take both French and Latin, and of these, 3 wanted to take Spanish as well.  Five want only Latin, and 8 want only Spanish.  How many students want French only?

## Solution

\begin{venndiagram3sets}[labelA={French}, labelB={Spanish}, labelC={Latin}]
\end{venndiagram3sets}

A teacher is planning schedules for 30 students.  Sixteen students say they want to take French, 16 want to take Spanish, and 11 want to take Latin.  Five say they want to take both French and Latin, and of these, 3 wanted to take Spanish as well.  Five want only Latin, and 8 want only Spanish.  How many students want French only?

## Solution

\begin{venndiagram3sets}[labelA={French}, labelB={Spanish}, labelC={Latin}, labelABC={3}, labelOnlyAC={2}]
\end{venndiagram3sets}

A teacher is planning schedules for 30 students.  Sixteen students say they want to take French, 16 want to take Spanish, and 11 want to take Latin.  **Five say they want to take both French and Latin, and of these, 3 wanted to take Spanish as well.**  Five want only Latin, and 8 want only Spanish.  How many students want French only?

## Solution

\begin{venndiagram3sets}[labelA={French}, labelB={Spanish}, labelC={Latin}, labelABC={3}, labelOnlyAC={2}, labelOnlyC={5}]
\end{venndiagram3sets}

A teacher is planning schedules for 30 students.  Sixteen students say they want to take French, 16 want to take Spanish, and 11 want to take Latin.  Five say they want to take both French and Latin, and of these, 3 wanted to take Spanish as well.  **Five want only Latin,** and 8 want only Spanish.  How many students want French only?

## Solution

\begin{venndiagram3sets}[labelA={French}, labelB={Spanish}, labelC={Latin}, labelABC={3}, labelOnlyAC={2}, labelOnlyC={5},  labelOnlyB={8}]
\end{venndiagram3sets}

A teacher is planning schedules for 30 students.  Sixteen students say they want to take French, 16 want to take Spanish, and 11 want to take Latin.  Five say they want to take both French and Latin, and of these, 3 wanted to take Spanish as well.  Five want only Latin, and **8 want only Spanish.**  How many students want French only?

## Solution

\begin{venndiagram3sets}[labelA={French}, labelB={Spanish}, labelC={Latin}, labelABC={3}, labelOnlyAC={2}, labelOnlyC={5},  labelOnlyB={8}, labelOnlyBC={1}]
\setpostvennhook{
\draw[<-] (labelOnlyBC) -- ++(0:2.5cm)
node[right,text width=4cm,align=flush left]
{All Latin=11, we already have 5+3+2=10, missing 1.};}\end{venndiagram3sets}

A teacher is planning schedules for 30 students.  Sixteen students say they want to take French, 16 want to take Spanish, and **11 want to take Latin.**  Five say they want to take both French and Latin, and of these, 3 wanted to take Spanish as well.  Five want only Latin, and 8 want only Spanish.  How many students want French only?

## Solution

\begin{venndiagram3sets}[labelA={French}, labelB={Spanish}, labelC={Latin}, labelABC={3}, labelOnlyAC={2}, labelOnlyC={5},  labelOnlyB={8}, labelOnlyBC={1}, labelOnlyAB={4}]
\setpostvennhook{
\draw[<-] (labelOnlyAB) -- ++(0:3cm)
node[right,text width=4cm,align=flush left]
{All Spanish=16, we already have 8+3+1=12, missing 4.};}\end{venndiagram3sets}

A teacher is planning schedules for 30 students.  Sixteen students say they want to take French, **16 want to take Spanish,** and 11 want to take Latin.  Five say they want to take both French and Latin, and of these, 3 wanted to take Spanish as well.  Five want only Latin, and 8 want only Spanish.  How many students want French only?

## Solution

\begin{venndiagram3sets}[labelA={French}, labelB={Spanish}, labelC={Latin}, labelABC={3}, labelOnlyAC={2}, labelOnlyC={5},  labelOnlyB={8}, labelOnlyBC={1}, labelOnlyAB={4}, labelOnlyA={7}]
\setpostvennhook{
\draw[<-] (labelOnlyA) -- ++(0:4cm)
node[right,text width=4cm,align=flush left]
{All French=16, we already have 4+3+2=9, missing 7.};}\end{venndiagram3sets}

A teacher is planning schedules for 30 students.  **Sixteen students say they want to take French,** 16 want to take Spanish, and 11 want to take Latin.  Five say they want to take both French and Latin, and of these, 3 wanted to take Spanish as well.  Five want only Latin, and 8 want only Spanish.  How many students want French only?

## Solution

\begin{venndiagram3sets}[labelA={French}, labelB={Spanish}, labelC={Latin}, labelABC={3}, labelOnlyAC={2}, labelOnlyC={5},  labelOnlyB={8}, labelOnlyBC={1}, labelOnlyAB={4}, labelOnlyA={7}]
\setpostvennhook{
\draw[<-] (labelOnlyA) -- ++(0:4cm)
node[right,text width=4cm,align=flush left]
{All French=16, we already have 4+3+2=9, missing 7.};}\end{venndiagram3sets}

7 students want to take French only.

## Is this syllogism correct?

Make a Venn diagram to show whether the following is correct:

\vspace{2ex}

All Greeks like to be in Greece.  
All people who like to be in Greece spend their holidays there.  
 ---------------------------------------------------------------------------  
Some people who spend their holidays in Greece are Greek.

## Correct?

All Greeks (P) like to be in Greece (M).  
All people who like to be in Greece (M)  spend their holidays there (S).  
 --------------------------------------------------------------------------  
Some people who spend their holidays in Greece (S) are Greek (P).


## Correct?

\begin{venndiagram3sets}[labelA={S}, labelB={P}, labelC={M}]
\fillBNotC
\end{venndiagram3sets}

**All Greeks (P) like to be in Greece (M).**  
All people who like to be in Greece (M)  spend their holidays there (S).  
 --------------------------------------------------------------------------  
Some people who spend their holidays in Greece (S) are Greek (P).

## Correct?

\begin{venndiagram3sets}[labelA={S}, labelB={P}, labelC={M}]
\fillBNotC
\fillCNotA
\end{venndiagram3sets}

All Greeks (P) like to be in Greece (M).  
**All people who like to be in Greece (M)  spend their holidays there (S).**  
 --------------------------------------------------------------------------  
Some people who spend their holidays in Greece (S) are Greek (P).

## Correct?

\begin{venndiagram3sets}[labelA={S}, labelB={P}, labelC={M}]
\fillBNotC
\fillCNotA
\setpostvennhook{
\draw[<-] (labelABC) -- ++(0:3cm)
node[right,text width=4cm,align=flush left]
{This area would be the solution, but it does not have an 'x' inside it! So the conclusion does not follow from the premises. Invalid!};}
\end{venndiagram3sets}

All Greeks (P) like to be in Greece (M).  
All people who like to be in Greece (M)  spend their holidays there (S).  
 --------------------------------------------------------------------------  
**Some people who spend their holidays in Greece (S) are Greek (P) ???**


## Correct?

All Greeks (P) like to be in Greece (M).  
All people who like to be in Greece (M)  spend their holidays there (S).  
 --------------------------------------------------------------------------  
Some people who spend their holidays in Greece (S) are Greek (P).

\vspace{3ex}

- The root of the problem is that there might not be any Greeks at all.
- If we had an additional premise "There are Greeks," this would put an 'x' into the central area and then the syllogism would be correct.


## Show whether this is correct

All vertebrates reproduce sexually.  
All vertebrates are animals.  
 ------------------------------------------  
Conclusion: All animals reproduce sexually.

\vspace{3ex}

*Draw a Venn diagram to show whether this syllogism is valid!*


## Show whether this is correct

\begin{venndiagram3sets}[labelA={S}, labelB={P}, labelC={M}]
\fillCNotB
\fillCNotA
\setpostvennhook{
\draw[<-] (labelOnlyA) -- ++(0:4cm)
node[right,text width=4cm,align=flush left]
{If the conclusion was correct, then this area should be shaded (=empty). It is not, so the argument is not valid.};}
\end{venndiagram3sets}

All vertebrates (M) reproduce sexually (P).  
All vertebrates (M) are animals (S).  
 ------------------------------------------------------  
Conclusion: All animals (S) reproduce sexually (P).

## What did we learn today?

- Necessary and sufficient conditions
- Categorical syllogisms
- What can go wrong with them
- How Venn diagrams help us analyze syllogisms

## Any questions?

![](graphics/questions.jpg)\ 

## Thank you for your attention!

Email: <matthias@ln.edu.hk>

If you have questions, please come to my office hours (see course outline).
