% Critical Thinking
% 8. Arguments and validity in propositional logic
% A. Matthias

# Where we are

## What did we learn last time?
- More conditional arguments
- Necessary and sufficient
- Types of necessity
- Categorical syllogisms
- Venn diagrams

## What are we going to learn today?
- Validity in propositional logic.
- Natural deduction.

# Validity of arguments

## Validity of arguments
How do I know whether an argument is valid?

> - For example, how can I prove that “affirming the antecedent” is valid?
> - Definition of “valid”:
> - *An argument is valid if its conclusion is always true when its premises are true.*
>- (That is, when true premises always lead to a true conclusion.)

## Proof of validity in propositional logic
> - Is “affirming the antecedent” valid?
> - Means: “Is the conclusion always true when the premises are true?”
> - First, write it down formally:
> - Premise 1: P$\to$Q, Premise 2: P, Conclusion: $\vdash$ Q

## Proof of validity in propositional logic
> - Second, make a truth table which contains both premises and conclusion:

![](tt13a.png)\ 

> - If there is at least one line where the premises are true and the conclusion is false, then the argument is *invalid!*
> - The argument shown here is valid (green row in table)

## Prove the validity! (5 min)
Prove that “affirming the consequent” is invalid using a truth table!

## Affirming the consequent:

>- P$\to$Q, Q, therefore P.     

![](tt13b.png)\ 

>- In the second line of the table we can see that although the premises are true, the conclusion is false (invalid)!

## $\vdash$  and $\vDash$
The symbol “$\vdash$” (“turnstile”) technically means “sequent,” meaning that the expression on the right is supposed to follow from the expressions on the left. But this could be wrong.

The symbol “$\vDash$” means that the expression on the right *is known to or has been proven* to follow from the expressions on the left. The sequent is therefore valid.

e.g.

- Affirming the antecedent: P$\to$Q, P $\vDash$ Q

## Entailment (1)

If we have one logical expression B (conclusion) and a set of logical expressions A~1~...A~n~ (premises), then we say that:

A~1~...A~n~ “entail” B if and only if there is no assignment of truth-values under which A~1~...A~n~ (the premises) are true and B (the conclusion) is false.

- This is equivalent to saying that B follows logically from A~1~...A~n~.
- In a valid argument the premises entail the conclusion.

## Entailment (2)

“Entailment” is just another way of saying that a conclusion logically follows from the premises of an argument. So all these statements say the same thing:

- “B follows logically from A.”
- “If A is true, then B must be true.”
- “A entails B.”
- “An argument with the premise A and the conclusion B is valid.”
- “A $\vdash$ B.”


## Entailment (3)

For example:

\begin{tabular}{|c|c||c|}
\hline
$ P $ & $ Q $ & $ (P \vee Q) $ \\
\hline
F & F & F \\
\hline
F & T & T \\
\hline
T & F & T \\
\hline
T & T & T \\
\hline
\end{tabular}

- P entails P$\lor$Q, because whenever P is true, P$\lor$Q will also be true.
- P$\lor$Q does *not* entail P, because when P$\lor$Q is true, P is not necessarily true (it can be false, and Q can be true).

## Exercise

Are the following true? Use a truth table to decide.

1. P entails P&Q
2. P&Q entails P
3. P&Q $\vdash$ PvQ
4. PvQ $\vdash$ P&Q
5. P & ~P $\vdash$ Q

Pay particular attention to the last one! (Explained in more detail in the next session).


## Common valid argument patterns
>1. Affirming the antecedent: \f{P$\to$Q, P $\vDash$ Q}
>2. Denying the consequent: \f{P$\to$Q, $\sim$Q $\vDash$ $\sim$P}
>3. Hypothetical syllogism: \f{A$\to$B, B$\to$C $\vDash$ A$\to$C}    
Example:      
If I get up late (A), I’ll miss the class (B)      
If I miss the class (B), I’ll get a bad grade (C)     
(Conclusion:) If I get up late (A), I’ll get a bad grade (C)

## Common valid argument patterns

4. Disjunctive syllogism:    
\f{PvQ, $\sim$P $\vDash$ Q}     
\f{PvQ, $\sim$Q $\vDash$ P}

## Common valid argument patterns

5. Dilemma:      
\f{P$\to$Q, R$\to$S, PvR $\vDash$ QvS}

Example:

If I go shopping, I will spend a lot of money     
If I stay at home, I will be bored     
I have to go shopping or stay at home    
-----------------------------------------------------     
I will spend a lot of money or I will be bored 

## Common valid argument patterns

6. Conjunction-In:     
\f{P, Q $\vDash$ P$\&$Q}

7. Conjunction-Out:     
\f{P$\&$Q $\vDash$ P}    
\f{P$\&$Q $\vDash$ Q}    

## Common valid argument patterns
8. Double negation: \f{P $\equiv$ $\sim$$\sim$P}

“$\equiv$” means “equivalent”. Two expressions are equivalent if they have exactly the same truth values.

## Common valid argument patterns

9. De Morgan’s laws:    
\f{$\sim$(PvQ) $\equiv$ $\sim$P$\&$$\sim$Q}    
\f{$\sim$(P$\&$Q) $\equiv$ $\sim$Pv$\sim$Q}

*(Remember the soup without chicken meat or fish balls!)*

## Common valid argument patterns

10. Contraposition:    
\f{P$\to$Q $\equiv$ $\sim$Q$\to$$\sim$P}    
(denying the consequent)

## Common valid argument patterns

11. Conditional-disjunction exchange:    
\f{P$\to$Q $\equiv$ $\sim$PvQ}

Example:

“If it rains, the street will be wet” is equivalent to:     
“It has not rained or the street is wet.”

You can easily show that these are equivalent using a truth table! (Left as an exercise to the reader)

## Common valid argument patterns (review)
1. Affirming the antecedent: P$\to$Q, P $\vDash$ Q
2. Denying the consequent: P$\to$Q, \~Q $\vDash$ \~P
3. Hypothetical syllogism: A$\to$B, B$\to$C $\vDash$ A$\to$C
4. Disjunctive syllogism: PvQ, \~P $\vDash$ Q and PvQ, \~Q $\vDash$ P
5. Dilemma: P$\to$Q, R$\to$S, PvR $\vDash$ QvS
6. Conjunction-In: P, Q $\vDash$ P$\&$Q
7. Conjunction-Out: P$\&$Q $\vDash$ P and P$\&$Q $\vDash$ Q
8. Double negation: P $\equiv$ \~\~P
9. De Morgan’s laws \~(PvQ) $\equiv$ \~P$\&$\~Q and \~(P$\&$Q) $\equiv$ \~Pv\~Q
10. Contraposition P$\to$Q $\equiv$ \~Q$\to$\~P (denying the consequent)
11. Conditional-disjunction exchange P$\to$Q $\equiv$ \~PvQ

# Natural deduction

## Natural deduction

When we show the validity of statements by deriving the conclusion from the premises using the equivalences above, this is called “natural deduction” (because it follows the way we reason “naturally”).

## Show the validity! (3 min)

If coffee was healthier than tea, then all people would be drinking coffee.     
If all people were drinking coffee, then coffee would be expensive.     
Coffee is not expensive.     
 -----------------------------------------------------------     
Therefore, coffee is not healthier than tea.

## Step 1: Translate into propositional logic     
C: Coffee healthier than tea    
D: All people drink coffee    
E: Coffee is expensive    

C $\to$ D    
D $\to$ E    
\~E   
 ------------     
\~C

## Step 2: Find valid patterns and equivalences

C $\to$ D     
D $\to$ E     
\~E   
 ------------     
\~C

1. From C$\to$D and D$\to$E follows C$\to$E (hypoth. syll.)
2. Since \~E (3), we know that \~C (deny consequent)

## Another solution:

C $\to$ D     
D $\to$ E     
\~E     
 ------------     
\~C

1. From \~E and premise (2) we know that \~D (denying consequent)
2. From \~D and (1) we know that \~C (denying consequent)

You can also show the validity of this with a truth table! (Exercise for the reader).


## Natural deduction

In natural deduction, we use a particular set of rules and symbols for these rules:

- from A follows that A: **R** (reiteration)
- Conjunction-out: **&O**
- Conjunction-in: **&I**
- Disjunction-In (A$\vDash$AvB): **vI**
- Aff. antecedent (“modus ponens”): **MP**
- Deny consequent (“modus tollens”): **MT**
- Hypothetical syllogism: **HS**
- Disjunctive syllogism: **DS**
- Double negation: **DN**
- DeMorgan’s laws: **DeM**
- Contraposition: **Con**

## Natural deduction
In the formalism of natural deduction, you would write the same thing in a more formal way:

> 1. Each line is numbered
> 2. In the first column you write the result of each step
> 3. In the second column you write the numbers of the lines you use to derive that conclusion, plus the short form of the name of the rule you use
> 4. Repeat until the left column contains the desired conclusion

## Natural deduction form

------------------------------------------
C $\to$ D, D $\to$ E, \~E $\vdash$ \~C
------------------------- ----------------
1. \~E                    premise

2. D$\to$E                premise

3. \~D                    1, 2, MT

4. C$\to$D                premise

5. \~C                    3, 4, MT
------------------------------------------

## Show that this is valid (5 min)
(P$\&$Q$\to$R), Q$\to$P, Q $\vdash$ R

Try to write down your result in the formalism of natural deduction!

## (P$\&$Q$\to$R), Q$\to$P, Q $\vdash$ R

Informal notes:

> - P $\&$ Q $\to$ R (premise)
> - Q $\to$ P (premise)
> - Q (premise)
> - From Q $\to$ P and Q follows that P (aff. ant.)
> - From P and Q follows that P$\&$Q (conj. in)
> - From P$\&$Q and the first premise follows R (aff. ant.)

## (P$\&$Q$\to$R), Q$\to$P, Q $\vdash$ R

Natural deduction:

----------------------------------------------
(P$\&$Q$\to$R), Q$\to$P, Q    $\vdash$ R
----------------------------- ----------------
1. P$\&$Q$\to$R               premise

2. Q$\to$P                    premise

3. Q                          premise

4. P                          2, 3, MP

5. P$\&$Q                     4, 3, $\&$I

6. R                          5, 1, MP
----------------------------------------------

## Show the validity! (5 min)
Show the validity of this argument, by deriving it from the valid argument forms shown before:

P v \~Q v R     
R $\to$ \~S     
S$\&$Q     
 -----------------     
P

## Pv\~QvR, R$\to$\~S, S$\&$Q $\vdash$ P

1. From S$\&$Q follows: S and Q (by conjunction-out).
2. S is equivalent to \~\~S (by double negation).
3. From \~\~S and the premise R$\to$\~S follows \~R (deny consequent).
4. From \~R and the premise Pv\~QvR follows Pv\~Q (by disjunctive syllogism).
5. Q is equivalent to \~\~Q (by double negation).
6. From \~\~Q and Pv\~Q follows P, the desired conclusion (by disjunctive syllogism).

## Pv\~QvR, R$\to$\~S, S$\&$Q $\vdash$ P
--------------------------------------- ----------------
1. S$\&$Q                               premise
2. S                                    1, \&O
3. Q                                    1, \&O
4. \~\~S                                2, DN
5. R$\to$\~S                            premise
6. \~R                                  4, 5, MT
7. Pv\~QvR                              premise
8. Pv\~Q                                6, 7, DS
9. \~\~Q                                3, DN
10. P                                   9, 8, DS
--------------------------------------- ----------------


## What did we learn today?
- Validity in propositional logic
- Natural deduction

## References
HKU OpenCourseWare on critical thinking, logic, and creativity:

http://philosophy.hku.hk/think/pl

(Modules Q04, Q05)

## Any questions?

![](graphics/questions.jpg)\ 

## Thank you for your attention!

Email: <matthias@ln.edu.hk>

If you have questions, please come to my office hours (see course outline).





