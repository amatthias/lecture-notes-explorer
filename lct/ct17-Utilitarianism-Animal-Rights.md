% Critical Thinking
% 17. Debate 1: Human and animal rights
% A. Matthias

# Where we are

## What did we learn last time?

- Numbers, probabilities, utility

## What are we going to learn today?

- How to brainstorm a debate topic


# Utilitarianism

## Bentham, Mill, and their times

The two main historical proponents of utilitarianism are:

- Jeremy BENTHAM (1748-1832)
- John Stuart MILL (1806-1873)

. . .

Note:

- Bentham was about sixty years older than Mill, or about two generations. Bentham could have been Mill's grandfather.
- One of the newest moral theories (only about 200 years old).

## Bentham, Mill, and their times (1)
Around the same time, we have

- the American Declaration of Independence (1776),
- the French Revolution (1789), and
- the Industrial Revolution (around 1760 to 1840).

## Bentham, Mill, and their times (2)

All these movements share common basic ideas with Bentham's utilitarianism:

- American Declaration of Independence (1776): Freedom and democracy,
- French Revolution: The equality of all human beings, equal rights for all,
- Industrial Revolution: The belief that science and engineering can be fruitfully applied to human matters and problems of ethics (see: Jules Verne, born 1828), and that science and technology are *beneficial* for society.

. . .

We will understand utilitarianism better if we keep these ideas and the historical origins of the theory in mind.

## Bentham's Utilitarianism: Utility

### Utility is:

1. *Pleasure* *over* *pain*, *happiness* over unhappiness.
2. The greatest amount of happiness for the *greatest number* (of people).

### Notes:

1. What does "over" in "pleasure over pain" mean exactly?
2. What is the significance of talking about the "greatest number"?
3. What is the relation of happiness to pleasure and pain in this quote?

# Animal rights

## How to deal with an applied question

*Not necessarily in that order:*

- Understand the problem
- Identify the stakeholders
- Make relevant distinctions and clarify the issues
- Find and quote relevant theory (from research)
- Find the main arguments for each side (from research)
- Find places where more research (facts) is needed
- Identify benefits and costs and weigh them against each other
- Avoid yes/no answers. Try to see the complexities of the problem
- Try to find original alternatives that solve the problem better than just saying that one side is right (for example: meat eating might be morally right if animals are treated well, if we reduce the amount of meat consumed, if we eat only wild animals or insects etc)

## Rachels on animal rights: I 7.4, II Ch. 14-16

## Understand the problem

Animal rights:

- Which rights? (not: voting, freedom of religion, freedom of speech; but: right to live in a natural way, to mate, to have children, to seek happiness in a way appropriate to that animal, to avoid torture and pain)
- Which animals? (higher/lower? harmful/harmless? pain perceiving/not pain perceiving? Endangered?)

. . .

Further questions:

- If I'm attacked by a shark, I can defend myself by harming the shark. Does this give me the right to kill or torture all sharks in the ocean? (No, my response should be limited to preventing actual harm to myself!)

## What is a right? (1)

*In utilitarianism, one would be granted a right if and only if having this right would increase the happiness of all.*

We can ask:

- Is this the case for (some? all?) animal rights?
- Does the happiness of "all" include animals or do we look only at human happiness?

### Mill:
"To have a right, then, is, I conceive, to have something which society ought to defend me in the possession of. If the objector goes on to ask, why it ought? I can give him no other reason than general utility."

## What is a right? (2)

This way of explaining rights is great, because we can easily apply it to any creature, and we will get a sensible estimate of its possible relevant rights:

- Humans have the right to vote and to free speech because taking away these rights would reduce general utility.
- Pigs don’t need the right to vote, because taking away that right would *not* reduce utility for the pigs.
- On the other hand, pigs *should* have the right to live in a natural environment, because being raised in a cage (like in a meat factory) likely reduces the pigs’ happiness.
- Mosquitoes don’t need any rights, because they don’t show any indication of experiencing happiness at all.


## Identify the stakeholders

The stakeholders are those who are (strongly) affected by that moral problem.

- Animals
- The government (has to pass and enforce laws and regulations, has to finance infrastructure etc)
- Animal farmers
- Meat consumers
- Scientists
- Consumers of cosmetics
- Producers of cosmetics
- Doctors and patients waiting for medical research results
- Medicine and biology students

## Stakeholder errors (1)

- Be careful with the stakeholders!
- A common mistake is to see them as distinct groups, and to weigh them against each other!

. . .

For example:

- There are 1000 animal farmers.
- There are one million meat consumers.
- Therefore, meat eating is good for utilitarianism, and animal suffering has to be tolerated.

. . .

There are multiple mistakes in this thought (for example it ignores the animals), but one often overlooked is that the animal farmers and the meat eaters probably share views about cruelty to animals. Most meat eaters don't want animals to suffer either!

## Stakeholder errors (2)

The same is true of many other areas.

- Car drivers don't want the environment to be destroyed. They are also interested in clean air.
- Even criminals have an interest in a safe society (from which they profit themselves as citizens)
- Even people who are in favour of euthanasia don't want euthanasia to be abused.
- People who are in favour of abortion still love babies and are not interested in devaluing human life.

## Stakeholder errors (3)

*So you have to be aware that your stakeholder divisions are useful in order to understand the problem, but they cannot be used in order to estimate the size of population groups with opposite moral views! Different stakeholders don't necessarily have opposite moral views!*

## Stakeholder errors (4)

Another mistake is to count everyone who might be involved in a case as a stakeholder. This is wrong.

A stakeholder is someone whose *interests* are *significantly* affected by that moral issue.

## Stakeholder errors (5)

For example, if you bribe your teacher to get a good grade, the stakeholders would be:

- Your teacher
- You
- Perhaps the university, because if this case became known, then the reputation of the university would suffer.

. . .

On the other hand, the police and the courts, although they would definitely be involved in the case itself, are *not* stakeholders. They have no own interests in how this case plays out. Whether you bribe someone or not, and whether this becomes public or not, and whether you get a particular grade as a result of that or not, does not *affect the police's or the court's own interests.* Therefore, they are not stakeholders.


## Theory: Animals don't need rights

Possible reasons to ignore animal rights:

1. Human beings have superior value. Animals just don't count. We can do with them what we want.
    - Supporting theory? (Christian Natural Law theory, Thomas Aquinas)
    - Problems? (Not convincing to non-Christians)
2. Social Darwinism.

### Thomas Aquinas (1225-1274)

"[Animals] by the divine providence they are intended for man's use in the natural order. Hence it is not wrong for man to make use of them, either by killing them or in any other way whatever."

## Is-Ought-Fallacy

>- If we subscribe to Darwinism, we might say: “Animals don’t need rights, because in nature they don’t have rights either. Animals kill and eat other animals all the time. So we don’t need to protect animals’ rights.”
>- Is this a good argument?
>- No. This is called the Is-Ought-Fallacy. 
>     - From the fact that something *is* the case, I cannot conclude that it is morally right (that it *ought* to be so). 
>     - For example, there are criminals in the world. From this I cannot conclude that it is morally right that there are criminals in the world!
>- This is a counterargument to social Darwinism: the idea, that it is right that a society is organised like nature is, and that it is good that the strong kill the weak.

## Theory: Other reasons to ignore animal rights

- Animals are not rational
- Animals don't speak
- Animals are not human
- Animals don't have autonomy (Kant: "Autonomy" is the ability of someone to freely make their own decisions and to act accordingly^[see also Rachels, p.3 for a similar definition]). Animals are driven by instinct and not free to decide how they will act

## Counter-arguments to ignoring animal rights

- Some humans are not rational either (babies)
- Babies don't speak. Mute people don't speak
- Some animals do "speak" in a limited way (whales? bees!) -- Research!
- Why should "being human" be important for utilitarianism? (Peter Singer: "speciecism"!)
- Why should autonomy be a requirement for having rights? (Needs at least to be explained further! -- Research!)

## Theory: Utilitarianism

The only thing that counts is the balance of pleasure and pain.

- Can an animal feel pleasure?
- Can it feel pain?

### Bentham:

"A full-grown horse or a dog is ... a more rational animal than a (human) infant of a day or a week ... old. ... The question is not, can [animals] *reason?* Can they *talk?* But: can they *suffer?*"

## Distinctions: Kinds of animals

- We have to distinguish between different *kinds* of animals!
- Can all animals "suffer" equally?
- What makes an animal "suffer"? (imprisonment, lack of company, physical pain, mistreatment, fear?)
- Do all animals experience pain? Do they have the neural infrastructure required?

. . .

What abilities does the animal (not) have that are relevant?

- Anticipate future events
- Understand intentions
- Value its own future (animals care only about the present)

## Are animals scared of death?

- Why are humans scared of death?
- If person X is told that he is going to be killed tomorrow morning, *why exactly* would this make him unhappy?
	 - Pain in the process of dying
     - Loss of future happiness, cutting off of future plans
	 - We don’t want our life or death to be decided by others
	 - Fear of what happens *after* death (hell etc)
	 - One’s death might make others unhappy or cause pain to others
- Is this the same with animals?
     - One might argue that only the pain is relevant to animals’ experience of death.
- If not, then perhaps death itself is not a negative experience for an animal. It is the *circumstances* of death that are unpleasant. (This is also a factor of human fear of death).

## Weighing happiness against pain (1)

- Human gain in happiness from meat eating

. . .

- Pain for pigs from being held in life-long captivity under terrible conditions
- Pain for pigs from being unable to procreate, hunt, or follow any of their natural instincts
- Fear induced by watching other pigs die in front of them
- Painful method of killing?

## Weighing happiness against pain (2)

Since twe are weighing two sides against each other, we could either stop eating meat, or try to remove the pain for the animals. Note: all these causes of animal pain *could,* in principle, be removed!

- But meat then would become very expensive, and much less would be eaten.
- Both ecologically and from a health perspective this would probably be good.
- Additional reasons to not eat meat.

## Weighing happiness against pain (3)

- The weighing would turn out differently if we talk about medical uses of animals, cosmetics, etc.
- Even medical uses can evaluate differently, depending on circumstances.

## Distinctions: Importance of animal uses

Are all animal uses of equal importance?

- Medical experiments for life-saving drugs
- Experiments for development of cosmetics
- Eating meat
- Ocean Park, zoos, circus?

## Alternatives: Ways to avoid animal suffering

Are there alternatives to animal suffering?

Can we reconcile the interests of humans and animals?

- Lab tests on cell cultures
- Use of computer simulation for anatomy teaching
- Use of techniques to reduce suffering (pain killers)
- Production of meat that reduces suffering (natural farming)

## Conclusion: Animal vs human interests in utilitarianism

- We have to weigh animal interests against human gain
- We cannot put either side's interests absolutely above the other's
- (Some) animals deserve serious consideration, but not necessarily absolute protection (if the human benefit is much higher than the animal's pain)

## Animals rights (graphics)

![](ShouldAnimalsHaveRights.png)

## Conclusion about creating your arguments / writing a paper

- Think through the different aspects of the problem
- Show that you thought about the different aspects, but then pick a small part of the question that you can answer well for the main part of your argument or paper
- Avoid yes/no answers. Appreciate the complexity of the problem
- Do your research, both about theory and about facts
- Use your research, and quote it correctly if you write a paper
- Try to find original solutions to the problem that go beyond saying that one side is right or wrong


## Any questions?

![](graphics/questions.jpg)\ 

## Thank you for your attention!

Email: <matthias@ln.edu.hk>

If you have questions, please come to my office hours (see course outline).
