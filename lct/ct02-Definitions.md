% Critical Thinking
% 2. Definitions
% A. Matthias

# Where we are

## What did we learn last time?

- Course outline and formalities

## What are we going to learn today?

- What is a definition?
- What is a correct definition?
- Descriptive and stipulative definitions
- Intensional and extensional definitions
- Descriptive (reportive), stipulative and precising definitions
- Intensional and extensional definitions
- Genus and difference
- Circular definitions
- Common definition errors

# Definitions

## Definitions (\cn{定義})

1. What does “definition” mean?
2. Define the following: knife, elephant, kitchen, table, air.

## Definitions must be precise

- You should try to be as precise as possible.
- A definition should ideally include only one object: that, which you wish to define.
- It should exclude everything else

## Is this a good definition?

“A pen is something which writes.”

## Is this a good definition?

“A pen is something which writes.”

No: it is too general (also applies to pencils and brushes, and to my little brother who is writing a letter to his friend.)

## Is this a good definition?

“A pen is an instrument for writing, which has a hard tip through which ink is applied onto the paper.”

## Is this a good definition?

“A pen is an instrument for writing, which has a hard tip through which ink is applied onto the paper.”

>- Good: “instrument for writing” excludes my little brother.
>- Good: “hard tip” excludes brushes.
>- Good: “ink” excludes pencils.
>- Good: “ink is applied onto the paper” includes both ball pens and fountain pens.

## No definition is perfect

>- You can almost always find flaws in definitions.
>- This is okay, as long as the definition fulfills its purpose.
>- *What is the purpose of a definition?*

. . .

The most common purpose of a definition is to make sure that two parties agree on the meanings of the words they use.

If the definition can achieve that, it is good enough.

# Kinds of definitions

## Descriptive definitions

>- *Descriptive definitions* (\cn{描述性定義}) describe how a word is used.
>- They can be right or wrong.

. . .

Right descriptive definition: “A pen is an instrument for writing, which has a hard tip through which ink is applied onto the paper.”

Wrong descriptive definition: “A pen is a bird with red feathers.”

## Descriptive and stipulative definitions

>- Descriptive definitions (\cn{描述性定義}) describe how a word is used. *They can be right or wrong.*
>- Stipulative definitions (\cn{規約性定義}) introduce a new word to the language, or a new meaning for a known word. *Therefore, they cannot be wrong.*

## Stipulative definition (1)

“When I say ‘trair’ I want the word to mean ‘three-legged chair’.”

(This can never be wrong)

## Stipulative definition (2)

“I just invented a new game. I will call it Greek Chess.”

(This can never be wrong)

## Stipulative definitions are common

For example when culture or technology advances: “astronaut”, “microwave”, “PDA”, “mobile phone”, “bungee-jumping”, “blu-ray disc” have all been relatively recently defined.

## Which definitions are descriptive, which stipulative?

- A chair is a piece of furniture with a straight back, for sitting upon.
- Let's call the new star, that was discovered yesterday, “Ferry-Star.”
- “James” is the name that I will give to my first son.
- “Water” we call the substance which in Chemistry is known as H$_2$O.

## Which definitions are descriptive, which stipulative?

- A chair is a piece of furniture with a straight back, for sitting upon. (descriptive)
- Let's call the new star, that was discovered yesterday, “Ferry-Star.” (stipulative)
- “James” is the name that I will give to my first son. (stipulative)
- “Water” we call the substance which in Chemistry is known as H$_2$O (descriptive)

## Intensional and extensional definitions

What is the difference between the following two?

- “An astronaut is a person trained to fly to space”
- “Astronauts are people like Neil Armstrong, Edwin Aldrin and Yang Li Wei.”

## Intensional definitions (\cn{內涵定義})

An *intensional definition* specifies the *conditions for something to be a member of a specific set.*

>- What does this mean? What are the conditions for something to be a member of the set “astronaut?”
>- It must be a human
>- It must be trained to fly to space
>- Thus: “Astronauts” are people who are trained to fly to space.

## Extensional definitions (\cn{外延定義})

An *extensional definition* just lists all or some members of the set to be defined.

>- “Vehicles are cars, trucks, and buses.”
>- “Fruit are apples, bananas and mangoes.”

## Now do it yourself

Define “vehicle,” “Chinese cooking” and “fruit” intensionally!

## Intensional definitions

>- “A vehicle is a self-moving container used for transporting people or goods, esp. on land.”
>- “Chinese cooking is a method of preparation of food which has originated and is typically used in the PRC, Taiwan, Hong Kong and Singapore.” (details depend on whether you want to define “Chinese” or “cooking”!)
>- “Fruit are usually sweet and fleshy parts of plants that contain seeds and are edible by humans, usually raw.”


## Reportive definitions

*Reportive definition* is another word for what we called *descriptive* definition above. It reports (or describes) how a word is used.


## Precising definitions

A *precising definition* might be regarded as a combination of reportive and stipulative definition.

The aim of a precising definition is to make the meaning of a term more precise for some purpose (although we know what the term generally means).

. . . 

For example, a bus company might want to give discounts to old people. But simply declaring that old people can get discounts will lead to many disputes since it is not clear how old should one be in order to be an old person. So one might define “old person” to mean any person of age 65 or above. This is of course one among many possible definitions of “old.” (From: HKU Critical Thinking Web)

. . . 

Another example: Lingnan university administration: “A full-time student is a student who takes at least three courses each semester.”

## Different types of definitions

What is correct as one type of definition, need not be correct as another. Example:

“A full-time student is a student who takes at least 2 courses each term.”

>- This is wrong (presently) as a reportive definition (because full-time students need to take 3 courses).
>- It would be correct as a precising definition issued by the university administration, though.

## Which definitions are descriptive, which stipulative, which precising?

1. A telephone is an electronic device used to transmit voice over a cable.
2. “Jane” is a woman who is my wife.
3. “Andy tea” is a drink I made up, that is composed of tea, lemonade, and fish sauce.
4. Under the terms of this regulation, a "Chinese" person is a person who holds a Chinese passport.
5. “Fido” is the name that I will give to my new dog.

## Descriptive (D), stipulative (S), precising (P):

1. D: A telephone is an electronic device used to transmit voice over a cable.
2. D: “Jane” is a woman who is my wife (I cannot rename her!)
3. S: “Andy tea” is a drink I made up, that is composed of tea, lemonade, and fish sauce.
4. P: Under the terms of this regulation, a "Chinese" person is a person who holds a Chinese passport.
5. S: “Fido” is the name that I will give to my new dog.

## Intensional or extensional?

1. “Drinks are tea, coffee and lemonade”
2. “A bicycle is a vehicle with two wheels that is powered by its driver”
3. “Educational institutions are primary schools, secondary schools and universities”
4. “A truck is a big car that is used for transporting goods”

## Intensional (I) or extensional (E)?

1. E: “Drinks are tea, coffee and lemonade”
2. I: “A bicycle is a vehicle with two wheels that is powered by its driver”
3. E: “Educational institutions are primary schools, secondary schools and universities”
4. I: “A truck is a big car that is used for transporting goods”

# Comparing definitions

## Compare definitions

Define first extensionally and then intensionally:

- Major subject
- Science
- Plant
- Vegetable

## Intensional vs. extensional

>- Extensional definitions are easier to give.
>- You just need some examples.
>- But they are less precise.
>- Sometimes they can be very confusing.

## Confusing extensional definitions

*In Greek, “διάσημος” means people like, for example, Donald Trump, Boris Johnson and Vladimir Putin.*

- What did I define here?

. . . 

>- Politician?
>- Bad politician who doesn’t care about democracy?
>- Man?
>- Person who can speak English?
> Actually, the word means “famous”.

## Intensional vs. extensional

>- Intensional definitions are harder to give.
>- You need to think about the properties of things and how different sets of properties relate to each other.
>- But they are more precise.
>- Usually they are very clear.
>- This is why they are primarily used in science.

## Kinds of definitions

There is no connection between the two pairs of concepts!

- *Intensional and descriptive:* A chair is a piece of furniture, which is used for sitting upon and has ...
- *Intensional and stipulative:* I call “Greek Chess” a game which has the following rules ...
- *Extensional and descriptive:* A fruit is something like a banana, an apple or an orange.
- *Extensional and stipulative:* Greek Chess is something like Chess, Chinese Chess, or Checkers.

## Some more examples

- “A table is a piece of furniture which is used for placing things upon”
- “Air is the gas which surrounds the planet Earth”
- “A mobile phone is a telephone which can be carried around and used on the way”

# Genus and difference

## Genus (\cn{類}) and difference (\cn{種差})

These definitions are called definitions by “genus” (family) and difference.

## Genus

The *genus* is the family of things something belongs to.

Examples of genus:

>- “Table” is of the genus “piece of furniture”
>- “Air” is of the genus “gas”
>- “Mobile phone” is of the genus “phone”

## Genus

Name the genus of:

- Elephant
- Computer
- University
- Student
- Rice

## Genus

- Elephant, gen.: mammal (or animal)
- Computer, gen: electronic device
- University, gen: educational institution
- Student, gen: human or occupation
- Rice, gen: plant or dish

## One thing can belong to multiple genera

- E.g. “rice” can be a dish or a plant.
- A “student” can be a human or an occupation.

As what we see it, depends on what properties we want to emphasise.

*There is no “right” genus in these cases.*

## Difference

The difference is *what makes this particular thing different from the other members of its genus.*

In a definition, it often appears after the word “which”: “a spaceship is a vehicle, which flies into space.”

## Genus and difference

So the idea is that we say to which group (genus) something belongs to, and then what makes it different from all the other members of that group.

In this way we can define one single thing.

The narrower the genus, the easier it is to specify a good difference. Therefore, the genus in a definition should be as narrow as possible!

So, "mammal" is a better genus than "animal." "Electronic device" is better than "machine." Any concrete class of things is better than just "thing."

## Difference

Now find the difference in order to complete the following definitions:

- Elephant, gen.: mammal
- Computer, gen: electronic device
- University, gen: educational institution
- Student, gen: human
- Rice, gen: plant or dish

## Examples of difference

- Elephant, gen.: mammal. Diff: big, has long nose.
- Computer, gen: electronic device. Diff: is programmable.
- University, gen: educational institution. Diff: the highest.
- Student, gen: human. Diff: being educated at an educational institution.
- Rice, gen: plant or dish.
    - Diff (plant): monocarpic annual, growing in such-and such conditions ...
    - Diff (dish): is a staple food in south-east Asia.

## Now try it yourself!

Give genus-difference definitions for the following:

- Cinema
- Ship
- Powerpoint
- Apple
- Cantonese pop music

## Now try it yourself!

Give genus-difference definitions for the following:

>- Cinema: a hall where people buy a ticket to watch movies together.
>- Ship: a vessel which floats on the surface of water.
>- Powerpoint: a computer program for making presentations.
>- Apple:
    - a fruit that is red or green and sweet (?)
    - Or: the fruit of the apple-tree (?)
    - Or: the computer company which makes the iPhone
    - Or: the fruit that inspired Newton to think about gravitation
>- Cantonese pop music: Here it depends on what you want to define. Are you defining "Cantonese," or "pop music," or "music"? Take care not to make a circular definition!

# Problems with definitions

## Circular definitions

1. In one definition: “A cook is someone who cooks.”

2. In two related definitions:
    - “Apple: the fruit of the apple-tree.”
    - “Apple-tree: a tree which produces apples.”

## Circular definitions (2)

*Circular definitions don't define anything!*

. . .

To see this, just replace the word being defined by something meaningless.

“A tagaf is someone who tagafs.”

Can you now say what a “tagaf” is?

## Circular definitions (3)

Or:

1. Tagaf: the fruit of the tagaf-tree.
2. Tagaf-tree: a tree which produces tagafs.

Can you now say what a “tagaf” is? -- No.

It could be anything: apples, bananas, oranges.

## Circular definitions (4)

Now make the same test with a correct definition:

Tagaf: a hall where people have to buy a ticket in order to watch movies together.

You see that you can still understand what is being defined!

## Test for definitions

You can test whether a definition is a good one, by replacing the word which is being defined by a meaningless word. 

If you can still understand what is being defined, then the definition is good.

## Another example

>- A tagaf is something which is a lot of fun and excitement.
>- (Bad definition, we don't know what exactly it is!)

. . .

But:

>- A tagaf is a red, round, soft vegetable, used to make ketchup.
>- (Good definition: we know it's a tomato!)

## Persuasive definitions

Definitions are not always objective.

Persuasive definitions are designed to transfer emotions, such as feelings of approval or disapproval.

Compare:

1. Greece: a South-European country located between Italy and Turkey.
2. Greece: a South-European country that was home to a great ancient civilisation.

## Persuasive definitions

Compare:

1. Homosexual: somebody who is attracted by the same sex.
2. Homosexual: somebody who has an unnatural desire for those of the same sex.

## Persuasive definitions

Compare:

1. China: a country, occupying the easternmost part of the Asian continent.
2. China: a country in Asia, home to many great poets and philosophers.
3. China: a country in Asia, home to many poor people.

## Persuasive definitions

Persuasive definitions can be very subtle and hard to detect.

They try to influence us to accept their view of the world, while pretending to be objective.

## Persuasive vocabulary

Single words can also be persuasive. Have a look at the following:

- chairman / chairperson
- cop / police officer
- house / home
- the guy over there / the man over there / the gentleman over there

## Exercise

Try to locate the problems in the following definitions:

1. Democracy is a form of government where all people are free and have the same rights.
2. A book is a source of pleasure and entertainment.
3. A car is a means of transport.
4. A car is a means of transport with four wheels and four doors, which is used on streets.
5. A car is a means of transport which is often dangerous.

## Solutions (1)

"Democracy is a form of government where all people are free and have the same rights."

1. Persuasive. 
2. Does not specify key points of democracy, for example that the majority decides on issues.
3. Not all people are free (we have prisons in democracies, too!)

## Solutions (2)

"A book is a source of pleasure and entertainment."

1. Persuasive. 
2. Too broad (a cinema or a TV set are also sources of pleasure and entertainment).
3. No genus and difference.

## Solutions (3)

"A car is a means of transport."

1. Too broad (includes bicycles, ships and airplanes).
2. The difference is missing.

## Solutions (4)

"A car is a means of transport with four wheels and four doors, which is used on streets."

Too narrow: some cars have only two doors; cars can drive off-street.

## Solutions (5)

"A car is a means of transport which is often dangerous."

1. Persuasive. Almost everything can be dangerous.
2. Difference is missing.

## Is this a good definition?

"Physics is the systematic study of objects, processes and properties that are physical in nature."

. . .

Circular definition.

## Is this a good definition?

"Philosophy is the light that shines upon the dark corners of knowledge."

. . . 

Persuasive language, wrong (metaphorical) genus, too general.

## Is this a good definition?

"A religion is a fairy tale used for indoctrinating the uneducated."

. . .

Persuasive language, too general.

## Is this a good definition?

"To swim: To move the body forward through water with limbs, fins, or tail."

. . . 

Too specific (too narrow). People can swim backwards, and they don't need to swim in *water.* One could, for example, swim in milk.

## Is this a good definition?

"To discriminate against a person is to treat that person wrongly without good justification."

. . . 

Too general (includes stealing, lying).

## Is this a good definition?

“*Girl* refers to any young female human being."

. . . 

Good definition.

## What did we learn today?

- Descriptive, stipulative, precising, reporive, intensional and extensional definitions
- Genus and difference
- Circular definitions
- Common definition errors

## Optional reading list

Merrilee H. Salmon: Introduction to Logic and Critical Thinking. 5th ed. Thomson/Wadsworth, 2007. Definitions, pp. 56-67

## Any questions?

![](graphics/questions.jpg)\ 

## Thank you for your attention!

Email: <matthias@ln.edu.hk>

If you have questions, please come to my office hours (see course outline).
