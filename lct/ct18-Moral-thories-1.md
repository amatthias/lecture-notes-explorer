% Critical Thinking
% 18. Moral theories (1)
% A. Matthias

# Where we are

## What did we learn last time?

- Utilitarianism and animal rights

## What are we going to learn today?

- Introduction to ethical decision making

## Source

- This session is based on material from the Markkula Center for Applied Ethics at the University of Santa Clara, available online.

# A Framework for Ethical Decision Making

## Recognise an Ethical Issue (1)

>- Could this decision or situation be damaging to someone or to some group? Does this decision involve a choice between a good and bad alternative, or perhaps between two "goods" or between two "bads"?
>     - Moral decisions usually involve dilemmas where the right course of action is not immediately clear. 
>     - Often harming other’s interests is a sign that this is a moral issue.

## Recognise an Ethical Issue (2)

>- Is this issue about more than what is legal or what is most efficient? If so, how?
>     - Is the law sufficient to deal with all moral issues? If so, do we need morality at all? Can we not just do what the law says?
>     - We can easily name situations where laws are immoral (for example, laws about the treatment of Jews in Nazi Germany); and also cases where morally right actions are illegal (crossing a red traffic light in order to save a child on the other side of the road).
>     - Therefore, the law is not a reliable indicator for the morality of an action. 
>     - Laws can be morally wrong themselves.


## Get the Facts (1)

>- What are the relevant facts of the case? What facts are not known? Can I learn more about the situation? Do I know enough to make a decision?
>     - Often we need some research so that we can judge a moral issue.
>     - For example, animal rights: do animals feel pain? Which animals are conscious? And so on.
>     - Or: Abortion: When do embryos start to become conscious? Do they feel pain? Are they human or just organs? And on on.

## Get the Facts (2)

>- What individuals and groups have an important stake in the outcome? Are some concerns more important than others? Why?
>     - We need to identify the stakeholders.
>     - Often, stakeholders are overlooked.
>     - For example, animal rights: Animals, farmers, meat eaters, supermarket chains, other employees in the meat business; but also doctors, researchers, patients, pharmaceutical and cosmetics companies and their employees, and so on.
>     - A stakeholder is someone whose interests will be *significantly affected* by a moral decision.
>     - For example, a policeman or a judge are *not* stakeholders in a theft case!
>     - But *all citizens* are stakeholders in environmental pollution cases. Not only the environmental activists!

## Get the Facts (3)

>- What are the options for acting? Have all the relevant persons and groups been consulted? Have I identified creative options?
>     - Consider all relevant groups (see above: stakeholders).
>     - Creative options: Not all moral issues need to be decided in a binary way (right/wrong).
>     - Often, the best solution is a compromise.
>     - Eat some meat, but not too much, and make sure it’s not gained through animal suffering.
>     - Use animals for experiments, but only in very limited ways and only for life-saving drugs, not cosmetics.
>     - Instead of legalising or outlawing abortion, make it easier for mothers to give their children away for adoption by couples who want, but cannot have children.

## Evaluate Alternative Actions (1)

>- Evaluate the options by asking the following questions:
>- Which option will produce the most good and do the least harm? (The Utilitarian Approach).


## Evaluate Alternative Actions (2)

>- Which option best respects the rights of all who have a stake? (The Rights Approach).
>     - Would we want this right respected if we were in that person's position?
>     - Exceptions: Morality must be the same for all. "What if everyone did it?" 
>     - Would the action become impossible or the social/business environment unacceptable?
>     - Is this particular situation a valid exception to an, otherwise valid, moral rule? How do you justify the exception? Would a stranger agree that you are exceptional in this case?
>- Example: Illegally copying music from the Internet. What if the copier is a music journalist who will publish an article in a big, national newspaper about this piece of music? What if the copier is just a normal student?

## Evaluate Alternative Actions (3)

>- Choices: "Are the people affected able to make their own choices?"
>     - According to Kant, we need to think of others as “ends,” that means, of human beings who are free and infinitely valuable. They should have the freedom to make their own choices and to decide what is valuable to them.
>     - Meaning of means/ends (forgotten pen, taxi).
>     - The “end” is the goal of my action, what I want to achieve.
>     - The “means” are the tools, methods, ways I use in order to achieve my ends.
>     - Examples of treating others as “means” or “ends”? (restaurant cook, taxi driver)
>     - It is not wrong to treat others as means as long as we also treat them as ends! (see above: own choices!)


## Evaluate Alternative Actions (4)

>- Which option treats people equally or proportionately? (The Justice Approach).
>     - Kant again: All people are equal. They should have equal rights.
>     - But sometimes rights should not be equal, but proportional to people’s needs or abilities.
>     - For example, telling a wheelchair user to use the stairs like everyone else is not fair, although it would be “equal” treatment.
>     - Also, giving all students an “A” regardless of performance, would be unfair. 
>     - Here, the treatment has to be proportional to needs or performance.
>- Discrimination is a very complex issue.

## Evaluate Alternative Actions (5)

>- Which option best serves the community as a whole, not just some members? (The Common Good Approach)
>     - This is utilitarian: The idea that the morally right action should benefit most people.
>     - For example, electricity flatrates for a company. Morally right?
>     - Only this company is benefited, but the environment (and the rest of society) are harmed by wasted energy (which causes pollution and uses up common resources).
>     - Therefore, it is not morally right. The company should save energy instead of using flatrate contracts.

## Evaluate Alternative Actions (6)

>- Which option leads me to act as the sort of person or the kind of institution I/we want to be? (The Virtue Approach)
>     - Some actions have an influence on our self-image.
>     - For example, the University can give a particular vacant shop area to a fast food shop or a book shop. Which would be morally right? (Consider all factors mentioned above!)
>     - The university is considering to relax cheating rules for students’ homework. Morally right?

## Make a Decision and Test It (1)

>- Here are all the approaches again:
>     - Maximising benefits for most stakeholders
>     - Respecting others’ rights
>     - Protecting the ability of others to make their own choices
>     - Justice, equal consideration, proportionate treatment
>     - Maximising *common* good (as opposed to individual benefit)
>     - Reflecting the right set of values in one’s actions
>- Considering all these approaches, which option best addresses the situation?

## Make a Decision and Test It (2)

>- If I told someone I respect – or told a television audience – which option I have chosen, what would they say?
>     - Often, we see the morality of our actions clearer if we try to see ourselves from the outside, from another person’s viewpoint.
>     - If you wouldn’t justify doing something on TV, then perhaps something is wrong with it!

## Act and Reflect on the Outcome

>- How can my decision be implemented with the greatest care and attention to the concerns of all stakeholders?
>     - Often, we can still try to consider the “losing” or “minority” party and make it easier for them to accept the outcome of our decision.
>- How did my decision turn out and what have I learned from this specific situation?
>     - Learn for future cases. In this way, we improve in our moral judgements and avoid making the same mistakes over and over again.


## What did we learn today?

- Introduction to ethical decision making


## Any questions?

![](graphics/questions.jpg)\ 

## Thank you for your attention!

Email: <matthias@ln.edu.hk>

If you have questions, please come to my office hours (see course outline).





