% Critical Thinking
% 6. Tautologies, contradictions, conditions and conditional arguments
% A. Matthias

# Where we are

## What did we learn last time?

- We practiced using truth tables on everyday language.
- We talked about tautologies, contradictions, contingent, consistent and inconsistent statements.

## What are we going to learn today?

- More tautologies and contradictions
- Conditional expressions and conditional arguments
- Antecedent and consequent
- Unstated (hidden) conditions
- Valid and invalid argument forms

# Equivalent expressions

## From last session:

"For lunch, I want a sandwich with cheese or egg. Or I might have a soup or a steak, but the soup without chicken meat or fish balls."

- w: sandwich
- c: cheese
- e: egg
- s: soup, t: steak
- m: chicken meat, f: fish balls

\f{(w$\land$(c$\lor$e))$\lor$(s $\land\lnot$(m$\lor$f))$\lor$t}

## Equivalent expressions

![](graphics/08-equiv1.png)\ 

## Equivalent expressions

>- Come for hot-pot tonight, but don’t bring Mary **or** Peter!
>      - (means: I don’t like Mary, I don’t like Peter. I don’t like them together and I don’t like them separately. Just come without either of them.)

>- Come for hot-pot tonight, but don’t bring Mary **and** Peter!
>      - (means: don’t bring both of them together, perhaps because they’re always quarreling with each other. But you can bring one of them if you want!)

## Don’t bring Mary and/or Peter

- Don’t bring Mary **or** Peter: $\lnot$(m$\lor$p)
- Don’t bring Mary **and** Peter: $\lnot$(m$\land$p)

![](graphics/08-equiv2.png)\ 

## Prove with a truth table that the two statements are equivalent!

(1) I'll have soup with no meat or fishballs: $\lnot$(M$\lor$F)
(2) I'll have soup with no meat and no fishballs: $\lnot$M$\land\lnot$F

## Equivalent logical forms

\f{$\lnot$M$\land\lnot$F}

In such cases, we can take the “not” out of the bracket, by removing it from all propositions and changing the “and” into an “or”:

\f{$\lnot$(M$\lor$F)}

This is called *de Morgan's Law.*

# Tautologies and contradictions

## Logical tautologies and contradictions

>- Logical tautologies: statements that are *necessarily* true, for logical reasons.
>- Logical contradictions: statements that are *necessarily* false, for logical reasons.

## Logical tautologies or contradictions?

Guess what these are without using a truth table!

1. Tonight I will go to the cinema and not go to the cinema.
2. Tonight I will go to the cinema or not go to the cinema.
3. (A$\to$(B$\lor$A))$\lor$(C$\land$D)

## Logical tautologies or contradictions?

>1. Tonight I will go to the cinema **and** not go to the cinema.
>      - Contradiction: C$\land\lnot$C is always false!
>2. Tonight I will go to the cinema **or** not go to the cinema.
>      - Tautology: C$\lor\lnot$C is always true!
>3. (A$\to$(B$\lor$A))$\lor$(C$\land$D)
>      - Tautology: The only case where the implication could be false is if A is true and (B$\lor$A) is false.
>      - But if A is true, then (B$\lor$A) must also be true (the same A!)
>      - Thus, (A$\to$(B$\lor$A)) is true, and therefore (disjunction)  (A$\to$(B$\lor$A))$\lor$(C$\land$D) must also (always) be true.

## Are these tautologies, contradictions, or contingent statements?

*Do not use a truth table.*

1. (A$\to$B)$\lor\lnot$A
2. (A$\to$B)$\lor$A
3. (A$\to$B)$\lor\lnot$B
4. (A$\to$B)$\lor$B

## Are these tautologies, contradictions, or contingent statements?

1. (A$\to$B)$\lor\lnot$A : Contingent
2. (A$\to$B)$\lor$A : Tautology
3. (A$\to$B)$\lor\lnot$B: Tautology
4. (A$\to$B)$\lor$B: Contingent

## Rhetorical tautologies

We don't have only *logical* tautologies and  contradictions, but also *rhetorical* tautologies and contradictions.

*Rhetorical tautologies* are sentences in which we repeat redundant information.

*Rhetorical contradictions* are sentences in which we contradict ourselves.

## Tautologies and contradictions

Please comment on the following:

1. She is a female girl.
2. Tonight I'll either go to the cinema or not.
3. I very much enjoyed the finished result of that play.
4. You need to know these true facts about heart disease.
5. You are required to have three credits, but you may be exempted.
6. Let's go to the ATM machine!
7. She is the daughter of her uncle William.
8. This is a necessary requirement

## Tautologies and contradictions

1. She is a female girl: Tautology (rhetorical).
2. Tonight I'll either go to the cinema or not: Tautology (logical).
3. I very much enjoyed the finished result of that play: Tautology (rhetorical).
4. You need to know these true facts about heart disease: Tautology (rhetorical: "true facts").
5. You are required to have three credits, but you may be exempted: Contradiction (rhetorical).
6. Let's go to the ATM machine: Tautology (rhetorical): The M in ATM already stands for "machine."
7. She is the daughter of her uncle William: Contradiction (rhetorical).
8. This is a necessary requirement: Tautology (rhetorical).

# Conditional arguments

## Conditional expressions
A conditional expression is a particular form of expression:

>- It contains two propositions.
>- They are linked together by an implication (if-then).

. . .

Examples:

>- If I study, then I will pass my examination.
>- My girlfriend would not have left me, if I had made her more presents.
>- You will get sick if you eat only fast food.

## Standard form

The standard form of a conditional expression is

**if** (something) **then** (something else)

## Standard form

>- If  I study, then I will pass my examination
>- My girlfriend would not have left me, if I had made her more presents = If  I had made my girlfriend more presents, then she would not have left me.
>- You will get sick if you eat only fast food = If you eat only fast food, then you will get sick.

. . . 

>- The proposition after the “if” is called the *antecedent.*
>- The proposition after the “then” is called the *consequent.*

## *Antecedent* and **consequent**

- *If I study*, **then I will pass my examination.**
- **My girlfriend would not have left me,** *if I had made her more presents.*
- **You will get sick** *if you eat only fast food.*

# Hidden conditions

## Hidden conditions

>- Often there are hidden (unstated) conditions involved, which are required to bring about the consequent.
>- We need to uncover them, in order to understand the full meaning of the conditional expression and to evaluate its truth.

. . . 

>- "If you have a tea bag, you can make some tea."
>- This *looks like* a sufficient condition, but it is not: you still need hot water and a cup.

. . .

The full argument thus is:

(1) If you have a tea bag and   
(2) hot water *(hidden condition)* and   
(3) a cup, *(hidden condition)*   
then you can make some tea.

## Hidden conditions
Find the hidden conditions:

1. If the weather forecast says that it will rain, it will.
2. If I go to the beach, I will get sunburned.
3. If I study hard, I will get a good grade.
4. If I do sports every day, I will be healthy.
5. If I read many books, I will be educated.

## Hidden conditions

>- If the weather forecast says that it will rain,
>- and the forecast is right,
>- then it will rain.

## Hidden conditions

>- If I go to the beach, 
>- and the sun is shining,
>- and I sit in the sun,
>- and I expose my skin to the sun,
>- and I don't use any protective cream,
>- then I will get sunburned.

## Hidden conditions

>- If I study hard, 
>- and I participate in the classes,
>- and I am not sick or too tired at the examinations,
>- and nothing distracts me from the examinations,
>- then I will get a good grade.

## Hidden conditions

>- If I do sports every day,
>- and I don't have any accidents,
>- and I take care of what I eat,
>- and I sleep well,
>- and I'm generally lucky,
>- then I will be healthy (for a few years).

## Hidden conditions

>- If I read many books,
>- and these are educating books (e.g. not telephone directories),
>- and I understand what they say,
>- and I can integrate what I read into my life,
>- then I will be educated.

# Conditional argument forms

## From conditions to arguments

Until now, we examined only conditional expressions of the form:

*if something then something else*

. . . 

This is a *hypothetical* form, which does not tell us about what *actually* happened (it doesn't have a conclusion!)

. . . 

In order to know what actually happened, we have to add a statement telling us the *facts.*

## Argument forms

1. If you eat too many mooncakes, you will be sick. *(conditional)*
2. You eat too many mooncakes. *(statement of fact)*
3. You are sick. *(conclusion)*

## Premises and  conclusion

1. If you eat too many mooncakes, you will be sick. *(conditional)*
2. You eat too many mooncakes. *(statement of fact)*
3. You are sick. *(conclusion)*

- 1 and 2 are called the *premises* of the argument.
- 3 is called the *conclusion.*

## Premises and conclusion

- We saw before that premises can be unstated (hidden).
- Conclusions may also be hidden.

. . . 

For example:

“Giving students a fail grade, will damage their self-confidence. We should not damage students’ self-confidence.”

Here the conclusion is obvious, but not explicitly stated. Still this would count as an argument.

## Conditional argument forms

In an abstract way, we can see two categories of conditional arguments.

1. Affirming the antecedent
2. Denying the consequent

*Which is which depends on the second premise (the statement of fact)!*

## Affirming the antecedent

You remember antecedent and consequent:

If *antecedent* then **consequent.**

If *I buy this watch,* **I'll get into financial trouble.**  
*I bought this watch.*  
 ------------------------------------------------------------------  
**I got into financial trouble.**

. . .

\vspace{3ex}

Logical form:

if *p* then **q** (*p*=antecedent, **q**=consequent)  
*p* (affirming the antecedent)  
 ---------------------------------------------------  
**q** (conclusion)

## Affirming the antecedent

>- If I take the bus, I'll arrive late. I took the bus. Conclusion: I arrived late.
>- If I always do the exercises, I will get a good grade. So I know I'll get a good grade, because I'm always doing the exercises. (See that the order of second premise and conclusion is reversed!)

## Affirming the antecedent

*Make up an example of an argument which affirms the antecedent!*

You need three sentences:

- a conditional,
- a statement of fact that affirms the antecedent,
- and a conclusion!

## Denying the consequent

If *you really like noodles,* then **you eat them at least once a week.**  
You **don't eat noodles at least once a week.**  
 -----------------------------------------------------------------  
You *don't really like noodles.*

\vspace{3ex}

if *p* then **q** (*p*: antecedent, **q**: consequent)  
not **q** (denying the consequent)  
 ------------------------------------------------  
not *p*

## Denying the consequent

Make up an example of an argument which denies the consequent!

## Latin names

And two more names to remember. You will need them later as abbreviations ("MP" and "MT"):

>- Affirming the antecedent is called Modus Ponens (“the way of putting in place”).
>- Denying the consequent is called Modus Tollens (“the way of taking away”).

## Invalid forms of argument

What is wrong here?

If bats were birds, then bats would have wings.  
Bats have wings.  
 ------------------------------------------------  
Therefore, bats are birds.

. . . 

\vspace{3ex}

If empty bottles were ships, they would float on water.  
Empty bottles do float on water.  
 ---------------------------------------------------------  
Therefore, empty bottles are ships.

## Invalid forms of argument

If bats were birds, then bats would have wings.  
Bats have wings.  
 ------------------------------------------------  
Therefore, bats are birds.

. . . 

\vspace{3ex}

if *p*, then **q**  
**q** (affirming the consequent)  
 --------------------------------------  
*p* (invalid!)

. . . 

\vspace{3ex}

“Affirming the consequent” is an invalid form of argument!

## Invalid forms of argument

What is wrong here?

If electric lights were candles, they would give light.  
Electric lights are not candles.  
 ------------------------------------------------------------  
Therefore, they do not give light.

\vspace{3ex}

. . . 

if p, then q  
not p (denying the antecedent!)  
 ------------------------------------  
not q (invalid!)

. . .

\vspace{3ex}

“Denying the antecedent” is an invalid form of argument!

## Valid and invalid

Using what we learned here, we can determine the validity of conditional arguments independently of their content, looking only at the logical form.

Remember:

>- Always when the “A-words” (**a**ntecedent, **a**ffirm) are paired with a “non-A” word, the argument is *invalid!*
>- **A**ffirming **a**ntecedent (a with a), **d**enying **c**onsequent (not-a with not-a): valid.
>- **A**ffirming **c**onsequent (a with c), **d**enying **a**ntecedent (a with d): *invalid!*

# Exercises

## Is the following correct?

"Without complications, life would be boring. Life is not boring. So there are complications."

. . . 

\vspace{3ex}

p=There are no complications  
q=Life is boring  

. . .

\vspace{3ex}

if p then q  
not q  
 ------------  
not p

. . .

Valid: “Denying the consequent.”

## Is the following correct?

"The night sky looks different in the northern and southern parts of the earth, and this would be the case if the earth were spherical^[Like a ball.] in shape. Therefore, the earth must be spherical in shape." (Aristotle)

. . .

\vspace{2ex}

If the earth is spherical in shape, then the night sky ...  
The night sky ...  
 ------------------------------------------------------------  
Therefore the earth is spherical.

. . . 

\vspace{2ex}

If p then q  
q  
 ------------  
p

. . . 

Invalid: “Affirming the consequent.”

## Is the following correct?

"I tell you this: if it wasn't James who killed the gangster, it must be you. And I know it wasn't James. So it must have been you."

. . . 

p=It wasn't James  
q=It's you

if p then q  
p  
 ------------  
q

. . .

Valid: “Affirming the antecedent.”


## Valid and invalid argument forms

![](graphics/09-valid.png)\ 

## Valid and invalid argument forms

Remember:

>- Always when the “A-words” (**a**ntecedent, **a**ffirm) are paired with a “non-A” word, the argument is *invalid!*
>- **A**ffirming **a**ntecedent (a with a), **d**enying **c**onsequent (not-a with not-a): valid.
>- **A**ffirming **c**onsequent (a with c), **d**enying **a**ntecedent (a with d): *invalid!*

## Valid and invalid forms

- Affirming the antecedent (valid): If it rains, the street will be wet. It rains. Therefore, the street is wet.
- Denying the consequent (valid): If it rains, the street will be wet. The street is not wet. Therefore, it has not rained.
- Affirming the consequent (*invalid*): If it rains, the street will be wet. The street is wet. Therefore, it has rained.
- Denying the antecedent (*invalid*): If it rains, the street will be wet. It has not rained. Therefore, the street is not wet.

## Is the following correct?

"Cats have four legs, and precisely this would be the case if cats were animals. Therefore, cats are animals."

\vspace{3ex}

. . .

If cats were animals they would have four legs.  
Cats have four legs.  
 ---------------------------------------------------------  
(Conclusion:) Therefore cats are animals.

\vspace{3ex}

. . . 

*Invalid: affirming the consequent!*

## Is the following correct?

"If each man had a definite set of rules of conduct by which he regulated his life he would be nothing but a machine. But there are no such rules, so men cannot be machines." (Alan Turing)

\vspace{3ex}

. . . 

p=Each man has a set of rules (antecedent)  
q=Man is nothing but a machine (consequent)  

\vspace{3ex}

. . . 

Premise 1: if p then q  
Premise 2: not p  
 --------------------------  
Conclusion: not q

*Invalid: “Denying the antecedent”!*

## Is this valid?

“However, if a Renshaw cell is also activated by the recurrent branch of the motor neuron, it will inhibit the motor neuron of the agonist. But the Renshaw cell was not activated, because you can clearly see in this measurement that the motor neuron of the agonist has not been inhibited.”

. . . 

\vspace{3ex}

Denying the consequent (valid).

\vspace{3ex}

*Observe that you can decide on the validity of this argument, without understanding anything of it! This is cool.*

## Conditional arguments

Is this valid?

“If bromine and chlorine return to the levels that were present in 1980 and every other aspect of the atmospheric environment is unchanged, we can expect a full recovery of the ozone layer. But bromine and chlorine will not return to the levels that were present in 1980, and therefore we cannot expect a full recovery of the ozone layer.”

## Conditional arguments

Is this valid?

“If *bromine and chlorine return to the levels that were present in 1980* and every other aspect of the atmospheric environment is unchanged, we can expect a full recovery of the ozone layer. But *bromine and chlorine* **will not** *return to the levels that were present in 1980,* and therefore we cannot expect a full recovery of the ozone layer.”

\vspace{3ex}

**Denying** the *antecedent* (invalid).

## Conditional arguments

Is this valid?

“If I could understand the concept of conditional arguments, I would get a passing grade. Now I got a passing grade, so I must have understood the concept of conditional arguments.”

. . . 

Affirming the consequent (invalid).

## Conditional arguments

Is this valid?

“I must still be eating too much ice cream,” complained George, “because my waist measurement is the same as it was six months ago, and I know that if I didn’t eat so much ice cream, I would reduce my waist size.”

\vspace{3ex}

. . . 

If I didn’t eat so much ice cream, I would reduce my waist size.  
My waist measurement is the same as it was six months ago.  
 ------------------------------------------------------------------------------  
Conclusion: I must still be eating too much ice cream.

. . . 

Denying the consequent (valid).

## Conditional arguments

If you get hit by a car when you are six then you will die young. But you were not hit by a car when you were six. Thus you will not die young.

. . .

p: hit by a car when six  
q: die young  

\vspace{3ex}

if p then q  
not p  
 ------------------  
therefore not q

. . . 

\vspace{3ex}

Denying the antecedent (*invalid*).


# Additional exercises

## Additional exercises

Use a truth table to show whether the two statements:

“If you make coffee, I will drink a cup”

and

“I’ll drink a cup or you didn’t make coffee”

are equivalent!

## Equivalent?

“If you make coffee, I will drink a cup”

\f{M$\to$D}

“I’ll drink a cup or you didn’t make coffee”

\f{D$\lor\lnot$M}

\begin{tabular}{|c|c||c|c|c|}
\hline
$M$ & $D$ & $(M \rightarrow D)$ & $\neg M$ & $(D \vee\neg M)$ \\
\hline
F & F & T & T & T \\
\hline
F & T & T & T & T \\
\hline
T & F & F & F & F \\
\hline
T & T & T & F & T \\
\hline
\end{tabular}

## Are these equivalent?

Can I convert:

“If you don't practice, you'll never be good at the piano”

into:

 “If you want to be good at the piano, you'll have to practice”?

*Examine it using a truth table!*

## Are these really equivalent?

(p=practice,  g=good at piano)

- If you don't practice, you'll never be good at the piano: $\lnot$p$\to\lnot$g
- If you want to be good at the piano, you'll have to practice: g$\to$p

. . .

![](graphics/08-piano.png)\ 


## What did we learn today?

- Conditional expressions and conditional arguments
- Antecedent and consequent
- Unstated (hidden) conditions
- Valid and invalid argument forms
- Equivalent expressions

## Any questions?

![](graphics/questions.jpg)\ 

## Thank you for your attention!

Email: <matthias@ln.edu.hk>

If you have questions, please come to my office hours (see course outline).
