% Critical Thinking
% 14. Fallacies (2)
% A. Matthias


# Where we are

## What did we learn last time?

- Fallacies group A (core and very important)

## What are we going to learn today?

- More fallacies, groups B and C.

## Reminder: Core and extended fallacies

>- Since different teachers teach this course, and not all focus on fallacies, we have two categories of fallacies:
>     - ‘Core’ fallacies are those taught in all sections of this course. They are tested in the common part of the final exam.
>     - ‘Extended’ are all the other fallacies. I may ask you about them in my own part of the exam.
>- Core fallacies are: Hasty generalisation, begging the question, equivocation, denying the antecedent, affirming the consequent and wishful thinking.

## Reminder: A, B and C fallacies

>- The other fallacies in this presentation are specific to my class. You still have to learn them, but you will find that other sections might talk about other topics instead.
>- Because there are so many, I have graded the following pages into A (very important and core), B (important) and C (not that important).
>- Essentially, the letter is the reverse of the grade you want to get with fallacy questions: For an A, you’ll need A, B and C fallacies. For a B, you will need A and B fallacies. For a C, you will need only the A fallacies.
>- You could also see the letters as indicators of the probability that a question about a fallacy will be in an exam: A fallacies will very likely be asked in an exam; B fallacies are likely; and C fallacies are less likely (but not impossible!)

# Fallacies Group B

## B: Fallacies

- Bill: "I think that plastic bags harm the environment."
- Jane: "I disagree completely. Dr. Jones says that plastic bags are harmless. He has to be right, after all, he is a respected expert in his field."
- Bill: "I've never heard of Dr. Jones. Who is he?"
- Jane: "He's the guy who won the Nobel Prize for medicine, for his work on hair loss. He's a world famous expert, so I believe him." 

## B: Appeal to authority

>- An unjustified “appeal to authority” is committed when the person in question is not a legitimate authority on the subject.
>- In our example, Dr. Jones is an expert on hair loss, not on plastic bags.
>- Since he has no expertise on the subject, we cannot appeal to his authority.
>- An appeal to authority is not a fallacy, however, if the person really *does* have relevant expertise.

## B: Fallacy?

1. An actor on TV tells you that you should use a particular shampoo in order to stop hair loss.
2. Your history teacher tells you that the Greek revolution against the Turkish occupation of Greece happened in 1821.

. . . 

Fallacy?

1. Yes. An actor is not an expert on hair loss.
2. No. A history teacher is supposed to be an expert on history. So you should believe what he says about history.

## B: Fallacies

Salad must be healthy, since so many people believe it to be healthy.

## B: Appeal to belief

>- “Something must be true because many people believe it.”
>- This is is, of course, a fallacy.
>- In old times, almost all people believed that the Earth was flat. This did not make this belief true.

## B: Appeal to belief

>- In our example, the fact that many people believe salad to be healthy, does not mean that it actually is.
>- (It might be for other reasons, but not *because* people believe it!)
>- This is an interesting example of a fallacy where both the premise and the conclusion are actually true, but the argument is still a fallacy (because the premise does not support the conclusion!)

## B: Fallacies?

1. Bill is the best student in class, because everyone believes that this is the case.
2. Bill is the most handsome boy in class, because everyone believes that this is the case.

Are these fallacies, or is there valid support for the conclusions?

## B: Fallacies?

>1. Fallacy. Whether someone is the best student or not does not depend on the belief of others. It can be measured, for example by looking at examination results.
>2. Correct. Handsomeness is defined only by the belief of others. There is no objective measure for beauty, or for how attractive or well-dressed someone is. These are entirely matters of opinion.

## B: Fallacies?

>- An appeal to belief is *not* a fallacy if the subject is shaped by the belief itself!
>- For example, if most people believe that you shouldn't wear a swimsuit to a wedding, then you should not.
>- The only reason you should not, is exactly that most people believe that you should not.
>- So in this case, there is no truth to the matter that is independent of the beliefs of a majority. The majority’s belief is the only truth.
>- This also applies to table manners, fashion, statements about beauty, and other social phenomena where truth is defined by common belief!


## B: Fallacies

- James: "So, what is this new marketing method?
- Bill: "Well, the latest thing in marketing techniques is the GK method. It was just published last week."
- James: "Well, our old marketing method has been quite effective."
- Bill: "But you know that we have to go ahead with progress. That means new ideas and new techniques have to be used. The GK method is new, so it will do better than that old, dusty method."

## B: Appeal to Novelty

>- It is assumed that something is better or correct simply because it is new.
>- This is a fallacy if, in the area we are talking about, a new thing is not necessarily better than an old thing.
>- In our example, no evidence for the efficiency of the new marketing method is presented.
>- Since marketing methods do not necessarily get better if they are newer, it is a fallacy to assume that a new method must necessarily be better than an old, successful one.

## B: Appeal to Novelty

Is an appeal to novelty always a fallacy, or are there cases where one could use an appeal to novelty to support a valid argument?

## B: Appeal to Novelty

>- An appeal to novelty doesn't need to be always a fallacy.
>- For example, fresh milk is really better than old milk.
>- A new computer is probably better than an old one.
>- In these examples, time is an important factor.

## B: Fallacies

"Of course the university teaching system, with lectures and tutorials, is the best. Universities have been teaching in this way for almost one thousand years. So, it has got to be good."

## B: Appeal to Tradition

>- “Something is better or correct simply because it is older, traditional, or has always been done that way.”
>- In our example, the fact that the university teaching principles have not changed for a thousand years does not automatically make them good. Perhaps it’s time they finally changed.

## B: Which are fallacies?

1. These street shoes are the newest model, so they must be better than the shoes I bought last year!
2. This mobile phone is the newest model, so the touchscreen must be better and it must have a higher resolution than the phone I bought last year!

## B: Fallacies?

>1. Yes. Shoes do generally not get “better” each year. There is not much progress in shoe technology from year to year. An exception would be perhaps some hi-tech running shoes, but these are supposed to be “street shoes”.
>2. No. Mobile phone technology does progress fast, so every year phones will have better screens, faster processors and more memory than the year before. So it is an inductive argument to assume that this year’s phone will be better than last year’s. (This conclusion might still be wrong, but the premises do support the conclusion, so it’s not a fallacy! Even a low probability that the conclusion is true makes an argument a weak argument and prevents it from being a fallacy.)

## B: Fallacies

What do you think of this?

- Bill: “I really like this Critical Thinking course. It is somehow interesting, isn't it?”
- James: “Oh no! Help! It is so boring!”
- Ann: “Are you crazy, Bill? How can you like this?”
- Sandy: “I can't stand it another minute!”
- Bill: “Oh well, I was just joking... Of course it is terrible!”

## B: Bandwagon

>- In the Bandwagon fallacy, a threat of rejection by a group of people one wants to be accepted by is substituted for evidence.
>- In our case, Bill says that he likes the course, until he perceives that all others have a different opinion. He then says that he just had been joking.

## B: Now do it yourself!

Try to imagine a situation which shows a bandwagon fallacy!


## B: Bandwagon vs appeal to belief

The two fallacies are similar, but:

>- In the bandwagon fallacy, you want to be part of a *particular group,* and you will adjust your opinion to that of that particular group, even if this is not a common opinion (for example, religious beliefs of a minority)
>- In the appeal to belief, you believe something because *most or all* people believe it.


## B: Fallacies

This cake is made from delicious ingredients. Therefore, it must be delicious.

## B: The fallacy of Composition

- Individual things have characteristics A, B, C, etc. 
- Therefore, something which is composed of  these things has the same characteristics A, B, C, etc. 

. . .

This is a fallacy because the mere fact that the components have certain characteristics does not, in itself, guarantee that the composed thing (taken as a whole) will have the same characteristics.

## B: The fallacy of Composition

In our example, it is concluded that the cake must be delicious because the ingredients are.
This is a fallacy, because a cake made up from delicious ingredients can still be disgusting (for example, if it has the wrong proportion of ingredients or if the ingredients just don't fit together)

## B: Fallacies

Bill lives in a large building, so his flat must be large.

## B: The fallacy of Division

- The whole, X, has properties A, B, C, etc. 
- Therefore each part of X must have the same properties A, B, C, etc.

In our example, the fact that the building is large, does not mean that the individual flats are.

## B: Composition and Division

Now do it yourself! Make up an argument which contains a fallacy of composition or division!

## B: Is this a fallacy?

Is this a fallacy?

- All the ingredients of this dish are salty, therefore this dish must be salty.

. . . 

Not a fallacy! If we add up salty ingredients we will get a salty dish!

Whether composition/division are fallacies or not depend on whether the properties we consider do add up or not!

# Fallacies Group C

## C: Fallacies

“Yeah, I know some people say that cheating at exams is wrong. But we all know that everyone does it, so it’s okay.”

## C: Appeal to Common Practice

“Something is right because many people do it.”

This is a fallacy because the mere fact that most people do something does not make it morally right, or worthy of imitating.

## C: What is the difference?

- *Belief* is what everybody thinks.
- *Common practice* is what everybody is doing.

These two do not need to agree!

Example:
To use too many plastic bags is common practice, although nobody believes that it is good, or that this should be done.

## C: Appeal to Popularity

>- Sometimes the two fallacies are put together under the heading "Appeal to Popularity."
>- What is *popular* can either be a belief (what we called Appeal to Belief), or an actual practice (what we called Appeal to Common Practice). 
>- So an **Appeal to Popularity** covers both cases and is the same as these two fallacies taken together.

## C: Is this a fallacy?

“Buy this book! It has sold 5 million copies worldwide!”

. . . 

It is hard to decide whether this is a fallacy or not.

>1. It could be argued to be a fallacy, because the fact that it has sold so many copies does not support the conclusion that I should buy it:
>      - Book tastes of different people are different. Even if 5 million people liked it, it does not follow that I will like it!
>      - People who buy books typically buy them before they read them. So the important number would be how many people actually liked the book after reading it, not how many bought it.
>2. On the other hand, many people have similar tastes. If I know that I generally like best-sellers, then probably I will also like this book, since obviously my taste in books is close to the average. Then this would not be a fallacy (for me).

## C: Fallacies

"The new UltraSkinny diet will make you feel great. No longer be troubled by your weight. Enjoy the admiring stares of other people. You will know true happiness if you try our diet!"
 
## C: Appeal to Emotion

>- This fallacy is committed when someone manipulates peoples’ emotions in order to get them to accept a claim as being true.
>- In this fallacy, one substitutes various means of producing strong emotions in place of evidence for a claim.
>- What is missing in this arguments are two premises (and the facts that would support them):
>      1. The particular diet is effective
>      2. Being skinny would make me happy (not certain, even if the diet actually works!)
>- In our example, feelings are used to cover up the fact that no evidence is given to prove that the diet is effective.

## C: Emotion and Fear

>- Of course, fear is also an emotion.
>- But for the purposes of classifying the fallacies, an “appeal to emotion” is always an appeal to a positive emotion; while an “appeal to fear” counts as a separate fallacy.

## C: Fallacies

"The previous speaker said that we should ban plastic bags, but this is obviously a joke! How funny!" 

## C: Appeal to Ridicule

>- Ridicule (making fun of an argument) or mockery is substituted for evidence.
>- This is a fallacy, because making fun of a claim does not show that it is false.
>- But it can be very effective.
>- In our example, no real arguments are presented concerning the topic of plastic bags.

## C: Fallacies

- James: “I think that UFOs exist and are spaceships coming to us from other planets.”
- Erica: “What makes you think so?”
- James: “Well, can you prove that they do not exist?”

## C: Burden of Proof

In many situations, one side has the burden of proof resting on it. This side must provide evidence for its position. The claim of the other side, the one that does not bear the burden of proof, is assumed to be true unless proven otherwise. 

## C: Burden of Proof

The difficulty in such cases is determining which side has the burden of proof. In some cases the burden of proof is set by the situation. 

>- In most legal systems, a person is assumed to be innocent until proven guilty. 
>- In debate the burden of proof is placed on the affirmative team. 
>- In most cases the burden of proof rests on those who support a surprising or unusual claim, or one that is not shared by the majority of the group they are in.

## C: Burden of Proof

In our example, not Erica has to prove that UFOs don't exist, but James have to give a proof that they do, since his position is the one which is not generally accepted as being true.

. . . 

Another example: at a meeting of an environmentalist group, you would not have to prove that global warming is a threat to the planet, because this is generally assumed to be the case in that group. But if you were in a group of automobile industry leaders, you might have to prove that claim.

## C: Fallacies

- Jane: “I've been thinking about buying a computer.” 
- Bill: “What sort of computer do you want to buy?” 
- Jane: “I've been thinking about getting a Kiwi Fruit 2200. I read in that consumer magazine that they have been found to be very reliable in six independent industry studies.” 
- Bill: “I wouldn't get the Kiwi Fruit. A friend of mine bought one a month ago to finish his master's thesis. He was halfway through it when smoke started pouring out of the CPU, then his desk started burning, and then even his hair caught fire. He didn't get his thesis done on time and he had to quit studying. Now he's working over at the student cafeteria. He’s they guy without hair over there!”
- Jane: “Oh! Then I should not buy the Kiwi!”

## C: Misleading Vividness

>- Misleading Vividness is a fallacy in which a very small number of particularly dramatic events are taken to *outweigh a significant amount of statistical evidence.*
>- This sort of "reasoning" is fallacious because the mere fact that an event is particularly vivid or dramatic does not make the event more likely to occur, especially in the face of significant statistical evidence.
>- If there is *no* evidence to the contrary, though, this would not necessarily be a fallacy. It might be a weak argument (depending on circumstances).

## C: Misleading Vividness

In our example, the fact that the Kiwi Fruit Computer started to produce smoke and sent his owner working in the student cafeteria does not constitute enough evidence to outweigh six independent industry studies.

If the independent industry studies had not been there, then the text would be either a hasty generalization or a weak inductive argument, depending on how convincing the conclusion was.

## C: Now do it yourself!

Make up an argument which contains a misleading vividness fallacy! 


## C: Fallacies

“I think it is important to make the requirements stricter for the graduate students. I recommend that you support it, too. After all, we are in a budget crisis and we do not want our salaries affected.”

## C: Red Herring

>- An irrelevant topic is presented in order to divert attention from the original issue. The basic idea is to “win” an argument by leading attention away from the argument and to another topic.
>- In our example, the question of salaries has nothing to do with the question of whether to make requirements stricter for graduate students.

## C: Now do it yourself!
Make up an argument which contains a red herring fallacy! 

. . .

A good red herring fallacy is not made by saying something totally irrelevant. This would just sound crazy. The point is to say something that *seems* to make sense superficially, but that, in reality, leads away from the topic that is discussed!

## C: Fallacies

Father and daughter argue about washing the family car: 

- Father: “You should help me wash the car. It is getting very dirty.”
- Daughter: “Why, we just washed it last year. Do we have to wash it *every day?”*

## C: Straw Man

A person simply ignores the actual position of the opponent and substitutes a distorted or exaggerated version of that position: 

- Person A has position X. 
- Person B presents position Y (which is a distorted version of X). 
- Person B attacks position Y. 
- Therefore, X is assumed to be false.

But attacking a distorted version of a position does not constitute an attack on the position itself!


# Exercises and additional points

## What do you think of this?

It is wrong to say that the sensational newspapers, those which are concerned mainly with the love life of movie stars and with aliens visiting the Earth, do not serve the public interest. After all, the public buys them, and therefore proves to have an interest in them!

## Equivocation “interest”

Two meanings of “interest” (equivocation):

1. “to have an interest in something” = to be attracted by something. Example: I have an interest in computers, or spaceships, or Cantonese pop songs.
2. “something is in my interest” = something is good for me. Example: it is in my interest to eat more vegetables. It is in my interest to do my Critical Thinking exercises (although they may not interest me!)

. . . 

The public shows interest in many things (car accidents, plane crashes, natural catastrophes) which are obviously not “serving its interest.”

## Identify the problem!

“May I recommend the 1985 Pinot Gris wine? The 1985 cuvee has a firm structure with race and texture. I would describe it as sleek, noble, refined, with intense varietal character.”

## Obscurity

We don't know what he is talking about, so this is not an argument which appeals to our reason, but instead it tries to make us feel that the salesperson has impressive knowledge of wine.

## Identify the problem!

Some people say that they would believe in God, if they only saw some miracle to convince them. What can I say? Are they blind? They should just look around! The world is full of miracles! The miracle of modern medicine, the miracle of being able to fly in airplanes, the miracle of man's landing on the moon! What more miracles do these people expect to see?

## Equivocation “miracle”

>- “Miracle” in the first sense is an unnatural and unexpected event, which can be clearly traced back to God's intervention.
>- “Miracle” in the second sense is a technological achievement, which only looks “miraculous” because most people don't know how it works. But for the engineer or doctor, there is nothing “miraculous” about it.

## Identify the problem!

If Peter wants to get into medical school, he'll need to have good grades. But if he's going to keep good grades, he can't work so many hours that he doesn't have time to study. So if Peter wants to get into medical school, he shouldn't work too many hours. He should take more breaks and relax!

## Equivocation “work”

>- Here, “work” in the first part of the argument means “work besides studying.”
>- “Work” in the conclusion is used in the sense of any work, *including studying.*
>- Therefore, this is a fallacy of equivocation. The two words look the same, but are used in a different sense!
>- The conclusion does not follow from the premises.

## Is this a good argument?

People who love wild animals, would never buy a fur coat. But if no one buys fur coats, lots of fur-bearing animals will not be raised. And if they are not raised, there will be fewer of them. So, people who love animals are actually reducing the number of fur-bearing animals found in nature!

## Equivocation

>- Here, the notion of an animal existing “in nature” is confused with the animal “especially raised to produce fur.”
>- If more animals are raised to produce fur, it doesn't follow that more such animals will be found in nature!


## Now use fallacies in a speech

- Write a short speech (1 minute) about whether we should use disposable plastic bags or not.
- Make sure not to use a single real argument in it!
- Use only fallacies, but try to make it as convincing as possible!

## Example speech with fallacies

Ban plastic bags! What a funny idea! We all know that everybody needs plastic bags. All people use them, so they cannot be too bad! I only know one person who doesn't use them, and this is an ugly old man who is poor and has no friends. This is the kind of people who are against plastic bags! And besides, recently this famous pop star said that it's okay to use them, remember? And if we ban them? Well, then we'll have no way to carry things from the market. People's handbags will stink of fish and the unemployment rate will rise and with it the crime rate! Really, we can't afford to ban plastic bags! We've always used them, anyway, so they can't be too bad! So let's stop making silly proposals and do something useful!

## Which fallacies do you see here?

“Great businessmen have used leather suitcases for centuries, so you don’t want to be left out! Therefore you should now take a look at our new line of suitcases ‘PremiumCase.’ They have the highest sales world-wide. Recently, the Global Business Travel Association has included them into their list of ‘50 Essential Items for the Business Traveler.’ The PremiumCase line is completely new, and combines power and flexibility in a way that guarantees true pleasure to the user.”

## Which fallacies do you see here?

“Great businessmen have used leather suitcases for centuries (Tradition), so you don’t want to be left out (Fear)! Therefore you should now take a look at our new line of suitcases ‘PremiumCase.’ They have the highest sales world-wide (Common practice). Recently, the Global Business Travel Association has included them into their list of ‘50 Essential Items for the Business Traveler.’ The PremiumCase line is completely new (Novelty), and combines power and flexibility in a way that guarantees true pleasure to the user (Emotion).”

## Is this a fallacy?

In the previous text, we see this sentence:

“Recently, the Global Business Travel Association has included them into their list of ‘50 Essential Items for the Business Traveler.’”

Is this a fallacy too? Which one?

## Fallacy?

No. The Global Business Travel Association should have relevant expertise on the things one needs to have while business traveling, so this is not an unjustified appeal to authority!

## Choose the right fallacy!

You have to produce ads for the following products:

- a hair drying machine
- a mechanical clock from Switzerland
- a new sushi place
- running shoes for children
- nail polish
- a set of brown leather suitcases
- a reusable shopping bag for a supermarket chain

What would be the most suitable fallacy for each product?

## Which fallacy is this?

“Though many other poets could be addressed, and though the discourse between Whitman and the century of American poets following him could be shown to hyperlink between uncountable connections ad infinitum, the ouroboric antipoetic-poetic of spokenness and the ouroboric rhetorical relationship of speaker and subject, extending from that of self and other, demand primary attention here.”

T. Gilmore: Toward the Death and Flowering of Transcendentalism in Walt Whitman

. . . 

Obscurity.

## Which fallacy is this?

“Either you accept nuclear power, or global warming will destroy the planet.”

. . . 

False dilemma and appeal to fear.

## Which fallacy is this?

“I believe that God exists, because the Bible tells us so. The Bible must be right: after all, it is God’s word.”

. . . 

Begging the question.

## Which fallacy is this?

“You will have to study for the final exam, or you will get a bad grade.”

. . . 

Not a fallacy. This is a (true) dilemma.

## Which fallacy is this?

“All ingredients in this cake are sweet, so the cake will be sweet.”

. . . 

Not a fallacy. The sweetness of the ingredients *does* add up to the sweetness of the whole cake!


## What did we learn today?

- Lots and lots of fallacies...

## Reference

All fallacies and many examples are from:

The Nizkor Project,
http://www.nizkor.org/features/fallacies

Originally by Dr. Michael C. Labossiere, ontologist@aol.com

## Any questions?

![](graphics/questions.jpg)\ 

## Thank you for your attention!

Email: <matthias@ln.edu.hk>

If you have questions, please come to my office hours (see course outline).

