% Critical Thinking
% 13. Fallacies (1)
% A. Matthias


# Where we are

## What did we learn last time?

- Argument analysis, restructuring, hidden premises, conclusions.
- Analogies.

## What are we going to learn today?

- What fallacies are, and a few common types.

## Core and extended fallacies

>- Since different teachers teach this course, and not all focus on fallacies, we have two categories of fallacies:
>     - ‘Core’ fallacies are those taught in all sections of this course. They are tested in the common part of the final exam.
>     - ‘Extended’ are all the other fallacies. I may ask you about them in my own part of the exam.
>- Core fallacies are: Hasty generalisation, begging the question, equivocation, denying the antecedent, affirming the consequent and wishful thinking.

## A, B and C fallacies

>- The other fallacies in this presentation are specific to my class. You still have to learn them, but you will find that other sections might talk about other topics instead.
>- Because there are so many, I have graded the following pages into A (very important and core), B (important) and C (not that important).
>- Essentially, the letter is the reverse of the grade you want to get with fallacy questions: For an A, you’ll need A, B and C fallacies. For a B, you will need A and B fallacies. For a C, you will need only the A fallacies.
>- You could also see the letters as indicators of the probability that a question about a fallacy will be in an exam: A fallacies will very likely be asked in an exam; B fallacies are likely; and C fallacies are less likely (but not impossible!)

# Fallacies Group A

## A: Which are “acceptable” and “bad” arguments?

You can’t be serious that smoking should be banned completely! This must be a joke! On the one hand, the government has a lot of income from smoking. If smoking was banned, this income would be missing, more people would be unemployed, and they would go to the streets and become criminals. So if we ban smoking, the streets will be full of criminals and we will not be able to go out any more! Also, the black market for cigarettes will increase. And I know only one person who is actually against smoking, and this is a stupid, ugly guy, who has no friends. All cool people agree that banning smoking is a bad idea!

## A: *Acceptable* and **bad** arguments

**You can’t be serious** that smoking should be banned completely! This must be a joke! On the one hand, *the government has a lot of income from smoking.* If smoking was banned, *this income would be missing,* *more people would be unemployed,* and **they would go to the streets and become criminals.** So if we ban smoking, the **streets will be full of criminals and we will not be able to go out any more**! Also, *the black market for cigarettes will increase.* And I know only one person who is actually against smoking, and this is **a stupid, ugly guy, who has no friends.** All **cool people agree** that banning smoking is a bad idea!

## A: Fallacies

>- Fallacies are errors in argumentation.
>- In a fallacy, *the premises do not support the conclusion at all.*
>- Arguments containing fallacies are not valid, but sometimes they seem to be convincing.
>- Very often fallacies are made by mistake.
>- Sometimes they are made on purpose, when the speaker does not have any correct arguments for his cause.
  
## A: What is a fallacy?

We said that an argument must have reasons that are relevant to a conclusion:

\f{Argument = Relevant reasons + logical connections}

- Relevant reasons: Evidence, facts.
- Connections: Logical form of argument.

. . . 

In a fallacy, the reasons do not support the conclusion, because:

1. the evidence is missing, or 
2. the logical connection between reasons and conclusion is wrong.

## A: What do you think of this?

"If you buy me this golden ring, you are nice to me. If you are nice, people like you. So if you buy me this golden ring, people will like you."

## A: Wrong hypothetical syllogism

A valid hypothetical syllogism would be like this:

if A then B  
if B then C  
 --------------  
if A then C


## A: Wrong hypothetical syllogism

Sometimes, B is replaced in the second premise by another proposition, which looks similar, but is really different: 

if A then B1  
if B2 then C  
 --------------  
Wrong: if A then C

This is an invalid argument form!

## A: Wrong hypothetical syllogism

If you buy me this golden ring, you are nice (to me). If you are nice (to them!), people like you. (Wrong:) So if you buy me this golden ring, people will like you.

## A: Equivocation

When two words look the same but mean different things (“being nice” in our example)
we call this “equivocation.”

Equivocation can lead to a fallacy by confusing the connecting terms of an argument, like we saw above.

## A: Try to describe the problem!

Elena, the salesperson in an audio equipment shop, talks to a customer:

“I notice you've been listening mainly to those bipolar planar speakers. If you want to upgrade to them, you'll need high current, lots of power with pure class A biasing or maybe AB, and a quick slew rate. You can't get that in a receiver. Besides, you might want to biamp later on, which is a lot easier with separates.”

## A: Obscurity

>- “Obscurity” means to say something in such a way that the audience cannot understand what is being said.
>- In this case, the customer might be impressed because Elena seems to know so many strange words. But he is not convinced to buy the particular speakers because of reasons and an argument he understands.
>- An obscure argument does not provide useful evidence that the conclusion is correct, and is therefore a bad argument.
>- Obscurity is somewhat relative to the audience. An explanation in French would be obscure to me, but not to someone who speaks French. The same applies to technical terms, a doctor’s medical explanations and so on.


## A: Identify the problem!

If you're going to get a job in a competitive field, you've got to be good. If someone's good, that means they're honest, caring and loyal. Since I'm honest, caring and loyal, I'll easily get a job, no matter how competitive the field is.

## A: Equivocation “good”

>- Here, “good” in the first part of the argument means “good at your studies.”
>- “Good” in the second premise is used in the sense of “good as a friend” which is a different thing.
>- Therefore, this is a fallacy of equivocation. The two words look the same, but are used in a different sense!
>- The conclusion does not follow from the premises.

## A: Fallacies

“We have to stop the increase in MTR prices! Now they charge you 4 dollars, next year they will want 10, and the year after we will have to pay 100 or 1000 dollars for an MTR ride!”

## A: Slippery Slope

>- The Slippery Slope is a fallacy in which a person asserts that some event must inevitably follow from another without any argument for the inevitability of the sequence of events.
>- In our example, the fact that MTR ticket prices are increased, does not mean that there will be an increase to 100 or 1000 dollars any time soon.

## A: Another example:

“We have to stop the children from watching TV every day. Now they watch only for half hour every day, but next week it will be one hour, then two, and soon they will sit all day in front of the TV!”

## A: Fallacy?

"You should not begin to take drugs at all. In the beginning you will only take a small amount, but next time you will need a higher dose, and next time still more, and soon you will be unable to control your addiction and your whole life will be centered on drugs."

Is this a slippery slope fallacy?

## A: Fallacy?

Not a fallacy. Some slippery slopes are real.

In the case of drugs (or cigarettes) the biochemical and psychological mechanisms of addiction make it probable that the user will indeed need higher and higher doses of the drug in the future.


## A: What do you think of this?

- Teacher: “It is important that you study hard, in order to get a good grade.”
- Student: “Of course you would say that, you're a teacher. Therefore, I don't believe it.”

## A: Ad hominem

“Ad hominem” (an attack “at the person”) is a fallacy, in which a claim or argument is rejected on the basis of some irrelevant fact about the author of or the person presenting the claim or argument. 

## A: Ad hominem

- Teacher: “It is important that you study hard, in order to get a good grade.”
- Student: “Of course you would say that, you're a teacher. Therefore, I don't believe it.”

. . . 

>- The fact that a teacher says it, does not make it wrong!
>- Very often, it doesn't matter who says something. It can still be true or false.

## A: Now do it yourself!

Make up an argument which contains an ad hominem fallacy! 

## A: Fallacy?

A poor man on the street wants to sell you a paper in which he has written down his advice about a revolutionary new way to get rich that does not require any investment in advance, is available to all, and always works.

You think: “Why should I buy this? If this man really had a secret way of getting rich that worked, he would use it himself first, and he wouldn't be poor. So I don’t believe whatever is in that paper!”

Correct or fallacy?

## A: Correct or fallacy? (1)

Unclear.

>- Opinion 1: This is not an ad hominem fallacy, because the fact that this person is poor is *not* irrelevant to the question.
>- The conclusion that I don’t believe what that man says is supported by the observation that he is himself poor. (It is not necessarily true, but it has some support.)
>- An ad hominem fallacy is an attack that is based on an *irrelevant* fact about the person presenting an argument. For example, if I rejected his advice because he has blond hair, then this would be a fallacy.

## A: Correct or fallacy? (2)

>- Opinion 2: This is a fallacy. The man could have decided to be poor for other reasons, and this fact does not affect the possibility that his advice is good. One would have to judge the advice itself, not the man’s appearance.

>- But it seems that generally people don't *decide* to be poor. We can assume that usually someone who has the means to be rich would not decide to be poor. Therefore, there is a high probability that the advice is indeed useless.

## A: Fallacies

"If you don't vote for our party, we will not be able to protect you. All sorts of criminals will take control of the streets, and you will not be able to leave your house any more and feel safe. Your old parents will suffer as well as your children!"

## A: Appeal to Fear

>- Evidence in the argument is replaced with fear.
>- In our example, fear of criminals does not support the claim that I should vote for that party.
>- For example, they too might be unable to protect me.
>- Or some other party might do it even better than they claim.
>- Or it might be that crime is not a problem at all for any number of other reasons.


## A: Fallacy?

1. Your doctor tells you that if you don’t take this pill, your stomach pain will get worse.
2. A robber with a gun threatens to shoot you if you don’t give him your money.

## A: Fallacy?

Sometimes we are threatened in a way that constitutes a *conclusive* argument. Then the appeal to fear would *not* be a fallacy!

1. Your doctor tells you that if you don’t take this pill, your stomach pain will get worse.
2. A robber with a gun threatens to shoot you if you don’t give him your money.

These are valid threats that do support a conclusion, not fallacies!

## A: What is a fallacy?

>- A fallacy is an argument that is wrong (the premises do *not* support the conclusion), either because its *structure* is wrong, or because it replaces *evidence* in an argument by something else.
>- What this “something else” is, determines the type of the fallacy:
>      - If I replace evidence by fear, I have an appeal to fear.
>      - If I replace it by an implausible prediction of how things will go worse and worse in the future, I have a slippery slope fallacy, and so on.


## A: Fallacies

- Interviewer: “Your CV looks impressive but I need another reference.”
- Bill: “Jill can give me a good reference.”
- Interviewer: “Good. But how do I know that Jill is trustworthy?”
- Bill: “Oh, she certainly is. You can trust me on this.”

## A: Begging the Question

>- Begging the Question is a fallacy in which the premises include the claim that the conclusion is true or (directly or indirectly) assume that the conclusion is true. So the reasoning is circular.
>- (We talked about this before: Mary told me I’m her best friend, and this must be true, because Mary would not lie to her best friend.”)
>- In our example, the interviewer will have to trust Jill's reference on Bill, because Bill himself says that it is trustworthy. So Jill's reference is the reason to trust Bill, and Bill's word is the reason to trust Jill's reference.

## A: Begging the question

Another example:

- On the street, you get arrested by someone who claims to be a police officer, but he is not wearing a uniform.
- You: “How do I know you are a police officer?”
- The man: “Of course I am! How could I have arrested you if I were not?”

## A: Fallacies

Bill works at a newspaper. He is assigned by his editor to determine what most Hong Kong people think about a new law that will place a special tax on all computers. The money will be used to build new housing for the poor. Bill decides to use an email poll, sending emails to one million Hong Kong citizens. In his poll, 95% of 900,000 replies opposed the tax. Bill concludes that almost everybody is against this tax.

## A: What the problem is not

The sample size (900,000) is actually huge, and more than most surveys handle. You can have a good and valid survey with only 500 or 1000 people, so the sample size is not the problem here.

. . .

The conclusion from 95% to “almost everybody” is also not the problem. 95% of a class of 20 people would be all but one person. This is “almost everybody”!

## A: Biased Sample

This fallacy is committed when a person draws a conclusion about a population based on a sample that is biased or prejudiced in some way. 

## A: Biased Sample

In our example, Bill used an email survey, thus asking exclusively for the opinion of computer users, and these are the people who will have to pay the tax. 

The poor, who typically don't participate in email surveys, could not express their opinions in this survey, and these are the people who will likely profit from the tax.

So by choosing email as a medium, Bill is already determining the outcome of the survey!

## A: Now do it yourself!

Make up an argument which contains a biased sample! 

## A: Biased sample

Example: I go to McDonalds and ask the people who sit there whether they like to eat at McDonalds.

(If they didn’t like it, they wouldn’t be sitting there!)

Or I ask people in the MTR whether they like to take the MTR. (Those who really dislike it will not be there, because they’re taking other means of transport).

## A: Fallacy?

If I want to know whether students like Critical Thinking, I have to go to a Critical Thinking class and ask them. Does this mean that I am committing a fallacy?

. . . 

>- It depends.
>- If class attendance is mandatory, then the sample of the students present will be representative, because it will include both the students who like the class and those who don’t like it.
>- If, on the other hand, the students can choose freely to attend or not, then the sample would be biased, because the students who attend the class under these circumstances are likely to like it.

## A: Fallacy?

Similarly, if an island is connected with the mainland by only one ferry, and there is no bridge or other way to leave the island, then doing a satisfaction survey about the ferry service on the ferry would not necessarily be a fallacy. Because all people who live on the island and wish to leave it would have to take the ferry, and that would include those who don’t like the ferry service. So the sample would be representative in this respect.


## A: Fallacies

“We need nuclear power, else we have to burn coal for energy, the CO~2~ in the atmosphere will keep rising, and we will have an even more serious global warming.”

## A: Dilemma

>- *A dilemma is a forced choice between two undesirable outcomes.*
>- A false dilemma is a fallacy in which an alternative is presented and it is stated that either one or the other of two things have to be chosen. Often this is not true, because we have more options to chose from.
>- In our example, there is not only the alternative between nuclear power and global warming. One could, for example, use solar power, or wind power, or try to reduce power consumption.

## A: Real and false dilemma

>- Real dilemma: If I go out, I will get into the rain. If I stay at home, I will miss that party. I will either go out or stay at home. Therefore, I will get into the rain or I will miss that party.
>- False dilemma: If I marry Paul, I will be poor. If I marry Bill, I will be bored. I have to marry Paul or Bill. Therefore, I will be poor or bored.
>- This is false, because I could *not* marry at all; or I could marry someone else entirely.

## A: Now try it yourself!

Make up a dilemma, and then explain whether it is a real dilemma or a false one!

## A: Fallacies

In sports, I had been doing pretty poorly this season. Then my girlfriend gave me these new laces for my running shoes and I won my next three races. Those laces must be good luck...if I keep on wearing them I will always win!

## A: Correlation/causation

>- This fallacy is committed when it is concluded that one event causes another simply because the two events take place together or shortly after each other.
>- The speaker is confusing correlation (two phenomena have some statistical connection to each other) and causation (one phenomenon is the cause of the other).

## A: Correlation/causation

The mere fact that A occurs before B does not indicate a causal relationship!

For example, suppose Jill, who is in London, sneezed at the exact same time an earthquake started in California. Even if this happens multiple times, it would still be irrational to arrest Jill for starting a natural disaster, since there is no reason to suspect any causal connection between the two events.

## A: Correlation/causation

>- In our example, we don't need to assume that there is a causal relationship between the new laces and winning the race.
>- However, there might be a psychological cause: sometimes a strong belief might itself cause someone to succeed or fail in an enterprise.

## A: Another example:

>- There is a correlation between small shoe size and the length of one person’s hair. Can we conclude that small shoes cause long hair?
>- No. Women are more likely to have long hair than men, and women also have smaller shoes than men. But the two things, although they are statistically correlated, are not causally connected!

## A: Common causes

>- Sometimes correlations between two events can be better explained with *common causes* rather than direct causation between the two events.
>- If event A always occurs before event B, it is possible that:
>      - A causes B; or
>      - Both A and B are caused by another event C (in the case above, being a woman is the common cause for both having long hair and smaller shoes).

## A: Other examples of common causes

>- People who have expensive cars are more likely to have expensive watches. Not because the cars *cause* the watches, but because there is a common cause (having money and the need to display it).
>- More educated people live longer and healthier lives. (Not because education directly causes longer life, but because education allows people to understand how to live more healthily. It also normally leads to a higher income, which can then be used to make healthy lifestyle choices. Education is the common cause of both better understanding and higher income, and these two lead to longer and healthier lives.)

## A: What do you think of this?

“It would be terrible if the environmental destruction was so bad that all human life on Earth would be extinguished within the next two hundred years! I cannot imagine this happening! So there will surely be some way out. Perhaps the scientists will find some cure for the environmental problems.”

## A: Wishful thinking

Wishful thinking is a fallacy in which one believes X to be true because one does not want to believe not-X.

Of course, the truth of X or not-X does not depend on whether one wants to believe one of the two!

## A: Wishful thinking

Wishful thinking is very common:

>- In human relationships (“I cannot believe that my girlfriend doesn’t like me any more!”)
>- In international relations (“surely the government of country X will not turn into a dictatorship!”)
>- In everyday life (“I cannot imagine that it will be raining *again* on my birthday. So let’s plan to go hiking. The weather will be fine...”)


## Reference

All fallacies and many examples are from:

The Nizkor Project,
http://www.nizkor.org/features/fallacies

Originally by Dr. Michael C. Labossiere, ontologist@aol.com

## Any questions?

![](graphics/questions.jpg)\ 

## Thank you for your attention!

Email: <matthias@ln.edu.hk>

If you have questions, please come to my office hours (see course outline).

