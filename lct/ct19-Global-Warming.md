% Critical Thinking
% 19. Global warming and environmental justice
% A. Matthias

# The problem of global warming

## Note

*This presentation provides only a rough sketch of a few, selected points related to the ethics of global warming and climate change. The topic is huge, and such a short presentation cannot be exhaustive. You will find a lot more information on the Internet, in popular books, and in specialist literature.*

## References

Please read these sources as a preparation for the session. You can find them online at Google Scholar:

- Moellendorf, D. (2012). Climate change and global justice. Wiley Interdisciplinary Reviews: Climate Change, 3(2), 131-143.
- Peter Lee: Adaptation, Not Mitigation, The Realistic Climate Debate
Date: 27/12/14 Peter Lee, The Australian. Online: https://www.thegwpf.com/peter-lee-adaptation-not-mitigation-the-realistic-climate-debate/
- Hayes AW (2005). The precautionary principle. Arh Hig Rada Toksikol. 2005 Jun;56(2):161-6.


## The problem of global warming

>- How does it work?
>- Some gases (CO2, methane) trap the heat of sunlight and don’t let it leave the atmosphere.
>- As a result, the atmosphere heats up.
>- This often produces secondary effects (polar ice melting, release of CO2 from permafrost areas, destruction of forests, higher frequency of fires, expansion of deserts) that cause even more CO2 to be released into the atmosphere.
>- In this way, a positive feedback loop can accelerate the effects of global warming.

## Causes and effects of global warming (Wikipedia)


![](graphics/19-global-warming-causes-effects-wikipedia-200.png)\ 




## Sources of emissions for USA (Source: climatecentral.org)


![](graphics/19-greenhouse-gas-sources.jpg)\ 




## Why the phenomenon is disputed (1)

>- One of the problems with the global warming debate is that the processes involved are relatively slow and hard to observe, playing out on a timescale of decades or centuries.
>- Over such periods, we don’t have reliable climate data, and it is also difficult to predict climate changes over such long periods. No one really knows how to do that.
>- Even where data exist, stronger short-term effects can hide the more subtle long-term effects of climate change.


## Why the phenomenon is disputed (2)

>- For example, there have always been famines, floods, hot summers, cold summers, warm winters, snowy winters. These short-term variations are much more dramatic than the long-term rises of the average temperature that are in the order of 1-3 degrees Celsius.
>- Because of these difficulties to clearly *see* the effects of global warming, many people don’t believe that the phenomenon even exists.
>- Wishful thinking and economic profit additionally work against the necessary realisation that climate change is both real and threatening.
>- Here, the *precautionary principle* can help.


## Precautionary principle

> “When an activity raises threats of harm to human health or the environment, precautionary measures should be taken even if some cause and effect relationships are not fully established scientifically. In this context, the proponent of an activity, rather than the public, should bear the burden of proof. The process of applying the Precautionary Principle must be open, informed and democratic and must include potentially affected parties. It must also involve an examination of the full range of alternatives, including no action.” (Hayes 2005).

## The problem (1)

Several competing elements of the carbon dioxide dilemma (Lee 2014):

>- Greenhouse gas emissions caused by humans are at record levels;
>- the climate is changing;
>- the global economy wants to keep growing;
>- populations demand higher living standards;
>- wealth creation emits carbon dioxide;
>- renewable energy sources require significant financial subsidies and reduce economic competitiveness against those who do not use them.

## The problem (2)

Further complications (Lee 2014):

>- democratically elected leaders cannot sustain unpopular climate policies in the face of opposition from voters;
>- wealthy countries use more energy than poor ones but the growth of energy use is greatest in developing countries;
>- in 2010 one fifth of the global population was living on less than USD 1.25 per day (down from two fifths in 1990); 
>- fossil fuels are much cheaper to use than renewables (not necessarily true any more -- a.m.);
>- new technologies like fracking have increased the amount of potentially available oil and gas over the next century (but they come with their own ecological problems -- a.m.);
>- many environmentalists and environmentalism groups, especially in Europe, are vociferously opposed to fracking regardless of any potential economic or energy security benefits.


## The difficulty of monetising climate change

>- Global effects.
>- Many regions don’t have enough data.
>- Some areas don’t even have monetised economies (north pole, Amazon rainforest)
>- Diversity of effects (agriculture, fishing, forestry, tourism, cities, migration, ...)

## Responsibility and moral issues

>- Responsibility for past
>- Allocating future emissions
>- Ability to pay
>- Grandfathering (acceptance of existing situation and past practices)
>- Emissions entitlements
>- Right to (sustainable) development
>- Costs of adaptation and mitigation


## Different kinds of countries (Pollution)

>- The names of the countries given here are just for the purposes of giving commonly known, illustrating examples. They have not been researched further and might not actually fall into exactly the neat categories that we’re using for this discussion.
>- Historically polluting, presently willing to reduce emissions (early industrialised and now wealthy countries like England, Germany, Denmark, ...)
>- Historically polluting, presently also and unwilling to reduce (USA, Australia)
>- Historically not polluting, but presently growing and overtaking industrialised countries (China, India)
>- Neither historically nor presently significantly polluting (some small Pacific island nations)

## Different kinds of countries (Wealth)

>- Early industrialised, historically and presently wealthy countries (Western Europe, USA)
>- Late industrialised, but wealthy countries (China)
>- Late industrialised, historically poor and presently growing countries (India)
>- Late or barely industrialised, historically and presently poor countries
>- Not yet industrialised

## Different kinds of countries (Effects)

In the short term (effects due to failing agriculture, storms, desertification, fires and droughts, or flooding):

>- Strongly affected countries (Pacific islands, Middle East)
>- Somewhat affected countries (coastal areas, Australia)
>- Not very much affected countries (North Europe, Scandinavia, Russia, Canada)

## Different kinds of countries (Mitigation)

>- Able to mitigate negative effects without external help
>- Able to mitigate negative effects with external help
>- Not able to mitigate negative effects (with catastrophic consequences)


## How to measure pollution/CO2 emissions?

>- Per capita? (USA worst)
>- Total for a country? (China worst)
>- Historical total for the country?
>- Historical total per capita?
>- Future projection of needs until a particular stage of development reached? (“Development rights” approach)


## Financing options

>- World Climate Change Fund. Equal voice in governance for all countries. 
>- Global carbon tax.
>- Auctioning international emissions permits and distributing the proceeds to developing countries.


## A right to develop?

>- Since industrial development is based on CO2 emissions, the right of a country to create such emissions is, to some extent, a right to develop.
>- Underdeveloped countries will have more difficulty developing if they are not allowed to emit CO2 freely.
>- This creates a historical injustice.
>- Should not all countries have the same rights to develop?
>- Arguments?

. . . 

>- The same could be said of slavery: some countries (e.g. the USA and the British Empire) historically have profited from slavery. Does this mean that now all other countries have a right to also employ slavery?
>- No. Advantages gained in criminal or immoral ways cannot be claimed by others.
>- Compare: “My friend cheated in the exam. Now I also want the right to cheat.”

## Carbon offsets (Source: unenvironment.org)

![](graphics/19-carbon-offset.png)\ 





## Carbon offsets

>- The problems
>     - Cheating
>     - Counting of old infrastructure as new
>     - Offsetting for rich countries often happens in poor countries (offsets export trade), thus increasing the burden on poor countries.
>- Here is a short, introductory video about the problems of carbon offsets:
<https://www.youtube.com/watch?v=xdW-6MXB0sI>
>- Here is a somewhat longer, more critical video: <https://www.youtube.com/watch?v=WRNd6K8kS4M>



## Ways of adaptation / Effects of global warming

>- Migration, abandoning of flooded or desertified areas
>- Flooding of coastal cities (Hong Kong!)
>- Spread of tropical diseases
>- Extinction of animals and insects
>- Change of growing seasons and plant choices in agriculture
>- Spread of new plant diseases
>- Water scarcity
>- Health (infectious diseases, cardiovascular diseases)
>- Changes in energy availability (some populations dependent on wood, wind, water, which might change). Our society dependent on oil, which might not be usable soon.
>- Intensive agriculture dependent on fossil fuels.


# Moral theories

## Justice and equality

>- Equality: Should all nations and all men on Earth be treated equally?
>     - What does this mean?
>     - If people have different needs, should they be treated strictly equally, or according to their needs (and potentially unequally)?
>     - For example, if the government has 1 million dollars to give away, should every citizen get the same amount, or should each one get an amount inversely proportional to their present wealth?


## Backward-looking justice

>- In “backward-looking” conceptions of justice, we look to the past and try to provide compensation for past wrongs.
>- In the case of climate ethics, we would give an advantage (financial compensation, more CO2 quota, technological assistance) to countries that have polluted less in the past.
>- But is this just?
>- For example, why should the citizens of present-day Germany be punished for the over-use of resources by their great-grandfathers?
>- Possible answer: Because they still enjoy the benefits of that over-use of resources (better development, more technology, wealthier lives).


## Backward-looking justice: Problems (1)

>- Backward-looking justice can sometimes feel unjust.
>     - For example, because the past 50 years we had men as professors, now we should have 50 years of woman as professors. So no man will get a chance to become a professor for the next two generations.
>     - This does not seem perfectly just. Why should the next generation’s men, who are innocent and have not participated in the initial injustice, be punished in this way?


## Backward-looking justice: Problems (2)

>- Immanuel Kant (1724-1804): Every human being has the same, absolute value. Every single person should be treated as an end, and not only as the means to some other end.
>     - End: The goal of an action. Means: the instrument used to achieve some end.
>- In trying to “undo” past wrongs, we would be treating present human beings only as means to the end of establishing justice. This is immoral, according to Kant.


## Forward-looking justice

>- Here we look towards equal chances for the future, rather than trying to compensate for past wrongs: What can we do to eliminate the injustice from now on and in the future?
>- For CO2 emissions, we should establish “equal” emission rights.
>- But what is “equal”?
>     - Absolute amounts of CO2?
>     - Amounts relative to population size?
>     - Amounts relative to development needs?
>     - Amounts relative (inversely?) to present technological status?
>     - Considering available renewable resources?
>     - Considering a country’s wealth?
>- It looks pretty hard to do this right, too.

## Rawls’ idea of justice (1)

>- John Rawls (1921-2002), American philosopher: “A Theory of Justice” (1971).
>- Original position (explained differently from Rawls): 
>     - Imagine you are conscious before you are actually born as a particular individual.
>     - Now you look down on Earth and try to create rules for a just society, not knowing as who of all those people down there you will be born (man, woman, poor, rich, Chinese, American or Greek).
>     - This is called the “veil of ignorance”. Veil: a kind of curtain that hides the (future) reality from your view.
>- Rawls: If we think and create rules in this way, we are more likely to create a truly just society.


## Rawls’ idea of justice (2)

>- From this original position, we would arrive at the following principles of justice:
>- The Liberty Principle: Equal basic liberties for all citizens: freedom of conscience, association, expression and personal property.
>- Fair Equality of Opportunity: offices and positions should be open and *effectively* accessible to all.
>- The Difference Principle: Any inequalities are only permitted if they work to the advantage of the least well-off.


## Rawls’ idea of justice (3)

>- *Think how we can apply these principles to CO2 emissions and global warming justice!*
>- When we talk about how to regulate CO2 emissions, we should do so from “behind the veil,” that is, assuming that we don’t yet know as who we will be born on this Earth.
>- So you wouldn’t try to regulate CO2 emissions knowing that you are a German teacher, an Indian programmer, a US farmer, or a Chinese president. 
>- You would assume that you might be in any of these roles, having to deal with the consequences of your regulation. 
>- How would you regulate then???

## Rawls’ idea of justice (4)


>- The Liberty Principle: Equal basic liberties for all citizens: freedom of conscience, association, expression and personal property.
>     - Applied to environmental questions, we see that personal liberty often goes against the benefit of all.
>     - My liberty to own and drive a car, or fly to a holiday destination by airplane, causes many environmental problems that others will have to bear the consequences of.
>     - So the liberties must really be restricted to the most basic ones, while liberties that are more likely to cause harm to society at large should perhaps be restricted.

## Rawls’ idea of justice (5)


>- Fair Equality of Opportunity: offices and positions should be open and *effectively* accessible to all.
>     - In terms of global warming, we might understand this to mean that access to CO2 emissions (and therefore cheap development options) should be available equally to all. It cannot only be a privilege of rich countries, or only allowed to poor countries.
>- The Difference Principle: Any inequalities are only permitted if they work to the advantage of the least well-off.
>     - This principle breaks the symmetry of the previous one. If we need to restrict emissions, we should always restrict in favour of the poorer, least well-off countries. That means, richer countries should contribute more towards the mitigation of global warming than poorer countries.


## Utilitarianism (1)

>- Utilitarianism would ask us to look at the benefit of the majority, and to weight benefits and harms against each other.
>- For example: What is the harm of CO2 emissions?
>     - Global warming leads to flooding, changing climate that affects crops and may cause famines, it will lead to people losing their homes, potentially to more international conflicts, more violent storms etc.


## Utilitarianism (2)

>- On the other hand, what is the benefit of CO2 emissions for the emitter?
>     - To a large extent, it is luxury items and convenience: a private car, a big house, many manufactured items that are convenient or trendy but not essential for life, holidays using airplanes, a meat-based diet instead of a vegetable-based one, etc.
>- Accordingly, utilitarianism would probably conclude that we should give up these luxuries and conveniences in order to mitigate the existential, harmful effects of global warming.

## Utilitarianism (3)

>- But you’d also have to look at the number of people involved. How many suffer from the effects vs how many benefit?
>- It seems that the harms will affect many people (floods, famines, storms, diseases), while the benefits of excessive CO2 emissions are concentrated mainly on the more developed countries, which have smaller populations.

## Utilitarianism (4)

>- But it becomes more difficult to judge when you consider the right of less developed nations to develop. Since the less developed nations, taken together, have a higher population than the developed nations, would a cap on CO2 emissions cause more harm to more people by hindering development of these underdeveloped regions?


## Other perspectives

>- This presentation provides just some discussion-starters about the ethics of global warming.
>- For your own debate contributions, please go out and find your own, additional materials. You are *not* supposed to just copy from this presentation.
>- You could, for example, talk more about the science of global warming; the consequences for particular groups; the views of Social Contract or Virtue Ethics theories; about alternative proposals to deal with the problem; and so on.



## Any questions?

![](graphics/questions.jpg)\ 

## Thank you for your attention!

Email: <matthias@ln.edu.hk>

If you have questions, please come to my office hours (see course outline).





