% Critical Thinking
% 9/10. More Natural Deduction and Other Logic Exercises
% A. Matthias

# Where we are

## What did we learn last time?

- Validity in propositional logic.
- Natural deduction.

## What are we going to learn today?

- More natural deduction
- More logic exercises

# Rules for all five operators

## In-Out-Rules

In order to do natural deduction more systematically, we need to be able to transform all four basic logical operators (we don't need to deal with the biconditional separately, because it is just a conjunction of two implications). We therefore need rules to:

>- Introduce and eliminate conjunction (\&I and \&O).
>- Introduce and eliminate disjunction (vI and vO)
>- Introduce and eliminate implication ($\to$I and $\to$O)
>- Introduce and eliminate negation (\~I and \~O)

. . .

All O-(out-)rules are sometimes also called *elimination rules.* The O then becomes an E.

So \&O, \&E, E\&, E$\land$, $\land$E, $\land$O all mean the same thing. Different authors just write them differently.

We will only look at *some* of those.

## Conjunction-in

This we already know:

\f{A, B $\vDash$ A\&B (\&I)}

## Conjunction-out

This is also easy:

\f{A\&B $\vDash$ A (\&O)}

\f{A\&B $\vDash$ B (\&O)}


## Disjunction-in

Disjunction-in is pretty trivial. If I know that A, then I also know that A$\lor$B:

\f{A $\vDash$ A$\lor$B (vI)}

Since A is true in this case (premise), A$\lor$B must also be true, no matter what the truth value of B is!

## Disjunction-out

This is what we called the *disjunctive syllogism* above:

\f{A$\lor$B, $\sim$A $\vDash$ B (vO)}

\f{A$\lor$B, $\sim$B $\vDash$ A (vO)}

Clearly, if A$\lor$B is known to be true (premise), and B is false, then A must be true (otherwise A$\lor$B could not possibly be true!)

## Implication-in

We will not discuss this here. See the HKU Think resource for an introduction.

## Implication-out

This one is easier. Implication elimination we know already as *modus ponens* or *affirming the antecedent:*

\f{A$\to$B, A $\vDash$ B ($\to$O)}

## Negation-in

We will not do this now, because it is not required for this course.

## Negation-out

Negation-out just means eliminating double negations:

\f{$\sim\sim$A $\vDash$ A ($\sim$O)}

## How to use these rules

>- In principle you could construct proofs for every provable statement out of these elementary proofs. But, of course, this would be a lot of work.
>- This is why we already assume that we know that some more complex equivalences hold (for example, the deMorgan laws).
>- And therefore we can shorten and simplify our proofs by assuming that we have already proven these equivalences.

## Exercise

Show that: P, P$\to$Q $\vdash$ P\&Q

. . .

------------------------------------------
P, P$\to$Q $\vdash$ P\&Q
------------------------------------ ----------------
1. P	          	                             Premise

2. P$\to$Q                              Premise

3. Q	          	                            1, 2, $\to$O

4. P\&Q	          	                     1, 3, &I
------------------------------------------

## Exercise

Show that: (P\&Q)$\to$R, Q$\to$P, Q $\vdash$ R

. . .

------------------------------------------
(P\&Q)$\to$R, Q$\to$P, Q $\vdash$ R
------------------------------------ ----------------
1. (P\&Q)$\to$R	    	                             Premise

2. Q$\to$P				                              Premise

3. Q	          		                           Premise

4. P					                      2, 3, $\to$O

5. P\&Q                               3, 4, \&I

6. R					                        1, 5, $\to$O
------------------------------------------


## Common valid argument patterns (review)
1. Affirming the antecedent: P$\to$Q, P $\vDash$ Q
2. Denying the consequent: P$\to$Q, \~Q $\vDash$ \~P
3. Hypothetical syllogism: A$\to$B, B$\to$C $\vDash$ A$\to$C
4. Disjunctive syllogism: PvQ, \~P $\vDash$ Q and PvQ, \~Q $\vDash$ P
5. Dilemma: P$\to$Q, R$\to$S, PvR $\vDash$ QvS
6. Conjunction-In: P, Q $\vDash$ P$\&$Q
7. Conjunction-Out: P$\&$Q $\vDash$ P and P$\&$Q $\vDash$ Q
8. Double negation: P $\equiv$ \~\~P
9. De Morgan’s laws \~(PvQ) $\equiv$ \~P$\&$\~Q and \~(P$\&$Q) $\equiv$ \~Pv\~Q
10. Contraposition P$\to$Q $\equiv$ \~Q$\to$\~P (denying the consequent)
11. Conditional-disjunction exchange P$\to$Q $\equiv$ \~PvQ

## Exercise

*Show whether this is valid:*

I went to my Critical Thinking class instead of having a tea, although I knew that if I go to the Critical Thinking class I will be bored by the logic part. So now I am in the Critical Thinking class and I’m bored by the logic part.

## Exercise

*Show whether this is valid:*

I went to my Critical Thinking class (C) **instead of having a tea (T)**, although I knew that if I go to the Critical Thinking class I will be bored (B) by the logic part. So now I am in the Critical Thinking class (C) and I’m bored (B) by the logic part.

. . .

C\textbf{\&$\sim$T}  
C$\to$B  
 ---------  
C\&B

We can in principle ignore the bit about the tea (boldface above), because it appears only once and does not influence the proof.

## Natural deduction proof

C\&$\sim$T, C$\to$B $\vdash$ C\&B

--------------------------------------   ----------------
1. C\&$\sim$T						     Premise
2. C								     1, \&O
3. C$\to$B							     Premise
4. B								     2, 3, MP (same as $\to$O)
5. C\&B								     2, 4, \&I
--------------------------------------   ----------------


## How to think about these exercises

We did this exercise in the last session. Now have another look at it, and try to solve it “backwards,” by assuming that the conclusion (P) is true.

P$\lor$$\sim$Q$\lor$R  
R$\to$$\sim$S  
S\&Q  
 ---------------  
P

## One way to think about it

P$\lor$$\sim$Q$\lor$R, R$\to$$\sim$S, S\&Q $\vdash$ P

>- I want to show that P. Therefore, I have to begin with a premise that contains P: P$\lor$$\sim$Q$\lor$R.
>- If I am to conclude that P, then $\sim$Q$\lor$R must be shown to be false. Then, since the premise is assumed to be true, P must be true (disjunctive syllogism).
>- $\sim$Q$\lor$R will be false if both $\sim$Q is false and R is false.
>- I know that S\&Q is true (premise), so both S and Q must be true (\&O). If so, then $\sim$Q must be false.
>- Now I must show that R is false. I know that R$\to$$\sim$S is true (premise). I also know that S is true (see previous step). Then $\sim$S is false. If $\sim$S is false and R$\to$$\sim$S is true, then R must be false too!
>- So now I know that $\sim$Q$\lor$R is false.
>- Therefore, P must be true.

## P$\lor$$\sim$Q$\lor$R, R$\to$$\sim$S, S$\&$Q $\vdash$ P

In order to write it down, we have to re-order it:

--------------------------------------- ----------------
1. S$\&$Q                               premise
2. S                                    1, \&O
3. Q                                    1, \&O
4. \~\~S                                2, DN
5. R$\to$\~S                            premise
6. \~R                                  4, 5, MT
7. Pv\~QvR                              premise
8. Pv\~Q                                6, 7, DS
9. \~\~Q                                3, DN
10. P                                   9, 8, DS
--------------------------------------- ----------------


# Natural deduction exercises from old exams

## Translate and show validity (1)

Translate the following into propositional logic and show, using natural deduction, that it is valid:

"Jack and Jill went up the hill. Jack fell down and broke his crown. Jack did not fall down. Therefore, Jill did not come tumbling after."

. . .

>- Don't be confused by the words. You don't need to understand the text!
>- P: Jack and Jill went up the hill. Q: Jack fell down. R: Jack broke his crown. S: Jill came tumbling after.

. . .

P, Q\&R, $\sim$Q $\vdash$ $\sim$S

## Translate and show validity (2)

P, Q\&R, $\sim$Q $\vdash$ $\sim$S

--------------------------------------- ----------------
1. Q\&R                                   premise
2. Q                                    1, \&O
3. $\sim$Q                                    premise
4. $\sim$Q$\lor$$\sim$S                         3, vI
5. $\sim$S                                     2, 4, DS
--------------------------------------- ----------------

. . .

What happens here is that *you can derive anything from a contradiction!* (2) and (3) are a contradiction, and, together with a disjunction, which you can always add to anything, you can prove anything you want.

Remember: *You can derive anything from a contradiction!*

## Any conclusion follows from a contradiction

Here is the general way how this works:

-------------------------------- ----------------
1. A\&$\sim$A                       (premise, contradiction)
2. A		                         (1, \&O)
3. $\sim$A	                         (1, \&O)
4. AvB		                        (2, vI)
5. B		                        (3, 4, DS)
-------------------------------- ----------------

This shows that from a contradictory premise one can by deduction prove *any* statement!

## Any conclusion follows from a contradiction

Thus:

Premise 1: Bananas are yellow  
Premise 2: Bananas are not yellow  
Premise 3: Bananas are yellow or Hong Kong is in Japan  
Conclusion: Hong Kong is in Japan

The conclusion here *does* follow from the premises! The argument is valid.

## Proof

Use natural deduction to prove that:

((P\&Q)\&S), (P\&S)$\to$$\sim$R, (R$\lor$T) $\vdash$ T.

. . .

Easy to see. This is more about how to write things down correctly.

## Proof

Show, using natural deduction, that the following is valid: Q\&(T$\lor$B), P$\lor$R, Q$\to$S, S$\to$$\sim$R, $\sim$(P\&Q)$\lor$T $\vdash$ T.

. . . 

--------------------------------------- ----------------
1. Q\&(T$\lor$B)	                         Premise
2. P$\lor$R			                                Premise
3. Q$\to$S			                                 Premise
4. S$\to$$\sim$R	                        Premise
5. $\sim$(P\&Q)$\lor$T                    Premise
6. Q				                                       1, &E
7. S				                                       6, 3, $\to$E (MP)
8. $\sim$R			                             7, 4, $\to$E
9. P				                                       2, 8, vE (DS)
10. P\&Q			                             9, 6, \&I
11. T				                                   5, 10, vE (DS)
--------------------------------------- ----------------

## Proof (1)

Consider the following argument:

"God is a being of which nothing greater can be conceived. We understand the term *God.* If we understand this term, then God exists in the understanding. If God exists in the understanding, but not in reality, then God is not a being of which nothing greater can be conceived. Therefore, God exists in reality." -- Formalize this in the language of sentential^[Sentential logic = propositional logic.] logic and construct a proof of this argument!

## Proof (2)

N: God is a being of which nothing greater can be conceived.  
U: We understand the term God.  
E: God exists in the understanding.  
R: God exists in reality.

1. N
2. U
3. U$\to$E
4. (E\&$\sim$R)$\to$$\sim$N
5. Conclusion: R (?)

## Proof (3)

N: God is a being of which nothing greater can be conceived.  
U: We understand the term God.  
E: God exists in the understanding.  
R: God exists in reality.

--------------------------------------- ----------------
1. N					                Premise
2. U					                Premise
3. U$\to$E				                Premise
4. (E\&$\sim$R)$\to$$\sim$N               Premise
5. $\sim$(E\&$\sim$R)	                1, 4, MT
6. $\sim$E$\lor$R		                5, DeM
7. E					                2, 3, MP
8. R					                6, 7, DS
--------------------------------------- ----------------

## What to learn from this

- If the exercise is about validity, generally don't try to *understand* the argument with common sense. You don't even need to understand the words.
- Trying to figure out the meaning will just cost you time.
- Just translate the argument into formal logic (propositional or predicate logic) and then prove its validity formally.
- The same applies to conditional arguments, which we talked about earlier in this course.

## Inference (1)

What can be validly inferred from the following premises?

- (P$\lor$Q)$\to$$\sim$R
- P\&S
- T$\to$R

1. R
2. T
3. $\sim$T\&S
4. P$\to$R

. . . 

3 (explanation on next page).

## Inference (2)

--------------------------------------- ----------------
1. (P$\lor$Q)$\to$$\sim$R               Premise
2. P\&S					                Premise
3. T$\to$R				                Premise
4. P					                2, \&E
5. $\sim$R				                1, 4, MP
6. $\sim$T				                3, 5, MT
7. S					                2, \&E
8. $\sim$T\&S			                6, 7, \&I
--------------------------------------- ----------------


# Other logic exercises from old exams

## Valid?

Is the following valid?

$\sim$(P$\to$Q)$\lor$R, ($\sim$R\&P) $\leftrightarrow$ $\sim$Q $\vdash$ R

Use a truth table.


## Entailment

If P entails Q, then:

A. P is inconsistent with $\sim$Q  
B. P is consistent with $\sim$Q  
C. If Q entails R, P is consistent with $\sim$R  
D. All of the above

. . .

Answer: A.

## Translation

"Our opinions differ on this matter, and one and only one of us is correct." -- Which is the correct translation of this sentence? (O: Our opinions differ in this matter; Y: You are correct; I: I am correct)

A. O\&(Y$\lor$I)  
B. O\&(I$\leftrightarrow$$\sim$Y)  
C. O\&$\sim$(Y\&I)  
D. O\&(I$\leftrightarrow$Y)  

. . .

Answer B.

## Evaluation/model

Which of the following is an assignment of truth values (evaluation/model) on which
((P$\to$P)$\to$P) $\to$ ((P$\to$R)$\to$Q) is *false?*

A. P=T, Q=F, R=T  
B. P=T, Q=T, R=T  
C. P=F, Q=F, R=F  
D. P=T, Q=F, R=F  

. . .

A.

## Translation and validity

"If the world is disordered, it cannot be reformed unless a sage appears. But no sage can appear if the world is disordered. It follows that the world cannot be reformed if it is disordered." -- Show the validity of this argument.

. . .

D: World is disordered. R: World can be reformed. S: A sage appears.

D$\to$($\sim$S$\to$$\sim$R)  
D$\to$$\sim$S  
 ------------------------------  
D$\to$$\sim$R

The easiest way to show this is with a truth table. A natural deduction solution would require an implication-in, which needs an assumption. We have not discussed this in this class. You can see how to do this in the HKU Think online resource (but you don’t need to).

## Tautologies

![](graphics/13-exam-1.png)\ 

. . .

All of the above.

## Entailment

![](graphics/13-exam-2.png)\ 

. . .

X entails Y if: When X is true, Y has to be true. *For which expression is this the case?*

. . . 

This is the case only for (S$\lor$$\sim$S) because this is *always* true, and therefore it is also true if the premise is true. Every expression entails a tautology!

## Joint entailment

![](graphics/13-exam-3.png)\ 

. . .

Q (obviously), since $\sim$Q is a premise. No expression can entail its negation (except if the expression is a contradiction, and the negation is, therefore, a tautology).

## Translation

![](graphics/13-exam-4.png)\ 

. . .

Answer C. You can also see this because it is the *only* pair of formulas where the two parts of the pair are actually logically equivalent! (DeMorgan law!)

## Consistency and entailment (1)

![](graphics/13-exam-5.png)\ 

. . .

About all the answers: Assuming P: "you love me"; Q: "I love you."

>- Then statement (1) is P$\lor$$\sim$P. This is a tautology.
>- Entailment: If the premises are true, the conclusion *must* be true. If the conclusion is a tautology, it is *always* true (this is what tautology means).
>- Therefore, *every statement entails a tautology.*
>- The correct answer is B.
>- Since only one answer can be correct, we are done.

## Consistency and entailment (2)

![](graphics/13-exam-5.png)\ 

- For completeness: A tautology cannot be inconsistent with any statement (except for a contradiction), since every contingent statement is true sometimes, and the tautology is true always, that is, surely also at the same time as the other statement.

## Is the following valid? Examine it with a truth table.

![](graphics/13-exam-6.png)\ 

. . . 

>- You can see that PvQ must be true (premise 1).
>- If P is true, then Q must be true (premise 2). So either P and Q are true, or only Q.
>- In any case, Q is true. Therefore, (RvQ) is true (premise 3).
>- But (RvQ) will always be true when Q is true, no matter what the R is.
>- Therefore, it can happen that the R is actually false, although all premises are true.
>- Therefore, the argument is *not* valid.

## Entailment and consistency

Suppose S1 is the formula (P$\lor$Q) and S2 is the expression:
(((P\&R)$\lor$(Q\&R))$\lor$((P\&$\sim$R)$\lor$(Q\&$\sim$R))).

Decide, by means of a truth table, which of the following logical relations hold between S1 and S2 and which do not hold (multiple answers possible!):

1. S1 entails S2.
2. S2 entails S1.
3. S1 is logically equivalent to S2.
4. S1 is inconsistent with S2.

. . .

The two expressions are equivalent, and so they also entail each other. The right answer is therefore that the logical relations 1, 2, and 3 hold; but 4 is not the case.

## Validity (1)

![](graphics/13-exam-8.png)\ 

Is this argument valid? Explain your answer!

. . .

The easiest way to do this is with a Venn diagram!

## Validity (2)

![](graphics/13-exam-8.png)\ 

\begin{venndiagram3sets}[labelA={Lawful}, labelB={Respect}, labelC={Rebels}]
\fillOnlyA
\fillACapC
\setpostvennhook{
\draw[<-] (labelOnlyBC) -- ++(0:2.5cm)
node[right,text width=4cm,align=flush left]
{Something could be here, so the argument is not valid!};}
\end{venndiagram3sets}

## Tools for checking entailment

Generally, you have three tools for checking entailment (or validity):

1. Truth tables (for propositional logic)
    - Work always but a lot of effort to apply to complex expressions.
2. Natural deduction (for propositional logic)
    - You might not always be able to derive the result you want, but much faster than truth tables *if* you can do it.
3. Venn diagrams (only for syllogisms)
    - You *have* to use Venn diagrams when your argument is about syllogisms with quantifiers (‘all’, ‘some’).

Choose wisely which tool to use for every exercise!

## Entailment and consistency

S1: ((P$\lor$Q)$\to$R)  
S2: (R$\to$$\sim$(P\&Q))

Use a truth table to find out which is true:

1. S1 entails S2
2. S2 entails S1
3. S1 is inconsistent with S2

## Assignment of truth values

Which of the following is an assignment of truth values on which

(P$\to$$\sim$P) $\leftrightarrow$ ((P\&Q)$\to$$\sim$Q) is *false?*

1. P=T, Q=T
2. P=T, Q=F
3. P=F, Q=T
4. P=F, Q=F

## Consistency

![](graphics/13-exam-9.png)\ 

. . .

*Inconsistent* means that when (P$\lor$Q) becomes true, the other expression should always be false. That is, it should be false if either P, or Q, or both are true. (Use a truth table if not sure.)

Also, using the conditional-disjunction-exchange rule, we know that P$\lor$Q $\equiv$ $\sim$P$\to$Q. Therefore, option C is clearly inconsistent, because it is the negation of that.

## Translation (1)

"If neither the first witness nor the second witness is telling a lie, then the third witness must be mistaken or her statement is not inconsistent with either that of the first or that of the second witness."

F: The first witness is telling a lie.
S: The second witness is telling a lie.
T: The third witness is mistaken.
P: The third witness' statement is inconsistent with that of the first witness.
Q: The third witness' statement is inconsistent with that of the second witness.

![](graphics/13-exam-10.png)\ 

## Translation (2)

"If neither the first witness nor the second witness is telling a lie, then the third witness must be mistaken or her statement is not inconsistent with either that of the first or that of the second witness."

Rephrase and expand:

>- If the first witness is not telling a lie AND the second witness is not telling a lie \~F\&\~S $\equiv$ \~(F$\lor$S)
>- then the third witness must be mistaken: T, OR
>- her statement is not inconsistent with that of the first OR her statement is not inconsistent with that of the second: \~P$\lor$\~Q $\equiv$ \~(P\&Q)

## Translation (3)

- If the first witness is not telling a lie AND the second witness is not telling a lie \~F\&\~S $\equiv$ \~(F$\lor$S)
- then the third witness must be mistaken: T, OR
- her statement is not inconsistent with that of the first OR her statement is not inconsistent with that of the second: \~P$\lor$\~Q $\equiv$ \~(P\&Q)

![](graphics/13-exam-10.png)\ 

. . .

The right answer is D.

## True and False

Suppose a formula containing P and Q is true if and only if both P and Q are false. Which of the following is logically equivalent to the formula?

1. (($\sim$P$\lor$$\sim$Q)\&(P$\lor$Q))
2. (($\sim$P$\lor$$\sim$Q)\&($\sim$P$\lor$Q))
3. (($\sim$P\&$\sim$Q)\&$\sim$($\sim$P$\to$Q))
4. (($\sim$P\&$\sim$Q)$\lor$(P\&Q))

. . .

>- Which of the options is true if and only if both P and Q are false?
>- Option (a) would be false if both were false because of ... \&(P$\lor$Q).
>- Option (b) would be true if P=T and Q=F.
>- Option (c) would be true if both P and Q were false.
>- Option (d) would also be true if both were true.
>- So the answer is C.

## Translation

A: Andy Warhol is a great artist. W: Ai Wei Wei is a great artist. E: One should go to the Andy Warhol and Ai Wei Wei exhibition in Melbourne. M: One should go to Melbourne.

"If Andy Warhol and Ai Wei Wei are not both great artists, it is not true that one should go to the Andy Warhol and Ai Wei Wei exhibition in Melbourne."

![](graphics/13-exam-11.png)\ 

. . .

Answer B.

## Joint entailment

![](graphics/13-exam-12.png)\ 

. . .

Answer D.

## New connective

![](graphics/13-exam-13.png)\ 

Consider the two formulas:

1. (P\*Q) \* Q
2. Q \* (Q\*P)

Does (1) entail (2)? Does (2) entail (1)? Use a truth table to justify your answers.

## Consistency

Two of the following four formulas are *not* logically consistent with each other. Use a truth table to determine which.

![](graphics/13-exam-14.png)\ 

## Valid argument

Arrange the following formulas into a valid argument form. That is, choose one of them to be the conclusion and the other two to be the premises, so that the resulting argument form is valid. Draw a truth table to verify your answer.

![](graphics/13-exam-15.png)\ 

## Validity

Consider the following argument:

Premise: All Fs are Gs  
Premise: Some G is an H  
Premise: All Hs are Ks  
 -------------------------------------  
Conclusion: All Fs are Ks

Is this valid? If not, would it become valid if we added one of the premises:

- No G is not an H; *or*
- Some F is a K?

. . .

This is a *thinking* exercise. We don't have the tools to solve it formally, but we can easily see the solution.

If all Fs are Gs, and all Hs are Ks, then for the conclusion to be true (all Fs are Ks), it would be necessary that all Gs are Hs. The second premise is not sufficient for that. But the additional premise: No G is not an H means exactly that: All Gs are Hs. And so the argument becomes valid by adding the first of the two additional premises.

## Translation

"We will go to Ocean Park for your birthday if you want to go, unless it's raining or you fail your Critical Thinking final exam."

O: We will go to Ocean Park; W: You want to go to Ocean Park; R: It's raining; F: You fail your Critical Thinking final exam.

![](graphics/13-exam-17.png)\ 

. . .

"We will do A unless B," means: if not B then A!

Therefore, the right answer is:

$\sim$(RvF) $\to$ (W$\to$O)

## Tautology

Which of the following is a tautology?

1. Q\&$\sim$Q
2. (PvQ) v ($\sim$PvQ)
3. P $\to$ (P\&Q)
4. P $\leftrightarrow$ (Q$\to$P)

## Validity

Is the following argument valid? Explain your answer!

"All brand name products are expensive. Some expensive products are not worth the price. Therefore, some brand name products are not worth the price."

## Bombs and cakes (1)

Suppose you are presented with two boxes, and are told truthfully that each box contains a cake, or a bomb, but not both.

- On box 1 is written: "At least one of the two boxes contains a cake."
- On box 2 is written: "The other box contains a bomb."

Which of the following statements must be *false?*

1. Both sentences on box 1 and box 2 are true.
2. Both sentences on box 1 and box 2 are false.
3. The sentence on box 1 is true and the sentence on box 2 is false.
4. The sentence on box 2 is true and the sentence on box 1 is false.

. . .

Option 2 must be false. (Explanation on next page)

## Bombs and cakes (2)

>- Each box can contain independently contain a cake or a bomb, but not both.
>- This means that it is also possible that both boxes contain cakes, or bombs!
>- Let's look at statement 2: If both sentences are false, then box 2, which says that the other box (box 1) contains a bomb, should be false. Then, box 1 should contain a cake.
>- But box 1 says that at least one box contains a cake. If *this is also to be false,* then box 1 cannot contain a cake.
>- So there is a contradiction. Both sentences cannot be false at the same time.
>- Therefore, option 2 must be false.
>- We leave it as an exercise to the reader to make sense of the other options.

## Equivalence

Which of the following is logically equivalent to P?

1. P\&Q
2. P\&$\sim$P
3. P\&P
4. P$\leftrightarrow$P

. . .

- 1 also depends on Q.
- 2 is a contradiction (always false).
- 3 is the correct answer.
- 4 is a tautology (always true).

## Necessary and sufficient

Which of the following would show that X is *not* a necessary condition for Y?

1. An example that satisfies both X and Y.
2. An example that satisfies X but does not satisfy Y.
3. An example that satisfies Y but does not satisfy X.
4. An example that satisfies neither X nor Y.

. . .

Answer: 3. 

## Necessary and sufficient

If doing well in the job interview is *not* a sufficient condition for being employed, then:

1. Someone not doing well in the job interview still has a chance to be employed.
2. Someone will not be employed unless they do well in the job interview.
3. Someone may not be employed although they did well in the job interview.
4. None of the above.

. . .

Answer: 3.


## Necessary and sufficient

Which is true of the properties of being a triangle and having three sides?

1. Being a triangle is both necessary and sufficient for having three sides.
2. Being a triangle is necessary, but not sufficient for having three sides.
3. Being a triangle is sufficient, but not necessary for having three sides.
4. Being a triangle is neither necessary nor sufficient for having three sides.

## Necessary and sufficient

Answer: Being a triangle is sufficient, but not necessary, for having three sides.

- Every triangle has three sides, so if something is a triangle, we can be sure that it has three sides.
- But not every shape with 3 sides is a triangle. For example, a “doorframe” shape (Greek $\Pi$) has three sides, but is not a triangle.

## Evaluation/model

Which of the following is an evaluation/model on which

(P $\to$ (Q $\to$ R)) $\to$ (P $\to$ (P $\to$ R))

is *false?*

. . . 

>- In order to make this expression false, the first part (before the top-level implication) has to be true, and the second part false.
>- In order to get the second part to become false, P must be true, and P$\to$R must be false. For this, R must be false. So we need P to be true, and R to be false.
>- What about Q? If the first part is true, and P is true, while R is false, then (Q$\to$R) must be true (because P is true, and only if Q$\to$R is also true, the whole first part of the expression will be true). In order for Q$\to$R to be true if R is false, Q must also be false.
>- Therefore, we need P=T, Q=F, R=F.

## Equivalent expressions

![](graphics/13-exam-18.png)\ 

. . . 

>- $\sim$(P$\leftrightarrow$Q) has the truth table F, T, T, F (false when P and Q have the same value, true otherwise).
>- ($\sim$P$\leftrightarrow$Q) has the same truth table, as does (P$\leftrightarrow$$\sim$Q).
>- (Pv$\sim$Q)\&(Qv$\sim$P) will be true when P and Q are both true or both false (because then one of them will be negated and thus one will always be true, making each OR true).
>- (P\&$\sim$Q)v(Q\&$\sim$P) will be false P and Q are the same (because then one of them will be negated and thus one will always be false, making each AND false).
>- Thus, the right answer is “(Pv$\sim$Q)\&(Qv$\sim$P)”. 

## Valid consequence

Which of the following is *not* a valid consequence of $\sim$P?

1. (P$\to$Q)
2. (Q$\to$P)
3. (Q$\to$$\sim$P)
4. (P$\to$$\sim$Q)

. . . 

>- If $\sim$P (P is false), then P$\to$Q must be true.
>- If $\sim$P (P is false), then Q$\to$P could be true if Q is also false, but it does not *need* to be true. Therefore, this is not a valid consequence of $\sim$P (=if the premises are true the conclusion *must* be true!)
>- If $\sim$P (P is false), then Q$\to$$\sim$P must be true.
>- If $\sim$P (P is false), then P$\to$$\sim$Q must be true.

## Tautologies

Which of these statements must be true?

1. P $\leftrightarrow$ P
2. P$\to$(P\&Q)
3. (PvQ)$\to$P
4. P\&$\sim$Q

. . . 

>- P $\leftrightarrow$ P must be true, since P always has the same value as itself.
>- P$\to$(P\&Q) could be false if P=T, Q=F.
>- (PvQ)$\to$P could be false if P=F, Q=T.
>- P\&$\sim$Q could be false if P=F or Q=T.


## Remember:

>- You don’t need to actually do any natural deduction in the exam. But you might find the patterns we discussed useful in order to judge entailment, consistency and other such questions faster than with a truth table.
>- When premises include a contradiction, the conclusion can be *any* proposition and the argument will be valid!
>- Entailment: If the premises are true, the conclusion *must* be true. If the conclusion is a tautology, it is *always* true (this is what tautology means).
>- Therefore, *every statement entails a tautology.*
>- A tautology cannot be inconsistent with any statement (except for a contradiction), since every contingent statement is true sometimes, and the tautology is true always, that is, surely also at the same time as the other statement.


## References

HKU OpenCourseWare on critical thinking, logic, and creativity:

http://philosophy.hku.hk/think/pl

## Any questions?

![](graphics/questions.jpg)\ 

## Thank you for your attention!

Email: <matthias@ln.edu.hk>

If you have questions, please come to my office hours (see course outline).





