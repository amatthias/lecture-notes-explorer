ct01-Introduction.md&0.1&% Critical Thinking
ct01-Introduction.md&0.1&% 1. Introduction
ct01-Introduction.md&0.1&% A. Matthias
ct01-Introduction.md&0.1&
ct01-Introduction.md&1.0&# Introductions
ct01-Introduction.md&1.0&
ct01-Introduction.md&1.1&## Introduction
ct01-Introduction.md&1.1&
ct01-Introduction.md&1.1&My name is: **Andreas Matthias**
ct01-Introduction.md&1.1&
ct01-Introduction.md&1.1&If this is too difficult, you can call me: “Mr. Ma (\cn{馬逸文})” or “Andy”
ct01-Introduction.md&1.1&
ct01-Introduction.md&1.2&## Introduction
ct01-Introduction.md&1.2&
ct01-Introduction.md&1.2&![Last I lived in Goettingen, in Germany](graphics/01-goe1.png)\ 
ct01-Introduction.md&1.2&
ct01-Introduction.md&1.3&## Introduction
ct01-Introduction.md&1.3&
ct01-Introduction.md&1.3&![This is how shopping streets look like there...](graphics/01-goe2.png)\ 
ct01-Introduction.md&1.3&
ct01-Introduction.md&1.4&## Introduction
ct01-Introduction.md&1.4&
ct01-Introduction.md&1.4&![This is the “Girl with geese”, symbol of the city](graphics/01-goe3.jpg)\ 
ct01-Introduction.md&1.4&
ct01-Introduction.md&1.5&## Introduction
ct01-Introduction.md&1.5&
ct01-Introduction.md&1.5&![I actually come from Greece](graphics/01-map1.png)\ 
ct01-Introduction.md&1.5&
ct01-Introduction.md&1.6&## Introduction
ct01-Introduction.md&1.6&
ct01-Introduction.md&1.6&![](graphics/01-greece1.png) \ 
ct01-Introduction.md&1.6&
ct01-Introduction.md&1.7&## Introduction
ct01-Introduction.md&1.7&
ct01-Introduction.md&1.7&![](graphics/01-greece2.png) \ 
ct01-Introduction.md&1.7&
ct01-Introduction.md&1.8&## Introduction
ct01-Introduction.md&1.8&
ct01-Introduction.md&1.8&![Back to work!](graphics/01-greece3.png)\ 
ct01-Introduction.md&1.8&
ct01-Introduction.md&1.9&## Moodle
ct01-Introduction.md&1.9&
ct01-Introduction.md&1.9&In this course, I use Moodle to give you the presentations and
ct01-Introduction.md&1.9&other materials.
ct01-Introduction.md&1.9&
ct01-Introduction.md&1.9&(Demonstration of Moodle)
ct01-Introduction.md&1.9&
ct01-Introduction.md&1.10&## Course outline
ct01-Introduction.md&1.10&
ct01-Introduction.md&1.10&(See outline in Moodle)
ct01-Introduction.md&1.10&
ct01-Introduction.md&1.11&## Old presentations warning
ct01-Introduction.md&1.11&
ct01-Introduction.md&1.11&Please do NOT use old materials!
ct01-Introduction.md&1.11&
ct01-Introduction.md&1.11&This course has changed significantly since last term. Old presentations will NOT be useful.
ct01-Introduction.md&1.11&
ct01-Introduction.md&1.11&Please download the NEW presentations from Moodle each week! Additionally, these presentations are much nicer to print and don't use up so much space on paper!
ct01-Introduction.md&1.11&
ct01-Introduction.md&1.12&## Big List of Bad News
ct01-Introduction.md&1.12&
ct01-Introduction.md&1.12&**1. You need to work**
ct01-Introduction.md&1.12&
ct01-Introduction.md&1.12&Lingnan University Guidelines for Learning:
ct01-Introduction.md&1.12&
ct01-Introduction.md&1.12&“For each hour of class contact, the expectation is that students will undertake 2 additional hours of personal study.”
ct01-Introduction.md&1.12&
ct01-Introduction.md&1.12&We have 3 hours per week. This means that I expect you to revise
ct01-Introduction.md&1.12&the material and do the homework for 6 full hours each week.
ct01-Introduction.md&1.12&
ct01-Introduction.md&1.13&## Big List of Bad News
ct01-Introduction.md&1.13&
ct01-Introduction.md&1.13&**2. You need to speak enough English**
ct01-Introduction.md&1.13&
ct01-Introduction.md&1.13&Lingnan University and I expect you to be able to follow this course in English. You will also have English writing assignments.
ct01-Introduction.md&1.13&
ct01-Introduction.md&1.13&If you cannot handle these, it is your responsibility to do something about it:
ct01-Introduction.md&1.13&
ct01-Introduction.md&1.13&* Watch movies in English.
ct01-Introduction.md&1.13&* Read English books.
ct01-Introduction.md&1.13&* Go to the language centre (CEAL) for help!
ct01-Introduction.md&1.13&
ct01-Introduction.md&1.14&## Big List of Bad News
ct01-Introduction.md&1.14&
ct01-Introduction.md&1.14&**3. You need to be on time**
ct01-Introduction.md&1.14&
ct01-Introduction.md&1.14&Attendance is mandatory. I take the attendance every time you say something in class.
ct01-Introduction.md&1.14&
ct01-Introduction.md&1.14&* If you miss a test without excuse, you get 0 points. There are no make-up opportunities for in-class tests.
ct01-Introduction.md&1.14&* You need to arrive here on time (max. 5 minutes late)
ct01-Introduction.md&1.14&* If you repeatedly come later than that, your grade will go down.
ct01-Introduction.md&1.14&* If you come more than 15 minutes late, you will be considered absent for this session.
ct01-Introduction.md&1.14&
ct01-Introduction.md&1.15&## Big List of Bad News
ct01-Introduction.md&1.15&
ct01-Introduction.md&1.15&**4. You need to participate in class**
ct01-Introduction.md&1.15&
ct01-Introduction.md&1.15&* A significant percentage of your grade is participation in class!
ct01-Introduction.md&1.15&* This means that you need to actively *say* something, not just sit there.
ct01-Introduction.md&1.15&* I record your performance every time you say something. If you don't participate, your participation is a "C".
ct01-Introduction.md&1.15&* I also record it in my list every time I see you play with your phone or work on materials for other classes. If you do these things, you get a participation grade “F” for this session. This will affect your final grade.
ct01-Introduction.md&1.15&
ct01-Introduction.md&1.15&*So be careful what you do in class. Participate. Switch off your phone. Don’t read materials other than those for this class.*
ct01-Introduction.md&1.15&
ct01-Introduction.md&1.16&## Big List of Bad News
ct01-Introduction.md&1.16&
ct01-Introduction.md&1.16&**5. Final grade distribution (approx.):**
ct01-Introduction.md&1.16&
ct01-Introduction.md&1.16&* \~20% A
ct01-Introduction.md&1.16&* \~45% B
ct01-Introduction.md&1.16&* \~40% C
ct01-Introduction.md&1.16&* D and F as needed. Normally a little less than 5% D’s and one or two F per term (through absence or cheating).
ct01-Introduction.md&1.16&
ct01-Introduction.md&1.16&This distribution is required by the University and will always be the same, regardless of your performance.
ct01-Introduction.md&1.16&
ct01-Introduction.md&1.16&So make sure you are in the best 20% of the class if you want an A.
ct01-Introduction.md&1.16&
ct01-Introduction.md&1.17&## Questions?
ct01-Introduction.md&1.17&
ct01-Introduction.md&1.17&Any questions about the requirements and the grading?
ct01-Introduction.md&1.17&
ct01-Introduction.md&2.0&# Groupwork
ct01-Introduction.md&2.0&
ct01-Introduction.md&2.1&## We'll work in groups (1)
ct01-Introduction.md&2.1&
ct01-Introduction.md&2.1&Groups stay together until the end of term.
ct01-Introduction.md&2.1&
ct01-Introduction.md&2.1&If you help each other, you'll have better grades.
ct01-Introduction.md&2.1&
ct01-Introduction.md&2.2&## We'll work in groups (2)
ct01-Introduction.md&2.2&
ct01-Introduction.md&2.2&Let's organize ourselves into groups of 2 or 3.
ct01-Introduction.md&2.2&
ct01-Introduction.md&2.2&Please not more than 3 participants in a group!
ct01-Introduction.md&2.2&
ct01-Introduction.md&2.3&## Get to know your group
ct01-Introduction.md&2.3&
ct01-Introduction.md&2.3&Take five minutes.
ct01-Introduction.md&2.3&
ct01-Introduction.md&2.3&Sit together, and find out about each other's:
ct01-Introduction.md&2.3&
ct01-Introduction.md&2.3&* Name, email address, major subjects
ct01-Introduction.md&2.3&* Favorite way to spend a free afternoon
ct01-Introduction.md&2.3&* Favorite place (in HK or elsewhere)
ct01-Introduction.md&2.3&* Favorite music
ct01-Introduction.md&2.3&
ct01-Introduction.md&3.0&# What is critical thinking?
ct01-Introduction.md&3.0&
ct01-Introduction.md&3.1&## "Thinking" exercise (3 minutes)
ct01-Introduction.md&3.1&
ct01-Introduction.md&3.1&Name situations and actions that have to do with "thinking."
ct01-Introduction.md&3.1&
ct01-Introduction.md&3.2&## "Thinking" exercise
ct01-Introduction.md&3.2&
ct01-Introduction.md&3.2&Name situations and actions that have to do with "thinking."
ct01-Introduction.md&3.2&
ct01-Introduction.md&3.2&> - Playing chess
ct01-Introduction.md&3.2&> - Doing mathematics
ct01-Introduction.md&3.2&> - Reading a book
ct01-Introduction.md&3.2&> - Writing a song
ct01-Introduction.md&3.2&> - Thinking of my girlfriend
ct01-Introduction.md&3.2&> - Shopping, going to the restaurant …
ct01-Introduction.md&3.2&
ct01-Introduction.md&3.3&## "Thinking" exercise
ct01-Introduction.md&3.3&
ct01-Introduction.md&3.3&- Playing chess
ct01-Introduction.md&3.3&- Doing mathematics
ct01-Introduction.md&3.3&- Reading a book
ct01-Introduction.md&3.3&- Writing a song
ct01-Introduction.md&3.3&- Thinking of my girlfriend
ct01-Introduction.md&3.3&- Shopping, going to the restaurant …
ct01-Introduction.md&3.3&
ct01-Introduction.md&3.3&*What is common in all this?*
ct01-Introduction.md&3.3&
ct01-Introduction.md&3.4&## "Thinking"
ct01-Introduction.md&3.4&
ct01-Introduction.md&3.4&Thinking ...
ct01-Introduction.md&3.4&
ct01-Introduction.md&3.4&> - Is a function of the brain
ct01-Introduction.md&3.4&> - Is something we do on purpose
ct01-Introduction.md&3.4&> - Usually has an aim, a target
ct01-Introduction.md&3.4&
ct01-Introduction.md&3.5&## "Critical" thinking
ct01-Introduction.md&3.5&
ct01-Introduction.md&3.5&All this is thinking, somehow.
ct01-Introduction.md&3.5&
ct01-Introduction.md&3.5&... but what means *critical* thinking?
ct01-Introduction.md&3.5&
ct01-Introduction.md&3.6&## Exercise: The word "critical" (3 minutes)
ct01-Introduction.md&3.6&
ct01-Introduction.md&3.6&What other uses of the word "critic(al)" do you know?
ct01-Introduction.md&3.6&
ct01-Introduction.md&3.7&## “Critical”
ct01-Introduction.md&3.7&
ct01-Introduction.md&3.7&Uses of “critic/al:”
ct01-Introduction.md&3.7&
ct01-Introduction.md&3.7&> - To criticize someone
ct01-Introduction.md&3.7&> - To be critical
ct01-Introduction.md&3.7&> - To be a (cinema) critic
ct01-Introduction.md&3.7&> - A critical condition of a hospital patient
ct01-Introduction.md&3.7&> - A political crisis
ct01-Introduction.md&3.7&
ct01-Introduction.md&3.8&## “Critical”: two meanings
ct01-Introduction.md&3.8&
ct01-Introduction.md&3.9&### One:
ct01-Introduction.md&3.9&- To criticize someone
ct01-Introduction.md&3.9&- To be critical
ct01-Introduction.md&3.9&- To be a (cinema) critic
ct01-Introduction.md&3.9&
ct01-Introduction.md&3.10&### Two:
ct01-Introduction.md&3.10&- A critical condition of a hospital patient
ct01-Introduction.md&3.10&- A political crisis
ct01-Introduction.md&3.10&
ct01-Introduction.md&3.11&## Ancient Greek: “κρίσις”
ct01-Introduction.md&3.11&
ct01-Introduction.md&3.11&Ancient Greek: “κρίσις”
ct01-Introduction.md&3.11&
ct01-Introduction.md&3.11&* means the ability to judge things (to be critical, to be a critic, to criticize)
ct01-Introduction.md&3.11&* or: it can mean a point of decision (a critical stage, a crisis)
ct01-Introduction.md&3.11&
ct01-Introduction.md&3.12&## To criticize = to judge
ct01-Introduction.md&3.12&
ct01-Introduction.md&3.12&“Critical thinking” = “Judging” thinking
ct01-Introduction.md&3.12&
ct01-Introduction.md&3.13&## Exercise (3 minutes)
ct01-Introduction.md&3.13&
ct01-Introduction.md&3.13&What does it mean “to judge”?
ct01-Introduction.md&3.13&
ct01-Introduction.md&3.13&Write down 5 elements of “judging” or five cases in which
ct01-Introduction.md&3.13&someone judges something!
ct01-Introduction.md&3.13&
ct01-Introduction.md&3.14&## Is this critical “judgment”?
ct01-Introduction.md&3.14&“Somehow I like the right picture better than the left.”
ct01-Introduction.md&3.14&
ct01-Introduction.md&3.14&![](graphics/01-pic12.png) \ 
ct01-Introduction.md&3.14&
ct01-Introduction.md&3.15&## “Opinion” vs. “judgement”
ct01-Introduction.md&3.15&
ct01-Introduction.md&3.16&### Opinion:
ct01-Introduction.md&3.16&> - An *opinion* is personal, subjective.
ct01-Introduction.md&3.16&> - Another person can agree or disagree equally well (“But I like the left picture better!!!”)
ct01-Introduction.md&3.16&
ct01-Introduction.md&3.17&### Judgement:
ct01-Introduction.md&3.17&> - A *judgement* is supported by reasons.
ct01-Introduction.md&3.17&> - Another person cannot disagree without better reasons.
ct01-Introduction.md&3.17&
ct01-Introduction.md&3.18&## An opinion:
ct01-Introduction.md&3.18&
ct01-Introduction.md&3.18&> - “Orange juice is much better than coke!”
ct01-Introduction.md&3.18&> - “I disagree! Coke is better!”
ct01-Introduction.md&3.18&
ct01-Introduction.md&3.19&## A judgement:
ct01-Introduction.md&3.19&
ct01-Introduction.md&3.19&> - “Orange juice is much better than coke, because it has vitamins, it is healthy, it doesn't have added sugar and preservatives and it has less calories.”
ct01-Introduction.md&3.19&> - “ . . . ”
ct01-Introduction.md&3.19&
ct01-Introduction.md&3.20&## Critical judgment
ct01-Introduction.md&3.20&
ct01-Introduction.md&3.20&To *judge critically* means:
ct01-Introduction.md&3.20&
ct01-Introduction.md&3.20&> - To decide between two or more alternatives based on reasons and logical thinking.
ct01-Introduction.md&3.20&> - Such a judgment is not just a personal opinion, but supported by reasons.
ct01-Introduction.md&3.20&> - It can only be *refuted* (rejected, shown to be false) by better reasons.
ct01-Introduction.md&3.20&
ct01-Introduction.md&3.21&## Exercise (5 minutes)
ct01-Introduction.md&3.21&
ct01-Introduction.md&3.21&Is it better to live in Central or in the New Territories?
ct01-Introduction.md&3.21&
ct01-Introduction.md&3.21&Decide on one answer in your group, then give a list of reasons for your choice.
ct01-Introduction.md&3.21&
ct01-Introduction.md&4.0&# A little history
ct01-Introduction.md&4.0&
ct01-Introduction.md&4.1&## Ancient Greece
ct01-Introduction.md&4.1&
ct01-Introduction.md&4.1&![Critical thinking, in its modern form, begins in ancient Greece (\cn{希臘})](graphics/01-map1.png)
ct01-Introduction.md&4.1&
ct01-Introduction.md&4.2&## Athens (\cn{雅典})
ct01-Introduction.md&4.2&
ct01-Introduction.md&4.2&![Athens is here.](graphics/01-map3b.png)
ct01-Introduction.md&4.2&
ct01-Introduction.md&4.3&## Aristotle (\cn{亞里士多德})
ct01-Introduction.md&4.3&
ct01-Introduction.md&4.3&![Aristotle lived in Athens in 384-322 BC (one century later than Confucius \cn{孔子}).](graphics/01-aristotle.png)
ct01-Introduction.md&4.3&
ct01-Introduction.md&4.4&## Aristotle (\cn{亞里士多德})
ct01-Introduction.md&4.4&
ct01-Introduction.md&4.4&![He laid the foundations of modern logic.](graphics/01-aristotle.png)
ct01-Introduction.md&4.4&
ct01-Introduction.md&4.5&## Exercise: Why the Greeks?
ct01-Introduction.md&4.5&
ct01-Introduction.md&4.5&Can you imagine why particularly the ancient Greeks should have advanced logic?
ct01-Introduction.md&4.5&
ct01-Introduction.md&4.5&Think about it for a moment!
ct01-Introduction.md&4.5&
ct01-Introduction.md&4.5&(What was the particular thing about ancient Athens other cultures of that time did not have?)
ct01-Introduction.md&4.5&
ct01-Introduction.md&4.6&## The Athenians
ct01-Introduction.md&4.6&
ct01-Introduction.md&4.6&The people of Athens...
ct01-Introduction.md&4.6&
ct01-Introduction.md&4.6&> - ... had a democracy.
ct01-Introduction.md&4.6&> - In a democracy all citizens decide together.
ct01-Introduction.md&4.6&> - In order to influence the decision, one has to convince the others.
ct01-Introduction.md&4.6&> - This led to the advancement of *rhetoric* (the art of speaking in public) and logic.
ct01-Introduction.md&4.6&
ct01-Introduction.md&4.7&## Critical Thinking today
ct01-Introduction.md&4.7&
ct01-Introduction.md&4.7&So why is Critical Thinking important for you?
ct01-Introduction.md&4.7&
ct01-Introduction.md&4.7&> - Today you live (or will live) in democratic societies.
ct01-Introduction.md&4.7&> - As citizens you decide about the future together.
ct01-Introduction.md&4.7&> - In order to participate in decisions, you have to understand and use arguments and reasons.
ct01-Introduction.md&4.7&> - You have to know how to convince others.
ct01-Introduction.md&4.7&> - And you have to be able to spot mistakes in others' thoughts.
ct01-Introduction.md&4.7&
ct01-Introduction.md&4.8&## Any questions?
ct01-Introduction.md&4.8&
ct01-Introduction.md&4.8&![](graphics/questions.jpg)\ 
ct01-Introduction.md&4.8&
ct01-Introduction.md&4.9&## Thank you for your attention!
ct01-Introduction.md&4.9&
ct01-Introduction.md&4.9&Email: <matthias@ln.edu.hk>
ct01-Introduction.md&4.9&
ct01-Introduction.md&4.9&If you have questions, please come to my office hours (see course outline).
ct02-Definitions.md&0.1&% Critical Thinking
ct02-Definitions.md&0.1&% 2. Definitions
ct02-Definitions.md&0.1&% A. Matthias
ct02-Definitions.md&0.1&
ct02-Definitions.md&1.0&# Where we are
ct02-Definitions.md&1.0&
ct02-Definitions.md&1.1&## What did we learn last time?
ct02-Definitions.md&1.1&
ct02-Definitions.md&1.1&- Course outline and formalities
ct02-Definitions.md&1.1&
ct02-Definitions.md&1.2&## What are we going to learn today?
ct02-Definitions.md&1.2&
ct02-Definitions.md&1.2&- What is a definition?
ct02-Definitions.md&1.2&- What is a correct definition?
ct02-Definitions.md&1.2&- Descriptive and stipulative definitions
ct02-Definitions.md&1.2&- Intensional and extensional definitions
ct02-Definitions.md&1.2&- Descriptive (reportive), stipulative and precising definitions
ct02-Definitions.md&1.2&- Intensional and extensional definitions
ct02-Definitions.md&1.2&- Genus and difference
ct02-Definitions.md&1.2&- Circular definitions
ct02-Definitions.md&1.2&- Common definition errors
ct02-Definitions.md&1.2&
ct02-Definitions.md&2.0&# Definitions
ct02-Definitions.md&2.0&
ct02-Definitions.md&2.1&## Definitions (\cn{定義})
ct02-Definitions.md&2.1&
ct02-Definitions.md&2.1&1. What does “definition” mean?
ct02-Definitions.md&2.1&2. Define the following: knife, elephant, kitchen, table, air.
ct02-Definitions.md&2.1&
ct02-Definitions.md&2.2&## Definitions must be precise
ct02-Definitions.md&2.2&
ct02-Definitions.md&2.2&- You should try to be as precise as possible.
ct02-Definitions.md&2.2&- A definition should ideally include only one object: that, which you wish to define.
ct02-Definitions.md&2.2&- It should exclude everything else
ct02-Definitions.md&2.2&
ct02-Definitions.md&2.3&## Is this a good definition?
ct02-Definitions.md&2.3&
ct02-Definitions.md&2.3&“A pen is something which writes.”
ct02-Definitions.md&2.3&
ct02-Definitions.md&2.4&## Is this a good definition?
ct02-Definitions.md&2.4&
ct02-Definitions.md&2.4&“A pen is something which writes.”
ct02-Definitions.md&2.4&
ct02-Definitions.md&2.4&No: it is too general (also applies to pencils and brushes, and to my little brother who is writing a letter to his friend.)
ct02-Definitions.md&2.4&
ct02-Definitions.md&2.5&## Is this a good definition?
ct02-Definitions.md&2.5&
ct02-Definitions.md&2.5&“A pen is an instrument for writing, which has a hard tip through which ink is applied onto the paper.”
ct02-Definitions.md&2.5&
ct02-Definitions.md&2.6&## Is this a good definition?
ct02-Definitions.md&2.6&
ct02-Definitions.md&2.6&“A pen is an instrument for writing, which has a hard tip through which ink is applied onto the paper.”
ct02-Definitions.md&2.6&
ct02-Definitions.md&2.6&>- Good: “instrument for writing” excludes my little brother.
ct02-Definitions.md&2.6&>- Good: “hard tip” excludes brushes.
ct02-Definitions.md&2.6&>- Good: “ink” excludes pencils.
ct02-Definitions.md&2.6&>- Good: “ink is applied onto the paper” includes both ball pens and fountain pens.
ct02-Definitions.md&2.6&
ct02-Definitions.md&2.7&## No definition is perfect
ct02-Definitions.md&2.7&
ct02-Definitions.md&2.7&>- You can almost always find flaws in definitions.
ct02-Definitions.md&2.7&>- This is okay, as long as the definition fulfills its purpose.
ct02-Definitions.md&2.7&>- *What is the purpose of a definition?*
ct02-Definitions.md&2.7&
ct02-Definitions.md&2.7&. . .
ct02-Definitions.md&2.7&
ct02-Definitions.md&2.7&The most common purpose of a definition is to make sure that two parties agree on the meanings of the words they use.
ct02-Definitions.md&2.7&
ct02-Definitions.md&2.7&If the definition can achieve that, it is good enough.
ct02-Definitions.md&2.7&
ct02-Definitions.md&3.0&# Kinds of definitions
ct02-Definitions.md&3.0&
ct02-Definitions.md&3.1&## Descriptive definitions
ct02-Definitions.md&3.1&
ct02-Definitions.md&3.1&>- *Descriptive definitions* (\cn{描述性定義}) describe how a word is used.
ct02-Definitions.md&3.1&>- They can be right or wrong.
ct02-Definitions.md&3.1&
ct02-Definitions.md&3.1&. . .
ct02-Definitions.md&3.1&
ct02-Definitions.md&3.1&Right descriptive definition: “A pen is an instrument for writing, which has a hard tip through which ink is applied onto the paper.”
ct02-Definitions.md&3.1&
ct02-Definitions.md&3.1&Wrong descriptive definition: “A pen is a bird with red feathers.”
ct02-Definitions.md&3.1&
ct02-Definitions.md&3.2&## Descriptive and stipulative definitions
ct02-Definitions.md&3.2&
ct02-Definitions.md&3.2&>- Descriptive definitions (\cn{描述性定義}) describe how a word is used. *They can be right or wrong.*
ct02-Definitions.md&3.2&>- Stipulative definitions (\cn{規約性定義}) introduce a new word to the language, or a new meaning for a known word. *Therefore, they cannot be wrong.*
ct02-Definitions.md&3.2&
ct02-Definitions.md&3.3&## Stipulative definition (1)
ct02-Definitions.md&3.3&
ct02-Definitions.md&3.3&“When I say ‘trair’ I want the word to mean ‘three-legged chair’.”
ct02-Definitions.md&3.3&
ct02-Definitions.md&3.3&(This can never be wrong)
ct02-Definitions.md&3.3&
ct02-Definitions.md&3.4&## Stipulative definition (2)
ct02-Definitions.md&3.4&
ct02-Definitions.md&3.4&“I just invented a new game. I will call it Greek Chess.”
ct02-Definitions.md&3.4&
ct02-Definitions.md&3.4&(This can never be wrong)
ct02-Definitions.md&3.4&
ct02-Definitions.md&3.5&## Stipulative definitions are common
ct02-Definitions.md&3.5&
ct02-Definitions.md&3.5&For example when culture or technology advances: “astronaut”, “microwave”, “PDA”, “mobile phone”, “bungee-jumping”, “blu-ray disc” have all been relatively recently defined.
ct02-Definitions.md&3.5&
ct02-Definitions.md&3.6&## Which definitions are descriptive, which stipulative?
ct02-Definitions.md&3.6&
ct02-Definitions.md&3.6&- A chair is a piece of furniture with a straight back, for sitting upon.
ct02-Definitions.md&3.6&- Let's call the new star, that was discovered yesterday, “Ferry-Star.”
ct02-Definitions.md&3.6&- “James” is the name that I will give to my first son.
ct02-Definitions.md&3.6&- “Water” we call the substance which in Chemistry is known as H$_2$O.
ct02-Definitions.md&3.6&
ct02-Definitions.md&3.7&## Which definitions are descriptive, which stipulative?
ct02-Definitions.md&3.7&
ct02-Definitions.md&3.7&- A chair is a piece of furniture with a straight back, for sitting upon. (descriptive)
ct02-Definitions.md&3.7&- Let's call the new star, that was discovered yesterday, “Ferry-Star.” (stipulative)
ct02-Definitions.md&3.7&- “James” is the name that I will give to my first son. (stipulative)
ct02-Definitions.md&3.7&- “Water” we call the substance which in Chemistry is known as H$_2$O (descriptive)
ct02-Definitions.md&3.7&
ct02-Definitions.md&3.8&## Intensional and extensional definitions
ct02-Definitions.md&3.8&
ct02-Definitions.md&3.8&What is the difference between the following two?
ct02-Definitions.md&3.8&
ct02-Definitions.md&3.8&- “An astronaut is a person trained to fly to space”
ct02-Definitions.md&3.8&- “Astronauts are people like Neil Armstrong, Edwin Aldrin and Yang Li Wei.”
ct02-Definitions.md&3.8&
ct02-Definitions.md&3.9&## Intensional definitions (\cn{內涵定義})
ct02-Definitions.md&3.9&
ct02-Definitions.md&3.9&An *intensional definition* specifies the *conditions for something to be a member of a specific set.*
ct02-Definitions.md&3.9&
ct02-Definitions.md&3.9&>- What does this mean? What are the conditions for something to be a member of the set “astronaut?”
ct02-Definitions.md&3.9&>- It must be a human
ct02-Definitions.md&3.9&>- It must be trained to fly to space
ct02-Definitions.md&3.9&>- Thus: “Astronauts” are people who are trained to fly to space.
ct02-Definitions.md&3.9&
ct02-Definitions.md&3.10&## Extensional definitions (\cn{外延定義})
ct02-Definitions.md&3.10&
ct02-Definitions.md&3.10&An *extensional definition* just lists all or some members of the set to be defined.
ct02-Definitions.md&3.10&
ct02-Definitions.md&3.10&>- “Vehicles are cars, trucks, and buses.”
ct02-Definitions.md&3.10&>- “Fruit are apples, bananas and mangoes.”
ct02-Definitions.md&3.10&
ct02-Definitions.md&3.11&## Now do it yourself
ct02-Definitions.md&3.11&
ct02-Definitions.md&3.11&Define “vehicle,” “Chinese cooking” and “fruit” intensionally!
ct02-Definitions.md&3.11&
ct02-Definitions.md&3.12&## Intensional definitions
ct02-Definitions.md&3.12&
ct02-Definitions.md&3.12&>- “A vehicle is a self-moving container used for transporting people or goods, esp. on land.”
ct02-Definitions.md&3.12&>- “Chinese cooking is a method of preparation of food which has originated and is typically used in the PRC, Taiwan, Hong Kong and Singapore.” (details depend on whether you want to define “Chinese” or “cooking”!)
ct02-Definitions.md&3.12&>- “Fruit are usually sweet and fleshy parts of plants that contain seeds and are edible by humans, usually raw.”
ct02-Definitions.md&3.12&
ct02-Definitions.md&3.12&
ct02-Definitions.md&3.13&## Reportive definitions
ct02-Definitions.md&3.13&
ct02-Definitions.md&3.13&*Reportive definition* is another word for what we called *descriptive* definition above. It reports (or describes) how a word is used.
ct02-Definitions.md&3.13&
ct02-Definitions.md&3.13&
ct02-Definitions.md&3.14&## Precising definitions
ct02-Definitions.md&3.14&
ct02-Definitions.md&3.14&A *precising definition* might be regarded as a combination of reportive and stipulative definition.
ct02-Definitions.md&3.14&
ct02-Definitions.md&3.14&The aim of a precising definition is to make the meaning of a term more precise for some purpose (although we know what the term generally means).
ct02-Definitions.md&3.14&
ct02-Definitions.md&3.14&. . . 
ct02-Definitions.md&3.14&
ct02-Definitions.md&3.14&For example, a bus company might want to give discounts to old people. But simply declaring that old people can get discounts will lead to many disputes since it is not clear how old should one be in order to be an old person. So one might define “old person” to mean any person of age 65 or above. This is of course one among many possible definitions of “old.” (From: HKU Critical Thinking Web)
ct02-Definitions.md&3.14&
ct02-Definitions.md&3.14&. . . 
ct02-Definitions.md&3.14&
ct02-Definitions.md&3.14&Another example: Lingnan university administration: “A full-time student is a student who takes at least three courses each semester.”
ct02-Definitions.md&3.14&
ct02-Definitions.md&3.15&## Different types of definitions
ct02-Definitions.md&3.15&
ct02-Definitions.md&3.15&What is correct as one type of definition, need not be correct as another. Example:
ct02-Definitions.md&3.15&
ct02-Definitions.md&3.15&“A full-time student is a student who takes at least 2 courses each term.”
ct02-Definitions.md&3.15&
ct02-Definitions.md&3.15&>- This is wrong (presently) as a reportive definition (because full-time students need to take 3 courses).
ct02-Definitions.md&3.15&>- It would be correct as a precising definition issued by the university administration, though.
ct02-Definitions.md&3.15&
ct02-Definitions.md&3.16&## Which definitions are descriptive, which stipulative, which precising?
ct02-Definitions.md&3.16&
ct02-Definitions.md&3.16&1. A telephone is an electronic device used to transmit voice over a cable.
ct02-Definitions.md&3.16&2. “Jane” is a woman who is my wife.
ct02-Definitions.md&3.16&3. “Andy tea” is a drink I made up, that is composed of tea, lemonade, and fish sauce.
ct02-Definitions.md&3.16&4. Under the terms of this regulation, a "Chinese" person is a person who holds a Chinese passport.
ct02-Definitions.md&3.16&5. “Fido” is the name that I will give to my new dog.
ct02-Definitions.md&3.16&
ct02-Definitions.md&3.17&## Descriptive (D), stipulative (S), precising (P):
ct02-Definitions.md&3.17&
ct02-Definitions.md&3.17&1. D: A telephone is an electronic device used to transmit voice over a cable.
ct02-Definitions.md&3.17&2. D: “Jane” is a woman who is my wife (I cannot rename her!)
ct02-Definitions.md&3.17&3. S: “Andy tea” is a drink I made up, that is composed of tea, lemonade, and fish sauce.
ct02-Definitions.md&3.17&4. P: Under the terms of this regulation, a "Chinese" person is a person who holds a Chinese passport.
ct02-Definitions.md&3.17&5. S: “Fido” is the name that I will give to my new dog.
ct02-Definitions.md&3.17&
ct02-Definitions.md&3.18&## Intensional or extensional?
ct02-Definitions.md&3.18&
ct02-Definitions.md&3.18&1. “Drinks are tea, coffee and lemonade”
ct02-Definitions.md&3.18&2. “A bicycle is a vehicle with two wheels that is powered by its driver”
ct02-Definitions.md&3.18&3. “Educational institutions are primary schools, secondary schools and universities”
ct02-Definitions.md&3.18&4. “A truck is a big car that is used for transporting goods”
ct02-Definitions.md&3.18&
ct02-Definitions.md&3.19&## Intensional (I) or extensional (E)?
ct02-Definitions.md&3.19&
ct02-Definitions.md&3.19&1. E: “Drinks are tea, coffee and lemonade”
ct02-Definitions.md&3.19&2. I: “A bicycle is a vehicle with two wheels that is powered by its driver”
ct02-Definitions.md&3.19&3. E: “Educational institutions are primary schools, secondary schools and universities”
ct02-Definitions.md&3.19&4. I: “A truck is a big car that is used for transporting goods”
ct02-Definitions.md&3.19&
ct02-Definitions.md&4.0&# Comparing definitions
ct02-Definitions.md&4.0&
ct02-Definitions.md&4.1&## Compare definitions
ct02-Definitions.md&4.1&
ct02-Definitions.md&4.1&Define first extensionally and then intensionally:
ct02-Definitions.md&4.1&
ct02-Definitions.md&4.1&- Major subject
ct02-Definitions.md&4.1&- Science
ct02-Definitions.md&4.1&- Plant
ct02-Definitions.md&4.1&- Vegetable
ct02-Definitions.md&4.1&
ct02-Definitions.md&4.2&## Intensional vs. extensional
ct02-Definitions.md&4.2&
ct02-Definitions.md&4.2&>- Extensional definitions are easier to give.
ct02-Definitions.md&4.2&>- You just need some examples.
ct02-Definitions.md&4.2&>- But they are less precise.
ct02-Definitions.md&4.2&>- Sometimes they can be very confusing.
ct02-Definitions.md&4.2&
ct02-Definitions.md&4.3&## Confusing extensional definitions
ct02-Definitions.md&4.3&
ct02-Definitions.md&4.3&*In Greek, “διάσημος” means people like, for example, Donald Trump, Boris Johnson and Vladimir Putin.*
ct02-Definitions.md&4.3&
ct02-Definitions.md&4.3&- What did I define here?
ct02-Definitions.md&4.3&
ct02-Definitions.md&4.3&. . . 
ct02-Definitions.md&4.3&
ct02-Definitions.md&4.3&>- Politician?
ct02-Definitions.md&4.3&>- Bad politician who doesn’t care about democracy?
ct02-Definitions.md&4.3&>- Man?
ct02-Definitions.md&4.3&>- Person who can speak English?
ct02-Definitions.md&4.3&> Actually, the word means “famous”.
ct02-Definitions.md&4.3&
ct02-Definitions.md&4.4&## Intensional vs. extensional
ct02-Definitions.md&4.4&
ct02-Definitions.md&4.4&>- Intensional definitions are harder to give.
ct02-Definitions.md&4.4&>- You need to think about the properties of things and how different sets of properties relate to each other.
ct02-Definitions.md&4.4&>- But they are more precise.
ct02-Definitions.md&4.4&>- Usually they are very clear.
ct02-Definitions.md&4.4&>- This is why they are primarily used in science.
ct02-Definitions.md&4.4&
ct02-Definitions.md&4.5&## Kinds of definitions
ct02-Definitions.md&4.5&
ct02-Definitions.md&4.5&There is no connection between the two pairs of concepts!
ct02-Definitions.md&4.5&
ct02-Definitions.md&4.5&- *Intensional and descriptive:* A chair is a piece of furniture, which is used for sitting upon and has ...
ct02-Definitions.md&4.5&- *Intensional and stipulative:* I call “Greek Chess” a game which has the following rules ...
ct02-Definitions.md&4.5&- *Extensional and descriptive:* A fruit is something like a banana, an apple or an orange.
ct02-Definitions.md&4.5&- *Extensional and stipulative:* Greek Chess is something like Chess, Chinese Chess, or Checkers.
ct02-Definitions.md&4.5&
ct02-Definitions.md&4.6&## Some more examples
ct02-Definitions.md&4.6&
ct02-Definitions.md&4.6&- “A table is a piece of furniture which is used for placing things upon”
ct02-Definitions.md&4.6&- “Air is the gas which surrounds the planet Earth”
ct02-Definitions.md&4.6&- “A mobile phone is a telephone which can be carried around and used on the way”
ct02-Definitions.md&4.6&
ct02-Definitions.md&5.0&# Genus and difference
ct02-Definitions.md&5.0&
ct02-Definitions.md&5.1&## Genus (\cn{類}) and difference (\cn{種差})
ct02-Definitions.md&5.1&
ct02-Definitions.md&5.1&These definitions are called definitions by “genus” (family) and difference.
ct02-Definitions.md&5.1&
ct02-Definitions.md&5.2&## Genus
ct02-Definitions.md&5.2&
ct02-Definitions.md&5.2&The *genus* is the family of things something belongs to.
ct02-Definitions.md&5.2&
ct02-Definitions.md&5.2&Examples of genus:
ct02-Definitions.md&5.2&
ct02-Definitions.md&5.2&>- “Table” is of the genus “piece of furniture”
ct02-Definitions.md&5.2&>- “Air” is of the genus “gas”
ct02-Definitions.md&5.2&>- “Mobile phone” is of the genus “phone”
ct02-Definitions.md&5.2&
ct02-Definitions.md&5.3&## Genus
ct02-Definitions.md&5.3&
ct02-Definitions.md&5.3&Name the genus of:
ct02-Definitions.md&5.3&
ct02-Definitions.md&5.3&- Elephant
ct02-Definitions.md&5.3&- Computer
ct02-Definitions.md&5.3&- University
ct02-Definitions.md&5.3&- Student
ct02-Definitions.md&5.3&- Rice
ct02-Definitions.md&5.3&
ct02-Definitions.md&5.4&## Genus
ct02-Definitions.md&5.4&
ct02-Definitions.md&5.4&- Elephant, gen.: mammal (or animal)
ct02-Definitions.md&5.4&- Computer, gen: electronic device
ct02-Definitions.md&5.4&- University, gen: educational institution
ct02-Definitions.md&5.4&- Student, gen: human or occupation
ct02-Definitions.md&5.4&- Rice, gen: plant or dish
ct02-Definitions.md&5.4&
ct02-Definitions.md&5.5&## One thing can belong to multiple genera
ct02-Definitions.md&5.5&
ct02-Definitions.md&5.5&- E.g. “rice” can be a dish or a plant.
ct02-Definitions.md&5.5&- A “student” can be a human or an occupation.
ct02-Definitions.md&5.5&
ct02-Definitions.md&5.5&As what we see it, depends on what properties we want to emphasise.
ct02-Definitions.md&5.5&
ct02-Definitions.md&5.5&*There is no “right” genus in these cases.*
ct02-Definitions.md&5.5&
ct02-Definitions.md&5.6&## Difference
ct02-Definitions.md&5.6&
ct02-Definitions.md&5.6&The difference is *what makes this particular thing different from the other members of its genus.*
ct02-Definitions.md&5.6&
ct02-Definitions.md&5.6&In a definition, it often appears after the word “which”: “a spaceship is a vehicle, which flies into space.”
ct02-Definitions.md&5.6&
ct02-Definitions.md&5.7&## Genus and difference
ct02-Definitions.md&5.7&
ct02-Definitions.md&5.7&So the idea is that we say to which group (genus) something belongs to, and then what makes it different from all the other members of that group.
ct02-Definitions.md&5.7&
ct02-Definitions.md&5.7&In this way we can define one single thing.
ct02-Definitions.md&5.7&
ct02-Definitions.md&5.7&The narrower the genus, the easier it is to specify a good difference. Therefore, the genus in a definition should be as narrow as possible!
ct02-Definitions.md&5.7&
ct02-Definitions.md&5.7&So, "mammal" is a better genus than "animal." "Electronic device" is better than "machine." Any concrete class of things is better than just "thing."
ct02-Definitions.md&5.7&
ct02-Definitions.md&5.8&## Difference
ct02-Definitions.md&5.8&
ct02-Definitions.md&5.8&Now find the difference in order to complete the following definitions:
ct02-Definitions.md&5.8&
ct02-Definitions.md&5.8&- Elephant, gen.: mammal
ct02-Definitions.md&5.8&- Computer, gen: electronic device
ct02-Definitions.md&5.8&- University, gen: educational institution
ct02-Definitions.md&5.8&- Student, gen: human
ct02-Definitions.md&5.8&- Rice, gen: plant or dish
ct02-Definitions.md&5.8&
ct02-Definitions.md&5.9&## Examples of difference
ct02-Definitions.md&5.9&
ct02-Definitions.md&5.9&- Elephant, gen.: mammal. Diff: big, has long nose.
ct02-Definitions.md&5.9&- Computer, gen: electronic device. Diff: is programmable.
ct02-Definitions.md&5.9&- University, gen: educational institution. Diff: the highest.
ct02-Definitions.md&5.9&- Student, gen: human. Diff: being educated at an educational institution.
ct02-Definitions.md&5.9&- Rice, gen: plant or dish.
ct02-Definitions.md&5.9&    - Diff (plant): monocarpic annual, growing in such-and such conditions ...
ct02-Definitions.md&5.9&    - Diff (dish): is a staple food in south-east Asia.
ct02-Definitions.md&5.9&
ct02-Definitions.md&5.10&## Now try it yourself!
ct02-Definitions.md&5.10&
ct02-Definitions.md&5.10&Give genus-difference definitions for the following:
ct02-Definitions.md&5.10&
ct02-Definitions.md&5.10&- Cinema
ct02-Definitions.md&5.10&- Ship
ct02-Definitions.md&5.10&- Powerpoint
ct02-Definitions.md&5.10&- Apple
ct02-Definitions.md&5.10&- Cantonese pop music
ct02-Definitions.md&5.10&
ct02-Definitions.md&5.11&## Now try it yourself!
ct02-Definitions.md&5.11&
ct02-Definitions.md&5.11&Give genus-difference definitions for the following:
ct02-Definitions.md&5.11&
ct02-Definitions.md&5.11&>- Cinema: a hall where people buy a ticket to watch movies together.
ct02-Definitions.md&5.11&>- Ship: a vessel which floats on the surface of water.
ct02-Definitions.md&5.11&>- Powerpoint: a computer program for making presentations.
ct02-Definitions.md&5.11&>- Apple:
ct02-Definitions.md&5.11&    - a fruit that is red or green and sweet (?)
ct02-Definitions.md&5.11&    - Or: the fruit of the apple-tree (?)
ct02-Definitions.md&5.11&    - Or: the computer company which makes the iPhone
ct02-Definitions.md&5.11&    - Or: the fruit that inspired Newton to think about gravitation
ct02-Definitions.md&5.11&>- Cantonese pop music: Here it depends on what you want to define. Are you defining "Cantonese," or "pop music," or "music"? Take care not to make a circular definition!
ct02-Definitions.md&5.11&
ct02-Definitions.md&6.0&# Problems with definitions
ct02-Definitions.md&6.0&
ct02-Definitions.md&6.1&## Circular definitions
ct02-Definitions.md&6.1&
ct02-Definitions.md&6.1&1. In one definition: “A cook is someone who cooks.”
ct02-Definitions.md&6.1&
ct02-Definitions.md&6.1&2. In two related definitions:
ct02-Definitions.md&6.1&    - “Apple: the fruit of the apple-tree.”
ct02-Definitions.md&6.1&    - “Apple-tree: a tree which produces apples.”
ct02-Definitions.md&6.1&
ct02-Definitions.md&6.2&## Circular definitions (2)
ct02-Definitions.md&6.2&
ct02-Definitions.md&6.2&*Circular definitions don't define anything!*
ct02-Definitions.md&6.2&
ct02-Definitions.md&6.2&. . .
ct02-Definitions.md&6.2&
ct02-Definitions.md&6.2&To see this, just replace the word being defined by something meaningless.
ct02-Definitions.md&6.2&
ct02-Definitions.md&6.2&“A tagaf is someone who tagafs.”
ct02-Definitions.md&6.2&
ct02-Definitions.md&6.2&Can you now say what a “tagaf” is?
ct02-Definitions.md&6.2&
ct02-Definitions.md&6.3&## Circular definitions (3)
ct02-Definitions.md&6.3&
ct02-Definitions.md&6.3&Or:
ct02-Definitions.md&6.3&
ct02-Definitions.md&6.3&1. Tagaf: the fruit of the tagaf-tree.
ct02-Definitions.md&6.3&2. Tagaf-tree: a tree which produces tagafs.
ct02-Definitions.md&6.3&
ct02-Definitions.md&6.3&Can you now say what a “tagaf” is? -- No.
ct02-Definitions.md&6.3&
ct02-Definitions.md&6.3&It could be anything: apples, bananas, oranges.
ct02-Definitions.md&6.3&
ct02-Definitions.md&6.4&## Circular definitions (4)
ct02-Definitions.md&6.4&
ct02-Definitions.md&6.4&Now make the same test with a correct definition:
ct02-Definitions.md&6.4&
ct02-Definitions.md&6.4&Tagaf: a hall where people have to buy a ticket in order to watch movies together.
ct02-Definitions.md&6.4&
ct02-Definitions.md&6.4&You see that you can still understand what is being defined!
ct02-Definitions.md&6.4&
ct02-Definitions.md&6.5&## Test for definitions
ct02-Definitions.md&6.5&
ct02-Definitions.md&6.5&You can test whether a definition is a good one, by replacing the word which is being defined by a meaningless word. 
ct02-Definitions.md&6.5&
ct02-Definitions.md&6.5&If you can still understand what is being defined, then the definition is good.
ct02-Definitions.md&6.5&
ct02-Definitions.md&6.6&## Another example
ct02-Definitions.md&6.6&
ct02-Definitions.md&6.6&>- A tagaf is something which is a lot of fun and excitement.
ct02-Definitions.md&6.6&>- (Bad definition, we don't know what exactly it is!)
ct02-Definitions.md&6.6&
ct02-Definitions.md&6.6&. . .
ct02-Definitions.md&6.6&
ct02-Definitions.md&6.6&But:
ct02-Definitions.md&6.6&
ct02-Definitions.md&6.6&>- A tagaf is a red, round, soft vegetable, used to make ketchup.
ct02-Definitions.md&6.6&>- (Good definition: we know it's a tomato!)
ct02-Definitions.md&6.6&
ct02-Definitions.md&6.7&## Persuasive definitions
ct02-Definitions.md&6.7&
ct02-Definitions.md&6.7&Definitions are not always objective.
ct02-Definitions.md&6.7&
ct02-Definitions.md&6.7&Persuasive definitions are designed to transfer emotions, such as feelings of approval or disapproval.
ct02-Definitions.md&6.7&
ct02-Definitions.md&6.7&Compare:
ct02-Definitions.md&6.7&
ct02-Definitions.md&6.7&1. Greece: a South-European country located between Italy and Turkey.
ct02-Definitions.md&6.7&2. Greece: a South-European country that was home to a great ancient civilisation.
ct02-Definitions.md&6.7&
ct02-Definitions.md&6.8&## Persuasive definitions
ct02-Definitions.md&6.8&
ct02-Definitions.md&6.8&Compare:
ct02-Definitions.md&6.8&
ct02-Definitions.md&6.8&1. Homosexual: somebody who is attracted by the same sex.
ct02-Definitions.md&6.8&2. Homosexual: somebody who has an unnatural desire for those of the same sex.
ct02-Definitions.md&6.8&
ct02-Definitions.md&6.9&## Persuasive definitions
ct02-Definitions.md&6.9&
ct02-Definitions.md&6.9&Compare:
ct02-Definitions.md&6.9&
ct02-Definitions.md&6.9&1. China: a country, occupying the easternmost part of the Asian continent.
ct02-Definitions.md&6.9&2. China: a country in Asia, home to many great poets and philosophers.
ct02-Definitions.md&6.9&3. China: a country in Asia, home to many poor people.
ct02-Definitions.md&6.9&
ct02-Definitions.md&6.10&## Persuasive definitions
ct02-Definitions.md&6.10&
ct02-Definitions.md&6.10&Persuasive definitions can be very subtle and hard to detect.
ct02-Definitions.md&6.10&
ct02-Definitions.md&6.10&They try to influence us to accept their view of the world, while pretending to be objective.
ct02-Definitions.md&6.10&
ct02-Definitions.md&6.11&## Persuasive vocabulary
ct02-Definitions.md&6.11&
ct02-Definitions.md&6.11&Single words can also be persuasive. Have a look at the following:
ct02-Definitions.md&6.11&
ct02-Definitions.md&6.11&- chairman / chairperson
ct02-Definitions.md&6.11&- cop / police officer
ct02-Definitions.md&6.11&- house / home
ct02-Definitions.md&6.11&- the guy over there / the man over there / the gentleman over there
ct02-Definitions.md&6.11&
ct02-Definitions.md&6.12&## Exercise
ct02-Definitions.md&6.12&
ct02-Definitions.md&6.12&Try to locate the problems in the following definitions:
ct02-Definitions.md&6.12&
ct02-Definitions.md&6.12&1. Democracy is a form of government where all people are free and have the same rights.
ct02-Definitions.md&6.12&2. A book is a source of pleasure and entertainment.
ct02-Definitions.md&6.12&3. A car is a means of transport.
ct02-Definitions.md&6.12&4. A car is a means of transport with four wheels and four doors, which is used on streets.
ct02-Definitions.md&6.12&5. A car is a means of transport which is often dangerous.
ct02-Definitions.md&6.12&
ct02-Definitions.md&6.13&## Solutions (1)
ct02-Definitions.md&6.13&
ct02-Definitions.md&6.13&"Democracy is a form of government where all people are free and have the same rights."
ct02-Definitions.md&6.13&
ct02-Definitions.md&6.13&1. Persuasive. 
ct02-Definitions.md&6.13&2. Does not specify key points of democracy, for example that the majority decides on issues.
ct02-Definitions.md&6.13&3. Not all people are free (we have prisons in democracies, too!)
ct02-Definitions.md&6.13&
ct02-Definitions.md&6.14&## Solutions (2)
ct02-Definitions.md&6.14&
ct02-Definitions.md&6.14&"A book is a source of pleasure and entertainment."
ct02-Definitions.md&6.14&
ct02-Definitions.md&6.14&1. Persuasive. 
ct02-Definitions.md&6.14&2. Too broad (a cinema or a TV set are also sources of pleasure and entertainment).
ct02-Definitions.md&6.14&3. No genus and difference.
ct02-Definitions.md&6.14&
ct02-Definitions.md&6.15&## Solutions (3)
ct02-Definitions.md&6.15&
ct02-Definitions.md&6.15&"A car is a means of transport."
ct02-Definitions.md&6.15&
ct02-Definitions.md&6.15&1. Too broad (includes bicycles, ships and airplanes).
ct02-Definitions.md&6.15&2. The difference is missing.
ct02-Definitions.md&6.15&
ct02-Definitions.md&6.16&## Solutions (4)
ct02-Definitions.md&6.16&
ct02-Definitions.md&6.16&"A car is a means of transport with four wheels and four doors, which is used on streets."
ct02-Definitions.md&6.16&
ct02-Definitions.md&6.16&Too narrow: some cars have only two doors; cars can drive off-street.
ct02-Definitions.md&6.16&
ct02-Definitions.md&6.17&## Solutions (5)
ct02-Definitions.md&6.17&
ct02-Definitions.md&6.17&"A car is a means of transport which is often dangerous."
ct02-Definitions.md&6.17&
ct02-Definitions.md&6.17&1. Persuasive. Almost everything can be dangerous.
ct02-Definitions.md&6.17&2. Difference is missing.
ct02-Definitions.md&6.17&
ct02-Definitions.md&6.18&## Is this a good definition?
ct02-Definitions.md&6.18&
ct02-Definitions.md&6.18&"Physics is the systematic study of objects, processes and properties that are physical in nature."
ct02-Definitions.md&6.18&
ct02-Definitions.md&6.18&. . .
ct02-Definitions.md&6.18&
ct02-Definitions.md&6.18&Circular definition.
ct02-Definitions.md&6.18&
ct02-Definitions.md&6.19&## Is this a good definition?
ct02-Definitions.md&6.19&
ct02-Definitions.md&6.19&"Philosophy is the light that shines upon the dark corners of knowledge."
ct02-Definitions.md&6.19&
ct02-Definitions.md&6.19&. . . 
ct02-Definitions.md&6.19&
ct02-Definitions.md&6.19&Persuasive language, wrong (metaphorical) genus, too general.
ct02-Definitions.md&6.19&
ct02-Definitions.md&6.20&## Is this a good definition?
ct02-Definitions.md&6.20&
ct02-Definitions.md&6.20&"A religion is a fairy tale used for indoctrinating the uneducated."
ct02-Definitions.md&6.20&
ct02-Definitions.md&6.20&. . .
ct02-Definitions.md&6.20&
ct02-Definitions.md&6.20&Persuasive language, too general.
ct02-Definitions.md&6.20&
ct02-Definitions.md&6.21&## Is this a good definition?
ct02-Definitions.md&6.21&
ct02-Definitions.md&6.21&"To swim: To move the body forward through water with limbs, fins, or tail."
ct02-Definitions.md&6.21&
ct02-Definitions.md&6.21&. . . 
ct02-Definitions.md&6.21&
ct02-Definitions.md&6.21&Too specific (too narrow). People can swim backwards, and they don't need to swim in *water.* One could, for example, swim in milk.
ct02-Definitions.md&6.21&
ct02-Definitions.md&6.22&## Is this a good definition?
ct02-Definitions.md&6.22&
ct02-Definitions.md&6.22&"To discriminate against a person is to treat that person wrongly without good justification."
ct02-Definitions.md&6.22&
ct02-Definitions.md&6.22&. . . 
ct02-Definitions.md&6.22&
ct02-Definitions.md&6.22&Too general (includes stealing, lying).
ct02-Definitions.md&6.22&
ct02-Definitions.md&6.23&## Is this a good definition?
ct02-Definitions.md&6.23&
ct02-Definitions.md&6.23&“*Girl* refers to any young female human being."
ct02-Definitions.md&6.23&
ct02-Definitions.md&6.23&. . . 
ct02-Definitions.md&6.23&
ct02-Definitions.md&6.23&Good definition.
ct02-Definitions.md&6.23&
ct02-Definitions.md&6.24&## What did we learn today?
ct02-Definitions.md&6.24&
ct02-Definitions.md&6.24&- Descriptive, stipulative, precising, reporive, intensional and extensional definitions
ct02-Definitions.md&6.24&- Genus and difference
ct02-Definitions.md&6.24&- Circular definitions
ct02-Definitions.md&6.24&- Common definition errors
ct02-Definitions.md&6.24&
ct02-Definitions.md&6.25&## Optional reading list
ct02-Definitions.md&6.25&
ct02-Definitions.md&6.25&Merrilee H. Salmon: Introduction to Logic and Critical Thinking. 5th ed. Thomson/Wadsworth, 2007. Definitions, pp. 56-67
ct02-Definitions.md&6.25&
ct02-Definitions.md&6.26&## Any questions?
ct02-Definitions.md&6.26&
ct02-Definitions.md&6.26&![](graphics/questions.jpg)\ 
ct02-Definitions.md&6.26&
ct02-Definitions.md&6.27&## Thank you for your attention!
ct02-Definitions.md&6.27&
ct02-Definitions.md&6.27&Email: <matthias@ln.edu.hk>
ct02-Definitions.md&6.27&
ct02-Definitions.md&6.27&If you have questions, please come to my office hours (see course outline).
ct03-Propositions.md&0.1&% Critical Thinking
ct03-Propositions.md&0.1&% 3. Propositions
ct03-Propositions.md&0.1&% A. Matthias
ct03-Propositions.md&0.1&
ct03-Propositions.md&1.0&# Where we are
ct03-Propositions.md&1.0&
ct03-Propositions.md&1.1&## What did we learn last time?
ct03-Propositions.md&1.1&
ct03-Propositions.md&1.1&- Intensional and extensional definitions
ct03-Propositions.md&1.1&- Genus and difference
ct03-Propositions.md&1.1&- Circular definitions
ct03-Propositions.md&1.1&- Common definition errors
ct03-Propositions.md&1.1&
ct03-Propositions.md&1.2&## What are we going to learn today?
ct03-Propositions.md&1.2&
ct03-Propositions.md&1.2&- How to correct unclear definitions
ct03-Propositions.md&1.2&- What is a proposition?
ct03-Propositions.md&1.2&- What is a non-propositional utterance?
ct03-Propositions.md&1.2&- The language we use in daily life can be represented symbolically
ct03-Propositions.md&1.2&- One way to do this is the so-called propositional calculus
ct03-Propositions.md&1.2&- Propositions can be combined using logical operations
ct03-Propositions.md&1.2&
ct03-Propositions.md&2.0&# Unclear and inaccurate definitions
ct03-Propositions.md&2.0&
ct03-Propositions.md&2.1&## Unclear and inaccurate definitions
ct03-Propositions.md&2.1&
ct03-Propositions.md&2.1&Look at the (bad) definitions below. Find a counter-example for each definition to show the problem (an example which fits the definition but is not the thing defined).
ct03-Propositions.md&2.1&
ct03-Propositions.md&2.1&1. Love means sincerely caring about someone.
ct03-Propositions.md&2.1&2. An airplane is a heavier-than-air device that flies.
ct03-Propositions.md&2.1&3. Tea is a warm beverage people drink instead of coffee.
ct03-Propositions.md&2.1&4. Oxygen is a gas which we need to breathe in order to stay alive.
ct03-Propositions.md&2.1&
ct03-Propositions.md&2.2&## Unclear and inaccurate definitions
ct03-Propositions.md&2.2&
ct03-Propositions.md&2.2&Look at the definitions below. Identify all ambiguous, persuasive or unclear points, and write a new definition to solve those problems:
ct03-Propositions.md&2.2&
ct03-Propositions.md&2.2&1. A dog is man's best friend.
ct03-Propositions.md&2.2&2. Poetry is the kind of thing poets write.
ct03-Propositions.md&2.2&3. A drug is any substance that has an effect on the body or mind.
ct03-Propositions.md&2.2&
ct03-Propositions.md&3.0&# Propositions
ct03-Propositions.md&3.0&
ct03-Propositions.md&3.1&## How would you group the following sentences into two groups?
ct03-Propositions.md&3.1&
ct03-Propositions.md&3.1&- Airplanes fly
ct03-Propositions.md&3.1&- The sea is made of wine
ct03-Propositions.md&3.1&- I was in Greece last summer
ct03-Propositions.md&3.1&- What is your name?
ct03-Propositions.md&3.1&- Come here!
ct03-Propositions.md&3.1&- Oh, no!
ct03-Propositions.md&3.1&
ct03-Propositions.md&3.2&## Propositions (\cn{命題})
ct03-Propositions.md&3.2&
ct03-Propositions.md&3.2&Propositions we call sentences which can be true or false.
ct03-Propositions.md&3.2&
ct03-Propositions.md&3.2&>- Airplanes fly (true)
ct03-Propositions.md&3.2&>- The sea is made of wine (false)
ct03-Propositions.md&3.2&>- I was in Greece last summer (true or false? But surely one of the two!)
ct03-Propositions.md&3.2&
ct03-Propositions.md&3.3&## Propositions (\cn{命題})
ct03-Propositions.md&3.3&
ct03-Propositions.md&3.3&I don’t need to know whether a sentence is actually true or false, in order for it to be a proposition.
ct03-Propositions.md&3.3&
ct03-Propositions.md&3.3&“On the dark side of the moon there are 5278 craters” *is* a proposition, because it must be either true or false.
ct03-Propositions.md&3.3&
ct03-Propositions.md&3.3&It does not matter that I don’t know which.
ct03-Propositions.md&3.3&
ct03-Propositions.md&3.4&## Non-propositional utterances \cn{(非命題言說)}
ct03-Propositions.md&3.4&
ct03-Propositions.md&3.4&Some other sentences can not be said to be true or false.
ct03-Propositions.md&3.4&
ct03-Propositions.md&3.4&These are all called non-propositional (because they are not propositions) utterances (things that are said; to utter = to say)
ct03-Propositions.md&3.4&
ct03-Propositions.md&3.5&## Non-propositional utterances \cn{(非命題言說)}
ct03-Propositions.md&3.5&
ct03-Propositions.md&3.5&We cannot meaningfully say that the following are true of false:
ct03-Propositions.md&3.5&
ct03-Propositions.md&3.5&- What is your name? 
ct03-Propositions.md&3.5&- Come here!
ct03-Propositions.md&3.5&- Oh, no!
ct03-Propositions.md&3.5&
ct03-Propositions.md&3.6&## Which of the following are propositions?
ct03-Propositions.md&3.6&
ct03-Propositions.md&3.6&1. Today is Monday
ct03-Propositions.md&3.6&2. Every day is Monday
ct03-Propositions.md&3.6&3. Help!
ct03-Propositions.md&3.6&4. I love China!
ct03-Propositions.md&3.6&5. Do you love China?
ct03-Propositions.md&3.6&6. It will rain tomorrow
ct03-Propositions.md&3.6&
ct03-Propositions.md&3.7&## Which of the following are **propositions** (P)?
ct03-Propositions.md&3.7&
ct03-Propositions.md&3.7&- **Today is Monday** (P)
ct03-Propositions.md&3.7&- **Every day is Monday** (P)
ct03-Propositions.md&3.7&- Help!
ct03-Propositions.md&3.7&- **I love China!** (P) (must be true or false, even if others don't know which)
ct03-Propositions.md&3.7&- Do you love China?
ct03-Propositions.md&3.7&- **It will rain tomorrow** (P)
ct03-Propositions.md&3.7&
ct03-Propositions.md&3.8&## Test for propositions
ct03-Propositions.md&3.8&
ct03-Propositions.md&3.8&Sometimes it is helpful to add “It is true that...” in front of a sentence, in order to see whether it is a proposition.
ct03-Propositions.md&3.8&
ct03-Propositions.md&3.8&If it is one, the resulting sentence will be grammatically correct. Otherwise it will not make sense. (But there are exceptions!)
ct03-Propositions.md&3.8&
ct03-Propositions.md&3.9&## Test for propositions
ct03-Propositions.md&3.9&
ct03-Propositions.md&3.9&>- It is true that today is Monday (correct sentence)
ct03-Propositions.md&3.9&>- It is true that every day is Monday (wrong, but grammatically correct)
ct03-Propositions.md&3.9&>- It is true that help! (?)
ct03-Propositions.md&3.9&>- It is true that I love China! (correct sentence)
ct03-Propositions.md&3.9&>- It is true that do you love China? (?)
ct03-Propositions.md&3.9&>- It is true that it will rain tomorrow. (correct sentence)
ct03-Propositions.md&3.9&
ct03-Propositions.md&3.10&## Propositions
ct03-Propositions.md&3.10&
ct03-Propositions.md&3.10&Propositions are statements which can be true or false.
ct03-Propositions.md&3.10&
ct03-Propositions.md&3.10&- The cat is here.
ct03-Propositions.md&3.10&- Today is Monday.
ct03-Propositions.md&3.10&- I love China.
ct03-Propositions.md&3.10&
ct03-Propositions.md&4.0&# Symbolic representation of propositions
ct03-Propositions.md&4.0&
ct03-Propositions.md&4.1&## Symbolic representation
ct03-Propositions.md&4.1&
ct03-Propositions.md&4.1&Sometimes it is useful to represent propositions symbolically, in order to better see their structure.
ct03-Propositions.md&4.1&
ct03-Propositions.md&4.1&Propositions can be represented symbolically with letters of the alphabet.
ct03-Propositions.md&4.1&
ct03-Propositions.md&4.1&Usually we start with “p” for “proposition”.
ct03-Propositions.md&4.1&
ct03-Propositions.md&4.2&## Propositions
ct03-Propositions.md&4.2&
ct03-Propositions.md&4.2&p: The cat is here.  
ct03-Propositions.md&4.2&q: Today is Monday.  
ct03-Propositions.md&4.2&r: I love China.  
ct03-Propositions.md&4.2&
ct03-Propositions.md&4.3&## Propositions and logical glue
ct03-Propositions.md&4.3&
ct03-Propositions.md&4.3&In sentences, propositions are often connected with words like “and”, “or”, “not”:
ct03-Propositions.md&4.3&
ct03-Propositions.md&4.3&- I will eat **or** I will sleep: p or q
ct03-Propositions.md&4.3&- I want to eat **and** sleep: p and q
ct03-Propositions.md&4.3&- I will **not** eat noodles: not p
ct03-Propositions.md&4.3&
ct03-Propositions.md&4.4&## Logical operations
ct03-Propositions.md&4.4&
ct03-Propositions.md&4.4&*How many propositions do you see in each of the following statements? How are they connected?*
ct03-Propositions.md&4.4&
ct03-Propositions.md&4.4&1. Yesterday I went shopping
ct03-Propositions.md&4.4&2. Yesterday I went shopping and bought a watch
ct03-Propositions.md&4.4&3. Yesterday I did not go shopping
ct03-Propositions.md&4.4&4. Tomorrow I will go shopping or to the cinema
ct03-Propositions.md&4.4&
ct03-Propositions.md&4.5&## Logical operations
ct03-Propositions.md&4.5&
ct03-Propositions.md&4.5&Try to separate the single propositions from the logic which glues them together:
ct03-Propositions.md&4.5&
ct03-Propositions.md&4.5&>- "Yesterday I went shopping": p
ct03-Propositions.md&4.5&>- "Yesterday I went shopping **and** bought a watch": p **and** q
ct03-Propositions.md&4.5&>- “Yesterday I did not go shopping”: "It is **not** the case that yesterday I went shopping": **not** r
ct03-Propositions.md&4.5&>- “Tomorrow I will go shopping or to the cinema”: "Tomorrow I will go shopping **or** tomorrow I will go to the cinema": s **or** t
ct03-Propositions.md&4.5&
ct03-Propositions.md&5.0&# Logical operators
ct03-Propositions.md&5.0&
ct03-Propositions.md&5.1&## Logical operators
ct03-Propositions.md&5.1&
ct03-Propositions.md&5.1&The operations “and”, “or” and “not” are called *logical operators.*
ct03-Propositions.md&5.1&
ct03-Propositions.md&5.1&They are usually represented by symbols:
ct03-Propositions.md&5.1&
ct03-Propositions.md&5.1&>- “and” (conjunction): $\land$ or \&
ct03-Propositions.md&5.1&>- “or” (disjunction): $\lor$ or v
ct03-Propositions.md&5.1&>- “not” (negation): $\lnot$ or ~
ct03-Propositions.md&5.1&
ct03-Propositions.md&5.2&## Logical operators
ct03-Propositions.md&5.2&
ct03-Propositions.md&5.2&>- Yesterday I went shopping: p
ct03-Propositions.md&5.2&>- Yesterday I went shopping and (I) bought a watch: p$\land$q (or: p\&q)
ct03-Propositions.md&5.2&>- Yesterday I did not go shopping: $\lnot$ p (or: ~p)
ct03-Propositions.md&5.2&>- Tomorrow I will go shopping or to the cinema: r$\lor$s
ct03-Propositions.md&5.2&
ct03-Propositions.md&5.3&## Now do it yourself:
ct03-Propositions.md&5.3&
ct03-Propositions.md&5.3&Write down the symbolic representation of the following sentences:
ct03-Propositions.md&5.3&
ct03-Propositions.md&5.3&- This is a keyboard.
ct03-Propositions.md&5.3&- This is not a mouse.
ct03-Propositions.md&5.3&- It will rain tomorrow
ct03-Propositions.md&5.3&- It will not rain tomorrow
ct03-Propositions.md&5.3&- It will rain tomorrow and I will stay at home
ct03-Propositions.md&5.3&- It will rain tomorrow, and I will stay at home and eat a salad or a noodle soup
ct03-Propositions.md&5.3&
ct03-Propositions.md&5.4&## Now do it yourself:
ct03-Propositions.md&5.4&
ct03-Propositions.md&5.4&Of course, it doesn't matter which letters we use!
ct03-Propositions.md&5.4&
ct03-Propositions.md&5.4&If the examples are not connected, you can use the same letters! 
ct03-Propositions.md&5.4&
ct03-Propositions.md&5.4&>- This is a keyboard: A
ct03-Propositions.md&5.4&>- This is not a mouse: $\lnot$A
ct03-Propositions.md&5.4&>- It will rain tomorrow: A
ct03-Propositions.md&5.4&>- It will not rain tomorrow:  $\lnot$A
ct03-Propositions.md&5.4&>- It will rain tomorrow and I will stay at home: A$\land$B
ct03-Propositions.md&5.4&
ct03-Propositions.md&5.5&## Now do it yourself:
ct03-Propositions.md&5.5&
ct03-Propositions.md&5.5&“It will rain tomorrow, and I will stay at home and eat a salad or a noodle soup”:
ct03-Propositions.md&5.5&
ct03-Propositions.md&5.5&A: It will rain tomorrow  
ct03-Propositions.md&5.5&B: I will stay at home  
ct03-Propositions.md&5.5&C: I will eat a salad  
ct03-Propositions.md&5.5&D: I will eat a noodle soup  
ct03-Propositions.md&5.5&
ct03-Propositions.md&5.5&\f{A$\land$B$\land$(C$\lor$D)}
ct03-Propositions.md&5.5&
ct03-Propositions.md&5.5&Use parentheses to make the structure clear!
ct03-Propositions.md&5.5&
ct03-Propositions.md&5.6&## Use parentheses to make the structure clear
ct03-Propositions.md&5.6&
ct03-Propositions.md&5.6&It will rain tomorrow, and I will stay at home and eat (a salad or a noodle soup):
ct03-Propositions.md&5.6&
ct03-Propositions.md&5.6&\f{A$\land$B$\land$(C$\lor$D)}
ct03-Propositions.md&5.6&
ct03-Propositions.md&5.6&(It will rain tomorrow, and I will stay at home and eat a salad) or I will eat a noodle soup:
ct03-Propositions.md&5.6&
ct03-Propositions.md&5.6&\f{(A$\land$B$\land$C)$\lor$D}
ct03-Propositions.md&5.6&
ct03-Propositions.md&5.6&(this doesn't make much sense!)
ct03-Propositions.md&5.6&
ct03-Propositions.md&5.7&## Implication and biconditional
ct03-Propositions.md&5.7&
ct03-Propositions.md&5.7&There are two additional logical operators:
ct03-Propositions.md&5.7&
ct03-Propositions.md&5.7&Implication: $\to$
ct03-Propositions.md&5.7&
ct03-Propositions.md&5.7&and co-implication or biconditional: $\leftrightarrow$
ct03-Propositions.md&5.7&
ct03-Propositions.md&5.8&## Implication
ct03-Propositions.md&5.8&
ct03-Propositions.md&5.8&The implication “implies” that if something is the case, then something else will also be the case.
ct03-Propositions.md&5.8&
ct03-Propositions.md&5.8&"If it rains (p), (then) the street will be wet (q)": p $\to$ q
ct03-Propositions.md&5.8&
ct03-Propositions.md&5.9&## Implication
ct03-Propositions.md&5.9&
ct03-Propositions.md&5.9&The implication is true only in one direction!
ct03-Propositions.md&5.9&
ct03-Propositions.md&5.9&We cannot turn it around:
ct03-Propositions.md&5.9&
ct03-Propositions.md&5.9&Assume that “If it rains, then the street will be wet” (p $\to$ q) is true.
ct03-Propositions.md&5.9&
ct03-Propositions.md&5.9&Then “If the street is wet, then it has rained” (q $\to$ p) is not necessarily true (because somebody might have spilled a bucket of water)
ct03-Propositions.md&5.9&
ct03-Propositions.md&5.10&## Logical biconditional
ct03-Propositions.md&5.10&
ct03-Propositions.md&5.10&The logical biconditional operator expresses the fact that two propositions can only be true or false together (because they imply each other).
ct03-Propositions.md&5.10&
ct03-Propositions.md&5.10&It amounts to saying that something is the case *if and only if* something else is the case.
ct03-Propositions.md&5.10&
ct03-Propositions.md&5.11&## Biconditional
ct03-Propositions.md&5.11&
ct03-Propositions.md&5.11&>- If the sun shines, the sky will be blue: p $\to$ q
ct03-Propositions.md&5.11&>- If the sky is blue, the sun will be shining: q $\to$ p
ct03-Propositions.md&5.11&>- “The sky is blue” co-implies “the sun is shining” (that is, the sky is blue if and only if the sun is shining): p $\leftrightarrow$ q
ct03-Propositions.md&5.11&
ct03-Propositions.md&5.12&## Now do it yourself:
ct03-Propositions.md&5.12&
ct03-Propositions.md&5.12&Write down the symbolic representation of the following sentences:
ct03-Propositions.md&5.12&
ct03-Propositions.md&5.12&1. If you strike a match, it will light
ct03-Propositions.md&5.12&2. If you strike a match, it will light or it will do nothing
ct03-Propositions.md&5.12&3. If you strike a match and it is wet, it will do nothing
ct03-Propositions.md&5.12&4. If it is Sunday and I'm hungry, I usually eat noodles or a pizza.
ct03-Propositions.md&5.12&
ct03-Propositions.md&5.13&## Now do it yourself:
ct03-Propositions.md&5.13&
ct03-Propositions.md&5.13&Write down the symbolic representation of the following sentences:
ct03-Propositions.md&5.13&
ct03-Propositions.md&5.13&1. If you strike a match, it will light:  
ct03-Propositions.md&5.13&\f{p$\to$q}
ct03-Propositions.md&5.13&2. If you strike a match, it will light or it will do nothing:  
ct03-Propositions.md&5.13&\f{p$\to$(q$\lor$r)}
ct03-Propositions.md&5.13&3. If you strike a match and it (the match) is wet, it will do nothing:  
ct03-Propositions.md&5.13&\f{(p$\land$q)$\to$r}
ct03-Propositions.md&5.13&4. If it is Sunday and I'm hungry, I usually eat noodles or (I eat) a pizza:  
ct03-Propositions.md&5.13&\f{(p$\land$q)$\to$(r$\lor$s)}
ct03-Propositions.md&5.13&
ct03-Propositions.md&5.14&## Comments
ct03-Propositions.md&5.14&
ct03-Propositions.md&5.14&"If you strike a match, it will light or it will do nothing": p$\to$(q$\lor$r)
ct03-Propositions.md&5.14&
ct03-Propositions.md&5.14&This could be understood to mean:
ct03-Propositions.md&5.14&
ct03-Propositions.md&5.14&"If you strike a match, it will light or it will not light": p$\to$(q$\lor\lnot$q)
ct03-Propositions.md&5.14&
ct03-Propositions.md&5.14&>- But this is a different expression!
ct03-Propositions.md&5.14&>- “Do nothing” means more than “not light”: for example it means it will not break, not fall to the floor and so on.
ct03-Propositions.md&5.14&>- Please be precise!
ct03-Propositions.md&5.14&
ct03-Propositions.md&5.15&## Now do it yourself backwards:
ct03-Propositions.md&5.15&
ct03-Propositions.md&5.15&Write down example sentences for the following symbolic forms:
ct03-Propositions.md&5.15&
ct03-Propositions.md&5.15&- (p$\land$q) $\to$ (r$\land$s)
ct03-Propositions.md&5.15&- p $\to$ q
ct03-Propositions.md&5.15&- q $\to$ $\lnot$p
ct03-Propositions.md&5.15&- q $\leftrightarrow$ $\lnot$p
ct03-Propositions.md&5.15&
ct03-Propositions.md&5.16&## Now do it yourself backwards:
ct03-Propositions.md&5.16&
ct03-Propositions.md&5.16&>- (p$\land$q)$\to$(r$\land$s)  
ct03-Propositions.md&5.16&If you get up early (p) and work hard (q) then you will be rich (r) and famous (s).
ct03-Propositions.md&5.16&>- p $\to$ q  
ct03-Propositions.md&5.16&If I do sports, I will be more healthy.
ct03-Propositions.md&5.16&>- q$\to\lnot$p  
ct03-Propositions.md&5.16&If I go out every night, I will not get good grades
ct03-Propositions.md&5.16&>- q$\leftrightarrow\lnot$p
ct03-Propositions.md&5.16&>      - If and only if I get an ice cream now, I will not complain.
ct03-Propositions.md&5.16&>      - If and only if I'm dead, then I am not alive.
ct03-Propositions.md&5.16&
ct03-Propositions.md&6.0&# Exercises
ct03-Propositions.md&6.0&
ct03-Propositions.md&6.1&## From old exams
ct03-Propositions.md&6.1&
ct03-Propositions.md&6.1&Which of the following are propositions? (check all that apply)
ct03-Propositions.md&6.1&
ct03-Propositions.md&6.1&1. Stand over there.
ct03-Propositions.md&6.1&2. I like the music of Mozart.
ct03-Propositions.md&6.1&3. Do you like his music?
ct03-Propositions.md&6.1&4. I’m sure you will like his music!
ct03-Propositions.md&6.1&5. Don’t tell me you don’t like it.
ct03-Propositions.md&6.1&
ct03-Propositions.md&6.2&## From old exams
ct03-Propositions.md&6.2&
ct03-Propositions.md&6.2&Convert into a formal expression:
ct03-Propositions.md&6.2& 
ct03-Propositions.md&6.2&“If and only if it is Monday and I have not eaten yet, I will call my friend and we will go to have lunch together.”
ct03-Propositions.md&6.2&
ct03-Propositions.md&6.3&## From old exams
ct03-Propositions.md&6.3&
ct03-Propositions.md&6.3&Find an English sentence with the following logical structure: 
ct03-Propositions.md&6.3&
ct03-Propositions.md&6.3&\f{P$\to$((Q$\land$R)$\lor$Q)}
ct03-Propositions.md&6.3&
ct03-Propositions.md&6.4&## From old exams
ct03-Propositions.md&6.4&
ct03-Propositions.md&6.4&“I want a soup with noodles or potatoes if and only if it is hot and not too salty and if it contains dumplings with pork or cabbage but no prawns.”
ct03-Propositions.md&6.4&
ct03-Propositions.md&6.4&A. (A$\lor$D)$\to$(B$\land\lnot$C$\land$G$\land$(E$\lor$F$\land\lnot$I))  
ct03-Propositions.md&6.4&B. (A$\lor$D)$\leftrightarrow$(B$\land\lnot$C$\land$G$\land$(E$\lor$F$\lor\lnot$I))  
ct03-Propositions.md&6.4&C. (A$\lor$D)$\leftrightarrow$(B$\land\lnot$C$\lor$G$\land$(E$\lor$F$\land\lnot$I))  
ct03-Propositions.md&6.4&D. (A$\lor$D)$\leftrightarrow$(B$\land\lnot$C$\land$G$\land$(E$\land$F$\lor\lnot$I))  
ct03-Propositions.md&6.4&E. (A$\lor$D)$\leftrightarrow$(B$\land\lnot$C$\land$G$\land$(E$\lor$F$\land\lnot$I))  
ct03-Propositions.md&6.4&
ct03-Propositions.md&6.5&## References
ct03-Propositions.md&6.5&
ct03-Propositions.md&6.5&Some examples from:
ct03-Propositions.md&6.5&
ct03-Propositions.md&6.5&- J. Fahnestock, M. Secor: A Rhetoric of Argument (2nd ed.), Lingnan Library: PE 143.F3 1990
ct03-Propositions.md&6.5&- Hinderer, Drew E.: Building Arguments. Lingnan Library: PE 1431.H5 1992
ct03-Propositions.md&6.5&
ct03-Propositions.md&6.6&## What did we learn today?
ct03-Propositions.md&6.6&
ct03-Propositions.md&6.6&- How to correct unclear definitions
ct03-Propositions.md&6.6&- What is a proposition?
ct03-Propositions.md&6.6&- What is a non-propositional utterance?
ct03-Propositions.md&6.6&- The language we use in daily life can be represented symbolically
ct03-Propositions.md&6.6&- One way to do this is the so-called propositional calculus
ct03-Propositions.md&6.6&- Propositions can be combined using logical operations
ct03-Propositions.md&6.6&
ct03-Propositions.md&6.7&## Any questions?
ct03-Propositions.md&6.7&
ct03-Propositions.md&6.7&![](graphics/questions.jpg)\ 
ct03-Propositions.md&6.7&
ct03-Propositions.md&6.8&## Thank you for your attention!
ct03-Propositions.md&6.8&
ct03-Propositions.md&6.8&Email: <matthias@ln.edu.hk>
ct03-Propositions.md&6.8&
ct03-Propositions.md&6.8&If you have questions, please come to my office hours (see course outline).
ct04-Truth-Tables.md&0.1&% Critical Thinking
ct04-Truth-Tables.md&0.1&% 4. Propositional Logic and Truth Tables
ct04-Truth-Tables.md&0.1&% A. Matthias
ct04-Truth-Tables.md&0.1&
ct04-Truth-Tables.md&1.0&# Where we are
ct04-Truth-Tables.md&1.1&## What did we learn last time?
ct04-Truth-Tables.md&1.1&
ct04-Truth-Tables.md&1.1&- How to correct unclear definitions
ct04-Truth-Tables.md&1.1&- What is a proposition?
ct04-Truth-Tables.md&1.1&- What is a non-propositional utterance?
ct04-Truth-Tables.md&1.1&- The language we use in daily life can be represented symbolically
ct04-Truth-Tables.md&1.1&- One way to do this is the so-called propositional calculus
ct04-Truth-Tables.md&1.1&- Propositions can be combined using logical operations
ct04-Truth-Tables.md&1.1&
ct04-Truth-Tables.md&1.2&## What are we going to learn today?
ct04-Truth-Tables.md&1.2&
ct04-Truth-Tables.md&1.2&- Logical operations can be described by truth tables
ct04-Truth-Tables.md&1.2&
ct04-Truth-Tables.md&2.0&# The five basic logical operations
ct04-Truth-Tables.md&2.0&
ct04-Truth-Tables.md&2.1&## Truth values
ct04-Truth-Tables.md&2.1&
ct04-Truth-Tables.md&2.1&>- In order to be able to analyze the truth of ordinary-language sentences, we must first understand truth values.
ct04-Truth-Tables.md&2.1&>- We already know that a proposition is a statement which can be true or false.
ct04-Truth-Tables.md&2.1&>- We denote true with T and false with F.
ct04-Truth-Tables.md&2.1&>      - Today is Monday: (if it is really Monday) T
ct04-Truth-Tables.md&2.1&>      - Everyday is Monday: F
ct04-Truth-Tables.md&2.1&>      - You are students: T
ct04-Truth-Tables.md&2.1&>      - It rained yesterday: F
ct04-Truth-Tables.md&2.1&>      - 2+2=4 : T
ct04-Truth-Tables.md&2.1&
ct04-Truth-Tables.md&2.2&## True and false
ct04-Truth-Tables.md&2.2&
ct04-Truth-Tables.md&2.2&>- If we apply the logical operators to propositions, we can calculate the truth of the resulting expression.
ct04-Truth-Tables.md&2.2&>- Let's assume p is true.
ct04-Truth-Tables.md&2.2&>- Then $\lnot$p is false.
ct04-Truth-Tables.md&2.2&>- Example: if it is true that it rains (p), then it is false that it does not rain ($\lnot$p).
ct04-Truth-Tables.md&2.2&>- Thus:
ct04-Truth-Tables.md&2.2&>      - If p is true, then $\lnot$p is false.
ct04-Truth-Tables.md&2.2&>      - If p is false, then $\lnot$p is true.
ct04-Truth-Tables.md&2.2&
ct04-Truth-Tables.md&2.3&## True and false
ct04-Truth-Tables.md&2.3&
ct04-Truth-Tables.md&2.3&We can write this in table form (“truth table”):
ct04-Truth-Tables.md&2.3&
ct04-Truth-Tables.md&2.3&\begin{center}
ct04-Truth-Tables.md&2.3&\begin{tabular}{|c||c|}
ct04-Truth-Tables.md&2.3&\hline
ct04-Truth-Tables.md&2.3&$ p $ & $  \neg p $ \\
ct04-Truth-Tables.md&2.3&\hline
ct04-Truth-Tables.md&2.3&F & T \\
ct04-Truth-Tables.md&2.3&\hline
ct04-Truth-Tables.md&2.3&T & F \\
ct04-Truth-Tables.md&2.3&\hline
ct04-Truth-Tables.md&2.3&\end{tabular}
ct04-Truth-Tables.md&2.3&\end{center}
ct04-Truth-Tables.md&2.3&
ct04-Truth-Tables.md&2.4&## Truth table for conjunction
ct04-Truth-Tables.md&2.4&
ct04-Truth-Tables.md&2.4&We can write down truth tables for all logical operations:
ct04-Truth-Tables.md&2.4&
ct04-Truth-Tables.md&2.4&E.g. conjunction (“and”):
ct04-Truth-Tables.md&2.4&
ct04-Truth-Tables.md&2.4&\begin{center}
ct04-Truth-Tables.md&2.4&\begin{tabular}{|c|c||c|}
ct04-Truth-Tables.md&2.4&\hline
ct04-Truth-Tables.md&2.4&$ p $ & $ q $ & $ (p \wedge q) $ \\
ct04-Truth-Tables.md&2.4&\hline
ct04-Truth-Tables.md&2.4&F & F & F \\
ct04-Truth-Tables.md&2.4&\hline
ct04-Truth-Tables.md&2.4&F & T & F \\
ct04-Truth-Tables.md&2.4&\hline
ct04-Truth-Tables.md&2.4&T & F & F \\
ct04-Truth-Tables.md&2.4&\hline
ct04-Truth-Tables.md&2.4&T & T & T \\
ct04-Truth-Tables.md&2.4&\hline
ct04-Truth-Tables.md&2.4&\end{tabular}
ct04-Truth-Tables.md&2.4&\end{center}
ct04-Truth-Tables.md&2.4&
ct04-Truth-Tables.md&2.4&. . . 
ct04-Truth-Tables.md&2.4&
ct04-Truth-Tables.md&2.4&>- What does this mean?
ct04-Truth-Tables.md&2.4&>- p$\land$q is only true, if both p is true and q is true.
ct04-Truth-Tables.md&2.4&>- “I had noodles and an apple for dinner” is only true, if both parts (“I had noodles”, “I had an apple”) are true. Otherwise it is false.
ct04-Truth-Tables.md&2.4&
ct04-Truth-Tables.md&2.4&
ct04-Truth-Tables.md&2.5&## Truth table for disjunction (“or”)
ct04-Truth-Tables.md&2.5&
ct04-Truth-Tables.md&2.5&Now please write down a truth table for disjunction yourself!
ct04-Truth-Tables.md&2.5&
ct04-Truth-Tables.md&2.5&As an example, take the sentence:
ct04-Truth-Tables.md&2.5&
ct04-Truth-Tables.md&2.5&*The Observatory has raised the black rainstorm or the typhoon number 8 signal.*
ct04-Truth-Tables.md&2.5&
ct04-Truth-Tables.md&2.6&## Truth table for disjunction
ct04-Truth-Tables.md&2.6&
ct04-Truth-Tables.md&2.6&\begin{center}
ct04-Truth-Tables.md&2.6&\begin{tabular}{|c|c||c|}
ct04-Truth-Tables.md&2.6&\hline
ct04-Truth-Tables.md&2.6&$ p $ & $ q $ & $ (p \vee q) $ \\
ct04-Truth-Tables.md&2.6&\hline
ct04-Truth-Tables.md&2.6&F & F & F \\
ct04-Truth-Tables.md&2.6&\hline
ct04-Truth-Tables.md&2.6&F & T & T \\
ct04-Truth-Tables.md&2.6&\hline
ct04-Truth-Tables.md&2.6&T & F & T \\
ct04-Truth-Tables.md&2.6&\hline
ct04-Truth-Tables.md&2.6&T & T & T \\
ct04-Truth-Tables.md&2.6&\hline
ct04-Truth-Tables.md&2.6&\end{tabular}
ct04-Truth-Tables.md&2.6&\end{center}
ct04-Truth-Tables.md&2.6&
ct04-Truth-Tables.md&2.6&>- What does this mean?
ct04-Truth-Tables.md&2.6&>- p$\lor$q is only false, if both p is false and q is false.
ct04-Truth-Tables.md&2.6&>- “If there is a black rainstorm or a typhoon number 8 signal, classes will be cancelled.”
ct04-Truth-Tables.md&2.6&>- Classes will be cancelled if any of its parts is true (if there is a black rainstorm, or a number 8 typhoon, or both!) Otherwise they will not be cancelled.
ct04-Truth-Tables.md&2.6&
ct04-Truth-Tables.md&2.7&## Problems with disjunction
ct04-Truth-Tables.md&2.7&
ct04-Truth-Tables.md&2.7&Problem 1:
ct04-Truth-Tables.md&2.7&
ct04-Truth-Tables.md&2.7&>- Why is the result true, when only one of the propositions is true?
ct04-Truth-Tables.md&2.7&>- Example: the University administration says: "You will have to get 3 credits from a CLB course or a CLD course."
ct04-Truth-Tables.md&2.7&>- Now you take a CLB course. Do you satisfy the requirement?
ct04-Truth-Tables.md&2.7&>- Yes.
ct04-Truth-Tables.md&2.7&
ct04-Truth-Tables.md&2.8&## Problems with disjunction
ct04-Truth-Tables.md&2.8&
ct04-Truth-Tables.md&2.8&Problem 2:
ct04-Truth-Tables.md&2.8&
ct04-Truth-Tables.md&2.8&>- Why is the result true, when both propositions are true?
ct04-Truth-Tables.md&2.8&>- Example: "If you steal a car or if you steal a diamond ring, you go to prison."
ct04-Truth-Tables.md&2.8&>- Now if you steal a car, and then use it to drive away with somebody's diamond ring, do you go to prison?
ct04-Truth-Tables.md&2.8&>- Yes.
ct04-Truth-Tables.md&2.8&
ct04-Truth-Tables.md&2.9&## Exclusive “or”
ct04-Truth-Tables.md&2.9&
ct04-Truth-Tables.md&2.9&There is another logical operator, called the “exclusive or ($\oplus$)”, because it excludes the case where both propositions are true.
ct04-Truth-Tables.md&2.9&
ct04-Truth-Tables.md&2.9&This would be a strict “either/or.”
ct04-Truth-Tables.md&2.9&
ct04-Truth-Tables.md&2.9&We will not talk about this here. In this course, we deal only with the “normal” (inclusive) “or.”
ct04-Truth-Tables.md&2.9&
ct04-Truth-Tables.md&2.10&## Truth table for implication
ct04-Truth-Tables.md&2.10&
ct04-Truth-Tables.md&2.10&\begin{center}
ct04-Truth-Tables.md&2.10&\begin{tabular}{|c|c||c|}
ct04-Truth-Tables.md&2.10&\hline
ct04-Truth-Tables.md&2.10&$ p $ & $ q $ & $ (p \rightarrow q) $ \\
ct04-Truth-Tables.md&2.10&\hline
ct04-Truth-Tables.md&2.10&F & F & T \\
ct04-Truth-Tables.md&2.10&\hline
ct04-Truth-Tables.md&2.10&F & T & T \\
ct04-Truth-Tables.md&2.10&\hline
ct04-Truth-Tables.md&2.10&T & F & F \\
ct04-Truth-Tables.md&2.10&\hline
ct04-Truth-Tables.md&2.10&T & T & T \\
ct04-Truth-Tables.md&2.10&\hline
ct04-Truth-Tables.md&2.10&\end{tabular}
ct04-Truth-Tables.md&2.10&\end{center}
ct04-Truth-Tables.md&2.10&
ct04-Truth-Tables.md&2.10&>- What does this mean?
ct04-Truth-Tables.md&2.10&>- p$\to$q is only false if the first proposition is true and the second false.
ct04-Truth-Tables.md&2.10&>- “If it rains, the street will get wet” is true in all cases, except if it rains and the street does not get wet.
ct04-Truth-Tables.md&2.10&
ct04-Truth-Tables.md&2.11&## Implication truth table
ct04-Truth-Tables.md&2.11&
ct04-Truth-Tables.md&2.11&The truth table for the implication does not really match the meaning of “if...then...” very well.
ct04-Truth-Tables.md&2.11&
ct04-Truth-Tables.md&2.11&Why we use it to translate “if...then...” anyway is discussed in:
ct04-Truth-Tables.md&2.11&
ct04-Truth-Tables.md&2.11&<http://philosophy.hku.hk/think/sl/ifthen.php>
ct04-Truth-Tables.md&2.11&
ct04-Truth-Tables.md&2.11&Please have a look there if you are interested in the details.
ct04-Truth-Tables.md&2.11&
ct04-Truth-Tables.md&2.12&## Truth table for co-implication or biconditional
ct04-Truth-Tables.md&2.12&
ct04-Truth-Tables.md&2.12&\begin{center}
ct04-Truth-Tables.md&2.12&\begin{tabular}{|c|c||c|}
ct04-Truth-Tables.md&2.12&\hline
ct04-Truth-Tables.md&2.12&$ p $ & $ q $ & $ (p \leftrightarrow q) $ \\
ct04-Truth-Tables.md&2.12&\hline
ct04-Truth-Tables.md&2.12&F & F & T \\
ct04-Truth-Tables.md&2.12&\hline
ct04-Truth-Tables.md&2.12&F & T & F \\
ct04-Truth-Tables.md&2.12&\hline
ct04-Truth-Tables.md&2.12&T & F & F \\
ct04-Truth-Tables.md&2.12&\hline
ct04-Truth-Tables.md&2.12&T & T & T \\
ct04-Truth-Tables.md&2.12&\hline
ct04-Truth-Tables.md&2.12&\end{tabular}
ct04-Truth-Tables.md&2.12&\end{center}
ct04-Truth-Tables.md&2.12&
ct04-Truth-Tables.md&2.12&>- What does this mean?
ct04-Truth-Tables.md&2.12&>- p$\leftrightarrow$q is only false if the two propositions have *different* truth values and true otherwise.
ct04-Truth-Tables.md&2.12&>- “If and only if the sun shines will the sky be blue” is true if both parts are true or if both are false.
ct04-Truth-Tables.md&2.12&
ct04-Truth-Tables.md&3.0&# Calculating truth tables
ct04-Truth-Tables.md&3.0&
ct04-Truth-Tables.md&3.1&## More complex expressions
ct04-Truth-Tables.md&3.1&
ct04-Truth-Tables.md&3.1&>- Now we can calculate the truth tables of more complex expressions. By doing this, we can find out under which conditions they are true or false.
ct04-Truth-Tables.md&3.1&>- “I will have pork or beef, together with rice and no tea.”
ct04-Truth-Tables.md&3.1&>- Write this sentence down in symbolic form. 
ct04-Truth-Tables.md&3.1&
ct04-Truth-Tables.md&3.1&. . .
ct04-Truth-Tables.md&3.1&
ct04-Truth-Tables.md&3.2&### “I will have pork or beef, together with rice and no tea.”
ct04-Truth-Tables.md&3.2&
ct04-Truth-Tables.md&3.2&- p: I will have pork
ct04-Truth-Tables.md&3.2&- b: I will have beef
ct04-Truth-Tables.md&3.2&- r: I will have rice
ct04-Truth-Tables.md&3.2&- t: I will have tea
ct04-Truth-Tables.md&3.2&
ct04-Truth-Tables.md&3.2&\begin{center}
ct04-Truth-Tables.md&3.2&\f{(p$\lor$b)$\land$(r$\land$$\lnot$t)}
ct04-Truth-Tables.md&3.2&\end{center}
ct04-Truth-Tables.md&3.2&
ct04-Truth-Tables.md&3.3&## Now calculate the truth table!
ct04-Truth-Tables.md&3.3&
ct04-Truth-Tables.md&3.3&“I will have pork or beef, together with rice and no tea.”
ct04-Truth-Tables.md&3.3&
ct04-Truth-Tables.md&3.3&\f{(p$\lor$b)$\land$(r$\land$$\lnot$t)}
ct04-Truth-Tables.md&3.3&
ct04-Truth-Tables.md&3.3&
ct04-Truth-Tables.md&3.3&![](graphics/06-tt01.png)\ 
ct04-Truth-Tables.md&3.3&
ct04-Truth-Tables.md&3.4&## (p$\lor$b)$\land$(r$\land$$\lnot$t)
ct04-Truth-Tables.md&3.4&
ct04-Truth-Tables.md&3.4&\begin{center}
ct04-Truth-Tables.md&3.4&\begin{small}
ct04-Truth-Tables.md&3.4&\begin{tabular}{|c|c|c|c||c|c|c|c|}
ct04-Truth-Tables.md&3.4&\hline
ct04-Truth-Tables.md&3.4&$ p $ & $ b $ & $ r $ & $ t $ & $ (p \vee b) $ & $  \neg t $ & $ (r \wedge  \neg t) $ & $ ((p \vee b) \wedge (r \wedge  \neg t)) $ \\
ct04-Truth-Tables.md&3.4&\hline
ct04-Truth-Tables.md&3.4&F & F & F & F & F & T & F & F \\
ct04-Truth-Tables.md&3.4&\hline
ct04-Truth-Tables.md&3.4&F & F & F & T & F & F & F & F \\
ct04-Truth-Tables.md&3.4&\hline
ct04-Truth-Tables.md&3.4&F & F & T & F & F & T & T & F \\
ct04-Truth-Tables.md&3.4&\hline
ct04-Truth-Tables.md&3.4&F & F & T & T & F & F & F & F \\
ct04-Truth-Tables.md&3.4&\hline
ct04-Truth-Tables.md&3.4&F & T & F & F & T & T & F & F \\
ct04-Truth-Tables.md&3.4&\hline
ct04-Truth-Tables.md&3.4&F & T & F & T & T & F & F & F \\
ct04-Truth-Tables.md&3.4&\hline
ct04-Truth-Tables.md&3.4&F & T & T & F & T & T & T & T \\
ct04-Truth-Tables.md&3.4&\hline
ct04-Truth-Tables.md&3.4&F & T & T & T & T & F & F & F \\
ct04-Truth-Tables.md&3.4&\hline
ct04-Truth-Tables.md&3.4&T & F & F & F & T & T & F & F \\
ct04-Truth-Tables.md&3.4&\hline
ct04-Truth-Tables.md&3.4&T & F & F & T & T & F & F & F \\
ct04-Truth-Tables.md&3.4&\hline
ct04-Truth-Tables.md&3.4&T & F & T & F & T & T & T & T \\
ct04-Truth-Tables.md&3.4&\hline
ct04-Truth-Tables.md&3.4&T & F & T & T & T & F & F & F \\
ct04-Truth-Tables.md&3.4&\hline
ct04-Truth-Tables.md&3.4&T & T & F & F & T & T & F & F \\
ct04-Truth-Tables.md&3.4&\hline
ct04-Truth-Tables.md&3.4&T & T & F & T & T & F & F & F \\
ct04-Truth-Tables.md&3.4&\hline
ct04-Truth-Tables.md&3.4&T & T & T & F & T & T & T & T \\
ct04-Truth-Tables.md&3.4&\hline
ct04-Truth-Tables.md&3.4&T & T & T & T & T & F & F & F \\
ct04-Truth-Tables.md&3.4&\hline
ct04-Truth-Tables.md&3.4&\end{tabular}
ct04-Truth-Tables.md&3.4&\end{small}
ct04-Truth-Tables.md&3.4&\end{center}
ct04-Truth-Tables.md&3.4&
ct04-Truth-Tables.md&3.5&## Truth tables (\cn{真值表})
ct04-Truth-Tables.md&3.5&Let's see how to build a truth table for a sentence:
ct04-Truth-Tables.md&3.5&
ct04-Truth-Tables.md&3.5&>1. Write down the propositions as a complex expression in symbolic form
ct04-Truth-Tables.md&3.5&>2. Write all variables down, each into one table column
ct04-Truth-Tables.md&3.5&>3. Do the same for the sub-expressions
ct04-Truth-Tables.md&3.5&>4. Start with any variable and fill in T, F, T, F, ... and so on (starting with T!)
ct04-Truth-Tables.md&3.5&>5. For the second variable, go in pairs of T, T, F, F, ..., then in groups of 4, 8, 16, 32 and so on.
ct04-Truth-Tables.md&3.5&>6. Calculate the truth values of the subexpressions and the expression.
ct04-Truth-Tables.md&3.5&
ct04-Truth-Tables.md&3.6&## Calculating a truth table
ct04-Truth-Tables.md&3.6&
ct04-Truth-Tables.md&3.6&![](graphics/06-tt03.png)\ 
ct04-Truth-Tables.md&3.6&
ct04-Truth-Tables.md&3.6&
ct04-Truth-Tables.md&3.7&## Now try it yourself!
ct04-Truth-Tables.md&3.7&
ct04-Truth-Tables.md&3.7&Convert the following sentence into symbolic form:
ct04-Truth-Tables.md&3.7&
ct04-Truth-Tables.md&3.7&“I will paint my room white, but I want those blue sheets for the bed, or red curtains. Otherwise I will just paint it yellow, but without red curtains.”
ct04-Truth-Tables.md&3.7&
ct04-Truth-Tables.md&3.7&
ct04-Truth-Tables.md&3.8&## Symbolic representation
ct04-Truth-Tables.md&3.8&
ct04-Truth-Tables.md&3.8&“I will paint my room white, but I want those blue sheets for the bed, or red curtains. Otherwise I will just paint it yellow, but without red curtains.”
ct04-Truth-Tables.md&3.8&
ct04-Truth-Tables.md&3.8&Variables used: (use letters that make sense!)
ct04-Truth-Tables.md&3.8&
ct04-Truth-Tables.md&3.8&- w: “I will paint my room white”
ct04-Truth-Tables.md&3.8&- b: “I can get the blue sheets”
ct04-Truth-Tables.md&3.8&- r: “I can get the red curtains”
ct04-Truth-Tables.md&3.8&- y: “I will paint it yellow”
ct04-Truth-Tables.md&3.8&
ct04-Truth-Tables.md&3.8&\center{\f{\Large{(w$\land$(b$\lor$r))$\lor$(y$\land$$\lnot$r)}}}
ct04-Truth-Tables.md&3.8&
ct04-Truth-Tables.md&3.9&## Converting the little words
ct04-Truth-Tables.md&3.9&
ct04-Truth-Tables.md&3.9&“I will paint my room white, but I want those blue sheets for the bed, or red curtains. Otherwise I will just paint it yellow, but without red curtains.”
ct04-Truth-Tables.md&3.9&
ct04-Truth-Tables.md&3.9&\center{\f{\Large{(w$\land$(b$\lor$r))$\lor$(y$\land$$\lnot$r)}}}
ct04-Truth-Tables.md&3.9&
ct04-Truth-Tables.md&3.9&>- “but” = and
ct04-Truth-Tables.md&3.9&>- “otherwise” = or
ct04-Truth-Tables.md&3.9&>- “without” = not
ct04-Truth-Tables.md&3.9&>- “just” = (ignored)
ct04-Truth-Tables.md&3.9&
ct04-Truth-Tables.md&3.10&## “Otherwise”
ct04-Truth-Tables.md&3.10&
ct04-Truth-Tables.md&3.10&>- In the last example we translated “otherwise” as “or.”
ct04-Truth-Tables.md&3.10&>- A more precise translation would be: “And if not ..., then ...”
ct04-Truth-Tables.md&3.10&>- So: “If they have coke, I will stay, otherwise I will leave (=not stay)”
ct04-Truth-Tables.md&3.10&>- means:
ct04-Truth-Tables.md&3.10&>- “If they have coke, I will stay. If they don’t have coke, I will not stay,” and this translates to: “I will stay *if and only if* they have coke”:
ct04-Truth-Tables.md&3.10&>- (C $\to$ S) $\land$ ($\lnot$C $\to$ $\lnot$S)
ct04-Truth-Tables.md&3.10&>- which is the same as:
ct04-Truth-Tables.md&3.10&>- C$\leftrightarrow$S
ct04-Truth-Tables.md&3.10&
ct04-Truth-Tables.md&3.11&## “Otherwise”
ct04-Truth-Tables.md&3.11&
ct04-Truth-Tables.md&3.11&More generally, “if A then B, otherwise C” translates as:
ct04-Truth-Tables.md&3.11&
ct04-Truth-Tables.md&3.11&\f{(A$\to$B)$\land$($\lnot$A$\to$C)}
ct04-Truth-Tables.md&3.11&
ct04-Truth-Tables.md&3.11&Only in the special case where C is equivalent to $\lnot$B (as in the previous example), “otherwise” becomes a biconditional. So
ct04-Truth-Tables.md&3.11&
ct04-Truth-Tables.md&3.11&“If A then B, otherwise not B” becomes:
ct04-Truth-Tables.md&3.11&
ct04-Truth-Tables.md&3.11&\f{(A$\to$B)$\land$($\lnot$A$\to$$\lnot$B)}
ct04-Truth-Tables.md&3.11&
ct04-Truth-Tables.md&3.11&which is the same as:
ct04-Truth-Tables.md&3.11&
ct04-Truth-Tables.md&3.11&\f{A$\leftrightarrow$B}
ct04-Truth-Tables.md&3.11&
ct04-Truth-Tables.md&3.12&## Now try it yourself!
ct04-Truth-Tables.md&3.12&
ct04-Truth-Tables.md&3.12&Calculate the truth table for the following sentence:
ct04-Truth-Tables.md&3.12&
ct04-Truth-Tables.md&3.12&“I will paint my room white, but I want those blue sheets for the bed, or red curtains. Otherwise I will just paint it yellow, but without red curtains.”
ct04-Truth-Tables.md&3.12&
ct04-Truth-Tables.md&3.12&*First write it down as a symbolic expression!*
ct04-Truth-Tables.md&3.12&
ct04-Truth-Tables.md&3.13&## Symbolic representation
ct04-Truth-Tables.md&3.13&
ct04-Truth-Tables.md&3.13&- w: “I will paint my room white”
ct04-Truth-Tables.md&3.13&- b: “I can get the blue sheets”
ct04-Truth-Tables.md&3.13&- r: “I can get the red curtains”
ct04-Truth-Tables.md&3.13&- y: “I will paint it yellow”
ct04-Truth-Tables.md&3.13&
ct04-Truth-Tables.md&3.13&\center{\f{\Large (w$\land$(b$\lor$r))$\lor$(y$\land$$\lnot$r)}}
ct04-Truth-Tables.md&3.13&
ct04-Truth-Tables.md&3.13&*Now calculate the truth table!*
ct04-Truth-Tables.md&3.13&
ct04-Truth-Tables.md&3.14&## Truth table for (w$\land$(b$\lor$r))$\lor$(y$\land$$\lnot$r)
ct04-Truth-Tables.md&3.14&
ct04-Truth-Tables.md&3.14&![](graphics/06-tt04.png)\ 
ct04-Truth-Tables.md&3.14&
ct04-Truth-Tables.md&3.15&## Why do we do all this?
ct04-Truth-Tables.md&3.15&
ct04-Truth-Tables.md&3.15&“I will paint my room white, but I want those blue sheets for the bed, or red curtains. Otherwise I will just paint it yellow, but without red curtains.”
ct04-Truth-Tables.md&3.15&
ct04-Truth-Tables.md&3.15&![](graphics/06-tt04.png)\ 
ct04-Truth-Tables.md&3.15&
ct04-Truth-Tables.md&3.15&>- By analyzing the truth table of complex expressions, we can say exactly under which conditions they will be true of false.
ct04-Truth-Tables.md&3.15&>- In the above example, we can see exactly which combination of curtains, bed sheets and wall colour would satisfy our customer.
ct04-Truth-Tables.md&3.15&
ct04-Truth-Tables.md&4.0&# More exercises
ct04-Truth-Tables.md&4.0&
ct04-Truth-Tables.md&4.1&## Translate into English:
ct04-Truth-Tables.md&4.1&
ct04-Truth-Tables.md&4.1&Find an English sentence with the following logical structure:
ct04-Truth-Tables.md&4.1&
ct04-Truth-Tables.md&4.1&\f{A$\leftrightarrow$((B$\land$C)$\lor$(D$\land$C))}
ct04-Truth-Tables.md&4.1&
ct04-Truth-Tables.md&4.1&. . . 
ct04-Truth-Tables.md&4.1&
ct04-Truth-Tables.md&4.1&"If and only if you give me a good grade, I will attend your class and like you, or I will be your Facebook friend and like you."
ct04-Truth-Tables.md&4.1&
ct04-Truth-Tables.md&4.2&## Translate into English:
ct04-Truth-Tables.md&4.2&
ct04-Truth-Tables.md&4.2&(A$\land\lnot$B)$\to$C$\land$(D$\lor$E)
ct04-Truth-Tables.md&4.2&
ct04-Truth-Tables.md&4.2&. . . 
ct04-Truth-Tables.md&4.2&
ct04-Truth-Tables.md&4.2&Solution: “If I get my results without A's, then my mother will kill me with a gun or a knife.”
ct04-Truth-Tables.md&4.2&
ct04-Truth-Tables.md&4.3&## Which is the correct expression?
ct04-Truth-Tables.md&4.3&
ct04-Truth-Tables.md&4.3&“I want a pizza, but it should have no meat or pineapple, and make sure it has some tuna and a lot of cheese or pepper!”
ct04-Truth-Tables.md&4.3&
ct04-Truth-Tables.md&4.3&Which is the correct formal representation of this sentence?
ct04-Truth-Tables.md&4.3&
ct04-Truth-Tables.md&4.3&P: pizza, M: meat, I: pineapple, T: tuna, C: a lot of cheese, E: a lot of pepper.
ct04-Truth-Tables.md&4.3&
ct04-Truth-Tables.md&4.3&1. P$\land\lnot$(M$\land\lnot$I)$\land$T$\land$(C$\lor$E)
ct04-Truth-Tables.md&4.3&2. P$\land$($\lnot$M$\lor$I)$\land$(T$\land$C)$\lor$E
ct04-Truth-Tables.md&4.3&3. P$\land$($\lnot$M$\lor\lnot$I)$\land$T$\land$(C$\land$E)
ct04-Truth-Tables.md&4.3&4. P$\land\lnot$(M$\lor$I)$\land$T$\land$(C$\lor$E)
ct04-Truth-Tables.md&4.3&5. P$\land\lnot$(M$\land$I)$\land$T$\land$(C$\lor$E)
ct04-Truth-Tables.md&4.3&
ct04-Truth-Tables.md&4.3&. . .
ct04-Truth-Tables.md&4.3&
ct04-Truth-Tables.md&4.3&Solution:
ct04-Truth-Tables.md&4.3&
ct04-Truth-Tables.md&4.3&4. P$\land\lnot$(M$\lor$I)$\land$T$\land$(C$\lor$E)
ct04-Truth-Tables.md&4.3&
ct04-Truth-Tables.md&4.4&## Translate into English:
ct04-Truth-Tables.md&4.4&
ct04-Truth-Tables.md&4.4&(P $\land$ $\lnot$Q) $\leftrightarrow$ R $\land$ S $\land$ T
ct04-Truth-Tables.md&4.4&
ct04-Truth-Tables.md&4.4&. . .
ct04-Truth-Tables.md&4.4&
ct04-Truth-Tables.md&4.4&“If and only if you buy me the expensive ring and not the cheap one, I will like you and marry you and we will be happy.”
ct04-Truth-Tables.md&4.4&
ct04-Truth-Tables.md&4.5&## Which is the correct expression?
ct04-Truth-Tables.md&4.5&
ct04-Truth-Tables.md&4.5&“If and only if I marry Paul but not Mike or John, I will have a house with cows and elephants.”
ct04-Truth-Tables.md&4.5&
ct04-Truth-Tables.md&4.5&Which is the correct formal representation of this sentence?
ct04-Truth-Tables.md&4.5&
ct04-Truth-Tables.md&4.5&M: Paul, H: Mike, E: John, P: house, C: cows, J: elephants.
ct04-Truth-Tables.md&4.5&
ct04-Truth-Tables.md&4.5&1. M$\land\lnot$(H$\lor$E)$\leftrightarrow$P$\land$(C$\lor$J)
ct04-Truth-Tables.md&4.5&2. M$\lor\lnot$(H$\lor$E)$\leftrightarrow$(P$\land$C)$\land$J
ct04-Truth-Tables.md&4.5&3. M$\land$($\lnot$H$\lor\lnot$E)$\leftrightarrow$P$\land$(C$\land$J)
ct04-Truth-Tables.md&4.5&4. M$\land\lnot$(H$\lor$E)$\to$P$\land$(C$\land$J)
ct04-Truth-Tables.md&4.5&5. M$\land\lnot$(H$\lor$E)$\leftrightarrow$P$\land$(C$\land$J)
ct04-Truth-Tables.md&4.5&
ct04-Truth-Tables.md&4.5&. . .
ct04-Truth-Tables.md&4.5&
ct04-Truth-Tables.md&4.5&Solution:
ct04-Truth-Tables.md&4.5&
ct04-Truth-Tables.md&4.5&5. M$\land\lnot$(H$\lor$E)$\leftrightarrow$P$\land$(C$\land$J)
ct04-Truth-Tables.md&4.5&
ct04-Truth-Tables.md&4.6&## Which is the correct expression?
ct04-Truth-Tables.md&4.6&
ct04-Truth-Tables.md&4.6&“If I read a book and don’t go to the cinema or to the restaurant, I will be satisfied without having spent too much money.” -- Which is the correct formal representation of this sentence?
ct04-Truth-Tables.md&4.6&
ct04-Truth-Tables.md&4.6&q: I read a book, p: I go to the restaurant, d: I go to the cinema, b: I will be satisfied, m: I will have spent too much money.
ct04-Truth-Tables.md&4.6&
ct04-Truth-Tables.md&4.6&1. q$\land\lnot$p$\lor$d$\to$b$\land\lnot$m
ct04-Truth-Tables.md&4.6&2. q$\land\lnot$(p$\lor$d)$\leftrightarrow$b$\land\lnot$m
ct04-Truth-Tables.md&4.6&3. q$\land\lnot$p$\lor\lnot$d$\to$b$\land\lnot$m
ct04-Truth-Tables.md&4.6&4. q$\land\lnot$p$\land\lnot$d$\to$b$\land\lnot$m
ct04-Truth-Tables.md&4.6&5. q$\land\lnot$(p$\land$d)$\to$b$\land\lnot$m
ct04-Truth-Tables.md&4.6&
ct04-Truth-Tables.md&4.6&. . .
ct04-Truth-Tables.md&4.6&
ct04-Truth-Tables.md&4.6&Solution:
ct04-Truth-Tables.md&4.6&
ct04-Truth-Tables.md&4.6&4. q$\land\lnot$p$\land\lnot$d$\to$b$\land\lnot$m
ct04-Truth-Tables.md&4.6&
ct04-Truth-Tables.md&4.7&## What did we learn today?
ct04-Truth-Tables.md&4.7&
ct04-Truth-Tables.md&4.7&Logical operations can be described by truth tables
ct04-Truth-Tables.md&4.7&
ct04-Truth-Tables.md&4.8&## Any questions?
ct04-Truth-Tables.md&4.8&
ct04-Truth-Tables.md&4.8&![](graphics/questions.jpg)
ct04-Truth-Tables.md&4.8&
ct04-Truth-Tables.md&4.9&## Thank you for your attention!
ct04-Truth-Tables.md&4.9&
ct04-Truth-Tables.md&4.9&Email: <matthias@ln.edu.hk>
ct04-Truth-Tables.md&4.9&
ct04-Truth-Tables.md&4.9&If you have questions, please come to my office hours (see course outline).
ct05-Truth-tables-2.md&0.1&% Critical Thinking
ct05-Truth-tables-2.md&0.1&% 5. Truth tables, tautologies, contradictions
ct05-Truth-tables-2.md&0.1&% A. Matthias
ct05-Truth-tables-2.md&0.1&
ct05-Truth-tables-2.md&1.0&# Where we are
ct05-Truth-tables-2.md&1.0&
ct05-Truth-tables-2.md&1.1&## What did we learn last time?
ct05-Truth-tables-2.md&1.1&
ct05-Truth-tables-2.md&1.1&- Logical operations can be described by truth tables
ct05-Truth-tables-2.md&1.1&
ct05-Truth-tables-2.md&1.2&## What are we going to learn today?
ct05-Truth-tables-2.md&1.2&
ct05-Truth-tables-2.md&1.2&- We will practice using truth tables on everyday language
ct05-Truth-tables-2.md&1.2&- We will talk about tautologies, contradictions, contingent, consistent and inconsistent statements
ct05-Truth-tables-2.md&1.2&
ct05-Truth-tables-2.md&2.0&# Translations
ct05-Truth-tables-2.md&2.0&
ct05-Truth-tables-2.md&2.1&## Translate into symbolic form:
ct05-Truth-tables-2.md&2.1&
ct05-Truth-tables-2.md&2.1&"If and only if this tree gets enough water and sunshine and it’s not too cold, it will grow well and give fruit."
ct05-Truth-tables-2.md&2.1&
ct05-Truth-tables-2.md&2.1&. . .
ct05-Truth-tables-2.md&2.1&
ct05-Truth-tables-2.md&2.1&w: this tree gets enough water  
ct05-Truth-tables-2.md&2.1&s: this tree gets enough sunshine  
ct05-Truth-tables-2.md&2.1&c: it’s too cold  
ct05-Truth-tables-2.md&2.1&g: this tree will grow well  
ct05-Truth-tables-2.md&2.1&f: this tree will give fruit  
ct05-Truth-tables-2.md&2.1&
ct05-Truth-tables-2.md&2.1&. . .
ct05-Truth-tables-2.md&2.1&
ct05-Truth-tables-2.md&2.1&\f{(w$\land$s$\land$$\lnot$c)$\leftrightarrow$(g$\land$f)}
ct05-Truth-tables-2.md&2.1&
ct05-Truth-tables-2.md&2.2&## Translate into a formal expression!
ct05-Truth-tables-2.md&2.2&The school has collapsed, but it is not true that five students died.
ct05-Truth-tables-2.md&2.2&
ct05-Truth-tables-2.md&2.2&. . .
ct05-Truth-tables-2.md&2.2&
ct05-Truth-tables-2.md&2.2&\f{S$\land$$\lnot$D}
ct05-Truth-tables-2.md&2.2&
ct05-Truth-tables-2.md&2.3&## Translate into a formal expression!
ct05-Truth-tables-2.md&2.3&
ct05-Truth-tables-2.md&2.3&If you want to go, then I will go with you if it is sunny.
ct05-Truth-tables-2.md&2.3&
ct05-Truth-tables-2.md&2.3&. . .
ct05-Truth-tables-2.md&2.3&
ct05-Truth-tables-2.md&2.3&Y: You want to go.
ct05-Truth-tables-2.md&2.3&I: I will go with you
ct05-Truth-tables-2.md&2.3&S: It is sunny.
ct05-Truth-tables-2.md&2.3&
ct05-Truth-tables-2.md&2.3&. . . 
ct05-Truth-tables-2.md&2.3&
ct05-Truth-tables-2.md&2.3&\f{Y$\to$(S$\to$I)}
ct05-Truth-tables-2.md&2.3&
ct05-Truth-tables-2.md&2.3&or
ct05-Truth-tables-2.md&2.3&
ct05-Truth-tables-2.md&2.3&\f{S$\land$Y$\to$I}
ct05-Truth-tables-2.md&2.3&
ct05-Truth-tables-2.md&2.4&## Translate into a formal expression!
ct05-Truth-tables-2.md&2.4&
ct05-Truth-tables-2.md&2.4&Whether Peter is coming to the party or not, Mary is not going to come.
ct05-Truth-tables-2.md&2.4&
ct05-Truth-tables-2.md&2.4&. . . 
ct05-Truth-tables-2.md&2.4&
ct05-Truth-tables-2.md&2.4&\f{(P$\lor$$\lnot$P)$\to$$\lnot$M}
ct05-Truth-tables-2.md&2.4&
ct05-Truth-tables-2.md&2.4&or
ct05-Truth-tables-2.md&2.4&
ct05-Truth-tables-2.md&2.4&\f{(P$\to$$\lnot$M)$\land$($\lnot$P$\to$$\lnot$M)}
ct05-Truth-tables-2.md&2.4&
ct05-Truth-tables-2.md&2.4&or simply
ct05-Truth-tables-2.md&2.4&
ct05-Truth-tables-2.md&2.4&\f{$\lnot$M}
ct05-Truth-tables-2.md&2.4&
ct05-Truth-tables-2.md&2.4&(but this is not a good translation!)
ct05-Truth-tables-2.md&2.4&
ct05-Truth-tables-2.md&2.5&## Make a truth table
ct05-Truth-tables-2.md&2.5&
ct05-Truth-tables-2.md&2.5&Whether Peter is coming to the party or not, Mary is not going to come.
ct05-Truth-tables-2.md&2.5&
ct05-Truth-tables-2.md&2.5&\f{(P$\to$$\lnot$M)$\land$($\lnot$P$\to$$\lnot$M)}
ct05-Truth-tables-2.md&2.5&
ct05-Truth-tables-2.md&2.5&or simply:
ct05-Truth-tables-2.md&2.5&
ct05-Truth-tables-2.md&2.5&\f{$\lnot$M}
ct05-Truth-tables-2.md&2.5&
ct05-Truth-tables-2.md&2.5&Check with a truth table that these two solutions have the same truth values!
ct05-Truth-tables-2.md&2.5&
ct05-Truth-tables-2.md&2.5&. . .
ct05-Truth-tables-2.md&2.5&
ct05-Truth-tables-2.md&2.5&*Make sure to write the two expressions into the same truth table!*
ct05-Truth-tables-2.md&2.5&
ct05-Truth-tables-2.md&2.6&## Translate into a formal expression!
ct05-Truth-tables-2.md&2.6&
ct05-Truth-tables-2.md&2.6&Unless you try to improve yourself, and unless you improve your attitude, you are not going to succeed.
ct05-Truth-tables-2.md&2.6&
ct05-Truth-tables-2.md&2.6&. . . 
ct05-Truth-tables-2.md&2.6&
ct05-Truth-tables-2.md&2.6&Y: You try to improve yourself.
ct05-Truth-tables-2.md&2.6&A: You improve your attitude.
ct05-Truth-tables-2.md&2.6&S: You are going to succeed.
ct05-Truth-tables-2.md&2.6&
ct05-Truth-tables-2.md&2.6&. . . 
ct05-Truth-tables-2.md&2.6&
ct05-Truth-tables-2.md&2.6&\f{$\lnot$(Y$\land$A)$\to$$\lnot$S} or \f{(Y$\land$A)$\lor$$\lnot$S} or \f{S$\to$(Y$\land$A)}
ct05-Truth-tables-2.md&2.6&
ct05-Truth-tables-2.md&2.6&. . . 
ct05-Truth-tables-2.md&2.6&
ct05-Truth-tables-2.md&2.6&Be careful:
ct05-Truth-tables-2.md&2.6&
ct05-Truth-tables-2.md&2.6&\f{$\lnot$(Y$\land$A)$\to$$\lnot$S} is equivalent to: \f{S$\to$(Y$\land$A)}
ct05-Truth-tables-2.md&2.6&
ct05-Truth-tables-2.md&2.6&but *not* equivalent to:
ct05-Truth-tables-2.md&2.6&
ct05-Truth-tables-2.md&2.6&\f{(Y$\land$A)$\to$S}
ct05-Truth-tables-2.md&2.6&
ct05-Truth-tables-2.md&2.6&(S might need additional reasons in order to be true!)
ct05-Truth-tables-2.md&2.6&
ct05-Truth-tables-2.md&2.7&## “Unless ... not”
ct05-Truth-tables-2.md&2.7&
ct05-Truth-tables-2.md&2.7&For example:
ct05-Truth-tables-2.md&2.7&
ct05-Truth-tables-2.md&2.7&In order to stay alive, you need to breathe, to eat and to drink. So it is correct to say:
ct05-Truth-tables-2.md&2.7&
ct05-Truth-tables-2.md&2.7&“Unless you breathe and eat you are not going to stay alive.”
ct05-Truth-tables-2.md&2.7&
ct05-Truth-tables-2.md&2.7&But this can *not* be converted into:
ct05-Truth-tables-2.md&2.7&
ct05-Truth-tables-2.md&2.7&“If you breathe and eat you will stay alive” (because I also need to drink!)
ct05-Truth-tables-2.md&2.7&
ct05-Truth-tables-2.md&3.0&# Truth tables
ct05-Truth-tables-2.md&3.0&
ct05-Truth-tables-2.md&3.1&## Truth tables for the basic logical operators (summary)
ct05-Truth-tables-2.md&3.1&
ct05-Truth-tables-2.md&3.1&![](graphics/07-operators.png)\ 
ct05-Truth-tables-2.md&3.1&
ct05-Truth-tables-2.md&3.2&## How to build a truth table
ct05-Truth-tables-2.md&3.2&
ct05-Truth-tables-2.md&3.2&Example:
ct05-Truth-tables-2.md&3.2&
ct05-Truth-tables-2.md&3.2&If the weather is fine and I don't have to work, I will go to the park or to the beach.
ct05-Truth-tables-2.md&3.2&
ct05-Truth-tables-2.md&3.2&. . . 
ct05-Truth-tables-2.md&3.2&
ct05-Truth-tables-2.md&3.2&>- 1st step: translate into symbolic form: \f{(F$\land\lnot$W)$\to$(P$\lor$B)}
ct05-Truth-tables-2.md&3.2&
ct05-Truth-tables-2.md&3.2&
ct05-Truth-tables-2.md&3.3&## How to build a truth table
ct05-Truth-tables-2.md&3.3&
ct05-Truth-tables-2.md&3.3&![](graphics/07-tt1.png)\ 
ct05-Truth-tables-2.md&3.3&
ct05-Truth-tables-2.md&3.4&## How to build a truth table
ct05-Truth-tables-2.md&3.4&
ct05-Truth-tables-2.md&3.4&![](graphics/07-tt2.png)\ 
ct05-Truth-tables-2.md&3.4&
ct05-Truth-tables-2.md&3.5&## How to build a truth table
ct05-Truth-tables-2.md&3.5&
ct05-Truth-tables-2.md&3.5&![](graphics/07-tt3.png)\ 
ct05-Truth-tables-2.md&3.5&
ct05-Truth-tables-2.md&3.6&## How to build a truth table
ct05-Truth-tables-2.md&3.6&
ct05-Truth-tables-2.md&3.6&![](graphics/07-tt4.png)\ 
ct05-Truth-tables-2.md&3.6&
ct05-Truth-tables-2.md&3.7&## How many lines do I need?
ct05-Truth-tables-2.md&3.7&
ct05-Truth-tables-2.md&3.7&>- Non-maths answer: Insert “T” and “F” until all your variables go from true, true, true ... to false, false, false ...
ct05-Truth-tables-2.md&3.7&>- Maths answer: $2^n$ where $n$ is the number of variables (propositions) in your expression.
ct05-Truth-tables-2.md&3.7&>- That is:
ct05-Truth-tables-2.md&3.7&>      - 2 variables = 4 lines
ct05-Truth-tables-2.md&3.7&>      - 3 variables = 8 lines
ct05-Truth-tables-2.md&3.7&>      - 4 variables = 16 lines
ct05-Truth-tables-2.md&3.7&
ct05-Truth-tables-2.md&3.8&## How to build a truth table
ct05-Truth-tables-2.md&3.8&
ct05-Truth-tables-2.md&3.8&![](graphics/07-tt5.png)\ 
ct05-Truth-tables-2.md&3.8&
ct05-Truth-tables-2.md&3.9&## How to build a truth table
ct05-Truth-tables-2.md&3.9&
ct05-Truth-tables-2.md&3.9&![](graphics/07-tt6.png)\ 
ct05-Truth-tables-2.md&3.9&
ct05-Truth-tables-2.md&3.10&## How to build a truth table
ct05-Truth-tables-2.md&3.10&
ct05-Truth-tables-2.md&3.10&![](graphics/07-tt7.png)\ 
ct05-Truth-tables-2.md&3.10&
ct05-Truth-tables-2.md&3.11&## How to build a truth table
ct05-Truth-tables-2.md&3.11&
ct05-Truth-tables-2.md&3.11&![](graphics/07-tt8.png)\ 
ct05-Truth-tables-2.md&3.11&
ct05-Truth-tables-2.md&3.12&## How to build a truth table
ct05-Truth-tables-2.md&3.12&
ct05-Truth-tables-2.md&3.12&![](graphics/07-tt9.png)\ 
ct05-Truth-tables-2.md&3.12&
ct05-Truth-tables-2.md&3.13&## How to build a truth table
ct05-Truth-tables-2.md&3.13&
ct05-Truth-tables-2.md&3.13&![](graphics/07-tt10.png)\ 
ct05-Truth-tables-2.md&3.13&
ct05-Truth-tables-2.md&3.14&## How to build a truth table
ct05-Truth-tables-2.md&3.14&
ct05-Truth-tables-2.md&3.14&![](graphics/07-tt11.png)\ 
ct05-Truth-tables-2.md&3.14&
ct05-Truth-tables-2.md&3.15&## How to build a truth table
ct05-Truth-tables-2.md&3.15&
ct05-Truth-tables-2.md&3.15&![](graphics/07-tt12.png)\ 
ct05-Truth-tables-2.md&3.15&
ct05-Truth-tables-2.md&3.16&## How to build a truth table
ct05-Truth-tables-2.md&3.16&
ct05-Truth-tables-2.md&3.16&![](graphics/07-tt13.png)\ 
ct05-Truth-tables-2.md&3.16&
ct05-Truth-tables-2.md&3.17&## How to build a truth table
ct05-Truth-tables-2.md&3.17&
ct05-Truth-tables-2.md&3.17&![](graphics/07-tt14.png)\ 
ct05-Truth-tables-2.md&3.17&
ct05-Truth-tables-2.md&3.18&## How to build a truth table
ct05-Truth-tables-2.md&3.18&
ct05-Truth-tables-2.md&3.18&![](graphics/07-tt15.png)\ 
ct05-Truth-tables-2.md&3.18&
ct05-Truth-tables-2.md&3.19&## How to build a truth table
ct05-Truth-tables-2.md&3.19&
ct05-Truth-tables-2.md&3.19&![](graphics/07-tt16.png)\ 
ct05-Truth-tables-2.md&3.19&
ct05-Truth-tables-2.md&3.20&## How to build a truth table
ct05-Truth-tables-2.md&3.20&
ct05-Truth-tables-2.md&3.20&![](graphics/07-tt17.png)\ 
ct05-Truth-tables-2.md&3.20&
ct05-Truth-tables-2.md&3.21&## How to build a truth table
ct05-Truth-tables-2.md&3.21&
ct05-Truth-tables-2.md&3.21&![](graphics/07-tt18.png)\ 
ct05-Truth-tables-2.md&3.21&
ct05-Truth-tables-2.md&3.22&## How to build a truth table
ct05-Truth-tables-2.md&3.22&
ct05-Truth-tables-2.md&3.22&![](graphics/07-tt19.png)\ 
ct05-Truth-tables-2.md&3.22&
ct05-Truth-tables-2.md&3.23&## How to build a truth table
ct05-Truth-tables-2.md&3.23&
ct05-Truth-tables-2.md&3.23&![](graphics/07-tt20.png)\ 
ct05-Truth-tables-2.md&3.23&
ct05-Truth-tables-2.md&3.24&## How to build a truth table
ct05-Truth-tables-2.md&3.24&
ct05-Truth-tables-2.md&3.24&![](graphics/07-tt21.png)\ 
ct05-Truth-tables-2.md&3.24&
ct05-Truth-tables-2.md&3.25&## How to build a truth table
ct05-Truth-tables-2.md&3.25&
ct05-Truth-tables-2.md&3.25&![“If the weather is fine and I don't have to work, I'll go to the park or to the beach”: (F$\land\lnot$W)$\to$(P$\lor$B)](graphics/07-tt22.png)
ct05-Truth-tables-2.md&3.25&
ct05-Truth-tables-2.md&3.26&## Truth table practice
ct05-Truth-tables-2.md&3.26&
ct05-Truth-tables-2.md&3.26&Now calculate the truth table for:
ct05-Truth-tables-2.md&3.26&
ct05-Truth-tables-2.md&3.26&"If it is hot, I will go to the beach and swim or surf."
ct05-Truth-tables-2.md&3.26&
ct05-Truth-tables-2.md&3.26&To help you, here are again the truth tables for the basic logical operators:
ct05-Truth-tables-2.md&3.26&
ct05-Truth-tables-2.md&3.26&![](graphics/07-operators.png)\ 
ct05-Truth-tables-2.md&3.26&
ct05-Truth-tables-2.md&3.27&## Solution
ct05-Truth-tables-2.md&3.27&
ct05-Truth-tables-2.md&3.27&"If it is hot, I will go to the beach and swim or surf."
ct05-Truth-tables-2.md&3.27&
ct05-Truth-tables-2.md&3.27&\f{H$\to$B$\land$(S$\lor$F)}
ct05-Truth-tables-2.md&3.27&
ct05-Truth-tables-2.md&3.27&11 true, 5 false.
ct05-Truth-tables-2.md&3.27&
ct05-Truth-tables-2.md&3.28&## Summary: How to calculate a truth table
ct05-Truth-tables-2.md&3.28&
ct05-Truth-tables-2.md&3.28&![](graphics/07-tt-how.png)\ 
ct05-Truth-tables-2.md&3.28&
ct05-Truth-tables-2.md&4.0&# Tautologies and contradictions
ct05-Truth-tables-2.md&4.0&
ct05-Truth-tables-2.md&4.1&## Logical tautologies (\cn{重言式})
ct05-Truth-tables-2.md&4.1&
ct05-Truth-tables-2.md&4.1&>- All daughters have a mother
ct05-Truth-tables-2.md&4.1&>- All ships are ships
ct05-Truth-tables-2.md&4.1&>- Bananas are either sweet or not sweet
ct05-Truth-tables-2.md&4.1&>- These sentences are not only true.
ct05-Truth-tables-2.md&4.1&>- They are *necessarily* true. They could never be false.
ct05-Truth-tables-2.md&4.1&>- They are called *logical tautologies.*
ct05-Truth-tables-2.md&4.1&
ct05-Truth-tables-2.md&4.2&## Logical tautologies (\cn{重言式})
ct05-Truth-tables-2.md&4.2&
ct05-Truth-tables-2.md&4.2&A logical tautology is a sentence that cannot be anything but true for logical reasons.
ct05-Truth-tables-2.md&4.2&
ct05-Truth-tables-2.md&4.2&e.g.: “Bananas are sweet or not sweet”
ct05-Truth-tables-2.md&4.2&
ct05-Truth-tables-2.md&4.2&S: Bananas are sweet
ct05-Truth-tables-2.md&4.2&
ct05-Truth-tables-2.md&4.2&\f{S$\lor$$\lnot$S}
ct05-Truth-tables-2.md&4.2&
ct05-Truth-tables-2.md&4.2&The truth table of this shows that it is always true!
ct05-Truth-tables-2.md&4.2&
ct05-Truth-tables-2.md&4.3&## Contradictions (\cn{矛盾})
ct05-Truth-tables-2.md&4.3&
ct05-Truth-tables-2.md&4.3&The opposite of the tautology is the *contradiction.*
ct05-Truth-tables-2.md&4.3&
ct05-Truth-tables-2.md&4.3&A logical contradiction is a sentence which, for logical reasons, is always false.
ct05-Truth-tables-2.md&4.3&
ct05-Truth-tables-2.md&4.4&## Contradictions (\cn{矛盾})
ct05-Truth-tables-2.md&4.4&
ct05-Truth-tables-2.md&4.4&e.g.: “My car is big and small (=not big)”
ct05-Truth-tables-2.md&4.4&
ct05-Truth-tables-2.md&4.4&\f{B$\land$$\lnot$B}
ct05-Truth-tables-2.md&4.4&
ct05-Truth-tables-2.md&4.5&## Contingent statements (\cn{適然句})
ct05-Truth-tables-2.md&4.5&
ct05-Truth-tables-2.md&4.5&Every statement which is neither a tautology nor a contradiction is said to be a *contingent statement.*
ct05-Truth-tables-2.md&4.5&
ct05-Truth-tables-2.md&4.5&This means that contingent statements can be either true or false, depending on the truth values of the propositions which compose them.
ct05-Truth-tables-2.md&4.5&
ct05-Truth-tables-2.md&4.6&## Tautologies/contradictions
ct05-Truth-tables-2.md&4.6&
ct05-Truth-tables-2.md&4.6&We can use truth tables to show that some statements are tautologies (always true), contradictions (always false), or contingent statements (sometimes true, sometimes false).
ct05-Truth-tables-2.md&4.6&
ct05-Truth-tables-2.md&4.7&## Tautologies/contradictions
ct05-Truth-tables-2.md&4.7&
ct05-Truth-tables-2.md&4.7&*Show whether the following statement is a tautology, a contradiction, or a contingent statement:*
ct05-Truth-tables-2.md&4.7&
ct05-Truth-tables-2.md&4.7&“If John marries me I will be happy, and if he doesn’t marry me then I will not be happy.”
ct05-Truth-tables-2.md&4.7&
ct05-Truth-tables-2.md&4.7&. . . 
ct05-Truth-tables-2.md&4.7&
ct05-Truth-tables-2.md&4.7&\f{(m$\to$h)$\land$($\lnot$m$\to\lnot$h)}
ct05-Truth-tables-2.md&4.7&
ct05-Truth-tables-2.md&4.7&. . .
ct05-Truth-tables-2.md&4.7&
ct05-Truth-tables-2.md&4.7&Contingent statement! (Some lines in the truth table are true, some are false).
ct05-Truth-tables-2.md&4.7&
ct05-Truth-tables-2.md&4.7&This is actually equivalent to:
ct05-Truth-tables-2.md&4.7&
ct05-Truth-tables-2.md&4.7&\f{m$\leftrightarrow$h}
ct05-Truth-tables-2.md&4.7&
ct05-Truth-tables-2.md&5.0&# Consistent and inconsistent
ct05-Truth-tables-2.md&5.0&
ct05-Truth-tables-2.md&5.1&## Consistent statements
ct05-Truth-tables-2.md&5.1&
ct05-Truth-tables-2.md&5.1&Multiple statements are *consistent* with each other if they can be true at the same time. That is, if it is possible for them to be all true together.
ct05-Truth-tables-2.md&5.1&
ct05-Truth-tables-2.md&5.1&A set of statements is *inconsistent* if it would be impossible for them all to be true at the same time.
ct05-Truth-tables-2.md&5.1&
ct05-Truth-tables-2.md&5.1&*Don’t confuse consistent and contingent!*
ct05-Truth-tables-2.md&5.1&
ct05-Truth-tables-2.md&5.2&## Consistent statements
ct05-Truth-tables-2.md&5.2&
ct05-Truth-tables-2.md&5.2&C: I am in class now  
ct05-Truth-tables-2.md&5.2&H: I am hungry now  
ct05-Truth-tables-2.md&5.2&P: I am at Pacific Place now  
ct05-Truth-tables-2.md&5.2&
ct05-Truth-tables-2.md&5.2&>- C and H are consistent (I can be in class and hungry at the same time, although this doesn’t *have* to be true!)
ct05-Truth-tables-2.md&5.2&>- H and P are also consistent.
ct05-Truth-tables-2.md&5.2&>- But C and P are not consistent (they are inconsistent), because I cannot be in two places at the same time.
ct05-Truth-tables-2.md&5.2&
ct05-Truth-tables-2.md&5.3&## Are these statements consistent?
ct05-Truth-tables-2.md&5.3&
ct05-Truth-tables-2.md&5.3&1. I will go the cinema tonight.
ct05-Truth-tables-2.md&5.3&2. If I don’t go to the cinema tonight, I will not eat rice tonight.
ct05-Truth-tables-2.md&5.3&3. I don’t eat rice tonight.
ct05-Truth-tables-2.md&5.3&
ct05-Truth-tables-2.md&5.3&*Use a truth table to show whether these statements are consistent!*
ct05-Truth-tables-2.md&5.3&
ct05-Truth-tables-2.md&5.4&## Are these statements consistent?
ct05-Truth-tables-2.md&5.4&
ct05-Truth-tables-2.md&5.4&1. I will go the cinema tonight:  
ct05-Truth-tables-2.md&5.4&\f{c}
ct05-Truth-tables-2.md&5.4&2. If I don’t go to the cinema tonight, I will not eat rice tonight:  
ct05-Truth-tables-2.md&5.4&\f{$\lnot$c$\to\lnot$r}
ct05-Truth-tables-2.md&5.4&3. I don’t eat rice tonight:  
ct05-Truth-tables-2.md&5.4&\f{$\lnot$r}
ct05-Truth-tables-2.md&5.4&
ct05-Truth-tables-2.md&5.4&. . . 
ct05-Truth-tables-2.md&5.4&
ct05-Truth-tables-2.md&5.4&![Yes, they are consistent.](graphics/07-tt-consistent.png)\ 
ct05-Truth-tables-2.md&5.4&
ct05-Truth-tables-2.md&5.5&## Are these statements consistent?
ct05-Truth-tables-2.md&5.5&
ct05-Truth-tables-2.md&5.5&1. I will go the cinema tonight
ct05-Truth-tables-2.md&5.5&2. *If and only if* I don’t go to the cinema tonight, I will not eat rice tonight
ct05-Truth-tables-2.md&5.5&3. I don’t eat rice tonight
ct05-Truth-tables-2.md&5.5&
ct05-Truth-tables-2.md&5.5&*Use a truth table to show whether these statements are consistent!*
ct05-Truth-tables-2.md&5.5&
ct05-Truth-tables-2.md&5.6&## Are these statements consistent?
ct05-Truth-tables-2.md&5.6&
ct05-Truth-tables-2.md&5.6&1. I will go the cinema tonight:  
ct05-Truth-tables-2.md&5.6&\f{c}
ct05-Truth-tables-2.md&5.6&2. If and only if I don’t go to the cinema tonight, I will not eat rice tonight:  
ct05-Truth-tables-2.md&5.6&\f{$\lnot$c$\leftrightarrow\lnot$r}
ct05-Truth-tables-2.md&5.6&3. I don’t eat rice tonight:  
ct05-Truth-tables-2.md&5.6&\f{$\lnot$r}
ct05-Truth-tables-2.md&5.6&
ct05-Truth-tables-2.md&5.6&. . . 
ct05-Truth-tables-2.md&5.6&
ct05-Truth-tables-2.md&5.6&![No, they are inconsistent.](graphics/07-tt-inconsistent.png)\ 
ct05-Truth-tables-2.md&5.6&
ct05-Truth-tables-2.md&6.0&# Exercises
ct05-Truth-tables-2.md&6.0&
ct05-Truth-tables-2.md&6.1&## Translate into symbolic form:
ct05-Truth-tables-2.md&6.1&
ct05-Truth-tables-2.md&6.1&"For lunch, I want a sandwich with cheese or egg. Or I might have a soup or a steak, but the soup without chicken meat or fish balls."
ct05-Truth-tables-2.md&6.1&
ct05-Truth-tables-2.md&6.1&. . .
ct05-Truth-tables-2.md&6.1&
ct05-Truth-tables-2.md&6.1&- w: sandwich, c: cheese, e: egg
ct05-Truth-tables-2.md&6.1&- s: soup, t: steak
ct05-Truth-tables-2.md&6.1&- m: chicken meat, f: fish balls
ct05-Truth-tables-2.md&6.1&
ct05-Truth-tables-2.md&6.1&\f{\LARGE{(w$\land$(c$\lor$e)) $\lor$ (s$\land\lnot$(m$\lor$f)) $\lor$ t}}
ct05-Truth-tables-2.md&6.1&
ct05-Truth-tables-2.md&6.1&. . . 
ct05-Truth-tables-2.md&6.1&
ct05-Truth-tables-2.md&6.1&Second version (not so good):
ct05-Truth-tables-2.md&6.1&
ct05-Truth-tables-2.md&6.1&\f{\textcolor{red}{(c$\lor$e)}$\lor$(s$\land\lnot$(m$\lor$f))$\lor$t}
ct05-Truth-tables-2.md&6.1&
ct05-Truth-tables-2.md&6.1&This is “not so good” because it does not mirror the original sentence exactly 
ct05-Truth-tables-2.md&6.1&(the “sandwich with” part is gone)
ct05-Truth-tables-2.md&6.1&
ct05-Truth-tables-2.md&6.2&## Why is this one wrong?
ct05-Truth-tables-2.md&6.2&
ct05-Truth-tables-2.md&6.2&"For lunch, I want a sandwich with cheese or egg. Or I might have a soup or a steak, but the soup without chicken meat or fish balls."
ct05-Truth-tables-2.md&6.2&
ct05-Truth-tables-2.md&6.2&- w: sandwich, c: cheese, e: egg
ct05-Truth-tables-2.md&6.2&- s: soup, t: steak
ct05-Truth-tables-2.md&6.2&- m: chicken meat, f: fish balls
ct05-Truth-tables-2.md&6.2&
ct05-Truth-tables-2.md&6.2&**Wrong:** \f{(w$\land$(c$\lor$e))$\lor$((s$\lor$t)$\land$\textcolor{red}{$\lnot$(m$\lor$f)})}
ct05-Truth-tables-2.md&6.2&
ct05-Truth-tables-2.md&6.2&. . .
ct05-Truth-tables-2.md&6.2&
ct05-Truth-tables-2.md&6.2&Because it doesn't connect the chicken meat and the fish balls with the soup!
ct05-Truth-tables-2.md&6.2&
ct05-Truth-tables-2.md&6.2&(So this one would forbid the serving of a chicken steak. But the customer didn't exclude it.)
ct05-Truth-tables-2.md&6.2&
ct05-Truth-tables-2.md&6.3&## Why is this one wrong?
ct05-Truth-tables-2.md&6.3&
ct05-Truth-tables-2.md&6.3&"For lunch, I want a sandwich with cheese or egg. Or I might have a soup or a steak, but the soup without chicken meat or fish balls."
ct05-Truth-tables-2.md&6.3&
ct05-Truth-tables-2.md&6.3&- w: sandwich, c: cheese, e: egg
ct05-Truth-tables-2.md&6.3&- s: soup, t: steak
ct05-Truth-tables-2.md&6.3&- m: chicken meat, f: fish balls
ct05-Truth-tables-2.md&6.3&
ct05-Truth-tables-2.md&6.3&**Wrong:** \f{(w$\land$(c$\lor$e))$\lor$(s$\land\lnot$\textcolor{red}{(m$\land$f)})$\lor$t}
ct05-Truth-tables-2.md&6.3&
ct05-Truth-tables-2.md&6.3&. . .
ct05-Truth-tables-2.md&6.3&
ct05-Truth-tables-2.md&6.3&This would mean that I don't want meat *and* fishballs at the same time. But if I had *only* meat, I would be happy (which is not what the speaker meant). So it must be “not (meat *or* fishballs)” instead:
ct05-Truth-tables-2.md&6.3&
ct05-Truth-tables-2.md&6.3&\f{(w$\land$(c$\lor$e))$\lor$(s$\land\lnot$\textcolor{green}{(m$\lor$f)})$\lor$t}
ct05-Truth-tables-2.md&6.3&
ct05-Truth-tables-2.md&6.4&## Meat and/or fishballs
ct05-Truth-tables-2.md&6.4&
ct05-Truth-tables-2.md&6.4&!["Without meat and/or fishballs." We will discuss this more next time!](graphics/07-meat-fishballs.png)
ct05-Truth-tables-2.md&6.4&
ct05-Truth-tables-2.md&6.5&## Translate into symbolic form:
ct05-Truth-tables-2.md&6.5&
ct05-Truth-tables-2.md&6.5&"I'd like a green tea with peach flavour or a jasmine tea without any flavour. Into the green tea with peach flavour I like sugar, but put sugar into the jasmine tea only if it is very hot. Otherwise please serve it with some milk. Thank you!"
ct05-Truth-tables-2.md&6.5&
ct05-Truth-tables-2.md&6.5&- g: I like green tea, p: I like peach flavour
ct05-Truth-tables-2.md&6.5&- j: I like jasmine tea, f: I like any flavour
ct05-Truth-tables-2.md&6.5&- s: put sugar into the tea, h: tea is very hot
ct05-Truth-tables-2.md&6.5&- m: serve it with some milk
ct05-Truth-tables-2.md&6.5&
ct05-Truth-tables-2.md&6.5&. . .
ct05-Truth-tables-2.md&6.5&
ct05-Truth-tables-2.md&6.5&\f{\LARGE{(g$\land$p$\land$s)$\lor$(j$\land\lnot$f$\land$h$\land$s)$\lor$(j$\land\lnot$f$\land\lnot$h$\land$m)}}
ct05-Truth-tables-2.md&6.5&
ct05-Truth-tables-2.md&6.5&or
ct05-Truth-tables-2.md&6.5&
ct05-Truth-tables-2.md&6.5&\f{\LARGE{(g$\land$p$\land$s)$\lor$((j$\land\lnot$f)$\land$((h$\leftrightarrow$s)$\land$($\lnot$h$\leftrightarrow$m)))}}
ct05-Truth-tables-2.md&6.5&
ct05-Truth-tables-2.md&6.6&## Common mistake
ct05-Truth-tables-2.md&6.6&
ct05-Truth-tables-2.md&6.6&A common mistake in the previous expression would be to write something like:
ct05-Truth-tables-2.md&6.6&
ct05-Truth-tables-2.md&6.6&- A: expression for the one tea
ct05-Truth-tables-2.md&6.6&- B: expression for the other tea
ct05-Truth-tables-2.md&6.6&- M: I want the tea with milk
ct05-Truth-tables-2.md&6.6&
ct05-Truth-tables-2.md&6.6&Wrong: \f{A$\lor$B$\lor$M}
ct05-Truth-tables-2.md&6.6&
ct05-Truth-tables-2.md&6.6&Because now we could have the milk (M) without any tea!
ct05-Truth-tables-2.md&6.6&
ct05-Truth-tables-2.md&6.6&You must make sure that the milk is connected to some tea.
ct05-Truth-tables-2.md&6.6&
ct05-Truth-tables-2.md&6.7&## Comparing truth tables
ct05-Truth-tables-2.md&6.7&
ct05-Truth-tables-2.md&6.7&By comparing the truth tables of sentences, we can see exactly the logical differences between sentences.
ct05-Truth-tables-2.md&6.7&
ct05-Truth-tables-2.md&6.7&Compare:
ct05-Truth-tables-2.md&6.7&
ct05-Truth-tables-2.md&6.7&1. Give me a tea with milk and no sugar
ct05-Truth-tables-2.md&6.7&2. If you give me a tea with milk, it should have no sugar.
ct05-Truth-tables-2.md&6.7&
ct05-Truth-tables-2.md&6.7&*Write down and compare the truth tables!*
ct05-Truth-tables-2.md&6.7&
ct05-Truth-tables-2.md&6.7&. . .
ct05-Truth-tables-2.md&6.7&
ct05-Truth-tables-2.md&6.7&1. \f{t$\land$m$\land\lnot$s}
ct05-Truth-tables-2.md&6.7&2. \f{(t$\land$m)$\to\lnot$s}
ct05-Truth-tables-2.md&6.7&
ct05-Truth-tables-2.md&6.8&## Solution
ct05-Truth-tables-2.md&6.8&
ct05-Truth-tables-2.md&6.8&![](graphics/07-tea-milk-sugar.png)
ct05-Truth-tables-2.md&6.8&
ct05-Truth-tables-2.md&6.9&## What did we learn today?
ct05-Truth-tables-2.md&6.9&
ct05-Truth-tables-2.md&6.9&- We practiced using truth tables on everyday language.
ct05-Truth-tables-2.md&6.9&- We talked about tautologies, contradictions, contingent, consistent and inconsistent statements.
ct05-Truth-tables-2.md&6.9&
ct05-Truth-tables-2.md&6.10&## Any questions?
ct05-Truth-tables-2.md&6.10&
ct05-Truth-tables-2.md&6.10&![](graphics/questions.jpg)\ 
ct05-Truth-tables-2.md&6.10&
ct05-Truth-tables-2.md&6.11&## Thank you for your attention!
ct05-Truth-tables-2.md&6.11&
ct05-Truth-tables-2.md&6.11&Email: <matthias@ln.edu.hk>
ct05-Truth-tables-2.md&6.11&
ct05-Truth-tables-2.md&6.11&If you have questions, please come to my office hours (see course outline).
ct06-Tautologies-Conditional.md&0.1&% Critical Thinking
ct06-Tautologies-Conditional.md&0.1&% 6. Tautologies, contradictions, conditions and conditional arguments
ct06-Tautologies-Conditional.md&0.1&% A. Matthias
ct06-Tautologies-Conditional.md&0.1&
ct06-Tautologies-Conditional.md&1.0&# Where we are
ct06-Tautologies-Conditional.md&1.0&
ct06-Tautologies-Conditional.md&1.1&## What did we learn last time?
ct06-Tautologies-Conditional.md&1.1&
ct06-Tautologies-Conditional.md&1.1&- We practiced using truth tables on everyday language.
ct06-Tautologies-Conditional.md&1.1&- We talked about tautologies, contradictions, contingent, consistent and inconsistent statements.
ct06-Tautologies-Conditional.md&1.1&
ct06-Tautologies-Conditional.md&1.2&## What are we going to learn today?
ct06-Tautologies-Conditional.md&1.2&
ct06-Tautologies-Conditional.md&1.2&- More tautologies and contradictions
ct06-Tautologies-Conditional.md&1.2&- Conditional expressions and conditional arguments
ct06-Tautologies-Conditional.md&1.2&- Antecedent and consequent
ct06-Tautologies-Conditional.md&1.2&- Unstated (hidden) conditions
ct06-Tautologies-Conditional.md&1.2&- Valid and invalid argument forms
ct06-Tautologies-Conditional.md&1.2&
ct06-Tautologies-Conditional.md&2.0&# Equivalent expressions
ct06-Tautologies-Conditional.md&2.0&
ct06-Tautologies-Conditional.md&2.1&## From last session:
ct06-Tautologies-Conditional.md&2.1&
ct06-Tautologies-Conditional.md&2.1&"For lunch, I want a sandwich with cheese or egg. Or I might have a soup or a steak, but the soup without chicken meat or fish balls."
ct06-Tautologies-Conditional.md&2.1&
ct06-Tautologies-Conditional.md&2.1&- w: sandwich
ct06-Tautologies-Conditional.md&2.1&- c: cheese
ct06-Tautologies-Conditional.md&2.1&- e: egg
ct06-Tautologies-Conditional.md&2.1&- s: soup, t: steak
ct06-Tautologies-Conditional.md&2.1&- m: chicken meat, f: fish balls
ct06-Tautologies-Conditional.md&2.1&
ct06-Tautologies-Conditional.md&2.1&\f{(w$\land$(c$\lor$e))$\lor$(s $\land\lnot$(m$\lor$f))$\lor$t}
ct06-Tautologies-Conditional.md&2.1&
ct06-Tautologies-Conditional.md&2.2&## Equivalent expressions
ct06-Tautologies-Conditional.md&2.2&
ct06-Tautologies-Conditional.md&2.2&![](graphics/08-equiv1.png)\ 
ct06-Tautologies-Conditional.md&2.2&
ct06-Tautologies-Conditional.md&2.3&## Equivalent expressions
ct06-Tautologies-Conditional.md&2.3&
ct06-Tautologies-Conditional.md&2.3&>- Come for hot-pot tonight, but don’t bring Mary **or** Peter!
ct06-Tautologies-Conditional.md&2.3&>      - (means: I don’t like Mary, I don’t like Peter. I don’t like them together and I don’t like them separately. Just come without either of them.)
ct06-Tautologies-Conditional.md&2.3&
ct06-Tautologies-Conditional.md&2.3&>- Come for hot-pot tonight, but don’t bring Mary **and** Peter!
ct06-Tautologies-Conditional.md&2.3&>      - (means: don’t bring both of them together, perhaps because they’re always quarreling with each other. But you can bring one of them if you want!)
ct06-Tautologies-Conditional.md&2.3&
ct06-Tautologies-Conditional.md&2.4&## Don’t bring Mary and/or Peter
ct06-Tautologies-Conditional.md&2.4&
ct06-Tautologies-Conditional.md&2.4&- Don’t bring Mary **or** Peter: $\lnot$(m$\lor$p)
ct06-Tautologies-Conditional.md&2.4&- Don’t bring Mary **and** Peter: $\lnot$(m$\land$p)
ct06-Tautologies-Conditional.md&2.4&
ct06-Tautologies-Conditional.md&2.4&![](graphics/08-equiv2.png)\ 
ct06-Tautologies-Conditional.md&2.4&
ct06-Tautologies-Conditional.md&2.5&## Prove with a truth table that the two statements are equivalent!
ct06-Tautologies-Conditional.md&2.5&
ct06-Tautologies-Conditional.md&2.5&(1) I'll have soup with no meat or fishballs: $\lnot$(M$\lor$F)
ct06-Tautologies-Conditional.md&2.5&(2) I'll have soup with no meat and no fishballs: $\lnot$M$\land\lnot$F
ct06-Tautologies-Conditional.md&2.5&
ct06-Tautologies-Conditional.md&2.6&## Equivalent logical forms
ct06-Tautologies-Conditional.md&2.6&
ct06-Tautologies-Conditional.md&2.6&\f{$\lnot$M$\land\lnot$F}
ct06-Tautologies-Conditional.md&2.6&
ct06-Tautologies-Conditional.md&2.6&In such cases, we can take the “not” out of the bracket, by removing it from all propositions and changing the “and” into an “or”:
ct06-Tautologies-Conditional.md&2.6&
ct06-Tautologies-Conditional.md&2.6&\f{$\lnot$(M$\lor$F)}
ct06-Tautologies-Conditional.md&2.6&
ct06-Tautologies-Conditional.md&2.6&This is called *de Morgan's Law.*
ct06-Tautologies-Conditional.md&2.6&
ct06-Tautologies-Conditional.md&3.0&# Tautologies and contradictions
ct06-Tautologies-Conditional.md&3.0&
ct06-Tautologies-Conditional.md&3.1&## Logical tautologies and contradictions
ct06-Tautologies-Conditional.md&3.1&
ct06-Tautologies-Conditional.md&3.1&>- Logical tautologies: statements that are *necessarily* true, for logical reasons.
ct06-Tautologies-Conditional.md&3.1&>- Logical contradictions: statements that are *necessarily* false, for logical reasons.
ct06-Tautologies-Conditional.md&3.1&
ct06-Tautologies-Conditional.md&3.2&## Logical tautologies or contradictions?
ct06-Tautologies-Conditional.md&3.2&
ct06-Tautologies-Conditional.md&3.2&Guess what these are without using a truth table!
ct06-Tautologies-Conditional.md&3.2&
ct06-Tautologies-Conditional.md&3.2&1. Tonight I will go to the cinema and not go to the cinema.
ct06-Tautologies-Conditional.md&3.2&2. Tonight I will go to the cinema or not go to the cinema.
ct06-Tautologies-Conditional.md&3.2&3. (A$\to$(B$\lor$A))$\lor$(C$\land$D)
ct06-Tautologies-Conditional.md&3.2&
ct06-Tautologies-Conditional.md&3.3&## Logical tautologies or contradictions?
ct06-Tautologies-Conditional.md&3.3&
ct06-Tautologies-Conditional.md&3.3&>1. Tonight I will go to the cinema **and** not go to the cinema.
ct06-Tautologies-Conditional.md&3.3&>      - Contradiction: C$\land\lnot$C is always false!
ct06-Tautologies-Conditional.md&3.3&>2. Tonight I will go to the cinema **or** not go to the cinema.
ct06-Tautologies-Conditional.md&3.3&>      - Tautology: C$\lor\lnot$C is always true!
ct06-Tautologies-Conditional.md&3.3&>3. (A$\to$(B$\lor$A))$\lor$(C$\land$D)
ct06-Tautologies-Conditional.md&3.3&>      - Tautology: The only case where the implication could be false is if A is true and (B$\lor$A) is false.
ct06-Tautologies-Conditional.md&3.3&>      - But if A is true, then (B$\lor$A) must also be true (the same A!)
ct06-Tautologies-Conditional.md&3.3&>      - Thus, (A$\to$(B$\lor$A)) is true, and therefore (disjunction)  (A$\to$(B$\lor$A))$\lor$(C$\land$D) must also (always) be true.
ct06-Tautologies-Conditional.md&3.3&
ct06-Tautologies-Conditional.md&3.4&## Are these tautologies, contradictions, or contingent statements?
ct06-Tautologies-Conditional.md&3.4&
ct06-Tautologies-Conditional.md&3.4&*Do not use a truth table.*
ct06-Tautologies-Conditional.md&3.4&
ct06-Tautologies-Conditional.md&3.4&1. (A$\to$B)$\lor\lnot$A
ct06-Tautologies-Conditional.md&3.4&2. (A$\to$B)$\lor$A
ct06-Tautologies-Conditional.md&3.4&3. (A$\to$B)$\lor\lnot$B
ct06-Tautologies-Conditional.md&3.4&4. (A$\to$B)$\lor$B
ct06-Tautologies-Conditional.md&3.4&
ct06-Tautologies-Conditional.md&3.5&## Are these tautologies, contradictions, or contingent statements?
ct06-Tautologies-Conditional.md&3.5&
ct06-Tautologies-Conditional.md&3.5&1. (A$\to$B)$\lor\lnot$A : Contingent
ct06-Tautologies-Conditional.md&3.5&2. (A$\to$B)$\lor$A : Tautology
ct06-Tautologies-Conditional.md&3.5&3. (A$\to$B)$\lor\lnot$B: Tautology
ct06-Tautologies-Conditional.md&3.5&4. (A$\to$B)$\lor$B: Contingent
ct06-Tautologies-Conditional.md&3.5&
ct06-Tautologies-Conditional.md&3.6&## Rhetorical tautologies
ct06-Tautologies-Conditional.md&3.6&
ct06-Tautologies-Conditional.md&3.6&We don't have only *logical* tautologies and  contradictions, but also *rhetorical* tautologies and contradictions.
ct06-Tautologies-Conditional.md&3.6&
ct06-Tautologies-Conditional.md&3.6&*Rhetorical tautologies* are sentences in which we repeat redundant information.
ct06-Tautologies-Conditional.md&3.6&
ct06-Tautologies-Conditional.md&3.6&*Rhetorical contradictions* are sentences in which we contradict ourselves.
ct06-Tautologies-Conditional.md&3.6&
ct06-Tautologies-Conditional.md&3.7&## Tautologies and contradictions
ct06-Tautologies-Conditional.md&3.7&
ct06-Tautologies-Conditional.md&3.7&Please comment on the following:
ct06-Tautologies-Conditional.md&3.7&
ct06-Tautologies-Conditional.md&3.7&1. She is a female girl.
ct06-Tautologies-Conditional.md&3.7&2. Tonight I'll either go to the cinema or not.
ct06-Tautologies-Conditional.md&3.7&3. I very much enjoyed the finished result of that play.
ct06-Tautologies-Conditional.md&3.7&4. You need to know these true facts about heart disease.
ct06-Tautologies-Conditional.md&3.7&5. You are required to have three credits, but you may be exempted.
ct06-Tautologies-Conditional.md&3.7&6. Let's go to the ATM machine!
ct06-Tautologies-Conditional.md&3.7&7. She is the daughter of her uncle William.
ct06-Tautologies-Conditional.md&3.7&8. This is a necessary requirement
ct06-Tautologies-Conditional.md&3.7&
ct06-Tautologies-Conditional.md&3.8&## Tautologies and contradictions
ct06-Tautologies-Conditional.md&3.8&
ct06-Tautologies-Conditional.md&3.8&1. She is a female girl: Tautology (rhetorical).
ct06-Tautologies-Conditional.md&3.8&2. Tonight I'll either go to the cinema or not: Tautology (logical).
ct06-Tautologies-Conditional.md&3.8&3. I very much enjoyed the finished result of that play: Tautology (rhetorical).
ct06-Tautologies-Conditional.md&3.8&4. You need to know these true facts about heart disease: Tautology (rhetorical: "true facts").
ct06-Tautologies-Conditional.md&3.8&5. You are required to have three credits, but you may be exempted: Contradiction (rhetorical).
ct06-Tautologies-Conditional.md&3.8&6. Let's go to the ATM machine: Tautology (rhetorical): The M in ATM already stands for "machine."
ct06-Tautologies-Conditional.md&3.8&7. She is the daughter of her uncle William: Contradiction (rhetorical).
ct06-Tautologies-Conditional.md&3.8&8. This is a necessary requirement: Tautology (rhetorical).
ct06-Tautologies-Conditional.md&3.8&
ct06-Tautologies-Conditional.md&4.0&# Conditional arguments
ct06-Tautologies-Conditional.md&4.0&
ct06-Tautologies-Conditional.md&4.1&## Conditional expressions
ct06-Tautologies-Conditional.md&4.1&A conditional expression is a particular form of expression:
ct06-Tautologies-Conditional.md&4.1&
ct06-Tautologies-Conditional.md&4.1&>- It contains two propositions.
ct06-Tautologies-Conditional.md&4.1&>- They are linked together by an implication (if-then).
ct06-Tautologies-Conditional.md&4.1&
ct06-Tautologies-Conditional.md&4.1&. . .
ct06-Tautologies-Conditional.md&4.1&
ct06-Tautologies-Conditional.md&4.1&Examples:
ct06-Tautologies-Conditional.md&4.1&
ct06-Tautologies-Conditional.md&4.1&>- If I study, then I will pass my examination.
ct06-Tautologies-Conditional.md&4.1&>- My girlfriend would not have left me, if I had made her more presents.
ct06-Tautologies-Conditional.md&4.1&>- You will get sick if you eat only fast food.
ct06-Tautologies-Conditional.md&4.1&
ct06-Tautologies-Conditional.md&4.2&## Standard form
ct06-Tautologies-Conditional.md&4.2&
ct06-Tautologies-Conditional.md&4.2&The standard form of a conditional expression is
ct06-Tautologies-Conditional.md&4.2&
ct06-Tautologies-Conditional.md&4.2&**if** (something) **then** (something else)
ct06-Tautologies-Conditional.md&4.2&
ct06-Tautologies-Conditional.md&4.3&## Standard form
ct06-Tautologies-Conditional.md&4.3&
ct06-Tautologies-Conditional.md&4.3&>- If  I study, then I will pass my examination
ct06-Tautologies-Conditional.md&4.3&>- My girlfriend would not have left me, if I had made her more presents = If  I had made my girlfriend more presents, then she would not have left me.
ct06-Tautologies-Conditional.md&4.3&>- You will get sick if you eat only fast food = If you eat only fast food, then you will get sick.
ct06-Tautologies-Conditional.md&4.3&
ct06-Tautologies-Conditional.md&4.3&. . . 
ct06-Tautologies-Conditional.md&4.3&
ct06-Tautologies-Conditional.md&4.3&>- The proposition after the “if” is called the *antecedent.*
ct06-Tautologies-Conditional.md&4.3&>- The proposition after the “then” is called the *consequent.*
ct06-Tautologies-Conditional.md&4.3&
ct06-Tautologies-Conditional.md&4.4&## *Antecedent* and **consequent**
ct06-Tautologies-Conditional.md&4.4&
ct06-Tautologies-Conditional.md&4.4&- *If I study*, **then I will pass my examination.**
ct06-Tautologies-Conditional.md&4.4&- **My girlfriend would not have left me,** *if I had made her more presents.*
ct06-Tautologies-Conditional.md&4.4&- **You will get sick** *if you eat only fast food.*
ct06-Tautologies-Conditional.md&4.4&
ct06-Tautologies-Conditional.md&5.0&# Hidden conditions
ct06-Tautologies-Conditional.md&5.0&
ct06-Tautologies-Conditional.md&5.1&## Hidden conditions
ct06-Tautologies-Conditional.md&5.1&
ct06-Tautologies-Conditional.md&5.1&>- Often there are hidden (unstated) conditions involved, which are required to bring about the consequent.
ct06-Tautologies-Conditional.md&5.1&>- We need to uncover them, in order to understand the full meaning of the conditional expression and to evaluate its truth.
ct06-Tautologies-Conditional.md&5.1&
ct06-Tautologies-Conditional.md&5.1&. . . 
ct06-Tautologies-Conditional.md&5.1&
ct06-Tautologies-Conditional.md&5.1&>- "If you have a tea bag, you can make some tea."
ct06-Tautologies-Conditional.md&5.1&>- This *looks like* a sufficient condition, but it is not: you still need hot water and a cup.
ct06-Tautologies-Conditional.md&5.1&
ct06-Tautologies-Conditional.md&5.1&. . .
ct06-Tautologies-Conditional.md&5.1&
ct06-Tautologies-Conditional.md&5.1&The full argument thus is:
ct06-Tautologies-Conditional.md&5.1&
ct06-Tautologies-Conditional.md&5.1&(1) If you have a tea bag and   
ct06-Tautologies-Conditional.md&5.1&(2) hot water *(hidden condition)* and   
ct06-Tautologies-Conditional.md&5.1&(3) a cup, *(hidden condition)*   
ct06-Tautologies-Conditional.md&5.1&then you can make some tea.
ct06-Tautologies-Conditional.md&5.1&
ct06-Tautologies-Conditional.md&5.2&## Hidden conditions
ct06-Tautologies-Conditional.md&5.2&Find the hidden conditions:
ct06-Tautologies-Conditional.md&5.2&
ct06-Tautologies-Conditional.md&5.2&1. If the weather forecast says that it will rain, it will.
ct06-Tautologies-Conditional.md&5.2&2. If I go to the beach, I will get sunburned.
ct06-Tautologies-Conditional.md&5.2&3. If I study hard, I will get a good grade.
ct06-Tautologies-Conditional.md&5.2&4. If I do sports every day, I will be healthy.
ct06-Tautologies-Conditional.md&5.2&5. If I read many books, I will be educated.
ct06-Tautologies-Conditional.md&5.2&
ct06-Tautologies-Conditional.md&5.3&## Hidden conditions
ct06-Tautologies-Conditional.md&5.3&
ct06-Tautologies-Conditional.md&5.3&>- If the weather forecast says that it will rain,
ct06-Tautologies-Conditional.md&5.3&>- and the forecast is right,
ct06-Tautologies-Conditional.md&5.3&>- then it will rain.
ct06-Tautologies-Conditional.md&5.3&
ct06-Tautologies-Conditional.md&5.4&## Hidden conditions
ct06-Tautologies-Conditional.md&5.4&
ct06-Tautologies-Conditional.md&5.4&>- If I go to the beach, 
ct06-Tautologies-Conditional.md&5.4&>- and the sun is shining,
ct06-Tautologies-Conditional.md&5.4&>- and I sit in the sun,
ct06-Tautologies-Conditional.md&5.4&>- and I expose my skin to the sun,
ct06-Tautologies-Conditional.md&5.4&>- and I don't use any protective cream,
ct06-Tautologies-Conditional.md&5.4&>- then I will get sunburned.
ct06-Tautologies-Conditional.md&5.4&
ct06-Tautologies-Conditional.md&5.5&## Hidden conditions
ct06-Tautologies-Conditional.md&5.5&
ct06-Tautologies-Conditional.md&5.5&>- If I study hard, 
ct06-Tautologies-Conditional.md&5.5&>- and I participate in the classes,
ct06-Tautologies-Conditional.md&5.5&>- and I am not sick or too tired at the examinations,
ct06-Tautologies-Conditional.md&5.5&>- and nothing distracts me from the examinations,
ct06-Tautologies-Conditional.md&5.5&>- then I will get a good grade.
ct06-Tautologies-Conditional.md&5.5&
ct06-Tautologies-Conditional.md&5.6&## Hidden conditions
ct06-Tautologies-Conditional.md&5.6&
ct06-Tautologies-Conditional.md&5.6&>- If I do sports every day,
ct06-Tautologies-Conditional.md&5.6&>- and I don't have any accidents,
ct06-Tautologies-Conditional.md&5.6&>- and I take care of what I eat,
ct06-Tautologies-Conditional.md&5.6&>- and I sleep well,
ct06-Tautologies-Conditional.md&5.6&>- and I'm generally lucky,
ct06-Tautologies-Conditional.md&5.6&>- then I will be healthy (for a few years).
ct06-Tautologies-Conditional.md&5.6&
ct06-Tautologies-Conditional.md&5.7&## Hidden conditions
ct06-Tautologies-Conditional.md&5.7&
ct06-Tautologies-Conditional.md&5.7&>- If I read many books,
ct06-Tautologies-Conditional.md&5.7&>- and these are educating books (e.g. not telephone directories),
ct06-Tautologies-Conditional.md&5.7&>- and I understand what they say,
ct06-Tautologies-Conditional.md&5.7&>- and I can integrate what I read into my life,
ct06-Tautologies-Conditional.md&5.7&>- then I will be educated.
ct06-Tautologies-Conditional.md&5.7&
ct06-Tautologies-Conditional.md&6.0&# Conditional argument forms
ct06-Tautologies-Conditional.md&6.0&
ct06-Tautologies-Conditional.md&6.1&## From conditions to arguments
ct06-Tautologies-Conditional.md&6.1&
ct06-Tautologies-Conditional.md&6.1&Until now, we examined only conditional expressions of the form:
ct06-Tautologies-Conditional.md&6.1&
ct06-Tautologies-Conditional.md&6.1&*if something then something else*
ct06-Tautologies-Conditional.md&6.1&
ct06-Tautologies-Conditional.md&6.1&. . . 
ct06-Tautologies-Conditional.md&6.1&
ct06-Tautologies-Conditional.md&6.1&This is a *hypothetical* form, which does not tell us about what *actually* happened (it doesn't have a conclusion!)
ct06-Tautologies-Conditional.md&6.1&
ct06-Tautologies-Conditional.md&6.1&. . . 
ct06-Tautologies-Conditional.md&6.1&
ct06-Tautologies-Conditional.md&6.1&In order to know what actually happened, we have to add a statement telling us the *facts.*
ct06-Tautologies-Conditional.md&6.1&
ct06-Tautologies-Conditional.md&6.2&## Argument forms
ct06-Tautologies-Conditional.md&6.2&
ct06-Tautologies-Conditional.md&6.2&1. If you eat too many mooncakes, you will be sick. *(conditional)*
ct06-Tautologies-Conditional.md&6.2&2. You eat too many mooncakes. *(statement of fact)*
ct06-Tautologies-Conditional.md&6.2&3. You are sick. *(conclusion)*
ct06-Tautologies-Conditional.md&6.2&
ct06-Tautologies-Conditional.md&6.3&## Premises and  conclusion
ct06-Tautologies-Conditional.md&6.3&
ct06-Tautologies-Conditional.md&6.3&1. If you eat too many mooncakes, you will be sick. *(conditional)*
ct06-Tautologies-Conditional.md&6.3&2. You eat too many mooncakes. *(statement of fact)*
ct06-Tautologies-Conditional.md&6.3&3. You are sick. *(conclusion)*
ct06-Tautologies-Conditional.md&6.3&
ct06-Tautologies-Conditional.md&6.3&- 1 and 2 are called the *premises* of the argument.
ct06-Tautologies-Conditional.md&6.3&- 3 is called the *conclusion.*
ct06-Tautologies-Conditional.md&6.3&
ct06-Tautologies-Conditional.md&6.4&## Premises and conclusion
ct06-Tautologies-Conditional.md&6.4&
ct06-Tautologies-Conditional.md&6.4&- We saw before that premises can be unstated (hidden).
ct06-Tautologies-Conditional.md&6.4&- Conclusions may also be hidden.
ct06-Tautologies-Conditional.md&6.4&
ct06-Tautologies-Conditional.md&6.4&. . . 
ct06-Tautologies-Conditional.md&6.4&
ct06-Tautologies-Conditional.md&6.4&For example:
ct06-Tautologies-Conditional.md&6.4&
ct06-Tautologies-Conditional.md&6.4&“Giving students a fail grade, will damage their self-confidence. We should not damage students’ self-confidence.”
ct06-Tautologies-Conditional.md&6.4&
ct06-Tautologies-Conditional.md&6.4&Here the conclusion is obvious, but not explicitly stated. Still this would count as an argument.
ct06-Tautologies-Conditional.md&6.4&
ct06-Tautologies-Conditional.md&6.5&## Conditional argument forms
ct06-Tautologies-Conditional.md&6.5&
ct06-Tautologies-Conditional.md&6.5&In an abstract way, we can see two categories of conditional arguments.
ct06-Tautologies-Conditional.md&6.5&
ct06-Tautologies-Conditional.md&6.5&1. Affirming the antecedent
ct06-Tautologies-Conditional.md&6.5&2. Denying the consequent
ct06-Tautologies-Conditional.md&6.5&
ct06-Tautologies-Conditional.md&6.5&*Which is which depends on the second premise (the statement of fact)!*
ct06-Tautologies-Conditional.md&6.5&
ct06-Tautologies-Conditional.md&6.6&## Affirming the antecedent
ct06-Tautologies-Conditional.md&6.6&
ct06-Tautologies-Conditional.md&6.6&You remember antecedent and consequent:
ct06-Tautologies-Conditional.md&6.6&
ct06-Tautologies-Conditional.md&6.6&If *antecedent* then **consequent.**
ct06-Tautologies-Conditional.md&6.6&
ct06-Tautologies-Conditional.md&6.6&If *I buy this watch,* **I'll get into financial trouble.**  
ct06-Tautologies-Conditional.md&6.6&*I bought this watch.*  
ct06-Tautologies-Conditional.md&6.6& ------------------------------------------------------------------  
ct06-Tautologies-Conditional.md&6.6&**I got into financial trouble.**
ct06-Tautologies-Conditional.md&6.6&
ct06-Tautologies-Conditional.md&6.6&. . .
ct06-Tautologies-Conditional.md&6.6&
ct06-Tautologies-Conditional.md&6.6&\vspace{3ex}
ct06-Tautologies-Conditional.md&6.6&
ct06-Tautologies-Conditional.md&6.6&Logical form:
ct06-Tautologies-Conditional.md&6.6&
ct06-Tautologies-Conditional.md&6.6&if *p* then **q** (*p*=antecedent, **q**=consequent)  
ct06-Tautologies-Conditional.md&6.6&*p* (affirming the antecedent)  
ct06-Tautologies-Conditional.md&6.6& ---------------------------------------------------  
ct06-Tautologies-Conditional.md&6.6&**q** (conclusion)
ct06-Tautologies-Conditional.md&6.6&
ct06-Tautologies-Conditional.md&6.7&## Affirming the antecedent
ct06-Tautologies-Conditional.md&6.7&
ct06-Tautologies-Conditional.md&6.7&>- If I take the bus, I'll arrive late. I took the bus. Conclusion: I arrived late.
ct06-Tautologies-Conditional.md&6.7&>- If I always do the exercises, I will get a good grade. So I know I'll get a good grade, because I'm always doing the exercises. (See that the order of second premise and conclusion is reversed!)
ct06-Tautologies-Conditional.md&6.7&
ct06-Tautologies-Conditional.md&6.8&## Affirming the antecedent
ct06-Tautologies-Conditional.md&6.8&
ct06-Tautologies-Conditional.md&6.8&*Make up an example of an argument which affirms the antecedent!*
ct06-Tautologies-Conditional.md&6.8&
ct06-Tautologies-Conditional.md&6.8&You need three sentences:
ct06-Tautologies-Conditional.md&6.8&
ct06-Tautologies-Conditional.md&6.8&- a conditional,
ct06-Tautologies-Conditional.md&6.8&- a statement of fact that affirms the antecedent,
ct06-Tautologies-Conditional.md&6.8&- and a conclusion!
ct06-Tautologies-Conditional.md&6.8&
ct06-Tautologies-Conditional.md&6.9&## Denying the consequent
ct06-Tautologies-Conditional.md&6.9&
ct06-Tautologies-Conditional.md&6.9&If *you really like noodles,* then **you eat them at least once a week.**  
ct06-Tautologies-Conditional.md&6.9&You **don't eat noodles at least once a week.**  
ct06-Tautologies-Conditional.md&6.9& -----------------------------------------------------------------  
ct06-Tautologies-Conditional.md&6.9&You *don't really like noodles.*
ct06-Tautologies-Conditional.md&6.9&
ct06-Tautologies-Conditional.md&6.9&\vspace{3ex}
ct06-Tautologies-Conditional.md&6.9&
ct06-Tautologies-Conditional.md&6.9&if *p* then **q** (*p*: antecedent, **q**: consequent)  
ct06-Tautologies-Conditional.md&6.9&not **q** (denying the consequent)  
ct06-Tautologies-Conditional.md&6.9& ------------------------------------------------  
ct06-Tautologies-Conditional.md&6.9&not *p*
ct06-Tautologies-Conditional.md&6.9&
ct06-Tautologies-Conditional.md&6.10&## Denying the consequent
ct06-Tautologies-Conditional.md&6.10&
ct06-Tautologies-Conditional.md&6.10&Make up an example of an argument which denies the consequent!
ct06-Tautologies-Conditional.md&6.10&
ct06-Tautologies-Conditional.md&6.11&## Latin names
ct06-Tautologies-Conditional.md&6.11&
ct06-Tautologies-Conditional.md&6.11&And two more names to remember. You will need them later as abbreviations ("MP" and "MT"):
ct06-Tautologies-Conditional.md&6.11&
ct06-Tautologies-Conditional.md&6.11&>- Affirming the antecedent is called Modus Ponens (“the way of putting in place”).
ct06-Tautologies-Conditional.md&6.11&>- Denying the consequent is called Modus Tollens (“the way of taking away”).
ct06-Tautologies-Conditional.md&6.11&
ct06-Tautologies-Conditional.md&6.12&## Invalid forms of argument
ct06-Tautologies-Conditional.md&6.12&
ct06-Tautologies-Conditional.md&6.12&What is wrong here?
ct06-Tautologies-Conditional.md&6.12&
ct06-Tautologies-Conditional.md&6.12&If bats were birds, then bats would have wings.  
ct06-Tautologies-Conditional.md&6.12&Bats have wings.  
ct06-Tautologies-Conditional.md&6.12& ------------------------------------------------  
ct06-Tautologies-Conditional.md&6.12&Therefore, bats are birds.
ct06-Tautologies-Conditional.md&6.12&
ct06-Tautologies-Conditional.md&6.12&. . . 
ct06-Tautologies-Conditional.md&6.12&
ct06-Tautologies-Conditional.md&6.12&\vspace{3ex}
ct06-Tautologies-Conditional.md&6.12&
ct06-Tautologies-Conditional.md&6.12&If empty bottles were ships, they would float on water.  
ct06-Tautologies-Conditional.md&6.12&Empty bottles do float on water.  
ct06-Tautologies-Conditional.md&6.12& ---------------------------------------------------------  
ct06-Tautologies-Conditional.md&6.12&Therefore, empty bottles are ships.
ct06-Tautologies-Conditional.md&6.12&
ct06-Tautologies-Conditional.md&6.13&## Invalid forms of argument
ct06-Tautologies-Conditional.md&6.13&
ct06-Tautologies-Conditional.md&6.13&If bats were birds, then bats would have wings.  
ct06-Tautologies-Conditional.md&6.13&Bats have wings.  
ct06-Tautologies-Conditional.md&6.13& ------------------------------------------------  
ct06-Tautologies-Conditional.md&6.13&Therefore, bats are birds.
ct06-Tautologies-Conditional.md&6.13&
ct06-Tautologies-Conditional.md&6.13&. . . 
ct06-Tautologies-Conditional.md&6.13&
ct06-Tautologies-Conditional.md&6.13&\vspace{3ex}
ct06-Tautologies-Conditional.md&6.13&
ct06-Tautologies-Conditional.md&6.13&if *p*, then **q**  
ct06-Tautologies-Conditional.md&6.13&**q** (affirming the consequent)  
ct06-Tautologies-Conditional.md&6.13& --------------------------------------  
ct06-Tautologies-Conditional.md&6.13&*p* (invalid!)
ct06-Tautologies-Conditional.md&6.13&
ct06-Tautologies-Conditional.md&6.13&. . . 
ct06-Tautologies-Conditional.md&6.13&
ct06-Tautologies-Conditional.md&6.13&\vspace{3ex}
ct06-Tautologies-Conditional.md&6.13&
ct06-Tautologies-Conditional.md&6.13&“Affirming the consequent” is an invalid form of argument!
ct06-Tautologies-Conditional.md&6.13&
ct06-Tautologies-Conditional.md&6.14&## Invalid forms of argument
ct06-Tautologies-Conditional.md&6.14&
ct06-Tautologies-Conditional.md&6.14&What is wrong here?
ct06-Tautologies-Conditional.md&6.14&
ct06-Tautologies-Conditional.md&6.14&If electric lights were candles, they would give light.  
ct06-Tautologies-Conditional.md&6.14&Electric lights are not candles.  
ct06-Tautologies-Conditional.md&6.14& ------------------------------------------------------------  
ct06-Tautologies-Conditional.md&6.14&Therefore, they do not give light.
ct06-Tautologies-Conditional.md&6.14&
ct06-Tautologies-Conditional.md&6.14&\vspace{3ex}
ct06-Tautologies-Conditional.md&6.14&
ct06-Tautologies-Conditional.md&6.14&. . . 
ct06-Tautologies-Conditional.md&6.14&
ct06-Tautologies-Conditional.md&6.14&if p, then q  
ct06-Tautologies-Conditional.md&6.14&not p (denying the antecedent!)  
ct06-Tautologies-Conditional.md&6.14& ------------------------------------  
ct06-Tautologies-Conditional.md&6.14&not q (invalid!)
ct06-Tautologies-Conditional.md&6.14&
ct06-Tautologies-Conditional.md&6.14&. . .
ct06-Tautologies-Conditional.md&6.14&
ct06-Tautologies-Conditional.md&6.14&\vspace{3ex}
ct06-Tautologies-Conditional.md&6.14&
ct06-Tautologies-Conditional.md&6.14&“Denying the antecedent” is an invalid form of argument!
ct06-Tautologies-Conditional.md&6.14&
ct06-Tautologies-Conditional.md&6.15&## Valid and invalid
ct06-Tautologies-Conditional.md&6.15&
ct06-Tautologies-Conditional.md&6.15&Using what we learned here, we can determine the validity of conditional arguments independently of their content, looking only at the logical form.
ct06-Tautologies-Conditional.md&6.15&
ct06-Tautologies-Conditional.md&6.15&Remember:
ct06-Tautologies-Conditional.md&6.15&
ct06-Tautologies-Conditional.md&6.15&>- Always when the “A-words” (**a**ntecedent, **a**ffirm) are paired with a “non-A” word, the argument is *invalid!*
ct06-Tautologies-Conditional.md&6.15&>- **A**ffirming **a**ntecedent (a with a), **d**enying **c**onsequent (not-a with not-a): valid.
ct06-Tautologies-Conditional.md&6.15&>- **A**ffirming **c**onsequent (a with c), **d**enying **a**ntecedent (a with d): *invalid!*
ct06-Tautologies-Conditional.md&6.15&
ct06-Tautologies-Conditional.md&7.0&# Exercises
ct06-Tautologies-Conditional.md&7.0&
ct06-Tautologies-Conditional.md&7.1&## Is the following correct?
ct06-Tautologies-Conditional.md&7.1&
ct06-Tautologies-Conditional.md&7.1&"Without complications, life would be boring. Life is not boring. So there are complications."
ct06-Tautologies-Conditional.md&7.1&
ct06-Tautologies-Conditional.md&7.1&. . . 
ct06-Tautologies-Conditional.md&7.1&
ct06-Tautologies-Conditional.md&7.1&\vspace{3ex}
ct06-Tautologies-Conditional.md&7.1&
ct06-Tautologies-Conditional.md&7.1&p=There are no complications  
ct06-Tautologies-Conditional.md&7.1&q=Life is boring  
ct06-Tautologies-Conditional.md&7.1&
ct06-Tautologies-Conditional.md&7.1&. . .
ct06-Tautologies-Conditional.md&7.1&
ct06-Tautologies-Conditional.md&7.1&\vspace{3ex}
ct06-Tautologies-Conditional.md&7.1&
ct06-Tautologies-Conditional.md&7.1&if p then q  
ct06-Tautologies-Conditional.md&7.1&not q  
ct06-Tautologies-Conditional.md&7.1& ------------  
ct06-Tautologies-Conditional.md&7.1&not p
ct06-Tautologies-Conditional.md&7.1&
ct06-Tautologies-Conditional.md&7.1&. . .
ct06-Tautologies-Conditional.md&7.1&
ct06-Tautologies-Conditional.md&7.1&Valid: “Denying the consequent.”
ct06-Tautologies-Conditional.md&7.1&
ct06-Tautologies-Conditional.md&7.2&## Is the following correct?
ct06-Tautologies-Conditional.md&7.2&
ct06-Tautologies-Conditional.md&7.2&"The night sky looks different in the northern and southern parts of the earth, and this would be the case if the earth were spherical^[Like a ball.] in shape. Therefore, the earth must be spherical in shape." (Aristotle)
ct06-Tautologies-Conditional.md&7.2&
ct06-Tautologies-Conditional.md&7.2&. . .
ct06-Tautologies-Conditional.md&7.2&
ct06-Tautologies-Conditional.md&7.2&\vspace{2ex}
ct06-Tautologies-Conditional.md&7.2&
ct06-Tautologies-Conditional.md&7.2&If the earth is spherical in shape, then the night sky ...  
ct06-Tautologies-Conditional.md&7.2&The night sky ...  
ct06-Tautologies-Conditional.md&7.2& ------------------------------------------------------------  
ct06-Tautologies-Conditional.md&7.2&Therefore the earth is spherical.
ct06-Tautologies-Conditional.md&7.2&
ct06-Tautologies-Conditional.md&7.2&. . . 
ct06-Tautologies-Conditional.md&7.2&
ct06-Tautologies-Conditional.md&7.2&\vspace{2ex}
ct06-Tautologies-Conditional.md&7.2&
ct06-Tautologies-Conditional.md&7.2&If p then q  
ct06-Tautologies-Conditional.md&7.2&q  
ct06-Tautologies-Conditional.md&7.2& ------------  
ct06-Tautologies-Conditional.md&7.2&p
ct06-Tautologies-Conditional.md&7.2&
ct06-Tautologies-Conditional.md&7.2&. . . 
ct06-Tautologies-Conditional.md&7.2&
ct06-Tautologies-Conditional.md&7.2&Invalid: “Affirming the consequent.”
ct06-Tautologies-Conditional.md&7.2&
ct06-Tautologies-Conditional.md&7.3&## Is the following correct?
ct06-Tautologies-Conditional.md&7.3&
ct06-Tautologies-Conditional.md&7.3&"I tell you this: if it wasn't James who killed the gangster, it must be you. And I know it wasn't James. So it must have been you."
ct06-Tautologies-Conditional.md&7.3&
ct06-Tautologies-Conditional.md&7.3&. . . 
ct06-Tautologies-Conditional.md&7.3&
ct06-Tautologies-Conditional.md&7.3&p=It wasn't James  
ct06-Tautologies-Conditional.md&7.3&q=It's you
ct06-Tautologies-Conditional.md&7.3&
ct06-Tautologies-Conditional.md&7.3&if p then q  
ct06-Tautologies-Conditional.md&7.3&p  
ct06-Tautologies-Conditional.md&7.3& ------------  
ct06-Tautologies-Conditional.md&7.3&q
ct06-Tautologies-Conditional.md&7.3&
ct06-Tautologies-Conditional.md&7.3&. . .
ct06-Tautologies-Conditional.md&7.3&
ct06-Tautologies-Conditional.md&7.3&Valid: “Affirming the antecedent.”
ct06-Tautologies-Conditional.md&7.3&
ct06-Tautologies-Conditional.md&7.3&
ct06-Tautologies-Conditional.md&7.4&## Valid and invalid argument forms
ct06-Tautologies-Conditional.md&7.4&
ct06-Tautologies-Conditional.md&7.4&![](graphics/09-valid.png)\ 
ct06-Tautologies-Conditional.md&7.4&
ct06-Tautologies-Conditional.md&7.5&## Valid and invalid argument forms
ct06-Tautologies-Conditional.md&7.5&
ct06-Tautologies-Conditional.md&7.5&Remember:
ct06-Tautologies-Conditional.md&7.5&
ct06-Tautologies-Conditional.md&7.5&>- Always when the “A-words” (**a**ntecedent, **a**ffirm) are paired with a “non-A” word, the argument is *invalid!*
ct06-Tautologies-Conditional.md&7.5&>- **A**ffirming **a**ntecedent (a with a), **d**enying **c**onsequent (not-a with not-a): valid.
ct06-Tautologies-Conditional.md&7.5&>- **A**ffirming **c**onsequent (a with c), **d**enying **a**ntecedent (a with d): *invalid!*
ct06-Tautologies-Conditional.md&7.5&
ct06-Tautologies-Conditional.md&7.6&## Valid and invalid forms
ct06-Tautologies-Conditional.md&7.6&
ct06-Tautologies-Conditional.md&7.6&- Affirming the antecedent (valid): If it rains, the street will be wet. It rains. Therefore, the street is wet.
ct06-Tautologies-Conditional.md&7.6&- Denying the consequent (valid): If it rains, the street will be wet. The street is not wet. Therefore, it has not rained.
ct06-Tautologies-Conditional.md&7.6&- Affirming the consequent (*invalid*): If it rains, the street will be wet. The street is wet. Therefore, it has rained.
ct06-Tautologies-Conditional.md&7.6&- Denying the antecedent (*invalid*): If it rains, the street will be wet. It has not rained. Therefore, the street is not wet.
ct06-Tautologies-Conditional.md&7.6&
ct06-Tautologies-Conditional.md&7.7&## Is the following correct?
ct06-Tautologies-Conditional.md&7.7&
ct06-Tautologies-Conditional.md&7.7&"Cats have four legs, and precisely this would be the case if cats were animals. Therefore, cats are animals."
ct06-Tautologies-Conditional.md&7.7&
ct06-Tautologies-Conditional.md&7.7&\vspace{3ex}
ct06-Tautologies-Conditional.md&7.7&
ct06-Tautologies-Conditional.md&7.7&. . .
ct06-Tautologies-Conditional.md&7.7&
ct06-Tautologies-Conditional.md&7.7&If cats were animals they would have four legs.  
ct06-Tautologies-Conditional.md&7.7&Cats have four legs.  
ct06-Tautologies-Conditional.md&7.7& ---------------------------------------------------------  
ct06-Tautologies-Conditional.md&7.7&(Conclusion:) Therefore cats are animals.
ct06-Tautologies-Conditional.md&7.7&
ct06-Tautologies-Conditional.md&7.7&\vspace{3ex}
ct06-Tautologies-Conditional.md&7.7&
ct06-Tautologies-Conditional.md&7.7&. . . 
ct06-Tautologies-Conditional.md&7.7&
ct06-Tautologies-Conditional.md&7.7&*Invalid: affirming the consequent!*
ct06-Tautologies-Conditional.md&7.7&
ct06-Tautologies-Conditional.md&7.8&## Is the following correct?
ct06-Tautologies-Conditional.md&7.8&
ct06-Tautologies-Conditional.md&7.8&"If each man had a definite set of rules of conduct by which he regulated his life he would be nothing but a machine. But there are no such rules, so men cannot be machines." (Alan Turing)
ct06-Tautologies-Conditional.md&7.8&
ct06-Tautologies-Conditional.md&7.8&\vspace{3ex}
ct06-Tautologies-Conditional.md&7.8&
ct06-Tautologies-Conditional.md&7.8&. . . 
ct06-Tautologies-Conditional.md&7.8&
ct06-Tautologies-Conditional.md&7.8&p=Each man has a set of rules (antecedent)  
ct06-Tautologies-Conditional.md&7.8&q=Man is nothing but a machine (consequent)  
ct06-Tautologies-Conditional.md&7.8&
ct06-Tautologies-Conditional.md&7.8&\vspace{3ex}
ct06-Tautologies-Conditional.md&7.8&
ct06-Tautologies-Conditional.md&7.8&. . . 
ct06-Tautologies-Conditional.md&7.8&
ct06-Tautologies-Conditional.md&7.8&Premise 1: if p then q  
ct06-Tautologies-Conditional.md&7.8&Premise 2: not p  
ct06-Tautologies-Conditional.md&7.8& --------------------------  
ct06-Tautologies-Conditional.md&7.8&Conclusion: not q
ct06-Tautologies-Conditional.md&7.8&
ct06-Tautologies-Conditional.md&7.8&*Invalid: “Denying the antecedent”!*
ct06-Tautologies-Conditional.md&7.8&
ct06-Tautologies-Conditional.md&7.9&## Is this valid?
ct06-Tautologies-Conditional.md&7.9&
ct06-Tautologies-Conditional.md&7.9&“However, if a Renshaw cell is also activated by the recurrent branch of the motor neuron, it will inhibit the motor neuron of the agonist. But the Renshaw cell was not activated, because you can clearly see in this measurement that the motor neuron of the agonist has not been inhibited.”
ct06-Tautologies-Conditional.md&7.9&
ct06-Tautologies-Conditional.md&7.9&. . . 
ct06-Tautologies-Conditional.md&7.9&
ct06-Tautologies-Conditional.md&7.9&\vspace{3ex}
ct06-Tautologies-Conditional.md&7.9&
ct06-Tautologies-Conditional.md&7.9&Denying the consequent (valid).
ct06-Tautologies-Conditional.md&7.9&
ct06-Tautologies-Conditional.md&7.9&\vspace{3ex}
ct06-Tautologies-Conditional.md&7.9&
ct06-Tautologies-Conditional.md&7.9&*Observe that you can decide on the validity of this argument, without understanding anything of it! This is cool.*
ct06-Tautologies-Conditional.md&7.9&
ct06-Tautologies-Conditional.md&7.10&## Conditional arguments
ct06-Tautologies-Conditional.md&7.10&
ct06-Tautologies-Conditional.md&7.10&Is this valid?
ct06-Tautologies-Conditional.md&7.10&
ct06-Tautologies-Conditional.md&7.10&“If bromine and chlorine return to the levels that were present in 1980 and every other aspect of the atmospheric environment is unchanged, we can expect a full recovery of the ozone layer. But bromine and chlorine will not return to the levels that were present in 1980, and therefore we cannot expect a full recovery of the ozone layer.”
ct06-Tautologies-Conditional.md&7.10&
ct06-Tautologies-Conditional.md&7.11&## Conditional arguments
ct06-Tautologies-Conditional.md&7.11&
ct06-Tautologies-Conditional.md&7.11&Is this valid?
ct06-Tautologies-Conditional.md&7.11&
ct06-Tautologies-Conditional.md&7.11&“If *bromine and chlorine return to the levels that were present in 1980* and every other aspect of the atmospheric environment is unchanged, we can expect a full recovery of the ozone layer. But *bromine and chlorine* **will not** *return to the levels that were present in 1980,* and therefore we cannot expect a full recovery of the ozone layer.”
ct06-Tautologies-Conditional.md&7.11&
ct06-Tautologies-Conditional.md&7.11&\vspace{3ex}
ct06-Tautologies-Conditional.md&7.11&
ct06-Tautologies-Conditional.md&7.11&**Denying** the *antecedent* (invalid).
ct06-Tautologies-Conditional.md&7.11&
ct06-Tautologies-Conditional.md&7.12&## Conditional arguments
ct06-Tautologies-Conditional.md&7.12&
ct06-Tautologies-Conditional.md&7.12&Is this valid?
ct06-Tautologies-Conditional.md&7.12&
ct06-Tautologies-Conditional.md&7.12&“If I could understand the concept of conditional arguments, I would get a passing grade. Now I got a passing grade, so I must have understood the concept of conditional arguments.”
ct06-Tautologies-Conditional.md&7.12&
ct06-Tautologies-Conditional.md&7.12&. . . 
ct06-Tautologies-Conditional.md&7.12&
ct06-Tautologies-Conditional.md&7.12&Affirming the consequent (invalid).
ct06-Tautologies-Conditional.md&7.12&
ct06-Tautologies-Conditional.md&7.13&## Conditional arguments
ct06-Tautologies-Conditional.md&7.13&
ct06-Tautologies-Conditional.md&7.13&Is this valid?
ct06-Tautologies-Conditional.md&7.13&
ct06-Tautologies-Conditional.md&7.13&“I must still be eating too much ice cream,” complained George, “because my waist measurement is the same as it was six months ago, and I know that if I didn’t eat so much ice cream, I would reduce my waist size.”
ct06-Tautologies-Conditional.md&7.13&
ct06-Tautologies-Conditional.md&7.13&\vspace{3ex}
ct06-Tautologies-Conditional.md&7.13&
ct06-Tautologies-Conditional.md&7.13&. . . 
ct06-Tautologies-Conditional.md&7.13&
ct06-Tautologies-Conditional.md&7.13&If I didn’t eat so much ice cream, I would reduce my waist size.  
ct06-Tautologies-Conditional.md&7.13&My waist measurement is the same as it was six months ago.  
ct06-Tautologies-Conditional.md&7.13& ------------------------------------------------------------------------------  
ct06-Tautologies-Conditional.md&7.13&Conclusion: I must still be eating too much ice cream.
ct06-Tautologies-Conditional.md&7.13&
ct06-Tautologies-Conditional.md&7.13&. . . 
ct06-Tautologies-Conditional.md&7.13&
ct06-Tautologies-Conditional.md&7.13&Denying the consequent (valid).
ct06-Tautologies-Conditional.md&7.13&
ct06-Tautologies-Conditional.md&7.14&## Conditional arguments
ct06-Tautologies-Conditional.md&7.14&
ct06-Tautologies-Conditional.md&7.14&If you get hit by a car when you are six then you will die young. But you were not hit by a car when you were six. Thus you will not die young.
ct06-Tautologies-Conditional.md&7.14&
ct06-Tautologies-Conditional.md&7.14&. . .
ct06-Tautologies-Conditional.md&7.14&
ct06-Tautologies-Conditional.md&7.14&p: hit by a car when six  
ct06-Tautologies-Conditional.md&7.14&q: die young  
ct06-Tautologies-Conditional.md&7.14&
ct06-Tautologies-Conditional.md&7.14&\vspace{3ex}
ct06-Tautologies-Conditional.md&7.14&
ct06-Tautologies-Conditional.md&7.14&if p then q  
ct06-Tautologies-Conditional.md&7.14&not p  
ct06-Tautologies-Conditional.md&7.14& ------------------  
ct06-Tautologies-Conditional.md&7.14&therefore not q
ct06-Tautologies-Conditional.md&7.14&
ct06-Tautologies-Conditional.md&7.14&. . . 
ct06-Tautologies-Conditional.md&7.14&
ct06-Tautologies-Conditional.md&7.14&\vspace{3ex}
ct06-Tautologies-Conditional.md&7.14&
ct06-Tautologies-Conditional.md&7.14&Denying the antecedent (*invalid*).
ct06-Tautologies-Conditional.md&7.14&
ct06-Tautologies-Conditional.md&7.14&
ct06-Tautologies-Conditional.md&8.0&# Additional exercises
ct06-Tautologies-Conditional.md&8.0&
ct06-Tautologies-Conditional.md&8.1&## Additional exercises
ct06-Tautologies-Conditional.md&8.1&
ct06-Tautologies-Conditional.md&8.1&Use a truth table to show whether the two statements:
ct06-Tautologies-Conditional.md&8.1&
ct06-Tautologies-Conditional.md&8.1&“If you make coffee, I will drink a cup”
ct06-Tautologies-Conditional.md&8.1&
ct06-Tautologies-Conditional.md&8.1&and
ct06-Tautologies-Conditional.md&8.1&
ct06-Tautologies-Conditional.md&8.1&“I’ll drink a cup or you didn’t make coffee”
ct06-Tautologies-Conditional.md&8.1&
ct06-Tautologies-Conditional.md&8.1&are equivalent!
ct06-Tautologies-Conditional.md&8.1&
ct06-Tautologies-Conditional.md&8.2&## Equivalent?
ct06-Tautologies-Conditional.md&8.2&
ct06-Tautologies-Conditional.md&8.2&“If you make coffee, I will drink a cup”
ct06-Tautologies-Conditional.md&8.2&
ct06-Tautologies-Conditional.md&8.2&\f{M$\to$D}
ct06-Tautologies-Conditional.md&8.2&
ct06-Tautologies-Conditional.md&8.2&“I’ll drink a cup or you didn’t make coffee”
ct06-Tautologies-Conditional.md&8.2&
ct06-Tautologies-Conditional.md&8.2&\f{D$\lor\lnot$M}
ct06-Tautologies-Conditional.md&8.2&
ct06-Tautologies-Conditional.md&8.2&\begin{tabular}{|c|c||c|c|c|}
ct06-Tautologies-Conditional.md&8.2&\hline
ct06-Tautologies-Conditional.md&8.2&$M$ & $D$ & $(M \rightarrow D)$ & $\neg M$ & $(D \vee\neg M)$ \\
ct06-Tautologies-Conditional.md&8.2&\hline
ct06-Tautologies-Conditional.md&8.2&F & F & T & T & T \\
ct06-Tautologies-Conditional.md&8.2&\hline
ct06-Tautologies-Conditional.md&8.2&F & T & T & T & T \\
ct06-Tautologies-Conditional.md&8.2&\hline
ct06-Tautologies-Conditional.md&8.2&T & F & F & F & F \\
ct06-Tautologies-Conditional.md&8.2&\hline
ct06-Tautologies-Conditional.md&8.2&T & T & T & F & T \\
ct06-Tautologies-Conditional.md&8.2&\hline
ct06-Tautologies-Conditional.md&8.2&\end{tabular}
ct06-Tautologies-Conditional.md&8.2&
ct06-Tautologies-Conditional.md&8.3&## Are these equivalent?
ct06-Tautologies-Conditional.md&8.3&
ct06-Tautologies-Conditional.md&8.3&Can I convert:
ct06-Tautologies-Conditional.md&8.3&
ct06-Tautologies-Conditional.md&8.3&“If you don't practice, you'll never be good at the piano”
ct06-Tautologies-Conditional.md&8.3&
ct06-Tautologies-Conditional.md&8.3&into:
ct06-Tautologies-Conditional.md&8.3&
ct06-Tautologies-Conditional.md&8.3& “If you want to be good at the piano, you'll have to practice”?
ct06-Tautologies-Conditional.md&8.3&
ct06-Tautologies-Conditional.md&8.3&*Examine it using a truth table!*
ct06-Tautologies-Conditional.md&8.3&
ct06-Tautologies-Conditional.md&8.4&## Are these really equivalent?
ct06-Tautologies-Conditional.md&8.4&
ct06-Tautologies-Conditional.md&8.4&(p=practice,  g=good at piano)
ct06-Tautologies-Conditional.md&8.4&
ct06-Tautologies-Conditional.md&8.4&- If you don't practice, you'll never be good at the piano: $\lnot$p$\to\lnot$g
ct06-Tautologies-Conditional.md&8.4&- If you want to be good at the piano, you'll have to practice: g$\to$p
ct06-Tautologies-Conditional.md&8.4&
ct06-Tautologies-Conditional.md&8.4&. . .
ct06-Tautologies-Conditional.md&8.4&
ct06-Tautologies-Conditional.md&8.4&![](graphics/08-piano.png)\ 
ct06-Tautologies-Conditional.md&8.4&
ct06-Tautologies-Conditional.md&8.4&
ct06-Tautologies-Conditional.md&8.5&## What did we learn today?
ct06-Tautologies-Conditional.md&8.5&
ct06-Tautologies-Conditional.md&8.5&- Conditional expressions and conditional arguments
ct06-Tautologies-Conditional.md&8.5&- Antecedent and consequent
ct06-Tautologies-Conditional.md&8.5&- Unstated (hidden) conditions
ct06-Tautologies-Conditional.md&8.5&- Valid and invalid argument forms
ct06-Tautologies-Conditional.md&8.5&- Equivalent expressions
ct06-Tautologies-Conditional.md&8.5&
ct06-Tautologies-Conditional.md&8.6&## Any questions?
ct06-Tautologies-Conditional.md&8.6&
ct06-Tautologies-Conditional.md&8.6&![](graphics/questions.jpg)\ 
ct06-Tautologies-Conditional.md&8.6&
ct06-Tautologies-Conditional.md&8.7&## Thank you for your attention!
ct06-Tautologies-Conditional.md&8.7&
ct06-Tautologies-Conditional.md&8.7&Email: <matthias@ln.edu.hk>
ct06-Tautologies-Conditional.md&8.7&
ct06-Tautologies-Conditional.md&8.7&If you have questions, please come to my office hours (see course outline).
ct07-Syllogisms.md&0.1&% Critical Thinking
ct07-Syllogisms.md&0.1&% 7. Syllogisms
ct07-Syllogisms.md&0.1&% A. Matthias
ct07-Syllogisms.md&0.1&
ct07-Syllogisms.md&1.0&# Where we are
ct07-Syllogisms.md&1.0&
ct07-Syllogisms.md&1.1&## What did we learn last time?
ct07-Syllogisms.md&1.1&
ct07-Syllogisms.md&1.1&- Conditional expressions and conditional arguments
ct07-Syllogisms.md&1.1&- Antecedent and consequent
ct07-Syllogisms.md&1.1&- Unstated (hidden) conditions
ct07-Syllogisms.md&1.1&- Valid and invalid argument forms of conditional arguments
ct07-Syllogisms.md&1.1&
ct07-Syllogisms.md&1.2&## What are we going to learn today?
ct07-Syllogisms.md&1.2&
ct07-Syllogisms.md&1.2&- Necessary and sufficient conditions
ct07-Syllogisms.md&1.2&- Categorical syllogisms
ct07-Syllogisms.md&1.2&- What can go wrong with them
ct07-Syllogisms.md&1.2&- How Venn diagrams help us analyze syllogisms
ct07-Syllogisms.md&1.2&
ct07-Syllogisms.md&2.0&# Necessary and sufficient conditions
ct07-Syllogisms.md&2.0&
ct07-Syllogisms.md&2.1&## Necessary and sufficient
ct07-Syllogisms.md&2.1&A necessary condition (\cn{必要條件}) is one which must be true in order for the consequent to happen.
ct07-Syllogisms.md&2.1&
ct07-Syllogisms.md&2.1&But still, even if it is true, the consequent might also need other conditions to be fulfilled in order to happen.
ct07-Syllogisms.md&2.1&
ct07-Syllogisms.md&2.2&## Necessary conditions
ct07-Syllogisms.md&2.2&
ct07-Syllogisms.md&2.2&“If I study, then I will pass my examination.”
ct07-Syllogisms.md&2.2&
ct07-Syllogisms.md&2.2&>- “If I study” is a necessary condition.
ct07-Syllogisms.md&2.2&>- It must be true, otherwise I will not pass my examination.
ct07-Syllogisms.md&2.2&>- But even if I study, I might fail (e.g. because I am sick or drunk at the date of the examination).
ct07-Syllogisms.md&2.2&>- So this condition is *necessary* but not *sufficient.*
ct07-Syllogisms.md&2.2&
ct07-Syllogisms.md&2.3&## Sufficient conditions
ct07-Syllogisms.md&2.3&
ct07-Syllogisms.md&2.3&“If I eat ten cheeseburgers, then I will be sick.”
ct07-Syllogisms.md&2.3&
ct07-Syllogisms.md&2.3&>- “If I eat ten cheeseburgers” is a sufficient condition (\cn{充分條件}) for getting sick.
ct07-Syllogisms.md&2.3&>- If this is true, then the consequence will also be true.
ct07-Syllogisms.md&2.3&>- Nothing else besides this condition is needed to bring about the consequence.
ct07-Syllogisms.md&2.3&
ct07-Syllogisms.md&2.4&## Necessary conditions
ct07-Syllogisms.md&2.4&
ct07-Syllogisms.md&2.4&![](graphics/08-necessary.png)\ 
ct07-Syllogisms.md&2.4&
ct07-Syllogisms.md&2.5&## Sufficient conditions
ct07-Syllogisms.md&2.5&
ct07-Syllogisms.md&2.5&![](graphics/08-sufficient.png)\ 
ct07-Syllogisms.md&2.5&
ct07-Syllogisms.md&2.6&## Necessary and sufficient
ct07-Syllogisms.md&2.6&
ct07-Syllogisms.md&2.6&Look at the result first:
ct07-Syllogisms.md&2.6&
ct07-Syllogisms.md&2.6&>- If you cannot have the result without the condition, then the condition is necessary.
ct07-Syllogisms.md&2.6&>- If the condition *alone* is sure bring about the result, then the condition is sufficient.
ct07-Syllogisms.md&2.6&
ct07-Syllogisms.md&2.7&## Necessary and sufficient
ct07-Syllogisms.md&2.7&
ct07-Syllogisms.md&2.7&We can express this as a conditional statement:
ct07-Syllogisms.md&2.7&
ct07-Syllogisms.md&2.7&>- If A is *necessary* for B, then:
ct07-Syllogisms.md&2.7&>- If not A then not B; or
ct07-Syllogisms.md&2.7&>- if B then A
ct07-Syllogisms.md&2.7&
ct07-Syllogisms.md&2.7&>- If A is *sufficient* for B, then:
ct07-Syllogisms.md&2.7&>- If A then B; or
ct07-Syllogisms.md&2.7&>- if not B then not A
ct07-Syllogisms.md&2.7&
ct07-Syllogisms.md&2.8&## Equivalent?
ct07-Syllogisms.md&2.8&
ct07-Syllogisms.md&2.8&We just said:
ct07-Syllogisms.md&2.8&
ct07-Syllogisms.md&2.8&>- If A is sufficient for B, then:
ct07-Syllogisms.md&2.8&>- If A then B; or
ct07-Syllogisms.md&2.8&>- if not B then not A
ct07-Syllogisms.md&2.8&
ct07-Syllogisms.md&2.8&>- It seems then, that A$\to$B and $\lnot$B$\to\lnot$A are equivalent.
ct07-Syllogisms.md&2.8&>- But is this true? *Show with a truth table that these two expressions are equivalent!*
ct07-Syllogisms.md&2.8&
ct07-Syllogisms.md&2.9&## Now try it yourself!
ct07-Syllogisms.md&2.9&
ct07-Syllogisms.md&2.9&Which are necessary, which are sufficient?
ct07-Syllogisms.md&2.9&
ct07-Syllogisms.md&2.9&1. If you eat a lot of rice, you will not be hungry.
ct07-Syllogisms.md&2.9&2. You have to take a General Education course in order to complete your studies.
ct07-Syllogisms.md&2.9&3. If you walk in the rain without any protection, you will get wet.
ct07-Syllogisms.md&2.9&4. If you print out that email, you can read it at home.
ct07-Syllogisms.md&2.9&5. If you have a tea bag, you can make some tea.
ct07-Syllogisms.md&2.9&
ct07-Syllogisms.md&2.10&## Necessary and sufficient
ct07-Syllogisms.md&2.10&
ct07-Syllogisms.md&2.10&>1. If you eat a lot of rice, you will not be hungry: **sufficient.**
ct07-Syllogisms.md&2.10&>2. You have to take some common core courses in order to complete your studies: **necessary.**
ct07-Syllogisms.md&2.10&>3. If you walk in the rain without any protection, you will get wet: **sufficient.**
ct07-Syllogisms.md&2.10&>4. If you print out that email, you can read it at home: **sufficient.**
ct07-Syllogisms.md&2.10&>5. If you have a tea bag, you can make some tea: **neither necessary nor sufficient**:
ct07-Syllogisms.md&2.10&>      1. I could have tea leaves instead of a tea bag.
ct07-Syllogisms.md&2.10&>      2. I still need hot water.
ct07-Syllogisms.md&2.10&
ct07-Syllogisms.md&2.11&## “If” and “only” (further explanation) (1)
ct07-Syllogisms.md&2.11&
ct07-Syllogisms.md&2.11&>- "Betty will eat the fruit if it is an apple."
ct07-Syllogisms.md&2.11&>- This is equivalent to: "Only if Betty will eat the fruit, is it an apple."
ct07-Syllogisms.md&2.11&>- Or: "If the fruit is an apple, then Betty will eat it."
ct07-Syllogisms.md&2.11&
ct07-Syllogisms.md&2.11&. . . 
ct07-Syllogisms.md&2.11&
ct07-Syllogisms.md&2.11&>- This states simply that Betty will eat fruits that are apples.
ct07-Syllogisms.md&2.11&>- It does not, however, exclude the possibility that Betty might also eat bananas or other types of fruit.
ct07-Syllogisms.md&2.11&>- All that is known for certain is that she will eat any and all apples that she happens upon.
ct07-Syllogisms.md&2.11&>- That the fruit is an apple is a sufficient condition for Betty to eat the fruit.
ct07-Syllogisms.md&2.11&
ct07-Syllogisms.md&2.12&## “If” and “only” (2)
ct07-Syllogisms.md&2.12&
ct07-Syllogisms.md&2.12&>- "Betty will eat the fruit *only* if it is an apple."
ct07-Syllogisms.md&2.12&>- This is equivalent to: "If Betty will eat the fruit, then it is an apple."
ct07-Syllogisms.md&2.12&>- Or: "Betty will eat the fruit $\to$ fruit is an apple."
ct07-Syllogisms.md&2.12&
ct07-Syllogisms.md&2.12&. . .
ct07-Syllogisms.md&2.12&
ct07-Syllogisms.md&2.12&>- This states that the only fruit Betty will eat is an apple.
ct07-Syllogisms.md&2.12&>- It does not, however, exclude the possibility that Betty will refuse an apple if it is made available, in contrast with (1), which requires Betty to eat any available apple.
ct07-Syllogisms.md&2.12&>- In this case, that a given fruit is an apple is a *necessary* condition for Betty to be eating it.
ct07-Syllogisms.md&2.12&>- It is not a *sufficient* condition since Betty might not eat all the apples she is given.
ct07-Syllogisms.md&2.12&
ct07-Syllogisms.md&2.13&## “If” and “only” (3)
ct07-Syllogisms.md&2.13&
ct07-Syllogisms.md&2.13&"Betty will eat the fruit *if and only if* it is an apple."
ct07-Syllogisms.md&2.13&
ct07-Syllogisms.md&2.13&>- This is equivalent to: "Betty will eat the fruit $\leftrightarrow$ fruit is an apple."
ct07-Syllogisms.md&2.13&>- This statement makes it clear that Betty will eat all and only those fruits that are apples.
ct07-Syllogisms.md&2.13&>- She will not leave any apple uneaten, and she will not eat any other type of fruit.
ct07-Syllogisms.md&2.13&>- That a given fruit is an apple is *both a necessary and a sufficient condition* for Betty to eat the fruit.^[From: https://en.wikipedia.org/wiki/If_and_only_if]
ct07-Syllogisms.md&2.13&
ct07-Syllogisms.md&3.0&# Possibility and necessity
ct07-Syllogisms.md&3.0&
ct07-Syllogisms.md&3.1&## How is it possible? (1)
ct07-Syllogisms.md&3.1&
ct07-Syllogisms.md&3.1&Compare different types of impossibility here^[from: https://philosophy.hku.hk/think/meaning/possibility.php]:
ct07-Syllogisms.md&3.1&
ct07-Syllogisms.md&3.1&- It is impossible to be a tall man without being tall.
ct07-Syllogisms.md&3.1&- It is impossible to dissolve gold in pure water.
ct07-Syllogisms.md&3.1&- It is impossible to travel from Hong Kong to New York in less than ten minutes.
ct07-Syllogisms.md&3.1&- It is impossible to visit the doctor without an appointment.
ct07-Syllogisms.md&3.1&
ct07-Syllogisms.md&3.2&## How is it possible? (2)
ct07-Syllogisms.md&3.2&
ct07-Syllogisms.md&3.2&>- It is LOGICALLY impossible to be tall without being tall. There is no possible universe in which this might be the case.
ct07-Syllogisms.md&3.2&>- It is EMPIRICALLY (physically, causally) impossible to dissolve gold in water. But there might be another universe, with different physics, in which this is possible.
ct07-Syllogisms.md&3.2&>- To travel very fast is TECHNOLOGICALLY impossible today, but it might become possible in two hundred years. It is neither logically nor empirically (physically) impossible.
ct07-Syllogisms.md&3.2&>- The last example is possible in all the above senses. It is only LEGALLY impossible (=not permitted), but one could certainly force it to make it happen.
ct07-Syllogisms.md&3.2&
ct07-Syllogisms.md&3.3&## Different kinds of necessity
ct07-Syllogisms.md&3.3&
ct07-Syllogisms.md&3.3&Necessary/sufficient also mean different things depending on what kind of possibility we have in mind (examples from HKU Think):
ct07-Syllogisms.md&3.3&
ct07-Syllogisms.md&3.3&>- Having four sides is *logically necessary* for being a square.
ct07-Syllogisms.md&3.3&>- Being a father is *logically sufficient* for being a parent.
ct07-Syllogisms.md&3.3&>- The presence of oxygen is *causally necessary* for the proper functioning of the brain.
ct07-Syllogisms.md&3.3&>- Passing current through a resistor is *causally sufficient* for the generation of heat.
ct07-Syllogisms.md&3.3&>- Being an adult of over 18 years old is *legally necessary* for having the right to vote.
ct07-Syllogisms.md&3.3&>- The presence of a witness is *legally necessary* for a valid marriage.
ct07-Syllogisms.md&3.3&>- Other types of necessity are also possible, for example, necessity from morality, politeness, prudence and so on.
ct07-Syllogisms.md&3.3&
ct07-Syllogisms.md&3.4&## Which kind of necessity/sufficiency to you see here?
ct07-Syllogisms.md&3.4&
ct07-Syllogisms.md&3.4&>- A recent photograph is _____ _____ for a new ID card.
ct07-Syllogisms.md&3.4&>- Eating twenty mooncakes is _____ sufficient for me to be sick.
ct07-Syllogisms.md&3.4&>- Having committed a serious crime is _____ necessary for going to jail.
ct07-Syllogisms.md&3.4&>- Owning a cat is _____ _____ for being a pet owner.
ct07-Syllogisms.md&3.4&>- Fuel is _____ necessary for my car to drive.
ct07-Syllogisms.md&3.4&>- Being a father or a mother is _____ _____ for being a parent.
ct07-Syllogisms.md&3.4&
ct07-Syllogisms.md&3.4&
ct07-Syllogisms.md&3.5&## Which kind of necessity/sufficiency to you see here?
ct07-Syllogisms.md&3.5&
ct07-Syllogisms.md&3.5&>- A recent photograph is legally necessary for a new ID card.
ct07-Syllogisms.md&3.5&>- Eating twenty mooncakes is causally sufficient for me to be sick.
ct07-Syllogisms.md&3.5&>- Having committed a serious crime is legally necessary for going to jail.
ct07-Syllogisms.md&3.5&>- Owning a cat is logically sufficient for being a pet owner.
ct07-Syllogisms.md&3.5&>- Fuel is causally necessary for my car to drive.
ct07-Syllogisms.md&3.5&>- Being a father or a mother is logically necessary and sufficient for being a parent. (Conditions can be both!)
ct07-Syllogisms.md&3.5&
ct07-Syllogisms.md&3.5&
ct07-Syllogisms.md&4.0&# Categorical syllogisms
ct07-Syllogisms.md&4.0&
ct07-Syllogisms.md&4.1&## Categorical syllogisms
ct07-Syllogisms.md&4.1&
ct07-Syllogisms.md&4.1&*Why do chickens have feathers?*
ct07-Syllogisms.md&4.1&
ct07-Syllogisms.md&4.1&. . . 
ct07-Syllogisms.md&4.1&
ct07-Syllogisms.md&4.1&Because they are birds and all birds have feathers.
ct07-Syllogisms.md&4.1&
ct07-Syllogisms.md&4.1&\vspace{3ex}
ct07-Syllogisms.md&4.1&
ct07-Syllogisms.md&4.1&. . . 
ct07-Syllogisms.md&4.1&
ct07-Syllogisms.md&4.1&Premise 1: All birds have feathers   
ct07-Syllogisms.md&4.1&Premise 2: Chickens are birds   
ct07-Syllogisms.md&4.1& --------------------------------------------  
ct07-Syllogisms.md&4.1&Conclusion: Chickens have feathers.
ct07-Syllogisms.md&4.1&
ct07-Syllogisms.md&4.2&## Categorical syllogisms
ct07-Syllogisms.md&4.2&
ct07-Syllogisms.md&4.2&It doesn't help very much to represent this symbolically using propositional calculus:
ct07-Syllogisms.md&4.2&
ct07-Syllogisms.md&4.2&\vspace{3ex}
ct07-Syllogisms.md&4.2&
ct07-Syllogisms.md&4.2&All birds have feathers = p  
ct07-Syllogisms.md&4.2&Chickens are birds = q  
ct07-Syllogisms.md&4.2& ---------------------------------------------  
ct07-Syllogisms.md&4.2&Therefore, chickens have feathers = r
ct07-Syllogisms.md&4.2&
ct07-Syllogisms.md&4.2& p  
ct07-Syllogisms.md&4.2& q  
ct07-Syllogisms.md&4.2& ---  
ct07-Syllogisms.md&4.2& r
ct07-Syllogisms.md&4.2&
ct07-Syllogisms.md&4.3&## Categorical syllogisms
ct07-Syllogisms.md&4.3&
ct07-Syllogisms.md&4.3& Every lion is a mammal.  
ct07-Syllogisms.md&4.3& No fish is  a mammal.  
ct07-Syllogisms.md&4.3& -----------------------------  
ct07-Syllogisms.md&4.3& No fish is a lion.
ct07-Syllogisms.md&4.3&
ct07-Syllogisms.md&4.3&\vspace{3ex}
ct07-Syllogisms.md&4.3&
ct07-Syllogisms.md&4.3&. . .
ct07-Syllogisms.md&4.3&
ct07-Syllogisms.md&4.3& All students have a student ID.  
ct07-Syllogisms.md&4.3& Some people have no student ID.  
ct07-Syllogisms.md&4.3& -----------------------------------------  
ct07-Syllogisms.md&4.3& Some people are not students.
ct07-Syllogisms.md&4.3& 
ct07-Syllogisms.md&4.4&## Categorical syllogisms
ct07-Syllogisms.md&4.4&
ct07-Syllogisms.md&4.4&In categorical syllogisms we talk about the *categories* things belong to.
ct07-Syllogisms.md&4.4&
ct07-Syllogisms.md&4.4&We can recognize them by the use of the words “every”, “all”, “some”.
ct07-Syllogisms.md&4.4&
ct07-Syllogisms.md&4.5&## Premises and conclusion
ct07-Syllogisms.md&4.5&
ct07-Syllogisms.md&4.5&Categorical syllogisms have two premises and a conclusion:
ct07-Syllogisms.md&4.5&
ct07-Syllogisms.md&4.5&\vspace{3ex}
ct07-Syllogisms.md&4.5&
ct07-Syllogisms.md&4.5&(P1) Every lion is a mammal.  
ct07-Syllogisms.md&4.5&(P2) No fish is a mammal.  
ct07-Syllogisms.md&4.5& -----------------------------------  
ct07-Syllogisms.md&4.5&(C) No fish is a lion.
ct07-Syllogisms.md&4.5&
ct07-Syllogisms.md&4.6&## Middle term
ct07-Syllogisms.md&4.6&
ct07-Syllogisms.md&4.6&The two premises are connected by a *middle term,* which appears in both, but not in the conclusion:
ct07-Syllogisms.md&4.6&
ct07-Syllogisms.md&4.6&\vspace{3ex}
ct07-Syllogisms.md&4.6&
ct07-Syllogisms.md&4.6&All students have a *student ID.*  
ct07-Syllogisms.md&4.6&Some people have no *student ID.*  
ct07-Syllogisms.md&4.6& -----------------------------------------  
ct07-Syllogisms.md&4.6&Some people are not students.
ct07-Syllogisms.md&4.6&
ct07-Syllogisms.md&4.6&\vspace{3ex}
ct07-Syllogisms.md&4.6&
ct07-Syllogisms.md&4.6&. . . 
ct07-Syllogisms.md&4.6&
ct07-Syllogisms.md&4.6&Every lion is a *mammal.*  
ct07-Syllogisms.md&4.6&No fish is a *mammal.*  
ct07-Syllogisms.md&4.6& ------------------------------  
ct07-Syllogisms.md&4.6&No fish is a lion.
ct07-Syllogisms.md&4.6&
ct07-Syllogisms.md&4.7&## Major and minor term
ct07-Syllogisms.md&4.7&
ct07-Syllogisms.md&4.7&The conclusion has a *subject* and a **predicate** ( = a property which applies to the subject).
ct07-Syllogisms.md&4.7&
ct07-Syllogisms.md&4.7&\vspace{3ex}
ct07-Syllogisms.md&4.7&
ct07-Syllogisms.md&4.7&All students have a student ID.  
ct07-Syllogisms.md&4.7&Some people have no student ID.  
ct07-Syllogisms.md&4.7& -----------------------------------------  
ct07-Syllogisms.md&4.7&*Some people* are **not students.**
ct07-Syllogisms.md&4.7&
ct07-Syllogisms.md&4.7&\vspace{3ex}
ct07-Syllogisms.md&4.7&
ct07-Syllogisms.md&4.7&. . . 
ct07-Syllogisms.md&4.7&
ct07-Syllogisms.md&4.7&Every lion is a mammal.  
ct07-Syllogisms.md&4.7&No fish is a mammal.  
ct07-Syllogisms.md&4.7& ------------------------------  
ct07-Syllogisms.md&4.7&*No fish* is **a lion.**
ct07-Syllogisms.md&4.7&
ct07-Syllogisms.md&4.8&## Major and minor term
ct07-Syllogisms.md&4.8&
ct07-Syllogisms.md&4.8&The conclusion has a subject and a predicate (=a property which applies to the subject).
ct07-Syllogisms.md&4.8&
ct07-Syllogisms.md&4.8&>- The subject of the conclusion is called the *minor term* of the syllogism.
ct07-Syllogisms.md&4.8&>- The predicate of the conclusion is called the **major term.**
ct07-Syllogisms.md&4.8&
ct07-Syllogisms.md&4.8&. . .
ct07-Syllogisms.md&4.8&
ct07-Syllogisms.md&4.8&*No fish* is **a lion.**
ct07-Syllogisms.md&4.8&*Some people* are **not students.**
ct07-Syllogisms.md&4.8&
ct07-Syllogisms.md&4.9&## Find the major, minor and middle terms!
ct07-Syllogisms.md&4.9&
ct07-Syllogisms.md&4.9&P1: Every dog is a mammal.  
ct07-Syllogisms.md&4.9&P2: Some pets are dogs.  
ct07-Syllogisms.md&4.9& ------------------------------------  
ct07-Syllogisms.md&4.9&C:  Some pets are mammals.  
ct07-Syllogisms.md&4.9&
ct07-Syllogisms.md&4.9&\vspace{3ex}
ct07-Syllogisms.md&4.9&
ct07-Syllogisms.md&4.9&P1: Some cakes are sweet.  
ct07-Syllogisms.md&4.9&P2: Some presents are cakes.  
ct07-Syllogisms.md&4.9& ------------------------------------  
ct07-Syllogisms.md&4.9&C: Some presents are sweet.  
ct07-Syllogisms.md&4.9&
ct07-Syllogisms.md&4.10&## Find the major, minor and middle terms!
ct07-Syllogisms.md&4.10&
ct07-Syllogisms.md&4.10&**Major**, *minor*, and \underline{middle} terms:
ct07-Syllogisms.md&4.10&
ct07-Syllogisms.md&4.10&\vspace{3ex}
ct07-Syllogisms.md&4.10&
ct07-Syllogisms.md&4.10&P1: Every \underline{dog} is a **mammal**.  
ct07-Syllogisms.md&4.10&P2: Some *pets* are \underline{dogs}.  
ct07-Syllogisms.md&4.10& ------------------------------------  
ct07-Syllogisms.md&4.10&C:  Some *pets* are **mammals**.  
ct07-Syllogisms.md&4.10&
ct07-Syllogisms.md&4.10&\vspace{3ex}
ct07-Syllogisms.md&4.10&
ct07-Syllogisms.md&4.10&P1: Some \underline{cakes} are **sweet**.  
ct07-Syllogisms.md&4.10&P2: Some *presents* are \underline{cakes}.  
ct07-Syllogisms.md&4.10& ------------------------------------  
ct07-Syllogisms.md&4.10&C: Some *presents* are **sweet**.  
ct07-Syllogisms.md&4.10&
ct07-Syllogisms.md&5.0&# Venn diagrams
ct07-Syllogisms.md&5.0&
ct07-Syllogisms.md&5.1&## Venn diagrams
ct07-Syllogisms.md&5.1&
ct07-Syllogisms.md&5.1&Another way of testing the validity of syllogisms are the so-called Venn diagrams.
ct07-Syllogisms.md&5.1&
ct07-Syllogisms.md&5.1&A Venn diagram represents a syllogism graphically.
ct07-Syllogisms.md&5.1&
ct07-Syllogisms.md&5.2&## Basic Venn diagram
ct07-Syllogisms.md&5.2&
ct07-Syllogisms.md&5.2&\begin{venndiagram3sets}[labelA={Subject}, labelB={Pred.}, labelC={Middle}]
ct07-Syllogisms.md&5.2&\end{venndiagram3sets}
ct07-Syllogisms.md&5.2&
ct07-Syllogisms.md&5.2&We always begin with a diagram like this. Each circle represents one of: subject, predicate, middle term.
ct07-Syllogisms.md&5.2&
ct07-Syllogisms.md&5.3&## Basic Venn diagram
ct07-Syllogisms.md&5.3&
ct07-Syllogisms.md&5.3&\begin{venndiagram3sets}[labelA={S}, labelB={P}, labelC={M}, labelOnlyBC={x}]
ct07-Syllogisms.md&5.3&\fillOnlyA
ct07-Syllogisms.md&5.3&\end{venndiagram3sets}
ct07-Syllogisms.md&5.3&
ct07-Syllogisms.md&5.3&>- If a circle is empty, we don’t know anything about it.
ct07-Syllogisms.md&5.3&>- If it is shaded, then there are *no* elements in it ("S" in the picture).
ct07-Syllogisms.md&5.3&>- If it contains an X, then there is at least one thing in that category.
ct07-Syllogisms.md&5.3&
ct07-Syllogisms.md&5.4&## “All S are P”
ct07-Syllogisms.md&5.4&
ct07-Syllogisms.md&5.4&Example: All lions are mammals.
ct07-Syllogisms.md&5.4&
ct07-Syllogisms.md&5.4&\begin{venndiagram3sets}[labelA={S}, labelB={P}, labelC={M}]
ct07-Syllogisms.md&5.4&\fillANotB
ct07-Syllogisms.md&5.4&\end{venndiagram3sets}
ct07-Syllogisms.md&5.4&
ct07-Syllogisms.md&5.5&## “Some S are P”
ct07-Syllogisms.md&5.5&
ct07-Syllogisms.md&5.5&Example: Some cats are black.
ct07-Syllogisms.md&5.5&
ct07-Syllogisms.md&5.5&![](graphics/09-venn-4.png)\ 
ct07-Syllogisms.md&5.5&
ct07-Syllogisms.md&5.5&Since we don’t know where the X goes (area 1 or 2), we can just put it onto the line. Often, you can avoid this by shading the “all” areas first!
ct07-Syllogisms.md&5.5&
ct07-Syllogisms.md&5.6&## “Some S are not P”
ct07-Syllogisms.md&5.6&
ct07-Syllogisms.md&5.6&Example: Some cats are not black.
ct07-Syllogisms.md&5.6&
ct07-Syllogisms.md&5.6&![](graphics/09-venn-5.png)\ 
ct07-Syllogisms.md&5.6&
ct07-Syllogisms.md&5.6&Again: by putting the “X” onto the line, you can avoid the need for two “X.”
ct07-Syllogisms.md&5.6&
ct07-Syllogisms.md&5.7&## “No S is P”
ct07-Syllogisms.md&5.7&
ct07-Syllogisms.md&5.7&Example: No taxi drivers are bus drivers.
ct07-Syllogisms.md&5.7&
ct07-Syllogisms.md&5.7&\begin{venndiagram3sets}[labelA={S}, labelB={P}, labelC={M}]
ct07-Syllogisms.md&5.7&\fillACapB
ct07-Syllogisms.md&5.7&\end{venndiagram3sets}
ct07-Syllogisms.md&5.7&
ct07-Syllogisms.md&5.7&
ct07-Syllogisms.md&5.8&## Venn diagram principles
ct07-Syllogisms.md&5.8&
ct07-Syllogisms.md&5.8&1. “All/no” premises result in shaded areas.
ct07-Syllogisms.md&5.8&2. “Some” premises result in “x”.
ct07-Syllogisms.md&5.8&3. You expect one shading or “x” per premise.
ct07-Syllogisms.md&5.8&4. Therefore, an argument cannot possibly be valid if it has only “all” premises but a “some” conclusion: where would the “x” for the conclusion come from?
ct07-Syllogisms.md&5.8&5. You don’t draw the conclusion. You just check for it after you have finished drawing the premises.
ct07-Syllogisms.md&5.8&6. Remember: You shade what does *not* have any elements in it!
ct07-Syllogisms.md&5.8&7. Don’t forget to clearly answer the question: is the argument valid, and why? (Or why not?). A diagram alone is not a sufficient answer.
ct07-Syllogisms.md&5.8&
ct07-Syllogisms.md&5.8&
ct07-Syllogisms.md&5.9&## Venn diagram numbering
ct07-Syllogisms.md&5.9&
ct07-Syllogisms.md&5.9&\begin{venndiagram3sets}[labelA={S}, labelB={P}, labelC={M}, labelOnlyA={1}, labelOnlyAB={2}, labelOnlyB={3}, labelOnlyAC={4}, labelABC={5}, labelOnlyBC={6}, labelOnlyC={7}]
ct07-Syllogisms.md&5.9&\end{venndiagram3sets}
ct07-Syllogisms.md&5.9&
ct07-Syllogisms.md&5.9&A good example answer for an exam question would be (if you cannot submit the drawing itself, e.g. online): “Areas 4 and 5 are shaded. There is an x in 6. For the conclusion to be valid, I would expect an x in 6. Since it is there, the argument is valid.” (Or something like that).
ct07-Syllogisms.md&5.9&
ct07-Syllogisms.md&5.9&
ct07-Syllogisms.md&5.10&## Prove that this is correct
ct07-Syllogisms.md&5.10&
ct07-Syllogisms.md&5.10&All lions (M) are mammals (P)  
ct07-Syllogisms.md&5.10&Some rare animals (S) are lions (M)  
ct07-Syllogisms.md&5.10& -----------------------------------------------  
ct07-Syllogisms.md&5.10&Some rare animals (S) are mammals (P)  
ct07-Syllogisms.md&5.10&
ct07-Syllogisms.md&5.10&. . . 
ct07-Syllogisms.md&5.10&
ct07-Syllogisms.md&5.10&>- In order to prove this with a Venn diagram, we would first shade the circles according to the premises.
ct07-Syllogisms.md&5.10&>- Then we would see whether the resulting shading is consistent with the conclusion.
ct07-Syllogisms.md&5.10&>- *Always shade “all” sentences first!*
ct07-Syllogisms.md&5.10&
ct07-Syllogisms.md&5.10&
ct07-Syllogisms.md&5.11&## Prove that this is correct
ct07-Syllogisms.md&5.11&
ct07-Syllogisms.md&5.11&\begin{venndiagram3sets}[labelA={S}, labelB={P}, labelC={M}]
ct07-Syllogisms.md&5.11&\end{venndiagram3sets}
ct07-Syllogisms.md&5.11&
ct07-Syllogisms.md&5.11&All lions (M) are mammals (P)  
ct07-Syllogisms.md&5.11&Some rare animals (S) are lions (M)  
ct07-Syllogisms.md&5.11& -----------------------------------------------  
ct07-Syllogisms.md&5.11&Some rare animals (S) are mammals (P)  
ct07-Syllogisms.md&5.11&
ct07-Syllogisms.md&5.12&## Prove that this is correct
ct07-Syllogisms.md&5.12&
ct07-Syllogisms.md&5.12&\begin{venndiagram3sets}[labelA={S}, labelB={P}, labelC={M}]
ct07-Syllogisms.md&5.12&\fillCNotB
ct07-Syllogisms.md&5.12&\end{venndiagram3sets}
ct07-Syllogisms.md&5.12&
ct07-Syllogisms.md&5.12&**All lions (M) are mammals (P)**  
ct07-Syllogisms.md&5.12&Some rare animals (S) are lions (M)  
ct07-Syllogisms.md&5.12& -----------------------------------------------  
ct07-Syllogisms.md&5.12&Some rare animals (S) are mammals (P)  
ct07-Syllogisms.md&5.12&
ct07-Syllogisms.md&5.13&## Prove that this is correct
ct07-Syllogisms.md&5.13&
ct07-Syllogisms.md&5.13&\begin{venndiagram3sets}[labelA={S}, labelB={P}, labelC={M},labelABC={x}]
ct07-Syllogisms.md&5.13&\fillCNotB
ct07-Syllogisms.md&5.13&\end{venndiagram3sets}
ct07-Syllogisms.md&5.13&
ct07-Syllogisms.md&5.13&All lions (M) are mammals (P)  
ct07-Syllogisms.md&5.13&**Some rare animals (S) are lions (M)**  
ct07-Syllogisms.md&5.13& -----------------------------------------------  
ct07-Syllogisms.md&5.13&Some rare animals (S) are mammals (P)  
ct07-Syllogisms.md&5.13&
ct07-Syllogisms.md&5.14&## Prove that this is correct
ct07-Syllogisms.md&5.14&
ct07-Syllogisms.md&5.14&\begin{venndiagram3sets}[labelA={S}, labelB={P}, labelC={M},labelABC={x}]
ct07-Syllogisms.md&5.14&\fillCNotB
ct07-Syllogisms.md&5.14&\setpostvennhook{
ct07-Syllogisms.md&5.14&\draw[<-] (labelABC) -- ++(0:3cm)
ct07-Syllogisms.md&5.14&node[right,text width=4cm,align=flush left]
ct07-Syllogisms.md&5.14&{This area is the solution, and it does have an x (something inside) = valid!};}
ct07-Syllogisms.md&5.14&\end{venndiagram3sets}
ct07-Syllogisms.md&5.14&
ct07-Syllogisms.md&5.14&All lions (M) are mammals (P)  
ct07-Syllogisms.md&5.14&Some rare animals (S) are lions (M)  
ct07-Syllogisms.md&5.14& -----------------------------------------------  
ct07-Syllogisms.md&5.14&**Some rare animals (S) are mammals (P)**  
ct07-Syllogisms.md&5.14&
ct07-Syllogisms.md&5.15&## No do it yourself!
ct07-Syllogisms.md&5.15&
ct07-Syllogisms.md&5.15&Is this correct?
ct07-Syllogisms.md&5.15&
ct07-Syllogisms.md&5.15&\vspace{3ex}
ct07-Syllogisms.md&5.15&
ct07-Syllogisms.md&5.15&No fish is a mammal  
ct07-Syllogisms.md&5.15&Some mammals are animals  
ct07-Syllogisms.md&5.15& -----------------------------------------  
ct07-Syllogisms.md&5.15&Some animals are not fish  
ct07-Syllogisms.md&5.15&
ct07-Syllogisms.md&5.16&## Correct?
ct07-Syllogisms.md&5.16&
ct07-Syllogisms.md&5.16&\begin{venndiagram3sets}[labelA={S}, labelB={P}, labelC={M}, labelOnlyAC={x}]
ct07-Syllogisms.md&5.16&\fillBCapC
ct07-Syllogisms.md&5.16&\setpostvennhook{
ct07-Syllogisms.md&5.16&\draw[<-] (labelOnlyAC) -- ++(0:3.5cm)
ct07-Syllogisms.md&5.16&node[right,text width=4cm,align=flush left]
ct07-Syllogisms.md&5.16&{This area is the solution, and it does have an x (something inside) = valid!};}
ct07-Syllogisms.md&5.16&\end{venndiagram3sets}
ct07-Syllogisms.md&5.16&
ct07-Syllogisms.md&5.16&No fish (P) is a mammal (M)  
ct07-Syllogisms.md&5.16&Some mammals (M) are animals (S)  
ct07-Syllogisms.md&5.16& -----------------------------------------  
ct07-Syllogisms.md&5.16&Some animals (S) are not fish (P)  
ct07-Syllogisms.md&5.16&
ct07-Syllogisms.md&5.16&- First premise: shaded area.
ct07-Syllogisms.md&5.16&- Second premise: "x."
ct07-Syllogisms.md&5.16&
ct07-Syllogisms.md&5.17&## Why is the left diagram correct and not the right?
ct07-Syllogisms.md&5.17&
ct07-Syllogisms.md&5.17&![](graphics/09-venn-11.png)\ 
ct07-Syllogisms.md&5.17&
ct07-Syllogisms.md&5.17&"Some animals (S) are not fish (P)."
ct07-Syllogisms.md&5.17&
ct07-Syllogisms.md&5.17&Because we know that there are elements where the “X” is, but we don’t know anything about the red area on the right outside of the area with the “X”. So we cannot assume that there will be things in that area.
ct07-Syllogisms.md&5.17&
ct07-Syllogisms.md&5.18&## Venn diagrams
ct07-Syllogisms.md&5.18&
ct07-Syllogisms.md&5.18&Use a Venn diagram to show that this syllogism is *incorrect!*
ct07-Syllogisms.md&5.18&
ct07-Syllogisms.md&5.18&\vspace{3ex}
ct07-Syllogisms.md&5.18&
ct07-Syllogisms.md&5.18&All cats are animals  
ct07-Syllogisms.md&5.18&Some cats are pets  
ct07-Syllogisms.md&5.18& -----------------------  
ct07-Syllogisms.md&5.18&All animals are pets
ct07-Syllogisms.md&5.18&
ct07-Syllogisms.md&5.19&## Invalid
ct07-Syllogisms.md&5.19&
ct07-Syllogisms.md&5.19&\begin{venndiagram3sets}[labelA={S}, labelB={P}, labelC={M},labelABC={x}]
ct07-Syllogisms.md&5.19&\fillCNotA
ct07-Syllogisms.md&5.19&\setpostvennhook{
ct07-Syllogisms.md&5.19&\draw[<-] (labelOnlyA) -- ++(0:3.5cm)
ct07-Syllogisms.md&5.19&node[right,text width=4cm,align=flush left]
ct07-Syllogisms.md&5.19&{If the syllogism was correct, this area should be shaded!};}
ct07-Syllogisms.md&5.19&\end{venndiagram3sets}
ct07-Syllogisms.md&5.19&
ct07-Syllogisms.md&5.19&All cats (M) are animals (S) [shaded area]  
ct07-Syllogisms.md&5.19&Some cats (M) are pets (P) ['x']  
ct07-Syllogisms.md&5.19& --------------------------------------------------  
ct07-Syllogisms.md&5.19&All animals (S) are pets (P)
ct07-Syllogisms.md&5.19&
ct07-Syllogisms.md&5.19&*The conclusion does not follow from the premises.*
ct07-Syllogisms.md&5.19&
ct07-Syllogisms.md&5.20&## Venn diagrams for sets
ct07-Syllogisms.md&5.20&
ct07-Syllogisms.md&5.20&>- Venn diagrams are not only useful for syllogisms.
ct07-Syllogisms.md&5.20&>- They can also be used to solve mathematical problems involving sets.
ct07-Syllogisms.md&5.20&
ct07-Syllogisms.md&5.20&. . .
ct07-Syllogisms.md&5.20&
ct07-Syllogisms.md&5.20&A teacher is planning schedules for 30 students.  Sixteen students say they want to take French, 16 want to take Spanish, and 11 want to take Latin.  Five say they want to take both French and Latin, and of these, 3 wanted to take Spanish as well.  Five want only Latin, and 8 want only Spanish.  How many students want French only?
ct07-Syllogisms.md&5.20&
ct07-Syllogisms.md&5.21&## Solution
ct07-Syllogisms.md&5.21&
ct07-Syllogisms.md&5.21&\begin{venndiagram3sets}[labelA={French}, labelB={Spanish}, labelC={Latin}]
ct07-Syllogisms.md&5.21&\end{venndiagram3sets}
ct07-Syllogisms.md&5.21&
ct07-Syllogisms.md&5.21&A teacher is planning schedules for 30 students.  Sixteen students say they want to take French, 16 want to take Spanish, and 11 want to take Latin.  Five say they want to take both French and Latin, and of these, 3 wanted to take Spanish as well.  Five want only Latin, and 8 want only Spanish.  How many students want French only?
ct07-Syllogisms.md&5.21&
ct07-Syllogisms.md&5.22&## Solution
ct07-Syllogisms.md&5.22&
ct07-Syllogisms.md&5.22&\begin{venndiagram3sets}[labelA={French}, labelB={Spanish}, labelC={Latin}, labelABC={3}, labelOnlyAC={2}]
ct07-Syllogisms.md&5.22&\end{venndiagram3sets}
ct07-Syllogisms.md&5.22&
ct07-Syllogisms.md&5.22&A teacher is planning schedules for 30 students.  Sixteen students say they want to take French, 16 want to take Spanish, and 11 want to take Latin.  **Five say they want to take both French and Latin, and of these, 3 wanted to take Spanish as well.**  Five want only Latin, and 8 want only Spanish.  How many students want French only?
ct07-Syllogisms.md&5.22&
ct07-Syllogisms.md&5.23&## Solution
ct07-Syllogisms.md&5.23&
ct07-Syllogisms.md&5.23&\begin{venndiagram3sets}[labelA={French}, labelB={Spanish}, labelC={Latin}, labelABC={3}, labelOnlyAC={2}, labelOnlyC={5}]
ct07-Syllogisms.md&5.23&\end{venndiagram3sets}
ct07-Syllogisms.md&5.23&
ct07-Syllogisms.md&5.23&A teacher is planning schedules for 30 students.  Sixteen students say they want to take French, 16 want to take Spanish, and 11 want to take Latin.  Five say they want to take both French and Latin, and of these, 3 wanted to take Spanish as well.  **Five want only Latin,** and 8 want only Spanish.  How many students want French only?
ct07-Syllogisms.md&5.23&
ct07-Syllogisms.md&5.24&## Solution
ct07-Syllogisms.md&5.24&
ct07-Syllogisms.md&5.24&\begin{venndiagram3sets}[labelA={French}, labelB={Spanish}, labelC={Latin}, labelABC={3}, labelOnlyAC={2}, labelOnlyC={5},  labelOnlyB={8}]
ct07-Syllogisms.md&5.24&\end{venndiagram3sets}
ct07-Syllogisms.md&5.24&
ct07-Syllogisms.md&5.24&A teacher is planning schedules for 30 students.  Sixteen students say they want to take French, 16 want to take Spanish, and 11 want to take Latin.  Five say they want to take both French and Latin, and of these, 3 wanted to take Spanish as well.  Five want only Latin, and **8 want only Spanish.**  How many students want French only?
ct07-Syllogisms.md&5.24&
ct07-Syllogisms.md&5.25&## Solution
ct07-Syllogisms.md&5.25&
ct07-Syllogisms.md&5.25&\begin{venndiagram3sets}[labelA={French}, labelB={Spanish}, labelC={Latin}, labelABC={3}, labelOnlyAC={2}, labelOnlyC={5},  labelOnlyB={8}, labelOnlyBC={1}]
ct07-Syllogisms.md&5.25&\setpostvennhook{
ct07-Syllogisms.md&5.25&\draw[<-] (labelOnlyBC) -- ++(0:2.5cm)
ct07-Syllogisms.md&5.25&node[right,text width=4cm,align=flush left]
ct07-Syllogisms.md&5.25&{All Latin=11, we already have 5+3+2=10, missing 1.};}\end{venndiagram3sets}
ct07-Syllogisms.md&5.25&
ct07-Syllogisms.md&5.25&A teacher is planning schedules for 30 students.  Sixteen students say they want to take French, 16 want to take Spanish, and **11 want to take Latin.**  Five say they want to take both French and Latin, and of these, 3 wanted to take Spanish as well.  Five want only Latin, and 8 want only Spanish.  How many students want French only?
ct07-Syllogisms.md&5.25&
ct07-Syllogisms.md&5.26&## Solution
ct07-Syllogisms.md&5.26&
ct07-Syllogisms.md&5.26&\begin{venndiagram3sets}[labelA={French}, labelB={Spanish}, labelC={Latin}, labelABC={3}, labelOnlyAC={2}, labelOnlyC={5},  labelOnlyB={8}, labelOnlyBC={1}, labelOnlyAB={4}]
ct07-Syllogisms.md&5.26&\setpostvennhook{
ct07-Syllogisms.md&5.26&\draw[<-] (labelOnlyAB) -- ++(0:3cm)
ct07-Syllogisms.md&5.26&node[right,text width=4cm,align=flush left]
ct07-Syllogisms.md&5.26&{All Spanish=16, we already have 8+3+1=12, missing 4.};}\end{venndiagram3sets}
ct07-Syllogisms.md&5.26&
ct07-Syllogisms.md&5.26&A teacher is planning schedules for 30 students.  Sixteen students say they want to take French, **16 want to take Spanish,** and 11 want to take Latin.  Five say they want to take both French and Latin, and of these, 3 wanted to take Spanish as well.  Five want only Latin, and 8 want only Spanish.  How many students want French only?
ct07-Syllogisms.md&5.26&
ct07-Syllogisms.md&5.27&## Solution
ct07-Syllogisms.md&5.27&
ct07-Syllogisms.md&5.27&\begin{venndiagram3sets}[labelA={French}, labelB={Spanish}, labelC={Latin}, labelABC={3}, labelOnlyAC={2}, labelOnlyC={5},  labelOnlyB={8}, labelOnlyBC={1}, labelOnlyAB={4}, labelOnlyA={7}]
ct07-Syllogisms.md&5.27&\setpostvennhook{
ct07-Syllogisms.md&5.27&\draw[<-] (labelOnlyA) -- ++(0:4cm)
ct07-Syllogisms.md&5.27&node[right,text width=4cm,align=flush left]
ct07-Syllogisms.md&5.27&{All French=16, we already have 4+3+2=9, missing 7.};}\end{venndiagram3sets}
ct07-Syllogisms.md&5.27&
ct07-Syllogisms.md&5.27&A teacher is planning schedules for 30 students.  **Sixteen students say they want to take French,** 16 want to take Spanish, and 11 want to take Latin.  Five say they want to take both French and Latin, and of these, 3 wanted to take Spanish as well.  Five want only Latin, and 8 want only Spanish.  How many students want French only?
ct07-Syllogisms.md&5.27&
ct07-Syllogisms.md&5.28&## Solution
ct07-Syllogisms.md&5.28&
ct07-Syllogisms.md&5.28&\begin{venndiagram3sets}[labelA={French}, labelB={Spanish}, labelC={Latin}, labelABC={3}, labelOnlyAC={2}, labelOnlyC={5},  labelOnlyB={8}, labelOnlyBC={1}, labelOnlyAB={4}, labelOnlyA={7}]
ct07-Syllogisms.md&5.28&\setpostvennhook{
ct07-Syllogisms.md&5.28&\draw[<-] (labelOnlyA) -- ++(0:4cm)
ct07-Syllogisms.md&5.28&node[right,text width=4cm,align=flush left]
ct07-Syllogisms.md&5.28&{All French=16, we already have 4+3+2=9, missing 7.};}\end{venndiagram3sets}
ct07-Syllogisms.md&5.28&
ct07-Syllogisms.md&5.28&7 students want to take French only.
ct07-Syllogisms.md&5.28&
ct07-Syllogisms.md&5.29&## Is this syllogism correct?
ct07-Syllogisms.md&5.29&
ct07-Syllogisms.md&5.29&Make a Venn diagram to show whether the following is correct:
ct07-Syllogisms.md&5.29&
ct07-Syllogisms.md&5.29&\vspace{2ex}
ct07-Syllogisms.md&5.29&
ct07-Syllogisms.md&5.29&All Greeks like to be in Greece.  
ct07-Syllogisms.md&5.29&All people who like to be in Greece spend their holidays there.  
ct07-Syllogisms.md&5.29& ---------------------------------------------------------------------------  
ct07-Syllogisms.md&5.29&Some people who spend their holidays in Greece are Greek.
ct07-Syllogisms.md&5.29&
ct07-Syllogisms.md&5.30&## Correct?
ct07-Syllogisms.md&5.30&
ct07-Syllogisms.md&5.30&All Greeks (P) like to be in Greece (M).  
ct07-Syllogisms.md&5.30&All people who like to be in Greece (M)  spend their holidays there (S).  
ct07-Syllogisms.md&5.30& --------------------------------------------------------------------------  
ct07-Syllogisms.md&5.30&Some people who spend their holidays in Greece (S) are Greek (P).
ct07-Syllogisms.md&5.30&
ct07-Syllogisms.md&5.30&
ct07-Syllogisms.md&5.31&## Correct?
ct07-Syllogisms.md&5.31&
ct07-Syllogisms.md&5.31&\begin{venndiagram3sets}[labelA={S}, labelB={P}, labelC={M}]
ct07-Syllogisms.md&5.31&\fillBNotC
ct07-Syllogisms.md&5.31&\end{venndiagram3sets}
ct07-Syllogisms.md&5.31&
ct07-Syllogisms.md&5.31&**All Greeks (P) like to be in Greece (M).**  
ct07-Syllogisms.md&5.31&All people who like to be in Greece (M)  spend their holidays there (S).  
ct07-Syllogisms.md&5.31& --------------------------------------------------------------------------  
ct07-Syllogisms.md&5.31&Some people who spend their holidays in Greece (S) are Greek (P).
ct07-Syllogisms.md&5.31&
ct07-Syllogisms.md&5.32&## Correct?
ct07-Syllogisms.md&5.32&
ct07-Syllogisms.md&5.32&\begin{venndiagram3sets}[labelA={S}, labelB={P}, labelC={M}]
ct07-Syllogisms.md&5.32&\fillBNotC
ct07-Syllogisms.md&5.32&\fillCNotA
ct07-Syllogisms.md&5.32&\end{venndiagram3sets}
ct07-Syllogisms.md&5.32&
ct07-Syllogisms.md&5.32&All Greeks (P) like to be in Greece (M).  
ct07-Syllogisms.md&5.32&**All people who like to be in Greece (M)  spend their holidays there (S).**  
ct07-Syllogisms.md&5.32& --------------------------------------------------------------------------  
ct07-Syllogisms.md&5.32&Some people who spend their holidays in Greece (S) are Greek (P).
ct07-Syllogisms.md&5.32&
ct07-Syllogisms.md&5.33&## Correct?
ct07-Syllogisms.md&5.33&
ct07-Syllogisms.md&5.33&\begin{venndiagram3sets}[labelA={S}, labelB={P}, labelC={M}]
ct07-Syllogisms.md&5.33&\fillBNotC
ct07-Syllogisms.md&5.33&\fillCNotA
ct07-Syllogisms.md&5.33&\setpostvennhook{
ct07-Syllogisms.md&5.33&\draw[<-] (labelABC) -- ++(0:3cm)
ct07-Syllogisms.md&5.33&node[right,text width=4cm,align=flush left]
ct07-Syllogisms.md&5.33&{This area would be the solution, but it does not have an 'x' inside it! So the conclusion does not follow from the premises. Invalid!};}
ct07-Syllogisms.md&5.33&\end{venndiagram3sets}
ct07-Syllogisms.md&5.33&
ct07-Syllogisms.md&5.33&All Greeks (P) like to be in Greece (M).  
ct07-Syllogisms.md&5.33&All people who like to be in Greece (M)  spend their holidays there (S).  
ct07-Syllogisms.md&5.33& --------------------------------------------------------------------------  
ct07-Syllogisms.md&5.33&**Some people who spend their holidays in Greece (S) are Greek (P) ???**
ct07-Syllogisms.md&5.33&
ct07-Syllogisms.md&5.33&
ct07-Syllogisms.md&5.34&## Correct?
ct07-Syllogisms.md&5.34&
ct07-Syllogisms.md&5.34&All Greeks (P) like to be in Greece (M).  
ct07-Syllogisms.md&5.34&All people who like to be in Greece (M)  spend their holidays there (S).  
ct07-Syllogisms.md&5.34& --------------------------------------------------------------------------  
ct07-Syllogisms.md&5.34&Some people who spend their holidays in Greece (S) are Greek (P).
ct07-Syllogisms.md&5.34&
ct07-Syllogisms.md&5.34&\vspace{3ex}
ct07-Syllogisms.md&5.34&
ct07-Syllogisms.md&5.34&- The root of the problem is that there might not be any Greeks at all.
ct07-Syllogisms.md&5.34&- If we had an additional premise "There are Greeks," this would put an 'x' into the central area and then the syllogism would be correct.
ct07-Syllogisms.md&5.34&
ct07-Syllogisms.md&5.34&
ct07-Syllogisms.md&5.35&## Show whether this is correct
ct07-Syllogisms.md&5.35&
ct07-Syllogisms.md&5.35&All vertebrates reproduce sexually.  
ct07-Syllogisms.md&5.35&All vertebrates are animals.  
ct07-Syllogisms.md&5.35& ------------------------------------------  
ct07-Syllogisms.md&5.35&Conclusion: All animals reproduce sexually.
ct07-Syllogisms.md&5.35&
ct07-Syllogisms.md&5.35&\vspace{3ex}
ct07-Syllogisms.md&5.35&
ct07-Syllogisms.md&5.35&*Draw a Venn diagram to show whether this syllogism is valid!*
ct07-Syllogisms.md&5.35&
ct07-Syllogisms.md&5.35&
ct07-Syllogisms.md&5.36&## Show whether this is correct
ct07-Syllogisms.md&5.36&
ct07-Syllogisms.md&5.36&\begin{venndiagram3sets}[labelA={S}, labelB={P}, labelC={M}]
ct07-Syllogisms.md&5.36&\fillCNotB
ct07-Syllogisms.md&5.36&\fillCNotA
ct07-Syllogisms.md&5.36&\setpostvennhook{
ct07-Syllogisms.md&5.36&\draw[<-] (labelOnlyA) -- ++(0:4cm)
ct07-Syllogisms.md&5.36&node[right,text width=4cm,align=flush left]
ct07-Syllogisms.md&5.36&{If the conclusion was correct, then this area should be shaded (=empty). It is not, so the argument is not valid.};}
ct07-Syllogisms.md&5.36&\end{venndiagram3sets}
ct07-Syllogisms.md&5.36&
ct07-Syllogisms.md&5.36&All vertebrates (M) reproduce sexually (P).  
ct07-Syllogisms.md&5.36&All vertebrates (M) are animals (S).  
ct07-Syllogisms.md&5.36& ------------------------------------------------------  
ct07-Syllogisms.md&5.36&Conclusion: All animals (S) reproduce sexually (P).
ct07-Syllogisms.md&5.36&
ct07-Syllogisms.md&5.37&## What did we learn today?
ct07-Syllogisms.md&5.37&
ct07-Syllogisms.md&5.37&- Necessary and sufficient conditions
ct07-Syllogisms.md&5.37&- Categorical syllogisms
ct07-Syllogisms.md&5.37&- What can go wrong with them
ct07-Syllogisms.md&5.37&- How Venn diagrams help us analyze syllogisms
ct07-Syllogisms.md&5.37&
ct07-Syllogisms.md&5.38&## Any questions?
ct07-Syllogisms.md&5.38&
ct07-Syllogisms.md&5.38&![](graphics/questions.jpg)\ 
ct07-Syllogisms.md&5.38&
ct07-Syllogisms.md&5.39&## Thank you for your attention!
ct07-Syllogisms.md&5.39&
ct07-Syllogisms.md&5.39&Email: <matthias@ln.edu.hk>
ct07-Syllogisms.md&5.39&
ct07-Syllogisms.md&5.39&If you have questions, please come to my office hours (see course outline).
ct08-Arguments-and-Validity.md&0.1&% Critical Thinking
ct08-Arguments-and-Validity.md&0.1&% 8. Arguments and validity in propositional logic
ct08-Arguments-and-Validity.md&0.1&% A. Matthias
ct08-Arguments-and-Validity.md&0.1&
ct08-Arguments-and-Validity.md&1.0&# Where we are
ct08-Arguments-and-Validity.md&1.0&
ct08-Arguments-and-Validity.md&1.1&## What did we learn last time?
ct08-Arguments-and-Validity.md&1.1&- More conditional arguments
ct08-Arguments-and-Validity.md&1.1&- Necessary and sufficient
ct08-Arguments-and-Validity.md&1.1&- Types of necessity
ct08-Arguments-and-Validity.md&1.1&- Categorical syllogisms
ct08-Arguments-and-Validity.md&1.1&- Venn diagrams
ct08-Arguments-and-Validity.md&1.1&
ct08-Arguments-and-Validity.md&1.2&## What are we going to learn today?
ct08-Arguments-and-Validity.md&1.2&- Validity in propositional logic.
ct08-Arguments-and-Validity.md&1.2&- Natural deduction.
ct08-Arguments-and-Validity.md&1.2&
ct08-Arguments-and-Validity.md&2.0&# Validity of arguments
ct08-Arguments-and-Validity.md&2.0&
ct08-Arguments-and-Validity.md&2.1&## Validity of arguments
ct08-Arguments-and-Validity.md&2.1&How do I know whether an argument is valid?
ct08-Arguments-and-Validity.md&2.1&
ct08-Arguments-and-Validity.md&2.1&> - For example, how can I prove that “affirming the antecedent” is valid?
ct08-Arguments-and-Validity.md&2.1&> - Definition of “valid”:
ct08-Arguments-and-Validity.md&2.1&> - *An argument is valid if its conclusion is always true when its premises are true.*
ct08-Arguments-and-Validity.md&2.1&>- (That is, when true premises always lead to a true conclusion.)
ct08-Arguments-and-Validity.md&2.1&
ct08-Arguments-and-Validity.md&2.2&## Proof of validity in propositional logic
ct08-Arguments-and-Validity.md&2.2&> - Is “affirming the antecedent” valid?
ct08-Arguments-and-Validity.md&2.2&> - Means: “Is the conclusion always true when the premises are true?”
ct08-Arguments-and-Validity.md&2.2&> - First, write it down formally:
ct08-Arguments-and-Validity.md&2.2&> - Premise 1: P$\to$Q, Premise 2: P, Conclusion: $\vdash$ Q
ct08-Arguments-and-Validity.md&2.2&
ct08-Arguments-and-Validity.md&2.3&## Proof of validity in propositional logic
ct08-Arguments-and-Validity.md&2.3&> - Second, make a truth table which contains both premises and conclusion:
ct08-Arguments-and-Validity.md&2.3&
ct08-Arguments-and-Validity.md&2.3&![](tt13a.png)\ 
ct08-Arguments-and-Validity.md&2.3&
ct08-Arguments-and-Validity.md&2.3&> - If there is at least one line where the premises are true and the conclusion is false, then the argument is *invalid!*
ct08-Arguments-and-Validity.md&2.3&> - The argument shown here is valid (green row in table)
ct08-Arguments-and-Validity.md&2.3&
ct08-Arguments-and-Validity.md&2.4&## Prove the validity! (5 min)
ct08-Arguments-and-Validity.md&2.4&Prove that “affirming the consequent” is invalid using a truth table!
ct08-Arguments-and-Validity.md&2.4&
ct08-Arguments-and-Validity.md&2.5&## Affirming the consequent:
ct08-Arguments-and-Validity.md&2.5&
ct08-Arguments-and-Validity.md&2.5&>- P$\to$Q, Q, therefore P.     
ct08-Arguments-and-Validity.md&2.5&
ct08-Arguments-and-Validity.md&2.5&![](tt13b.png)\ 
ct08-Arguments-and-Validity.md&2.5&
ct08-Arguments-and-Validity.md&2.5&>- In the second line of the table we can see that although the premises are true, the conclusion is false (invalid)!
ct08-Arguments-and-Validity.md&2.5&
ct08-Arguments-and-Validity.md&2.6&## $\vdash$  and $\vDash$
ct08-Arguments-and-Validity.md&2.6&The symbol “$\vdash$” (“turnstile”) technically means “sequent,” meaning that the expression on the right is supposed to follow from the expressions on the left. But this could be wrong.
ct08-Arguments-and-Validity.md&2.6&
ct08-Arguments-and-Validity.md&2.6&The symbol “$\vDash$” means that the expression on the right *is known to or has been proven* to follow from the expressions on the left. The sequent is therefore valid.
ct08-Arguments-and-Validity.md&2.6&
ct08-Arguments-and-Validity.md&2.6&e.g.
ct08-Arguments-and-Validity.md&2.6&
ct08-Arguments-and-Validity.md&2.6&- Affirming the antecedent: P$\to$Q, P $\vDash$ Q
ct08-Arguments-and-Validity.md&2.6&
ct08-Arguments-and-Validity.md&2.7&## Entailment (1)
ct08-Arguments-and-Validity.md&2.7&
ct08-Arguments-and-Validity.md&2.7&If we have one logical expression B (conclusion) and a set of logical expressions A~1~...A~n~ (premises), then we say that:
ct08-Arguments-and-Validity.md&2.7&
ct08-Arguments-and-Validity.md&2.7&A~1~...A~n~ “entail” B if and only if there is no assignment of truth-values under which A~1~...A~n~ (the premises) are true and B (the conclusion) is false.
ct08-Arguments-and-Validity.md&2.7&
ct08-Arguments-and-Validity.md&2.7&- This is equivalent to saying that B follows logically from A~1~...A~n~.
ct08-Arguments-and-Validity.md&2.7&- In a valid argument the premises entail the conclusion.
ct08-Arguments-and-Validity.md&2.7&
ct08-Arguments-and-Validity.md&2.8&## Entailment (2)
ct08-Arguments-and-Validity.md&2.8&
ct08-Arguments-and-Validity.md&2.8&“Entailment” is just another way of saying that a conclusion logically follows from the premises of an argument. So all these statements say the same thing:
ct08-Arguments-and-Validity.md&2.8&
ct08-Arguments-and-Validity.md&2.8&- “B follows logically from A.”
ct08-Arguments-and-Validity.md&2.8&- “If A is true, then B must be true.”
ct08-Arguments-and-Validity.md&2.8&- “A entails B.”
ct08-Arguments-and-Validity.md&2.8&- “An argument with the premise A and the conclusion B is valid.”
ct08-Arguments-and-Validity.md&2.8&- “A $\vdash$ B.”
ct08-Arguments-and-Validity.md&2.8&
ct08-Arguments-and-Validity.md&2.8&
ct08-Arguments-and-Validity.md&2.9&## Entailment (3)
ct08-Arguments-and-Validity.md&2.9&
ct08-Arguments-and-Validity.md&2.9&For example:
ct08-Arguments-and-Validity.md&2.9&
ct08-Arguments-and-Validity.md&2.9&\begin{tabular}{|c|c||c|}
ct08-Arguments-and-Validity.md&2.9&\hline
ct08-Arguments-and-Validity.md&2.9&$ P $ & $ Q $ & $ (P \vee Q) $ \\
ct08-Arguments-and-Validity.md&2.9&\hline
ct08-Arguments-and-Validity.md&2.9&F & F & F \\
ct08-Arguments-and-Validity.md&2.9&\hline
ct08-Arguments-and-Validity.md&2.9&F & T & T \\
ct08-Arguments-and-Validity.md&2.9&\hline
ct08-Arguments-and-Validity.md&2.9&T & F & T \\
ct08-Arguments-and-Validity.md&2.9&\hline
ct08-Arguments-and-Validity.md&2.9&T & T & T \\
ct08-Arguments-and-Validity.md&2.9&\hline
ct08-Arguments-and-Validity.md&2.9&\end{tabular}
ct08-Arguments-and-Validity.md&2.9&
ct08-Arguments-and-Validity.md&2.9&- P entails P$\lor$Q, because whenever P is true, P$\lor$Q will also be true.
ct08-Arguments-and-Validity.md&2.9&- P$\lor$Q does *not* entail P, because when P$\lor$Q is true, P is not necessarily true (it can be false, and Q can be true).
ct08-Arguments-and-Validity.md&2.9&
ct08-Arguments-and-Validity.md&2.10&## Exercise
ct08-Arguments-and-Validity.md&2.10&
ct08-Arguments-and-Validity.md&2.10&Are the following true? Use a truth table to decide.
ct08-Arguments-and-Validity.md&2.10&
ct08-Arguments-and-Validity.md&2.10&1. P entails P&Q
ct08-Arguments-and-Validity.md&2.10&2. P&Q entails P
ct08-Arguments-and-Validity.md&2.10&3. P&Q $\vdash$ PvQ
ct08-Arguments-and-Validity.md&2.10&4. PvQ $\vdash$ P&Q
ct08-Arguments-and-Validity.md&2.10&5. P & ~P $\vdash$ Q
ct08-Arguments-and-Validity.md&2.10&
ct08-Arguments-and-Validity.md&2.10&Pay particular attention to the last one! (Explained in more detail in the next session).
ct08-Arguments-and-Validity.md&2.10&
ct08-Arguments-and-Validity.md&2.10&
ct08-Arguments-and-Validity.md&2.11&## Common valid argument patterns
ct08-Arguments-and-Validity.md&2.11&>1. Affirming the antecedent: \f{P$\to$Q, P $\vDash$ Q}
ct08-Arguments-and-Validity.md&2.11&>2. Denying the consequent: \f{P$\to$Q, $\sim$Q $\vDash$ $\sim$P}
ct08-Arguments-and-Validity.md&2.11&>3. Hypothetical syllogism: \f{A$\to$B, B$\to$C $\vDash$ A$\to$C}    
ct08-Arguments-and-Validity.md&2.11&Example:      
ct08-Arguments-and-Validity.md&2.11&If I get up late (A), I’ll miss the class (B)      
ct08-Arguments-and-Validity.md&2.11&If I miss the class (B), I’ll get a bad grade (C)     
ct08-Arguments-and-Validity.md&2.11&(Conclusion:) If I get up late (A), I’ll get a bad grade (C)
ct08-Arguments-and-Validity.md&2.11&
ct08-Arguments-and-Validity.md&2.12&## Common valid argument patterns
ct08-Arguments-and-Validity.md&2.12&
ct08-Arguments-and-Validity.md&2.12&4. Disjunctive syllogism:    
ct08-Arguments-and-Validity.md&2.12&\f{PvQ, $\sim$P $\vDash$ Q}     
ct08-Arguments-and-Validity.md&2.12&\f{PvQ, $\sim$Q $\vDash$ P}
ct08-Arguments-and-Validity.md&2.12&
ct08-Arguments-and-Validity.md&2.13&## Common valid argument patterns
ct08-Arguments-and-Validity.md&2.13&
ct08-Arguments-and-Validity.md&2.13&5. Dilemma:      
ct08-Arguments-and-Validity.md&2.13&\f{P$\to$Q, R$\to$S, PvR $\vDash$ QvS}
ct08-Arguments-and-Validity.md&2.13&
ct08-Arguments-and-Validity.md&2.13&Example:
ct08-Arguments-and-Validity.md&2.13&
ct08-Arguments-and-Validity.md&2.13&If I go shopping, I will spend a lot of money     
ct08-Arguments-and-Validity.md&2.13&If I stay at home, I will be bored     
ct08-Arguments-and-Validity.md&2.13&I have to go shopping or stay at home    
ct08-Arguments-and-Validity.md&2.13&-----------------------------------------------------     
ct08-Arguments-and-Validity.md&2.13&I will spend a lot of money or I will be bored 
ct08-Arguments-and-Validity.md&2.13&
ct08-Arguments-and-Validity.md&2.14&## Common valid argument patterns
ct08-Arguments-and-Validity.md&2.14&
ct08-Arguments-and-Validity.md&2.14&6. Conjunction-In:     
ct08-Arguments-and-Validity.md&2.14&\f{P, Q $\vDash$ P$\&$Q}
ct08-Arguments-and-Validity.md&2.14&
ct08-Arguments-and-Validity.md&2.14&7. Conjunction-Out:     
ct08-Arguments-and-Validity.md&2.14&\f{P$\&$Q $\vDash$ P}    
ct08-Arguments-and-Validity.md&2.14&\f{P$\&$Q $\vDash$ Q}    
ct08-Arguments-and-Validity.md&2.14&
ct08-Arguments-and-Validity.md&2.15&## Common valid argument patterns
ct08-Arguments-and-Validity.md&2.15&8. Double negation: \f{P $\equiv$ $\sim$$\sim$P}
ct08-Arguments-and-Validity.md&2.15&
ct08-Arguments-and-Validity.md&2.15&“$\equiv$” means “equivalent”. Two expressions are equivalent if they have exactly the same truth values.
ct08-Arguments-and-Validity.md&2.15&
ct08-Arguments-and-Validity.md&2.16&## Common valid argument patterns
ct08-Arguments-and-Validity.md&2.16&
ct08-Arguments-and-Validity.md&2.16&9. De Morgan’s laws:    
ct08-Arguments-and-Validity.md&2.16&\f{$\sim$(PvQ) $\equiv$ $\sim$P$\&$$\sim$Q}    
ct08-Arguments-and-Validity.md&2.16&\f{$\sim$(P$\&$Q) $\equiv$ $\sim$Pv$\sim$Q}
ct08-Arguments-and-Validity.md&2.16&
ct08-Arguments-and-Validity.md&2.16&*(Remember the soup without chicken meat or fish balls!)*
ct08-Arguments-and-Validity.md&2.16&
ct08-Arguments-and-Validity.md&2.17&## Common valid argument patterns
ct08-Arguments-and-Validity.md&2.17&
ct08-Arguments-and-Validity.md&2.17&10. Contraposition:    
ct08-Arguments-and-Validity.md&2.17&\f{P$\to$Q $\equiv$ $\sim$Q$\to$$\sim$P}    
ct08-Arguments-and-Validity.md&2.17&(denying the consequent)
ct08-Arguments-and-Validity.md&2.17&
ct08-Arguments-and-Validity.md&2.18&## Common valid argument patterns
ct08-Arguments-and-Validity.md&2.18&
ct08-Arguments-and-Validity.md&2.18&11. Conditional-disjunction exchange:    
ct08-Arguments-and-Validity.md&2.18&\f{P$\to$Q $\equiv$ $\sim$PvQ}
ct08-Arguments-and-Validity.md&2.18&
ct08-Arguments-and-Validity.md&2.18&Example:
ct08-Arguments-and-Validity.md&2.18&
ct08-Arguments-and-Validity.md&2.18&“If it rains, the street will be wet” is equivalent to:     
ct08-Arguments-and-Validity.md&2.18&“It has not rained or the street is wet.”
ct08-Arguments-and-Validity.md&2.18&
ct08-Arguments-and-Validity.md&2.18&You can easily show that these are equivalent using a truth table! (Left as an exercise to the reader)
ct08-Arguments-and-Validity.md&2.18&
ct08-Arguments-and-Validity.md&2.19&## Common valid argument patterns (review)
ct08-Arguments-and-Validity.md&2.19&1. Affirming the antecedent: P$\to$Q, P $\vDash$ Q
ct08-Arguments-and-Validity.md&2.19&2. Denying the consequent: P$\to$Q, \~Q $\vDash$ \~P
ct08-Arguments-and-Validity.md&2.19&3. Hypothetical syllogism: A$\to$B, B$\to$C $\vDash$ A$\to$C
ct08-Arguments-and-Validity.md&2.19&4. Disjunctive syllogism: PvQ, \~P $\vDash$ Q and PvQ, \~Q $\vDash$ P
ct08-Arguments-and-Validity.md&2.19&5. Dilemma: P$\to$Q, R$\to$S, PvR $\vDash$ QvS
ct08-Arguments-and-Validity.md&2.19&6. Conjunction-In: P, Q $\vDash$ P$\&$Q
ct08-Arguments-and-Validity.md&2.19&7. Conjunction-Out: P$\&$Q $\vDash$ P and P$\&$Q $\vDash$ Q
ct08-Arguments-and-Validity.md&2.19&8. Double negation: P $\equiv$ \~\~P
ct08-Arguments-and-Validity.md&2.19&9. De Morgan’s laws \~(PvQ) $\equiv$ \~P$\&$\~Q and \~(P$\&$Q) $\equiv$ \~Pv\~Q
ct08-Arguments-and-Validity.md&2.19&10. Contraposition P$\to$Q $\equiv$ \~Q$\to$\~P (denying the consequent)
ct08-Arguments-and-Validity.md&2.19&11. Conditional-disjunction exchange P$\to$Q $\equiv$ \~PvQ
ct08-Arguments-and-Validity.md&2.19&
ct08-Arguments-and-Validity.md&3.0&# Natural deduction
ct08-Arguments-and-Validity.md&3.0&
ct08-Arguments-and-Validity.md&3.1&## Natural deduction
ct08-Arguments-and-Validity.md&3.1&
ct08-Arguments-and-Validity.md&3.1&When we show the validity of statements by deriving the conclusion from the premises using the equivalences above, this is called “natural deduction” (because it follows the way we reason “naturally”).
ct08-Arguments-and-Validity.md&3.1&
ct08-Arguments-and-Validity.md&3.2&## Show the validity! (3 min)
ct08-Arguments-and-Validity.md&3.2&
ct08-Arguments-and-Validity.md&3.2&If coffee was healthier than tea, then all people would be drinking coffee.     
ct08-Arguments-and-Validity.md&3.2&If all people were drinking coffee, then coffee would be expensive.     
ct08-Arguments-and-Validity.md&3.2&Coffee is not expensive.     
ct08-Arguments-and-Validity.md&3.2& -----------------------------------------------------------     
ct08-Arguments-and-Validity.md&3.2&Therefore, coffee is not healthier than tea.
ct08-Arguments-and-Validity.md&3.2&
ct08-Arguments-and-Validity.md&3.3&## Step 1: Translate into propositional logic     
ct08-Arguments-and-Validity.md&3.3&C: Coffee healthier than tea    
ct08-Arguments-and-Validity.md&3.3&D: All people drink coffee    
ct08-Arguments-and-Validity.md&3.3&E: Coffee is expensive    
ct08-Arguments-and-Validity.md&3.3&
ct08-Arguments-and-Validity.md&3.3&C $\to$ D    
ct08-Arguments-and-Validity.md&3.3&D $\to$ E    
ct08-Arguments-and-Validity.md&3.3&\~E   
ct08-Arguments-and-Validity.md&3.3& ------------     
ct08-Arguments-and-Validity.md&3.3&\~C
ct08-Arguments-and-Validity.md&3.3&
ct08-Arguments-and-Validity.md&3.4&## Step 2: Find valid patterns and equivalences
ct08-Arguments-and-Validity.md&3.4&
ct08-Arguments-and-Validity.md&3.4&C $\to$ D     
ct08-Arguments-and-Validity.md&3.4&D $\to$ E     
ct08-Arguments-and-Validity.md&3.4&\~E   
ct08-Arguments-and-Validity.md&3.4& ------------     
ct08-Arguments-and-Validity.md&3.4&\~C
ct08-Arguments-and-Validity.md&3.4&
ct08-Arguments-and-Validity.md&3.4&1. From C$\to$D and D$\to$E follows C$\to$E (hypoth. syll.)
ct08-Arguments-and-Validity.md&3.4&2. Since \~E (3), we know that \~C (deny consequent)
ct08-Arguments-and-Validity.md&3.4&
ct08-Arguments-and-Validity.md&3.5&## Another solution:
ct08-Arguments-and-Validity.md&3.5&
ct08-Arguments-and-Validity.md&3.5&C $\to$ D     
ct08-Arguments-and-Validity.md&3.5&D $\to$ E     
ct08-Arguments-and-Validity.md&3.5&\~E     
ct08-Arguments-and-Validity.md&3.5& ------------     
ct08-Arguments-and-Validity.md&3.5&\~C
ct08-Arguments-and-Validity.md&3.5&
ct08-Arguments-and-Validity.md&3.5&1. From \~E and premise (2) we know that \~D (denying consequent)
ct08-Arguments-and-Validity.md&3.5&2. From \~D and (1) we know that \~C (denying consequent)
ct08-Arguments-and-Validity.md&3.5&
ct08-Arguments-and-Validity.md&3.5&You can also show the validity of this with a truth table! (Exercise for the reader).
ct08-Arguments-and-Validity.md&3.5&
ct08-Arguments-and-Validity.md&3.5&
ct08-Arguments-and-Validity.md&3.6&## Natural deduction
ct08-Arguments-and-Validity.md&3.6&
ct08-Arguments-and-Validity.md&3.6&In natural deduction, we use a particular set of rules and symbols for these rules:
ct08-Arguments-and-Validity.md&3.6&
ct08-Arguments-and-Validity.md&3.6&- from A follows that A: **R** (reiteration)
ct08-Arguments-and-Validity.md&3.6&- Conjunction-out: **&O**
ct08-Arguments-and-Validity.md&3.6&- Conjunction-in: **&I**
ct08-Arguments-and-Validity.md&3.6&- Disjunction-In (A$\vDash$AvB): **vI**
ct08-Arguments-and-Validity.md&3.6&- Aff. antecedent (“modus ponens”): **MP**
ct08-Arguments-and-Validity.md&3.6&- Deny consequent (“modus tollens”): **MT**
ct08-Arguments-and-Validity.md&3.6&- Hypothetical syllogism: **HS**
ct08-Arguments-and-Validity.md&3.6&- Disjunctive syllogism: **DS**
ct08-Arguments-and-Validity.md&3.6&- Double negation: **DN**
ct08-Arguments-and-Validity.md&3.6&- DeMorgan’s laws: **DeM**
ct08-Arguments-and-Validity.md&3.6&- Contraposition: **Con**
ct08-Arguments-and-Validity.md&3.6&
ct08-Arguments-and-Validity.md&3.7&## Natural deduction
ct08-Arguments-and-Validity.md&3.7&In the formalism of natural deduction, you would write the same thing in a more formal way:
ct08-Arguments-and-Validity.md&3.7&
ct08-Arguments-and-Validity.md&3.7&> 1. Each line is numbered
ct08-Arguments-and-Validity.md&3.7&> 2. In the first column you write the result of each step
ct08-Arguments-and-Validity.md&3.7&> 3. In the second column you write the numbers of the lines you use to derive that conclusion, plus the short form of the name of the rule you use
ct08-Arguments-and-Validity.md&3.7&> 4. Repeat until the left column contains the desired conclusion
ct08-Arguments-and-Validity.md&3.7&
ct08-Arguments-and-Validity.md&3.8&## Natural deduction form
ct08-Arguments-and-Validity.md&3.8&
ct08-Arguments-and-Validity.md&3.8&------------------------------------------
ct08-Arguments-and-Validity.md&3.8&C $\to$ D, D $\to$ E, \~E $\vdash$ \~C
ct08-Arguments-and-Validity.md&3.8&------------------------- ----------------
ct08-Arguments-and-Validity.md&3.8&1. \~E                    premise
ct08-Arguments-and-Validity.md&3.8&
ct08-Arguments-and-Validity.md&3.8&2. D$\to$E                premise
ct08-Arguments-and-Validity.md&3.8&
ct08-Arguments-and-Validity.md&3.8&3. \~D                    1, 2, MT
ct08-Arguments-and-Validity.md&3.8&
ct08-Arguments-and-Validity.md&3.8&4. C$\to$D                premise
ct08-Arguments-and-Validity.md&3.8&
ct08-Arguments-and-Validity.md&3.8&5. \~C                    3, 4, MT
ct08-Arguments-and-Validity.md&3.8&------------------------------------------
ct08-Arguments-and-Validity.md&3.8&
ct08-Arguments-and-Validity.md&3.9&## Show that this is valid (5 min)
ct08-Arguments-and-Validity.md&3.9&(P$\&$Q$\to$R), Q$\to$P, Q $\vdash$ R
ct08-Arguments-and-Validity.md&3.9&
ct08-Arguments-and-Validity.md&3.9&Try to write down your result in the formalism of natural deduction!
ct08-Arguments-and-Validity.md&3.9&
ct08-Arguments-and-Validity.md&3.10&## (P$\&$Q$\to$R), Q$\to$P, Q $\vdash$ R
ct08-Arguments-and-Validity.md&3.10&
ct08-Arguments-and-Validity.md&3.10&Informal notes:
ct08-Arguments-and-Validity.md&3.10&
ct08-Arguments-and-Validity.md&3.10&> - P $\&$ Q $\to$ R (premise)
ct08-Arguments-and-Validity.md&3.10&> - Q $\to$ P (premise)
ct08-Arguments-and-Validity.md&3.10&> - Q (premise)
ct08-Arguments-and-Validity.md&3.10&> - From Q $\to$ P and Q follows that P (aff. ant.)
ct08-Arguments-and-Validity.md&3.10&> - From P and Q follows that P$\&$Q (conj. in)
ct08-Arguments-and-Validity.md&3.10&> - From P$\&$Q and the first premise follows R (aff. ant.)
ct08-Arguments-and-Validity.md&3.10&
ct08-Arguments-and-Validity.md&3.11&## (P$\&$Q$\to$R), Q$\to$P, Q $\vdash$ R
ct08-Arguments-and-Validity.md&3.11&
ct08-Arguments-and-Validity.md&3.11&Natural deduction:
ct08-Arguments-and-Validity.md&3.11&
ct08-Arguments-and-Validity.md&3.11&----------------------------------------------
ct08-Arguments-and-Validity.md&3.11&(P$\&$Q$\to$R), Q$\to$P, Q    $\vdash$ R
ct08-Arguments-and-Validity.md&3.11&----------------------------- ----------------
ct08-Arguments-and-Validity.md&3.11&1. P$\&$Q$\to$R               premise
ct08-Arguments-and-Validity.md&3.11&
ct08-Arguments-and-Validity.md&3.11&2. Q$\to$P                    premise
ct08-Arguments-and-Validity.md&3.11&
ct08-Arguments-and-Validity.md&3.11&3. Q                          premise
ct08-Arguments-and-Validity.md&3.11&
ct08-Arguments-and-Validity.md&3.11&4. P                          2, 3, MP
ct08-Arguments-and-Validity.md&3.11&
ct08-Arguments-and-Validity.md&3.11&5. P$\&$Q                     4, 3, $\&$I
ct08-Arguments-and-Validity.md&3.11&
ct08-Arguments-and-Validity.md&3.11&6. R                          5, 1, MP
ct08-Arguments-and-Validity.md&3.11&----------------------------------------------
ct08-Arguments-and-Validity.md&3.11&
ct08-Arguments-and-Validity.md&3.12&## Show the validity! (5 min)
ct08-Arguments-and-Validity.md&3.12&Show the validity of this argument, by deriving it from the valid argument forms shown before:
ct08-Arguments-and-Validity.md&3.12&
ct08-Arguments-and-Validity.md&3.12&P v \~Q v R     
ct08-Arguments-and-Validity.md&3.12&R $\to$ \~S     
ct08-Arguments-and-Validity.md&3.12&S$\&$Q     
ct08-Arguments-and-Validity.md&3.12& -----------------     
ct08-Arguments-and-Validity.md&3.12&P
ct08-Arguments-and-Validity.md&3.12&
ct08-Arguments-and-Validity.md&3.13&## Pv\~QvR, R$\to$\~S, S$\&$Q $\vdash$ P
ct08-Arguments-and-Validity.md&3.13&
ct08-Arguments-and-Validity.md&3.13&1. From S$\&$Q follows: S and Q (by conjunction-out).
ct08-Arguments-and-Validity.md&3.13&2. S is equivalent to \~\~S (by double negation).
ct08-Arguments-and-Validity.md&3.13&3. From \~\~S and the premise R$\to$\~S follows \~R (deny consequent).
ct08-Arguments-and-Validity.md&3.13&4. From \~R and the premise Pv\~QvR follows Pv\~Q (by disjunctive syllogism).
ct08-Arguments-and-Validity.md&3.13&5. Q is equivalent to \~\~Q (by double negation).
ct08-Arguments-and-Validity.md&3.13&6. From \~\~Q and Pv\~Q follows P, the desired conclusion (by disjunctive syllogism).
ct08-Arguments-and-Validity.md&3.13&
ct08-Arguments-and-Validity.md&3.14&## Pv\~QvR, R$\to$\~S, S$\&$Q $\vdash$ P
ct08-Arguments-and-Validity.md&3.14&--------------------------------------- ----------------
ct08-Arguments-and-Validity.md&3.14&1. S$\&$Q                               premise
ct08-Arguments-and-Validity.md&3.14&2. S                                    1, \&O
ct08-Arguments-and-Validity.md&3.14&3. Q                                    1, \&O
ct08-Arguments-and-Validity.md&3.14&4. \~\~S                                2, DN
ct08-Arguments-and-Validity.md&3.14&5. R$\to$\~S                            premise
ct08-Arguments-and-Validity.md&3.14&6. \~R                                  4, 5, MT
ct08-Arguments-and-Validity.md&3.14&7. Pv\~QvR                              premise
ct08-Arguments-and-Validity.md&3.14&8. Pv\~Q                                6, 7, DS
ct08-Arguments-and-Validity.md&3.14&9. \~\~Q                                3, DN
ct08-Arguments-and-Validity.md&3.14&10. P                                   9, 8, DS
ct08-Arguments-and-Validity.md&3.14&--------------------------------------- ----------------
ct08-Arguments-and-Validity.md&3.14&
ct08-Arguments-and-Validity.md&3.14&
ct08-Arguments-and-Validity.md&3.15&## What did we learn today?
ct08-Arguments-and-Validity.md&3.15&- Validity in propositional logic
ct08-Arguments-and-Validity.md&3.15&- Natural deduction
ct08-Arguments-and-Validity.md&3.15&
ct08-Arguments-and-Validity.md&3.16&## References
ct08-Arguments-and-Validity.md&3.16&HKU OpenCourseWare on critical thinking, logic, and creativity:
ct08-Arguments-and-Validity.md&3.16&
ct08-Arguments-and-Validity.md&3.16&http://philosophy.hku.hk/think/pl
ct08-Arguments-and-Validity.md&3.16&
ct08-Arguments-and-Validity.md&3.16&(Modules Q04, Q05)
ct08-Arguments-and-Validity.md&3.16&
ct08-Arguments-and-Validity.md&3.17&## Any questions?
ct08-Arguments-and-Validity.md&3.17&
ct08-Arguments-and-Validity.md&3.17&![](graphics/questions.jpg)\ 
ct08-Arguments-and-Validity.md&3.17&
ct08-Arguments-and-Validity.md&3.18&## Thank you for your attention!
ct08-Arguments-and-Validity.md&3.18&
ct08-Arguments-and-Validity.md&3.18&Email: <matthias@ln.edu.hk>
ct08-Arguments-and-Validity.md&3.18&
ct08-Arguments-and-Validity.md&3.18&If you have questions, please come to my office hours (see course outline).
ct08-Arguments-and-Validity.md&3.18&
ct08-Arguments-and-Validity.md&3.18&
ct08-Arguments-and-Validity.md&3.18&
ct08-Arguments-and-Validity.md&3.18&
ct08-Arguments-and-Validity.md&3.18&
ct09-10-More-Natural-Deduction.md&0.1&% Critical Thinking
ct09-10-More-Natural-Deduction.md&0.1&% 9/10. More Natural Deduction and Other Logic Exercises
ct09-10-More-Natural-Deduction.md&0.1&% A. Matthias
ct09-10-More-Natural-Deduction.md&0.1&
ct09-10-More-Natural-Deduction.md&1.0&# Where we are
ct09-10-More-Natural-Deduction.md&1.0&
ct09-10-More-Natural-Deduction.md&1.1&## What did we learn last time?
ct09-10-More-Natural-Deduction.md&1.1&
ct09-10-More-Natural-Deduction.md&1.1&- Validity in propositional logic.
ct09-10-More-Natural-Deduction.md&1.1&- Natural deduction.
ct09-10-More-Natural-Deduction.md&1.1&
ct09-10-More-Natural-Deduction.md&1.2&## What are we going to learn today?
ct09-10-More-Natural-Deduction.md&1.2&
ct09-10-More-Natural-Deduction.md&1.2&- More natural deduction
ct09-10-More-Natural-Deduction.md&1.2&- More logic exercises
ct09-10-More-Natural-Deduction.md&1.2&
ct09-10-More-Natural-Deduction.md&2.0&# Rules for all five operators
ct09-10-More-Natural-Deduction.md&2.0&
ct09-10-More-Natural-Deduction.md&2.1&## In-Out-Rules
ct09-10-More-Natural-Deduction.md&2.1&
ct09-10-More-Natural-Deduction.md&2.1&In order to do natural deduction more systematically, we need to be able to transform all four basic logical operators (we don't need to deal with the biconditional separately, because it is just a conjunction of two implications). We therefore need rules to:
ct09-10-More-Natural-Deduction.md&2.1&
ct09-10-More-Natural-Deduction.md&2.1&>- Introduce and eliminate conjunction (\&I and \&O).
ct09-10-More-Natural-Deduction.md&2.1&>- Introduce and eliminate disjunction (vI and vO)
ct09-10-More-Natural-Deduction.md&2.1&>- Introduce and eliminate implication ($\to$I and $\to$O)
ct09-10-More-Natural-Deduction.md&2.1&>- Introduce and eliminate negation (\~I and \~O)
ct09-10-More-Natural-Deduction.md&2.1&
ct09-10-More-Natural-Deduction.md&2.1&. . .
ct09-10-More-Natural-Deduction.md&2.1&
ct09-10-More-Natural-Deduction.md&2.1&All O-(out-)rules are sometimes also called *elimination rules.* The O then becomes an E.
ct09-10-More-Natural-Deduction.md&2.1&
ct09-10-More-Natural-Deduction.md&2.1&So \&O, \&E, E\&, E$\land$, $\land$E, $\land$O all mean the same thing. Different authors just write them differently.
ct09-10-More-Natural-Deduction.md&2.1&
ct09-10-More-Natural-Deduction.md&2.1&We will only look at *some* of those.
ct09-10-More-Natural-Deduction.md&2.1&
ct09-10-More-Natural-Deduction.md&2.2&## Conjunction-in
ct09-10-More-Natural-Deduction.md&2.2&
ct09-10-More-Natural-Deduction.md&2.2&This we already know:
ct09-10-More-Natural-Deduction.md&2.2&
ct09-10-More-Natural-Deduction.md&2.2&\f{A, B $\vDash$ A\&B (\&I)}
ct09-10-More-Natural-Deduction.md&2.2&
ct09-10-More-Natural-Deduction.md&2.3&## Conjunction-out
ct09-10-More-Natural-Deduction.md&2.3&
ct09-10-More-Natural-Deduction.md&2.3&This is also easy:
ct09-10-More-Natural-Deduction.md&2.3&
ct09-10-More-Natural-Deduction.md&2.3&\f{A\&B $\vDash$ A (\&O)}
ct09-10-More-Natural-Deduction.md&2.3&
ct09-10-More-Natural-Deduction.md&2.3&\f{A\&B $\vDash$ B (\&O)}
ct09-10-More-Natural-Deduction.md&2.3&
ct09-10-More-Natural-Deduction.md&2.3&
ct09-10-More-Natural-Deduction.md&2.4&## Disjunction-in
ct09-10-More-Natural-Deduction.md&2.4&
ct09-10-More-Natural-Deduction.md&2.4&Disjunction-in is pretty trivial. If I know that A, then I also know that A$\lor$B:
ct09-10-More-Natural-Deduction.md&2.4&
ct09-10-More-Natural-Deduction.md&2.4&\f{A $\vDash$ A$\lor$B (vI)}
ct09-10-More-Natural-Deduction.md&2.4&
ct09-10-More-Natural-Deduction.md&2.4&Since A is true in this case (premise), A$\lor$B must also be true, no matter what the truth value of B is!
ct09-10-More-Natural-Deduction.md&2.4&
ct09-10-More-Natural-Deduction.md&2.5&## Disjunction-out
ct09-10-More-Natural-Deduction.md&2.5&
ct09-10-More-Natural-Deduction.md&2.5&This is what we called the *disjunctive syllogism* above:
ct09-10-More-Natural-Deduction.md&2.5&
ct09-10-More-Natural-Deduction.md&2.5&\f{A$\lor$B, $\sim$A $\vDash$ B (vO)}
ct09-10-More-Natural-Deduction.md&2.5&
ct09-10-More-Natural-Deduction.md&2.5&\f{A$\lor$B, $\sim$B $\vDash$ A (vO)}
ct09-10-More-Natural-Deduction.md&2.5&
ct09-10-More-Natural-Deduction.md&2.5&Clearly, if A$\lor$B is known to be true (premise), and B is false, then A must be true (otherwise A$\lor$B could not possibly be true!)
ct09-10-More-Natural-Deduction.md&2.5&
ct09-10-More-Natural-Deduction.md&2.6&## Implication-in
ct09-10-More-Natural-Deduction.md&2.6&
ct09-10-More-Natural-Deduction.md&2.6&We will not discuss this here. See the HKU Think resource for an introduction.
ct09-10-More-Natural-Deduction.md&2.6&
ct09-10-More-Natural-Deduction.md&2.7&## Implication-out
ct09-10-More-Natural-Deduction.md&2.7&
ct09-10-More-Natural-Deduction.md&2.7&This one is easier. Implication elimination we know already as *modus ponens* or *affirming the antecedent:*
ct09-10-More-Natural-Deduction.md&2.7&
ct09-10-More-Natural-Deduction.md&2.7&\f{A$\to$B, A $\vDash$ B ($\to$O)}
ct09-10-More-Natural-Deduction.md&2.7&
ct09-10-More-Natural-Deduction.md&2.8&## Negation-in
ct09-10-More-Natural-Deduction.md&2.8&
ct09-10-More-Natural-Deduction.md&2.8&We will not do this now, because it is not required for this course.
ct09-10-More-Natural-Deduction.md&2.8&
ct09-10-More-Natural-Deduction.md&2.9&## Negation-out
ct09-10-More-Natural-Deduction.md&2.9&
ct09-10-More-Natural-Deduction.md&2.9&Negation-out just means eliminating double negations:
ct09-10-More-Natural-Deduction.md&2.9&
ct09-10-More-Natural-Deduction.md&2.9&\f{$\sim\sim$A $\vDash$ A ($\sim$O)}
ct09-10-More-Natural-Deduction.md&2.9&
ct09-10-More-Natural-Deduction.md&2.10&## How to use these rules
ct09-10-More-Natural-Deduction.md&2.10&
ct09-10-More-Natural-Deduction.md&2.10&>- In principle you could construct proofs for every provable statement out of these elementary proofs. But, of course, this would be a lot of work.
ct09-10-More-Natural-Deduction.md&2.10&>- This is why we already assume that we know that some more complex equivalences hold (for example, the deMorgan laws).
ct09-10-More-Natural-Deduction.md&2.10&>- And therefore we can shorten and simplify our proofs by assuming that we have already proven these equivalences.
ct09-10-More-Natural-Deduction.md&2.10&
ct09-10-More-Natural-Deduction.md&2.11&## Exercise
ct09-10-More-Natural-Deduction.md&2.11&
ct09-10-More-Natural-Deduction.md&2.11&Show that: P, P$\to$Q $\vdash$ P\&Q
ct09-10-More-Natural-Deduction.md&2.11&
ct09-10-More-Natural-Deduction.md&2.11&. . .
ct09-10-More-Natural-Deduction.md&2.11&
ct09-10-More-Natural-Deduction.md&2.11&------------------------------------------
ct09-10-More-Natural-Deduction.md&2.11&P, P$\to$Q $\vdash$ P\&Q
ct09-10-More-Natural-Deduction.md&2.11&------------------------------------ ----------------
ct09-10-More-Natural-Deduction.md&2.11&1. P	          	                             Premise
ct09-10-More-Natural-Deduction.md&2.11&
ct09-10-More-Natural-Deduction.md&2.11&2. P$\to$Q                              Premise
ct09-10-More-Natural-Deduction.md&2.11&
ct09-10-More-Natural-Deduction.md&2.11&3. Q	          	                            1, 2, $\to$O
ct09-10-More-Natural-Deduction.md&2.11&
ct09-10-More-Natural-Deduction.md&2.11&4. P\&Q	          	                     1, 3, &I
ct09-10-More-Natural-Deduction.md&2.11&------------------------------------------
ct09-10-More-Natural-Deduction.md&2.11&
ct09-10-More-Natural-Deduction.md&2.12&## Exercise
ct09-10-More-Natural-Deduction.md&2.12&
ct09-10-More-Natural-Deduction.md&2.12&Show that: (P\&Q)$\to$R, Q$\to$P, Q $\vdash$ R
ct09-10-More-Natural-Deduction.md&2.12&
ct09-10-More-Natural-Deduction.md&2.12&. . .
ct09-10-More-Natural-Deduction.md&2.12&
ct09-10-More-Natural-Deduction.md&2.12&------------------------------------------
ct09-10-More-Natural-Deduction.md&2.12&(P\&Q)$\to$R, Q$\to$P, Q $\vdash$ R
ct09-10-More-Natural-Deduction.md&2.12&------------------------------------ ----------------
ct09-10-More-Natural-Deduction.md&2.12&1. (P\&Q)$\to$R	    	                             Premise
ct09-10-More-Natural-Deduction.md&2.12&
ct09-10-More-Natural-Deduction.md&2.12&2. Q$\to$P				                              Premise
ct09-10-More-Natural-Deduction.md&2.12&
ct09-10-More-Natural-Deduction.md&2.12&3. Q	          		                           Premise
ct09-10-More-Natural-Deduction.md&2.12&
ct09-10-More-Natural-Deduction.md&2.12&4. P					                      2, 3, $\to$O
ct09-10-More-Natural-Deduction.md&2.12&
ct09-10-More-Natural-Deduction.md&2.12&5. P\&Q                               3, 4, \&I
ct09-10-More-Natural-Deduction.md&2.12&
ct09-10-More-Natural-Deduction.md&2.12&6. R					                        1, 5, $\to$O
ct09-10-More-Natural-Deduction.md&2.12&------------------------------------------
ct09-10-More-Natural-Deduction.md&2.12&
ct09-10-More-Natural-Deduction.md&2.12&
ct09-10-More-Natural-Deduction.md&2.13&## Common valid argument patterns (review)
ct09-10-More-Natural-Deduction.md&2.13&1. Affirming the antecedent: P$\to$Q, P $\vDash$ Q
ct09-10-More-Natural-Deduction.md&2.13&2. Denying the consequent: P$\to$Q, \~Q $\vDash$ \~P
ct09-10-More-Natural-Deduction.md&2.13&3. Hypothetical syllogism: A$\to$B, B$\to$C $\vDash$ A$\to$C
ct09-10-More-Natural-Deduction.md&2.13&4. Disjunctive syllogism: PvQ, \~P $\vDash$ Q and PvQ, \~Q $\vDash$ P
ct09-10-More-Natural-Deduction.md&2.13&5. Dilemma: P$\to$Q, R$\to$S, PvR $\vDash$ QvS
ct09-10-More-Natural-Deduction.md&2.13&6. Conjunction-In: P, Q $\vDash$ P$\&$Q
ct09-10-More-Natural-Deduction.md&2.13&7. Conjunction-Out: P$\&$Q $\vDash$ P and P$\&$Q $\vDash$ Q
ct09-10-More-Natural-Deduction.md&2.13&8. Double negation: P $\equiv$ \~\~P
ct09-10-More-Natural-Deduction.md&2.13&9. De Morgan’s laws \~(PvQ) $\equiv$ \~P$\&$\~Q and \~(P$\&$Q) $\equiv$ \~Pv\~Q
ct09-10-More-Natural-Deduction.md&2.13&10. Contraposition P$\to$Q $\equiv$ \~Q$\to$\~P (denying the consequent)
ct09-10-More-Natural-Deduction.md&2.13&11. Conditional-disjunction exchange P$\to$Q $\equiv$ \~PvQ
ct09-10-More-Natural-Deduction.md&2.13&
ct09-10-More-Natural-Deduction.md&2.14&## Exercise
ct09-10-More-Natural-Deduction.md&2.14&
ct09-10-More-Natural-Deduction.md&2.14&*Show whether this is valid:*
ct09-10-More-Natural-Deduction.md&2.14&
ct09-10-More-Natural-Deduction.md&2.14&I went to my Critical Thinking class instead of having a tea, although I knew that if I go to the Critical Thinking class I will be bored by the logic part. So now I am in the Critical Thinking class and I’m bored by the logic part.
ct09-10-More-Natural-Deduction.md&2.14&
ct09-10-More-Natural-Deduction.md&2.15&## Exercise
ct09-10-More-Natural-Deduction.md&2.15&
ct09-10-More-Natural-Deduction.md&2.15&*Show whether this is valid:*
ct09-10-More-Natural-Deduction.md&2.15&
ct09-10-More-Natural-Deduction.md&2.15&I went to my Critical Thinking class (C) **instead of having a tea (T)**, although I knew that if I go to the Critical Thinking class I will be bored (B) by the logic part. So now I am in the Critical Thinking class (C) and I’m bored (B) by the logic part.
ct09-10-More-Natural-Deduction.md&2.15&
ct09-10-More-Natural-Deduction.md&2.15&. . .
ct09-10-More-Natural-Deduction.md&2.15&
ct09-10-More-Natural-Deduction.md&2.15&C\textbf{\&$\sim$T}  
ct09-10-More-Natural-Deduction.md&2.15&C$\to$B  
ct09-10-More-Natural-Deduction.md&2.15& ---------  
ct09-10-More-Natural-Deduction.md&2.15&C\&B
ct09-10-More-Natural-Deduction.md&2.15&
ct09-10-More-Natural-Deduction.md&2.15&We can in principle ignore the bit about the tea (boldface above), because it appears only once and does not influence the proof.
ct09-10-More-Natural-Deduction.md&2.15&
ct09-10-More-Natural-Deduction.md&2.16&## Natural deduction proof
ct09-10-More-Natural-Deduction.md&2.16&
ct09-10-More-Natural-Deduction.md&2.16&C\&$\sim$T, C$\to$B $\vdash$ C\&B
ct09-10-More-Natural-Deduction.md&2.16&
ct09-10-More-Natural-Deduction.md&2.16&--------------------------------------   ----------------
ct09-10-More-Natural-Deduction.md&2.16&1. C\&$\sim$T						     Premise
ct09-10-More-Natural-Deduction.md&2.16&2. C								     1, \&O
ct09-10-More-Natural-Deduction.md&2.16&3. C$\to$B							     Premise
ct09-10-More-Natural-Deduction.md&2.16&4. B								     2, 3, MP (same as $\to$O)
ct09-10-More-Natural-Deduction.md&2.16&5. C\&B								     2, 4, \&I
ct09-10-More-Natural-Deduction.md&2.16&--------------------------------------   ----------------
ct09-10-More-Natural-Deduction.md&2.16&
ct09-10-More-Natural-Deduction.md&2.16&
ct09-10-More-Natural-Deduction.md&2.17&## How to think about these exercises
ct09-10-More-Natural-Deduction.md&2.17&
ct09-10-More-Natural-Deduction.md&2.17&We did this exercise in the last session. Now have another look at it, and try to solve it “backwards,” by assuming that the conclusion (P) is true.
ct09-10-More-Natural-Deduction.md&2.17&
ct09-10-More-Natural-Deduction.md&2.17&P$\lor$$\sim$Q$\lor$R  
ct09-10-More-Natural-Deduction.md&2.17&R$\to$$\sim$S  
ct09-10-More-Natural-Deduction.md&2.17&S\&Q  
ct09-10-More-Natural-Deduction.md&2.17& ---------------  
ct09-10-More-Natural-Deduction.md&2.17&P
ct09-10-More-Natural-Deduction.md&2.17&
ct09-10-More-Natural-Deduction.md&2.18&## One way to think about it
ct09-10-More-Natural-Deduction.md&2.18&
ct09-10-More-Natural-Deduction.md&2.18&P$\lor$$\sim$Q$\lor$R, R$\to$$\sim$S, S\&Q $\vdash$ P
ct09-10-More-Natural-Deduction.md&2.18&
ct09-10-More-Natural-Deduction.md&2.18&>- I want to show that P. Therefore, I have to begin with a premise that contains P: P$\lor$$\sim$Q$\lor$R.
ct09-10-More-Natural-Deduction.md&2.18&>- If I am to conclude that P, then $\sim$Q$\lor$R must be shown to be false. Then, since the premise is assumed to be true, P must be true (disjunctive syllogism).
ct09-10-More-Natural-Deduction.md&2.18&>- $\sim$Q$\lor$R will be false if both $\sim$Q is false and R is false.
ct09-10-More-Natural-Deduction.md&2.18&>- I know that S\&Q is true (premise), so both S and Q must be true (\&O). If so, then $\sim$Q must be false.
ct09-10-More-Natural-Deduction.md&2.18&>- Now I must show that R is false. I know that R$\to$$\sim$S is true (premise). I also know that S is true (see previous step). Then $\sim$S is false. If $\sim$S is false and R$\to$$\sim$S is true, then R must be false too!
ct09-10-More-Natural-Deduction.md&2.18&>- So now I know that $\sim$Q$\lor$R is false.
ct09-10-More-Natural-Deduction.md&2.18&>- Therefore, P must be true.
ct09-10-More-Natural-Deduction.md&2.18&
ct09-10-More-Natural-Deduction.md&2.19&## P$\lor$$\sim$Q$\lor$R, R$\to$$\sim$S, S$\&$Q $\vdash$ P
ct09-10-More-Natural-Deduction.md&2.19&
ct09-10-More-Natural-Deduction.md&2.19&In order to write it down, we have to re-order it:
ct09-10-More-Natural-Deduction.md&2.19&
ct09-10-More-Natural-Deduction.md&2.19&--------------------------------------- ----------------
ct09-10-More-Natural-Deduction.md&2.19&1. S$\&$Q                               premise
ct09-10-More-Natural-Deduction.md&2.19&2. S                                    1, \&O
ct09-10-More-Natural-Deduction.md&2.19&3. Q                                    1, \&O
ct09-10-More-Natural-Deduction.md&2.19&4. \~\~S                                2, DN
ct09-10-More-Natural-Deduction.md&2.19&5. R$\to$\~S                            premise
ct09-10-More-Natural-Deduction.md&2.19&6. \~R                                  4, 5, MT
ct09-10-More-Natural-Deduction.md&2.19&7. Pv\~QvR                              premise
ct09-10-More-Natural-Deduction.md&2.19&8. Pv\~Q                                6, 7, DS
ct09-10-More-Natural-Deduction.md&2.19&9. \~\~Q                                3, DN
ct09-10-More-Natural-Deduction.md&2.19&10. P                                   9, 8, DS
ct09-10-More-Natural-Deduction.md&2.19&--------------------------------------- ----------------
ct09-10-More-Natural-Deduction.md&2.19&
ct09-10-More-Natural-Deduction.md&2.19&
ct09-10-More-Natural-Deduction.md&3.0&# Natural deduction exercises from old exams
ct09-10-More-Natural-Deduction.md&3.0&
ct09-10-More-Natural-Deduction.md&3.1&## Translate and show validity (1)
ct09-10-More-Natural-Deduction.md&3.1&
ct09-10-More-Natural-Deduction.md&3.1&Translate the following into propositional logic and show, using natural deduction, that it is valid:
ct09-10-More-Natural-Deduction.md&3.1&
ct09-10-More-Natural-Deduction.md&3.1&"Jack and Jill went up the hill. Jack fell down and broke his crown. Jack did not fall down. Therefore, Jill did not come tumbling after."
ct09-10-More-Natural-Deduction.md&3.1&
ct09-10-More-Natural-Deduction.md&3.1&. . .
ct09-10-More-Natural-Deduction.md&3.1&
ct09-10-More-Natural-Deduction.md&3.1&>- Don't be confused by the words. You don't need to understand the text!
ct09-10-More-Natural-Deduction.md&3.1&>- P: Jack and Jill went up the hill. Q: Jack fell down. R: Jack broke his crown. S: Jill came tumbling after.
ct09-10-More-Natural-Deduction.md&3.1&
ct09-10-More-Natural-Deduction.md&3.1&. . .
ct09-10-More-Natural-Deduction.md&3.1&
ct09-10-More-Natural-Deduction.md&3.1&P, Q\&R, $\sim$Q $\vdash$ $\sim$S
ct09-10-More-Natural-Deduction.md&3.1&
ct09-10-More-Natural-Deduction.md&3.2&## Translate and show validity (2)
ct09-10-More-Natural-Deduction.md&3.2&
ct09-10-More-Natural-Deduction.md&3.2&P, Q\&R, $\sim$Q $\vdash$ $\sim$S
ct09-10-More-Natural-Deduction.md&3.2&
ct09-10-More-Natural-Deduction.md&3.2&--------------------------------------- ----------------
ct09-10-More-Natural-Deduction.md&3.2&1. Q\&R                                   premise
ct09-10-More-Natural-Deduction.md&3.2&2. Q                                    1, \&O
ct09-10-More-Natural-Deduction.md&3.2&3. $\sim$Q                                    premise
ct09-10-More-Natural-Deduction.md&3.2&4. $\sim$Q$\lor$$\sim$S                         3, vI
ct09-10-More-Natural-Deduction.md&3.2&5. $\sim$S                                     2, 4, DS
ct09-10-More-Natural-Deduction.md&3.2&--------------------------------------- ----------------
ct09-10-More-Natural-Deduction.md&3.2&
ct09-10-More-Natural-Deduction.md&3.2&. . .
ct09-10-More-Natural-Deduction.md&3.2&
ct09-10-More-Natural-Deduction.md&3.2&What happens here is that *you can derive anything from a contradiction!* (2) and (3) are a contradiction, and, together with a disjunction, which you can always add to anything, you can prove anything you want.
ct09-10-More-Natural-Deduction.md&3.2&
ct09-10-More-Natural-Deduction.md&3.2&Remember: *You can derive anything from a contradiction!*
ct09-10-More-Natural-Deduction.md&3.2&
ct09-10-More-Natural-Deduction.md&3.3&## Any conclusion follows from a contradiction
ct09-10-More-Natural-Deduction.md&3.3&
ct09-10-More-Natural-Deduction.md&3.3&Here is the general way how this works:
ct09-10-More-Natural-Deduction.md&3.3&
ct09-10-More-Natural-Deduction.md&3.3&-------------------------------- ----------------
ct09-10-More-Natural-Deduction.md&3.3&1. A\&$\sim$A                       (premise, contradiction)
ct09-10-More-Natural-Deduction.md&3.3&2. A		                         (1, \&O)
ct09-10-More-Natural-Deduction.md&3.3&3. $\sim$A	                         (1, \&O)
ct09-10-More-Natural-Deduction.md&3.3&4. AvB		                        (2, vI)
ct09-10-More-Natural-Deduction.md&3.3&5. B		                        (3, 4, DS)
ct09-10-More-Natural-Deduction.md&3.3&-------------------------------- ----------------
ct09-10-More-Natural-Deduction.md&3.3&
ct09-10-More-Natural-Deduction.md&3.3&This shows that from a contradictory premise one can by deduction prove *any* statement!
ct09-10-More-Natural-Deduction.md&3.3&
ct09-10-More-Natural-Deduction.md&3.4&## Any conclusion follows from a contradiction
ct09-10-More-Natural-Deduction.md&3.4&
ct09-10-More-Natural-Deduction.md&3.4&Thus:
ct09-10-More-Natural-Deduction.md&3.4&
ct09-10-More-Natural-Deduction.md&3.4&Premise 1: Bananas are yellow  
ct09-10-More-Natural-Deduction.md&3.4&Premise 2: Bananas are not yellow  
ct09-10-More-Natural-Deduction.md&3.4&Premise 3: Bananas are yellow or Hong Kong is in Japan  
ct09-10-More-Natural-Deduction.md&3.4&Conclusion: Hong Kong is in Japan
ct09-10-More-Natural-Deduction.md&3.4&
ct09-10-More-Natural-Deduction.md&3.4&The conclusion here *does* follow from the premises! The argument is valid.
ct09-10-More-Natural-Deduction.md&3.4&
ct09-10-More-Natural-Deduction.md&3.5&## Proof
ct09-10-More-Natural-Deduction.md&3.5&
ct09-10-More-Natural-Deduction.md&3.5&Use natural deduction to prove that:
ct09-10-More-Natural-Deduction.md&3.5&
ct09-10-More-Natural-Deduction.md&3.5&((P\&Q)\&S), (P\&S)$\to$$\sim$R, (R$\lor$T) $\vdash$ T.
ct09-10-More-Natural-Deduction.md&3.5&
ct09-10-More-Natural-Deduction.md&3.5&. . .
ct09-10-More-Natural-Deduction.md&3.5&
ct09-10-More-Natural-Deduction.md&3.5&Easy to see. This is more about how to write things down correctly.
ct09-10-More-Natural-Deduction.md&3.5&
ct09-10-More-Natural-Deduction.md&3.6&## Proof
ct09-10-More-Natural-Deduction.md&3.6&
ct09-10-More-Natural-Deduction.md&3.6&Show, using natural deduction, that the following is valid: Q\&(T$\lor$B), P$\lor$R, Q$\to$S, S$\to$$\sim$R, $\sim$(P\&Q)$\lor$T $\vdash$ T.
ct09-10-More-Natural-Deduction.md&3.6&
ct09-10-More-Natural-Deduction.md&3.6&. . . 
ct09-10-More-Natural-Deduction.md&3.6&
ct09-10-More-Natural-Deduction.md&3.6&--------------------------------------- ----------------
ct09-10-More-Natural-Deduction.md&3.6&1. Q\&(T$\lor$B)	                         Premise
ct09-10-More-Natural-Deduction.md&3.6&2. P$\lor$R			                                Premise
ct09-10-More-Natural-Deduction.md&3.6&3. Q$\to$S			                                 Premise
ct09-10-More-Natural-Deduction.md&3.6&4. S$\to$$\sim$R	                        Premise
ct09-10-More-Natural-Deduction.md&3.6&5. $\sim$(P\&Q)$\lor$T                    Premise
ct09-10-More-Natural-Deduction.md&3.6&6. Q				                                       1, &E
ct09-10-More-Natural-Deduction.md&3.6&7. S				                                       6, 3, $\to$E (MP)
ct09-10-More-Natural-Deduction.md&3.6&8. $\sim$R			                             7, 4, $\to$E
ct09-10-More-Natural-Deduction.md&3.6&9. P				                                       2, 8, vE (DS)
ct09-10-More-Natural-Deduction.md&3.6&10. P\&Q			                             9, 6, \&I
ct09-10-More-Natural-Deduction.md&3.6&11. T				                                   5, 10, vE (DS)
ct09-10-More-Natural-Deduction.md&3.6&--------------------------------------- ----------------
ct09-10-More-Natural-Deduction.md&3.6&
ct09-10-More-Natural-Deduction.md&3.7&## Proof (1)
ct09-10-More-Natural-Deduction.md&3.7&
ct09-10-More-Natural-Deduction.md&3.7&Consider the following argument:
ct09-10-More-Natural-Deduction.md&3.7&
ct09-10-More-Natural-Deduction.md&3.7&"God is a being of which nothing greater can be conceived. We understand the term *God.* If we understand this term, then God exists in the understanding. If God exists in the understanding, but not in reality, then God is not a being of which nothing greater can be conceived. Therefore, God exists in reality." -- Formalize this in the language of sentential^[Sentential logic = propositional logic.] logic and construct a proof of this argument!
ct09-10-More-Natural-Deduction.md&3.7&
ct09-10-More-Natural-Deduction.md&3.8&## Proof (2)
ct09-10-More-Natural-Deduction.md&3.8&
ct09-10-More-Natural-Deduction.md&3.8&N: God is a being of which nothing greater can be conceived.  
ct09-10-More-Natural-Deduction.md&3.8&U: We understand the term God.  
ct09-10-More-Natural-Deduction.md&3.8&E: God exists in the understanding.  
ct09-10-More-Natural-Deduction.md&3.8&R: God exists in reality.
ct09-10-More-Natural-Deduction.md&3.8&
ct09-10-More-Natural-Deduction.md&3.8&1. N
ct09-10-More-Natural-Deduction.md&3.8&2. U
ct09-10-More-Natural-Deduction.md&3.8&3. U$\to$E
ct09-10-More-Natural-Deduction.md&3.8&4. (E\&$\sim$R)$\to$$\sim$N
ct09-10-More-Natural-Deduction.md&3.8&5. Conclusion: R (?)
ct09-10-More-Natural-Deduction.md&3.8&
ct09-10-More-Natural-Deduction.md&3.9&## Proof (3)
ct09-10-More-Natural-Deduction.md&3.9&
ct09-10-More-Natural-Deduction.md&3.9&N: God is a being of which nothing greater can be conceived.  
ct09-10-More-Natural-Deduction.md&3.9&U: We understand the term God.  
ct09-10-More-Natural-Deduction.md&3.9&E: God exists in the understanding.  
ct09-10-More-Natural-Deduction.md&3.9&R: God exists in reality.
ct09-10-More-Natural-Deduction.md&3.9&
ct09-10-More-Natural-Deduction.md&3.9&--------------------------------------- ----------------
ct09-10-More-Natural-Deduction.md&3.9&1. N					                Premise
ct09-10-More-Natural-Deduction.md&3.9&2. U					                Premise
ct09-10-More-Natural-Deduction.md&3.9&3. U$\to$E				                Premise
ct09-10-More-Natural-Deduction.md&3.9&4. (E\&$\sim$R)$\to$$\sim$N               Premise
ct09-10-More-Natural-Deduction.md&3.9&5. $\sim$(E\&$\sim$R)	                1, 4, MT
ct09-10-More-Natural-Deduction.md&3.9&6. $\sim$E$\lor$R		                5, DeM
ct09-10-More-Natural-Deduction.md&3.9&7. E					                2, 3, MP
ct09-10-More-Natural-Deduction.md&3.9&8. R					                6, 7, DS
ct09-10-More-Natural-Deduction.md&3.9&--------------------------------------- ----------------
ct09-10-More-Natural-Deduction.md&3.9&
ct09-10-More-Natural-Deduction.md&3.10&## What to learn from this
ct09-10-More-Natural-Deduction.md&3.10&
ct09-10-More-Natural-Deduction.md&3.10&- If the exercise is about validity, generally don't try to *understand* the argument with common sense. You don't even need to understand the words.
ct09-10-More-Natural-Deduction.md&3.10&- Trying to figure out the meaning will just cost you time.
ct09-10-More-Natural-Deduction.md&3.10&- Just translate the argument into formal logic (propositional or predicate logic) and then prove its validity formally.
ct09-10-More-Natural-Deduction.md&3.10&- The same applies to conditional arguments, which we talked about earlier in this course.
ct09-10-More-Natural-Deduction.md&3.10&
ct09-10-More-Natural-Deduction.md&3.11&## Inference (1)
ct09-10-More-Natural-Deduction.md&3.11&
ct09-10-More-Natural-Deduction.md&3.11&What can be validly inferred from the following premises?
ct09-10-More-Natural-Deduction.md&3.11&
ct09-10-More-Natural-Deduction.md&3.11&- (P$\lor$Q)$\to$$\sim$R
ct09-10-More-Natural-Deduction.md&3.11&- P\&S
ct09-10-More-Natural-Deduction.md&3.11&- T$\to$R
ct09-10-More-Natural-Deduction.md&3.11&
ct09-10-More-Natural-Deduction.md&3.11&1. R
ct09-10-More-Natural-Deduction.md&3.11&2. T
ct09-10-More-Natural-Deduction.md&3.11&3. $\sim$T\&S
ct09-10-More-Natural-Deduction.md&3.11&4. P$\to$R
ct09-10-More-Natural-Deduction.md&3.11&
ct09-10-More-Natural-Deduction.md&3.11&. . . 
ct09-10-More-Natural-Deduction.md&3.11&
ct09-10-More-Natural-Deduction.md&3.11&3 (explanation on next page).
ct09-10-More-Natural-Deduction.md&3.11&
ct09-10-More-Natural-Deduction.md&3.12&## Inference (2)
ct09-10-More-Natural-Deduction.md&3.12&
ct09-10-More-Natural-Deduction.md&3.12&--------------------------------------- ----------------
ct09-10-More-Natural-Deduction.md&3.12&1. (P$\lor$Q)$\to$$\sim$R               Premise
ct09-10-More-Natural-Deduction.md&3.12&2. P\&S					                Premise
ct09-10-More-Natural-Deduction.md&3.12&3. T$\to$R				                Premise
ct09-10-More-Natural-Deduction.md&3.12&4. P					                2, \&E
ct09-10-More-Natural-Deduction.md&3.12&5. $\sim$R				                1, 4, MP
ct09-10-More-Natural-Deduction.md&3.12&6. $\sim$T				                3, 5, MT
ct09-10-More-Natural-Deduction.md&3.12&7. S					                2, \&E
ct09-10-More-Natural-Deduction.md&3.12&8. $\sim$T\&S			                6, 7, \&I
ct09-10-More-Natural-Deduction.md&3.12&--------------------------------------- ----------------
ct09-10-More-Natural-Deduction.md&3.12&
ct09-10-More-Natural-Deduction.md&3.12&
ct09-10-More-Natural-Deduction.md&4.0&# Other logic exercises from old exams
ct09-10-More-Natural-Deduction.md&4.0&
ct09-10-More-Natural-Deduction.md&4.1&## Valid?
ct09-10-More-Natural-Deduction.md&4.1&
ct09-10-More-Natural-Deduction.md&4.1&Is the following valid?
ct09-10-More-Natural-Deduction.md&4.1&
ct09-10-More-Natural-Deduction.md&4.1&$\sim$(P$\to$Q)$\lor$R, ($\sim$R\&P) $\leftrightarrow$ $\sim$Q $\vdash$ R
ct09-10-More-Natural-Deduction.md&4.1&
ct09-10-More-Natural-Deduction.md&4.1&Use a truth table.
ct09-10-More-Natural-Deduction.md&4.1&
ct09-10-More-Natural-Deduction.md&4.1&
ct09-10-More-Natural-Deduction.md&4.2&## Entailment
ct09-10-More-Natural-Deduction.md&4.2&
ct09-10-More-Natural-Deduction.md&4.2&If P entails Q, then:
ct09-10-More-Natural-Deduction.md&4.2&
ct09-10-More-Natural-Deduction.md&4.2&A. P is inconsistent with $\sim$Q  
ct09-10-More-Natural-Deduction.md&4.2&B. P is consistent with $\sim$Q  
ct09-10-More-Natural-Deduction.md&4.2&C. If Q entails R, P is consistent with $\sim$R  
ct09-10-More-Natural-Deduction.md&4.2&D. All of the above
ct09-10-More-Natural-Deduction.md&4.2&
ct09-10-More-Natural-Deduction.md&4.2&. . .
ct09-10-More-Natural-Deduction.md&4.2&
ct09-10-More-Natural-Deduction.md&4.2&Answer: A.
ct09-10-More-Natural-Deduction.md&4.2&
ct09-10-More-Natural-Deduction.md&4.3&## Translation
ct09-10-More-Natural-Deduction.md&4.3&
ct09-10-More-Natural-Deduction.md&4.3&"Our opinions differ on this matter, and one and only one of us is correct." -- Which is the correct translation of this sentence? (O: Our opinions differ in this matter; Y: You are correct; I: I am correct)
ct09-10-More-Natural-Deduction.md&4.3&
ct09-10-More-Natural-Deduction.md&4.3&A. O\&(Y$\lor$I)  
ct09-10-More-Natural-Deduction.md&4.3&B. O\&(I$\leftrightarrow$$\sim$Y)  
ct09-10-More-Natural-Deduction.md&4.3&C. O\&$\sim$(Y\&I)  
ct09-10-More-Natural-Deduction.md&4.3&D. O\&(I$\leftrightarrow$Y)  
ct09-10-More-Natural-Deduction.md&4.3&
ct09-10-More-Natural-Deduction.md&4.3&. . .
ct09-10-More-Natural-Deduction.md&4.3&
ct09-10-More-Natural-Deduction.md&4.3&Answer B.
ct09-10-More-Natural-Deduction.md&4.3&
ct09-10-More-Natural-Deduction.md&4.4&## Evaluation/model
ct09-10-More-Natural-Deduction.md&4.4&
ct09-10-More-Natural-Deduction.md&4.4&Which of the following is an assignment of truth values (evaluation/model) on which
ct09-10-More-Natural-Deduction.md&4.4&((P$\to$P)$\to$P) $\to$ ((P$\to$R)$\to$Q) is *false?*
ct09-10-More-Natural-Deduction.md&4.4&
ct09-10-More-Natural-Deduction.md&4.4&A. P=T, Q=F, R=T  
ct09-10-More-Natural-Deduction.md&4.4&B. P=T, Q=T, R=T  
ct09-10-More-Natural-Deduction.md&4.4&C. P=F, Q=F, R=F  
ct09-10-More-Natural-Deduction.md&4.4&D. P=T, Q=F, R=F  
ct09-10-More-Natural-Deduction.md&4.4&
ct09-10-More-Natural-Deduction.md&4.4&. . .
ct09-10-More-Natural-Deduction.md&4.4&
ct09-10-More-Natural-Deduction.md&4.4&A.
ct09-10-More-Natural-Deduction.md&4.4&
ct09-10-More-Natural-Deduction.md&4.5&## Translation and validity
ct09-10-More-Natural-Deduction.md&4.5&
ct09-10-More-Natural-Deduction.md&4.5&"If the world is disordered, it cannot be reformed unless a sage appears. But no sage can appear if the world is disordered. It follows that the world cannot be reformed if it is disordered." -- Show the validity of this argument.
ct09-10-More-Natural-Deduction.md&4.5&
ct09-10-More-Natural-Deduction.md&4.5&. . .
ct09-10-More-Natural-Deduction.md&4.5&
ct09-10-More-Natural-Deduction.md&4.5&D: World is disordered. R: World can be reformed. S: A sage appears.
ct09-10-More-Natural-Deduction.md&4.5&
ct09-10-More-Natural-Deduction.md&4.5&D$\to$($\sim$S$\to$$\sim$R)  
ct09-10-More-Natural-Deduction.md&4.5&D$\to$$\sim$S  
ct09-10-More-Natural-Deduction.md&4.5& ------------------------------  
ct09-10-More-Natural-Deduction.md&4.5&D$\to$$\sim$R
ct09-10-More-Natural-Deduction.md&4.5&
ct09-10-More-Natural-Deduction.md&4.5&The easiest way to show this is with a truth table. A natural deduction solution would require an implication-in, which needs an assumption. We have not discussed this in this class. You can see how to do this in the HKU Think online resource (but you don’t need to).
ct09-10-More-Natural-Deduction.md&4.5&
ct09-10-More-Natural-Deduction.md&4.6&## Tautologies
ct09-10-More-Natural-Deduction.md&4.6&
ct09-10-More-Natural-Deduction.md&4.6&![](graphics/13-exam-1.png)\ 
ct09-10-More-Natural-Deduction.md&4.6&
ct09-10-More-Natural-Deduction.md&4.6&. . .
ct09-10-More-Natural-Deduction.md&4.6&
ct09-10-More-Natural-Deduction.md&4.6&All of the above.
ct09-10-More-Natural-Deduction.md&4.6&
ct09-10-More-Natural-Deduction.md&4.7&## Entailment
ct09-10-More-Natural-Deduction.md&4.7&
ct09-10-More-Natural-Deduction.md&4.7&![](graphics/13-exam-2.png)\ 
ct09-10-More-Natural-Deduction.md&4.7&
ct09-10-More-Natural-Deduction.md&4.7&. . .
ct09-10-More-Natural-Deduction.md&4.7&
ct09-10-More-Natural-Deduction.md&4.7&X entails Y if: When X is true, Y has to be true. *For which expression is this the case?*
ct09-10-More-Natural-Deduction.md&4.7&
ct09-10-More-Natural-Deduction.md&4.7&. . . 
ct09-10-More-Natural-Deduction.md&4.7&
ct09-10-More-Natural-Deduction.md&4.7&This is the case only for (S$\lor$$\sim$S) because this is *always* true, and therefore it is also true if the premise is true. Every expression entails a tautology!
ct09-10-More-Natural-Deduction.md&4.7&
ct09-10-More-Natural-Deduction.md&4.8&## Joint entailment
ct09-10-More-Natural-Deduction.md&4.8&
ct09-10-More-Natural-Deduction.md&4.8&![](graphics/13-exam-3.png)\ 
ct09-10-More-Natural-Deduction.md&4.8&
ct09-10-More-Natural-Deduction.md&4.8&. . .
ct09-10-More-Natural-Deduction.md&4.8&
ct09-10-More-Natural-Deduction.md&4.8&Q (obviously), since $\sim$Q is a premise. No expression can entail its negation (except if the expression is a contradiction, and the negation is, therefore, a tautology).
ct09-10-More-Natural-Deduction.md&4.8&
ct09-10-More-Natural-Deduction.md&4.9&## Translation
ct09-10-More-Natural-Deduction.md&4.9&
ct09-10-More-Natural-Deduction.md&4.9&![](graphics/13-exam-4.png)\ 
ct09-10-More-Natural-Deduction.md&4.9&
ct09-10-More-Natural-Deduction.md&4.9&. . .
ct09-10-More-Natural-Deduction.md&4.9&
ct09-10-More-Natural-Deduction.md&4.9&Answer C. You can also see this because it is the *only* pair of formulas where the two parts of the pair are actually logically equivalent! (DeMorgan law!)
ct09-10-More-Natural-Deduction.md&4.9&
ct09-10-More-Natural-Deduction.md&4.10&## Consistency and entailment (1)
ct09-10-More-Natural-Deduction.md&4.10&
ct09-10-More-Natural-Deduction.md&4.10&![](graphics/13-exam-5.png)\ 
ct09-10-More-Natural-Deduction.md&4.10&
ct09-10-More-Natural-Deduction.md&4.10&. . .
ct09-10-More-Natural-Deduction.md&4.10&
ct09-10-More-Natural-Deduction.md&4.10&About all the answers: Assuming P: "you love me"; Q: "I love you."
ct09-10-More-Natural-Deduction.md&4.10&
ct09-10-More-Natural-Deduction.md&4.10&>- Then statement (1) is P$\lor$$\sim$P. This is a tautology.
ct09-10-More-Natural-Deduction.md&4.10&>- Entailment: If the premises are true, the conclusion *must* be true. If the conclusion is a tautology, it is *always* true (this is what tautology means).
ct09-10-More-Natural-Deduction.md&4.10&>- Therefore, *every statement entails a tautology.*
ct09-10-More-Natural-Deduction.md&4.10&>- The correct answer is B.
ct09-10-More-Natural-Deduction.md&4.10&>- Since only one answer can be correct, we are done.
ct09-10-More-Natural-Deduction.md&4.10&
ct09-10-More-Natural-Deduction.md&4.11&## Consistency and entailment (2)
ct09-10-More-Natural-Deduction.md&4.11&
ct09-10-More-Natural-Deduction.md&4.11&![](graphics/13-exam-5.png)\ 
ct09-10-More-Natural-Deduction.md&4.11&
ct09-10-More-Natural-Deduction.md&4.11&- For completeness: A tautology cannot be inconsistent with any statement (except for a contradiction), since every contingent statement is true sometimes, and the tautology is true always, that is, surely also at the same time as the other statement.
ct09-10-More-Natural-Deduction.md&4.11&
ct09-10-More-Natural-Deduction.md&4.12&## Is the following valid? Examine it with a truth table.
ct09-10-More-Natural-Deduction.md&4.12&
ct09-10-More-Natural-Deduction.md&4.12&![](graphics/13-exam-6.png)\ 
ct09-10-More-Natural-Deduction.md&4.12&
ct09-10-More-Natural-Deduction.md&4.12&. . . 
ct09-10-More-Natural-Deduction.md&4.12&
ct09-10-More-Natural-Deduction.md&4.12&>- You can see that PvQ must be true (premise 1).
ct09-10-More-Natural-Deduction.md&4.12&>- If P is true, then Q must be true (premise 2). So either P and Q are true, or only Q.
ct09-10-More-Natural-Deduction.md&4.12&>- In any case, Q is true. Therefore, (RvQ) is true (premise 3).
ct09-10-More-Natural-Deduction.md&4.12&>- But (RvQ) will always be true when Q is true, no matter what the R is.
ct09-10-More-Natural-Deduction.md&4.12&>- Therefore, it can happen that the R is actually false, although all premises are true.
ct09-10-More-Natural-Deduction.md&4.12&>- Therefore, the argument is *not* valid.
ct09-10-More-Natural-Deduction.md&4.12&
ct09-10-More-Natural-Deduction.md&4.13&## Entailment and consistency
ct09-10-More-Natural-Deduction.md&4.13&
ct09-10-More-Natural-Deduction.md&4.13&Suppose S1 is the formula (P$\lor$Q) and S2 is the expression:
ct09-10-More-Natural-Deduction.md&4.13&(((P\&R)$\lor$(Q\&R))$\lor$((P\&$\sim$R)$\lor$(Q\&$\sim$R))).
ct09-10-More-Natural-Deduction.md&4.13&
ct09-10-More-Natural-Deduction.md&4.13&Decide, by means of a truth table, which of the following logical relations hold between S1 and S2 and which do not hold (multiple answers possible!):
ct09-10-More-Natural-Deduction.md&4.13&
ct09-10-More-Natural-Deduction.md&4.13&1. S1 entails S2.
ct09-10-More-Natural-Deduction.md&4.13&2. S2 entails S1.
ct09-10-More-Natural-Deduction.md&4.13&3. S1 is logically equivalent to S2.
ct09-10-More-Natural-Deduction.md&4.13&4. S1 is inconsistent with S2.
ct09-10-More-Natural-Deduction.md&4.13&
ct09-10-More-Natural-Deduction.md&4.13&. . .
ct09-10-More-Natural-Deduction.md&4.13&
ct09-10-More-Natural-Deduction.md&4.13&The two expressions are equivalent, and so they also entail each other. The right answer is therefore that the logical relations 1, 2, and 3 hold; but 4 is not the case.
ct09-10-More-Natural-Deduction.md&4.13&
ct09-10-More-Natural-Deduction.md&4.14&## Validity (1)
ct09-10-More-Natural-Deduction.md&4.14&
ct09-10-More-Natural-Deduction.md&4.14&![](graphics/13-exam-8.png)\ 
ct09-10-More-Natural-Deduction.md&4.14&
ct09-10-More-Natural-Deduction.md&4.14&Is this argument valid? Explain your answer!
ct09-10-More-Natural-Deduction.md&4.14&
ct09-10-More-Natural-Deduction.md&4.14&. . .
ct09-10-More-Natural-Deduction.md&4.14&
ct09-10-More-Natural-Deduction.md&4.14&The easiest way to do this is with a Venn diagram!
ct09-10-More-Natural-Deduction.md&4.14&
ct09-10-More-Natural-Deduction.md&4.15&## Validity (2)
ct09-10-More-Natural-Deduction.md&4.15&
ct09-10-More-Natural-Deduction.md&4.15&![](graphics/13-exam-8.png)\ 
ct09-10-More-Natural-Deduction.md&4.15&
ct09-10-More-Natural-Deduction.md&4.15&\begin{venndiagram3sets}[labelA={Lawful}, labelB={Respect}, labelC={Rebels}]
ct09-10-More-Natural-Deduction.md&4.15&\fillOnlyA
ct09-10-More-Natural-Deduction.md&4.15&\fillACapC
ct09-10-More-Natural-Deduction.md&4.15&\setpostvennhook{
ct09-10-More-Natural-Deduction.md&4.15&\draw[<-] (labelOnlyBC) -- ++(0:2.5cm)
ct09-10-More-Natural-Deduction.md&4.15&node[right,text width=4cm,align=flush left]
ct09-10-More-Natural-Deduction.md&4.15&{Something could be here, so the argument is not valid!};}
ct09-10-More-Natural-Deduction.md&4.15&\end{venndiagram3sets}
ct09-10-More-Natural-Deduction.md&4.15&
ct09-10-More-Natural-Deduction.md&4.16&## Tools for checking entailment
ct09-10-More-Natural-Deduction.md&4.16&
ct09-10-More-Natural-Deduction.md&4.16&Generally, you have three tools for checking entailment (or validity):
ct09-10-More-Natural-Deduction.md&4.16&
ct09-10-More-Natural-Deduction.md&4.16&1. Truth tables (for propositional logic)
ct09-10-More-Natural-Deduction.md&4.16&    - Work always but a lot of effort to apply to complex expressions.
ct09-10-More-Natural-Deduction.md&4.16&2. Natural deduction (for propositional logic)
ct09-10-More-Natural-Deduction.md&4.16&    - You might not always be able to derive the result you want, but much faster than truth tables *if* you can do it.
ct09-10-More-Natural-Deduction.md&4.16&3. Venn diagrams (only for syllogisms)
ct09-10-More-Natural-Deduction.md&4.16&    - You *have* to use Venn diagrams when your argument is about syllogisms with quantifiers (‘all’, ‘some’).
ct09-10-More-Natural-Deduction.md&4.16&
ct09-10-More-Natural-Deduction.md&4.16&Choose wisely which tool to use for every exercise!
ct09-10-More-Natural-Deduction.md&4.16&
ct09-10-More-Natural-Deduction.md&4.17&## Entailment and consistency
ct09-10-More-Natural-Deduction.md&4.17&
ct09-10-More-Natural-Deduction.md&4.17&S1: ((P$\lor$Q)$\to$R)  
ct09-10-More-Natural-Deduction.md&4.17&S2: (R$\to$$\sim$(P\&Q))
ct09-10-More-Natural-Deduction.md&4.17&
ct09-10-More-Natural-Deduction.md&4.17&Use a truth table to find out which is true:
ct09-10-More-Natural-Deduction.md&4.17&
ct09-10-More-Natural-Deduction.md&4.17&1. S1 entails S2
ct09-10-More-Natural-Deduction.md&4.17&2. S2 entails S1
ct09-10-More-Natural-Deduction.md&4.17&3. S1 is inconsistent with S2
ct09-10-More-Natural-Deduction.md&4.17&
ct09-10-More-Natural-Deduction.md&4.18&## Assignment of truth values
ct09-10-More-Natural-Deduction.md&4.18&
ct09-10-More-Natural-Deduction.md&4.18&Which of the following is an assignment of truth values on which
ct09-10-More-Natural-Deduction.md&4.18&
ct09-10-More-Natural-Deduction.md&4.18&(P$\to$$\sim$P) $\leftrightarrow$ ((P\&Q)$\to$$\sim$Q) is *false?*
ct09-10-More-Natural-Deduction.md&4.18&
ct09-10-More-Natural-Deduction.md&4.18&1. P=T, Q=T
ct09-10-More-Natural-Deduction.md&4.18&2. P=T, Q=F
ct09-10-More-Natural-Deduction.md&4.18&3. P=F, Q=T
ct09-10-More-Natural-Deduction.md&4.18&4. P=F, Q=F
ct09-10-More-Natural-Deduction.md&4.18&
ct09-10-More-Natural-Deduction.md&4.19&## Consistency
ct09-10-More-Natural-Deduction.md&4.19&
ct09-10-More-Natural-Deduction.md&4.19&![](graphics/13-exam-9.png)\ 
ct09-10-More-Natural-Deduction.md&4.19&
ct09-10-More-Natural-Deduction.md&4.19&. . .
ct09-10-More-Natural-Deduction.md&4.19&
ct09-10-More-Natural-Deduction.md&4.19&*Inconsistent* means that when (P$\lor$Q) becomes true, the other expression should always be false. That is, it should be false if either P, or Q, or both are true. (Use a truth table if not sure.)
ct09-10-More-Natural-Deduction.md&4.19&
ct09-10-More-Natural-Deduction.md&4.19&Also, using the conditional-disjunction-exchange rule, we know that P$\lor$Q $\equiv$ $\sim$P$\to$Q. Therefore, option C is clearly inconsistent, because it is the negation of that.
ct09-10-More-Natural-Deduction.md&4.19&
ct09-10-More-Natural-Deduction.md&4.20&## Translation (1)
ct09-10-More-Natural-Deduction.md&4.20&
ct09-10-More-Natural-Deduction.md&4.20&"If neither the first witness nor the second witness is telling a lie, then the third witness must be mistaken or her statement is not inconsistent with either that of the first or that of the second witness."
ct09-10-More-Natural-Deduction.md&4.20&
ct09-10-More-Natural-Deduction.md&4.20&F: The first witness is telling a lie.
ct09-10-More-Natural-Deduction.md&4.20&S: The second witness is telling a lie.
ct09-10-More-Natural-Deduction.md&4.20&T: The third witness is mistaken.
ct09-10-More-Natural-Deduction.md&4.20&P: The third witness' statement is inconsistent with that of the first witness.
ct09-10-More-Natural-Deduction.md&4.20&Q: The third witness' statement is inconsistent with that of the second witness.
ct09-10-More-Natural-Deduction.md&4.20&
ct09-10-More-Natural-Deduction.md&4.20&![](graphics/13-exam-10.png)\ 
ct09-10-More-Natural-Deduction.md&4.20&
ct09-10-More-Natural-Deduction.md&4.21&## Translation (2)
ct09-10-More-Natural-Deduction.md&4.21&
ct09-10-More-Natural-Deduction.md&4.21&"If neither the first witness nor the second witness is telling a lie, then the third witness must be mistaken or her statement is not inconsistent with either that of the first or that of the second witness."
ct09-10-More-Natural-Deduction.md&4.21&
ct09-10-More-Natural-Deduction.md&4.21&Rephrase and expand:
ct09-10-More-Natural-Deduction.md&4.21&
ct09-10-More-Natural-Deduction.md&4.21&>- If the first witness is not telling a lie AND the second witness is not telling a lie \~F\&\~S $\equiv$ \~(F$\lor$S)
ct09-10-More-Natural-Deduction.md&4.21&>- then the third witness must be mistaken: T, OR
ct09-10-More-Natural-Deduction.md&4.21&>- her statement is not inconsistent with that of the first OR her statement is not inconsistent with that of the second: \~P$\lor$\~Q $\equiv$ \~(P\&Q)
ct09-10-More-Natural-Deduction.md&4.21&
ct09-10-More-Natural-Deduction.md&4.22&## Translation (3)
ct09-10-More-Natural-Deduction.md&4.22&
ct09-10-More-Natural-Deduction.md&4.22&- If the first witness is not telling a lie AND the second witness is not telling a lie \~F\&\~S $\equiv$ \~(F$\lor$S)
ct09-10-More-Natural-Deduction.md&4.22&- then the third witness must be mistaken: T, OR
ct09-10-More-Natural-Deduction.md&4.22&- her statement is not inconsistent with that of the first OR her statement is not inconsistent with that of the second: \~P$\lor$\~Q $\equiv$ \~(P\&Q)
ct09-10-More-Natural-Deduction.md&4.22&
ct09-10-More-Natural-Deduction.md&4.22&![](graphics/13-exam-10.png)\ 
ct09-10-More-Natural-Deduction.md&4.22&
ct09-10-More-Natural-Deduction.md&4.22&. . .
ct09-10-More-Natural-Deduction.md&4.22&
ct09-10-More-Natural-Deduction.md&4.22&The right answer is D.
ct09-10-More-Natural-Deduction.md&4.22&
ct09-10-More-Natural-Deduction.md&4.23&## True and False
ct09-10-More-Natural-Deduction.md&4.23&
ct09-10-More-Natural-Deduction.md&4.23&Suppose a formula containing P and Q is true if and only if both P and Q are false. Which of the following is logically equivalent to the formula?
ct09-10-More-Natural-Deduction.md&4.23&
ct09-10-More-Natural-Deduction.md&4.23&1. (($\sim$P$\lor$$\sim$Q)\&(P$\lor$Q))
ct09-10-More-Natural-Deduction.md&4.23&2. (($\sim$P$\lor$$\sim$Q)\&($\sim$P$\lor$Q))
ct09-10-More-Natural-Deduction.md&4.23&3. (($\sim$P\&$\sim$Q)\&$\sim$($\sim$P$\to$Q))
ct09-10-More-Natural-Deduction.md&4.23&4. (($\sim$P\&$\sim$Q)$\lor$(P\&Q))
ct09-10-More-Natural-Deduction.md&4.23&
ct09-10-More-Natural-Deduction.md&4.23&. . .
ct09-10-More-Natural-Deduction.md&4.23&
ct09-10-More-Natural-Deduction.md&4.23&>- Which of the options is true if and only if both P and Q are false?
ct09-10-More-Natural-Deduction.md&4.23&>- Option (a) would be false if both were false because of ... \&(P$\lor$Q).
ct09-10-More-Natural-Deduction.md&4.23&>- Option (b) would be true if P=T and Q=F.
ct09-10-More-Natural-Deduction.md&4.23&>- Option (c) would be true if both P and Q were false.
ct09-10-More-Natural-Deduction.md&4.23&>- Option (d) would also be true if both were true.
ct09-10-More-Natural-Deduction.md&4.23&>- So the answer is C.
ct09-10-More-Natural-Deduction.md&4.23&
ct09-10-More-Natural-Deduction.md&4.24&## Translation
ct09-10-More-Natural-Deduction.md&4.24&
ct09-10-More-Natural-Deduction.md&4.24&A: Andy Warhol is a great artist. W: Ai Wei Wei is a great artist. E: One should go to the Andy Warhol and Ai Wei Wei exhibition in Melbourne. M: One should go to Melbourne.
ct09-10-More-Natural-Deduction.md&4.24&
ct09-10-More-Natural-Deduction.md&4.24&"If Andy Warhol and Ai Wei Wei are not both great artists, it is not true that one should go to the Andy Warhol and Ai Wei Wei exhibition in Melbourne."
ct09-10-More-Natural-Deduction.md&4.24&
ct09-10-More-Natural-Deduction.md&4.24&![](graphics/13-exam-11.png)\ 
ct09-10-More-Natural-Deduction.md&4.24&
ct09-10-More-Natural-Deduction.md&4.24&. . .
ct09-10-More-Natural-Deduction.md&4.24&
ct09-10-More-Natural-Deduction.md&4.24&Answer B.
ct09-10-More-Natural-Deduction.md&4.24&
ct09-10-More-Natural-Deduction.md&4.25&## Joint entailment
ct09-10-More-Natural-Deduction.md&4.25&
ct09-10-More-Natural-Deduction.md&4.25&![](graphics/13-exam-12.png)\ 
ct09-10-More-Natural-Deduction.md&4.25&
ct09-10-More-Natural-Deduction.md&4.25&. . .
ct09-10-More-Natural-Deduction.md&4.25&
ct09-10-More-Natural-Deduction.md&4.25&Answer D.
ct09-10-More-Natural-Deduction.md&4.25&
ct09-10-More-Natural-Deduction.md&4.26&## New connective
ct09-10-More-Natural-Deduction.md&4.26&
ct09-10-More-Natural-Deduction.md&4.26&![](graphics/13-exam-13.png)\ 
ct09-10-More-Natural-Deduction.md&4.26&
ct09-10-More-Natural-Deduction.md&4.26&Consider the two formulas:
ct09-10-More-Natural-Deduction.md&4.26&
ct09-10-More-Natural-Deduction.md&4.26&1. (P\*Q) \* Q
ct09-10-More-Natural-Deduction.md&4.26&2. Q \* (Q\*P)
ct09-10-More-Natural-Deduction.md&4.26&
ct09-10-More-Natural-Deduction.md&4.26&Does (1) entail (2)? Does (2) entail (1)? Use a truth table to justify your answers.
ct09-10-More-Natural-Deduction.md&4.26&
ct09-10-More-Natural-Deduction.md&4.27&## Consistency
ct09-10-More-Natural-Deduction.md&4.27&
ct09-10-More-Natural-Deduction.md&4.27&Two of the following four formulas are *not* logically consistent with each other. Use a truth table to determine which.
ct09-10-More-Natural-Deduction.md&4.27&
ct09-10-More-Natural-Deduction.md&4.27&![](graphics/13-exam-14.png)\ 
ct09-10-More-Natural-Deduction.md&4.27&
ct09-10-More-Natural-Deduction.md&4.28&## Valid argument
ct09-10-More-Natural-Deduction.md&4.28&
ct09-10-More-Natural-Deduction.md&4.28&Arrange the following formulas into a valid argument form. That is, choose one of them to be the conclusion and the other two to be the premises, so that the resulting argument form is valid. Draw a truth table to verify your answer.
ct09-10-More-Natural-Deduction.md&4.28&
ct09-10-More-Natural-Deduction.md&4.28&![](graphics/13-exam-15.png)\ 
ct09-10-More-Natural-Deduction.md&4.28&
ct09-10-More-Natural-Deduction.md&4.29&## Validity
ct09-10-More-Natural-Deduction.md&4.29&
ct09-10-More-Natural-Deduction.md&4.29&Consider the following argument:
ct09-10-More-Natural-Deduction.md&4.29&
ct09-10-More-Natural-Deduction.md&4.29&Premise: All Fs are Gs  
ct09-10-More-Natural-Deduction.md&4.29&Premise: Some G is an H  
ct09-10-More-Natural-Deduction.md&4.29&Premise: All Hs are Ks  
ct09-10-More-Natural-Deduction.md&4.29& -------------------------------------  
ct09-10-More-Natural-Deduction.md&4.29&Conclusion: All Fs are Ks
ct09-10-More-Natural-Deduction.md&4.29&
ct09-10-More-Natural-Deduction.md&4.29&Is this valid? If not, would it become valid if we added one of the premises:
ct09-10-More-Natural-Deduction.md&4.29&
ct09-10-More-Natural-Deduction.md&4.29&- No G is not an H; *or*
ct09-10-More-Natural-Deduction.md&4.29&- Some F is a K?
ct09-10-More-Natural-Deduction.md&4.29&
ct09-10-More-Natural-Deduction.md&4.29&. . .
ct09-10-More-Natural-Deduction.md&4.29&
ct09-10-More-Natural-Deduction.md&4.29&This is a *thinking* exercise. We don't have the tools to solve it formally, but we can easily see the solution.
ct09-10-More-Natural-Deduction.md&4.29&
ct09-10-More-Natural-Deduction.md&4.29&If all Fs are Gs, and all Hs are Ks, then for the conclusion to be true (all Fs are Ks), it would be necessary that all Gs are Hs. The second premise is not sufficient for that. But the additional premise: No G is not an H means exactly that: All Gs are Hs. And so the argument becomes valid by adding the first of the two additional premises.
ct09-10-More-Natural-Deduction.md&4.29&
ct09-10-More-Natural-Deduction.md&4.30&## Translation
ct09-10-More-Natural-Deduction.md&4.30&
ct09-10-More-Natural-Deduction.md&4.30&"We will go to Ocean Park for your birthday if you want to go, unless it's raining or you fail your Critical Thinking final exam."
ct09-10-More-Natural-Deduction.md&4.30&
ct09-10-More-Natural-Deduction.md&4.30&O: We will go to Ocean Park; W: You want to go to Ocean Park; R: It's raining; F: You fail your Critical Thinking final exam.
ct09-10-More-Natural-Deduction.md&4.30&
ct09-10-More-Natural-Deduction.md&4.30&![](graphics/13-exam-17.png)\ 
ct09-10-More-Natural-Deduction.md&4.30&
ct09-10-More-Natural-Deduction.md&4.30&. . .
ct09-10-More-Natural-Deduction.md&4.30&
ct09-10-More-Natural-Deduction.md&4.30&"We will do A unless B," means: if not B then A!
ct09-10-More-Natural-Deduction.md&4.30&
ct09-10-More-Natural-Deduction.md&4.30&Therefore, the right answer is:
ct09-10-More-Natural-Deduction.md&4.30&
ct09-10-More-Natural-Deduction.md&4.30&$\sim$(RvF) $\to$ (W$\to$O)
ct09-10-More-Natural-Deduction.md&4.30&
ct09-10-More-Natural-Deduction.md&4.31&## Tautology
ct09-10-More-Natural-Deduction.md&4.31&
ct09-10-More-Natural-Deduction.md&4.31&Which of the following is a tautology?
ct09-10-More-Natural-Deduction.md&4.31&
ct09-10-More-Natural-Deduction.md&4.31&1. Q\&$\sim$Q
ct09-10-More-Natural-Deduction.md&4.31&2. (PvQ) v ($\sim$PvQ)
ct09-10-More-Natural-Deduction.md&4.31&3. P $\to$ (P\&Q)
ct09-10-More-Natural-Deduction.md&4.31&4. P $\leftrightarrow$ (Q$\to$P)
ct09-10-More-Natural-Deduction.md&4.31&
ct09-10-More-Natural-Deduction.md&4.32&## Validity
ct09-10-More-Natural-Deduction.md&4.32&
ct09-10-More-Natural-Deduction.md&4.32&Is the following argument valid? Explain your answer!
ct09-10-More-Natural-Deduction.md&4.32&
ct09-10-More-Natural-Deduction.md&4.32&"All brand name products are expensive. Some expensive products are not worth the price. Therefore, some brand name products are not worth the price."
ct09-10-More-Natural-Deduction.md&4.32&
ct09-10-More-Natural-Deduction.md&4.33&## Bombs and cakes (1)
ct09-10-More-Natural-Deduction.md&4.33&
ct09-10-More-Natural-Deduction.md&4.33&Suppose you are presented with two boxes, and are told truthfully that each box contains a cake, or a bomb, but not both.
ct09-10-More-Natural-Deduction.md&4.33&
ct09-10-More-Natural-Deduction.md&4.33&- On box 1 is written: "At least one of the two boxes contains a cake."
ct09-10-More-Natural-Deduction.md&4.33&- On box 2 is written: "The other box contains a bomb."
ct09-10-More-Natural-Deduction.md&4.33&
ct09-10-More-Natural-Deduction.md&4.33&Which of the following statements must be *false?*
ct09-10-More-Natural-Deduction.md&4.33&
ct09-10-More-Natural-Deduction.md&4.33&1. Both sentences on box 1 and box 2 are true.
ct09-10-More-Natural-Deduction.md&4.33&2. Both sentences on box 1 and box 2 are false.
ct09-10-More-Natural-Deduction.md&4.33&3. The sentence on box 1 is true and the sentence on box 2 is false.
ct09-10-More-Natural-Deduction.md&4.33&4. The sentence on box 2 is true and the sentence on box 1 is false.
ct09-10-More-Natural-Deduction.md&4.33&
ct09-10-More-Natural-Deduction.md&4.33&. . .
ct09-10-More-Natural-Deduction.md&4.33&
ct09-10-More-Natural-Deduction.md&4.33&Option 2 must be false. (Explanation on next page)
ct09-10-More-Natural-Deduction.md&4.33&
ct09-10-More-Natural-Deduction.md&4.34&## Bombs and cakes (2)
ct09-10-More-Natural-Deduction.md&4.34&
ct09-10-More-Natural-Deduction.md&4.34&>- Each box can contain independently contain a cake or a bomb, but not both.
ct09-10-More-Natural-Deduction.md&4.34&>- This means that it is also possible that both boxes contain cakes, or bombs!
ct09-10-More-Natural-Deduction.md&4.34&>- Let's look at statement 2: If both sentences are false, then box 2, which says that the other box (box 1) contains a bomb, should be false. Then, box 1 should contain a cake.
ct09-10-More-Natural-Deduction.md&4.34&>- But box 1 says that at least one box contains a cake. If *this is also to be false,* then box 1 cannot contain a cake.
ct09-10-More-Natural-Deduction.md&4.34&>- So there is a contradiction. Both sentences cannot be false at the same time.
ct09-10-More-Natural-Deduction.md&4.34&>- Therefore, option 2 must be false.
ct09-10-More-Natural-Deduction.md&4.34&>- We leave it as an exercise to the reader to make sense of the other options.
ct09-10-More-Natural-Deduction.md&4.34&
ct09-10-More-Natural-Deduction.md&4.35&## Equivalence
ct09-10-More-Natural-Deduction.md&4.35&
ct09-10-More-Natural-Deduction.md&4.35&Which of the following is logically equivalent to P?
ct09-10-More-Natural-Deduction.md&4.35&
ct09-10-More-Natural-Deduction.md&4.35&1. P\&Q
ct09-10-More-Natural-Deduction.md&4.35&2. P\&$\sim$P
ct09-10-More-Natural-Deduction.md&4.35&3. P\&P
ct09-10-More-Natural-Deduction.md&4.35&4. P$\leftrightarrow$P
ct09-10-More-Natural-Deduction.md&4.35&
ct09-10-More-Natural-Deduction.md&4.35&. . .
ct09-10-More-Natural-Deduction.md&4.35&
ct09-10-More-Natural-Deduction.md&4.35&- 1 also depends on Q.
ct09-10-More-Natural-Deduction.md&4.35&- 2 is a contradiction (always false).
ct09-10-More-Natural-Deduction.md&4.35&- 3 is the correct answer.
ct09-10-More-Natural-Deduction.md&4.35&- 4 is a tautology (always true).
ct09-10-More-Natural-Deduction.md&4.35&
ct09-10-More-Natural-Deduction.md&4.36&## Necessary and sufficient
ct09-10-More-Natural-Deduction.md&4.36&
ct09-10-More-Natural-Deduction.md&4.36&Which of the following would show that X is *not* a necessary condition for Y?
ct09-10-More-Natural-Deduction.md&4.36&
ct09-10-More-Natural-Deduction.md&4.36&1. An example that satisfies both X and Y.
ct09-10-More-Natural-Deduction.md&4.36&2. An example that satisfies X but does not satisfy Y.
ct09-10-More-Natural-Deduction.md&4.36&3. An example that satisfies Y but does not satisfy X.
ct09-10-More-Natural-Deduction.md&4.36&4. An example that satisfies neither X nor Y.
ct09-10-More-Natural-Deduction.md&4.36&
ct09-10-More-Natural-Deduction.md&4.36&. . .
ct09-10-More-Natural-Deduction.md&4.36&
ct09-10-More-Natural-Deduction.md&4.36&Answer: 3. 
ct09-10-More-Natural-Deduction.md&4.36&
ct09-10-More-Natural-Deduction.md&4.37&## Necessary and sufficient
ct09-10-More-Natural-Deduction.md&4.37&
ct09-10-More-Natural-Deduction.md&4.37&If doing well in the job interview is *not* a sufficient condition for being employed, then:
ct09-10-More-Natural-Deduction.md&4.37&
ct09-10-More-Natural-Deduction.md&4.37&1. Someone not doing well in the job interview still has a chance to be employed.
ct09-10-More-Natural-Deduction.md&4.37&2. Someone will not be employed unless they do well in the job interview.
ct09-10-More-Natural-Deduction.md&4.37&3. Someone may not be employed although they did well in the job interview.
ct09-10-More-Natural-Deduction.md&4.37&4. None of the above.
ct09-10-More-Natural-Deduction.md&4.37&
ct09-10-More-Natural-Deduction.md&4.37&. . .
ct09-10-More-Natural-Deduction.md&4.37&
ct09-10-More-Natural-Deduction.md&4.37&Answer: 3.
ct09-10-More-Natural-Deduction.md&4.37&
ct09-10-More-Natural-Deduction.md&4.37&
ct09-10-More-Natural-Deduction.md&4.38&## Necessary and sufficient
ct09-10-More-Natural-Deduction.md&4.38&
ct09-10-More-Natural-Deduction.md&4.38&Which is true of the properties of being a triangle and having three sides?
ct09-10-More-Natural-Deduction.md&4.38&
ct09-10-More-Natural-Deduction.md&4.38&1. Being a triangle is both necessary and sufficient for having three sides.
ct09-10-More-Natural-Deduction.md&4.38&2. Being a triangle is necessary, but not sufficient for having three sides.
ct09-10-More-Natural-Deduction.md&4.38&3. Being a triangle is sufficient, but not necessary for having three sides.
ct09-10-More-Natural-Deduction.md&4.38&4. Being a triangle is neither necessary nor sufficient for having three sides.
ct09-10-More-Natural-Deduction.md&4.38&
ct09-10-More-Natural-Deduction.md&4.39&## Necessary and sufficient
ct09-10-More-Natural-Deduction.md&4.39&
ct09-10-More-Natural-Deduction.md&4.39&Answer: Being a triangle is sufficient, but not necessary, for having three sides.
ct09-10-More-Natural-Deduction.md&4.39&
ct09-10-More-Natural-Deduction.md&4.39&- Every triangle has three sides, so if something is a triangle, we can be sure that it has three sides.
ct09-10-More-Natural-Deduction.md&4.39&- But not every shape with 3 sides is a triangle. For example, a “doorframe” shape (Greek $\Pi$) has three sides, but is not a triangle.
ct09-10-More-Natural-Deduction.md&4.39&
ct09-10-More-Natural-Deduction.md&4.40&## Evaluation/model
ct09-10-More-Natural-Deduction.md&4.40&
ct09-10-More-Natural-Deduction.md&4.40&Which of the following is an evaluation/model on which
ct09-10-More-Natural-Deduction.md&4.40&
ct09-10-More-Natural-Deduction.md&4.40&(P $\to$ (Q $\to$ R)) $\to$ (P $\to$ (P $\to$ R))
ct09-10-More-Natural-Deduction.md&4.40&
ct09-10-More-Natural-Deduction.md&4.40&is *false?*
ct09-10-More-Natural-Deduction.md&4.40&
ct09-10-More-Natural-Deduction.md&4.40&. . . 
ct09-10-More-Natural-Deduction.md&4.40&
ct09-10-More-Natural-Deduction.md&4.40&>- In order to make this expression false, the first part (before the top-level implication) has to be true, and the second part false.
ct09-10-More-Natural-Deduction.md&4.40&>- In order to get the second part to become false, P must be true, and P$\to$R must be false. For this, R must be false. So we need P to be true, and R to be false.
ct09-10-More-Natural-Deduction.md&4.40&>- What about Q? If the first part is true, and P is true, while R is false, then (Q$\to$R) must be true (because P is true, and only if Q$\to$R is also true, the whole first part of the expression will be true). In order for Q$\to$R to be true if R is false, Q must also be false.
ct09-10-More-Natural-Deduction.md&4.40&>- Therefore, we need P=T, Q=F, R=F.
ct09-10-More-Natural-Deduction.md&4.40&
ct09-10-More-Natural-Deduction.md&4.41&## Equivalent expressions
ct09-10-More-Natural-Deduction.md&4.41&
ct09-10-More-Natural-Deduction.md&4.41&![](graphics/13-exam-18.png)\ 
ct09-10-More-Natural-Deduction.md&4.41&
ct09-10-More-Natural-Deduction.md&4.41&. . . 
ct09-10-More-Natural-Deduction.md&4.41&
ct09-10-More-Natural-Deduction.md&4.41&>- $\sim$(P$\leftrightarrow$Q) has the truth table F, T, T, F (false when P and Q have the same value, true otherwise).
ct09-10-More-Natural-Deduction.md&4.41&>- ($\sim$P$\leftrightarrow$Q) has the same truth table, as does (P$\leftrightarrow$$\sim$Q).
ct09-10-More-Natural-Deduction.md&4.41&>- (Pv$\sim$Q)\&(Qv$\sim$P) will be true when P and Q are both true or both false (because then one of them will be negated and thus one will always be true, making each OR true).
ct09-10-More-Natural-Deduction.md&4.41&>- (P\&$\sim$Q)v(Q\&$\sim$P) will be false P and Q are the same (because then one of them will be negated and thus one will always be false, making each AND false).
ct09-10-More-Natural-Deduction.md&4.41&>- Thus, the right answer is “(Pv$\sim$Q)\&(Qv$\sim$P)”. 
ct09-10-More-Natural-Deduction.md&4.41&
ct09-10-More-Natural-Deduction.md&4.42&## Valid consequence
ct09-10-More-Natural-Deduction.md&4.42&
ct09-10-More-Natural-Deduction.md&4.42&Which of the following is *not* a valid consequence of $\sim$P?
ct09-10-More-Natural-Deduction.md&4.42&
ct09-10-More-Natural-Deduction.md&4.42&1. (P$\to$Q)
ct09-10-More-Natural-Deduction.md&4.42&2. (Q$\to$P)
ct09-10-More-Natural-Deduction.md&4.42&3. (Q$\to$$\sim$P)
ct09-10-More-Natural-Deduction.md&4.42&4. (P$\to$$\sim$Q)
ct09-10-More-Natural-Deduction.md&4.42&
ct09-10-More-Natural-Deduction.md&4.42&. . . 
ct09-10-More-Natural-Deduction.md&4.42&
ct09-10-More-Natural-Deduction.md&4.42&>- If $\sim$P (P is false), then P$\to$Q must be true.
ct09-10-More-Natural-Deduction.md&4.42&>- If $\sim$P (P is false), then Q$\to$P could be true if Q is also false, but it does not *need* to be true. Therefore, this is not a valid consequence of $\sim$P (=if the premises are true the conclusion *must* be true!)
ct09-10-More-Natural-Deduction.md&4.42&>- If $\sim$P (P is false), then Q$\to$$\sim$P must be true.
ct09-10-More-Natural-Deduction.md&4.42&>- If $\sim$P (P is false), then P$\to$$\sim$Q must be true.
ct09-10-More-Natural-Deduction.md&4.42&
ct09-10-More-Natural-Deduction.md&4.43&## Tautologies
ct09-10-More-Natural-Deduction.md&4.43&
ct09-10-More-Natural-Deduction.md&4.43&Which of these statements must be true?
ct09-10-More-Natural-Deduction.md&4.43&
ct09-10-More-Natural-Deduction.md&4.43&1. P $\leftrightarrow$ P
ct09-10-More-Natural-Deduction.md&4.43&2. P$\to$(P\&Q)
ct09-10-More-Natural-Deduction.md&4.43&3. (PvQ)$\to$P
ct09-10-More-Natural-Deduction.md&4.43&4. P\&$\sim$Q
ct09-10-More-Natural-Deduction.md&4.43&
ct09-10-More-Natural-Deduction.md&4.43&. . . 
ct09-10-More-Natural-Deduction.md&4.43&
ct09-10-More-Natural-Deduction.md&4.43&>- P $\leftrightarrow$ P must be true, since P always has the same value as itself.
ct09-10-More-Natural-Deduction.md&4.43&>- P$\to$(P\&Q) could be false if P=T, Q=F.
ct09-10-More-Natural-Deduction.md&4.43&>- (PvQ)$\to$P could be false if P=F, Q=T.
ct09-10-More-Natural-Deduction.md&4.43&>- P\&$\sim$Q could be false if P=F or Q=T.
ct09-10-More-Natural-Deduction.md&4.43&
ct09-10-More-Natural-Deduction.md&4.43&
ct09-10-More-Natural-Deduction.md&4.44&## Remember:
ct09-10-More-Natural-Deduction.md&4.44&
ct09-10-More-Natural-Deduction.md&4.44&>- You don’t need to actually do any natural deduction in the exam. But you might find the patterns we discussed useful in order to judge entailment, consistency and other such questions faster than with a truth table.
ct09-10-More-Natural-Deduction.md&4.44&>- When premises include a contradiction, the conclusion can be *any* proposition and the argument will be valid!
ct09-10-More-Natural-Deduction.md&4.44&>- Entailment: If the premises are true, the conclusion *must* be true. If the conclusion is a tautology, it is *always* true (this is what tautology means).
ct09-10-More-Natural-Deduction.md&4.44&>- Therefore, *every statement entails a tautology.*
ct09-10-More-Natural-Deduction.md&4.44&>- A tautology cannot be inconsistent with any statement (except for a contradiction), since every contingent statement is true sometimes, and the tautology is true always, that is, surely also at the same time as the other statement.
ct09-10-More-Natural-Deduction.md&4.44&
ct09-10-More-Natural-Deduction.md&4.44&
ct09-10-More-Natural-Deduction.md&4.45&## References
ct09-10-More-Natural-Deduction.md&4.45&
ct09-10-More-Natural-Deduction.md&4.45&HKU OpenCourseWare on critical thinking, logic, and creativity:
ct09-10-More-Natural-Deduction.md&4.45&
ct09-10-More-Natural-Deduction.md&4.45&http://philosophy.hku.hk/think/pl
ct09-10-More-Natural-Deduction.md&4.45&
ct09-10-More-Natural-Deduction.md&4.46&## Any questions?
ct09-10-More-Natural-Deduction.md&4.46&
ct09-10-More-Natural-Deduction.md&4.46&![](graphics/questions.jpg)\ 
ct09-10-More-Natural-Deduction.md&4.46&
ct09-10-More-Natural-Deduction.md&4.47&## Thank you for your attention!
ct09-10-More-Natural-Deduction.md&4.47&
ct09-10-More-Natural-Deduction.md&4.47&Email: <matthias@ln.edu.hk>
ct09-10-More-Natural-Deduction.md&4.47&
ct09-10-More-Natural-Deduction.md&4.47&If you have questions, please come to my office hours (see course outline).
ct09-10-More-Natural-Deduction.md&4.47&
ct09-10-More-Natural-Deduction.md&4.47&
ct09-10-More-Natural-Deduction.md&4.47&
ct09-10-More-Natural-Deduction.md&4.47&
ct09-10-More-Natural-Deduction.md&4.47&
ct11-Inductive-Deductive.md&0.1&% Critical Thinking
ct11-Inductive-Deductive.md&0.1&% 11. Deductive and inductive arguments. Validity and strength.
ct11-Inductive-Deductive.md&0.1&% A. Matthias
ct11-Inductive-Deductive.md&0.1&
ct11-Inductive-Deductive.md&1.0&# Where we are
ct11-Inductive-Deductive.md&1.0&
ct11-Inductive-Deductive.md&1.1&## What did we learn last time?
ct11-Inductive-Deductive.md&1.1&
ct11-Inductive-Deductive.md&1.1&- Logic exercises
ct11-Inductive-Deductive.md&1.1&
ct11-Inductive-Deductive.md&1.2&## What are we going to learn today?
ct11-Inductive-Deductive.md&1.2&
ct11-Inductive-Deductive.md&1.2&- What deductive, inductive, valid, strong and weak arguments are
ct11-Inductive-Deductive.md&1.2&
ct11-Inductive-Deductive.md&2.0&# Inductive and deductive arguments
ct11-Inductive-Deductive.md&2.0&
ct11-Inductive-Deductive.md&2.1&## Different ways to make an argument
ct11-Inductive-Deductive.md&2.1&
ct11-Inductive-Deductive.md&2.1&Arguments can be made in different ways
ct11-Inductive-Deductive.md&2.1&
ct11-Inductive-Deductive.md&2.1&*What is the difference?*
ct11-Inductive-Deductive.md&2.1&
ct11-Inductive-Deductive.md&2.1&1. On Fridays, the student restaurant has curry pork chop. Today is Friday. Therefore, the student restaurant will have curry pork chop.
ct11-Inductive-Deductive.md&2.1&
ct11-Inductive-Deductive.md&2.1&2. In the past six weeks, the student restaurant always had curry pork chop on Fridays. Today is Friday. Therefore, I expect them to have curry pork chop.
ct11-Inductive-Deductive.md&2.1&
ct11-Inductive-Deductive.md&2.2&## What is the difference?
ct11-Inductive-Deductive.md&2.2&
ct11-Inductive-Deductive.md&2.2&1. On Fridays, the student restaurant has curry pork chop. Today is Friday. Therefore, the student restaurant will have curry pork chop.
ct11-Inductive-Deductive.md&2.2&
ct11-Inductive-Deductive.md&2.2&2. In the past six weeks, the student restaurant always had curry pork chop on Fridays. Today is Friday. Therefore, I expect them to have curry pork chop.
ct11-Inductive-Deductive.md&2.2&
ct11-Inductive-Deductive.md&2.2&Two aspects:
ct11-Inductive-Deductive.md&2.2&
ct11-Inductive-Deductive.md&2.2&1. How do I know that they have curry pork chop on Fridays? (general rule or experience)
ct11-Inductive-Deductive.md&2.2&2. How sure can I be that they’ll have curry pork chop this Friday? (100% sure or less sure)
ct11-Inductive-Deductive.md&2.2&
ct11-Inductive-Deductive.md&2.3&## Deductive argument
ct11-Inductive-Deductive.md&2.3&
ct11-Inductive-Deductive.md&2.3&1. On Fridays, the student restaurant has curry pork chop. Today is Friday. Therefore, the student restaurant will have curry pork chop.
ct11-Inductive-Deductive.md&2.3&
ct11-Inductive-Deductive.md&2.3&>- This is the argument form we already know well.
ct11-Inductive-Deductive.md&2.3&>- It is called a *deductive* argument.
ct11-Inductive-Deductive.md&2.3&>- Deductive arguments are those where the conclusion logically follows from the premises.
ct11-Inductive-Deductive.md&2.3&>- This means: *If the premises are true, the conclusion must also be true.*
ct11-Inductive-Deductive.md&2.3&
ct11-Inductive-Deductive.md&2.4&## Inductive argument
ct11-Inductive-Deductive.md&2.4&
ct11-Inductive-Deductive.md&2.4&2. In the past six weeks, the student restaurant always had curry pork chop on Fridays. Today is Friday. Therefore, I expect them to have curry pork chop.
ct11-Inductive-Deductive.md&2.4&
ct11-Inductive-Deductive.md&2.4&>- This is an inductive argument.
ct11-Inductive-Deductive.md&2.4&>- Inductive arguments try to infer a rule from a set of observations.
ct11-Inductive-Deductive.md&2.4&>- The more observations I make, the higher the probability will be that the conclusion is true.
ct11-Inductive-Deductive.md&2.4&>- But I can never be completely sure of its truth.
ct11-Inductive-Deductive.md&2.4&
ct11-Inductive-Deductive.md&2.5&## Deductive vs. inductive
ct11-Inductive-Deductive.md&2.5&
ct11-Inductive-Deductive.md&2.5&**Deductive reasoning:** The conclusion is certain. It is derived logically from the premises. Often one of the premises will be a general rule (but not always).
ct11-Inductive-Deductive.md&2.5&
ct11-Inductive-Deductive.md&2.5&**Inductive reasoning:** The conclusion is not certain, but merely probable. Often (but not always) a general conclusion will be derived from the observation of specific cases (from the particular to the general).
ct11-Inductive-Deductive.md&2.5&
ct11-Inductive-Deductive.md&2.6&## Deductive and inductive
ct11-Inductive-Deductive.md&2.6&
ct11-Inductive-Deductive.md&2.7&### Certainty criterion
ct11-Inductive-Deductive.md&2.7&
ct11-Inductive-Deductive.md&2.7&A deductive argument is an argument in which it is thought that the premises provide a *guarantee of the truth of the conclusion.* In a deductive argument, the premises are intended to provide support for the conclusion that is so strong that, if the premises are true, it would be *impossible* for the conclusion to be false.
ct11-Inductive-Deductive.md&2.7&
ct11-Inductive-Deductive.md&2.7&An inductive argument is an argument in which it is thought that the premises provide reasons supporting the *probable truth of the conclusion.* In an inductive argument, the premises are intended only to be so strong that, if they are true, then it is *unlikely* that the conclusion is false.
ct11-Inductive-Deductive.md&2.7&
ct11-Inductive-Deductive.md&2.8&## Same conclusion proved both ways
ct11-Inductive-Deductive.md&2.8&
ct11-Inductive-Deductive.md&2.8&**Deduction:**
ct11-Inductive-Deductive.md&2.8&
ct11-Inductive-Deductive.md&2.8&(1) All birds fly.  
ct11-Inductive-Deductive.md&2.8&(2) Eagles are birds.  
ct11-Inductive-Deductive.md&2.8& ---------------------------  
ct11-Inductive-Deductive.md&2.8&(3) Therefore, all eagles fly.
ct11-Inductive-Deductive.md&2.8&
ct11-Inductive-Deductive.md&2.8&**Induction:**
ct11-Inductive-Deductive.md&2.8&
ct11-Inductive-Deductive.md&2.8&(1) This eagle flies.  
ct11-Inductive-Deductive.md&2.8&(2) This other eagle also flies.  
ct11-Inductive-Deductive.md&2.8&(3) ... (more observations)  
ct11-Inductive-Deductive.md&2.8& -----------------------------  
ct11-Inductive-Deductive.md&2.8&(3) Therefore, all eagles fly.
ct11-Inductive-Deductive.md&2.8&
ct11-Inductive-Deductive.md&2.9&## Good inductive arguments
ct11-Inductive-Deductive.md&2.9&
ct11-Inductive-Deductive.md&2.9&>- Rely on true, accurate and representative observations (don't use unusual cases!)
ct11-Inductive-Deductive.md&2.9&>- Must provide enough evidence
ct11-Inductive-Deductive.md&2.9&>- There must not be evidence pointing into another direction!
ct11-Inductive-Deductive.md&2.9&
ct11-Inductive-Deductive.md&2.10&## What type of argument is this?
ct11-Inductive-Deductive.md&2.10&
ct11-Inductive-Deductive.md&2.10&*What type of argument is this? Inductive? Deductive? Is it good?*
ct11-Inductive-Deductive.md&2.10&
ct11-Inductive-Deductive.md&2.10&"Alice, Amy and Anna have each lost ten pounds since the holidays and they all work out for an hour every morning in the gym. So if people go to the gym for an hour every morning, they will also lose some weight."
ct11-Inductive-Deductive.md&2.10&
ct11-Inductive-Deductive.md&2.10&. . . 
ct11-Inductive-Deductive.md&2.10&
ct11-Inductive-Deductive.md&2.10&>- Inductive arguments must provide enough evidence!
ct11-Inductive-Deductive.md&2.10&>- It is an inductive argument, but the evidence is weak (only three cases).
ct11-Inductive-Deductive.md&2.10&
ct11-Inductive-Deductive.md&2.11&## Other factors?
ct11-Inductive-Deductive.md&2.11&
ct11-Inductive-Deductive.md&2.11&- You might say that there are other reasons why people could gain weight, even if they go to the gym (for example, if they eat too much).
ct11-Inductive-Deductive.md&2.11&- But when there is nothing said about other factors, we assume that they just stay the same.
ct11-Inductive-Deductive.md&2.11&(After all, we don't know how much Alice, Amy and Anna are eating. So we assume that they behave in an average way and that the other people do so, too).
ct11-Inductive-Deductive.md&2.11&
ct11-Inductive-Deductive.md&2.12&## What type of argument is this?
ct11-Inductive-Deductive.md&2.12&
ct11-Inductive-Deductive.md&2.12&*What type of argument is this? Inductive? Deductive? Is it good?*
ct11-Inductive-Deductive.md&2.12&
ct11-Inductive-Deductive.md&2.12&"Alice, Amy and Anna have each lost ten pounds since the holidays and they all work out for an hour every morning in the gym. So if Susan goes to the gym for an hour every morning, she will also lose some weight."
ct11-Inductive-Deductive.md&2.12&
ct11-Inductive-Deductive.md&2.12&. . . 
ct11-Inductive-Deductive.md&2.12&
ct11-Inductive-Deductive.md&2.12&>- Still an inductive argument
ct11-Inductive-Deductive.md&2.12&>- The reason is that the conclusion is only probable, but not certain!
ct11-Inductive-Deductive.md&2.12&>- It is still weak, for the same reasons, but it is a little better in that the conclusion is not so general. By concluding something only about Susan (who is also a woman, for example), we strengthen the argument a bit.
ct11-Inductive-Deductive.md&2.12&
ct11-Inductive-Deductive.md&2.13&## What type of argument is this?
ct11-Inductive-Deductive.md&2.13&
ct11-Inductive-Deductive.md&2.13&*What type of argument is this? Inductive? Deductive? Is it good?*
ct11-Inductive-Deductive.md&2.13&
ct11-Inductive-Deductive.md&2.13&"Alice, Amy and Anna have each lost ten pounds since the holidays and they all work out for an hour every morning in the gym. Therefore, Alice, Amy and Anna are now ten pounds lighter than before."
ct11-Inductive-Deductive.md&2.13&
ct11-Inductive-Deductive.md&2.13&. . . 
ct11-Inductive-Deductive.md&2.13&
ct11-Inductive-Deductive.md&2.13&>- Deductive
ct11-Inductive-Deductive.md&2.13&>- This is a deductive argument, because the conclusion is certain. It follows logically from the premises.
ct11-Inductive-Deductive.md&2.13&
ct11-Inductive-Deductive.md&2.14&## What type of argument is this?
ct11-Inductive-Deductive.md&2.14&
ct11-Inductive-Deductive.md&2.14&*What type of argument is this? Inductive? Deductive? Is it good?*
ct11-Inductive-Deductive.md&2.14&
ct11-Inductive-Deductive.md&2.14&"Mario studies for 2 hours every night and he's got an A. Mimi studies for three hours every morning and she's close to an A too. Michael studies for an hour every Thursday after work and he has an A, too. So it looks as if studying a lot is important in order to get a good grade."
ct11-Inductive-Deductive.md&2.14&
ct11-Inductive-Deductive.md&2.14&. . .
ct11-Inductive-Deductive.md&2.14&
ct11-Inductive-Deductive.md&2.14&>- Inductive but bad.
ct11-Inductive-Deductive.md&2.14&>- There must not be evidence pointing into another direction!
ct11-Inductive-Deductive.md&2.14&>- Here, Michael is studying only one hour on Thursdays, but he has an A too. The argument is not convincing, because there is evidence pointing into another direction!
ct11-Inductive-Deductive.md&2.14&
ct11-Inductive-Deductive.md&2.15&## What type of argument is this?
ct11-Inductive-Deductive.md&2.15&
ct11-Inductive-Deductive.md&2.15&*What type of argument is this? Inductive? Deductive? Is it good?*
ct11-Inductive-Deductive.md&2.15&
ct11-Inductive-Deductive.md&2.15&"Anna's father is an investment banker and he's making a lot of money. So if I want to be rich, I should consider a career in investment banking."
ct11-Inductive-Deductive.md&2.15&
ct11-Inductive-Deductive.md&2.15&. . . 
ct11-Inductive-Deductive.md&2.15&
ct11-Inductive-Deductive.md&2.15&>- Inductive argument, but too few observations (this is a fallacy called “hasty generalization”).
ct11-Inductive-Deductive.md&2.15&
ct11-Inductive-Deductive.md&2.16&## Inductive or deductive?
ct11-Inductive-Deductive.md&2.16&
ct11-Inductive-Deductive.md&2.16&“Today I wear a black T-shirt. Yesterday, and the day before, and the day before that, I have also been wearing black T-shirts. Therefore, the past three days I have always been wearing black T-shirts!”
ct11-Inductive-Deductive.md&2.16&
ct11-Inductive-Deductive.md&2.16&. . . 
ct11-Inductive-Deductive.md&2.16&
ct11-Inductive-Deductive.md&2.16&It looks like it proceeds from observations to a more general rule, but actually the conclusion does follow logically from the premises. Therefore, this is really a *deductive* argument! (See: certainty criterion!)
ct11-Inductive-Deductive.md&2.16&
ct11-Inductive-Deductive.md&2.17&## Inductive or deductive?
ct11-Inductive-Deductive.md&2.17&
ct11-Inductive-Deductive.md&2.17&"The members of the Williams family are Susan, Nathan and Alexander. Susan wears glasses. Nathan wears glasses. Alexander wears glasses. Therefore, all members of the Williams family wear glasses."
ct11-Inductive-Deductive.md&2.17&
ct11-Inductive-Deductive.md&2.17&. . . 
ct11-Inductive-Deductive.md&2.17&
ct11-Inductive-Deductive.md&2.17&>- Deductive. The conclusion is certain.
ct11-Inductive-Deductive.md&2.17&
ct11-Inductive-Deductive.md&2.18&## Inductive or deductive?
ct11-Inductive-Deductive.md&2.18&
ct11-Inductive-Deductive.md&2.18&"It has snowed in New York every December in recorded history. Therefore, it will snow in New York this coming December."
ct11-Inductive-Deductive.md&2.18&
ct11-Inductive-Deductive.md&2.18&. . . 
ct11-Inductive-Deductive.md&2.18&
ct11-Inductive-Deductive.md&2.18&>- Inductive. The conclusion is probable, but not certain.
ct11-Inductive-Deductive.md&2.18&
ct11-Inductive-Deductive.md&2.19&## Deductive or inductive?
ct11-Inductive-Deductive.md&2.19&
ct11-Inductive-Deductive.md&2.19&1. Three friends of mine have a Thinkbox 2000 computer and they all say it's slow and expensive. Therefore, the Thinkbox must be slow and expensive.
ct11-Inductive-Deductive.md&2.19&2. Thinkbox computers are slow and expensive. I don't want a slow and expensive computer, therefore I will not buy a Thinkbox.
ct11-Inductive-Deductive.md&2.19&3. All my friends who exercise daily are healthy. Therefore if I exercise daily, I'll be healthy.
ct11-Inductive-Deductive.md&2.19&4. Everybody who exercises daily is healthy. Therefore if I exercise daily, I'll be healthy.
ct11-Inductive-Deductive.md&2.19&
ct11-Inductive-Deductive.md&2.20&## Deductive or inductive?
ct11-Inductive-Deductive.md&2.20&
ct11-Inductive-Deductive.md&2.20&1. Three friends of mine have a Thinkbox 2000 computer and they all say it's slow and expensive. Therefore, the Thinkbox must be slow and expensive. **Inductive.**
ct11-Inductive-Deductive.md&2.20&2. Thinkbox computers are slow and expensive. I don't want a slow and expensive computer, therefore I will not buy a Thinkbox. **Deductive.**
ct11-Inductive-Deductive.md&2.20&3. All my friends who exercise daily are healthy. Therefore if I exercise daily, I'll be healthy. **Inductive.**
ct11-Inductive-Deductive.md&2.20&4. Everybody who exercises daily is healthy. Therefore if I exercise daily, I'll be healthy. **Deductive.**
ct11-Inductive-Deductive.md&2.20&
ct11-Inductive-Deductive.md&2.21&## Deductive or inductive?
ct11-Inductive-Deductive.md&2.21&
ct11-Inductive-Deductive.md&2.21&“All my friends who exercise daily are healthy.”
ct11-Inductive-Deductive.md&2.21&
ct11-Inductive-Deductive.md&2.21&- This might *look like* a rule, but it is not.
ct11-Inductive-Deductive.md&2.21&- It is an observation.
ct11-Inductive-Deductive.md&2.21&- If it was a rule, you could assume it to be always true. But obviously your friends will not always be healthy. Therefore, this is not a general rule, but the summary of particular observations.
ct11-Inductive-Deductive.md&2.21&
ct11-Inductive-Deductive.md&2.22&## Deductive or inductive?
ct11-Inductive-Deductive.md&2.22&
ct11-Inductive-Deductive.md&2.22&Sometimes it is hard to decide.
ct11-Inductive-Deductive.md&2.22&
ct11-Inductive-Deductive.md&2.22&Someone says: “This is a piece of Feta cheese, so it must have been produced in Greece.”
ct11-Inductive-Deductive.md&2.22&
ct11-Inductive-Deductive.md&2.22&(Feta cheese is a kind of cheese made in Greece.)
ct11-Inductive-Deductive.md&2.22&
ct11-Inductive-Deductive.md&2.22&*Is this inductive or deductive?*
ct11-Inductive-Deductive.md&2.22&
ct11-Inductive-Deductive.md&2.23&## Deductive or inductive?
ct11-Inductive-Deductive.md&2.23&
ct11-Inductive-Deductive.md&2.23&“This is a piece of Feta cheese, so it must have been produced in Greece.”
ct11-Inductive-Deductive.md&2.23&
ct11-Inductive-Deductive.md&2.23&>- If the speaker believes that most Feta cheese is made in Greece, or that usually Feta cheese is made in Greece, then the argument would be inductive.
ct11-Inductive-Deductive.md&2.23&>- If (and this is true) the name “Feta cheese” is by law reserved only for cheeses made in Greece, then there can be no Feta that is not from Greece. So the argument is deductive, because the conclusion is certain.
ct11-Inductive-Deductive.md&2.23&>- But does the speaker know that?
ct11-Inductive-Deductive.md&2.23&>- So whether the argument is inductive or deductive would depend on the speaker’s or the audience’s knowledge!
ct11-Inductive-Deductive.md&2.23&
ct11-Inductive-Deductive.md&3.0&# Validity, soundness, and inductive strength
ct11-Inductive-Deductive.md&3.0&
ct11-Inductive-Deductive.md&3.1&## How good are these arguments?
ct11-Inductive-Deductive.md&3.1&
ct11-Inductive-Deductive.md&3.1&1. If Anne watches TV instead of going to her Critical Thinking class, she'll fail Critical Thinking. Now she watches TV instead of going to her Critical Thinking class. Therefore she’s going to fail.
ct11-Inductive-Deductive.md&3.1&2. I’ve eaten three times in this restaurant, and the food was always good. Therefore, this restaurant is good.
ct11-Inductive-Deductive.md&3.1&3. No one has ever proved that aliens don't exist. So aliens probably do exist and they have visited the earth.
ct11-Inductive-Deductive.md&3.1&
ct11-Inductive-Deductive.md&3.2&## Valid arguments
ct11-Inductive-Deductive.md&3.2&
ct11-Inductive-Deductive.md&3.2&Remember: *If the truth of the conclusion of a (deductive) argument depends only on the truth of its premises, it is called valid. Valid arguments cannot have true premises and a false conclusion.*
ct11-Inductive-Deductive.md&3.2&
ct11-Inductive-Deductive.md&3.2&. . .
ct11-Inductive-Deductive.md&3.2&
ct11-Inductive-Deductive.md&3.2&Example: “If Anne watches TV instead of going to her Critical Thinking class, she'll fail Critical Thinking. Now she watches TV instead of going to her Critical Thinking class. Therefore she’s going to fail.”
ct11-Inductive-Deductive.md&3.2&
ct11-Inductive-Deductive.md&3.2&This is a deductively valid argument.
ct11-Inductive-Deductive.md&3.2&
ct11-Inductive-Deductive.md&3.3&## Valid arguments
ct11-Inductive-Deductive.md&3.3&
ct11-Inductive-Deductive.md&3.3&"If Anne watches TV instead of going to her Critical Thinking class, she'll fail Critical Thinking. Now she watches TV instead of going to her Critical Thinking class. Therefore she’s going to fail."
ct11-Inductive-Deductive.md&3.3&
ct11-Inductive-Deductive.md&3.3&>- You might think that this argument is not valid, because Anne might fail for other reasons. But this does not change the validity of this argument!
ct11-Inductive-Deductive.md&3.3&>- What we have here, is an implication: If Ann watches TV, then Ann will fail: A$\to$B.
ct11-Inductive-Deductive.md&3.3&>- Of course B may come about through other causes, but still A is a sufficient cause for B!
ct11-Inductive-Deductive.md&3.3&
ct11-Inductive-Deductive.md&3.4&## Sound arguments
ct11-Inductive-Deductive.md&3.4&
ct11-Inductive-Deductive.md&3.4&If a (deductive) argument is valid and its premises are true (and, consequently, its conclusion is true), then it is called a *sound* argument.
ct11-Inductive-Deductive.md&3.4&
ct11-Inductive-Deductive.md&3.5&## Invalid arguments
ct11-Inductive-Deductive.md&3.5&
ct11-Inductive-Deductive.md&3.5&>- (Deductively) invalid arguments don't prove their conclusions, even if their premises are true.
ct11-Inductive-Deductive.md&3.5&>- That is, they can have true premises but still a false conclusion!
ct11-Inductive-Deductive.md&3.5&>- Invalid arguments are fallacies (mistakes in argumentation)!
ct11-Inductive-Deductive.md&3.5&>- Example: “No one has ever proved that aliens don't exist. So aliens probably do exist and they have visited the earth.”
ct11-Inductive-Deductive.md&3.5&>- In this case, although the premise is true, the conclusion can be false. This is therefore not valid.
ct11-Inductive-Deductive.md&3.5&
ct11-Inductive-Deductive.md&3.6&## Inductive arguments can be strong or weak
ct11-Inductive-Deductive.md&3.6&
ct11-Inductive-Deductive.md&3.6&>- *Inductive* arguments can be very convincing or only a little convincing.
ct11-Inductive-Deductive.md&3.6&>- We call them strong or weak, accordingly.
ct11-Inductive-Deductive.md&3.6&
ct11-Inductive-Deductive.md&3.7&## Strong arguments
ct11-Inductive-Deductive.md&3.7&
ct11-Inductive-Deductive.md&3.7&>- Example:
ct11-Inductive-Deductive.md&3.7&>- “I asked 10,000 Hong Kong citizens in a survey whether they like to drink beer. Almost all said they liked beer. Therefore I conclude that Hong Kong citizens generally like to drink beer.”
ct11-Inductive-Deductive.md&3.7&>- This is a strong argument, because the conclusion follows convincingly from the premises.
ct11-Inductive-Deductive.md&3.7&
ct11-Inductive-Deductive.md&3.8&## Weak arguments
ct11-Inductive-Deductive.md&3.8&
ct11-Inductive-Deductive.md&3.8&>- Example:
ct11-Inductive-Deductive.md&3.8&>- “I’ve eaten three times in this restaurant, and the food was always good. Therefore, this restaurant is good.” 
ct11-Inductive-Deductive.md&3.8&>- This is a weak argument, because we can doubt if the generalization is valid. It is not a fallacy though (which would be invalid) because to eat three times at one place is not too small a sample (how many times are you supposed to eat there until you can judge the food?)
ct11-Inductive-Deductive.md&3.8&
ct11-Inductive-Deductive.md&3.9&## Weak argument vs. fallacy
ct11-Inductive-Deductive.md&3.9&
ct11-Inductive-Deductive.md&3.9&>- A fallacy is an error in argumentation.
ct11-Inductive-Deductive.md&3.9&>- Whether an argument is only a weak argument or a fallacy is often a matter of degree and context.
ct11-Inductive-Deductive.md&3.9&>- If I say that all English people speak good Cantonese, because I know two English who speak good Cantonese, then this is a fallacy (not enough evidence to support that conclusion!)
ct11-Inductive-Deductive.md&3.9&>- On the other hand, if I conclude that a restaurant is good (in my opinion) after eating there 3 times, then this is a weak argument, but not a fallacy. There is some support for the conclusion, but it is not very strong.
ct11-Inductive-Deductive.md&3.9&
ct11-Inductive-Deductive.md&3.10&## What is a good argument?
ct11-Inductive-Deductive.md&3.10&
ct11-Inductive-Deductive.md&3.10&>- Criterion #1: A good argument must have true premises (that is, if it is deductive, it must be sound).
ct11-Inductive-Deductive.md&3.10&>- This means that if we have an argument with one or more false premises, then it is not a good argument. The reason for this condition is that we want a good argument to be one that can convince us to accept the conclusion. Unless the premises of an argument are all true, we would have no reason to accept its conclusion. 
ct11-Inductive-Deductive.md&3.10&
ct11-Inductive-Deductive.md&3.11&## What is a good argument?
ct11-Inductive-Deductive.md&3.11&
ct11-Inductive-Deductive.md&3.11&>- Criterion #2: A good argument must be either sound or strong.
ct11-Inductive-Deductive.md&3.11&>- Not all good arguments are sound, because only deductive arguments can be valid and sound.
ct11-Inductive-Deductive.md&3.11&>- If they are inductive, they need to at least be *strong* arguments.
ct11-Inductive-Deductive.md&3.11&
ct11-Inductive-Deductive.md&3.12&## What is a good argument?
ct11-Inductive-Deductive.md&3.12&
ct11-Inductive-Deductive.md&3.12&- Criterion #3: The premises of a good argument must not beg the question.
ct11-Inductive-Deductive.md&3.12&
ct11-Inductive-Deductive.md&3.12&. . . 
ct11-Inductive-Deductive.md&3.12&
ct11-Inductive-Deductive.md&3.12&“Begging the question”:
ct11-Inductive-Deductive.md&3.12&
ct11-Inductive-Deductive.md&3.12&>1. It is going to rain tomorrow. Therefore, it is going to rain tomorrow.
ct11-Inductive-Deductive.md&3.12&>2. If smoking was not bad for your health, then you could smoke without putting your health in danger. But now you are putting your health in danger by smoking. Therefore, smoking is bad for your health.
ct11-Inductive-Deductive.md&3.12&>3. Since Mary would not lie to her best friend, and Mary told me that I am indeed her best friend, I must really be Mary's best friend. 
ct11-Inductive-Deductive.md&3.12&
ct11-Inductive-Deductive.md&3.13&## What is a good argument?
ct11-Inductive-Deductive.md&3.13&
ct11-Inductive-Deductive.md&3.13&Criterion #4 :
ct11-Inductive-Deductive.md&3.13&
ct11-Inductive-Deductive.md&3.13&The premises of a good argument must be relevant to the conclusion
ct11-Inductive-Deductive.md&3.13&
ct11-Inductive-Deductive.md&3.14&## Summary: Good arguments
ct11-Inductive-Deductive.md&3.14&
ct11-Inductive-Deductive.md&3.14&A good argument is an argument that
ct11-Inductive-Deductive.md&3.14&
ct11-Inductive-Deductive.md&3.14&1. is either sound or strong, and
ct11-Inductive-Deductive.md&3.14&2. with premises that are true,
ct11-Inductive-Deductive.md&3.14&3. do not beg the question, and
ct11-Inductive-Deductive.md&3.14&4. are relevant to the conclusion.
ct11-Inductive-Deductive.md&3.14&
ct11-Inductive-Deductive.md&3.15&## Summary: Valid, invalid, sound, strong, weak
ct11-Inductive-Deductive.md&3.15&
ct11-Inductive-Deductive.md&3.15&>- *Inductive* arguments can be strong or weak or invalid (fallacies, for example hasty generalization).
ct11-Inductive-Deductive.md&3.15&>- *Deductive* arguments can be valid (conclusion true if premises are true), or sound (valid, plus premises are actually true), or invalid (fallacies, for example affirming the consequent).
ct11-Inductive-Deductive.md&3.15&>- *Good* arguments are sound deductive arguments, or strong inductive arguments.
ct11-Inductive-Deductive.md&3.15&
ct11-Inductive-Deductive.md&3.16&## Can inductive arguments be invalid?
ct11-Inductive-Deductive.md&3.16&
ct11-Inductive-Deductive.md&3.16&>- It is not clear whether *inductive* arguments can be said to be “invalid.” It depends on your use of “valid” and “invalid.”
ct11-Inductive-Deductive.md&3.16&>- If we understand “invalid” to mean “not valid,” then *all* inductive arguments would be “invalid” (because no inductive argument is deductively valid!)
ct11-Inductive-Deductive.md&3.16&>- But if we understand “invalid” to mean “does not support the conclusion at all,” then inductive arguments can be invalid too (e.g. the “hasty generalisation” fallacy).
ct11-Inductive-Deductive.md&3.16&>- So this is a matter of the usage of the term. Different books and teachers might have different opinions about that.
ct11-Inductive-Deductive.md&3.16&>- In exams, we will avoid questions that are unclear in this way.
ct11-Inductive-Deductive.md&3.16&
ct11-Inductive-Deductive.md&3.16&
ct11-Inductive-Deductive.md&3.16&
ct11-Inductive-Deductive.md&3.17&## Summary: Valid, invalid, sound, strong, weak
ct11-Inductive-Deductive.md&3.17&
ct11-Inductive-Deductive.md&3.17&~~~~~
ct11-Inductive-Deductive.md&3.17&
ct11-Inductive-Deductive.md&3.17&<------------------------------------------------>
ct11-Inductive-Deductive.md&3.17&Fallacy     Weak      Strong    Deductively Sound
ct11-Inductive-Deductive.md&3.17&(no support inductive inductive valid      (concl.
ct11-Inductive-Deductive.md&3.17&for concl)                      (concl.    certain
ct11-Inductive-Deductive.md&3.17&=invalid                        certain)   and 
ct11-Inductive-Deductive.md&3.17&                                           true)
ct11-Inductive-Deductive.md&3.17&                      |--------            ------>
ct11-Inductive-Deductive.md&3.17&                       “Good” arguments
ct11-Inductive-Deductive.md&3.17&
ct11-Inductive-Deductive.md&3.17&~~~~~
ct11-Inductive-Deductive.md&3.17&
ct11-Inductive-Deductive.md&3.17&
ct11-Inductive-Deductive.md&3.17&
ct11-Inductive-Deductive.md&3.17&
ct11-Inductive-Deductive.md&3.18&## Valid and circular arguments
ct11-Inductive-Deductive.md&3.18&
ct11-Inductive-Deductive.md&3.18&Technically, an argument is also valid if the premises include the conclusion.
ct11-Inductive-Deductive.md&3.18&
ct11-Inductive-Deductive.md&3.18&For example: P, Q, R $\vdash$ P
ct11-Inductive-Deductive.md&3.18&
ct11-Inductive-Deductive.md&3.18&"Valid" means: "if the premises are true, the conclusion must be true." This is the case here!
ct11-Inductive-Deductive.md&3.18&
ct11-Inductive-Deductive.md&3.19&## Which are true?
ct11-Inductive-Deductive.md&3.19&
ct11-Inductive-Deductive.md&3.19&A deductive argument...
ct11-Inductive-Deductive.md&3.19&	
ct11-Inductive-Deductive.md&3.19&A. ... is never a sound argument  
ct11-Inductive-Deductive.md&3.19&B. ... can be inductive if the conclusion is not 100% certain  
ct11-Inductive-Deductive.md&3.19&C. ... is always also a strong inductive argument  
ct11-Inductive-Deductive.md&3.19&D. ... can be a weak argument  
ct11-Inductive-Deductive.md&3.19&E. ... can be a sound argument  
ct11-Inductive-Deductive.md&3.19&
ct11-Inductive-Deductive.md&3.19&. . . 
ct11-Inductive-Deductive.md&3.19&
ct11-Inductive-Deductive.md&3.19&E.
ct11-Inductive-Deductive.md&3.19&
ct11-Inductive-Deductive.md&3.20&## Which are true?
ct11-Inductive-Deductive.md&3.20&
ct11-Inductive-Deductive.md&3.20&Which statements are correct of deductive arguments?
ct11-Inductive-Deductive.md&3.20&
ct11-Inductive-Deductive.md&3.20&A. All fallacies are weak arguments  
ct11-Inductive-Deductive.md&3.20&B. A valid argument with true premises must have a true conclusion  
ct11-Inductive-Deductive.md&3.20&C. A sound argument must have a true conclusion  
ct11-Inductive-Deductive.md&3.20&D. Sound arguments need not have true conclusions  
ct11-Inductive-Deductive.md&3.20&
ct11-Inductive-Deductive.md&3.20&. . . 
ct11-Inductive-Deductive.md&3.20&
ct11-Inductive-Deductive.md&3.20&B, C.
ct11-Inductive-Deductive.md&3.20&
ct11-Inductive-Deductive.md&3.20&
ct11-Inductive-Deductive.md&3.21&## Which are true?
ct11-Inductive-Deductive.md&3.21&
ct11-Inductive-Deductive.md&3.21&A. In a valid argument, if the premises are false, the conclusion must be false.  
ct11-Inductive-Deductive.md&3.21&B. In a valid argument, if at least one premise is true, the conclusion must be true.  
ct11-Inductive-Deductive.md&3.21&C. In a valid argument, if both premises are true, the conclusion must be true.  
ct11-Inductive-Deductive.md&3.21&D. In an invalid argument, true premises can sometimes have a true conclusion.  
ct11-Inductive-Deductive.md&3.21&E. In an invalid argument, true premises can never have a true conclusion.
ct11-Inductive-Deductive.md&3.21&
ct11-Inductive-Deductive.md&3.21&. . . 
ct11-Inductive-Deductive.md&3.21&
ct11-Inductive-Deductive.md&3.21&C, D.
ct11-Inductive-Deductive.md&3.21&
ct11-Inductive-Deductive.md&3.21&
ct11-Inductive-Deductive.md&3.22&## Which are true?
ct11-Inductive-Deductive.md&3.22&
ct11-Inductive-Deductive.md&3.22&A. All inductive arguments are either valid or invalid.  
ct11-Inductive-Deductive.md&3.22&B. No inductive argument can be valid.  
ct11-Inductive-Deductive.md&3.22&C. Deductive arguments can be strong or weak or invalid.  
ct11-Inductive-Deductive.md&3.22&D. Inductive arguments can be strong or weak or invalid.  
ct11-Inductive-Deductive.md&3.22&E. All deductive arguments are valid.  
ct11-Inductive-Deductive.md&3.22&
ct11-Inductive-Deductive.md&3.22&. . . 
ct11-Inductive-Deductive.md&3.22&
ct11-Inductive-Deductive.md&3.22&B (and perhaps D, depending on how you define “invalid”)
ct11-Inductive-Deductive.md&3.22&
ct11-Inductive-Deductive.md&3.23&## Which are true?
ct11-Inductive-Deductive.md&3.23&
ct11-Inductive-Deductive.md&3.23&A. All sound arguments are valid.  
ct11-Inductive-Deductive.md&3.23&B. All valid arguments are sound.  
ct11-Inductive-Deductive.md&3.23&C. No inductive argument can be sound.  
ct11-Inductive-Deductive.md&3.23&D. Some inductive arguments can be sound.  
ct11-Inductive-Deductive.md&3.23&E. Inductive arguments are sometimes valid.  
ct11-Inductive-Deductive.md&3.23&
ct11-Inductive-Deductive.md&3.23&. . .
ct11-Inductive-Deductive.md&3.23&
ct11-Inductive-Deductive.md&3.23&A, C.
ct11-Inductive-Deductive.md&3.23&
ct11-Inductive-Deductive.md&4.0&# Old exam exercises
ct11-Inductive-Deductive.md&4.0&
ct11-Inductive-Deductive.md&4.1&## Inductive strength
ct11-Inductive-Deductive.md&4.1&
ct11-Inductive-Deductive.md&4.1&Consider the following argument:
ct11-Inductive-Deductive.md&4.1&
ct11-Inductive-Deductive.md&4.1&Premise: John is the fastest 100m swimmer in the world.  
ct11-Inductive-Deductive.md&4.1&Premise: John is competing in the 100m swimming competition.  
ct11-Inductive-Deductive.md&4.1&Conclusion: John will win the 100m swimming competition.
ct11-Inductive-Deductive.md&4.1&
ct11-Inductive-Deductive.md&4.1&Which of the following premises will render the argument inductively weak if added to the argument?
ct11-Inductive-Deductive.md&4.1&
ct11-Inductive-Deductive.md&4.1&1. John is wearing a professional swim outfit.
ct11-Inductive-Deductive.md&4.1&2. John is very famous.
ct11-Inductive-Deductive.md&4.1&3. John has a serious shoulder injury.
ct11-Inductive-Deductive.md&4.1&4. John has a serious shoulder injury, but it doesn't affect his swimming ability.
ct11-Inductive-Deductive.md&4.1&
ct11-Inductive-Deductive.md&4.1&. . .
ct11-Inductive-Deductive.md&4.1&
ct11-Inductive-Deductive.md&4.1&>- To "make an argument inductively weaker" means to reduce the probability that the conclusion is true.
ct11-Inductive-Deductive.md&4.1&>- So here is must be answer 3. Only this premise would make the conclusion unlikely.
ct11-Inductive-Deductive.md&4.1&
ct11-Inductive-Deductive.md&4.2&## Inductive strength
ct11-Inductive-Deductive.md&4.2&
ct11-Inductive-Deductive.md&4.2&Consider the following argument:
ct11-Inductive-Deductive.md&4.2&
ct11-Inductive-Deductive.md&4.2&Premise: 95% of people will get cancer.  
ct11-Inductive-Deductive.md&4.2&Premise: 5% of people with blood type 2 will get cancer.  
ct11-Inductive-Deductive.md&4.2&Premise: John is a person with blood type 2.  
ct11-Inductive-Deductive.md&4.2&Conclusion: John will get cancer.
ct11-Inductive-Deductive.md&4.2&
ct11-Inductive-Deductive.md&4.2&Which of the following correctly describes this argument?
ct11-Inductive-Deductive.md&4.2&
ct11-Inductive-Deductive.md&4.2&1. It is deductively valid.
ct11-Inductive-Deductive.md&4.2&2. It is inductively strong.
ct11-Inductive-Deductive.md&4.2&3. It is inductively weak.
ct11-Inductive-Deductive.md&4.2&4. It is inductively bad.
ct11-Inductive-Deductive.md&4.2&
ct11-Inductive-Deductive.md&4.2&. . .
ct11-Inductive-Deductive.md&4.2&
ct11-Inductive-Deductive.md&4.2&Answer: 3.
ct11-Inductive-Deductive.md&4.2&
ct11-Inductive-Deductive.md&4.3&## Validity and soundness
ct11-Inductive-Deductive.md&4.3&
ct11-Inductive-Deductive.md&4.3&Consider the following argument:
ct11-Inductive-Deductive.md&4.3&
ct11-Inductive-Deductive.md&4.3&Premise: New York is in the United States.  
ct11-Inductive-Deductive.md&4.3&Premise: Hong Kong is in Russia.  
ct11-Inductive-Deductive.md&4.3&Conclusion: Hong Kong is in Russia.
ct11-Inductive-Deductive.md&4.3&
ct11-Inductive-Deductive.md&4.3&Is the argument valid, sound, both, or neither?
ct11-Inductive-Deductive.md&4.3&
ct11-Inductive-Deductive.md&4.3&. . .
ct11-Inductive-Deductive.md&4.3&
ct11-Inductive-Deductive.md&4.3&>- The first premise is true, the second is false. So it cannot be sound.
ct11-Inductive-Deductive.md&4.3&>- Is it valid?
ct11-Inductive-Deductive.md&4.3&>- If the premises are true, the conclusion must be true. So, yes, it is valid.
ct11-Inductive-Deductive.md&4.3&
ct11-Inductive-Deductive.md&4.3&
ct11-Inductive-Deductive.md&4.4&## Inductive strength
ct11-Inductive-Deductive.md&4.4&
ct11-Inductive-Deductive.md&4.4&Consider the following argument:
ct11-Inductive-Deductive.md&4.4&
ct11-Inductive-Deductive.md&4.4&Premise: Jack fell down.  
ct11-Inductive-Deductive.md&4.4&Conclusion: Jack hurt his head.
ct11-Inductive-Deductive.md&4.4&
ct11-Inductive-Deductive.md&4.4&Adding which of the following further premises will make the argument inductively weaker?
ct11-Inductive-Deductive.md&4.4&
ct11-Inductive-Deductive.md&4.4&1. Jill fell down the hill and landed on Jack's head.
ct11-Inductive-Deductive.md&4.4&2. The ground around Jack is soft and there are no hard objects nearby where Jack fell down.
ct11-Inductive-Deductive.md&4.4&3. Jack went to bed and bound his head with vinegar and brown paper.
ct11-Inductive-Deductive.md&4.4&4. None of the above.
ct11-Inductive-Deductive.md&4.4&
ct11-Inductive-Deductive.md&4.4&. . .
ct11-Inductive-Deductive.md&4.4&
ct11-Inductive-Deductive.md&4.4&Answer: 2.
ct11-Inductive-Deductive.md&4.4&
ct11-Inductive-Deductive.md&4.5&## Validity
ct11-Inductive-Deductive.md&4.5&
ct11-Inductive-Deductive.md&4.5&Consider the following argument:
ct11-Inductive-Deductive.md&4.5&
ct11-Inductive-Deductive.md&4.5&Premise: John fell from the top of the IFC building.  
ct11-Inductive-Deductive.md&4.5&Conclusion: John is dead.
ct11-Inductive-Deductive.md&4.5&
ct11-Inductive-Deductive.md&4.5&Which of the following additional premises would make the argument deductively valid?
ct11-Inductive-Deductive.md&4.5&
ct11-Inductive-Deductive.md&4.5&1. John is afraid of heights and suffered a heart attack during the fall.
ct11-Inductive-Deductive.md&4.5&2. John landed in a pool full of toxic chemicals.
ct11-Inductive-Deductive.md&4.5&3. John is dead.
ct11-Inductive-Deductive.md&4.5&4. All of the above.
ct11-Inductive-Deductive.md&4.5&
ct11-Inductive-Deductive.md&4.5&. . .
ct11-Inductive-Deductive.md&4.5&
ct11-Inductive-Deductive.md&4.5&Answer: Obviously, 3. It is the only premise that makes *certain* that he is dead. The others just make it more *probable* that he is dead (they make the argument inductively stronger), but they cannot make completely sure that he is dead.
ct11-Inductive-Deductive.md&4.5&
ct11-Inductive-Deductive.md&4.6&## Validity
ct11-Inductive-Deductive.md&4.6&
ct11-Inductive-Deductive.md&4.6&Consider the following argument:
ct11-Inductive-Deductive.md&4.6&
ct11-Inductive-Deductive.md&4.6&Premise: Alan fell from the top of the Mandarin Oriental Hotel.  
ct11-Inductive-Deductive.md&4.6&Conclusion: Alan is dead.
ct11-Inductive-Deductive.md&4.6&
ct11-Inductive-Deductive.md&4.6&Adding which premise would make the argument inductively *weaker?*
ct11-Inductive-Deductive.md&4.6&
ct11-Inductive-Deductive.md&4.6&1. Alan landed in a pool full of toxic chemicals.
ct11-Inductive-Deductive.md&4.6&2. Alan is afraid of heights, and suffered a heart attack during the fall.
ct11-Inductive-Deductive.md&4.6&3. Alan is a bird.
ct11-Inductive-Deductive.md&4.6&4. Alan is a bird, but has never learnt how to fly.
ct11-Inductive-Deductive.md&4.6&
ct11-Inductive-Deductive.md&4.6&. . .
ct11-Inductive-Deductive.md&4.6&
ct11-Inductive-Deductive.md&4.6&The intented answer is 3, although one could argue that 4 also makes the argument weaker.
ct11-Inductive-Deductive.md&4.6&
ct11-Inductive-Deductive.md&4.6&Still, given that only one answer can be correct, it is safe to say 3, which is stronger than 4 in its effect of weakening the argument.
ct11-Inductive-Deductive.md&4.6&
ct11-Inductive-Deductive.md&4.6&
ct11-Inductive-Deductive.md&4.7&## Validity
ct11-Inductive-Deductive.md&4.7&
ct11-Inductive-Deductive.md&4.7&"Eagles are fish. Fish are mammals. Therefore, it follows that eagles are mammals." -- This argument is:
ct11-Inductive-Deductive.md&4.7&
ct11-Inductive-Deductive.md&4.7&1. Invalid.
ct11-Inductive-Deductive.md&4.7&2. Deductively valid.
ct11-Inductive-Deductive.md&4.7&3. Deductively valid and sound.
ct11-Inductive-Deductive.md&4.7&4. Sound but not deductively valid.
ct11-Inductive-Deductive.md&4.7&
ct11-Inductive-Deductive.md&4.7&. . .
ct11-Inductive-Deductive.md&4.7&
ct11-Inductive-Deductive.md&4.7&2. The actual truth (or not) of the premises is not relevant. It is not sound though, because the premises are not true.
ct11-Inductive-Deductive.md&4.7&
ct11-Inductive-Deductive.md&4.7&
ct11-Inductive-Deductive.md&4.8&## Validity/strength
ct11-Inductive-Deductive.md&4.8&
ct11-Inductive-Deductive.md&4.8&Consider the following argument:
ct11-Inductive-Deductive.md&4.8&
ct11-Inductive-Deductive.md&4.8&Premise: There are 35 students in this class.  
ct11-Inductive-Deductive.md&4.8&Premise: Lisa is a student in this class.  
ct11-Inductive-Deductive.md&4.8&Premise: The teacher will give an A to one student only, who will be chosen randomly.  
ct11-Inductive-Deductive.md&4.8&Conclusion: Lisa will not get an A.  
ct11-Inductive-Deductive.md&4.8&
ct11-Inductive-Deductive.md&4.8&This argument is:
ct11-Inductive-Deductive.md&4.8&
ct11-Inductive-Deductive.md&4.8&1. Inductively weak
ct11-Inductive-Deductive.md&4.8&2. Inductively strong
ct11-Inductive-Deductive.md&4.8&3. Deductively valid
ct11-Inductive-Deductive.md&4.8&4. None of the above
ct11-Inductive-Deductive.md&4.8&
ct11-Inductive-Deductive.md&4.8&. . .
ct11-Inductive-Deductive.md&4.8&
ct11-Inductive-Deductive.md&4.8&Answer: 2. Inductively strong. Chances of 34:1 seem to justify well that she will not get an A.
ct11-Inductive-Deductive.md&4.8&
ct11-Inductive-Deductive.md&4.9&## Validity
ct11-Inductive-Deductive.md&4.9&
ct11-Inductive-Deductive.md&4.9&"It is obvious that, if we are good citizens, we must love our country. It is equally clear that we all love our country. Therefore, we are good citizens." -- Is this argument valid? Explain!
ct11-Inductive-Deductive.md&4.9&
ct11-Inductive-Deductive.md&4.9&. . .
ct11-Inductive-Deductive.md&4.9&
ct11-Inductive-Deductive.md&4.9&This is a conditional argument. It is affirming the consequent and is therefore *not* valid.
ct11-Inductive-Deductive.md&4.9&
ct11-Inductive-Deductive.md&4.10&## Validity
ct11-Inductive-Deductive.md&4.10&
ct11-Inductive-Deductive.md&4.10&Which of the following is a feature of validity?
ct11-Inductive-Deductive.md&4.10&
ct11-Inductive-Deductive.md&4.10&1. The premises are always true
ct11-Inductive-Deductive.md&4.10&2. The conclusion is always true
ct11-Inductive-Deductive.md&4.10&3. The premises are never false when the conclusion is true
ct11-Inductive-Deductive.md&4.10&4. The conclusion is never false when the premises are true
ct11-Inductive-Deductive.md&4.10&
ct11-Inductive-Deductive.md&4.10&. . .
ct11-Inductive-Deductive.md&4.10&
ct11-Inductive-Deductive.md&4.10&Answer: 4.
ct11-Inductive-Deductive.md&4.10&
ct11-Inductive-Deductive.md&4.11&## Validity/strength
ct11-Inductive-Deductive.md&4.11&
ct11-Inductive-Deductive.md&4.11&Consider the following argument:
ct11-Inductive-Deductive.md&4.11&
ct11-Inductive-Deductive.md&4.11&Premise: 20075 cats have been thrown from a balcony on the third floor of a building, and have all landed on their feet, alive and well.  
ct11-Inductive-Deductive.md&4.11&Conclusion: The next cat (the 20076th cat) to be thrown from the balcony will also land on its feet (alive and well).
ct11-Inductive-Deductive.md&4.11&
ct11-Inductive-Deductive.md&4.11&This argument:
ct11-Inductive-Deductive.md&4.11&
ct11-Inductive-Deductive.md&4.11&1. Is valid
ct11-Inductive-Deductive.md&4.11&2. Is inductively strong
ct11-Inductive-Deductive.md&4.11&3. Is inductively weak
ct11-Inductive-Deductive.md&4.11&4. Is a fallacy
ct11-Inductive-Deductive.md&4.11&
ct11-Inductive-Deductive.md&4.11&. . .
ct11-Inductive-Deductive.md&4.11&
ct11-Inductive-Deductive.md&4.11&Answer: 2.
ct11-Inductive-Deductive.md&4.11&
ct11-Inductive-Deductive.md&4.12&## Inductive strength
ct11-Inductive-Deductive.md&4.12&
ct11-Inductive-Deductive.md&4.12&Consider the following argument:
ct11-Inductive-Deductive.md&4.12&
ct11-Inductive-Deductive.md&4.12&- Premise: The helicopter is flying 10 km above land.
ct11-Inductive-Deductive.md&4.12&- Premise: Jill accidentally falls out of the helicopter.
ct11-Inductive-Deductive.md&4.12&- Conclusion: Jill will die.
ct11-Inductive-Deductive.md&4.12&
ct11-Inductive-Deductive.md&4.12&Which of the following additional premises would make the argument inductively *weak?*
ct11-Inductive-Deductive.md&4.12&
ct11-Inductive-Deductive.md&4.12&1. Jill has a parachute.
ct11-Inductive-Deductive.md&4.12&2. Jill has a parachute, but it doesn't work.
ct11-Inductive-Deductive.md&4.12&3. Jill has a working parachute, but doesn't know how to use it.
ct11-Inductive-Deductive.md&4.12&4. Jill has a working parachute, and knows how to use it, but she's knocked unconscious when she leaves the helicopter.
ct11-Inductive-Deductive.md&4.12&
ct11-Inductive-Deductive.md&4.12&. . .
ct11-Inductive-Deductive.md&4.12&
ct11-Inductive-Deductive.md&4.12&Answer: 1. The other answers just strengthen the argument again (making the conclusion that she'll die more probable!)
ct11-Inductive-Deductive.md&4.12&
ct11-Inductive-Deductive.md&4.12&
ct11-Inductive-Deductive.md&4.13&## What did we learn today?
ct11-Inductive-Deductive.md&4.13&
ct11-Inductive-Deductive.md&4.13&- A little more about the validity of arguments.
ct11-Inductive-Deductive.md&4.13&- What deductive, inductive, valid, strong and weak arguments are.
ct11-Inductive-Deductive.md&4.13&
ct11-Inductive-Deductive.md&4.14&## References
ct11-Inductive-Deductive.md&4.14&
ct11-Inductive-Deductive.md&4.14&Drew E. Hinderer: “Building Arguments.” Belmont, California: Wadsworth, 1992
ct11-Inductive-Deductive.md&4.14&
ct11-Inductive-Deductive.md&4.14&Lingnan University Library:
ct11-Inductive-Deductive.md&4.14&PE 1431 .H5 1992
ct11-Inductive-Deductive.md&4.14&
ct11-Inductive-Deductive.md&4.15&## Any questions?
ct11-Inductive-Deductive.md&4.15&
ct11-Inductive-Deductive.md&4.15&![](graphics/questions.jpg)\ 
ct11-Inductive-Deductive.md&4.15&
ct11-Inductive-Deductive.md&4.16&## Thank you for your attention!
ct11-Inductive-Deductive.md&4.16&
ct11-Inductive-Deductive.md&4.16&Email: <matthias@ln.edu.hk>
ct11-Inductive-Deductive.md&4.16&
ct11-Inductive-Deductive.md&4.16&If you have questions, please come to my office hours (see course outline).
ct11-Inductive-Deductive.md&4.16&
ct11-Inductive-Deductive.md&4.16&
ct11-Inductive-Deductive.md&4.16&
ct11-Inductive-Deductive.md&4.16&
ct11-Inductive-Deductive.md&4.16&
ct12-Argument-Structure-Analogies.md&0.1&% Critical Thinking
ct12-Argument-Structure-Analogies.md&0.1&% 12. Argument structure -- Analogies
ct12-Argument-Structure-Analogies.md&0.1&% A. Matthias
ct12-Argument-Structure-Analogies.md&0.1&
ct12-Argument-Structure-Analogies.md&1.0&# Where we are
ct12-Argument-Structure-Analogies.md&1.0&
ct12-Argument-Structure-Analogies.md&1.1&## What did we learn last time?
ct12-Argument-Structure-Analogies.md&1.1&
ct12-Argument-Structure-Analogies.md&1.1&- A little more about the validity of arguments
ct12-Argument-Structure-Analogies.md&1.1&- What deductive, inductive, valid, strong and weak arguments are
ct12-Argument-Structure-Analogies.md&1.1&
ct12-Argument-Structure-Analogies.md&1.2&## What are we going to learn today?
ct12-Argument-Structure-Analogies.md&1.2&
ct12-Argument-Structure-Analogies.md&1.2&- How to restructure and analyse arguments.
ct12-Argument-Structure-Analogies.md&1.2&- What analogies are, and how to evaluate them.
ct12-Argument-Structure-Analogies.md&1.2&
ct12-Argument-Structure-Analogies.md&2.0&# Argument structure
ct12-Argument-Structure-Analogies.md&2.0&
ct12-Argument-Structure-Analogies.md&2.1&## What is an argument?
ct12-Argument-Structure-Analogies.md&2.1&
ct12-Argument-Structure-Analogies.md&2.1&An argument is a series of statements (premises) that present *reasons* which, if true, would make it certain (deductive) or likely (inductive) that a particular conclusion is true.
ct12-Argument-Structure-Analogies.md&2.1&
ct12-Argument-Structure-Analogies.md&2.2&## Analysing arguments
ct12-Argument-Structure-Analogies.md&2.2&
ct12-Argument-Structure-Analogies.md&2.2&>- When presented with an argument, we must first identify what the premises are and what the conclusion is.
ct12-Argument-Structure-Analogies.md&2.2&>- Often you recognise the conclusion by words like "so," or "therefore."
ct12-Argument-Structure-Analogies.md&2.2&>- If unsure, see which premises/conclusion would give you a valid argument that does not beg the question and is not a tautology or contradiction.
ct12-Argument-Structure-Analogies.md&2.2&>- Sometimes you will need to add missing (hidden) premises, or even a missing conclusion in order to make the argument valid.
ct12-Argument-Structure-Analogies.md&2.2&>- If unsure, write the argument down as a series of logical expressions, and then look which missing premises would be needed to make it valid.
ct12-Argument-Structure-Analogies.md&2.2&
ct12-Argument-Structure-Analogies.md&2.3&## Analysing complex arguments
ct12-Argument-Structure-Analogies.md&2.3&
ct12-Argument-Structure-Analogies.md&2.3&>- More complex arguments have *sub-conclusions* (intermediate conclusions).
ct12-Argument-Structure-Analogies.md&2.3&>- You then need to write down the premises, the subconclusion that follows from them, other premises and *their* subconclusion and so on.
ct12-Argument-Structure-Analogies.md&2.3&>- In the end, the subconclusions will usually be the premises to the final conclusion.
ct12-Argument-Structure-Analogies.md&2.3&>- It is often easier to begin with the conclusion and work backwards towards the premises, adding whatever hidden premises and intermediate conclusions you need in order to make the argument valid.
ct12-Argument-Structure-Analogies.md&2.3&
ct12-Argument-Structure-Analogies.md&2.4&## Argument analysis (1)
ct12-Argument-Structure-Analogies.md&2.4&
ct12-Argument-Structure-Analogies.md&2.4&Consider the following argument:
ct12-Argument-Structure-Analogies.md&2.4&
ct12-Argument-Structure-Analogies.md&2.4&"The world would be better if there was a single world government. If there was such a government, then there would be no wars. There would also be much more economic cooperation and free trade between the different regions of the world. On the other hand, if there was a world government, there would be more administration costs. But these would be outweighed by the benefits of having a world government."
ct12-Argument-Structure-Analogies.md&2.4&
ct12-Argument-Structure-Analogies.md&2.4&- What is the conclusion?
ct12-Argument-Structure-Analogies.md&2.4&- Does the speaker believe that there would be more administrative costs if there was a world government?
ct12-Argument-Structure-Analogies.md&2.4&- If so, does this belief support the conclusion?
ct12-Argument-Structure-Analogies.md&2.4&
ct12-Argument-Structure-Analogies.md&2.5&## Argument analysis (2)
ct12-Argument-Structure-Analogies.md&2.5&
ct12-Argument-Structure-Analogies.md&2.5&"The world would be better if there was a single world government. If there was such a government, then there would be no wars. There would also be much more economic cooperation and free trade between the different regions of the world. On the other hand, if there was a world government, there would be more administration costs. But these would be outweighed by the benefits of having a world government."
ct12-Argument-Structure-Analogies.md&2.5&
ct12-Argument-Structure-Analogies.md&2.5&- Conclusion: The world would be better if there was a single world government.
ct12-Argument-Structure-Analogies.md&2.5&- Does the speaker believe that there would be more administrative costs if there was a world government? -- Yes.
ct12-Argument-Structure-Analogies.md&2.5&- Does this belief support the conclusion? -- No. This is a counter-argument, but he dismisses it, because the benefits would outweigh the additional costs.
ct12-Argument-Structure-Analogies.md&2.5&
ct12-Argument-Structure-Analogies.md&2.6&## Argument analysis (1)
ct12-Argument-Structure-Analogies.md&2.6&
ct12-Argument-Structure-Analogies.md&2.6&Consider the following argument:
ct12-Argument-Structure-Analogies.md&2.6&
ct12-Argument-Structure-Analogies.md&2.6&"To punish people merely for what they have done would be unjust, because the forbidden act might have been an accident for which the person who did it cannot be held to blame."
ct12-Argument-Structure-Analogies.md&2.6&
ct12-Argument-Structure-Analogies.md&2.6&Write down the argument in standard form (premises, conclusion), including any hidden premises.
ct12-Argument-Structure-Analogies.md&2.6&
ct12-Argument-Structure-Analogies.md&2.6&. . .
ct12-Argument-Structure-Analogies.md&2.6&
ct12-Argument-Structure-Analogies.md&2.6&>- You can see that the conclusion contains the word "unjust," for which there is no premise!
ct12-Argument-Structure-Analogies.md&2.6&>- So the argument cannot be valid.
ct12-Argument-Structure-Analogies.md&2.6&>- We need a premise to connect "cannot be blamed" with "unjust."
ct12-Argument-Structure-Analogies.md&2.6&
ct12-Argument-Structure-Analogies.md&2.7&## Argument analysis (2)
ct12-Argument-Structure-Analogies.md&2.7&
ct12-Argument-Structure-Analogies.md&2.7&The explicit premises:
ct12-Argument-Structure-Analogies.md&2.7&
ct12-Argument-Structure-Analogies.md&2.7&The forbidden act might be an accident.  
ct12-Argument-Structure-Analogies.md&2.7&Persons cannot be blamed for (some) accidents.  
ct12-Argument-Structure-Analogies.md&2.7& --------------------------------------------------------------  
ct12-Argument-Structure-Analogies.md&2.7&To punish people merely for what they have done would be unjust.
ct12-Argument-Structure-Analogies.md&2.7&
ct12-Argument-Structure-Analogies.md&2.7&You can easily see that this argument is not valid. There are no premises about "unjust," and so the conclusion does not follow from the premises. *What is the hidden premise?*
ct12-Argument-Structure-Analogies.md&2.7&
ct12-Argument-Structure-Analogies.md&2.7&. . .
ct12-Argument-Structure-Analogies.md&2.7&
ct12-Argument-Structure-Analogies.md&2.7&Hidden premise: it is unjust to punish people, if they cannot be blamed for an act.
ct12-Argument-Structure-Analogies.md&2.7&
ct12-Argument-Structure-Analogies.md&2.8&## Argument analysis (1)
ct12-Argument-Structure-Analogies.md&2.8&
ct12-Argument-Structure-Analogies.md&2.8&Consider the following argument:
ct12-Argument-Structure-Analogies.md&2.8&
ct12-Argument-Structure-Analogies.md&2.8&"Democracy is not perfect, since it has many problems. For example, it is much more difficult for a leader of a democracy to implement unpopular policies than it is for a leader of a non-democratic government, even when those policies are the best policies. Nevertheless, democracy is the best form of government there is, since other forms of government have even bigger problems. This is why Hong Kong should have a democracy."
ct12-Argument-Structure-Analogies.md&2.8&
ct12-Argument-Structure-Analogies.md&2.8&Present this argument in standard format. Identify the ultimate conclusion as well as the premises and sub-conclusions (intermediate conclusions). Do not include any premises or sub-conclusions that don't support the main conclusion of the argument!
ct12-Argument-Structure-Analogies.md&2.8&
ct12-Argument-Structure-Analogies.md&2.9&## Argument analysis (2)
ct12-Argument-Structure-Analogies.md&2.9&
ct12-Argument-Structure-Analogies.md&2.9&>- Conclusion: Hong Kong should have a democracy.
ct12-Argument-Structure-Analogies.md&2.9&>- In the conclusion we have "Hong Kong," but not in the premises.
ct12-Argument-Structure-Analogies.md&2.9&>- So we need to create a premise that mentions Hong Kong in a way that it can then be used in the conclusion.
ct12-Argument-Structure-Analogies.md&2.9&>- You also see that the whole section from "For example" to "best policies" is a counter-argument. According to the instructions, we don't need to include it in our answer.
ct12-Argument-Structure-Analogies.md&2.9&
ct12-Argument-Structure-Analogies.md&2.10&## Argument analysis (3)
ct12-Argument-Structure-Analogies.md&2.10&
ct12-Argument-Structure-Analogies.md&2.10&"Democracy is not perfect, since it has many problems. \textcolor{red}{For example, it is much more difficult for a leader of a democracy to implement unpopular policies than it is for a leader of a non-democratic government, even when those policies are the best policies.} Nevertheless, democracy is the best form of government there is, since other forms of government have even bigger problems. This is why Hong Kong should have a democracy."
ct12-Argument-Structure-Analogies.md&2.10&
ct12-Argument-Structure-Analogies.md&2.10&>- Premise: Other forms of government have bigger problems.
ct12-Argument-Structure-Analogies.md&2.10&>- Sub-conclusion: Democracy is the best form of government there is.
ct12-Argument-Structure-Analogies.md&2.10&>- Hidden premise: Hong Kong should have the best form of government.
ct12-Argument-Structure-Analogies.md&2.10&>- Conclusion: Hong Kong should have a democracy.
ct12-Argument-Structure-Analogies.md&2.10&>- The other premises and subconclusion do not support this conclusion, but form a counter-argument.
ct12-Argument-Structure-Analogies.md&2.10&
ct12-Argument-Structure-Analogies.md&2.11&## Standard form and hidden premises (1)
ct12-Argument-Structure-Analogies.md&2.11&
ct12-Argument-Structure-Analogies.md&2.11&"If we had burnt too much fossil fuel in the 20th century, there would be too much carbon dioxide in the atmosphere. If there was too much carbon dioxide in the atmosphere, global warming would be increasing. Life on earth is in great danger."
ct12-Argument-Structure-Analogies.md&2.11&
ct12-Argument-Structure-Analogies.md&2.11&Present this argument in standard form and add hidden premises as needed to make it into a deductively valid argument!
ct12-Argument-Structure-Analogies.md&2.11&
ct12-Argument-Structure-Analogies.md&2.12&## Standard form and hidden premises (2)
ct12-Argument-Structure-Analogies.md&2.12&
ct12-Argument-Structure-Analogies.md&2.12&>- First, find the conclusion.
ct12-Argument-Structure-Analogies.md&2.12&>- Conclusion: “Life on Earth is in great danger.”
ct12-Argument-Structure-Analogies.md&2.12&>- Second, try to see the logical structure of the argument:
ct12-Argument-Structure-Analogies.md&2.12&>     - Premise 1: If we had burnt too much fossil fuel (F) in the 20th century, there would be too much carbon dioxide in the atmosphere (C). 
ct12-Argument-Structure-Analogies.md&2.12&>     - Premise 2: If there was too much carbon dioxide in the atmosphere (C), global warming would be increasing (W).
ct12-Argument-Structure-Analogies.md&2.12&>     - Conclusion: Life on earth is in great danger (D).
ct12-Argument-Structure-Analogies.md&2.12&
ct12-Argument-Structure-Analogies.md&2.12&. . . 
ct12-Argument-Structure-Analogies.md&2.12&
ct12-Argument-Structure-Analogies.md&2.12&F$\to$C  
ct12-Argument-Structure-Analogies.md&2.12&C$\to$W  
ct12-Argument-Structure-Analogies.md&2.12&------  
ct12-Argument-Structure-Analogies.md&2.12&D
ct12-Argument-Structure-Analogies.md&2.12&
ct12-Argument-Structure-Analogies.md&2.12&
ct12-Argument-Structure-Analogies.md&2.13&## Standard form and hidden premises (3)
ct12-Argument-Structure-Analogies.md&2.13&
ct12-Argument-Structure-Analogies.md&2.13&F$\to$C  
ct12-Argument-Structure-Analogies.md&2.13&C$\to$W  
ct12-Argument-Structure-Analogies.md&2.13&------  
ct12-Argument-Structure-Analogies.md&2.13&D
ct12-Argument-Structure-Analogies.md&2.13&
ct12-Argument-Structure-Analogies.md&2.13&>- You can see immediately that this won’t do.
ct12-Argument-Structure-Analogies.md&2.13&>- D cannot be validly concluded from these two premises. So we need some additional (hidden) premises.
ct12-Argument-Structure-Analogies.md&2.13&>- Just looking at the logical form, without even looking at the meaning: if we had W$\to$D, then we’d have created a connection between the premises and the conclusion:
ct12-Argument-Structure-Analogies.md&2.13&
ct12-Argument-Structure-Analogies.md&2.13&. . . 
ct12-Argument-Structure-Analogies.md&2.13&
ct12-Argument-Structure-Analogies.md&2.13&F$\to$C  
ct12-Argument-Structure-Analogies.md&2.13&C$\to$W  
ct12-Argument-Structure-Analogies.md&2.13&W$\to$D  
ct12-Argument-Structure-Analogies.md&2.13&------  
ct12-Argument-Structure-Analogies.md&2.13&D
ct12-Argument-Structure-Analogies.md&2.13&
ct12-Argument-Structure-Analogies.md&2.13&
ct12-Argument-Structure-Analogies.md&2.14&## Standard form and hidden premises (4)
ct12-Argument-Structure-Analogies.md&2.14&
ct12-Argument-Structure-Analogies.md&2.14&F$\to$C  
ct12-Argument-Structure-Analogies.md&2.14&C$\to$W  
ct12-Argument-Structure-Analogies.md&2.14&W$\to$D  
ct12-Argument-Structure-Analogies.md&2.14&------  
ct12-Argument-Structure-Analogies.md&2.14&D
ct12-Argument-Structure-Analogies.md&2.14&
ct12-Argument-Structure-Analogies.md&2.14&>- This is a kind of hypothetical syllogism (look back to the session where we learned natural deduction!) But it’s still not complete. Can you see why?
ct12-Argument-Structure-Analogies.md&2.14&>- The premises are all hypothetical: If F, then C; etc. But in order to conclude D, we need a premise that gives us at least an F, C, or W, so that we can affirm the antecedent and get a D as a result.
ct12-Argument-Structure-Analogies.md&2.14&>- But if we had a premise, say, “W”, then the first two premises would be useless.
ct12-Argument-Structure-Analogies.md&2.14&>- In order to use *all* the premises, we’d need another hidden premise: F.
ct12-Argument-Structure-Analogies.md&2.14&
ct12-Argument-Structure-Analogies.md&2.14&
ct12-Argument-Structure-Analogies.md&2.15&## Standard form and hidden premises (5)
ct12-Argument-Structure-Analogies.md&2.15&
ct12-Argument-Structure-Analogies.md&2.15&F$\to$C  
ct12-Argument-Structure-Analogies.md&2.15&C$\to$W  
ct12-Argument-Structure-Analogies.md&2.15&W$\to$D  
ct12-Argument-Structure-Analogies.md&2.15&F   
ct12-Argument-Structure-Analogies.md&2.15&------  
ct12-Argument-Structure-Analogies.md&2.15&D
ct12-Argument-Structure-Analogies.md&2.15&
ct12-Argument-Structure-Analogies.md&2.15&This is now valid! (3x affirming antecedent).
ct12-Argument-Structure-Analogies.md&2.15&
ct12-Argument-Structure-Analogies.md&2.15&
ct12-Argument-Structure-Analogies.md&2.15&
ct12-Argument-Structure-Analogies.md&2.16&## Standard form and hidden premises (6)
ct12-Argument-Structure-Analogies.md&2.16&
ct12-Argument-Structure-Analogies.md&2.16&>- Translated back to English:
ct12-Argument-Structure-Analogies.md&2.16&>- Premise 1: If we had burnt too much fossil fuel in the 20th century, there would be too much carbon dioxide in the atmosphere.
ct12-Argument-Structure-Analogies.md&2.16&>- Premise 2: If there was too much carbon dioxide in the atmosphere, global warming would be increasing.
ct12-Argument-Structure-Analogies.md&2.16&>- Hidden premise: We burnt too much fossil fuel in the 20th century.
ct12-Argument-Structure-Analogies.md&2.16&>- Sub-conclusion: Global warming is increasing.
ct12-Argument-Structure-Analogies.md&2.16&>- Hidden premise: If global warming was increasing, life on earth would be in great danger.
ct12-Argument-Structure-Analogies.md&2.16&>- Conclusion: Life on earth is in great danger.
ct12-Argument-Structure-Analogies.md&2.16&
ct12-Argument-Structure-Analogies.md&2.16&
ct12-Argument-Structure-Analogies.md&2.16&
ct12-Argument-Structure-Analogies.md&2.17&## Present in standard form (1)
ct12-Argument-Structure-Analogies.md&2.17&
ct12-Argument-Structure-Analogies.md&2.17&"Men can be feminists just as easily as women, since they can believe in and advance causes that help reduce unfair discrimination against women."
ct12-Argument-Structure-Analogies.md&2.17&
ct12-Argument-Structure-Analogies.md&2.17&Present this argument in standard form and add hidden premises as needed to make it into a deductively valid argument!
ct12-Argument-Structure-Analogies.md&2.17&
ct12-Argument-Structure-Analogies.md&2.18&## Present in standard form (2)
ct12-Argument-Structure-Analogies.md&2.18&
ct12-Argument-Structure-Analogies.md&2.18&>- Conclusion: Men can be feminists just as easily as women.
ct12-Argument-Structure-Analogies.md&2.18&>- The conclusion contains "feminists," but no premise contains that term.
ct12-Argument-Structure-Analogies.md&2.18&>- So we need to add a premise that contains feminists.
ct12-Argument-Structure-Analogies.md&2.18&
ct12-Argument-Structure-Analogies.md&2.18&. . . 
ct12-Argument-Structure-Analogies.md&2.18&
ct12-Argument-Structure-Analogies.md&2.18&>- Premise: Men can easily believe in and advance causes that help reduce unfair discrimination against women.
ct12-Argument-Structure-Analogies.md&2.18&>- Hidden premise: Whoever believes in and advances causes that help reduce unfair discrimination against women is a feminist.
ct12-Argument-Structure-Analogies.md&2.18&>- Premise: Women can easily believe in and advance causes ... (this is needed, so that we can conclude that men can do this *just as easily* as women.)
ct12-Argument-Structure-Analogies.md&2.18&>- Conclusion: Men can be feminists just as easily as women.
ct12-Argument-Structure-Analogies.md&2.18&
ct12-Argument-Structure-Analogies.md&2.19&## Present in standard form
ct12-Argument-Structure-Analogies.md&2.19&
ct12-Argument-Structure-Analogies.md&2.19&"No method of inference about the future will work if there is no uniformity in nature. But we can affirm that there is uniformity in nature, for induction works. This is amply confirmed by our experiences."
ct12-Argument-Structure-Analogies.md&2.19&
ct12-Argument-Structure-Analogies.md&2.19&Write down this argument in standard form and supply any hidden premise needed for the argument to be valid.
ct12-Argument-Structure-Analogies.md&2.19&
ct12-Argument-Structure-Analogies.md&2.19&. . .
ct12-Argument-Structure-Analogies.md&2.19&
ct12-Argument-Structure-Analogies.md&2.19&>- Hidden premise: Induction is a method of inference about the future.
ct12-Argument-Structure-Analogies.md&2.19&>- Premise: Induction works.
ct12-Argument-Structure-Analogies.md&2.19&>- Sub-conclusion: A method of inference about the future works.
ct12-Argument-Structure-Analogies.md&2.19&>- Premise: If there is no uniformity in nature, then no method of inference about the future will work.
ct12-Argument-Structure-Analogies.md&2.19&>- Conclusion: There is uniformity in nature. (Denying the consequent!)
ct12-Argument-Structure-Analogies.md&2.19&
ct12-Argument-Structure-Analogies.md&2.20&## Derive a valid conclusion (1)
ct12-Argument-Structure-Analogies.md&2.20&
ct12-Argument-Structure-Analogies.md&2.20&Derive a valid conclusion using *all* the following sentences as premises. Write down the argument step by step, stating all the intermediate conclusions!
ct12-Argument-Structure-Analogies.md&2.20&
ct12-Argument-Structure-Analogies.md&2.20&"Anything that disturbs others should not be allowed in the classroom. Babies are always noisy. All distracting things are disturbing others. Ringing mobiles are as noisy as babies. Noisy things are distracting."
ct12-Argument-Structure-Analogies.md&2.20&
ct12-Argument-Structure-Analogies.md&2.20&
ct12-Argument-Structure-Analogies.md&2.21&## Derive a valid conclusion (2)
ct12-Argument-Structure-Analogies.md&2.21&
ct12-Argument-Structure-Analogies.md&2.21&>- What is the ultimate conclusion? 
ct12-Argument-Structure-Analogies.md&2.21&>- (Unstated): Babies and ringing mobiles should not be allowed in the classroom.
ct12-Argument-Structure-Analogies.md&2.21&>- *Now try to ask a series of questions to find a valid reason to support each conclusion and each step of the reasoning process.*
ct12-Argument-Structure-Analogies.md&2.21&
ct12-Argument-Structure-Analogies.md&2.21&
ct12-Argument-Structure-Analogies.md&2.22&## Derive a valid conclusion (3)
ct12-Argument-Structure-Analogies.md&2.22&
ct12-Argument-Structure-Analogies.md&2.22&>- (Ultimate conclusion): Babies and ringing mobiles should not be allowed in the classroom.
ct12-Argument-Structure-Analogies.md&2.22&>- Why should they not be allowed in the classroom?
ct12-Argument-Structure-Analogies.md&2.22&>- Because they are disturbing others and things that disturb others should not be allowed in the classroom.
ct12-Argument-Structure-Analogies.md&2.22&>- Why are they disturbing others?
ct12-Argument-Structure-Analogies.md&2.22&>- Because they are distracting others and all distracting things are disturbing others.
ct12-Argument-Structure-Analogies.md&2.22&>- Why are they distracting?
ct12-Argument-Structure-Analogies.md&2.22&>- Because they are noisy and all noisy things are distracting.
ct12-Argument-Structure-Analogies.md&2.22&>- How do we know that they are noisy?
ct12-Argument-Structure-Analogies.md&2.22&>- Premise: Babies are always noisy.
ct12-Argument-Structure-Analogies.md&2.22&>- Premise: Ringing mobiles are as noisy as babies.
ct12-Argument-Structure-Analogies.md&2.22&>- Intermediate conclusion: Ringing mobiles are (also) always noisy.
ct12-Argument-Structure-Analogies.md&2.22&
ct12-Argument-Structure-Analogies.md&2.22&
ct12-Argument-Structure-Analogies.md&2.23&## Derive a valid conclusion (4)
ct12-Argument-Structure-Analogies.md&2.23&
ct12-Argument-Structure-Analogies.md&2.23&*Now put the argument together in the right order:*
ct12-Argument-Structure-Analogies.md&2.23&
ct12-Argument-Structure-Analogies.md&2.23&Premise: Babies are always noisy.   
ct12-Argument-Structure-Analogies.md&2.23&Premise: Ringing mobiles are as noisy as babies.   
ct12-Argument-Structure-Analogies.md&2.23&Intermediate conclusion: Babies and ringing mobile phones are noisy.
ct12-Argument-Structure-Analogies.md&2.23&
ct12-Argument-Structure-Analogies.md&2.23&. . .
ct12-Argument-Structure-Analogies.md&2.23&
ct12-Argument-Structure-Analogies.md&2.23&Hidden premise: Babies and ringing mobile phones are noisy.  
ct12-Argument-Structure-Analogies.md&2.23&Premise: Noisy things are distracting.  
ct12-Argument-Structure-Analogies.md&2.23&Intermediate conclusion: Babies and mobile phones are distracting.
ct12-Argument-Structure-Analogies.md&2.23&
ct12-Argument-Structure-Analogies.md&2.23&(continued on next slide)
ct12-Argument-Structure-Analogies.md&2.23&
ct12-Argument-Structure-Analogies.md&2.24&## Derive a valid conclusion (5)
ct12-Argument-Structure-Analogies.md&2.24&
ct12-Argument-Structure-Analogies.md&2.24&Hidden premise: Babies and mobile phones are distracting.  
ct12-Argument-Structure-Analogies.md&2.24&Premise: All distracting things are disturbing others.  
ct12-Argument-Structure-Analogies.md&2.24&Intermediate conclusion: Babies and mobile phones disturb others.
ct12-Argument-Structure-Analogies.md&2.24&
ct12-Argument-Structure-Analogies.md&2.24&. . .
ct12-Argument-Structure-Analogies.md&2.24&
ct12-Argument-Structure-Analogies.md&2.24&Hidden premise: Babies and mobile phones disturb others.  
ct12-Argument-Structure-Analogies.md&2.24&Premise: Anything that disturbs others should not be allowed in the classroom.
ct12-Argument-Structure-Analogies.md&2.24&
ct12-Argument-Structure-Analogies.md&2.24&. . .
ct12-Argument-Structure-Analogies.md&2.24&
ct12-Argument-Structure-Analogies.md&2.24&Ultimate onclusion: Babies and mobile phones should not be allowed in the classroom.
ct12-Argument-Structure-Analogies.md&2.24&
ct12-Argument-Structure-Analogies.md&2.24&(Valid; used all statements from the question).
ct12-Argument-Structure-Analogies.md&2.24&
ct12-Argument-Structure-Analogies.md&2.24&
ct12-Argument-Structure-Analogies.md&2.24&
ct12-Argument-Structure-Analogies.md&2.25&## Hidden premises
ct12-Argument-Structure-Analogies.md&2.25&
ct12-Argument-Structure-Analogies.md&2.25&"No one comes to our party. They must all hate us." -- Which of the following is a hidden premise of this argument?
ct12-Argument-Structure-Analogies.md&2.25&
ct12-Argument-Structure-Analogies.md&2.25&1. All people hate us.
ct12-Argument-Structure-Analogies.md&2.25&2. Only if people hate us they will not come to our party.
ct12-Argument-Structure-Analogies.md&2.25&3. If people hate us they will not come to our party.
ct12-Argument-Structure-Analogies.md&2.25&4. Unless people do not come to our party, they do not hate us.
ct12-Argument-Structure-Analogies.md&2.25&
ct12-Argument-Structure-Analogies.md&2.25&. . .
ct12-Argument-Structure-Analogies.md&2.25&
ct12-Argument-Structure-Analogies.md&2.25&Assume *H: All people hate us. C: People come to our party.*
ct12-Argument-Structure-Analogies.md&2.25&
ct12-Argument-Structure-Analogies.md&2.25&\~C $\vdash$ H. Which premise is missing?
ct12-Argument-Structure-Analogies.md&2.25&
ct12-Argument-Structure-Analogies.md&2.25&>- Answer 4 does not make sense (one "not" too much).
ct12-Argument-Structure-Analogies.md&2.25&>- Answer 3 (H$\to$\~C) would affirm the consequent and give an invalid argument! Even if 3 was true, from the fact that they did not come to our party, we cannot conclude that they hate us.
ct12-Argument-Structure-Analogies.md&2.25&>- Answer 1 obviously does not follow.
ct12-Argument-Structure-Analogies.md&2.25&>- Answer 2 (H$\leftrightarrow$\~C) is correct.
ct12-Argument-Structure-Analogies.md&2.25&
ct12-Argument-Structure-Analogies.md&2.26&## Hidden premise
ct12-Argument-Structure-Analogies.md&2.26&
ct12-Argument-Structure-Analogies.md&2.26&"Peter is a bookworm, for he never plays with anyone." -- Which is the most appropriate hidden premise?
ct12-Argument-Structure-Analogies.md&2.26&
ct12-Argument-Structure-Analogies.md&2.26&1. Bookworms usually do not play with anyone.
ct12-Argument-Structure-Analogies.md&2.26&2. Only bookworms never play with anyone.
ct12-Argument-Structure-Analogies.md&2.26&3. Peter likes to read books.
ct12-Argument-Structure-Analogies.md&2.26&4. If Peter plays with other people, he's not a bookworm.
ct12-Argument-Structure-Analogies.md&2.26&
ct12-Argument-Structure-Analogies.md&2.26&. . .
ct12-Argument-Structure-Analogies.md&2.26&
ct12-Argument-Structure-Analogies.md&2.26&>- Answer 1 makes some sense, but is not conclusive. Because other people (let's say, computer geeks) also might not play with anyone. So Peter could be a computer geek instead of a bookworm if this hidden premise was true.
ct12-Argument-Structure-Analogies.md&2.26&>- Answer 2 excludes the possibility of computer geeks fulfilling the criteria of the bookworms, and so produces a valid argument.
ct12-Argument-Structure-Analogies.md&2.26&
ct12-Argument-Structure-Analogies.md&2.27&## Conclusion and hidden premises (1)
ct12-Argument-Structure-Analogies.md&2.27&
ct12-Argument-Structure-Analogies.md&2.27&"The Electricity Company's business is public utility. What it does affects the lives of many people. So it does have social responsibilities. But it is not taking its social responsibilities seriously. If it did, it would not just be doing everything to maximise its own profits. The government should consider not renewing the Electricity Company's franchise."
ct12-Argument-Structure-Analogies.md&2.27&
ct12-Argument-Structure-Analogies.md&2.27&What is the conclusion of this argument? Are there any hidden premises?
ct12-Argument-Structure-Analogies.md&2.27&
ct12-Argument-Structure-Analogies.md&2.27&. . .
ct12-Argument-Structure-Analogies.md&2.27&
ct12-Argument-Structure-Analogies.md&2.27&>- Conclusion: The government should consider not renewing the Electricity Company's franchise.
ct12-Argument-Structure-Analogies.md&2.27&
ct12-Argument-Structure-Analogies.md&2.28&## Conclusion and hidden premises (2)
ct12-Argument-Structure-Analogies.md&2.28&
ct12-Argument-Structure-Analogies.md&2.28&>- Premise: What the Electricity Company does affects the lives of many people.
ct12-Argument-Structure-Analogies.md&2.28&>- Hidden premise: Whoever affects the lives of many people has social responsibilities.
ct12-Argument-Structure-Analogies.md&2.28&>- Sub-conclusion: So the Electricity Company does have social responsibilities.
ct12-Argument-Structure-Analogies.md&2.28&
ct12-Argument-Structure-Analogies.md&2.28&>- Premise: If a company takes its social responsibilities seriously, it does not do everything to maximise its own profits.
ct12-Argument-Structure-Analogies.md&2.28&>- Hidden premise: The Electricity Company does everything just to maximise its own profits.
ct12-Argument-Structure-Analogies.md&2.28&>- Sub-conclusion: The Electricity Company is not taking its social responsibilities seriously. (Denying the consequent.)
ct12-Argument-Structure-Analogies.md&2.28&
ct12-Argument-Structure-Analogies.md&2.29&## Conclusion and hidden premises (3)
ct12-Argument-Structure-Analogies.md&2.29&
ct12-Argument-Structure-Analogies.md&2.29&>- Hidden premise: The government should consider not renewing the franchise of public utilities that don't take their social responsibilities seriously.
ct12-Argument-Structure-Analogies.md&2.29&>- Premise: The Electricity Company is a public utility.
ct12-Argument-Structure-Analogies.md&2.29&>- Premise: The Electricity Company is not taking its social responsibilities seriously.
ct12-Argument-Structure-Analogies.md&2.29&>- Ultimate conclusion: The government should consider not renewing the Electricity Company's franchise.
ct12-Argument-Structure-Analogies.md&2.29&
ct12-Argument-Structure-Analogies.md&2.29&
ct12-Argument-Structure-Analogies.md&2.30&## Conclusion and hidden premises (1)
ct12-Argument-Structure-Analogies.md&2.30&
ct12-Argument-Structure-Analogies.md&2.30&"The real justification for progressive taxation is that the wealthier receive more benefits from government and, therefore, ought to pay more. This may not be true in a strict accounting sense; the middle and upper classes cannot apply for public housing, for example. But the essence of government is to preserve the social order. The well-off benefit from this more than the poor."
ct12-Argument-Structure-Analogies.md&2.30&
ct12-Argument-Structure-Analogies.md&2.30&(“Progressive taxation” means: The more money you have, the more taxes you pay. Wealthier people pay more taxes than poor people.)
ct12-Argument-Structure-Analogies.md&2.30&
ct12-Argument-Structure-Analogies.md&2.30&What is the ultimate conclusion of this argument? Any hidden premises?
ct12-Argument-Structure-Analogies.md&2.30&
ct12-Argument-Structure-Analogies.md&2.30&
ct12-Argument-Structure-Analogies.md&2.31&## Conclusion and hidden premises (2)
ct12-Argument-Structure-Analogies.md&2.31&
ct12-Argument-Structure-Analogies.md&2.31&>- "The real justification for progressive taxation is that the wealthier receive more benefits from government and, therefore, ought to pay more. This may not be true in a strict accounting sense; the middle and upper classes cannot apply for public housing, for example. But the essence of government is to preserve the social order. The well-off benefit from this more than the poor."
ct12-Argument-Structure-Analogies.md&2.31&>- Conclusion?
ct12-Argument-Structure-Analogies.md&2.31&>- Conclusion: Therefore, the wealthier ought to pay more taxes.
ct12-Argument-Structure-Analogies.md&2.31&>- Now take all the sentences of the argument apart, one by one, and see what function they have. Can or should we rearrange them?
ct12-Argument-Structure-Analogies.md&2.31&
ct12-Argument-Structure-Analogies.md&2.31&
ct12-Argument-Structure-Analogies.md&2.32&## Conclusion and hidden premises (3)
ct12-Argument-Structure-Analogies.md&2.32&
ct12-Argument-Structure-Analogies.md&2.32&Statements (in the original order):
ct12-Argument-Structure-Analogies.md&2.32&
ct12-Argument-Structure-Analogies.md&2.32&- The real justification for progressive taxation is that the wealthier receive more benefits from government.
ct12-Argument-Structure-Analogies.md&2.32&- Therefore, the wealthy ought to pay more taxes. 
ct12-Argument-Structure-Analogies.md&2.32&- This may not be true in a strict accounting sense.
ct12-Argument-Structure-Analogies.md&2.32&- ... because the middle and upper classes cannot apply for public housing, for example. 
ct12-Argument-Structure-Analogies.md&2.32&- But the essence of government is to preserve the social order. 
ct12-Argument-Structure-Analogies.md&2.32&- The well-off benefit from social order more than the poor.
ct12-Argument-Structure-Analogies.md&2.32&
ct12-Argument-Structure-Analogies.md&2.32&
ct12-Argument-Structure-Analogies.md&2.33&## Conclusion and hidden premises (4)
ct12-Argument-Structure-Analogies.md&2.33&
ct12-Argument-Structure-Analogies.md&2.33&Now arrange these statements in a logical order: 
ct12-Argument-Structure-Analogies.md&2.33&
ct12-Argument-Structure-Analogies.md&2.33&>- Premise: The well-off benefit from social order more than the poor.
ct12-Argument-Structure-Analogies.md&2.33&>- Premise: The wealthier receive more benefits from government.
ct12-Argument-Structure-Analogies.md&2.33&>- *Hidden premise: Whoever receives more benefits ought to pay more taxes.*
ct12-Argument-Structure-Analogies.md&2.33&>- (counter-argument) That the wealthier receive more benefits may not be true in a strict accounting sense (example: the middle and upper classes cannot apply for public housing) 
ct12-Argument-Structure-Analogies.md&2.33&>- Premise: (counter-counter-argument): But the essence of government is to preserve the social order. 
ct12-Argument-Structure-Analogies.md&2.33&>- Conclusion: Therefore, the wealthier ought to pay more taxes.
ct12-Argument-Structure-Analogies.md&2.33&
ct12-Argument-Structure-Analogies.md&2.33&
ct12-Argument-Structure-Analogies.md&2.34&## Conclusion and hidden premises (5)
ct12-Argument-Structure-Analogies.md&2.34&
ct12-Argument-Structure-Analogies.md&2.34&Another version of the same:
ct12-Argument-Structure-Analogies.md&2.34&
ct12-Argument-Structure-Analogies.md&2.34&>- Premise: The essence of government is to preserve the social order.
ct12-Argument-Structure-Analogies.md&2.34&>- Premise: The well-off benefit from preserving the social order more than the poor.
ct12-Argument-Structure-Analogies.md&2.34&>- Sub-conslusion: The well-off benefit more from the government than the poor.
ct12-Argument-Structure-Analogies.md&2.34&
ct12-Argument-Structure-Analogies.md&2.34&>- Premise: The well-off benefit more from the government than the poor.
ct12-Argument-Structure-Analogies.md&2.34&>- Hidden premise: If a group of people receives more benefits from the government, then they ought to pay more taxes.
ct12-Argument-Structure-Analogies.md&2.34&>- Conclusion: The well-off ought to pay more taxes.
ct12-Argument-Structure-Analogies.md&2.34&
ct12-Argument-Structure-Analogies.md&2.35&## Conclusion?
ct12-Argument-Structure-Analogies.md&2.35&
ct12-Argument-Structure-Analogies.md&2.35&In the following passage, what is the conclusion?
ct12-Argument-Structure-Analogies.md&2.35&
ct12-Argument-Structure-Analogies.md&2.35&"Logic is difficult. Reasoning with abstract symbols is difficult, and logic, like mathematics, involves reasoning with abstract symbols."
ct12-Argument-Structure-Analogies.md&2.35&
ct12-Argument-Structure-Analogies.md&2.35&1. Logic is difficult.
ct12-Argument-Structure-Analogies.md&2.35&2. Reasoning with abstract symbols is difficult.
ct12-Argument-Structure-Analogies.md&2.35&3. Logic is like mathematics.
ct12-Argument-Structure-Analogies.md&2.35&4. Logic involves reasoning with abstract symbols.
ct12-Argument-Structure-Analogies.md&2.35&
ct12-Argument-Structure-Analogies.md&2.35&. . .
ct12-Argument-Structure-Analogies.md&2.35&
ct12-Argument-Structure-Analogies.md&2.35&Answer: 1.
ct12-Argument-Structure-Analogies.md&2.35&
ct12-Argument-Structure-Analogies.md&2.36&## Different terms
ct12-Argument-Structure-Analogies.md&2.36&
ct12-Argument-Structure-Analogies.md&2.36&Different teachers and books use slightly different terms. So:
ct12-Argument-Structure-Analogies.md&2.36&
ct12-Argument-Structure-Analogies.md&2.36&- Sub-conclusion means the same as intermediate conclusion.
ct12-Argument-Structure-Analogies.md&2.36&- Final conclusion (or just conclusion) means the same as ultimate conclusion.
ct12-Argument-Structure-Analogies.md&2.36&
ct12-Argument-Structure-Analogies.md&2.36&
ct12-Argument-Structure-Analogies.md&3.0&# Analogies
ct12-Argument-Structure-Analogies.md&3.0& 
ct12-Argument-Structure-Analogies.md&3.1&## Fill in the blanks!
ct12-Argument-Structure-Analogies.md&3.1&
ct12-Argument-Structure-Analogies.md&3.1&- Grass is to cows as _______ is to humans
ct12-Argument-Structure-Analogies.md&3.1&- Wheels are to cars what _______ are to humans
ct12-Argument-Structure-Analogies.md&3.1&- The sea is to a ship what _______ is to a car
ct12-Argument-Structure-Analogies.md&3.1&
ct12-Argument-Structure-Analogies.md&3.2&## Fill in the blanks!
ct12-Argument-Structure-Analogies.md&3.2&
ct12-Argument-Structure-Analogies.md&3.2&>- Grass is to cows as **rice** is to humans (not: “food”)
ct12-Argument-Structure-Analogies.md&3.2&>- Wheels are to cars what **legs** are to humans
ct12-Argument-Structure-Analogies.md&3.2&>- The sea is to a ship what **a road** is to a car
ct12-Argument-Structure-Analogies.md&3.2&
ct12-Argument-Structure-Analogies.md&3.3&## Analogy
ct12-Argument-Structure-Analogies.md&3.3&
ct12-Argument-Structure-Analogies.md&3.3&>- In *deduction,* we often draw a particular conclusion from a general rule.
ct12-Argument-Structure-Analogies.md&3.3&>- In *induction,* we often draw a general conclusion from particular instances.
ct12-Argument-Structure-Analogies.md&3.3&>- In *analogy,* we draw a particular conclusion from another, similar particular instance. Since the conclusion is not certain, analogies are a type of inductive argument.
ct12-Argument-Structure-Analogies.md&3.3&
ct12-Argument-Structure-Analogies.md&3.4&## Analogy in science and law
ct12-Argument-Structure-Analogies.md&3.4&
ct12-Argument-Structure-Analogies.md&3.4&>- Analogies are used in simulations. Every simulation of a process is an analogy.
ct12-Argument-Structure-Analogies.md&3.4&>-  Sometimes analogies are used in teaching (electronic circuits are compared to water pipes).
ct12-Argument-Structure-Analogies.md&3.4&>-  Analogies are used in law, when there is another similar case which has been decided on before.
ct12-Argument-Structure-Analogies.md&3.4&
ct12-Argument-Structure-Analogies.md&3.5&## What do you think of this analogy?
ct12-Argument-Structure-Analogies.md&3.5&
ct12-Argument-Structure-Analogies.md&3.5&- The universe is like a very complicated watch.
ct12-Argument-Structure-Analogies.md&3.5&- A watch must have been designed by a watchmaker.
ct12-Argument-Structure-Analogies.md&3.5&- Therefore, the universe must have been designed by some kind of creator.
ct12-Argument-Structure-Analogies.md&3.5&
ct12-Argument-Structure-Analogies.md&3.6&## False analogies
ct12-Argument-Structure-Analogies.md&3.6&
ct12-Argument-Structure-Analogies.md&3.6&>- An analogy can easily turn out to be false.
ct12-Argument-Structure-Analogies.md&3.6&>- This happens when the things compared share only *some* features, but are *different* in others, and the differences are greater than the similarities.
ct12-Argument-Structure-Analogies.md&3.6&
ct12-Argument-Structure-Analogies.md&3.7&## Universe and (mechanical) watch
ct12-Argument-Structure-Analogies.md&3.7&
ct12-Argument-Structure-Analogies.md&3.7&![](graphics/15-universe-watch.png)\ 
ct12-Argument-Structure-Analogies.md&3.7&
ct12-Argument-Structure-Analogies.md&3.7&So the real question is:  Is “having a creator” one of the *common* features or one of the *differences* between the two things? This question is not answered here!
ct12-Argument-Structure-Analogies.md&3.7&
ct12-Argument-Structure-Analogies.md&3.7&
ct12-Argument-Structure-Analogies.md&3.8&## Form of analogical arguments
ct12-Argument-Structure-Analogies.md&3.8&
ct12-Argument-Structure-Analogies.md&3.8&(Premise 1) Object A and object B are similar in having properties Q~1~ ... Q~n~.  
ct12-Argument-Structure-Analogies.md&3.8&(Premise 2) Object A has property P.  
ct12-Argument-Structure-Analogies.md&3.8& ------------------------------------------------------------  
ct12-Argument-Structure-Analogies.md&3.8&(Conclusion) Object B also (probably) has property P.
ct12-Argument-Structure-Analogies.md&3.8&
ct12-Argument-Structure-Analogies.md&3.8&\vspace{3ex}
ct12-Argument-Structure-Analogies.md&3.8&
ct12-Argument-Structure-Analogies.md&3.8&Note that P is not in Q~1~ ... Q~n~. If it was, the argument would be deductive!
ct12-Argument-Structure-Analogies.md&3.8&
ct12-Argument-Structure-Analogies.md&3.8&
ct12-Argument-Structure-Analogies.md&3.9&## Example
ct12-Argument-Structure-Analogies.md&3.9&
ct12-Argument-Structure-Analogies.md&3.9&(Premise 1) Object A and object B have common properties Q~1~ ... Q~n~.  
ct12-Argument-Structure-Analogies.md&3.9&(Premise 2) Object A has property P.  
ct12-Argument-Structure-Analogies.md&3.9& ------------------------------------------------------------  
ct12-Argument-Structure-Analogies.md&3.9&(Conclusion) Object B also (probably) has property P.
ct12-Argument-Structure-Analogies.md&3.9&
ct12-Argument-Structure-Analogies.md&3.9&**Example:**
ct12-Argument-Structure-Analogies.md&3.9&
ct12-Argument-Structure-Analogies.md&3.9&Paul is funny (Q1), likes to read Harry Potter (Q2), and he likes to listen to K-Pop music (Q3). I like Paul (P).  
ct12-Argument-Structure-Analogies.md&3.9&Peter is funny (Q1). Peter likes to read Harry Potter (Q2). Peter likes K-Pop music (Q3).  
ct12-Argument-Structure-Analogies.md&3.9& ------------------------------------------------------------  
ct12-Argument-Structure-Analogies.md&3.9&Because Paul and Peter share Q1...Q3, therefore I am justified in 
ct12-Argument-Structure-Analogies.md&3.9&believing that they will also share property P, that means: that I
ct12-Argument-Structure-Analogies.md&3.9&will also like Peter.  
ct12-Argument-Structure-Analogies.md&3.9&**Conclusion:** I will (*probably*) also like Peter.
ct12-Argument-Structure-Analogies.md&3.9&
ct12-Argument-Structure-Analogies.md&3.9&
ct12-Argument-Structure-Analogies.md&3.10&## Analogies as inductive arguments
ct12-Argument-Structure-Analogies.md&3.10&
ct12-Argument-Structure-Analogies.md&3.10&>- This is why analogies are a kind of *inductive* argument!
ct12-Argument-Structure-Analogies.md&3.10&>- If an analogy is weak or strong depends on the number and relevance of features that are similar in the two things that we compare to each other.
ct12-Argument-Structure-Analogies.md&3.10&
ct12-Argument-Structure-Analogies.md&3.10&. . . 
ct12-Argument-Structure-Analogies.md&3.10&
ct12-Argument-Structure-Analogies.md&3.10&**Example:**
ct12-Argument-Structure-Analogies.md&3.10&
ct12-Argument-Structure-Analogies.md&3.10&>- Paul is 1.75cm in height and has black hair. He is a good friend of mine.
ct12-Argument-Structure-Analogies.md&3.10&>- Peter is 1.75cm in height and has black hair too. He will surely also be a good friend to me.
ct12-Argument-Structure-Analogies.md&3.10&>- Weak argument, because the similarities are not relevant to the conclusion: Height or hair colour are *not* important in friendships, therefore this argument is weak. 
ct12-Argument-Structure-Analogies.md&3.10&>- In contrast, what one likes to read or the music one likes *is* important to friendship, therefore these features make a stronger analogical argument.
ct12-Argument-Structure-Analogies.md&3.10&
ct12-Argument-Structure-Analogies.md&3.10&
ct12-Argument-Structure-Analogies.md&3.10&
ct12-Argument-Structure-Analogies.md&3.10&
ct12-Argument-Structure-Analogies.md&3.10&
ct12-Argument-Structure-Analogies.md&3.10&
ct12-Argument-Structure-Analogies.md&3.10&
ct12-Argument-Structure-Analogies.md&3.10&
ct12-Argument-Structure-Analogies.md&3.11&## Which analogy is better? Why?
ct12-Argument-Structure-Analogies.md&3.11&
ct12-Argument-Structure-Analogies.md&3.11&1. Beijing is in the north of China and therefore it has cold, dry weather. Hong Kong is also in the north of China, and therefore it too has cold, dry weather.
ct12-Argument-Structure-Analogies.md&3.11&2. Shenzhen is in the south of China, and therefore it has hot, humid weather. Hong Kong is also in the south of China, and therefore it too has hot, humid weather.
ct12-Argument-Structure-Analogies.md&3.11&
ct12-Argument-Structure-Analogies.md&3.12&## Criterion 1: Truth
ct12-Argument-Structure-Analogies.md&3.12&
ct12-Argument-Structure-Analogies.md&3.12&First of all we need to check that the two objects being compared are indeed similar in the way assumed.
ct12-Argument-Structure-Analogies.md&3.12&
ct12-Argument-Structure-Analogies.md&3.12&For example, in the argument we just looked at, it is simply not true that Hong Kong is in the north of China. Therefore, the analogy number 1 is wrong.
ct12-Argument-Structure-Analogies.md&3.12&
ct12-Argument-Structure-Analogies.md&3.13&## Which analogy is better? Why?
ct12-Argument-Structure-Analogies.md&3.13&
ct12-Argument-Structure-Analogies.md&3.13&1. “This novel has a similar plot like the other one we have read, so probably it is also very boring.”
ct12-Argument-Structure-Analogies.md&3.13&2. “This novel has a similar cover like the other one we have read, so probably it is also very boring.”
ct12-Argument-Structure-Analogies.md&3.13&
ct12-Argument-Structure-Analogies.md&3.14&## Criterion 2: Relevance
ct12-Argument-Structure-Analogies.md&3.14&
ct12-Argument-Structure-Analogies.md&3.14&Even if two objects are similar, we also need to make sure that those aspects in which they are similar are actually *relevant* to the conclusion.
ct12-Argument-Structure-Analogies.md&3.14&
ct12-Argument-Structure-Analogies.md&3.15&## Irrelevant feature analogy
ct12-Argument-Structure-Analogies.md&3.15&
ct12-Argument-Structure-Analogies.md&3.15&This is a common trick in politics, because it allows for easy solutions to complex problems. 
ct12-Argument-Structure-Analogies.md&3.15&
ct12-Argument-Structure-Analogies.md&3.15&For example, the terrorists who hijacked an airplane all had long beards, and therefore now the government is increasing the security checks for people with long beards.
ct12-Argument-Structure-Analogies.md&3.15&
ct12-Argument-Structure-Analogies.md&3.15&Generally, we need to ensure that having properties Q~1~ ... Q~n~ *actually increases the probability* of an object having property P. 
ct12-Argument-Structure-Analogies.md&3.15&
ct12-Argument-Structure-Analogies.md&3.16&## Which analogy is better? Why?
ct12-Argument-Structure-Analogies.md&3.16&
ct12-Argument-Structure-Analogies.md&3.16&1. “This novel is supposed to have a similar plot like the other one we have read. Additionally, it is written by the same author, and the same friend recommended them to me. So probably it is also very boring.”
ct12-Argument-Structure-Analogies.md&3.16&2. “This novel is supposed to have a similar plot like the other one we have read and the same friend recommended them to me. So probably it is also very boring.”
ct12-Argument-Structure-Analogies.md&3.16&
ct12-Argument-Structure-Analogies.md&3.17&## Criterion 3: Number 
ct12-Argument-Structure-Analogies.md&3.17&
ct12-Argument-Structure-Analogies.md&3.17&If we discover a lot of shared properties between two objects, and they are all relevant to the conclusion, then the analogical argument is stronger than when we can only identify one or a few shared properties.
ct12-Argument-Structure-Analogies.md&3.17&
ct12-Argument-Structure-Analogies.md&3.17&Suppose we find out that novel B is not just similar to another boring novel A with a similar plot. We discover that the two novels are written by the same author, and that very few of both novels have been sold. Then we can justifiably be more confident in concluding that B is likely to be boring novel. 
ct12-Argument-Structure-Analogies.md&3.17&
ct12-Argument-Structure-Analogies.md&3.18&## Which analogy is better? Why?
ct12-Argument-Structure-Analogies.md&3.18&
ct12-Argument-Structure-Analogies.md&3.18&1. “Italian restaurant A is very good. Restaurant B buys its ingredients from the same supplier, it uses the same brand of pasta, it uses the same olive oil, and all ingredients are of the same quality. So restaurant B will also be good.”
ct12-Argument-Structure-Analogies.md&3.18&2. “Italian restaurant A is very good. Restaurant B buys its ingredients from the same supplier, both have lots of customers, and both restaurants have got an award from the same magazine. So restaurant B will also be good.”
ct12-Argument-Structure-Analogies.md&3.18&
ct12-Argument-Structure-Analogies.md&3.19&## Criterion 4: Diversity
ct12-Argument-Structure-Analogies.md&3.19&
ct12-Argument-Structure-Analogies.md&3.19&B uses the same olive oil in cooking as A, and buys meat and vegetables of the same quality from the same supplier. Such information of course increases the probability that B also serves good food. But the information we have so far are all of the same kind having to do with the quality of the raw cooking ingredients.
ct12-Argument-Structure-Analogies.md&3.19&
ct12-Argument-Structure-Analogies.md&3.19&But if we are told that both restaurants have lots of customers, and that both restaurants have obtained awards from the same magazine, then these different aspects of similarities are going to increase our confidence in the conclusion a lot more.
ct12-Argument-Structure-Analogies.md&3.19&
ct12-Argument-Structure-Analogies.md&3.20&## Which analogy is better? Why?
ct12-Argument-Structure-Analogies.md&3.20&
ct12-Argument-Structure-Analogies.md&3.20&1. “Italian restaurant A is very good. Restaurant B buys its ingredients from the same supplier, both have lots of customers, and both restaurants have got an award from the same magazine. So restaurant B will also be good.”
ct12-Argument-Structure-Analogies.md&3.20&2. “Italian restaurant A is very good. Restaurant B buys its ingredients from the same supplier, both have lots of customers, and both restaurants have got an award from the same magazine, and restaurant B has recently changed its owner. So restaurant B will also be good.”
ct12-Argument-Structure-Analogies.md&3.20&
ct12-Argument-Structure-Analogies.md&3.21&## Criterion 4: Disanalogy
ct12-Argument-Structure-Analogies.md&3.21&
ct12-Argument-Structure-Analogies.md&3.21&Even if two objects A and B are similar in lots of relevant respects, we should also consider whether there are dissimilarities between A and B which might cast doubt on the conclusion.
ct12-Argument-Structure-Analogies.md&3.21&
ct12-Argument-Structure-Analogies.md&3.21&For example, returning to the restaurant example, if we find out that restaurant B now has a new owner who has just hired a team of very bad cooks, we would think that the food is probably not going to be good anymore despite being the same as A in many other ways.
ct12-Argument-Structure-Analogies.md&3.21&
ct12-Argument-Structure-Analogies.md&3.22&## Criteria for a good analogical argument
ct12-Argument-Structure-Analogies.md&3.22&
ct12-Argument-Structure-Analogies.md&3.22&1. Truth of similarities
ct12-Argument-Structure-Analogies.md&3.22&2. Relevance of similarities
ct12-Argument-Structure-Analogies.md&3.22&3. Number of similarities
ct12-Argument-Structure-Analogies.md&3.22&4. Diversity of similarities
ct12-Argument-Structure-Analogies.md&3.22&5. Absence of significant dissimilarities
ct12-Argument-Structure-Analogies.md&3.22&
ct12-Argument-Structure-Analogies.md&3.22&
ct12-Argument-Structure-Analogies.md&3.23&## “Strong” and “weak” analogies
ct12-Argument-Structure-Analogies.md&3.23&
ct12-Argument-Structure-Analogies.md&3.23&>- Analogies are a kind of inductive argument, because their conclusions are only *probable* (if the premises are true), but not *certain.*
ct12-Argument-Structure-Analogies.md&3.23&>- The criteria above are ways in which we can increase the confidence that the conclusion will be true (given that the premises are true, that means, given that the similarities exist and are relevant).
ct12-Argument-Structure-Analogies.md&3.23&>- So we could talk of “weak” and “strong” analogies, as we talk about weak and strong inductive arguments. (It is more common to speak of “good” and “bad” analogies, though).
ct12-Argument-Structure-Analogies.md&3.23&
ct12-Argument-Structure-Analogies.md&3.23&
ct12-Argument-Structure-Analogies.md&3.24&## Is this analogy good? Why or why not?
ct12-Argument-Structure-Analogies.md&3.24&
ct12-Argument-Structure-Analogies.md&3.24&We should not blame the media for having a bad influence on moral standards. Newspapers and TV are like weather reporters who report the facts. We do not blame weather reports for telling us that the weather is bad.
ct12-Argument-Structure-Analogies.md&3.24&
ct12-Argument-Structure-Analogies.md&3.25&## Bad analogy
ct12-Argument-Structure-Analogies.md&3.25&
ct12-Argument-Structure-Analogies.md&3.25&News don’t report the pure facts, but almost always add an attitude towards or evaluation of the facts. It is this evaluation that might be problematic. 
ct12-Argument-Structure-Analogies.md&3.25&
ct12-Argument-Structure-Analogies.md&3.25&Weather reports do not change the weather, but newspaper reports and the public media can influence people and have an indirect effect on moral standards.
ct12-Argument-Structure-Analogies.md&3.25&
ct12-Argument-Structure-Analogies.md&3.26&## Is this analogy good? Why or why not?
ct12-Argument-Structure-Analogies.md&3.26&
ct12-Argument-Structure-Analogies.md&3.26&Democracy does not work in a family. Parents should have the ultimate say because they are wiser and their children do not know what is best for themselves. Similarly the best form of government for a society is not a democratic one but one where the leaders are more like parents and the citizens have to obey them.
ct12-Argument-Structure-Analogies.md&3.26&
ct12-Argument-Structure-Analogies.md&3.27&## Bad analogy
ct12-Argument-Structure-Analogies.md&3.27&
ct12-Argument-Structure-Analogies.md&3.27&There are many relevant ways in which a family is different from a society. 
ct12-Argument-Structure-Analogies.md&3.27&
ct12-Argument-Structure-Analogies.md&3.27&- First, the government officials need not be wiser than the citizens.
ct12-Argument-Structure-Analogies.md&3.27&- Also, many parents might care for their children out of love and affection but government officials might not always have the interests of the people at heart.
ct12-Argument-Structure-Analogies.md&3.27&
ct12-Argument-Structure-Analogies.md&3.27&
ct12-Argument-Structure-Analogies.md&3.28&## What did we learn today?
ct12-Argument-Structure-Analogies.md&3.28&
ct12-Argument-Structure-Analogies.md&3.28&- How to restructure and analyse arguments.
ct12-Argument-Structure-Analogies.md&3.28&- What analogies are, and how to evaluate them.
ct12-Argument-Structure-Analogies.md&3.28&
ct12-Argument-Structure-Analogies.md&3.29&## Any questions?
ct12-Argument-Structure-Analogies.md&3.29&
ct12-Argument-Structure-Analogies.md&3.29&![](graphics/questions.jpg)\ 
ct12-Argument-Structure-Analogies.md&3.29&
ct12-Argument-Structure-Analogies.md&3.30&## Thank you for your attention!
ct12-Argument-Structure-Analogies.md&3.30&
ct12-Argument-Structure-Analogies.md&3.30&Email: <matthias@ln.edu.hk>
ct12-Argument-Structure-Analogies.md&3.30&
ct12-Argument-Structure-Analogies.md&3.30&If you have questions, please come to my office hours (see course outline).
ct13-Fallacies-1.md&0.1&% Critical Thinking
ct13-Fallacies-1.md&0.1&% 13. Fallacies (1)
ct13-Fallacies-1.md&0.1&% A. Matthias
ct13-Fallacies-1.md&0.1&
ct13-Fallacies-1.md&0.1&
ct13-Fallacies-1.md&1.0&# Where we are
ct13-Fallacies-1.md&1.0&
ct13-Fallacies-1.md&1.1&## What did we learn last time?
ct13-Fallacies-1.md&1.1&
ct13-Fallacies-1.md&1.1&- Argument analysis, restructuring, hidden premises, conclusions.
ct13-Fallacies-1.md&1.1&- Analogies.
ct13-Fallacies-1.md&1.1&
ct13-Fallacies-1.md&1.2&## What are we going to learn today?
ct13-Fallacies-1.md&1.2&
ct13-Fallacies-1.md&1.2&- What fallacies are, and a few common types.
ct13-Fallacies-1.md&1.2&
ct13-Fallacies-1.md&1.3&## Core and extended fallacies
ct13-Fallacies-1.md&1.3&
ct13-Fallacies-1.md&1.3&>- Since different teachers teach this course, and not all focus on fallacies, we have two categories of fallacies:
ct13-Fallacies-1.md&1.3&>     - ‘Core’ fallacies are those taught in all sections of this course. They are tested in the common part of the final exam.
ct13-Fallacies-1.md&1.3&>     - ‘Extended’ are all the other fallacies. I may ask you about them in my own part of the exam.
ct13-Fallacies-1.md&1.3&>- Core fallacies are: Hasty generalisation, begging the question, equivocation, denying the antecedent, affirming the consequent and wishful thinking.
ct13-Fallacies-1.md&1.3&
ct13-Fallacies-1.md&1.4&## A, B and C fallacies
ct13-Fallacies-1.md&1.4&
ct13-Fallacies-1.md&1.4&>- The other fallacies in this presentation are specific to my class. You still have to learn them, but you will find that other sections might talk about other topics instead.
ct13-Fallacies-1.md&1.4&>- Because there are so many, I have graded the following pages into A (very important and core), B (important) and C (not that important).
ct13-Fallacies-1.md&1.4&>- Essentially, the letter is the reverse of the grade you want to get with fallacy questions: For an A, you’ll need A, B and C fallacies. For a B, you will need A and B fallacies. For a C, you will need only the A fallacies.
ct13-Fallacies-1.md&1.4&>- You could also see the letters as indicators of the probability that a question about a fallacy will be in an exam: A fallacies will very likely be asked in an exam; B fallacies are likely; and C fallacies are less likely (but not impossible!)
ct13-Fallacies-1.md&1.4&
ct13-Fallacies-1.md&2.0&# Fallacies Group A
ct13-Fallacies-1.md&2.0&
ct13-Fallacies-1.md&2.1&## A: Which are “acceptable” and “bad” arguments?
ct13-Fallacies-1.md&2.1&
ct13-Fallacies-1.md&2.1&You can’t be serious that smoking should be banned completely! This must be a joke! On the one hand, the government has a lot of income from smoking. If smoking was banned, this income would be missing, more people would be unemployed, and they would go to the streets and become criminals. So if we ban smoking, the streets will be full of criminals and we will not be able to go out any more! Also, the black market for cigarettes will increase. And I know only one person who is actually against smoking, and this is a stupid, ugly guy, who has no friends. All cool people agree that banning smoking is a bad idea!
ct13-Fallacies-1.md&2.1&
ct13-Fallacies-1.md&2.2&## A: *Acceptable* and **bad** arguments
ct13-Fallacies-1.md&2.2&
ct13-Fallacies-1.md&2.2&**You can’t be serious** that smoking should be banned completely! This must be a joke! On the one hand, *the government has a lot of income from smoking.* If smoking was banned, *this income would be missing,* *more people would be unemployed,* and **they would go to the streets and become criminals.** So if we ban smoking, the **streets will be full of criminals and we will not be able to go out any more**! Also, *the black market for cigarettes will increase.* And I know only one person who is actually against smoking, and this is **a stupid, ugly guy, who has no friends.** All **cool people agree** that banning smoking is a bad idea!
ct13-Fallacies-1.md&2.2&
ct13-Fallacies-1.md&2.3&## A: Fallacies
ct13-Fallacies-1.md&2.3&
ct13-Fallacies-1.md&2.3&>- Fallacies are errors in argumentation.
ct13-Fallacies-1.md&2.3&>- In a fallacy, *the premises do not support the conclusion at all.*
ct13-Fallacies-1.md&2.3&>- Arguments containing fallacies are not valid, but sometimes they seem to be convincing.
ct13-Fallacies-1.md&2.3&>- Very often fallacies are made by mistake.
ct13-Fallacies-1.md&2.3&>- Sometimes they are made on purpose, when the speaker does not have any correct arguments for his cause.
ct13-Fallacies-1.md&2.3&  
ct13-Fallacies-1.md&2.4&## A: What is a fallacy?
ct13-Fallacies-1.md&2.4&
ct13-Fallacies-1.md&2.4&We said that an argument must have reasons that are relevant to a conclusion:
ct13-Fallacies-1.md&2.4&
ct13-Fallacies-1.md&2.4&\f{Argument = Relevant reasons + logical connections}
ct13-Fallacies-1.md&2.4&
ct13-Fallacies-1.md&2.4&- Relevant reasons: Evidence, facts.
ct13-Fallacies-1.md&2.4&- Connections: Logical form of argument.
ct13-Fallacies-1.md&2.4&
ct13-Fallacies-1.md&2.4&. . . 
ct13-Fallacies-1.md&2.4&
ct13-Fallacies-1.md&2.4&In a fallacy, the reasons do not support the conclusion, because:
ct13-Fallacies-1.md&2.4&
ct13-Fallacies-1.md&2.4&1. the evidence is missing, or 
ct13-Fallacies-1.md&2.4&2. the logical connection between reasons and conclusion is wrong.
ct13-Fallacies-1.md&2.4&
ct13-Fallacies-1.md&2.5&## A: What do you think of this?
ct13-Fallacies-1.md&2.5&
ct13-Fallacies-1.md&2.5&"If you buy me this golden ring, you are nice to me. If you are nice, people like you. So if you buy me this golden ring, people will like you."
ct13-Fallacies-1.md&2.5&
ct13-Fallacies-1.md&2.6&## A: Wrong hypothetical syllogism
ct13-Fallacies-1.md&2.6&
ct13-Fallacies-1.md&2.6&A valid hypothetical syllogism would be like this:
ct13-Fallacies-1.md&2.6&
ct13-Fallacies-1.md&2.6&if A then B  
ct13-Fallacies-1.md&2.6&if B then C  
ct13-Fallacies-1.md&2.6& --------------  
ct13-Fallacies-1.md&2.6&if A then C
ct13-Fallacies-1.md&2.6&
ct13-Fallacies-1.md&2.6&
ct13-Fallacies-1.md&2.7&## A: Wrong hypothetical syllogism
ct13-Fallacies-1.md&2.7&
ct13-Fallacies-1.md&2.7&Sometimes, B is replaced in the second premise by another proposition, which looks similar, but is really different: 
ct13-Fallacies-1.md&2.7&
ct13-Fallacies-1.md&2.7&if A then B1  
ct13-Fallacies-1.md&2.7&if B2 then C  
ct13-Fallacies-1.md&2.7& --------------  
ct13-Fallacies-1.md&2.7&Wrong: if A then C
ct13-Fallacies-1.md&2.7&
ct13-Fallacies-1.md&2.7&This is an invalid argument form!
ct13-Fallacies-1.md&2.7&
ct13-Fallacies-1.md&2.8&## A: Wrong hypothetical syllogism
ct13-Fallacies-1.md&2.8&
ct13-Fallacies-1.md&2.8&If you buy me this golden ring, you are nice (to me). If you are nice (to them!), people like you. (Wrong:) So if you buy me this golden ring, people will like you.
ct13-Fallacies-1.md&2.8&
ct13-Fallacies-1.md&2.9&## A: Equivocation
ct13-Fallacies-1.md&2.9&
ct13-Fallacies-1.md&2.9&When two words look the same but mean different things (“being nice” in our example)
ct13-Fallacies-1.md&2.9&we call this “equivocation.”
ct13-Fallacies-1.md&2.9&
ct13-Fallacies-1.md&2.9&Equivocation can lead to a fallacy by confusing the connecting terms of an argument, like we saw above.
ct13-Fallacies-1.md&2.9&
ct13-Fallacies-1.md&2.10&## A: Try to describe the problem!
ct13-Fallacies-1.md&2.10&
ct13-Fallacies-1.md&2.10&Elena, the salesperson in an audio equipment shop, talks to a customer:
ct13-Fallacies-1.md&2.10&
ct13-Fallacies-1.md&2.10&“I notice you've been listening mainly to those bipolar planar speakers. If you want to upgrade to them, you'll need high current, lots of power with pure class A biasing or maybe AB, and a quick slew rate. You can't get that in a receiver. Besides, you might want to biamp later on, which is a lot easier with separates.”
ct13-Fallacies-1.md&2.10&
ct13-Fallacies-1.md&2.11&## A: Obscurity
ct13-Fallacies-1.md&2.11&
ct13-Fallacies-1.md&2.11&>- “Obscurity” means to say something in such a way that the audience cannot understand what is being said.
ct13-Fallacies-1.md&2.11&>- In this case, the customer might be impressed because Elena seems to know so many strange words. But he is not convinced to buy the particular speakers because of reasons and an argument he understands.
ct13-Fallacies-1.md&2.11&>- An obscure argument does not provide useful evidence that the conclusion is correct, and is therefore a bad argument.
ct13-Fallacies-1.md&2.11&>- Obscurity is somewhat relative to the audience. An explanation in French would be obscure to me, but not to someone who speaks French. The same applies to technical terms, a doctor’s medical explanations and so on.
ct13-Fallacies-1.md&2.11&
ct13-Fallacies-1.md&2.11&
ct13-Fallacies-1.md&2.12&## A: Identify the problem!
ct13-Fallacies-1.md&2.12&
ct13-Fallacies-1.md&2.12&If you're going to get a job in a competitive field, you've got to be good. If someone's good, that means they're honest, caring and loyal. Since I'm honest, caring and loyal, I'll easily get a job, no matter how competitive the field is.
ct13-Fallacies-1.md&2.12&
ct13-Fallacies-1.md&2.13&## A: Equivocation “good”
ct13-Fallacies-1.md&2.13&
ct13-Fallacies-1.md&2.13&>- Here, “good” in the first part of the argument means “good at your studies.”
ct13-Fallacies-1.md&2.13&>- “Good” in the second premise is used in the sense of “good as a friend” which is a different thing.
ct13-Fallacies-1.md&2.13&>- Therefore, this is a fallacy of equivocation. The two words look the same, but are used in a different sense!
ct13-Fallacies-1.md&2.13&>- The conclusion does not follow from the premises.
ct13-Fallacies-1.md&2.13&
ct13-Fallacies-1.md&2.14&## A: Fallacies
ct13-Fallacies-1.md&2.14&
ct13-Fallacies-1.md&2.14&“We have to stop the increase in MTR prices! Now they charge you 4 dollars, next year they will want 10, and the year after we will have to pay 100 or 1000 dollars for an MTR ride!”
ct13-Fallacies-1.md&2.14&
ct13-Fallacies-1.md&2.15&## A: Slippery Slope
ct13-Fallacies-1.md&2.15&
ct13-Fallacies-1.md&2.15&>- The Slippery Slope is a fallacy in which a person asserts that some event must inevitably follow from another without any argument for the inevitability of the sequence of events.
ct13-Fallacies-1.md&2.15&>- In our example, the fact that MTR ticket prices are increased, does not mean that there will be an increase to 100 or 1000 dollars any time soon.
ct13-Fallacies-1.md&2.15&
ct13-Fallacies-1.md&2.16&## A: Another example:
ct13-Fallacies-1.md&2.16&
ct13-Fallacies-1.md&2.16&“We have to stop the children from watching TV every day. Now they watch only for half hour every day, but next week it will be one hour, then two, and soon they will sit all day in front of the TV!”
ct13-Fallacies-1.md&2.16&
ct13-Fallacies-1.md&2.17&## A: Fallacy?
ct13-Fallacies-1.md&2.17&
ct13-Fallacies-1.md&2.17&"You should not begin to take drugs at all. In the beginning you will only take a small amount, but next time you will need a higher dose, and next time still more, and soon you will be unable to control your addiction and your whole life will be centered on drugs."
ct13-Fallacies-1.md&2.17&
ct13-Fallacies-1.md&2.17&Is this a slippery slope fallacy?
ct13-Fallacies-1.md&2.17&
ct13-Fallacies-1.md&2.18&## A: Fallacy?
ct13-Fallacies-1.md&2.18&
ct13-Fallacies-1.md&2.18&Not a fallacy. Some slippery slopes are real.
ct13-Fallacies-1.md&2.18&
ct13-Fallacies-1.md&2.18&In the case of drugs (or cigarettes) the biochemical and psychological mechanisms of addiction make it probable that the user will indeed need higher and higher doses of the drug in the future.
ct13-Fallacies-1.md&2.18&
ct13-Fallacies-1.md&2.18&
ct13-Fallacies-1.md&2.19&## A: What do you think of this?
ct13-Fallacies-1.md&2.19&
ct13-Fallacies-1.md&2.19&- Teacher: “It is important that you study hard, in order to get a good grade.”
ct13-Fallacies-1.md&2.19&- Student: “Of course you would say that, you're a teacher. Therefore, I don't believe it.”
ct13-Fallacies-1.md&2.19&
ct13-Fallacies-1.md&2.20&## A: Ad hominem
ct13-Fallacies-1.md&2.20&
ct13-Fallacies-1.md&2.20&“Ad hominem” (an attack “at the person”) is a fallacy, in which a claim or argument is rejected on the basis of some irrelevant fact about the author of or the person presenting the claim or argument. 
ct13-Fallacies-1.md&2.20&
ct13-Fallacies-1.md&2.21&## A: Ad hominem
ct13-Fallacies-1.md&2.21&
ct13-Fallacies-1.md&2.21&- Teacher: “It is important that you study hard, in order to get a good grade.”
ct13-Fallacies-1.md&2.21&- Student: “Of course you would say that, you're a teacher. Therefore, I don't believe it.”
ct13-Fallacies-1.md&2.21&
ct13-Fallacies-1.md&2.21&. . . 
ct13-Fallacies-1.md&2.21&
ct13-Fallacies-1.md&2.21&>- The fact that a teacher says it, does not make it wrong!
ct13-Fallacies-1.md&2.21&>- Very often, it doesn't matter who says something. It can still be true or false.
ct13-Fallacies-1.md&2.21&
ct13-Fallacies-1.md&2.22&## A: Now do it yourself!
ct13-Fallacies-1.md&2.22&
ct13-Fallacies-1.md&2.22&Make up an argument which contains an ad hominem fallacy! 
ct13-Fallacies-1.md&2.22&
ct13-Fallacies-1.md&2.23&## A: Fallacy?
ct13-Fallacies-1.md&2.23&
ct13-Fallacies-1.md&2.23&A poor man on the street wants to sell you a paper in which he has written down his advice about a revolutionary new way to get rich that does not require any investment in advance, is available to all, and always works.
ct13-Fallacies-1.md&2.23&
ct13-Fallacies-1.md&2.23&You think: “Why should I buy this? If this man really had a secret way of getting rich that worked, he would use it himself first, and he wouldn't be poor. So I don’t believe whatever is in that paper!”
ct13-Fallacies-1.md&2.23&
ct13-Fallacies-1.md&2.23&Correct or fallacy?
ct13-Fallacies-1.md&2.23&
ct13-Fallacies-1.md&2.24&## A: Correct or fallacy? (1)
ct13-Fallacies-1.md&2.24&
ct13-Fallacies-1.md&2.24&Unclear.
ct13-Fallacies-1.md&2.24&
ct13-Fallacies-1.md&2.24&>- Opinion 1: This is not an ad hominem fallacy, because the fact that this person is poor is *not* irrelevant to the question.
ct13-Fallacies-1.md&2.24&>- The conclusion that I don’t believe what that man says is supported by the observation that he is himself poor. (It is not necessarily true, but it has some support.)
ct13-Fallacies-1.md&2.24&>- An ad hominem fallacy is an attack that is based on an *irrelevant* fact about the person presenting an argument. For example, if I rejected his advice because he has blond hair, then this would be a fallacy.
ct13-Fallacies-1.md&2.24&
ct13-Fallacies-1.md&2.25&## A: Correct or fallacy? (2)
ct13-Fallacies-1.md&2.25&
ct13-Fallacies-1.md&2.25&>- Opinion 2: This is a fallacy. The man could have decided to be poor for other reasons, and this fact does not affect the possibility that his advice is good. One would have to judge the advice itself, not the man’s appearance.
ct13-Fallacies-1.md&2.25&
ct13-Fallacies-1.md&2.25&>- But it seems that generally people don't *decide* to be poor. We can assume that usually someone who has the means to be rich would not decide to be poor. Therefore, there is a high probability that the advice is indeed useless.
ct13-Fallacies-1.md&2.25&
ct13-Fallacies-1.md&2.26&## A: Fallacies
ct13-Fallacies-1.md&2.26&
ct13-Fallacies-1.md&2.26&"If you don't vote for our party, we will not be able to protect you. All sorts of criminals will take control of the streets, and you will not be able to leave your house any more and feel safe. Your old parents will suffer as well as your children!"
ct13-Fallacies-1.md&2.26&
ct13-Fallacies-1.md&2.27&## A: Appeal to Fear
ct13-Fallacies-1.md&2.27&
ct13-Fallacies-1.md&2.27&>- Evidence in the argument is replaced with fear.
ct13-Fallacies-1.md&2.27&>- In our example, fear of criminals does not support the claim that I should vote for that party.
ct13-Fallacies-1.md&2.27&>- For example, they too might be unable to protect me.
ct13-Fallacies-1.md&2.27&>- Or some other party might do it even better than they claim.
ct13-Fallacies-1.md&2.27&>- Or it might be that crime is not a problem at all for any number of other reasons.
ct13-Fallacies-1.md&2.27&
ct13-Fallacies-1.md&2.27&
ct13-Fallacies-1.md&2.28&## A: Fallacy?
ct13-Fallacies-1.md&2.28&
ct13-Fallacies-1.md&2.28&1. Your doctor tells you that if you don’t take this pill, your stomach pain will get worse.
ct13-Fallacies-1.md&2.28&2. A robber with a gun threatens to shoot you if you don’t give him your money.
ct13-Fallacies-1.md&2.28&
ct13-Fallacies-1.md&2.29&## A: Fallacy?
ct13-Fallacies-1.md&2.29&
ct13-Fallacies-1.md&2.29&Sometimes we are threatened in a way that constitutes a *conclusive* argument. Then the appeal to fear would *not* be a fallacy!
ct13-Fallacies-1.md&2.29&
ct13-Fallacies-1.md&2.29&1. Your doctor tells you that if you don’t take this pill, your stomach pain will get worse.
ct13-Fallacies-1.md&2.29&2. A robber with a gun threatens to shoot you if you don’t give him your money.
ct13-Fallacies-1.md&2.29&
ct13-Fallacies-1.md&2.29&These are valid threats that do support a conclusion, not fallacies!
ct13-Fallacies-1.md&2.29&
ct13-Fallacies-1.md&2.30&## A: What is a fallacy?
ct13-Fallacies-1.md&2.30&
ct13-Fallacies-1.md&2.30&>- A fallacy is an argument that is wrong (the premises do *not* support the conclusion), either because its *structure* is wrong, or because it replaces *evidence* in an argument by something else.
ct13-Fallacies-1.md&2.30&>- What this “something else” is, determines the type of the fallacy:
ct13-Fallacies-1.md&2.30&>      - If I replace evidence by fear, I have an appeal to fear.
ct13-Fallacies-1.md&2.30&>      - If I replace it by an implausible prediction of how things will go worse and worse in the future, I have a slippery slope fallacy, and so on.
ct13-Fallacies-1.md&2.30&
ct13-Fallacies-1.md&2.30&
ct13-Fallacies-1.md&2.31&## A: Fallacies
ct13-Fallacies-1.md&2.31&
ct13-Fallacies-1.md&2.31&- Interviewer: “Your CV looks impressive but I need another reference.”
ct13-Fallacies-1.md&2.31&- Bill: “Jill can give me a good reference.”
ct13-Fallacies-1.md&2.31&- Interviewer: “Good. But how do I know that Jill is trustworthy?”
ct13-Fallacies-1.md&2.31&- Bill: “Oh, she certainly is. You can trust me on this.”
ct13-Fallacies-1.md&2.31&
ct13-Fallacies-1.md&2.32&## A: Begging the Question
ct13-Fallacies-1.md&2.32&
ct13-Fallacies-1.md&2.32&>- Begging the Question is a fallacy in which the premises include the claim that the conclusion is true or (directly or indirectly) assume that the conclusion is true. So the reasoning is circular.
ct13-Fallacies-1.md&2.32&>- (We talked about this before: Mary told me I’m her best friend, and this must be true, because Mary would not lie to her best friend.”)
ct13-Fallacies-1.md&2.32&>- In our example, the interviewer will have to trust Jill's reference on Bill, because Bill himself says that it is trustworthy. So Jill's reference is the reason to trust Bill, and Bill's word is the reason to trust Jill's reference.
ct13-Fallacies-1.md&2.32&
ct13-Fallacies-1.md&2.33&## A: Begging the question
ct13-Fallacies-1.md&2.33&
ct13-Fallacies-1.md&2.33&Another example:
ct13-Fallacies-1.md&2.33&
ct13-Fallacies-1.md&2.33&- On the street, you get arrested by someone who claims to be a police officer, but he is not wearing a uniform.
ct13-Fallacies-1.md&2.33&- You: “How do I know you are a police officer?”
ct13-Fallacies-1.md&2.33&- The man: “Of course I am! How could I have arrested you if I were not?”
ct13-Fallacies-1.md&2.33&
ct13-Fallacies-1.md&2.34&## A: Fallacies
ct13-Fallacies-1.md&2.34&
ct13-Fallacies-1.md&2.34&Bill works at a newspaper. He is assigned by his editor to determine what most Hong Kong people think about a new law that will place a special tax on all computers. The money will be used to build new housing for the poor. Bill decides to use an email poll, sending emails to one million Hong Kong citizens. In his poll, 95% of 900,000 replies opposed the tax. Bill concludes that almost everybody is against this tax.
ct13-Fallacies-1.md&2.34&
ct13-Fallacies-1.md&2.35&## A: What the problem is not
ct13-Fallacies-1.md&2.35&
ct13-Fallacies-1.md&2.35&The sample size (900,000) is actually huge, and more than most surveys handle. You can have a good and valid survey with only 500 or 1000 people, so the sample size is not the problem here.
ct13-Fallacies-1.md&2.35&
ct13-Fallacies-1.md&2.35&. . .
ct13-Fallacies-1.md&2.35&
ct13-Fallacies-1.md&2.35&The conclusion from 95% to “almost everybody” is also not the problem. 95% of a class of 20 people would be all but one person. This is “almost everybody”!
ct13-Fallacies-1.md&2.35&
ct13-Fallacies-1.md&2.36&## A: Biased Sample
ct13-Fallacies-1.md&2.36&
ct13-Fallacies-1.md&2.36&This fallacy is committed when a person draws a conclusion about a population based on a sample that is biased or prejudiced in some way. 
ct13-Fallacies-1.md&2.36&
ct13-Fallacies-1.md&2.37&## A: Biased Sample
ct13-Fallacies-1.md&2.37&
ct13-Fallacies-1.md&2.37&In our example, Bill used an email survey, thus asking exclusively for the opinion of computer users, and these are the people who will have to pay the tax. 
ct13-Fallacies-1.md&2.37&
ct13-Fallacies-1.md&2.37&The poor, who typically don't participate in email surveys, could not express their opinions in this survey, and these are the people who will likely profit from the tax.
ct13-Fallacies-1.md&2.37&
ct13-Fallacies-1.md&2.37&So by choosing email as a medium, Bill is already determining the outcome of the survey!
ct13-Fallacies-1.md&2.37&
ct13-Fallacies-1.md&2.38&## A: Now do it yourself!
ct13-Fallacies-1.md&2.38&
ct13-Fallacies-1.md&2.38&Make up an argument which contains a biased sample! 
ct13-Fallacies-1.md&2.38&
ct13-Fallacies-1.md&2.39&## A: Biased sample
ct13-Fallacies-1.md&2.39&
ct13-Fallacies-1.md&2.39&Example: I go to McDonalds and ask the people who sit there whether they like to eat at McDonalds.
ct13-Fallacies-1.md&2.39&
ct13-Fallacies-1.md&2.39&(If they didn’t like it, they wouldn’t be sitting there!)
ct13-Fallacies-1.md&2.39&
ct13-Fallacies-1.md&2.39&Or I ask people in the MTR whether they like to take the MTR. (Those who really dislike it will not be there, because they’re taking other means of transport).
ct13-Fallacies-1.md&2.39&
ct13-Fallacies-1.md&2.40&## A: Fallacy?
ct13-Fallacies-1.md&2.40&
ct13-Fallacies-1.md&2.40&If I want to know whether students like Critical Thinking, I have to go to a Critical Thinking class and ask them. Does this mean that I am committing a fallacy?
ct13-Fallacies-1.md&2.40&
ct13-Fallacies-1.md&2.40&. . . 
ct13-Fallacies-1.md&2.40&
ct13-Fallacies-1.md&2.40&>- It depends.
ct13-Fallacies-1.md&2.40&>- If class attendance is mandatory, then the sample of the students present will be representative, because it will include both the students who like the class and those who don’t like it.
ct13-Fallacies-1.md&2.40&>- If, on the other hand, the students can choose freely to attend or not, then the sample would be biased, because the students who attend the class under these circumstances are likely to like it.
ct13-Fallacies-1.md&2.40&
ct13-Fallacies-1.md&2.41&## A: Fallacy?
ct13-Fallacies-1.md&2.41&
ct13-Fallacies-1.md&2.41&Similarly, if an island is connected with the mainland by only one ferry, and there is no bridge or other way to leave the island, then doing a satisfaction survey about the ferry service on the ferry would not necessarily be a fallacy. Because all people who live on the island and wish to leave it would have to take the ferry, and that would include those who don’t like the ferry service. So the sample would be representative in this respect.
ct13-Fallacies-1.md&2.41&
ct13-Fallacies-1.md&2.41&
ct13-Fallacies-1.md&2.42&## A: Fallacies
ct13-Fallacies-1.md&2.42&
ct13-Fallacies-1.md&2.42&“We need nuclear power, else we have to burn coal for energy, the CO~2~ in the atmosphere will keep rising, and we will have an even more serious global warming.”
ct13-Fallacies-1.md&2.42&
ct13-Fallacies-1.md&2.43&## A: Dilemma
ct13-Fallacies-1.md&2.43&
ct13-Fallacies-1.md&2.43&>- *A dilemma is a forced choice between two undesirable outcomes.*
ct13-Fallacies-1.md&2.43&>- A false dilemma is a fallacy in which an alternative is presented and it is stated that either one or the other of two things have to be chosen. Often this is not true, because we have more options to chose from.
ct13-Fallacies-1.md&2.43&>- In our example, there is not only the alternative between nuclear power and global warming. One could, for example, use solar power, or wind power, or try to reduce power consumption.
ct13-Fallacies-1.md&2.43&
ct13-Fallacies-1.md&2.44&## A: Real and false dilemma
ct13-Fallacies-1.md&2.44&
ct13-Fallacies-1.md&2.44&>- Real dilemma: If I go out, I will get into the rain. If I stay at home, I will miss that party. I will either go out or stay at home. Therefore, I will get into the rain or I will miss that party.
ct13-Fallacies-1.md&2.44&>- False dilemma: If I marry Paul, I will be poor. If I marry Bill, I will be bored. I have to marry Paul or Bill. Therefore, I will be poor or bored.
ct13-Fallacies-1.md&2.44&>- This is false, because I could *not* marry at all; or I could marry someone else entirely.
ct13-Fallacies-1.md&2.44&
ct13-Fallacies-1.md&2.45&## A: Now try it yourself!
ct13-Fallacies-1.md&2.45&
ct13-Fallacies-1.md&2.45&Make up a dilemma, and then explain whether it is a real dilemma or a false one!
ct13-Fallacies-1.md&2.45&
ct13-Fallacies-1.md&2.46&## A: Fallacies
ct13-Fallacies-1.md&2.46&
ct13-Fallacies-1.md&2.46&In sports, I had been doing pretty poorly this season. Then my girlfriend gave me these new laces for my running shoes and I won my next three races. Those laces must be good luck...if I keep on wearing them I will always win!
ct13-Fallacies-1.md&2.46&
ct13-Fallacies-1.md&2.47&## A: Correlation/causation
ct13-Fallacies-1.md&2.47&
ct13-Fallacies-1.md&2.47&>- This fallacy is committed when it is concluded that one event causes another simply because the two events take place together or shortly after each other.
ct13-Fallacies-1.md&2.47&>- The speaker is confusing correlation (two phenomena have some statistical connection to each other) and causation (one phenomenon is the cause of the other).
ct13-Fallacies-1.md&2.47&
ct13-Fallacies-1.md&2.48&## A: Correlation/causation
ct13-Fallacies-1.md&2.48&
ct13-Fallacies-1.md&2.48&The mere fact that A occurs before B does not indicate a causal relationship!
ct13-Fallacies-1.md&2.48&
ct13-Fallacies-1.md&2.48&For example, suppose Jill, who is in London, sneezed at the exact same time an earthquake started in California. Even if this happens multiple times, it would still be irrational to arrest Jill for starting a natural disaster, since there is no reason to suspect any causal connection between the two events.
ct13-Fallacies-1.md&2.48&
ct13-Fallacies-1.md&2.49&## A: Correlation/causation
ct13-Fallacies-1.md&2.49&
ct13-Fallacies-1.md&2.49&>- In our example, we don't need to assume that there is a causal relationship between the new laces and winning the race.
ct13-Fallacies-1.md&2.49&>- However, there might be a psychological cause: sometimes a strong belief might itself cause someone to succeed or fail in an enterprise.
ct13-Fallacies-1.md&2.49&
ct13-Fallacies-1.md&2.50&## A: Another example:
ct13-Fallacies-1.md&2.50&
ct13-Fallacies-1.md&2.50&>- There is a correlation between small shoe size and the length of one person’s hair. Can we conclude that small shoes cause long hair?
ct13-Fallacies-1.md&2.50&>- No. Women are more likely to have long hair than men, and women also have smaller shoes than men. But the two things, although they are statistically correlated, are not causally connected!
ct13-Fallacies-1.md&2.50&
ct13-Fallacies-1.md&2.51&## A: Common causes
ct13-Fallacies-1.md&2.51&
ct13-Fallacies-1.md&2.51&>- Sometimes correlations between two events can be better explained with *common causes* rather than direct causation between the two events.
ct13-Fallacies-1.md&2.51&>- If event A always occurs before event B, it is possible that:
ct13-Fallacies-1.md&2.51&>      - A causes B; or
ct13-Fallacies-1.md&2.51&>      - Both A and B are caused by another event C (in the case above, being a woman is the common cause for both having long hair and smaller shoes).
ct13-Fallacies-1.md&2.51&
ct13-Fallacies-1.md&2.52&## A: Other examples of common causes
ct13-Fallacies-1.md&2.52&
ct13-Fallacies-1.md&2.52&>- People who have expensive cars are more likely to have expensive watches. Not because the cars *cause* the watches, but because there is a common cause (having money and the need to display it).
ct13-Fallacies-1.md&2.52&>- More educated people live longer and healthier lives. (Not because education directly causes longer life, but because education allows people to understand how to live more healthily. It also normally leads to a higher income, which can then be used to make healthy lifestyle choices. Education is the common cause of both better understanding and higher income, and these two lead to longer and healthier lives.)
ct13-Fallacies-1.md&2.52&
ct13-Fallacies-1.md&2.53&## A: What do you think of this?
ct13-Fallacies-1.md&2.53&
ct13-Fallacies-1.md&2.53&“It would be terrible if the environmental destruction was so bad that all human life on Earth would be extinguished within the next two hundred years! I cannot imagine this happening! So there will surely be some way out. Perhaps the scientists will find some cure for the environmental problems.”
ct13-Fallacies-1.md&2.53&
ct13-Fallacies-1.md&2.54&## A: Wishful thinking
ct13-Fallacies-1.md&2.54&
ct13-Fallacies-1.md&2.54&Wishful thinking is a fallacy in which one believes X to be true because one does not want to believe not-X.
ct13-Fallacies-1.md&2.54&
ct13-Fallacies-1.md&2.54&Of course, the truth of X or not-X does not depend on whether one wants to believe one of the two!
ct13-Fallacies-1.md&2.54&
ct13-Fallacies-1.md&2.55&## A: Wishful thinking
ct13-Fallacies-1.md&2.55&
ct13-Fallacies-1.md&2.55&Wishful thinking is very common:
ct13-Fallacies-1.md&2.55&
ct13-Fallacies-1.md&2.55&>- In human relationships (“I cannot believe that my girlfriend doesn’t like me any more!”)
ct13-Fallacies-1.md&2.55&>- In international relations (“surely the government of country X will not turn into a dictatorship!”)
ct13-Fallacies-1.md&2.55&>- In everyday life (“I cannot imagine that it will be raining *again* on my birthday. So let’s plan to go hiking. The weather will be fine...”)
ct13-Fallacies-1.md&2.55&
ct13-Fallacies-1.md&2.55&
ct13-Fallacies-1.md&2.56&## Reference
ct13-Fallacies-1.md&2.56&
ct13-Fallacies-1.md&2.56&All fallacies and many examples are from:
ct13-Fallacies-1.md&2.56&
ct13-Fallacies-1.md&2.56&The Nizkor Project,
ct13-Fallacies-1.md&2.56&http://www.nizkor.org/features/fallacies
ct13-Fallacies-1.md&2.56&
ct13-Fallacies-1.md&2.56&Originally by Dr. Michael C. Labossiere, ontologist@aol.com
ct13-Fallacies-1.md&2.56&
ct13-Fallacies-1.md&2.57&## Any questions?
ct13-Fallacies-1.md&2.57&
ct13-Fallacies-1.md&2.57&![](graphics/questions.jpg)\ 
ct13-Fallacies-1.md&2.57&
ct13-Fallacies-1.md&2.58&## Thank you for your attention!
ct13-Fallacies-1.md&2.58&
ct13-Fallacies-1.md&2.58&Email: <matthias@ln.edu.hk>
ct13-Fallacies-1.md&2.58&
ct13-Fallacies-1.md&2.58&If you have questions, please come to my office hours (see course outline).
ct13-Fallacies-1.md&2.58&
ct14-Fallacies-2.md&0.1&% Critical Thinking
ct14-Fallacies-2.md&0.1&% 14. Fallacies (2)
ct14-Fallacies-2.md&0.1&% A. Matthias
ct14-Fallacies-2.md&0.1&
ct14-Fallacies-2.md&0.1&
ct14-Fallacies-2.md&1.0&# Where we are
ct14-Fallacies-2.md&1.0&
ct14-Fallacies-2.md&1.1&## What did we learn last time?
ct14-Fallacies-2.md&1.1&
ct14-Fallacies-2.md&1.1&- Fallacies group A (core and very important)
ct14-Fallacies-2.md&1.1&
ct14-Fallacies-2.md&1.2&## What are we going to learn today?
ct14-Fallacies-2.md&1.2&
ct14-Fallacies-2.md&1.2&- More fallacies, groups B and C.
ct14-Fallacies-2.md&1.2&
ct14-Fallacies-2.md&1.3&## Reminder: Core and extended fallacies
ct14-Fallacies-2.md&1.3&
ct14-Fallacies-2.md&1.3&>- Since different teachers teach this course, and not all focus on fallacies, we have two categories of fallacies:
ct14-Fallacies-2.md&1.3&>     - ‘Core’ fallacies are those taught in all sections of this course. They are tested in the common part of the final exam.
ct14-Fallacies-2.md&1.3&>     - ‘Extended’ are all the other fallacies. I may ask you about them in my own part of the exam.
ct14-Fallacies-2.md&1.3&>- Core fallacies are: Hasty generalisation, begging the question, equivocation, denying the antecedent, affirming the consequent and wishful thinking.
ct14-Fallacies-2.md&1.3&
ct14-Fallacies-2.md&1.4&## Reminder: A, B and C fallacies
ct14-Fallacies-2.md&1.4&
ct14-Fallacies-2.md&1.4&>- The other fallacies in this presentation are specific to my class. You still have to learn them, but you will find that other sections might talk about other topics instead.
ct14-Fallacies-2.md&1.4&>- Because there are so many, I have graded the following pages into A (very important and core), B (important) and C (not that important).
ct14-Fallacies-2.md&1.4&>- Essentially, the letter is the reverse of the grade you want to get with fallacy questions: For an A, you’ll need A, B and C fallacies. For a B, you will need A and B fallacies. For a C, you will need only the A fallacies.
ct14-Fallacies-2.md&1.4&>- You could also see the letters as indicators of the probability that a question about a fallacy will be in an exam: A fallacies will very likely be asked in an exam; B fallacies are likely; and C fallacies are less likely (but not impossible!)
ct14-Fallacies-2.md&1.4&
ct14-Fallacies-2.md&2.0&# Fallacies Group B
ct14-Fallacies-2.md&2.0&
ct14-Fallacies-2.md&2.1&## B: Fallacies
ct14-Fallacies-2.md&2.1&
ct14-Fallacies-2.md&2.1&- Bill: "I think that plastic bags harm the environment."
ct14-Fallacies-2.md&2.1&- Jane: "I disagree completely. Dr. Jones says that plastic bags are harmless. He has to be right, after all, he is a respected expert in his field."
ct14-Fallacies-2.md&2.1&- Bill: "I've never heard of Dr. Jones. Who is he?"
ct14-Fallacies-2.md&2.1&- Jane: "He's the guy who won the Nobel Prize for medicine, for his work on hair loss. He's a world famous expert, so I believe him." 
ct14-Fallacies-2.md&2.1&
ct14-Fallacies-2.md&2.2&## B: Appeal to authority
ct14-Fallacies-2.md&2.2&
ct14-Fallacies-2.md&2.2&>- An unjustified “appeal to authority” is committed when the person in question is not a legitimate authority on the subject.
ct14-Fallacies-2.md&2.2&>- In our example, Dr. Jones is an expert on hair loss, not on plastic bags.
ct14-Fallacies-2.md&2.2&>- Since he has no expertise on the subject, we cannot appeal to his authority.
ct14-Fallacies-2.md&2.2&>- An appeal to authority is not a fallacy, however, if the person really *does* have relevant expertise.
ct14-Fallacies-2.md&2.2&
ct14-Fallacies-2.md&2.3&## B: Fallacy?
ct14-Fallacies-2.md&2.3&
ct14-Fallacies-2.md&2.3&1. An actor on TV tells you that you should use a particular shampoo in order to stop hair loss.
ct14-Fallacies-2.md&2.3&2. Your history teacher tells you that the Greek revolution against the Turkish occupation of Greece happened in 1821.
ct14-Fallacies-2.md&2.3&
ct14-Fallacies-2.md&2.3&. . . 
ct14-Fallacies-2.md&2.3&
ct14-Fallacies-2.md&2.3&Fallacy?
ct14-Fallacies-2.md&2.3&
ct14-Fallacies-2.md&2.3&1. Yes. An actor is not an expert on hair loss.
ct14-Fallacies-2.md&2.3&2. No. A history teacher is supposed to be an expert on history. So you should believe what he says about history.
ct14-Fallacies-2.md&2.3&
ct14-Fallacies-2.md&2.4&## B: Fallacies
ct14-Fallacies-2.md&2.4&
ct14-Fallacies-2.md&2.4&Salad must be healthy, since so many people believe it to be healthy.
ct14-Fallacies-2.md&2.4&
ct14-Fallacies-2.md&2.5&## B: Appeal to belief
ct14-Fallacies-2.md&2.5&
ct14-Fallacies-2.md&2.5&>- “Something must be true because many people believe it.”
ct14-Fallacies-2.md&2.5&>- This is is, of course, a fallacy.
ct14-Fallacies-2.md&2.5&>- In old times, almost all people believed that the Earth was flat. This did not make this belief true.
ct14-Fallacies-2.md&2.5&
ct14-Fallacies-2.md&2.6&## B: Appeal to belief
ct14-Fallacies-2.md&2.6&
ct14-Fallacies-2.md&2.6&>- In our example, the fact that many people believe salad to be healthy, does not mean that it actually is.
ct14-Fallacies-2.md&2.6&>- (It might be for other reasons, but not *because* people believe it!)
ct14-Fallacies-2.md&2.6&>- This is an interesting example of a fallacy where both the premise and the conclusion are actually true, but the argument is still a fallacy (because the premise does not support the conclusion!)
ct14-Fallacies-2.md&2.6&
ct14-Fallacies-2.md&2.7&## B: Fallacies?
ct14-Fallacies-2.md&2.7&
ct14-Fallacies-2.md&2.7&1. Bill is the best student in class, because everyone believes that this is the case.
ct14-Fallacies-2.md&2.7&2. Bill is the most handsome boy in class, because everyone believes that this is the case.
ct14-Fallacies-2.md&2.7&
ct14-Fallacies-2.md&2.7&Are these fallacies, or is there valid support for the conclusions?
ct14-Fallacies-2.md&2.7&
ct14-Fallacies-2.md&2.8&## B: Fallacies?
ct14-Fallacies-2.md&2.8&
ct14-Fallacies-2.md&2.8&>1. Fallacy. Whether someone is the best student or not does not depend on the belief of others. It can be measured, for example by looking at examination results.
ct14-Fallacies-2.md&2.8&>2. Correct. Handsomeness is defined only by the belief of others. There is no objective measure for beauty, or for how attractive or well-dressed someone is. These are entirely matters of opinion.
ct14-Fallacies-2.md&2.8&
ct14-Fallacies-2.md&2.9&## B: Fallacies?
ct14-Fallacies-2.md&2.9&
ct14-Fallacies-2.md&2.9&>- An appeal to belief is *not* a fallacy if the subject is shaped by the belief itself!
ct14-Fallacies-2.md&2.9&>- For example, if most people believe that you shouldn't wear a swimsuit to a wedding, then you should not.
ct14-Fallacies-2.md&2.9&>- The only reason you should not, is exactly that most people believe that you should not.
ct14-Fallacies-2.md&2.9&>- So in this case, there is no truth to the matter that is independent of the beliefs of a majority. The majority’s belief is the only truth.
ct14-Fallacies-2.md&2.9&>- This also applies to table manners, fashion, statements about beauty, and other social phenomena where truth is defined by common belief!
ct14-Fallacies-2.md&2.9&
ct14-Fallacies-2.md&2.9&
ct14-Fallacies-2.md&2.10&## B: Fallacies
ct14-Fallacies-2.md&2.10&
ct14-Fallacies-2.md&2.10&- James: "So, what is this new marketing method?
ct14-Fallacies-2.md&2.10&- Bill: "Well, the latest thing in marketing techniques is the GK method. It was just published last week."
ct14-Fallacies-2.md&2.10&- James: "Well, our old marketing method has been quite effective."
ct14-Fallacies-2.md&2.10&- Bill: "But you know that we have to go ahead with progress. That means new ideas and new techniques have to be used. The GK method is new, so it will do better than that old, dusty method."
ct14-Fallacies-2.md&2.10&
ct14-Fallacies-2.md&2.11&## B: Appeal to Novelty
ct14-Fallacies-2.md&2.11&
ct14-Fallacies-2.md&2.11&>- It is assumed that something is better or correct simply because it is new.
ct14-Fallacies-2.md&2.11&>- This is a fallacy if, in the area we are talking about, a new thing is not necessarily better than an old thing.
ct14-Fallacies-2.md&2.11&>- In our example, no evidence for the efficiency of the new marketing method is presented.
ct14-Fallacies-2.md&2.11&>- Since marketing methods do not necessarily get better if they are newer, it is a fallacy to assume that a new method must necessarily be better than an old, successful one.
ct14-Fallacies-2.md&2.11&
ct14-Fallacies-2.md&2.12&## B: Appeal to Novelty
ct14-Fallacies-2.md&2.12&
ct14-Fallacies-2.md&2.12&Is an appeal to novelty always a fallacy, or are there cases where one could use an appeal to novelty to support a valid argument?
ct14-Fallacies-2.md&2.12&
ct14-Fallacies-2.md&2.13&## B: Appeal to Novelty
ct14-Fallacies-2.md&2.13&
ct14-Fallacies-2.md&2.13&>- An appeal to novelty doesn't need to be always a fallacy.
ct14-Fallacies-2.md&2.13&>- For example, fresh milk is really better than old milk.
ct14-Fallacies-2.md&2.13&>- A new computer is probably better than an old one.
ct14-Fallacies-2.md&2.13&>- In these examples, time is an important factor.
ct14-Fallacies-2.md&2.13&
ct14-Fallacies-2.md&2.14&## B: Fallacies
ct14-Fallacies-2.md&2.14&
ct14-Fallacies-2.md&2.14&"Of course the university teaching system, with lectures and tutorials, is the best. Universities have been teaching in this way for almost one thousand years. So, it has got to be good."
ct14-Fallacies-2.md&2.14&
ct14-Fallacies-2.md&2.15&## B: Appeal to Tradition
ct14-Fallacies-2.md&2.15&
ct14-Fallacies-2.md&2.15&>- “Something is better or correct simply because it is older, traditional, or has always been done that way.”
ct14-Fallacies-2.md&2.15&>- In our example, the fact that the university teaching principles have not changed for a thousand years does not automatically make them good. Perhaps it’s time they finally changed.
ct14-Fallacies-2.md&2.15&
ct14-Fallacies-2.md&2.16&## B: Which are fallacies?
ct14-Fallacies-2.md&2.16&
ct14-Fallacies-2.md&2.16&1. These street shoes are the newest model, so they must be better than the shoes I bought last year!
ct14-Fallacies-2.md&2.16&2. This mobile phone is the newest model, so the touchscreen must be better and it must have a higher resolution than the phone I bought last year!
ct14-Fallacies-2.md&2.16&
ct14-Fallacies-2.md&2.17&## B: Fallacies?
ct14-Fallacies-2.md&2.17&
ct14-Fallacies-2.md&2.17&>1. Yes. Shoes do generally not get “better” each year. There is not much progress in shoe technology from year to year. An exception would be perhaps some hi-tech running shoes, but these are supposed to be “street shoes”.
ct14-Fallacies-2.md&2.17&>2. No. Mobile phone technology does progress fast, so every year phones will have better screens, faster processors and more memory than the year before. So it is an inductive argument to assume that this year’s phone will be better than last year’s. (This conclusion might still be wrong, but the premises do support the conclusion, so it’s not a fallacy! Even a low probability that the conclusion is true makes an argument a weak argument and prevents it from being a fallacy.)
ct14-Fallacies-2.md&2.17&
ct14-Fallacies-2.md&2.18&## B: Fallacies
ct14-Fallacies-2.md&2.18&
ct14-Fallacies-2.md&2.18&What do you think of this?
ct14-Fallacies-2.md&2.18&
ct14-Fallacies-2.md&2.18&- Bill: “I really like this Critical Thinking course. It is somehow interesting, isn't it?”
ct14-Fallacies-2.md&2.18&- James: “Oh no! Help! It is so boring!”
ct14-Fallacies-2.md&2.18&- Ann: “Are you crazy, Bill? How can you like this?”
ct14-Fallacies-2.md&2.18&- Sandy: “I can't stand it another minute!”
ct14-Fallacies-2.md&2.18&- Bill: “Oh well, I was just joking... Of course it is terrible!”
ct14-Fallacies-2.md&2.18&
ct14-Fallacies-2.md&2.19&## B: Bandwagon
ct14-Fallacies-2.md&2.19&
ct14-Fallacies-2.md&2.19&>- In the Bandwagon fallacy, a threat of rejection by a group of people one wants to be accepted by is substituted for evidence.
ct14-Fallacies-2.md&2.19&>- In our case, Bill says that he likes the course, until he perceives that all others have a different opinion. He then says that he just had been joking.
ct14-Fallacies-2.md&2.19&
ct14-Fallacies-2.md&2.20&## B: Now do it yourself!
ct14-Fallacies-2.md&2.20&
ct14-Fallacies-2.md&2.20&Try to imagine a situation which shows a bandwagon fallacy!
ct14-Fallacies-2.md&2.20&
ct14-Fallacies-2.md&2.20&
ct14-Fallacies-2.md&2.21&## B: Bandwagon vs appeal to belief
ct14-Fallacies-2.md&2.21&
ct14-Fallacies-2.md&2.21&The two fallacies are similar, but:
ct14-Fallacies-2.md&2.21&
ct14-Fallacies-2.md&2.21&>- In the bandwagon fallacy, you want to be part of a *particular group,* and you will adjust your opinion to that of that particular group, even if this is not a common opinion (for example, religious beliefs of a minority)
ct14-Fallacies-2.md&2.21&>- In the appeal to belief, you believe something because *most or all* people believe it.
ct14-Fallacies-2.md&2.21&
ct14-Fallacies-2.md&2.21&
ct14-Fallacies-2.md&2.22&## B: Fallacies
ct14-Fallacies-2.md&2.22&
ct14-Fallacies-2.md&2.22&This cake is made from delicious ingredients. Therefore, it must be delicious.
ct14-Fallacies-2.md&2.22&
ct14-Fallacies-2.md&2.23&## B: The fallacy of Composition
ct14-Fallacies-2.md&2.23&
ct14-Fallacies-2.md&2.23&- Individual things have characteristics A, B, C, etc. 
ct14-Fallacies-2.md&2.23&- Therefore, something which is composed of  these things has the same characteristics A, B, C, etc. 
ct14-Fallacies-2.md&2.23&
ct14-Fallacies-2.md&2.23&. . .
ct14-Fallacies-2.md&2.23&
ct14-Fallacies-2.md&2.23&This is a fallacy because the mere fact that the components have certain characteristics does not, in itself, guarantee that the composed thing (taken as a whole) will have the same characteristics.
ct14-Fallacies-2.md&2.23&
ct14-Fallacies-2.md&2.24&## B: The fallacy of Composition
ct14-Fallacies-2.md&2.24&
ct14-Fallacies-2.md&2.24&In our example, it is concluded that the cake must be delicious because the ingredients are.
ct14-Fallacies-2.md&2.24&This is a fallacy, because a cake made up from delicious ingredients can still be disgusting (for example, if it has the wrong proportion of ingredients or if the ingredients just don't fit together)
ct14-Fallacies-2.md&2.24&
ct14-Fallacies-2.md&2.25&## B: Fallacies
ct14-Fallacies-2.md&2.25&
ct14-Fallacies-2.md&2.25&Bill lives in a large building, so his flat must be large.
ct14-Fallacies-2.md&2.25&
ct14-Fallacies-2.md&2.26&## B: The fallacy of Division
ct14-Fallacies-2.md&2.26&
ct14-Fallacies-2.md&2.26&- The whole, X, has properties A, B, C, etc. 
ct14-Fallacies-2.md&2.26&- Therefore each part of X must have the same properties A, B, C, etc.
ct14-Fallacies-2.md&2.26&
ct14-Fallacies-2.md&2.26&In our example, the fact that the building is large, does not mean that the individual flats are.
ct14-Fallacies-2.md&2.26&
ct14-Fallacies-2.md&2.27&## B: Composition and Division
ct14-Fallacies-2.md&2.27&
ct14-Fallacies-2.md&2.27&Now do it yourself! Make up an argument which contains a fallacy of composition or division!
ct14-Fallacies-2.md&2.27&
ct14-Fallacies-2.md&2.28&## B: Is this a fallacy?
ct14-Fallacies-2.md&2.28&
ct14-Fallacies-2.md&2.28&Is this a fallacy?
ct14-Fallacies-2.md&2.28&
ct14-Fallacies-2.md&2.28&- All the ingredients of this dish are salty, therefore this dish must be salty.
ct14-Fallacies-2.md&2.28&
ct14-Fallacies-2.md&2.28&. . . 
ct14-Fallacies-2.md&2.28&
ct14-Fallacies-2.md&2.28&Not a fallacy! If we add up salty ingredients we will get a salty dish!
ct14-Fallacies-2.md&2.28&
ct14-Fallacies-2.md&2.28&Whether composition/division are fallacies or not depend on whether the properties we consider do add up or not!
ct14-Fallacies-2.md&2.28&
ct14-Fallacies-2.md&3.0&# Fallacies Group C
ct14-Fallacies-2.md&3.0&
ct14-Fallacies-2.md&3.1&## C: Fallacies
ct14-Fallacies-2.md&3.1&
ct14-Fallacies-2.md&3.1&“Yeah, I know some people say that cheating at exams is wrong. But we all know that everyone does it, so it’s okay.”
ct14-Fallacies-2.md&3.1&
ct14-Fallacies-2.md&3.2&## C: Appeal to Common Practice
ct14-Fallacies-2.md&3.2&
ct14-Fallacies-2.md&3.2&“Something is right because many people do it.”
ct14-Fallacies-2.md&3.2&
ct14-Fallacies-2.md&3.2&This is a fallacy because the mere fact that most people do something does not make it morally right, or worthy of imitating.
ct14-Fallacies-2.md&3.2&
ct14-Fallacies-2.md&3.3&## C: What is the difference?
ct14-Fallacies-2.md&3.3&
ct14-Fallacies-2.md&3.3&- *Belief* is what everybody thinks.
ct14-Fallacies-2.md&3.3&- *Common practice* is what everybody is doing.
ct14-Fallacies-2.md&3.3&
ct14-Fallacies-2.md&3.3&These two do not need to agree!
ct14-Fallacies-2.md&3.3&
ct14-Fallacies-2.md&3.3&Example:
ct14-Fallacies-2.md&3.3&To use too many plastic bags is common practice, although nobody believes that it is good, or that this should be done.
ct14-Fallacies-2.md&3.3&
ct14-Fallacies-2.md&3.4&## C: Appeal to Popularity
ct14-Fallacies-2.md&3.4&
ct14-Fallacies-2.md&3.4&>- Sometimes the two fallacies are put together under the heading "Appeal to Popularity."
ct14-Fallacies-2.md&3.4&>- What is *popular* can either be a belief (what we called Appeal to Belief), or an actual practice (what we called Appeal to Common Practice). 
ct14-Fallacies-2.md&3.4&>- So an **Appeal to Popularity** covers both cases and is the same as these two fallacies taken together.
ct14-Fallacies-2.md&3.4&
ct14-Fallacies-2.md&3.5&## C: Is this a fallacy?
ct14-Fallacies-2.md&3.5&
ct14-Fallacies-2.md&3.5&“Buy this book! It has sold 5 million copies worldwide!”
ct14-Fallacies-2.md&3.5&
ct14-Fallacies-2.md&3.5&. . . 
ct14-Fallacies-2.md&3.5&
ct14-Fallacies-2.md&3.5&It is hard to decide whether this is a fallacy or not.
ct14-Fallacies-2.md&3.5&
ct14-Fallacies-2.md&3.5&>1. It could be argued to be a fallacy, because the fact that it has sold so many copies does not support the conclusion that I should buy it:
ct14-Fallacies-2.md&3.5&>      - Book tastes of different people are different. Even if 5 million people liked it, it does not follow that I will like it!
ct14-Fallacies-2.md&3.5&>      - People who buy books typically buy them before they read them. So the important number would be how many people actually liked the book after reading it, not how many bought it.
ct14-Fallacies-2.md&3.5&>2. On the other hand, many people have similar tastes. If I know that I generally like best-sellers, then probably I will also like this book, since obviously my taste in books is close to the average. Then this would not be a fallacy (for me).
ct14-Fallacies-2.md&3.5&
ct14-Fallacies-2.md&3.6&## C: Fallacies
ct14-Fallacies-2.md&3.6&
ct14-Fallacies-2.md&3.6&"The new UltraSkinny diet will make you feel great. No longer be troubled by your weight. Enjoy the admiring stares of other people. You will know true happiness if you try our diet!"
ct14-Fallacies-2.md&3.6& 
ct14-Fallacies-2.md&3.7&## C: Appeal to Emotion
ct14-Fallacies-2.md&3.7&
ct14-Fallacies-2.md&3.7&>- This fallacy is committed when someone manipulates peoples’ emotions in order to get them to accept a claim as being true.
ct14-Fallacies-2.md&3.7&>- In this fallacy, one substitutes various means of producing strong emotions in place of evidence for a claim.
ct14-Fallacies-2.md&3.7&>- What is missing in this arguments are two premises (and the facts that would support them):
ct14-Fallacies-2.md&3.7&>      1. The particular diet is effective
ct14-Fallacies-2.md&3.7&>      2. Being skinny would make me happy (not certain, even if the diet actually works!)
ct14-Fallacies-2.md&3.7&>- In our example, feelings are used to cover up the fact that no evidence is given to prove that the diet is effective.
ct14-Fallacies-2.md&3.7&
ct14-Fallacies-2.md&3.8&## C: Emotion and Fear
ct14-Fallacies-2.md&3.8&
ct14-Fallacies-2.md&3.8&>- Of course, fear is also an emotion.
ct14-Fallacies-2.md&3.8&>- But for the purposes of classifying the fallacies, an “appeal to emotion” is always an appeal to a positive emotion; while an “appeal to fear” counts as a separate fallacy.
ct14-Fallacies-2.md&3.8&
ct14-Fallacies-2.md&3.9&## C: Fallacies
ct14-Fallacies-2.md&3.9&
ct14-Fallacies-2.md&3.9&"The previous speaker said that we should ban plastic bags, but this is obviously a joke! How funny!" 
ct14-Fallacies-2.md&3.9&
ct14-Fallacies-2.md&3.10&## C: Appeal to Ridicule
ct14-Fallacies-2.md&3.10&
ct14-Fallacies-2.md&3.10&>- Ridicule (making fun of an argument) or mockery is substituted for evidence.
ct14-Fallacies-2.md&3.10&>- This is a fallacy, because making fun of a claim does not show that it is false.
ct14-Fallacies-2.md&3.10&>- But it can be very effective.
ct14-Fallacies-2.md&3.10&>- In our example, no real arguments are presented concerning the topic of plastic bags.
ct14-Fallacies-2.md&3.10&
ct14-Fallacies-2.md&3.11&## C: Fallacies
ct14-Fallacies-2.md&3.11&
ct14-Fallacies-2.md&3.11&- James: “I think that UFOs exist and are spaceships coming to us from other planets.”
ct14-Fallacies-2.md&3.11&- Erica: “What makes you think so?”
ct14-Fallacies-2.md&3.11&- James: “Well, can you prove that they do not exist?”
ct14-Fallacies-2.md&3.11&
ct14-Fallacies-2.md&3.12&## C: Burden of Proof
ct14-Fallacies-2.md&3.12&
ct14-Fallacies-2.md&3.12&In many situations, one side has the burden of proof resting on it. This side must provide evidence for its position. The claim of the other side, the one that does not bear the burden of proof, is assumed to be true unless proven otherwise. 
ct14-Fallacies-2.md&3.12&
ct14-Fallacies-2.md&3.13&## C: Burden of Proof
ct14-Fallacies-2.md&3.13&
ct14-Fallacies-2.md&3.13&The difficulty in such cases is determining which side has the burden of proof. In some cases the burden of proof is set by the situation. 
ct14-Fallacies-2.md&3.13&
ct14-Fallacies-2.md&3.13&>- In most legal systems, a person is assumed to be innocent until proven guilty. 
ct14-Fallacies-2.md&3.13&>- In debate the burden of proof is placed on the affirmative team. 
ct14-Fallacies-2.md&3.13&>- In most cases the burden of proof rests on those who support a surprising or unusual claim, or one that is not shared by the majority of the group they are in.
ct14-Fallacies-2.md&3.13&
ct14-Fallacies-2.md&3.14&## C: Burden of Proof
ct14-Fallacies-2.md&3.14&
ct14-Fallacies-2.md&3.14&In our example, not Erica has to prove that UFOs don't exist, but James have to give a proof that they do, since his position is the one which is not generally accepted as being true.
ct14-Fallacies-2.md&3.14&
ct14-Fallacies-2.md&3.14&. . . 
ct14-Fallacies-2.md&3.14&
ct14-Fallacies-2.md&3.14&Another example: at a meeting of an environmentalist group, you would not have to prove that global warming is a threat to the planet, because this is generally assumed to be the case in that group. But if you were in a group of automobile industry leaders, you might have to prove that claim.
ct14-Fallacies-2.md&3.14&
ct14-Fallacies-2.md&3.15&## C: Fallacies
ct14-Fallacies-2.md&3.15&
ct14-Fallacies-2.md&3.15&- Jane: “I've been thinking about buying a computer.” 
ct14-Fallacies-2.md&3.15&- Bill: “What sort of computer do you want to buy?” 
ct14-Fallacies-2.md&3.15&- Jane: “I've been thinking about getting a Kiwi Fruit 2200. I read in that consumer magazine that they have been found to be very reliable in six independent industry studies.” 
ct14-Fallacies-2.md&3.15&- Bill: “I wouldn't get the Kiwi Fruit. A friend of mine bought one a month ago to finish his master's thesis. He was halfway through it when smoke started pouring out of the CPU, then his desk started burning, and then even his hair caught fire. He didn't get his thesis done on time and he had to quit studying. Now he's working over at the student cafeteria. He’s they guy without hair over there!”
ct14-Fallacies-2.md&3.15&- Jane: “Oh! Then I should not buy the Kiwi!”
ct14-Fallacies-2.md&3.15&
ct14-Fallacies-2.md&3.16&## C: Misleading Vividness
ct14-Fallacies-2.md&3.16&
ct14-Fallacies-2.md&3.16&>- Misleading Vividness is a fallacy in which a very small number of particularly dramatic events are taken to *outweigh a significant amount of statistical evidence.*
ct14-Fallacies-2.md&3.16&>- This sort of "reasoning" is fallacious because the mere fact that an event is particularly vivid or dramatic does not make the event more likely to occur, especially in the face of significant statistical evidence.
ct14-Fallacies-2.md&3.16&>- If there is *no* evidence to the contrary, though, this would not necessarily be a fallacy. It might be a weak argument (depending on circumstances).
ct14-Fallacies-2.md&3.16&
ct14-Fallacies-2.md&3.17&## C: Misleading Vividness
ct14-Fallacies-2.md&3.17&
ct14-Fallacies-2.md&3.17&In our example, the fact that the Kiwi Fruit Computer started to produce smoke and sent his owner working in the student cafeteria does not constitute enough evidence to outweigh six independent industry studies.
ct14-Fallacies-2.md&3.17&
ct14-Fallacies-2.md&3.17&If the independent industry studies had not been there, then the text would be either a hasty generalization or a weak inductive argument, depending on how convincing the conclusion was.
ct14-Fallacies-2.md&3.17&
ct14-Fallacies-2.md&3.18&## C: Now do it yourself!
ct14-Fallacies-2.md&3.18&
ct14-Fallacies-2.md&3.18&Make up an argument which contains a misleading vividness fallacy! 
ct14-Fallacies-2.md&3.18&
ct14-Fallacies-2.md&3.18&
ct14-Fallacies-2.md&3.19&## C: Fallacies
ct14-Fallacies-2.md&3.19&
ct14-Fallacies-2.md&3.19&“I think it is important to make the requirements stricter for the graduate students. I recommend that you support it, too. After all, we are in a budget crisis and we do not want our salaries affected.”
ct14-Fallacies-2.md&3.19&
ct14-Fallacies-2.md&3.20&## C: Red Herring
ct14-Fallacies-2.md&3.20&
ct14-Fallacies-2.md&3.20&>- An irrelevant topic is presented in order to divert attention from the original issue. The basic idea is to “win” an argument by leading attention away from the argument and to another topic.
ct14-Fallacies-2.md&3.20&>- In our example, the question of salaries has nothing to do with the question of whether to make requirements stricter for graduate students.
ct14-Fallacies-2.md&3.20&
ct14-Fallacies-2.md&3.21&## C: Now do it yourself!
ct14-Fallacies-2.md&3.21&Make up an argument which contains a red herring fallacy! 
ct14-Fallacies-2.md&3.21&
ct14-Fallacies-2.md&3.21&. . .
ct14-Fallacies-2.md&3.21&
ct14-Fallacies-2.md&3.21&A good red herring fallacy is not made by saying something totally irrelevant. This would just sound crazy. The point is to say something that *seems* to make sense superficially, but that, in reality, leads away from the topic that is discussed!
ct14-Fallacies-2.md&3.21&
ct14-Fallacies-2.md&3.22&## C: Fallacies
ct14-Fallacies-2.md&3.22&
ct14-Fallacies-2.md&3.22&Father and daughter argue about washing the family car: 
ct14-Fallacies-2.md&3.22&
ct14-Fallacies-2.md&3.22&- Father: “You should help me wash the car. It is getting very dirty.”
ct14-Fallacies-2.md&3.22&- Daughter: “Why, we just washed it last year. Do we have to wash it *every day?”*
ct14-Fallacies-2.md&3.22&
ct14-Fallacies-2.md&3.23&## C: Straw Man
ct14-Fallacies-2.md&3.23&
ct14-Fallacies-2.md&3.23&A person simply ignores the actual position of the opponent and substitutes a distorted or exaggerated version of that position: 
ct14-Fallacies-2.md&3.23&
ct14-Fallacies-2.md&3.23&- Person A has position X. 
ct14-Fallacies-2.md&3.23&- Person B presents position Y (which is a distorted version of X). 
ct14-Fallacies-2.md&3.23&- Person B attacks position Y. 
ct14-Fallacies-2.md&3.23&- Therefore, X is assumed to be false.
ct14-Fallacies-2.md&3.23&
ct14-Fallacies-2.md&3.23&But attacking a distorted version of a position does not constitute an attack on the position itself!
ct14-Fallacies-2.md&3.23&
ct14-Fallacies-2.md&3.23&
ct14-Fallacies-2.md&4.0&# Exercises and additional points
ct14-Fallacies-2.md&4.0&
ct14-Fallacies-2.md&4.1&## What do you think of this?
ct14-Fallacies-2.md&4.1&
ct14-Fallacies-2.md&4.1&It is wrong to say that the sensational newspapers, those which are concerned mainly with the love life of movie stars and with aliens visiting the Earth, do not serve the public interest. After all, the public buys them, and therefore proves to have an interest in them!
ct14-Fallacies-2.md&4.1&
ct14-Fallacies-2.md&4.2&## Equivocation “interest”
ct14-Fallacies-2.md&4.2&
ct14-Fallacies-2.md&4.2&Two meanings of “interest” (equivocation):
ct14-Fallacies-2.md&4.2&
ct14-Fallacies-2.md&4.2&1. “to have an interest in something” = to be attracted by something. Example: I have an interest in computers, or spaceships, or Cantonese pop songs.
ct14-Fallacies-2.md&4.2&2. “something is in my interest” = something is good for me. Example: it is in my interest to eat more vegetables. It is in my interest to do my Critical Thinking exercises (although they may not interest me!)
ct14-Fallacies-2.md&4.2&
ct14-Fallacies-2.md&4.2&. . . 
ct14-Fallacies-2.md&4.2&
ct14-Fallacies-2.md&4.2&The public shows interest in many things (car accidents, plane crashes, natural catastrophes) which are obviously not “serving its interest.”
ct14-Fallacies-2.md&4.2&
ct14-Fallacies-2.md&4.3&## Identify the problem!
ct14-Fallacies-2.md&4.3&
ct14-Fallacies-2.md&4.3&“May I recommend the 1985 Pinot Gris wine? The 1985 cuvee has a firm structure with race and texture. I would describe it as sleek, noble, refined, with intense varietal character.”
ct14-Fallacies-2.md&4.3&
ct14-Fallacies-2.md&4.4&## Obscurity
ct14-Fallacies-2.md&4.4&
ct14-Fallacies-2.md&4.4&We don't know what he is talking about, so this is not an argument which appeals to our reason, but instead it tries to make us feel that the salesperson has impressive knowledge of wine.
ct14-Fallacies-2.md&4.4&
ct14-Fallacies-2.md&4.5&## Identify the problem!
ct14-Fallacies-2.md&4.5&
ct14-Fallacies-2.md&4.5&Some people say that they would believe in God, if they only saw some miracle to convince them. What can I say? Are they blind? They should just look around! The world is full of miracles! The miracle of modern medicine, the miracle of being able to fly in airplanes, the miracle of man's landing on the moon! What more miracles do these people expect to see?
ct14-Fallacies-2.md&4.5&
ct14-Fallacies-2.md&4.6&## Equivocation “miracle”
ct14-Fallacies-2.md&4.6&
ct14-Fallacies-2.md&4.6&>- “Miracle” in the first sense is an unnatural and unexpected event, which can be clearly traced back to God's intervention.
ct14-Fallacies-2.md&4.6&>- “Miracle” in the second sense is a technological achievement, which only looks “miraculous” because most people don't know how it works. But for the engineer or doctor, there is nothing “miraculous” about it.
ct14-Fallacies-2.md&4.6&
ct14-Fallacies-2.md&4.7&## Identify the problem!
ct14-Fallacies-2.md&4.7&
ct14-Fallacies-2.md&4.7&If Peter wants to get into medical school, he'll need to have good grades. But if he's going to keep good grades, he can't work so many hours that he doesn't have time to study. So if Peter wants to get into medical school, he shouldn't work too many hours. He should take more breaks and relax!
ct14-Fallacies-2.md&4.7&
ct14-Fallacies-2.md&4.8&## Equivocation “work”
ct14-Fallacies-2.md&4.8&
ct14-Fallacies-2.md&4.8&>- Here, “work” in the first part of the argument means “work besides studying.”
ct14-Fallacies-2.md&4.8&>- “Work” in the conclusion is used in the sense of any work, *including studying.*
ct14-Fallacies-2.md&4.8&>- Therefore, this is a fallacy of equivocation. The two words look the same, but are used in a different sense!
ct14-Fallacies-2.md&4.8&>- The conclusion does not follow from the premises.
ct14-Fallacies-2.md&4.8&
ct14-Fallacies-2.md&4.9&## Is this a good argument?
ct14-Fallacies-2.md&4.9&
ct14-Fallacies-2.md&4.9&People who love wild animals, would never buy a fur coat. But if no one buys fur coats, lots of fur-bearing animals will not be raised. And if they are not raised, there will be fewer of them. So, people who love animals are actually reducing the number of fur-bearing animals found in nature!
ct14-Fallacies-2.md&4.9&
ct14-Fallacies-2.md&4.10&## Equivocation
ct14-Fallacies-2.md&4.10&
ct14-Fallacies-2.md&4.10&>- Here, the notion of an animal existing “in nature” is confused with the animal “especially raised to produce fur.”
ct14-Fallacies-2.md&4.10&>- If more animals are raised to produce fur, it doesn't follow that more such animals will be found in nature!
ct14-Fallacies-2.md&4.10&
ct14-Fallacies-2.md&4.10&
ct14-Fallacies-2.md&4.11&## Now use fallacies in a speech
ct14-Fallacies-2.md&4.11&
ct14-Fallacies-2.md&4.11&- Write a short speech (1 minute) about whether we should use disposable plastic bags or not.
ct14-Fallacies-2.md&4.11&- Make sure not to use a single real argument in it!
ct14-Fallacies-2.md&4.11&- Use only fallacies, but try to make it as convincing as possible!
ct14-Fallacies-2.md&4.11&
ct14-Fallacies-2.md&4.12&## Example speech with fallacies
ct14-Fallacies-2.md&4.12&
ct14-Fallacies-2.md&4.12&Ban plastic bags! What a funny idea! We all know that everybody needs plastic bags. All people use them, so they cannot be too bad! I only know one person who doesn't use them, and this is an ugly old man who is poor and has no friends. This is the kind of people who are against plastic bags! And besides, recently this famous pop star said that it's okay to use them, remember? And if we ban them? Well, then we'll have no way to carry things from the market. People's handbags will stink of fish and the unemployment rate will rise and with it the crime rate! Really, we can't afford to ban plastic bags! We've always used them, anyway, so they can't be too bad! So let's stop making silly proposals and do something useful!
ct14-Fallacies-2.md&4.12&
ct14-Fallacies-2.md&4.13&## Which fallacies do you see here?
ct14-Fallacies-2.md&4.13&
ct14-Fallacies-2.md&4.13&“Great businessmen have used leather suitcases for centuries, so you don’t want to be left out! Therefore you should now take a look at our new line of suitcases ‘PremiumCase.’ They have the highest sales world-wide. Recently, the Global Business Travel Association has included them into their list of ‘50 Essential Items for the Business Traveler.’ The PremiumCase line is completely new, and combines power and flexibility in a way that guarantees true pleasure to the user.”
ct14-Fallacies-2.md&4.13&
ct14-Fallacies-2.md&4.14&## Which fallacies do you see here?
ct14-Fallacies-2.md&4.14&
ct14-Fallacies-2.md&4.14&“Great businessmen have used leather suitcases for centuries (Tradition), so you don’t want to be left out (Fear)! Therefore you should now take a look at our new line of suitcases ‘PremiumCase.’ They have the highest sales world-wide (Common practice). Recently, the Global Business Travel Association has included them into their list of ‘50 Essential Items for the Business Traveler.’ The PremiumCase line is completely new (Novelty), and combines power and flexibility in a way that guarantees true pleasure to the user (Emotion).”
ct14-Fallacies-2.md&4.14&
ct14-Fallacies-2.md&4.15&## Is this a fallacy?
ct14-Fallacies-2.md&4.15&
ct14-Fallacies-2.md&4.15&In the previous text, we see this sentence:
ct14-Fallacies-2.md&4.15&
ct14-Fallacies-2.md&4.15&“Recently, the Global Business Travel Association has included them into their list of ‘50 Essential Items for the Business Traveler.’”
ct14-Fallacies-2.md&4.15&
ct14-Fallacies-2.md&4.15&Is this a fallacy too? Which one?
ct14-Fallacies-2.md&4.15&
ct14-Fallacies-2.md&4.16&## Fallacy?
ct14-Fallacies-2.md&4.16&
ct14-Fallacies-2.md&4.16&No. The Global Business Travel Association should have relevant expertise on the things one needs to have while business traveling, so this is not an unjustified appeal to authority!
ct14-Fallacies-2.md&4.16&
ct14-Fallacies-2.md&4.17&## Choose the right fallacy!
ct14-Fallacies-2.md&4.17&
ct14-Fallacies-2.md&4.17&You have to produce ads for the following products:
ct14-Fallacies-2.md&4.17&
ct14-Fallacies-2.md&4.17&- a hair drying machine
ct14-Fallacies-2.md&4.17&- a mechanical clock from Switzerland
ct14-Fallacies-2.md&4.17&- a new sushi place
ct14-Fallacies-2.md&4.17&- running shoes for children
ct14-Fallacies-2.md&4.17&- nail polish
ct14-Fallacies-2.md&4.17&- a set of brown leather suitcases
ct14-Fallacies-2.md&4.17&- a reusable shopping bag for a supermarket chain
ct14-Fallacies-2.md&4.17&
ct14-Fallacies-2.md&4.17&What would be the most suitable fallacy for each product?
ct14-Fallacies-2.md&4.17&
ct14-Fallacies-2.md&4.18&## Which fallacy is this?
ct14-Fallacies-2.md&4.18&
ct14-Fallacies-2.md&4.18&“Though many other poets could be addressed, and though the discourse between Whitman and the century of American poets following him could be shown to hyperlink between uncountable connections ad infinitum, the ouroboric antipoetic-poetic of spokenness and the ouroboric rhetorical relationship of speaker and subject, extending from that of self and other, demand primary attention here.”
ct14-Fallacies-2.md&4.18&
ct14-Fallacies-2.md&4.18&T. Gilmore: Toward the Death and Flowering of Transcendentalism in Walt Whitman
ct14-Fallacies-2.md&4.18&
ct14-Fallacies-2.md&4.18&. . . 
ct14-Fallacies-2.md&4.18&
ct14-Fallacies-2.md&4.18&Obscurity.
ct14-Fallacies-2.md&4.18&
ct14-Fallacies-2.md&4.19&## Which fallacy is this?
ct14-Fallacies-2.md&4.19&
ct14-Fallacies-2.md&4.19&“Either you accept nuclear power, or global warming will destroy the planet.”
ct14-Fallacies-2.md&4.19&
ct14-Fallacies-2.md&4.19&. . . 
ct14-Fallacies-2.md&4.19&
ct14-Fallacies-2.md&4.19&False dilemma and appeal to fear.
ct14-Fallacies-2.md&4.19&
ct14-Fallacies-2.md&4.20&## Which fallacy is this?
ct14-Fallacies-2.md&4.20&
ct14-Fallacies-2.md&4.20&“I believe that God exists, because the Bible tells us so. The Bible must be right: after all, it is God’s word.”
ct14-Fallacies-2.md&4.20&
ct14-Fallacies-2.md&4.20&. . . 
ct14-Fallacies-2.md&4.20&
ct14-Fallacies-2.md&4.20&Begging the question.
ct14-Fallacies-2.md&4.20&
ct14-Fallacies-2.md&4.21&## Which fallacy is this?
ct14-Fallacies-2.md&4.21&
ct14-Fallacies-2.md&4.21&“You will have to study for the final exam, or you will get a bad grade.”
ct14-Fallacies-2.md&4.21&
ct14-Fallacies-2.md&4.21&. . . 
ct14-Fallacies-2.md&4.21&
ct14-Fallacies-2.md&4.21&Not a fallacy. This is a (true) dilemma.
ct14-Fallacies-2.md&4.21&
ct14-Fallacies-2.md&4.22&## Which fallacy is this?
ct14-Fallacies-2.md&4.22&
ct14-Fallacies-2.md&4.22&“All ingredients in this cake are sweet, so the cake will be sweet.”
ct14-Fallacies-2.md&4.22&
ct14-Fallacies-2.md&4.22&. . . 
ct14-Fallacies-2.md&4.22&
ct14-Fallacies-2.md&4.22&Not a fallacy. The sweetness of the ingredients *does* add up to the sweetness of the whole cake!
ct14-Fallacies-2.md&4.22&
ct14-Fallacies-2.md&4.22&
ct14-Fallacies-2.md&4.23&## What did we learn today?
ct14-Fallacies-2.md&4.23&
ct14-Fallacies-2.md&4.23&- Lots and lots of fallacies...
ct14-Fallacies-2.md&4.23&
ct14-Fallacies-2.md&4.24&## Reference
ct14-Fallacies-2.md&4.24&
ct14-Fallacies-2.md&4.24&All fallacies and many examples are from:
ct14-Fallacies-2.md&4.24&
ct14-Fallacies-2.md&4.24&The Nizkor Project,
ct14-Fallacies-2.md&4.24&http://www.nizkor.org/features/fallacies
ct14-Fallacies-2.md&4.24&
ct14-Fallacies-2.md&4.24&Originally by Dr. Michael C. Labossiere, ontologist@aol.com
ct14-Fallacies-2.md&4.24&
ct14-Fallacies-2.md&4.25&## Any questions?
ct14-Fallacies-2.md&4.25&
ct14-Fallacies-2.md&4.25&![](graphics/questions.jpg)\ 
ct14-Fallacies-2.md&4.25&
ct14-Fallacies-2.md&4.26&## Thank you for your attention!
ct14-Fallacies-2.md&4.26&
ct14-Fallacies-2.md&4.26&Email: <matthias@ln.edu.hk>
ct14-Fallacies-2.md&4.26&
ct14-Fallacies-2.md&4.26&If you have questions, please come to my office hours (see course outline).
ct14-Fallacies-2.md&4.26&
ct15-Argument-Mapping.md&0.1&% Critical Thinking
ct15-Argument-Mapping.md&0.1&% 15. Argument mapping
ct15-Argument-Mapping.md&0.1&% A. Matthias
ct15-Argument-Mapping.md&0.1&
ct15-Argument-Mapping.md&1.0&# Where we are
ct15-Argument-Mapping.md&1.0&
ct15-Argument-Mapping.md&1.1&## What did we learn last time?
ct15-Argument-Mapping.md&1.1&
ct15-Argument-Mapping.md&1.1&- Fallacies
ct15-Argument-Mapping.md&1.1&
ct15-Argument-Mapping.md&1.2&## What are we going to learn today?
ct15-Argument-Mapping.md&1.2&
ct15-Argument-Mapping.md&1.2&- How to map arguments
ct15-Argument-Mapping.md&1.2&
ct15-Argument-Mapping.md&2.0&# Argument mapping
ct15-Argument-Mapping.md&2.0&
ct15-Argument-Mapping.md&2.1&## Why map arguments?
ct15-Argument-Mapping.md&2.1&
ct15-Argument-Mapping.md&2.1&>- What is the purpose of argument mapping?
ct15-Argument-Mapping.md&2.1&>     - To show the structure of an argument more clearly.
ct15-Argument-Mapping.md&2.1&>     - To help identify premises, intermediate conclusions, and final (ultimate) conclusions.
ct15-Argument-Mapping.md&2.1&>     - To show clearly how the premises for a conclusion are connected with each other.
ct15-Argument-Mapping.md&2.1&>- In this way, we can understand an argument’s structure better.
ct15-Argument-Mapping.md&2.1&>- Argument mapping has been shown in studies to improve critical thinking skills.
ct15-Argument-Mapping.md&2.1&
ct15-Argument-Mapping.md&2.2&## Ways to map arguments
ct15-Argument-Mapping.md&2.2&
ct15-Argument-Mapping.md&2.2&>- There are many different ways one can use to map arguments.
ct15-Argument-Mapping.md&2.2&>- We will discuss three:
ct15-Argument-Mapping.md&2.2&>     - The argument mapping style discussed in HKU Think
ct15-Argument-Mapping.md&2.2&>     - The “standard diagram” style presented by Reed and Rowe (2007).
ct15-Argument-Mapping.md&2.2&>     - A more loose, less formal, mindmap-style approach to real-world debates, which we will use in the later sessions of this course (the debate preparation sessions).
ct15-Argument-Mapping.md&2.2&
ct15-Argument-Mapping.md&2.3&## How does it work?
ct15-Argument-Mapping.md&2.3&
ct15-Argument-Mapping.md&2.3&>- Each proposition (premise or conclusion) is numbered.
ct15-Argument-Mapping.md&2.3&>- The numbers are arranged in a specific way on the page, connected by arrows.
ct15-Argument-Mapping.md&2.3&>- Each arrow is a logical inference.
ct15-Argument-Mapping.md&2.3&
ct15-Argument-Mapping.md&2.4&## Standard diagram elements
ct15-Argument-Mapping.md&2.4&
ct15-Argument-Mapping.md&2.4&>- A “standard diagram” contains three kinds of elements:
ct15-Argument-Mapping.md&2.4&>     - **Dependent premises or co-premises:** Two premises that both need to be true in order to derive the conclusion.
ct15-Argument-Mapping.md&2.4&>     - **Independent premises:** Each premise stands on its own and supports a conclusion.
ct15-Argument-Mapping.md&2.4&>     - **Intermediate or sub-conclusions:** Conclusions that are used as premises for another conclusion further down the argument.
ct15-Argument-Mapping.md&2.4&
ct15-Argument-Mapping.md&2.4&
ct15-Argument-Mapping.md&2.5&## Dependent premises or co-premises
ct15-Argument-Mapping.md&2.5&
ct15-Argument-Mapping.md&2.5&Statements 1 and 2 must both be true in order to support the conclusion:
ct15-Argument-Mapping.md&2.5&
ct15-Argument-Mapping.md&2.5&![](graphics/15-amaps1.jpg)\ 
ct15-Argument-Mapping.md&2.5&
ct15-Argument-Mapping.md&2.5&
ct15-Argument-Mapping.md&2.5&
ct15-Argument-Mapping.md&2.5&
ct15-Argument-Mapping.md&2.6&## Independent premises
ct15-Argument-Mapping.md&2.6&
ct15-Argument-Mapping.md&2.6&Statements 2, 3, 4 each support the conclusion independently:
ct15-Argument-Mapping.md&2.6&
ct15-Argument-Mapping.md&2.6&![](graphics/15-amaps2.jpg)\ 
ct15-Argument-Mapping.md&2.6&
ct15-Argument-Mapping.md&2.6&
ct15-Argument-Mapping.md&2.6&
ct15-Argument-Mapping.md&2.6&
ct15-Argument-Mapping.md&2.7&## Intermediate or sub-conclusions
ct15-Argument-Mapping.md&2.7&
ct15-Argument-Mapping.md&2.7&Statement 5 supports statement 4, which is a sub-conclusion. 4, together with the independent premises 2 and 3, supports the ultimate conclusion 1:
ct15-Argument-Mapping.md&2.7&
ct15-Argument-Mapping.md&2.7&![](graphics/15-amaps3.jpg)\ 
ct15-Argument-Mapping.md&2.7&
ct15-Argument-Mapping.md&2.7&
ct15-Argument-Mapping.md&2.7&
ct15-Argument-Mapping.md&2.7&
ct15-Argument-Mapping.md&2.7&
ct15-Argument-Mapping.md&2.8&## Counter-arguments (1)^[from: By Sujai Thomman - Own work - made with The Reasoning PowerPoint App, CC BY-SA 4.0, https://commons.wikimedia.org/w/index.php?curid=54708372]
ct15-Argument-Mapping.md&2.8&
ct15-Argument-Mapping.md&2.8&![](graphics/15-amaps4.png)\ 
ct15-Argument-Mapping.md&2.8&
ct15-Argument-Mapping.md&2.8&
ct15-Argument-Mapping.md&2.9&## Counter-arguments (2)
ct15-Argument-Mapping.md&2.9&
ct15-Argument-Mapping.md&2.9&>- Observe how you can mark counter-arguments by putting a minus sign (“-”) at the end of the arrow. 
ct15-Argument-Mapping.md&2.9&>- You can do this with the other style we saw before, too. Just put a minus sign at the arrow’s tip.
ct15-Argument-Mapping.md&2.9&
ct15-Argument-Mapping.md&2.9&
ct15-Argument-Mapping.md&2.10&## How to diagram a text
ct15-Argument-Mapping.md&2.10&
ct15-Argument-Mapping.md&2.10&Here is one way to do it:^[Beardsley, Monroe C. (1950). Practical logic. New York: Prentice-Hall.]
ct15-Argument-Mapping.md&2.10&
ct15-Argument-Mapping.md&2.10&>- Use brackets to separate individual statements in a text.
ct15-Argument-Mapping.md&2.10&>- Number the statements.
ct15-Argument-Mapping.md&2.10&>- Circle the logical connectives and other structural words indicating conclusions and so on.
ct15-Argument-Mapping.md&2.10&>- Add any logical indicators that are left out.
ct15-Argument-Mapping.md&2.10&>- (Add any missing or hidden premises and conclusions.)
ct15-Argument-Mapping.md&2.10&>- Draw your diagram.
ct15-Argument-Mapping.md&2.10&
ct15-Argument-Mapping.md&2.10&
ct15-Argument-Mapping.md&2.11&## Full example (Wikipedia)
ct15-Argument-Mapping.md&2.11&
ct15-Argument-Mapping.md&2.11&![](graphics/15-amaps5.png)\ 
ct15-Argument-Mapping.md&2.11&
ct15-Argument-Mapping.md&2.11&
ct15-Argument-Mapping.md&2.11&![](graphics/15-amaps6.jpg)\ 
ct15-Argument-Mapping.md&2.11&
ct15-Argument-Mapping.md&2.11&
ct15-Argument-Mapping.md&2.11&
ct15-Argument-Mapping.md&2.12&## HKU Think diagrams
ct15-Argument-Mapping.md&2.12&
ct15-Argument-Mapping.md&2.12&>- The HKU Think resource^[https://philosophy.hku.hk/think/arg/complex.php] uses a slightly different way of drawing argument maps.
ct15-Argument-Mapping.md&2.12&>- The structure is the same, only the way of drawing the arrows is different (and, I think, more confusing).
ct15-Argument-Mapping.md&2.12&
ct15-Argument-Mapping.md&2.13&## HKU Think: Single premise, single conclusion
ct15-Argument-Mapping.md&2.13&
ct15-Argument-Mapping.md&2.13&“Death is inevitable. So life is meaningless.”
ct15-Argument-Mapping.md&2.13&
ct15-Argument-Mapping.md&2.13&![](graphics/15-amap10.png)\ 
ct15-Argument-Mapping.md&2.13&
ct15-Argument-Mapping.md&2.14&## Co-premises
ct15-Argument-Mapping.md&2.14&
ct15-Argument-Mapping.md&2.14&“(1) Paris is in France, and (2) France is in Europe. So obviously (3) Paris is in Europe.”
ct15-Argument-Mapping.md&2.14&
ct15-Argument-Mapping.md&2.14&![](graphics/15-amap11.png)\ 
ct15-Argument-Mapping.md&2.14&
ct15-Argument-Mapping.md&2.14&
ct15-Argument-Mapping.md&2.14&
ct15-Argument-Mapping.md&2.15&## Independent premises
ct15-Argument-Mapping.md&2.15&
ct15-Argument-Mapping.md&2.15&“(1) Smoking is unhealthy, since (2) it can cause cancer. Furthermore, (3) it also increases the chance of heart attacks and strokes.”
ct15-Argument-Mapping.md&2.15&
ct15-Argument-Mapping.md&2.15&![](graphics/15-amap12.png)\ 
ct15-Argument-Mapping.md&2.15&
ct15-Argument-Mapping.md&2.15&
ct15-Argument-Mapping.md&2.15&In your diagrams, better separate the premises’ lines at the end, keeping two separate arrows going into the conclusion’s circle. Otherwise it might look like these are co-premises!
ct15-Argument-Mapping.md&2.15&
ct15-Argument-Mapping.md&2.16&## One reason, multiple conclusions
ct15-Argument-Mapping.md&2.16&
ct15-Argument-Mapping.md&2.16&“(1) Gold is a metal. (2) So it conducts electricity. (3) It also conducts heat.”
ct15-Argument-Mapping.md&2.16&
ct15-Argument-Mapping.md&2.16&![](graphics/15-amap13.png)\ 
ct15-Argument-Mapping.md&2.16&
ct15-Argument-Mapping.md&2.16&
ct15-Argument-Mapping.md&2.17&## Exercise
ct15-Argument-Mapping.md&2.17&
ct15-Argument-Mapping.md&2.17&This computer can think. So it is conscious. Since we should not kill any conscious beings, we should not switch it off.
ct15-Argument-Mapping.md&2.17&
ct15-Argument-Mapping.md&2.17&*Do it yourself: create an argument map!*
ct15-Argument-Mapping.md&2.17&
ct15-Argument-Mapping.md&2.18&## Solution
ct15-Argument-Mapping.md&2.18&
ct15-Argument-Mapping.md&2.18&[1] This computer can think. So [2] it is conscious. Since [3] we should not kill any conscious beings, [4] we should not switch it off.
ct15-Argument-Mapping.md&2.18&
ct15-Argument-Mapping.md&2.18&![](graphics/15-amap15.png)\ 
ct15-Argument-Mapping.md&2.18&
ct15-Argument-Mapping.md&2.18&
ct15-Argument-Mapping.md&2.18&
ct15-Argument-Mapping.md&2.19&## More complex arguments (1)
ct15-Argument-Mapping.md&2.19&
ct15-Argument-Mapping.md&2.19&“Mary cannot come to the party because her scooter is broken. Peter also cannot come because he has to pick up his new hat. I did not invite my other friends, so none of my friends will come to the party.”
ct15-Argument-Mapping.md&2.19&
ct15-Argument-Mapping.md&2.19&*Do it yourself!*
ct15-Argument-Mapping.md&2.19&
ct15-Argument-Mapping.md&2.19&
ct15-Argument-Mapping.md&2.20&## More complex arguments (2)
ct15-Argument-Mapping.md&2.20&
ct15-Argument-Mapping.md&2.20&First rewrite the argument in standard form:
ct15-Argument-Mapping.md&2.20&
ct15-Argument-Mapping.md&2.20&1. Mary cannot come to the party.
ct15-Argument-Mapping.md&2.20&2. Mary's scooter is broken.
ct15-Argument-Mapping.md&2.20&3. Peter cannot come to the party.
ct15-Argument-Mapping.md&2.20&4. Peter has to pick up his new hat.
ct15-Argument-Mapping.md&2.20&5. I did not invite my other friends.
ct15-Argument-Mapping.md&2.20&6. [Conclusion] None of my friends will come to the party.
ct15-Argument-Mapping.md&2.20&
ct15-Argument-Mapping.md&2.20&![](graphics/15-amap14.png)\ 
ct15-Argument-Mapping.md&2.20&
ct15-Argument-Mapping.md&2.20&
ct15-Argument-Mapping.md&2.21&## Exercise
ct15-Argument-Mapping.md&2.21&
ct15-Argument-Mapping.md&2.21&Many people think that having a dark tan is attractive. But the fact is that too much exposure to the sun is very unhealthy. It has been shown that sunlight can cause premature aging of the skin. Ultraviolet rays in the sun might also trigger skin cancer.
ct15-Argument-Mapping.md&2.21& 
ct15-Argument-Mapping.md&2.21&*Create an argument map!*
ct15-Argument-Mapping.md&2.21&
ct15-Argument-Mapping.md&2.22&## Solution
ct15-Argument-Mapping.md&2.22&
ct15-Argument-Mapping.md&2.22& [1. Many people think that having a dark tan is attractive.] [2. But the fact is that too much exposure to the sun is very unhealthy.] [3. It has been shown that sunlight can cause premature aging of the skin.] [4. Ultraviolet rays in the sun might also trigger skin cancer.]
ct15-Argument-Mapping.md&2.22&
ct15-Argument-Mapping.md&2.22&![](graphics/15-amap16.png)\ 
ct15-Argument-Mapping.md&2.22&
ct15-Argument-Mapping.md&2.22&Sentence [1] is not a counter-argument! It is not related to the conclusion at all. This is why we leave it out in the diagram.
ct15-Argument-Mapping.md&2.22&
ct15-Argument-Mapping.md&2.23&## Exercise
ct15-Argument-Mapping.md&2.23&
ct15-Argument-Mapping.md&2.23&If Mary is here, then Peter should be here as well. It follows that if Peter is not here, Mary is also absent, and indeed Peter is not here. So most likely Mary is not around.
ct15-Argument-Mapping.md&2.23&
ct15-Argument-Mapping.md&2.23&*Create an argument map!*
ct15-Argument-Mapping.md&2.23&
ct15-Argument-Mapping.md&2.24&## Solution
ct15-Argument-Mapping.md&2.24&
ct15-Argument-Mapping.md&2.24&[1. If Mary is here, then Peter should be here as well.] [2. It follows that if Peter is not here, Mary is also absent,] and indeed [3. Peter is not here.] So most likely [4. Mary is not around.]
ct15-Argument-Mapping.md&2.24&
ct15-Argument-Mapping.md&2.24&
ct15-Argument-Mapping.md&2.24&![](graphics/15-amap17.png)\ 
ct15-Argument-Mapping.md&2.24&
ct15-Argument-Mapping.md&2.24&
ct15-Argument-Mapping.md&2.24&
ct15-Argument-Mapping.md&2.25&## Exercise
ct15-Argument-Mapping.md&2.25&
ct15-Argument-Mapping.md&2.25&Marriage is becoming unfashionable. Divorce rate is at an all time high and living together without being married is increasingly presented in a positive manner in the media. Movies are full of characters who live together and unwilling to commit to a lifelong partnership. Even newspaper columnists recommend people to live together for an extended period before marriage in order to test their compatibility.
ct15-Argument-Mapping.md&2.25&
ct15-Argument-Mapping.md&2.25&*Create an argument map!*
ct15-Argument-Mapping.md&2.25&
ct15-Argument-Mapping.md&2.26&## Solution
ct15-Argument-Mapping.md&2.26&
ct15-Argument-Mapping.md&2.26&[1. Marriage is becoming unfashionable.] [2. Divorce rate is at an all time high], and [3. living together without being married is increasingly presented in a positive manner in the media]. [4. Movies are full of characters who live together and unwilling to commit to a lifelong partnership]. [5. Even newspaper columnists recommend people to live together for an extended period before marriage in order to test their compatibility.]
ct15-Argument-Mapping.md&2.26&
ct15-Argument-Mapping.md&2.26&![](graphics/15-amap18.png)\ 
ct15-Argument-Mapping.md&2.26&
ct15-Argument-Mapping.md&2.26&
ct15-Argument-Mapping.md&2.26&
ct15-Argument-Mapping.md&2.27&## Exercise
ct15-Argument-Mapping.md&2.27&
ct15-Argument-Mapping.md&2.27&All university students should study critical thinking. After all, critical thinking is necessary for surviving in the new economy, as we need to adapt to rapid changes, and make critical use of information in making decisions. Also, critical thinking can help us reflect on our values and purposes in life. Finally, critical thinking helps us improve our study skills.
ct15-Argument-Mapping.md&2.27&
ct15-Argument-Mapping.md&2.27&*Create an argument map!*
ct15-Argument-Mapping.md&2.27&
ct15-Argument-Mapping.md&2.28&## Solution
ct15-Argument-Mapping.md&2.28&
ct15-Argument-Mapping.md&2.28&[1. All university students should study critical thinking.] After all, [2. critical thinking is necessary for surviving in the new economy] as [3. we need to adapt to rapid changes, and make critical use of information in making decisions.] Also, [4. critical thinking can help us reflect on our values and purposes in life.] Finally, [5. critical thinking helps us improve our study skills.]
ct15-Argument-Mapping.md&2.28&
ct15-Argument-Mapping.md&2.28&![](graphics/15-amap19.png)\ 
ct15-Argument-Mapping.md&2.28&
ct15-Argument-Mapping.md&2.28&
ct15-Argument-Mapping.md&3.0&# More exercises (from previous lectures)
ct15-Argument-Mapping.md&3.0&
ct15-Argument-Mapping.md&3.1&## Exercise
ct15-Argument-Mapping.md&3.1&
ct15-Argument-Mapping.md&3.1&"The world would be better if there was a single world government. If there was such a government, then there would be no wars. There would also be much more economic cooperation and free trade between the different regions of the world. On the other hand, if there was a world government, there would be more administration costs. But these would be outweighed by the benefits of having a world government."
ct15-Argument-Mapping.md&3.1&
ct15-Argument-Mapping.md&3.1&*Create an argument map!*
ct15-Argument-Mapping.md&3.1&
ct15-Argument-Mapping.md&3.2&## Considerations
ct15-Argument-Mapping.md&3.2&
ct15-Argument-Mapping.md&3.2&- Conclusion: The world would be better if there was a single world government.
ct15-Argument-Mapping.md&3.2&- Does the speaker believe that there would be more administrative costs if there was a world government? -- Yes.
ct15-Argument-Mapping.md&3.2&- Does this belief support the conclusion? -- No. This is a counter-argument, but he dismisses it, because the benefits would outweigh the additional costs.
ct15-Argument-Mapping.md&3.2&
ct15-Argument-Mapping.md&3.3&## Exercise
ct15-Argument-Mapping.md&3.3&
ct15-Argument-Mapping.md&3.3&"To punish people merely for what they have done would be unjust, because the forbidden act might have been an accident for which the person who did it cannot be held to blame."
ct15-Argument-Mapping.md&3.3&
ct15-Argument-Mapping.md&3.3&*Create an argument map!*
ct15-Argument-Mapping.md&3.3&
ct15-Argument-Mapping.md&3.3&. . .
ct15-Argument-Mapping.md&3.3&
ct15-Argument-Mapping.md&3.3&>- You can see that the conclusion contains the word "unjust," for which there is no premise!
ct15-Argument-Mapping.md&3.3&>- So the argument cannot be valid.
ct15-Argument-Mapping.md&3.3&>- We need a premise to connect "cannot be blamed" with "unjust."
ct15-Argument-Mapping.md&3.3&
ct15-Argument-Mapping.md&3.4&## Argument analysis
ct15-Argument-Mapping.md&3.4&
ct15-Argument-Mapping.md&3.4&The explicit premises:
ct15-Argument-Mapping.md&3.4&
ct15-Argument-Mapping.md&3.4&The forbidden act might be an accident.  
ct15-Argument-Mapping.md&3.4&Persons cannot be blamed for (some) accidents.  
ct15-Argument-Mapping.md&3.4& --------------------------------------------------------------  
ct15-Argument-Mapping.md&3.4&To punish people merely for what they have done would be unjust.
ct15-Argument-Mapping.md&3.4&
ct15-Argument-Mapping.md&3.4&You can easily see that this argument is not valid. There are no premises about "unjust," and so the conclusion does not follow from the premises. *What is the hidden premise?*
ct15-Argument-Mapping.md&3.4&
ct15-Argument-Mapping.md&3.4&. . .
ct15-Argument-Mapping.md&3.4&
ct15-Argument-Mapping.md&3.4&Hidden premise: it is unjust to punish people, if they cannot be blamed for an act.
ct15-Argument-Mapping.md&3.4&
ct15-Argument-Mapping.md&3.4&*Now you can draw the argument map.*
ct15-Argument-Mapping.md&3.4&
ct15-Argument-Mapping.md&3.4&
ct15-Argument-Mapping.md&3.5&## Exercise
ct15-Argument-Mapping.md&3.5&
ct15-Argument-Mapping.md&3.5&"Democracy is not perfect, since it has many problems. For example, it is much more difficult for a leader of a democracy to implement unpopular policies than it is for a leader of a non-democratic government, even when those policies are the best policies. Nevertheless, democracy is the best form of government there is, since other forms of government have even bigger problems. This is why Hong Kong should have a democracy."
ct15-Argument-Mapping.md&3.5&
ct15-Argument-Mapping.md&3.5&*Create an argument map. Present this argument in standard format. Identify the final conclusion as well as the premises and sub-conclusions. Do not include any premises or sub-conclusions that don't support the main conclusion of the argument!*
ct15-Argument-Mapping.md&3.5&
ct15-Argument-Mapping.md&3.6&## Argument analysis
ct15-Argument-Mapping.md&3.6&
ct15-Argument-Mapping.md&3.6&>- Conclusion: Hong Kong should have a democracy.
ct15-Argument-Mapping.md&3.6&>- In the conclusion we have "Hong Kong," but not in the premises.
ct15-Argument-Mapping.md&3.6&>- So we need to create a premise that mentions Hong Kong in a way that it can then be used in the conclusion.
ct15-Argument-Mapping.md&3.6&>- You also see that the whole section from "For example" to "best policies" is a counter-argument. According to the instructions, we don't need to include it in our answer.
ct15-Argument-Mapping.md&3.6&
ct15-Argument-Mapping.md&3.7&## Argument analysis
ct15-Argument-Mapping.md&3.7&
ct15-Argument-Mapping.md&3.7&"Democracy is not perfect, since it has many problems. \textcolor{red}{For example, it is much more difficult for a leader of a democracy to implement unpopular policies than it is for a leader of a non-democratic government, even when those policies are the best policies.} Nevertheless, democracy is the best form of government there is, since other forms of government have even bigger problems. This is why Hong Kong should have a democracy."
ct15-Argument-Mapping.md&3.7&
ct15-Argument-Mapping.md&3.7&>- Premise: Other forms of government have bigger problems.
ct15-Argument-Mapping.md&3.7&>- Sub-conclusion: Democracy is the best form of government there is.
ct15-Argument-Mapping.md&3.7&>- Hidden premise: Hong Kong should have the best form of government.
ct15-Argument-Mapping.md&3.7&>- Conclusion: Hong Kong should have a democracy.
ct15-Argument-Mapping.md&3.7&>- The other premises and subconclusion do not support this conclusion, but form a counter-argument.
ct15-Argument-Mapping.md&3.7&
ct15-Argument-Mapping.md&3.7&
ct15-Argument-Mapping.md&3.8&## What did we learn today?
ct15-Argument-Mapping.md&3.8&- How to map arguments
ct15-Argument-Mapping.md&3.8&
ct15-Argument-Mapping.md&3.9&## References
ct15-Argument-Mapping.md&3.9&HKU OpenCourseWare on critical thinking, logic, and creativity:
ct15-Argument-Mapping.md&3.9&
ct15-Argument-Mapping.md&3.9&http://philosophy.hku.hk/think
ct15-Argument-Mapping.md&3.9&
ct15-Argument-Mapping.md&3.9&
ct15-Argument-Mapping.md&3.10&## Any questions?
ct15-Argument-Mapping.md&3.10&
ct15-Argument-Mapping.md&3.10&![](graphics/questions.jpg)\ 
ct15-Argument-Mapping.md&3.10&
ct15-Argument-Mapping.md&3.11&## Thank you for your attention!
ct15-Argument-Mapping.md&3.11&
ct15-Argument-Mapping.md&3.11&Email: <matthias@ln.edu.hk>
ct15-Argument-Mapping.md&3.11&
ct15-Argument-Mapping.md&3.11&If you have questions, please come to my office hours (see course outline).
ct15-Argument-Mapping.md&3.11&
ct15-Argument-Mapping.md&3.11&
ct15-Argument-Mapping.md&3.11&
ct15-Argument-Mapping.md&3.11&
ct15-Argument-Mapping.md&3.11&
ct16-Numbers-and-Probabilities.md&0.1&% Critical Thinking
ct16-Numbers-and-Probabilities.md&0.1&% 16. Numbers and Probabilities
ct16-Numbers-and-Probabilities.md&0.1&% A. Matthias
ct16-Numbers-and-Probabilities.md&0.1&
ct16-Numbers-and-Probabilities.md&1.0&# Where we are
ct16-Numbers-and-Probabilities.md&1.0&
ct16-Numbers-and-Probabilities.md&1.1&## What did we learn last time?
ct16-Numbers-and-Probabilities.md&1.1&
ct16-Numbers-and-Probabilities.md&1.1&- Argument maps
ct16-Numbers-and-Probabilities.md&1.1&
ct16-Numbers-and-Probabilities.md&1.2&## What are we going to learn today?
ct16-Numbers-and-Probabilities.md&1.2&
ct16-Numbers-and-Probabilities.md&1.2&- How to calculate basic probabilities
ct16-Numbers-and-Probabilities.md&1.2&- Probabilities of independent events
ct16-Numbers-and-Probabilities.md&1.2&- Conditional probabilities
ct16-Numbers-and-Probabilities.md&1.2&- Risks, benefits, expected values
ct16-Numbers-and-Probabilities.md&1.2&- Utility
ct16-Numbers-and-Probabilities.md&1.2&- Conjunction fallacy
ct16-Numbers-and-Probabilities.md&1.2&
ct16-Numbers-and-Probabilities.md&2.0&# Probabilities
ct16-Numbers-and-Probabilities.md&2.0&
ct16-Numbers-and-Probabilities.md&2.1&## Basic probabilities
ct16-Numbers-and-Probabilities.md&2.1&
ct16-Numbers-and-Probabilities.md&2.1&An important part of critical thinking is the ability to think in probabilities and to avoid probability errors.
ct16-Numbers-and-Probabilities.md&2.1&
ct16-Numbers-and-Probabilities.md&2.1&Let's start with an easy one:
ct16-Numbers-and-Probabilities.md&2.1&
ct16-Numbers-and-Probabilities.md&2.1&You have a sack filled with socks. There are 200 white socks and 100 red socks in there. If I take out one sock, with what probability will it be a white sock?
ct16-Numbers-and-Probabilities.md&2.1&
ct16-Numbers-and-Probabilities.md&2.1&. . . 
ct16-Numbers-and-Probabilities.md&2.1&
ct16-Numbers-and-Probabilities.md&2.1&$\frac{2}{3}$ or 0.67, if the socks are mixed well.
ct16-Numbers-and-Probabilities.md&2.1&
ct16-Numbers-and-Probabilities.md&2.2&## Basic probabilities
ct16-Numbers-and-Probabilities.md&2.2&
ct16-Numbers-and-Probabilities.md&2.2&>- We express probabilities usually in fractions or in numbers between 0-1.
ct16-Numbers-and-Probabilities.md&2.2&>- 0 is the probability of an event which can not occur.
ct16-Numbers-and-Probabilities.md&2.2&>- 1 is the probability of an event which is *certain* to occur.
ct16-Numbers-and-Probabilities.md&2.2&
ct16-Numbers-and-Probabilities.md&2.2&. . . 
ct16-Numbers-and-Probabilities.md&2.2&
ct16-Numbers-and-Probabilities.md&2.2&How to calculate it:
ct16-Numbers-and-Probabilities.md&2.2&
ct16-Numbers-and-Probabilities.md&2.2&Probability = $\frac{number\ of\ ‘interesting’\ outcomes}{number\ of\ all\ outcomes}$
ct16-Numbers-and-Probabilities.md&2.2&
ct16-Numbers-and-Probabilities.md&2.2&p = $\frac{200\ (white\ socks)}{300\ (all\ socks)}$ = $\frac{2}{3}$
ct16-Numbers-and-Probabilities.md&2.2&
ct16-Numbers-and-Probabilities.md&2.2&
ct16-Numbers-and-Probabilities.md&2.3&## Basic probabilities
ct16-Numbers-and-Probabilities.md&2.3&
ct16-Numbers-and-Probabilities.md&2.3&You have a sack filled with socks.  There are 200 white socks and 100 red socks in there. If I take out ten socks (putting every sock back after I took it out), how many white and how many red socks do I expect to have seen?
ct16-Numbers-and-Probabilities.md&2.3&
ct16-Numbers-and-Probabilities.md&2.3&. . . 
ct16-Numbers-and-Probabilities.md&2.3&
ct16-Numbers-and-Probabilities.md&2.3&We expect $\frac{2}{3}$ of the socks (around 6-7) to be white, and $\frac{1}{3}$ (around 3-4) to be red.
ct16-Numbers-and-Probabilities.md&2.3&
ct16-Numbers-and-Probabilities.md&2.4&## One more question
ct16-Numbers-and-Probabilities.md&2.4&
ct16-Numbers-and-Probabilities.md&2.4&I have a sack of 34 red and 122 white socks. How often do I have to draw a sock from the sack in order to be *sure* to have a pair of the same colour?
ct16-Numbers-and-Probabilities.md&2.4&
ct16-Numbers-and-Probabilities.md&2.4&. . . 
ct16-Numbers-and-Probabilities.md&2.4&
ct16-Numbers-and-Probabilities.md&2.4&3 times. (This has nothing to do with probabilities).
ct16-Numbers-and-Probabilities.md&2.4&
ct16-Numbers-and-Probabilities.md&2.4&
ct16-Numbers-and-Probabilities.md&3.0&# More probabilities
ct16-Numbers-and-Probabilities.md&3.0&
ct16-Numbers-and-Probabilities.md&3.1&## Try it yourself!^[This and following from: http://www.teachervision.fen.com/tv/printables/Math_5_CT_12.pdf]
ct16-Numbers-and-Probabilities.md&3.1&
ct16-Numbers-and-Probabilities.md&3.1&![](graphics/17-games-2.png)\ 
ct16-Numbers-and-Probabilities.md&3.1&
ct16-Numbers-and-Probabilities.md&3.1&At the school fair, two spinner games offer prizes you would like to win. One offers a CD if the arrow points to an R; the other offers a computer game if it points to a 2. You have money to play only one game. Which game gives you the greater chance of winning a prize?
ct16-Numbers-and-Probabilities.md&3.1&
ct16-Numbers-and-Probabilities.md&3.2&## Solution
ct16-Numbers-and-Probabilities.md&3.2& 
ct16-Numbers-and-Probabilities.md&3.2&The second game.
ct16-Numbers-and-Probabilities.md&3.2&
ct16-Numbers-and-Probabilities.md&3.2&\Large{$\frac{2}{5}>\frac{3}{8}$}
ct16-Numbers-and-Probabilities.md&3.2&
ct16-Numbers-and-Probabilities.md&3.3&## Try it yourself:
ct16-Numbers-and-Probabilities.md&3.3&
ct16-Numbers-and-Probabilities.md&3.3&![](graphics/17-games-2.png)\ 
ct16-Numbers-and-Probabilities.md&3.3&
ct16-Numbers-and-Probabilities.md&3.3&There is also a dice game at the fair. Each of 2 dice are numbered 1–6. If you roll *a sum of* 3, 4, 5, or 6, you win a DVD. Is the chance of winning a DVD greater than the chance of winning a CD?
ct16-Numbers-and-Probabilities.md&3.3&
ct16-Numbers-and-Probabilities.md&3.4&## Solution (1)
ct16-Numbers-and-Probabilities.md&3.4&
ct16-Numbers-and-Probabilities.md&3.4&Yes, the chance for the DVD is greater:
ct16-Numbers-and-Probabilities.md&3.4&
ct16-Numbers-and-Probabilities.md&3.4&\Large{$\frac{7}{18} > \frac{3}{8}$}
ct16-Numbers-and-Probabilities.md&3.4&
ct16-Numbers-and-Probabilities.md&3.5&## Solution (2)
ct16-Numbers-and-Probabilities.md&3.5&
ct16-Numbers-and-Probabilities.md&3.5&Make a table of possible outcomes. Those with a star have the right sum:
ct16-Numbers-and-Probabilities.md&3.5&
ct16-Numbers-and-Probabilities.md&3.5&```
ct16-Numbers-and-Probabilities.md&3.5&11		21*	    31*		41*		51*		61
ct16-Numbers-and-Probabilities.md&3.5&12*		22*		32*		42*		52		62
ct16-Numbers-and-Probabilities.md&3.5&13*		23*		33*		43		53		63
ct16-Numbers-and-Probabilities.md&3.5&14*		24*		34		44		54		64
ct16-Numbers-and-Probabilities.md&3.5&15*		25		35		45		55		65
ct16-Numbers-and-Probabilities.md&3.5&16		26		36		46		56		66
ct16-Numbers-and-Probabilities.md&3.5&```
ct16-Numbers-and-Probabilities.md&3.5&
ct16-Numbers-and-Probabilities.md&3.5&\Large{$\frac{14}{36} = \frac{7}{18}$}
ct16-Numbers-and-Probabilities.md&3.5&
ct16-Numbers-and-Probabilities.md&3.6&## Solution (3)
ct16-Numbers-and-Probabilities.md&3.6&
ct16-Numbers-and-Probabilities.md&3.6&The easier (?) way:
ct16-Numbers-and-Probabilities.md&3.6&
ct16-Numbers-and-Probabilities.md&3.6&>- With 2 dice we have 6x6=36 possible outcomes.
ct16-Numbers-and-Probabilities.md&3.6&>- Of these, one half are the same like the other half: (1,5) is the same like (5,1) and so on.
ct16-Numbers-and-Probabilities.md&3.6&>- So we have 18 unique outcomes, and of those 7 have a sum of 3, 4, 5, or 6.
ct16-Numbers-and-Probabilities.md&3.6&>- Thus: \Large{$\frac{7}{18}$}
ct16-Numbers-and-Probabilities.md&3.6&
ct16-Numbers-and-Probabilities.md&3.7&## Probabilities of independent events^[Source for this and the following pages: http://www.mathgoodies.com/lessons/vol6/independent_events.html]
ct16-Numbers-and-Probabilities.md&3.7&
ct16-Numbers-and-Probabilities.md&3.7&Two events, A and B, are *independent* if the fact that A occurs does not affect the probability of B occurring.
ct16-Numbers-and-Probabilities.md&3.7&
ct16-Numbers-and-Probabilities.md&3.7&. . . 
ct16-Numbers-and-Probabilities.md&3.7&
ct16-Numbers-and-Probabilities.md&3.7&Examples:
ct16-Numbers-and-Probabilities.md&3.7&
ct16-Numbers-and-Probabilities.md&3.7&>- I pick one sock out of a sack, replace it, and pick a second.
ct16-Numbers-and-Probabilities.md&3.7&>- It rains outside, and I drop my pen.
ct16-Numbers-and-Probabilities.md&3.7&
ct16-Numbers-and-Probabilities.md&3.8&## Probabilities of independent events
ct16-Numbers-and-Probabilities.md&3.8&
ct16-Numbers-and-Probabilities.md&3.8&Multiplication rule:
ct16-Numbers-and-Probabilities.md&3.8&
ct16-Numbers-and-Probabilities.md&3.8&When two events, A and B, are independent, the probability of both occurring is:
ct16-Numbers-and-Probabilities.md&3.8&
ct16-Numbers-and-Probabilities.md&3.8&\f{P(A and B) = P(A) · P(B)}
ct16-Numbers-and-Probabilities.md&3.8&
ct16-Numbers-and-Probabilities.md&3.9&## Probabilities of independent events
ct16-Numbers-and-Probabilities.md&3.9&
ct16-Numbers-and-Probabilities.md&3.9&A coin is tossed and a single 6-sided die is rolled. Find the probability of landing on the head side of the coin and rolling a 3 on the die.
ct16-Numbers-and-Probabilities.md&3.9&
ct16-Numbers-and-Probabilities.md&3.9&. . . 
ct16-Numbers-and-Probabilities.md&3.9&
ct16-Numbers-and-Probabilities.md&3.9&P(head) = $\frac{1}{2}$
ct16-Numbers-and-Probabilities.md&3.9&
ct16-Numbers-and-Probabilities.md&3.9&P(3)	= $\frac{1}{6}$
ct16-Numbers-and-Probabilities.md&3.9&
ct16-Numbers-and-Probabilities.md&3.9&P(head and 3) = P(head) · P(3) = $\frac{1}{2}$ · $\frac{1}{6}$ = $\frac{1}{12}$
ct16-Numbers-and-Probabilities.md&3.9&
ct16-Numbers-and-Probabilities.md&3.10&## Probabilities of independent events
ct16-Numbers-and-Probabilities.md&3.10&
ct16-Numbers-and-Probabilities.md&3.10&A large basket of fruit contains 3 oranges, 2 apples, and 5 bananas. Now it is dark and you grab a piece of fruit at random. What is the probability of grabbing an orange or a banana?
ct16-Numbers-and-Probabilities.md&3.10&
ct16-Numbers-and-Probabilities.md&3.10&. . . 
ct16-Numbers-and-Probabilities.md&3.10&
ct16-Numbers-and-Probabilities.md&3.10&- Probability for an orange: $\frac{3}{10}$ or 0.3
ct16-Numbers-and-Probabilities.md&3.10&
ct16-Numbers-and-Probabilities.md&3.10&- Probability for a banana: $\frac{5}{10}$ or 0.5
ct16-Numbers-and-Probabilities.md&3.10&
ct16-Numbers-and-Probabilities.md&3.10&- Probability to get the one *or* the other:  0.3+0.5 = 0.8 = 80%
ct16-Numbers-and-Probabilities.md&3.10&
ct16-Numbers-and-Probabilities.md&3.11&## Probabilities of independent events
ct16-Numbers-and-Probabilities.md&3.11&
ct16-Numbers-and-Probabilities.md&3.11&>- In the previous examples we made sure that the events did not change the probability of subsequent events.
ct16-Numbers-and-Probabilities.md&3.11&>- For example, when I picked a sock, I *replaced it first,* before picking another sock. 
ct16-Numbers-and-Probabilities.md&3.11&>- This is not necessary, if the population is very big. Then not replacing the sample has only a very small effect which can normally be disregarded.
ct16-Numbers-and-Probabilities.md&3.11&
ct16-Numbers-and-Probabilities.md&3.12&## Probabilities of independent events
ct16-Numbers-and-Probabilities.md&3.12&
ct16-Numbers-and-Probabilities.md&3.12&A nationwide survey found that 72% of people in the United States like apples. If 3 people are selected at random, what is the probability that all three like apples?
ct16-Numbers-and-Probabilities.md&3.12&
ct16-Numbers-and-Probabilities.md&3.12&. . .
ct16-Numbers-and-Probabilities.md&3.12&
ct16-Numbers-and-Probabilities.md&3.12&Let L represent the event of randomly choosing a person who likes apples from the U.S.:
ct16-Numbers-and-Probabilities.md&3.12&
ct16-Numbers-and-Probabilities.md&3.12&P(L) · P(L) · P(L) =  (0.72)·(0.72)·(0.72) = 0.72^3^ = 0.37 = 37%
ct16-Numbers-and-Probabilities.md&3.12&
ct16-Numbers-and-Probabilities.md&3.13&## Probabilities
ct16-Numbers-and-Probabilities.md&3.13&
ct16-Numbers-and-Probabilities.md&3.13&According to a survey, in 7 children only 2 like salad.
ct16-Numbers-and-Probabilities.md&3.13&
ct16-Numbers-and-Probabilities.md&3.13&Your mother has invited 4 children to the birthday of your little brother, and is thinking of making a salad.
ct16-Numbers-and-Probabilities.md&3.13&
ct16-Numbers-and-Probabilities.md&3.13&How high is the probability that *all four children* will actually like the salad?
ct16-Numbers-and-Probabilities.md&3.13&
ct16-Numbers-and-Probabilities.md&3.13&. . . 
ct16-Numbers-and-Probabilities.md&3.13&
ct16-Numbers-and-Probabilities.md&3.13&Kids who like salad = $\frac{2}{7}$ = 0.285 = 28.5%
ct16-Numbers-and-Probabilities.md&3.13&
ct16-Numbers-and-Probabilities.md&3.13&Probability of 4 children to like salad:
ct16-Numbers-and-Probabilities.md&3.13&
ct16-Numbers-and-Probabilities.md&3.13&0.285 · 0.285 · 0.285 · 0.285 = 0.285^4^ = 0.006 or 0.6%
ct16-Numbers-and-Probabilities.md&3.13&
ct16-Numbers-and-Probabilities.md&3.13&(better serve cookies instead!)
ct16-Numbers-and-Probabilities.md&3.13&
ct16-Numbers-and-Probabilities.md&3.14&## Probabilities
ct16-Numbers-and-Probabilities.md&3.14&
ct16-Numbers-and-Probabilities.md&3.14&According to a survey, 3 out of 4 students like pop music, while only 1 out of 6 likes classic music.
ct16-Numbers-and-Probabilities.md&3.14&
ct16-Numbers-and-Probabilities.md&3.14&You have to organize some classic music to be played at the birthday of your best friend (6 guests) and some pop music for your department's end of term celebration (60 students).
ct16-Numbers-and-Probabilities.md&3.14&Assuming the guests are a random selection of students, which music is more probable to be liked by all invited guests?
ct16-Numbers-and-Probabilities.md&3.14&
ct16-Numbers-and-Probabilities.md&3.14&. . . 
ct16-Numbers-and-Probabilities.md&3.14&
ct16-Numbers-and-Probabilities.md&3.14&- Probability to like classic music = $\frac{1}{6}$ = 0.166
ct16-Numbers-and-Probabilities.md&3.14&
ct16-Numbers-and-Probabilities.md&3.14&- Probability to like pop music = $\frac{3}{4}$ = 0.75
ct16-Numbers-and-Probabilities.md&3.14&
ct16-Numbers-and-Probabilities.md&3.14&- Probability of 6 people to like classic: 0.166^6^ = 0.0000214
ct16-Numbers-and-Probabilities.md&3.14&- Probability of 60 people to like pop: 0.75^60^ = 0.0000000318
ct16-Numbers-and-Probabilities.md&3.14&- Probability for pop is around a 1000 times lower!
ct16-Numbers-and-Probabilities.md&3.14&
ct16-Numbers-and-Probabilities.md&4.0&# Conditional probabilities
ct16-Numbers-and-Probabilities.md&4.0&
ct16-Numbers-and-Probabilities.md&4.1&## Conditional probabilities
ct16-Numbers-and-Probabilities.md&4.1&
ct16-Numbers-and-Probabilities.md&4.1&>- The multiplication rule applies only to *independent* events.
ct16-Numbers-and-Probabilities.md&4.1&>- If the events are *not* independent, we need to use a way of calculating the probability of an event B happening, *given that another event A has already happened.*
ct16-Numbers-and-Probabilities.md&4.1&>- We call P(B|A) the “probability of B given that A has happened.”
ct16-Numbers-and-Probabilities.md&4.1&>- P(A and B) is the probability of the events A and B happening.
ct16-Numbers-and-Probabilities.md&4.1&
ct16-Numbers-and-Probabilities.md&4.1&. . . 
ct16-Numbers-and-Probabilities.md&4.1&
ct16-Numbers-and-Probabilities.md&4.1&Then:
ct16-Numbers-and-Probabilities.md&4.1&
ct16-Numbers-and-Probabilities.md&4.1&\Large{\f{P(B|A) = $\frac{P(A\ and\ B)}{P(A)}$}}
ct16-Numbers-and-Probabilities.md&4.1&
ct16-Numbers-and-Probabilities.md&4.2&## Example:
ct16-Numbers-and-Probabilities.md&4.2&
ct16-Numbers-and-Probabilities.md&4.2&A teacher gave her class two tests. 20% of the class passed both tests and 30% of the class passed the first test. What percent of those who passed the first test also passed the second test?
ct16-Numbers-and-Probabilities.md&4.2&
ct16-Numbers-and-Probabilities.md&4.2&. . .
ct16-Numbers-and-Probabilities.md&4.2&
ct16-Numbers-and-Probabilities.md&4.2&![](graphics/17-class-test.jpg)\ 
ct16-Numbers-and-Probabilities.md&4.2&
ct16-Numbers-and-Probabilities.md&4.2&P(Second\ |\ First) = $\frac{P(First\ and\ Second)}{P(First)}$ = $\frac{0.2}{0.3}$ = 0.667 = 67%
ct16-Numbers-and-Probabilities.md&4.2&
ct16-Numbers-and-Probabilities.md&4.2&(20 is 67% of 30)
ct16-Numbers-and-Probabilities.md&4.2&
ct16-Numbers-and-Probabilities.md&4.3&## Conditional probabilities
ct16-Numbers-and-Probabilities.md&4.3&
ct16-Numbers-and-Probabilities.md&4.3&We can also turn this formula around, so that we can compute the combined probability of two events happening, depending on the probability of the first event and the probability of the *second given the first:*
ct16-Numbers-and-Probabilities.md&4.3&
ct16-Numbers-and-Probabilities.md&4.3&\f{P(A and B) = P(A)·P(B|A)}
ct16-Numbers-and-Probabilities.md&4.3&
ct16-Numbers-and-Probabilities.md&4.4&## All probability formulas
ct16-Numbers-and-Probabilities.md&4.4&
ct16-Numbers-and-Probabilities.md&4.4&\f{Probability = $\frac{number\ of\ ‘interesting’\ outcomes}{number\ of\ all\ outcomes}$}
ct16-Numbers-and-Probabilities.md&4.4&
ct16-Numbers-and-Probabilities.md&4.4&Independent events (multiplication rule):
ct16-Numbers-and-Probabilities.md&4.4&
ct16-Numbers-and-Probabilities.md&4.4&\f{P(A and B) = P(A) · P(B)}
ct16-Numbers-and-Probabilities.md&4.4&
ct16-Numbers-and-Probabilities.md&4.4&Conditional probability (B given that A):
ct16-Numbers-and-Probabilities.md&4.4&
ct16-Numbers-and-Probabilities.md&4.4&\f{P(B|A) = $\frac{P(A\ and\ B)}{P(A)}$}
ct16-Numbers-and-Probabilities.md&4.4&
ct16-Numbers-and-Probabilities.md&4.4&\f{P(A\ and\ B) = P(A)·P(B|A)}
ct16-Numbers-and-Probabilities.md&4.4&
ct16-Numbers-and-Probabilities.md&4.5&## Conditional probabilities
ct16-Numbers-and-Probabilities.md&4.5&
ct16-Numbers-and-Probabilities.md&4.5&The probability that it is Friday and that a student is absent is 0.03. Since there are 5 school days in a week, the probability that it is Friday is 0.2.
ct16-Numbers-and-Probabilities.md&4.5&
ct16-Numbers-and-Probabilities.md&4.5&What is the probability that a student is absent *given that today is Friday?*
ct16-Numbers-and-Probabilities.md&4.5&
ct16-Numbers-and-Probabilities.md&4.5&. . . 
ct16-Numbers-and-Probabilities.md&4.5&
ct16-Numbers-and-Probabilities.md&4.5&P(Absent\ |\ Friday) = $\frac{P(Friday\ and\ Absent)}{P(Friday)}$ = $\frac{0.03}{0.2}$ = 0.15 = 15%
ct16-Numbers-and-Probabilities.md&4.5&
ct16-Numbers-and-Probabilities.md&4.6&## Conditional probabilities
ct16-Numbers-and-Probabilities.md&4.6&
ct16-Numbers-and-Probabilities.md&4.6&At Lingnan University, the probability that a student takes CCC-8001 Logic and Critical Thinking and CCC-8003 Understanding Morality both taught by Andy is 0.087. The probability that a student takes Critical Thinking taught by Andy is 0.68. What is the probability that a student takes Andy’s Understanding Morality given that the student is taking Andy’s Critical Thinking?
ct16-Numbers-and-Probabilities.md&4.6&
ct16-Numbers-and-Probabilities.md&4.6&. . .
ct16-Numbers-and-Probabilities.md&4.6&
ct16-Numbers-and-Probabilities.md&4.6&P(UM | LCT) = $\frac{P(LCT\ and\ UM) }{P(LCT)}$ = $\frac{0.087}{0.68}$ = 0.128 = 12.8%
ct16-Numbers-and-Probabilities.md&4.6&
ct16-Numbers-and-Probabilities.md&4.7&## Exercise
ct16-Numbers-and-Probabilities.md&4.7&
ct16-Numbers-and-Probabilities.md&4.7&In New York State, 48% of all teenagers own a skateboard and 39% of all teenagers own a skateboard and roller blades. What is the probability that a teenager owns roller blades given that the teenager owns a skateboard?
ct16-Numbers-and-Probabilities.md&4.7&
ct16-Numbers-and-Probabilities.md&4.7&. . .
ct16-Numbers-and-Probabilities.md&4.7&
ct16-Numbers-and-Probabilities.md&4.7&P(rb | s) = $\frac{P(s\ and\ rb)}{P(s)}$ = $\frac{0.39}{0.48}$ = 0.8125 = 81%
ct16-Numbers-and-Probabilities.md&4.7&
ct16-Numbers-and-Probabilities.md&4.8&## Exercise
ct16-Numbers-and-Probabilities.md&4.8&
ct16-Numbers-and-Probabilities.md&4.8&The government has estimated the following survival probabilities for men:
ct16-Numbers-and-Probabilities.md&4.8&
ct16-Numbers-and-Probabilities.md&4.8&- probability that a man lives at least 70 years: 80%
ct16-Numbers-and-Probabilities.md&4.8&- probability that a man lives at least 80 years: 50%.
ct16-Numbers-and-Probabilities.md&4.8&
ct16-Numbers-and-Probabilities.md&4.8&What is the conditional probability that a man lives at least 80 years given that he has just celebrated his 70th birthday?
ct16-Numbers-and-Probabilities.md&4.8&
ct16-Numbers-and-Probabilities.md&4.8&. . .
ct16-Numbers-and-Probabilities.md&4.8&
ct16-Numbers-and-Probabilities.md&4.8&- P(e): Probability that a man lives at least eighty years
ct16-Numbers-and-Probabilities.md&4.8&- P(s): Probability that a man lives at least seventy years
ct16-Numbers-and-Probabilities.md&4.8&
ct16-Numbers-and-Probabilities.md&4.8&P(e | s) = $\frac{P(s\ and\ e)}{P(s)}$ = $\frac{0.5}{0.8}$ = 0.625 = 62.5%
ct16-Numbers-and-Probabilities.md&4.8&
ct16-Numbers-and-Probabilities.md&4.9&## How to do these calculations in the final exam
ct16-Numbers-and-Probabilities.md&4.9&
ct16-Numbers-and-Probabilities.md&4.9&>- In the final exam you are not allowed to use a calculator! (Or any other electronic device).
ct16-Numbers-and-Probabilities.md&4.9&>- Therefore, you would answer the previous question by writing:
ct16-Numbers-and-Probabilities.md&4.9&>- P(e | s) = $\frac{P(s\ and\ e)}{P(s)}$ = $\frac{0.5}{0.8}$ (and stop here).
ct16-Numbers-and-Probabilities.md&4.9&>- You don't need to make the actual final calculation, but you *do* need to fill in all the values into the formulas you use, so that only that final calculation is missing.
ct16-Numbers-and-Probabilities.md&4.9&
ct16-Numbers-and-Probabilities.md&5.0&# Probability fallacies
ct16-Numbers-and-Probabilities.md&5.0&
ct16-Numbers-and-Probabilities.md&5.1&## Correct or incorrect?
ct16-Numbers-and-Probabilities.md&5.1&
ct16-Numbers-and-Probabilities.md&5.1&![](graphics/17-roulette.jpg)\ 
ct16-Numbers-and-Probabilities.md&5.1&
ct16-Numbers-and-Probabilities.md&5.1&Fred is playing roulette in a Macau casino. The roulette wheel has 36 numbers (ignoring the zero), of which half are red and half are black. In the last ten rounds, all the winning numbers have been red. So Fred thinks that now there must be more black numbers than red numbers coming up, and that he would have a better chance of winning if he bets on black.
ct16-Numbers-and-Probabilities.md&5.1&
ct16-Numbers-and-Probabilities.md&5.2&## Correct or incorrect?
ct16-Numbers-and-Probabilities.md&5.2&
ct16-Numbers-and-Probabilities.md&5.2&Incorrect! The outcomes of the previous ten rounds can have no effect on the motion of the wheel and the ball; past outcomes can’t affect future outcomes. In other words, the outcomes are independent. The probability of a black number is still $\frac{1}{2}$ in each round, irrespective of what has come before.
ct16-Numbers-and-Probabilities.md&5.2&
ct16-Numbers-and-Probabilities.md&5.3&## Getting an “A”
ct16-Numbers-and-Probabilities.md&5.3&
ct16-Numbers-and-Probabilities.md&5.3&The probability of getting an “A” is 20% (due to the prescribed distribution of grades), so the probability of the same person to get 3 “A”s in 3 different classes must be (0.2)^3^=0.008 or 0.8%. Correct?
ct16-Numbers-and-Probabilities.md&5.3&
ct16-Numbers-and-Probabilities.md&5.3&. . . 
ct16-Numbers-and-Probabilities.md&5.3&
ct16-Numbers-and-Probabilities.md&5.3&Not correct.
ct16-Numbers-and-Probabilities.md&5.3&
ct16-Numbers-and-Probabilities.md&5.3&Some people are better students, and they are more likely to get an “A” than others. So if someone already has 2 “A”s, then the probability for him to get another “A” is much higher than the normal 20%.
ct16-Numbers-and-Probabilities.md&5.3&
ct16-Numbers-and-Probabilities.md&5.3&Since the events are *not* independent, we would need to calculate the probability of getting a 3rd A *given that the same student already has 2 “A”s* (which would be a higher number than 0.2)
ct16-Numbers-and-Probabilities.md&5.3&
ct16-Numbers-and-Probabilities.md&5.4&## Conjunction of events
ct16-Numbers-and-Probabilities.md&5.4&
ct16-Numbers-and-Probabilities.md&5.4&Bill is 34 years old.  He is intelligent, but boring.  In school, he was strong in mathematics but weak in social studies and humanities.
ct16-Numbers-and-Probabilities.md&5.4&
ct16-Numbers-and-Probabilities.md&5.4&How probable are the following propositions? Order them according to their probability of being true!
ct16-Numbers-and-Probabilities.md&5.4&
ct16-Numbers-and-Probabilities.md&5.4&A:  Bill is an accountant who plays jazz for a hobby.  
ct16-Numbers-and-Probabilities.md&5.4&B:  Bill is an accountant.  
ct16-Numbers-and-Probabilities.md&5.4&C:  Bill plays jazz for a hobby.
ct16-Numbers-and-Probabilities.md&5.4&
ct16-Numbers-and-Probabilities.md&5.4&. . .
ct16-Numbers-and-Probabilities.md&5.4&
ct16-Numbers-and-Probabilities.md&5.4&>- Many students rank the propositions B>A>C.
ct16-Numbers-and-Probabilities.md&5.4&>- That is, they say that it is more probable that Bill is an accountant who plays jazz than a jazz player.
ct16-Numbers-and-Probabilities.md&5.4&>- But this must be wrong!
ct16-Numbers-and-Probabilities.md&5.4&>- For any two events, P(A&B) must always be lower or equal to either P(A) or P(B)!
ct16-Numbers-and-Probabilities.md&5.4&
ct16-Numbers-and-Probabilities.md&5.5&## Conjunction fallacy
ct16-Numbers-and-Probabilities.md&5.5&
ct16-Numbers-and-Probabilities.md&5.5&This is called the *conjunction fallacy.*
ct16-Numbers-and-Probabilities.md&5.5&
ct16-Numbers-and-Probabilities.md&5.5&Sometimes, a statement that contains a conjunction of propositions seems to be more probable than the same propositions by themselves. But this must be wrong.
ct16-Numbers-and-Probabilities.md&5.5&
ct16-Numbers-and-Probabilities.md&5.5&To be an accountant who plays jazz must always be less probable than to be a jazz player, since all accountants who play jazz are included in the set of jazz players. But additionally they must also fulfil another criterion, that of being accountants. So they must be fewer than the jazz players in general!
ct16-Numbers-and-Probabilities.md&5.5&
ct16-Numbers-and-Probabilities.md&6.0&# Risks and benefits
ct16-Numbers-and-Probabilities.md&6.0&
ct16-Numbers-and-Probabilities.md&6.1&## Risks and benefits
ct16-Numbers-and-Probabilities.md&6.1&
ct16-Numbers-and-Probabilities.md&6.1&When we talk about risks and benefits, it is not enough to just calculate probabilities!
ct16-Numbers-and-Probabilities.md&6.1&
ct16-Numbers-and-Probabilities.md&6.1&For example, how would you choose in these two cases:
ct16-Numbers-and-Probabilities.md&6.1&
ct16-Numbers-and-Probabilities.md&6.1&1. You can play a game where you have an 80% chance of winning 100 dollars, and a 20% chance of losing. If you lose, you have to pay 1 dollar. Do you play?
ct16-Numbers-and-Probabilities.md&6.1&2. You can fly with this very old airplane or take the train. There’s a 90% chance that the plane will make it, but there’s also a 10% chance that it may crash. In this case you will be killed. The train just takes one hour longer than the flight to the same destination and is perfectly safe. Do you take the plane or the train?
ct16-Numbers-and-Probabilities.md&6.1&
ct16-Numbers-and-Probabilities.md&6.2&## Risks
ct16-Numbers-and-Probabilities.md&6.2&
ct16-Numbers-and-Probabilities.md&6.2&>- Most people asked would decide to play the game but refuse to fly with the plane, although the chances to win the game are less than the chances of surviving the flight!
ct16-Numbers-and-Probabilities.md&6.2&>- This shows that in judging risks we don’t look at the probabilities alone, but also at the expected gain.
ct16-Numbers-and-Probabilities.md&6.2&>- To lose your life is an outcome which is so bad that a 10% chance seems unacceptable. A 20% chance of losing one dollar, on the other hand, seems okay if I can win 100 dollars.
ct16-Numbers-and-Probabilities.md&6.2&
ct16-Numbers-and-Probabilities.md&6.3&## Would you play this game?
ct16-Numbers-and-Probabilities.md&6.3&
ct16-Numbers-and-Probabilities.md&6.3&I toss two coins, and I pay you $2 if they both show heads but you pay me $1 if one or both show tails. -- *Calculate how much you can expect to win in the long run!*
ct16-Numbers-and-Probabilities.md&6.3&
ct16-Numbers-and-Probabilities.md&6.3&. . . 
ct16-Numbers-and-Probabilities.md&6.3&
ct16-Numbers-and-Probabilities.md&6.3&The **expected value** of this bet is obtained by multiplying the probability of each outcome by its value *to you,* and then adding the results.
ct16-Numbers-and-Probabilities.md&6.3&
ct16-Numbers-and-Probabilities.md&6.3&-----------   ------------------   ----------------
ct16-Numbers-and-Probabilities.md&6.3&HH	  			   		     +\$2          Probability 1/4
ct16-Numbers-and-Probabilities.md&6.3&HT	  				  	     -\$1           Probability 1/4
ct16-Numbers-and-Probabilities.md&6.3&TH	  					     -\$1           Probability 1/4
ct16-Numbers-and-Probabilities.md&6.3&TT	  						 -\$1           Probability 1/4
ct16-Numbers-and-Probabilities.md&6.3&----------------------------------------------
ct16-Numbers-and-Probabilities.md&6.3&
ct16-Numbers-and-Probabilities.md&6.3&. . . 
ct16-Numbers-and-Probabilities.md&6.3&
ct16-Numbers-and-Probabilities.md&6.3&So the expected value *to you* of the bet is
ct16-Numbers-and-Probabilities.md&6.3&
ct16-Numbers-and-Probabilities.md&6.3&(**2**∙$\frac{1}{4}$) - (**1**∙$\frac{3}{4}$) = $\frac{2}{4}$-$\frac{3}{4}$ =  -$\frac{1}{4}$ =  **-$0.25**
ct16-Numbers-and-Probabilities.md&6.3&
ct16-Numbers-and-Probabilities.md&6.3&**Bold**: dollars won or lost; fractions: probability.
ct16-Numbers-and-Probabilities.md&6.3&
ct16-Numbers-and-Probabilities.md&6.4&## Expected value
ct16-Numbers-and-Probabilities.md&6.4&
ct16-Numbers-and-Probabilities.md&6.4&>- The expected value does *not* mean that you should expect to lose $0.25 on each bet, since you will either win $2 or lose $1. 
ct16-Numbers-and-Probabilities.md&6.4&>- But it tells you something about what you should expect in the long run.
ct16-Numbers-and-Probabilities.md&6.4&>- If you bet over and over again, you will win some and lose some, but eventually your winnings and losses will average out to a loss of $0.25 per game. 
ct16-Numbers-and-Probabilities.md&6.4&>- In the long run, this bet will not be financially worthwhile to you (but it will be worthwhile to me!)
ct16-Numbers-and-Probabilities.md&6.4&
ct16-Numbers-and-Probabilities.md&6.5&## Fair bet
ct16-Numbers-and-Probabilities.md&6.5&
ct16-Numbers-and-Probabilities.md&6.5&*A bet with an expected value of zero is called a fair bet.*
ct16-Numbers-and-Probabilities.md&6.5&
ct16-Numbers-and-Probabilities.md&6.5&For example, if the above bet is modified so that I pay you $3 if both coins show heads, then it is fair:
ct16-Numbers-and-Probabilities.md&6.5&
ct16-Numbers-and-Probabilities.md&6.5&-----------   ------------------   ----------------
ct16-Numbers-and-Probabilities.md&6.5&HH	  			   		     +\$3          Probability 1/4
ct16-Numbers-and-Probabilities.md&6.5&HT	  				  	     -\$1           Probability 1/4
ct16-Numbers-and-Probabilities.md&6.5&TH	  					     -\$1           Probability 1/4
ct16-Numbers-and-Probabilities.md&6.5&TT	  						 -\$1           Probability 1/4
ct16-Numbers-and-Probabilities.md&6.5&----------------------------------------------
ct16-Numbers-and-Probabilities.md&6.5&
ct16-Numbers-and-Probabilities.md&6.5&(**3**∙$\frac{1}{4}$) - (**1**∙$\frac{3}{4}$) = $\frac{3}{4}$-$\frac{3}{4}$ = **0**
ct16-Numbers-and-Probabilities.md&6.5&
ct16-Numbers-and-Probabilities.md&6.5&(**Bold**: dollars won or lost; fractions: probability.)
ct16-Numbers-and-Probabilities.md&6.5&
ct16-Numbers-and-Probabilities.md&6.5&>- It is fair in the sense that there is no built-in bias regarding the expected value in favor of either one of us.
ct16-Numbers-and-Probabilities.md&6.5&>- But “fair” does *not* mean that we have the same chances of winning (see table!) 
ct16-Numbers-and-Probabilities.md&6.5&
ct16-Numbers-and-Probabilities.md&7.0&# Utility
ct16-Numbers-and-Probabilities.md&7.0&
ct16-Numbers-and-Probabilities.md&7.1&## Utility
ct16-Numbers-and-Probabilities.md&7.1&
ct16-Numbers-and-Probabilities.md&7.1&>- Similar to the concept of expected value is the concept of utility.
ct16-Numbers-and-Probabilities.md&7.1&>- Here we don’t have a gain or loss which can be expressed in numbers, but we try to express it in numerical form anyway, in order to be able to do a calculation of expected values.
ct16-Numbers-and-Probabilities.md&7.1&>- The utility of an outcome provides a numerical measure of how good or bad the outcome is.
ct16-Numbers-and-Probabilities.md&7.1&
ct16-Numbers-and-Probabilities.md&7.2&## Utility
ct16-Numbers-and-Probabilities.md&7.2&
ct16-Numbers-and-Probabilities.md&7.2&Suppose I am trying to decide whether to take my umbrella with me today.
ct16-Numbers-and-Probabilities.md&7.2& 
ct16-Numbers-and-Probabilities.md&7.2&There are four possible outcomes, because it may or may not rain, and I may or may not take my umbrella. 
ct16-Numbers-and-Probabilities.md&7.2&
ct16-Numbers-and-Probabilities.md&7.3&## Utility (2)
ct16-Numbers-and-Probabilities.md&7.3&>- If it rains, it is clearly better to take my umbrella than not, but if the weather is fine, then it is marginally better not to take my umbrella, so that I have fewer things to carry.
ct16-Numbers-and-Probabilities.md&7.3&>- The best outcome for me is if it's fine and I don't take my umbrella, so I'll assign that outcome a utility of +10. 
ct16-Numbers-and-Probabilities.md&7.3&>- The outcome in which it's fine and I do take my umbrella is only marginally worse than that, so I'll assign it a utility of +9. 
ct16-Numbers-and-Probabilities.md&7.3&>- The worst outcome is if it rains and I don't take my umbrella; I'll assign it a utility of -10.
ct16-Numbers-and-Probabilities.md&7.3&>- The outcome in which it rains and I do take my umbrella is somewhere in the middle, so I'll assign it a utility of 0.
ct16-Numbers-and-Probabilities.md&7.3&
ct16-Numbers-and-Probabilities.md&7.4&## Utility (3)
ct16-Numbers-and-Probabilities.md&7.4&
ct16-Numbers-and-Probabilities.md&7.4&The situation can be expressed in a table:
ct16-Numbers-and-Probabilities.md&7.4&
ct16-Numbers-and-Probabilities.md&7.4&\begin{tabular}{|c||c|c|}
ct16-Numbers-and-Probabilities.md&7.4&\hline
ct16-Numbers-and-Probabilities.md&7.4&\ & Umbrella & No umbrella \\
ct16-Numbers-and-Probabilities.md&7.4&\hline
ct16-Numbers-and-Probabilities.md&7.4&Rain & 0 & -10 \\
ct16-Numbers-and-Probabilities.md&7.4&\hline
ct16-Numbers-and-Probabilities.md&7.4&Fine & +9 & +10 \\
ct16-Numbers-and-Probabilities.md&7.4&\hline
ct16-Numbers-and-Probabilities.md&7.4&\end{tabular}
ct16-Numbers-and-Probabilities.md&7.4&
ct16-Numbers-and-Probabilities.md&7.4&\vspace{3ex}
ct16-Numbers-and-Probabilities.md&7.4&
ct16-Numbers-and-Probabilities.md&7.4&Now the weather forecast tells you that there is a 20% chance of rain today. Should you take your umbrella?
ct16-Numbers-and-Probabilities.md&7.4&
ct16-Numbers-and-Probabilities.md&7.5&## Solution
ct16-Numbers-and-Probabilities.md&7.5&
ct16-Numbers-and-Probabilities.md&7.5&Expected utility of taking the umbrella:
ct16-Numbers-and-Probabilities.md&7.5&
ct16-Numbers-and-Probabilities.md&7.5&![](graphics/17-utility1.png)\ 
ct16-Numbers-and-Probabilities.md&7.5&
ct16-Numbers-and-Probabilities.md&7.5&Expected utility of not taking the umbrella:
ct16-Numbers-and-Probabilities.md&7.5&
ct16-Numbers-and-Probabilities.md&7.5&![](graphics/17-utility2.png)\ 
ct16-Numbers-and-Probabilities.md&7.5&
ct16-Numbers-and-Probabilities.md&7.5&Since the expected utility of taking my umbrella is greater, I should take my umbrella.
ct16-Numbers-and-Probabilities.md&7.5&
ct16-Numbers-and-Probabilities.md&7.6&## Calculate the utility!
ct16-Numbers-and-Probabilities.md&7.6&
ct16-Numbers-and-Probabilities.md&7.6&Fred is short-sighted, and he is considering laser surgery to correct his vision.
ct16-Numbers-and-Probabilities.md&7.6&
ct16-Numbers-and-Probabilities.md&7.6&He does some research, and finds that the chance of a complete correction is 46%, the chance of a partial correction is 44%, the chance of no change in vision is 9% and the chance of a worsening of vision of 1%.
ct16-Numbers-and-Probabilities.md&7.6&
ct16-Numbers-and-Probabilities.md&7.6&Fred assigns a utility of +5 to achieving a complete vision correction, +2 to a partial correction, 0 to no change in vision, and -10 to a worsening of vision. He also assigns a utility of -2 to the cost and physical discomfort of the operation.
ct16-Numbers-and-Probabilities.md&7.6&
ct16-Numbers-and-Probabilities.md&7.6&Assuming Fred wants to maximise his expected utility, should he have the operation? 
ct16-Numbers-and-Probabilities.md&7.6&
ct16-Numbers-and-Probabilities.md&7.7&## Solution
ct16-Numbers-and-Probabilities.md&7.7&
ct16-Numbers-and-Probabilities.md&7.7&The expected utility of *not* having the operation is zero (no change in vision, no cost or discomfort due to the operation). 
ct16-Numbers-and-Probabilities.md&7.7&
ct16-Numbers-and-Probabilities.md&7.7&The expected utility of having the operation is:
ct16-Numbers-and-Probabilities.md&7.7&
ct16-Numbers-and-Probabilities.md&7.7&(5·0.46) + (2·0.44) + (0·0.09) + (-10·0.01)-2 = +1.08. 
ct16-Numbers-and-Probabilities.md&7.7&
ct16-Numbers-and-Probabilities.md&7.7&The expected utility of having the operation is larger than the expected utility of not having the operation, so Fred should have the operation. 
ct16-Numbers-and-Probabilities.md&7.7&
ct16-Numbers-and-Probabilities.md&7.8&## What did we learn today?
ct16-Numbers-and-Probabilities.md&7.8&
ct16-Numbers-and-Probabilities.md&7.8&- How to calculate basic probabilities
ct16-Numbers-and-Probabilities.md&7.8&- Probabilities of independent events
ct16-Numbers-and-Probabilities.md&7.8&- Conditional probabilities
ct16-Numbers-and-Probabilities.md&7.8&- Risks, benefits, expected values
ct16-Numbers-and-Probabilities.md&7.8&- Utility
ct16-Numbers-and-Probabilities.md&7.8&- Conjunction fallacy
ct16-Numbers-and-Probabilities.md&7.8&
ct16-Numbers-and-Probabilities.md&7.9&## Any questions?
ct16-Numbers-and-Probabilities.md&7.9&
ct16-Numbers-and-Probabilities.md&7.9&![](graphics/questions.jpg)\ 
ct16-Numbers-and-Probabilities.md&7.9&
ct16-Numbers-and-Probabilities.md&7.10&## Thank you for your attention!
ct16-Numbers-and-Probabilities.md&7.10&
ct16-Numbers-and-Probabilities.md&7.10&Email: <matthias@ln.edu.hk>
ct16-Numbers-and-Probabilities.md&7.10&
ct16-Numbers-and-Probabilities.md&7.10&If you have questions, please come to my office hours (see course outline).
ct17-Utilitarianism-Animal-Rights.md&0.1&% Critical Thinking
ct17-Utilitarianism-Animal-Rights.md&0.1&% 17. Debate 1: Human and animal rights
ct17-Utilitarianism-Animal-Rights.md&0.1&% A. Matthias
ct17-Utilitarianism-Animal-Rights.md&0.1&
ct17-Utilitarianism-Animal-Rights.md&1.0&# Where we are
ct17-Utilitarianism-Animal-Rights.md&1.0&
ct17-Utilitarianism-Animal-Rights.md&1.1&## What did we learn last time?
ct17-Utilitarianism-Animal-Rights.md&1.1&
ct17-Utilitarianism-Animal-Rights.md&1.1&- Numbers, probabilities, utility
ct17-Utilitarianism-Animal-Rights.md&1.1&
ct17-Utilitarianism-Animal-Rights.md&1.2&## What are we going to learn today?
ct17-Utilitarianism-Animal-Rights.md&1.2&
ct17-Utilitarianism-Animal-Rights.md&1.2&- How to brainstorm a debate topic
ct17-Utilitarianism-Animal-Rights.md&1.2&
ct17-Utilitarianism-Animal-Rights.md&1.2&
ct17-Utilitarianism-Animal-Rights.md&2.0&# Utilitarianism
ct17-Utilitarianism-Animal-Rights.md&2.0&
ct17-Utilitarianism-Animal-Rights.md&2.1&## Bentham, Mill, and their times
ct17-Utilitarianism-Animal-Rights.md&2.1&
ct17-Utilitarianism-Animal-Rights.md&2.1&The two main historical proponents of utilitarianism are:
ct17-Utilitarianism-Animal-Rights.md&2.1&
ct17-Utilitarianism-Animal-Rights.md&2.1&- Jeremy BENTHAM (1748-1832)
ct17-Utilitarianism-Animal-Rights.md&2.1&- John Stuart MILL (1806-1873)
ct17-Utilitarianism-Animal-Rights.md&2.1&
ct17-Utilitarianism-Animal-Rights.md&2.1&. . .
ct17-Utilitarianism-Animal-Rights.md&2.1&
ct17-Utilitarianism-Animal-Rights.md&2.1&Note:
ct17-Utilitarianism-Animal-Rights.md&2.1&
ct17-Utilitarianism-Animal-Rights.md&2.1&- Bentham was about sixty years older than Mill, or about two generations. Bentham could have been Mill's grandfather.
ct17-Utilitarianism-Animal-Rights.md&2.1&- One of the newest moral theories (only about 200 years old).
ct17-Utilitarianism-Animal-Rights.md&2.1&
ct17-Utilitarianism-Animal-Rights.md&2.2&## Bentham, Mill, and their times (1)
ct17-Utilitarianism-Animal-Rights.md&2.2&Around the same time, we have
ct17-Utilitarianism-Animal-Rights.md&2.2&
ct17-Utilitarianism-Animal-Rights.md&2.2&- the American Declaration of Independence (1776),
ct17-Utilitarianism-Animal-Rights.md&2.2&- the French Revolution (1789), and
ct17-Utilitarianism-Animal-Rights.md&2.2&- the Industrial Revolution (around 1760 to 1840).
ct17-Utilitarianism-Animal-Rights.md&2.2&
ct17-Utilitarianism-Animal-Rights.md&2.3&## Bentham, Mill, and their times (2)
ct17-Utilitarianism-Animal-Rights.md&2.3&
ct17-Utilitarianism-Animal-Rights.md&2.3&All these movements share common basic ideas with Bentham's utilitarianism:
ct17-Utilitarianism-Animal-Rights.md&2.3&
ct17-Utilitarianism-Animal-Rights.md&2.3&- American Declaration of Independence (1776): Freedom and democracy,
ct17-Utilitarianism-Animal-Rights.md&2.3&- French Revolution: The equality of all human beings, equal rights for all,
ct17-Utilitarianism-Animal-Rights.md&2.3&- Industrial Revolution: The belief that science and engineering can be fruitfully applied to human matters and problems of ethics (see: Jules Verne, born 1828), and that science and technology are *beneficial* for society.
ct17-Utilitarianism-Animal-Rights.md&2.3&
ct17-Utilitarianism-Animal-Rights.md&2.3&. . .
ct17-Utilitarianism-Animal-Rights.md&2.3&
ct17-Utilitarianism-Animal-Rights.md&2.3&We will understand utilitarianism better if we keep these ideas and the historical origins of the theory in mind.
ct17-Utilitarianism-Animal-Rights.md&2.3&
ct17-Utilitarianism-Animal-Rights.md&2.4&## Bentham's Utilitarianism: Utility
ct17-Utilitarianism-Animal-Rights.md&2.4&
ct17-Utilitarianism-Animal-Rights.md&2.5&### Utility is:
ct17-Utilitarianism-Animal-Rights.md&2.5&
ct17-Utilitarianism-Animal-Rights.md&2.5&1. *Pleasure* *over* *pain*, *happiness* over unhappiness.
ct17-Utilitarianism-Animal-Rights.md&2.5&2. The greatest amount of happiness for the *greatest number* (of people).
ct17-Utilitarianism-Animal-Rights.md&2.5&
ct17-Utilitarianism-Animal-Rights.md&2.6&### Notes:
ct17-Utilitarianism-Animal-Rights.md&2.6&
ct17-Utilitarianism-Animal-Rights.md&2.6&1. What does "over" in "pleasure over pain" mean exactly?
ct17-Utilitarianism-Animal-Rights.md&2.6&2. What is the significance of talking about the "greatest number"?
ct17-Utilitarianism-Animal-Rights.md&2.6&3. What is the relation of happiness to pleasure and pain in this quote?
ct17-Utilitarianism-Animal-Rights.md&2.6&
ct17-Utilitarianism-Animal-Rights.md&3.0&# Animal rights
ct17-Utilitarianism-Animal-Rights.md&3.0&
ct17-Utilitarianism-Animal-Rights.md&3.1&## How to deal with an applied question
ct17-Utilitarianism-Animal-Rights.md&3.1&
ct17-Utilitarianism-Animal-Rights.md&3.1&*Not necessarily in that order:*
ct17-Utilitarianism-Animal-Rights.md&3.1&
ct17-Utilitarianism-Animal-Rights.md&3.1&- Understand the problem
ct17-Utilitarianism-Animal-Rights.md&3.1&- Identify the stakeholders
ct17-Utilitarianism-Animal-Rights.md&3.1&- Make relevant distinctions and clarify the issues
ct17-Utilitarianism-Animal-Rights.md&3.1&- Find and quote relevant theory (from research)
ct17-Utilitarianism-Animal-Rights.md&3.1&- Find the main arguments for each side (from research)
ct17-Utilitarianism-Animal-Rights.md&3.1&- Find places where more research (facts) is needed
ct17-Utilitarianism-Animal-Rights.md&3.1&- Identify benefits and costs and weigh them against each other
ct17-Utilitarianism-Animal-Rights.md&3.1&- Avoid yes/no answers. Try to see the complexities of the problem
ct17-Utilitarianism-Animal-Rights.md&3.1&- Try to find original alternatives that solve the problem better than just saying that one side is right (for example: meat eating might be morally right if animals are treated well, if we reduce the amount of meat consumed, if we eat only wild animals or insects etc)
ct17-Utilitarianism-Animal-Rights.md&3.1&
ct17-Utilitarianism-Animal-Rights.md&3.2&## Rachels on animal rights: I 7.4, II Ch. 14-16
ct17-Utilitarianism-Animal-Rights.md&3.2&
ct17-Utilitarianism-Animal-Rights.md&3.3&## Understand the problem
ct17-Utilitarianism-Animal-Rights.md&3.3&
ct17-Utilitarianism-Animal-Rights.md&3.3&Animal rights:
ct17-Utilitarianism-Animal-Rights.md&3.3&
ct17-Utilitarianism-Animal-Rights.md&3.3&- Which rights? (not: voting, freedom of religion, freedom of speech; but: right to live in a natural way, to mate, to have children, to seek happiness in a way appropriate to that animal, to avoid torture and pain)
ct17-Utilitarianism-Animal-Rights.md&3.3&- Which animals? (higher/lower? harmful/harmless? pain perceiving/not pain perceiving? Endangered?)
ct17-Utilitarianism-Animal-Rights.md&3.3&
ct17-Utilitarianism-Animal-Rights.md&3.3&. . .
ct17-Utilitarianism-Animal-Rights.md&3.3&
ct17-Utilitarianism-Animal-Rights.md&3.3&Further questions:
ct17-Utilitarianism-Animal-Rights.md&3.3&
ct17-Utilitarianism-Animal-Rights.md&3.3&- If I'm attacked by a shark, I can defend myself by harming the shark. Does this give me the right to kill or torture all sharks in the ocean? (No, my response should be limited to preventing actual harm to myself!)
ct17-Utilitarianism-Animal-Rights.md&3.3&
ct17-Utilitarianism-Animal-Rights.md&3.4&## What is a right? (1)
ct17-Utilitarianism-Animal-Rights.md&3.4&
ct17-Utilitarianism-Animal-Rights.md&3.4&*In utilitarianism, one would be granted a right if and only if having this right would increase the happiness of all.*
ct17-Utilitarianism-Animal-Rights.md&3.4&
ct17-Utilitarianism-Animal-Rights.md&3.4&We can ask:
ct17-Utilitarianism-Animal-Rights.md&3.4&
ct17-Utilitarianism-Animal-Rights.md&3.4&- Is this the case for (some? all?) animal rights?
ct17-Utilitarianism-Animal-Rights.md&3.4&- Does the happiness of "all" include animals or do we look only at human happiness?
ct17-Utilitarianism-Animal-Rights.md&3.4&
ct17-Utilitarianism-Animal-Rights.md&3.5&### Mill:
ct17-Utilitarianism-Animal-Rights.md&3.5&"To have a right, then, is, I conceive, to have something which society ought to defend me in the possession of. If the objector goes on to ask, why it ought? I can give him no other reason than general utility."
ct17-Utilitarianism-Animal-Rights.md&3.5&
ct17-Utilitarianism-Animal-Rights.md&3.6&## What is a right? (2)
ct17-Utilitarianism-Animal-Rights.md&3.6&
ct17-Utilitarianism-Animal-Rights.md&3.6&This way of explaining rights is great, because we can easily apply it to any creature, and we will get a sensible estimate of its possible relevant rights:
ct17-Utilitarianism-Animal-Rights.md&3.6&
ct17-Utilitarianism-Animal-Rights.md&3.6&- Humans have the right to vote and to free speech because taking away these rights would reduce general utility.
ct17-Utilitarianism-Animal-Rights.md&3.6&- Pigs don’t need the right to vote, because taking away that right would *not* reduce utility for the pigs.
ct17-Utilitarianism-Animal-Rights.md&3.6&- On the other hand, pigs *should* have the right to live in a natural environment, because being raised in a cage (like in a meat factory) likely reduces the pigs’ happiness.
ct17-Utilitarianism-Animal-Rights.md&3.6&- Mosquitoes don’t need any rights, because they don’t show any indication of experiencing happiness at all.
ct17-Utilitarianism-Animal-Rights.md&3.6&
ct17-Utilitarianism-Animal-Rights.md&3.6&
ct17-Utilitarianism-Animal-Rights.md&3.7&## Identify the stakeholders
ct17-Utilitarianism-Animal-Rights.md&3.7&
ct17-Utilitarianism-Animal-Rights.md&3.7&The stakeholders are those who are (strongly) affected by that moral problem.
ct17-Utilitarianism-Animal-Rights.md&3.7&
ct17-Utilitarianism-Animal-Rights.md&3.7&- Animals
ct17-Utilitarianism-Animal-Rights.md&3.7&- The government (has to pass and enforce laws and regulations, has to finance infrastructure etc)
ct17-Utilitarianism-Animal-Rights.md&3.7&- Animal farmers
ct17-Utilitarianism-Animal-Rights.md&3.7&- Meat consumers
ct17-Utilitarianism-Animal-Rights.md&3.7&- Scientists
ct17-Utilitarianism-Animal-Rights.md&3.7&- Consumers of cosmetics
ct17-Utilitarianism-Animal-Rights.md&3.7&- Producers of cosmetics
ct17-Utilitarianism-Animal-Rights.md&3.7&- Doctors and patients waiting for medical research results
ct17-Utilitarianism-Animal-Rights.md&3.7&- Medicine and biology students
ct17-Utilitarianism-Animal-Rights.md&3.7&
ct17-Utilitarianism-Animal-Rights.md&3.8&## Stakeholder errors (1)
ct17-Utilitarianism-Animal-Rights.md&3.8&
ct17-Utilitarianism-Animal-Rights.md&3.8&- Be careful with the stakeholders!
ct17-Utilitarianism-Animal-Rights.md&3.8&- A common mistake is to see them as distinct groups, and to weigh them against each other!
ct17-Utilitarianism-Animal-Rights.md&3.8&
ct17-Utilitarianism-Animal-Rights.md&3.8&. . .
ct17-Utilitarianism-Animal-Rights.md&3.8&
ct17-Utilitarianism-Animal-Rights.md&3.8&For example:
ct17-Utilitarianism-Animal-Rights.md&3.8&
ct17-Utilitarianism-Animal-Rights.md&3.8&- There are 1000 animal farmers.
ct17-Utilitarianism-Animal-Rights.md&3.8&- There are one million meat consumers.
ct17-Utilitarianism-Animal-Rights.md&3.8&- Therefore, meat eating is good for utilitarianism, and animal suffering has to be tolerated.
ct17-Utilitarianism-Animal-Rights.md&3.8&
ct17-Utilitarianism-Animal-Rights.md&3.8&. . .
ct17-Utilitarianism-Animal-Rights.md&3.8&
ct17-Utilitarianism-Animal-Rights.md&3.8&There are multiple mistakes in this thought (for example it ignores the animals), but one often overlooked is that the animal farmers and the meat eaters probably share views about cruelty to animals. Most meat eaters don't want animals to suffer either!
ct17-Utilitarianism-Animal-Rights.md&3.8&
ct17-Utilitarianism-Animal-Rights.md&3.9&## Stakeholder errors (2)
ct17-Utilitarianism-Animal-Rights.md&3.9&
ct17-Utilitarianism-Animal-Rights.md&3.9&The same is true of many other areas.
ct17-Utilitarianism-Animal-Rights.md&3.9&
ct17-Utilitarianism-Animal-Rights.md&3.9&- Car drivers don't want the environment to be destroyed. They are also interested in clean air.
ct17-Utilitarianism-Animal-Rights.md&3.9&- Even criminals have an interest in a safe society (from which they profit themselves as citizens)
ct17-Utilitarianism-Animal-Rights.md&3.9&- Even people who are in favour of euthanasia don't want euthanasia to be abused.
ct17-Utilitarianism-Animal-Rights.md&3.9&- People who are in favour of abortion still love babies and are not interested in devaluing human life.
ct17-Utilitarianism-Animal-Rights.md&3.9&
ct17-Utilitarianism-Animal-Rights.md&3.10&## Stakeholder errors (3)
ct17-Utilitarianism-Animal-Rights.md&3.10&
ct17-Utilitarianism-Animal-Rights.md&3.10&*So you have to be aware that your stakeholder divisions are useful in order to understand the problem, but they cannot be used in order to estimate the size of population groups with opposite moral views! Different stakeholders don't necessarily have opposite moral views!*
ct17-Utilitarianism-Animal-Rights.md&3.10&
ct17-Utilitarianism-Animal-Rights.md&3.11&## Stakeholder errors (4)
ct17-Utilitarianism-Animal-Rights.md&3.11&
ct17-Utilitarianism-Animal-Rights.md&3.11&Another mistake is to count everyone who might be involved in a case as a stakeholder. This is wrong.
ct17-Utilitarianism-Animal-Rights.md&3.11&
ct17-Utilitarianism-Animal-Rights.md&3.11&A stakeholder is someone whose *interests* are *significantly* affected by that moral issue.
ct17-Utilitarianism-Animal-Rights.md&3.11&
ct17-Utilitarianism-Animal-Rights.md&3.12&## Stakeholder errors (5)
ct17-Utilitarianism-Animal-Rights.md&3.12&
ct17-Utilitarianism-Animal-Rights.md&3.12&For example, if you bribe your teacher to get a good grade, the stakeholders would be:
ct17-Utilitarianism-Animal-Rights.md&3.12&
ct17-Utilitarianism-Animal-Rights.md&3.12&- Your teacher
ct17-Utilitarianism-Animal-Rights.md&3.12&- You
ct17-Utilitarianism-Animal-Rights.md&3.12&- Perhaps the university, because if this case became known, then the reputation of the university would suffer.
ct17-Utilitarianism-Animal-Rights.md&3.12&
ct17-Utilitarianism-Animal-Rights.md&3.12&. . .
ct17-Utilitarianism-Animal-Rights.md&3.12&
ct17-Utilitarianism-Animal-Rights.md&3.12&On the other hand, the police and the courts, although they would definitely be involved in the case itself, are *not* stakeholders. They have no own interests in how this case plays out. Whether you bribe someone or not, and whether this becomes public or not, and whether you get a particular grade as a result of that or not, does not *affect the police's or the court's own interests.* Therefore, they are not stakeholders.
ct17-Utilitarianism-Animal-Rights.md&3.12&
ct17-Utilitarianism-Animal-Rights.md&3.12&
ct17-Utilitarianism-Animal-Rights.md&3.13&## Theory: Animals don't need rights
ct17-Utilitarianism-Animal-Rights.md&3.13&
ct17-Utilitarianism-Animal-Rights.md&3.13&Possible reasons to ignore animal rights:
ct17-Utilitarianism-Animal-Rights.md&3.13&
ct17-Utilitarianism-Animal-Rights.md&3.13&1. Human beings have superior value. Animals just don't count. We can do with them what we want.
ct17-Utilitarianism-Animal-Rights.md&3.13&    - Supporting theory? (Christian Natural Law theory, Thomas Aquinas)
ct17-Utilitarianism-Animal-Rights.md&3.13&    - Problems? (Not convincing to non-Christians)
ct17-Utilitarianism-Animal-Rights.md&3.13&2. Social Darwinism.
ct17-Utilitarianism-Animal-Rights.md&3.13&
ct17-Utilitarianism-Animal-Rights.md&3.14&### Thomas Aquinas (1225-1274)
ct17-Utilitarianism-Animal-Rights.md&3.14&
ct17-Utilitarianism-Animal-Rights.md&3.14&"[Animals] by the divine providence they are intended for man's use in the natural order. Hence it is not wrong for man to make use of them, either by killing them or in any other way whatever."
ct17-Utilitarianism-Animal-Rights.md&3.14&
ct17-Utilitarianism-Animal-Rights.md&3.15&## Is-Ought-Fallacy
ct17-Utilitarianism-Animal-Rights.md&3.15&
ct17-Utilitarianism-Animal-Rights.md&3.15&>- If we subscribe to Darwinism, we might say: “Animals don’t need rights, because in nature they don’t have rights either. Animals kill and eat other animals all the time. So we don’t need to protect animals’ rights.”
ct17-Utilitarianism-Animal-Rights.md&3.15&>- Is this a good argument?
ct17-Utilitarianism-Animal-Rights.md&3.15&>- No. This is called the Is-Ought-Fallacy. 
ct17-Utilitarianism-Animal-Rights.md&3.15&>     - From the fact that something *is* the case, I cannot conclude that it is morally right (that it *ought* to be so). 
ct17-Utilitarianism-Animal-Rights.md&3.15&>     - For example, there are criminals in the world. From this I cannot conclude that it is morally right that there are criminals in the world!
ct17-Utilitarianism-Animal-Rights.md&3.15&>- This is a counterargument to social Darwinism: the idea, that it is right that a society is organised like nature is, and that it is good that the strong kill the weak.
ct17-Utilitarianism-Animal-Rights.md&3.15&
ct17-Utilitarianism-Animal-Rights.md&3.16&## Theory: Other reasons to ignore animal rights
ct17-Utilitarianism-Animal-Rights.md&3.16&
ct17-Utilitarianism-Animal-Rights.md&3.16&- Animals are not rational
ct17-Utilitarianism-Animal-Rights.md&3.16&- Animals don't speak
ct17-Utilitarianism-Animal-Rights.md&3.16&- Animals are not human
ct17-Utilitarianism-Animal-Rights.md&3.16&- Animals don't have autonomy (Kant: "Autonomy" is the ability of someone to freely make their own decisions and to act accordingly^[see also Rachels, p.3 for a similar definition]). Animals are driven by instinct and not free to decide how they will act
ct17-Utilitarianism-Animal-Rights.md&3.16&
ct17-Utilitarianism-Animal-Rights.md&3.17&## Counter-arguments to ignoring animal rights
ct17-Utilitarianism-Animal-Rights.md&3.17&
ct17-Utilitarianism-Animal-Rights.md&3.17&- Some humans are not rational either (babies)
ct17-Utilitarianism-Animal-Rights.md&3.17&- Babies don't speak. Mute people don't speak
ct17-Utilitarianism-Animal-Rights.md&3.17&- Some animals do "speak" in a limited way (whales? bees!) -- Research!
ct17-Utilitarianism-Animal-Rights.md&3.17&- Why should "being human" be important for utilitarianism? (Peter Singer: "speciecism"!)
ct17-Utilitarianism-Animal-Rights.md&3.17&- Why should autonomy be a requirement for having rights? (Needs at least to be explained further! -- Research!)
ct17-Utilitarianism-Animal-Rights.md&3.17&
ct17-Utilitarianism-Animal-Rights.md&3.18&## Theory: Utilitarianism
ct17-Utilitarianism-Animal-Rights.md&3.18&
ct17-Utilitarianism-Animal-Rights.md&3.18&The only thing that counts is the balance of pleasure and pain.
ct17-Utilitarianism-Animal-Rights.md&3.18&
ct17-Utilitarianism-Animal-Rights.md&3.18&- Can an animal feel pleasure?
ct17-Utilitarianism-Animal-Rights.md&3.18&- Can it feel pain?
ct17-Utilitarianism-Animal-Rights.md&3.18&
ct17-Utilitarianism-Animal-Rights.md&3.19&### Bentham:
ct17-Utilitarianism-Animal-Rights.md&3.19&
ct17-Utilitarianism-Animal-Rights.md&3.19&"A full-grown horse or a dog is ... a more rational animal than a (human) infant of a day or a week ... old. ... The question is not, can [animals] *reason?* Can they *talk?* But: can they *suffer?*"
ct17-Utilitarianism-Animal-Rights.md&3.19&
ct17-Utilitarianism-Animal-Rights.md&3.20&## Distinctions: Kinds of animals
ct17-Utilitarianism-Animal-Rights.md&3.20&
ct17-Utilitarianism-Animal-Rights.md&3.20&- We have to distinguish between different *kinds* of animals!
ct17-Utilitarianism-Animal-Rights.md&3.20&- Can all animals "suffer" equally?
ct17-Utilitarianism-Animal-Rights.md&3.20&- What makes an animal "suffer"? (imprisonment, lack of company, physical pain, mistreatment, fear?)
ct17-Utilitarianism-Animal-Rights.md&3.20&- Do all animals experience pain? Do they have the neural infrastructure required?
ct17-Utilitarianism-Animal-Rights.md&3.20&
ct17-Utilitarianism-Animal-Rights.md&3.20&. . .
ct17-Utilitarianism-Animal-Rights.md&3.20&
ct17-Utilitarianism-Animal-Rights.md&3.20&What abilities does the animal (not) have that are relevant?
ct17-Utilitarianism-Animal-Rights.md&3.20&
ct17-Utilitarianism-Animal-Rights.md&3.20&- Anticipate future events
ct17-Utilitarianism-Animal-Rights.md&3.20&- Understand intentions
ct17-Utilitarianism-Animal-Rights.md&3.20&- Value its own future (animals care only about the present)
ct17-Utilitarianism-Animal-Rights.md&3.20&
ct17-Utilitarianism-Animal-Rights.md&3.21&## Are animals scared of death?
ct17-Utilitarianism-Animal-Rights.md&3.21&
ct17-Utilitarianism-Animal-Rights.md&3.21&- Why are humans scared of death?
ct17-Utilitarianism-Animal-Rights.md&3.21&- If person X is told that he is going to be killed tomorrow morning, *why exactly* would this make him unhappy?
ct17-Utilitarianism-Animal-Rights.md&3.21&	 - Pain in the process of dying
ct17-Utilitarianism-Animal-Rights.md&3.21&     - Loss of future happiness, cutting off of future plans
ct17-Utilitarianism-Animal-Rights.md&3.21&	 - We don’t want our life or death to be decided by others
ct17-Utilitarianism-Animal-Rights.md&3.21&	 - Fear of what happens *after* death (hell etc)
ct17-Utilitarianism-Animal-Rights.md&3.21&	 - One’s death might make others unhappy or cause pain to others
ct17-Utilitarianism-Animal-Rights.md&3.21&- Is this the same with animals?
ct17-Utilitarianism-Animal-Rights.md&3.21&     - One might argue that only the pain is relevant to animals’ experience of death.
ct17-Utilitarianism-Animal-Rights.md&3.21&- If not, then perhaps death itself is not a negative experience for an animal. It is the *circumstances* of death that are unpleasant. (This is also a factor of human fear of death).
ct17-Utilitarianism-Animal-Rights.md&3.21&
ct17-Utilitarianism-Animal-Rights.md&3.22&## Weighing happiness against pain (1)
ct17-Utilitarianism-Animal-Rights.md&3.22&
ct17-Utilitarianism-Animal-Rights.md&3.22&- Human gain in happiness from meat eating
ct17-Utilitarianism-Animal-Rights.md&3.22&
ct17-Utilitarianism-Animal-Rights.md&3.22&. . .
ct17-Utilitarianism-Animal-Rights.md&3.22&
ct17-Utilitarianism-Animal-Rights.md&3.22&- Pain for pigs from being held in life-long captivity under terrible conditions
ct17-Utilitarianism-Animal-Rights.md&3.22&- Pain for pigs from being unable to procreate, hunt, or follow any of their natural instincts
ct17-Utilitarianism-Animal-Rights.md&3.22&- Fear induced by watching other pigs die in front of them
ct17-Utilitarianism-Animal-Rights.md&3.22&- Painful method of killing?
ct17-Utilitarianism-Animal-Rights.md&3.22&
ct17-Utilitarianism-Animal-Rights.md&3.23&## Weighing happiness against pain (2)
ct17-Utilitarianism-Animal-Rights.md&3.23&
ct17-Utilitarianism-Animal-Rights.md&3.23&Since twe are weighing two sides against each other, we could either stop eating meat, or try to remove the pain for the animals. Note: all these causes of animal pain *could,* in principle, be removed!
ct17-Utilitarianism-Animal-Rights.md&3.23&
ct17-Utilitarianism-Animal-Rights.md&3.23&- But meat then would become very expensive, and much less would be eaten.
ct17-Utilitarianism-Animal-Rights.md&3.23&- Both ecologically and from a health perspective this would probably be good.
ct17-Utilitarianism-Animal-Rights.md&3.23&- Additional reasons to not eat meat.
ct17-Utilitarianism-Animal-Rights.md&3.23&
ct17-Utilitarianism-Animal-Rights.md&3.24&## Weighing happiness against pain (3)
ct17-Utilitarianism-Animal-Rights.md&3.24&
ct17-Utilitarianism-Animal-Rights.md&3.24&- The weighing would turn out differently if we talk about medical uses of animals, cosmetics, etc.
ct17-Utilitarianism-Animal-Rights.md&3.24&- Even medical uses can evaluate differently, depending on circumstances.
ct17-Utilitarianism-Animal-Rights.md&3.24&
ct17-Utilitarianism-Animal-Rights.md&3.25&## Distinctions: Importance of animal uses
ct17-Utilitarianism-Animal-Rights.md&3.25&
ct17-Utilitarianism-Animal-Rights.md&3.25&Are all animal uses of equal importance?
ct17-Utilitarianism-Animal-Rights.md&3.25&
ct17-Utilitarianism-Animal-Rights.md&3.25&- Medical experiments for life-saving drugs
ct17-Utilitarianism-Animal-Rights.md&3.25&- Experiments for development of cosmetics
ct17-Utilitarianism-Animal-Rights.md&3.25&- Eating meat
ct17-Utilitarianism-Animal-Rights.md&3.25&- Ocean Park, zoos, circus?
ct17-Utilitarianism-Animal-Rights.md&3.25&
ct17-Utilitarianism-Animal-Rights.md&3.26&## Alternatives: Ways to avoid animal suffering
ct17-Utilitarianism-Animal-Rights.md&3.26&
ct17-Utilitarianism-Animal-Rights.md&3.26&Are there alternatives to animal suffering?
ct17-Utilitarianism-Animal-Rights.md&3.26&
ct17-Utilitarianism-Animal-Rights.md&3.26&Can we reconcile the interests of humans and animals?
ct17-Utilitarianism-Animal-Rights.md&3.26&
ct17-Utilitarianism-Animal-Rights.md&3.26&- Lab tests on cell cultures
ct17-Utilitarianism-Animal-Rights.md&3.26&- Use of computer simulation for anatomy teaching
ct17-Utilitarianism-Animal-Rights.md&3.26&- Use of techniques to reduce suffering (pain killers)
ct17-Utilitarianism-Animal-Rights.md&3.26&- Production of meat that reduces suffering (natural farming)
ct17-Utilitarianism-Animal-Rights.md&3.26&
ct17-Utilitarianism-Animal-Rights.md&3.27&## Conclusion: Animal vs human interests in utilitarianism
ct17-Utilitarianism-Animal-Rights.md&3.27&
ct17-Utilitarianism-Animal-Rights.md&3.27&- We have to weigh animal interests against human gain
ct17-Utilitarianism-Animal-Rights.md&3.27&- We cannot put either side's interests absolutely above the other's
ct17-Utilitarianism-Animal-Rights.md&3.27&- (Some) animals deserve serious consideration, but not necessarily absolute protection (if the human benefit is much higher than the animal's pain)
ct17-Utilitarianism-Animal-Rights.md&3.27&
ct17-Utilitarianism-Animal-Rights.md&3.28&## Animals rights (graphics)
ct17-Utilitarianism-Animal-Rights.md&3.28&
ct17-Utilitarianism-Animal-Rights.md&3.28&![](ShouldAnimalsHaveRights.png)
ct17-Utilitarianism-Animal-Rights.md&3.28&
ct17-Utilitarianism-Animal-Rights.md&3.29&## Conclusion about creating your arguments / writing a paper
ct17-Utilitarianism-Animal-Rights.md&3.29&
ct17-Utilitarianism-Animal-Rights.md&3.29&- Think through the different aspects of the problem
ct17-Utilitarianism-Animal-Rights.md&3.29&- Show that you thought about the different aspects, but then pick a small part of the question that you can answer well for the main part of your argument or paper
ct17-Utilitarianism-Animal-Rights.md&3.29&- Avoid yes/no answers. Appreciate the complexity of the problem
ct17-Utilitarianism-Animal-Rights.md&3.29&- Do your research, both about theory and about facts
ct17-Utilitarianism-Animal-Rights.md&3.29&- Use your research, and quote it correctly if you write a paper
ct17-Utilitarianism-Animal-Rights.md&3.29&- Try to find original solutions to the problem that go beyond saying that one side is right or wrong
ct17-Utilitarianism-Animal-Rights.md&3.29&
ct17-Utilitarianism-Animal-Rights.md&3.29&
ct17-Utilitarianism-Animal-Rights.md&3.30&## Any questions?
ct17-Utilitarianism-Animal-Rights.md&3.30&
ct17-Utilitarianism-Animal-Rights.md&3.30&![](graphics/questions.jpg)\ 
ct17-Utilitarianism-Animal-Rights.md&3.30&
ct17-Utilitarianism-Animal-Rights.md&3.31&## Thank you for your attention!
ct17-Utilitarianism-Animal-Rights.md&3.31&
ct17-Utilitarianism-Animal-Rights.md&3.31&Email: <matthias@ln.edu.hk>
ct17-Utilitarianism-Animal-Rights.md&3.31&
ct17-Utilitarianism-Animal-Rights.md&3.31&If you have questions, please come to my office hours (see course outline).
ct18-Moral-thories-1.md&0.1&% Critical Thinking
ct18-Moral-thories-1.md&0.1&% 18. Moral theories (1)
ct18-Moral-thories-1.md&0.1&% A. Matthias
ct18-Moral-thories-1.md&0.1&
ct18-Moral-thories-1.md&1.0&# Where we are
ct18-Moral-thories-1.md&1.0&
ct18-Moral-thories-1.md&1.1&## What did we learn last time?
ct18-Moral-thories-1.md&1.1&
ct18-Moral-thories-1.md&1.1&- Utilitarianism and animal rights
ct18-Moral-thories-1.md&1.1&
ct18-Moral-thories-1.md&1.2&## What are we going to learn today?
ct18-Moral-thories-1.md&1.2&
ct18-Moral-thories-1.md&1.2&- Introduction to ethical decision making
ct18-Moral-thories-1.md&1.2&
ct18-Moral-thories-1.md&1.3&## Source
ct18-Moral-thories-1.md&1.3&
ct18-Moral-thories-1.md&1.3&- This session is based on material from the Markkula Center for Applied Ethics at the University of Santa Clara, available online.
ct18-Moral-thories-1.md&1.3&
ct18-Moral-thories-1.md&2.0&# A Framework for Ethical Decision Making
ct18-Moral-thories-1.md&2.0&
ct18-Moral-thories-1.md&2.1&## Recognise an Ethical Issue (1)
ct18-Moral-thories-1.md&2.1&
ct18-Moral-thories-1.md&2.1&>- Could this decision or situation be damaging to someone or to some group? Does this decision involve a choice between a good and bad alternative, or perhaps between two "goods" or between two "bads"?
ct18-Moral-thories-1.md&2.1&>     - Moral decisions usually involve dilemmas where the right course of action is not immediately clear. 
ct18-Moral-thories-1.md&2.1&>     - Often harming other’s interests is a sign that this is a moral issue.
ct18-Moral-thories-1.md&2.1&
ct18-Moral-thories-1.md&2.2&## Recognise an Ethical Issue (2)
ct18-Moral-thories-1.md&2.2&
ct18-Moral-thories-1.md&2.2&>- Is this issue about more than what is legal or what is most efficient? If so, how?
ct18-Moral-thories-1.md&2.2&>     - Is the law sufficient to deal with all moral issues? If so, do we need morality at all? Can we not just do what the law says?
ct18-Moral-thories-1.md&2.2&>     - We can easily name situations where laws are immoral (for example, laws about the treatment of Jews in Nazi Germany); and also cases where morally right actions are illegal (crossing a red traffic light in order to save a child on the other side of the road).
ct18-Moral-thories-1.md&2.2&>     - Therefore, the law is not a reliable indicator for the morality of an action. 
ct18-Moral-thories-1.md&2.2&>     - Laws can be morally wrong themselves.
ct18-Moral-thories-1.md&2.2&
ct18-Moral-thories-1.md&2.2&
ct18-Moral-thories-1.md&2.3&## Get the Facts (1)
ct18-Moral-thories-1.md&2.3&
ct18-Moral-thories-1.md&2.3&>- What are the relevant facts of the case? What facts are not known? Can I learn more about the situation? Do I know enough to make a decision?
ct18-Moral-thories-1.md&2.3&>     - Often we need some research so that we can judge a moral issue.
ct18-Moral-thories-1.md&2.3&>     - For example, animal rights: do animals feel pain? Which animals are conscious? And so on.
ct18-Moral-thories-1.md&2.3&>     - Or: Abortion: When do embryos start to become conscious? Do they feel pain? Are they human or just organs? And on on.
ct18-Moral-thories-1.md&2.3&
ct18-Moral-thories-1.md&2.4&## Get the Facts (2)
ct18-Moral-thories-1.md&2.4&
ct18-Moral-thories-1.md&2.4&>- What individuals and groups have an important stake in the outcome? Are some concerns more important than others? Why?
ct18-Moral-thories-1.md&2.4&>     - We need to identify the stakeholders.
ct18-Moral-thories-1.md&2.4&>     - Often, stakeholders are overlooked.
ct18-Moral-thories-1.md&2.4&>     - For example, animal rights: Animals, farmers, meat eaters, supermarket chains, other employees in the meat business; but also doctors, researchers, patients, pharmaceutical and cosmetics companies and their employees, and so on.
ct18-Moral-thories-1.md&2.4&>     - A stakeholder is someone whose interests will be *significantly affected* by a moral decision.
ct18-Moral-thories-1.md&2.4&>     - For example, a policeman or a judge are *not* stakeholders in a theft case!
ct18-Moral-thories-1.md&2.4&>     - But *all citizens* are stakeholders in environmental pollution cases. Not only the environmental activists!
ct18-Moral-thories-1.md&2.4&
ct18-Moral-thories-1.md&2.5&## Get the Facts (3)
ct18-Moral-thories-1.md&2.5&
ct18-Moral-thories-1.md&2.5&>- What are the options for acting? Have all the relevant persons and groups been consulted? Have I identified creative options?
ct18-Moral-thories-1.md&2.5&>     - Consider all relevant groups (see above: stakeholders).
ct18-Moral-thories-1.md&2.5&>     - Creative options: Not all moral issues need to be decided in a binary way (right/wrong).
ct18-Moral-thories-1.md&2.5&>     - Often, the best solution is a compromise.
ct18-Moral-thories-1.md&2.5&>     - Eat some meat, but not too much, and make sure it’s not gained through animal suffering.
ct18-Moral-thories-1.md&2.5&>     - Use animals for experiments, but only in very limited ways and only for life-saving drugs, not cosmetics.
ct18-Moral-thories-1.md&2.5&>     - Instead of legalising or outlawing abortion, make it easier for mothers to give their children away for adoption by couples who want, but cannot have children.
ct18-Moral-thories-1.md&2.5&
ct18-Moral-thories-1.md&2.6&## Evaluate Alternative Actions (1)
ct18-Moral-thories-1.md&2.6&
ct18-Moral-thories-1.md&2.6&>- Evaluate the options by asking the following questions:
ct18-Moral-thories-1.md&2.6&>- Which option will produce the most good and do the least harm? (The Utilitarian Approach).
ct18-Moral-thories-1.md&2.6&
ct18-Moral-thories-1.md&2.6&
ct18-Moral-thories-1.md&2.7&## Evaluate Alternative Actions (2)
ct18-Moral-thories-1.md&2.7&
ct18-Moral-thories-1.md&2.7&>- Which option best respects the rights of all who have a stake? (The Rights Approach).
ct18-Moral-thories-1.md&2.7&>     - Would we want this right respected if we were in that person's position?
ct18-Moral-thories-1.md&2.7&>     - Exceptions: Morality must be the same for all. "What if everyone did it?" 
ct18-Moral-thories-1.md&2.7&>     - Would the action become impossible or the social/business environment unacceptable?
ct18-Moral-thories-1.md&2.7&>     - Is this particular situation a valid exception to an, otherwise valid, moral rule? How do you justify the exception? Would a stranger agree that you are exceptional in this case?
ct18-Moral-thories-1.md&2.7&>- Example: Illegally copying music from the Internet. What if the copier is a music journalist who will publish an article in a big, national newspaper about this piece of music? What if the copier is just a normal student?
ct18-Moral-thories-1.md&2.7&
ct18-Moral-thories-1.md&2.8&## Evaluate Alternative Actions (3)
ct18-Moral-thories-1.md&2.8&
ct18-Moral-thories-1.md&2.8&>- Choices: "Are the people affected able to make their own choices?"
ct18-Moral-thories-1.md&2.8&>     - According to Kant, we need to think of others as “ends,” that means, of human beings who are free and infinitely valuable. They should have the freedom to make their own choices and to decide what is valuable to them.
ct18-Moral-thories-1.md&2.8&>     - Meaning of means/ends (forgotten pen, taxi).
ct18-Moral-thories-1.md&2.8&>     - The “end” is the goal of my action, what I want to achieve.
ct18-Moral-thories-1.md&2.8&>     - The “means” are the tools, methods, ways I use in order to achieve my ends.
ct18-Moral-thories-1.md&2.8&>     - Examples of treating others as “means” or “ends”? (restaurant cook, taxi driver)
ct18-Moral-thories-1.md&2.8&>     - It is not wrong to treat others as means as long as we also treat them as ends! (see above: own choices!)
ct18-Moral-thories-1.md&2.8&
ct18-Moral-thories-1.md&2.8&
ct18-Moral-thories-1.md&2.9&## Evaluate Alternative Actions (4)
ct18-Moral-thories-1.md&2.9&
ct18-Moral-thories-1.md&2.9&>- Which option treats people equally or proportionately? (The Justice Approach).
ct18-Moral-thories-1.md&2.9&>     - Kant again: All people are equal. They should have equal rights.
ct18-Moral-thories-1.md&2.9&>     - But sometimes rights should not be equal, but proportional to people’s needs or abilities.
ct18-Moral-thories-1.md&2.9&>     - For example, telling a wheelchair user to use the stairs like everyone else is not fair, although it would be “equal” treatment.
ct18-Moral-thories-1.md&2.9&>     - Also, giving all students an “A” regardless of performance, would be unfair. 
ct18-Moral-thories-1.md&2.9&>     - Here, the treatment has to be proportional to needs or performance.
ct18-Moral-thories-1.md&2.9&>- Discrimination is a very complex issue.
ct18-Moral-thories-1.md&2.9&
ct18-Moral-thories-1.md&2.10&## Evaluate Alternative Actions (5)
ct18-Moral-thories-1.md&2.10&
ct18-Moral-thories-1.md&2.10&>- Which option best serves the community as a whole, not just some members? (The Common Good Approach)
ct18-Moral-thories-1.md&2.10&>     - This is utilitarian: The idea that the morally right action should benefit most people.
ct18-Moral-thories-1.md&2.10&>     - For example, electricity flatrates for a company. Morally right?
ct18-Moral-thories-1.md&2.10&>     - Only this company is benefited, but the environment (and the rest of society) are harmed by wasted energy (which causes pollution and uses up common resources).
ct18-Moral-thories-1.md&2.10&>     - Therefore, it is not morally right. The company should save energy instead of using flatrate contracts.
ct18-Moral-thories-1.md&2.10&
ct18-Moral-thories-1.md&2.11&## Evaluate Alternative Actions (6)
ct18-Moral-thories-1.md&2.11&
ct18-Moral-thories-1.md&2.11&>- Which option leads me to act as the sort of person or the kind of institution I/we want to be? (The Virtue Approach)
ct18-Moral-thories-1.md&2.11&>     - Some actions have an influence on our self-image.
ct18-Moral-thories-1.md&2.11&>     - For example, the University can give a particular vacant shop area to a fast food shop or a book shop. Which would be morally right? (Consider all factors mentioned above!)
ct18-Moral-thories-1.md&2.11&>     - The university is considering to relax cheating rules for students’ homework. Morally right?
ct18-Moral-thories-1.md&2.11&
ct18-Moral-thories-1.md&2.12&## Make a Decision and Test It (1)
ct18-Moral-thories-1.md&2.12&
ct18-Moral-thories-1.md&2.12&>- Here are all the approaches again:
ct18-Moral-thories-1.md&2.12&>     - Maximising benefits for most stakeholders
ct18-Moral-thories-1.md&2.12&>     - Respecting others’ rights
ct18-Moral-thories-1.md&2.12&>     - Protecting the ability of others to make their own choices
ct18-Moral-thories-1.md&2.12&>     - Justice, equal consideration, proportionate treatment
ct18-Moral-thories-1.md&2.12&>     - Maximising *common* good (as opposed to individual benefit)
ct18-Moral-thories-1.md&2.12&>     - Reflecting the right set of values in one’s actions
ct18-Moral-thories-1.md&2.12&>- Considering all these approaches, which option best addresses the situation?
ct18-Moral-thories-1.md&2.12&
ct18-Moral-thories-1.md&2.13&## Make a Decision and Test It (2)
ct18-Moral-thories-1.md&2.13&
ct18-Moral-thories-1.md&2.13&>- If I told someone I respect – or told a television audience – which option I have chosen, what would they say?
ct18-Moral-thories-1.md&2.13&>     - Often, we see the morality of our actions clearer if we try to see ourselves from the outside, from another person’s viewpoint.
ct18-Moral-thories-1.md&2.13&>     - If you wouldn’t justify doing something on TV, then perhaps something is wrong with it!
ct18-Moral-thories-1.md&2.13&
ct18-Moral-thories-1.md&2.14&## Act and Reflect on the Outcome
ct18-Moral-thories-1.md&2.14&
ct18-Moral-thories-1.md&2.14&>- How can my decision be implemented with the greatest care and attention to the concerns of all stakeholders?
ct18-Moral-thories-1.md&2.14&>     - Often, we can still try to consider the “losing” or “minority” party and make it easier for them to accept the outcome of our decision.
ct18-Moral-thories-1.md&2.14&>- How did my decision turn out and what have I learned from this specific situation?
ct18-Moral-thories-1.md&2.14&>     - Learn for future cases. In this way, we improve in our moral judgements and avoid making the same mistakes over and over again.
ct18-Moral-thories-1.md&2.14&
ct18-Moral-thories-1.md&2.14&
ct18-Moral-thories-1.md&2.15&## What did we learn today?
ct18-Moral-thories-1.md&2.15&
ct18-Moral-thories-1.md&2.15&- Introduction to ethical decision making
ct18-Moral-thories-1.md&2.15&
ct18-Moral-thories-1.md&2.15&
ct18-Moral-thories-1.md&2.16&## Any questions?
ct18-Moral-thories-1.md&2.16&
ct18-Moral-thories-1.md&2.16&![](graphics/questions.jpg)\ 
ct18-Moral-thories-1.md&2.16&
ct18-Moral-thories-1.md&2.17&## Thank you for your attention!
ct18-Moral-thories-1.md&2.17&
ct18-Moral-thories-1.md&2.17&Email: <matthias@ln.edu.hk>
ct18-Moral-thories-1.md&2.17&
ct18-Moral-thories-1.md&2.17&If you have questions, please come to my office hours (see course outline).
ct18-Moral-thories-1.md&2.17&
ct18-Moral-thories-1.md&2.17&
ct18-Moral-thories-1.md&2.17&
ct18-Moral-thories-1.md&2.17&
ct18-Moral-thories-1.md&2.17&
ct19-Global-Warming.md&0.1&% Critical Thinking
ct19-Global-Warming.md&0.1&% 19. Global warming and environmental justice
ct19-Global-Warming.md&0.1&% A. Matthias
ct19-Global-Warming.md&0.1&
ct19-Global-Warming.md&1.0&# The problem of global warming
ct19-Global-Warming.md&1.0&
ct19-Global-Warming.md&1.1&## Note
ct19-Global-Warming.md&1.1&
ct19-Global-Warming.md&1.1&*This presentation provides only a rough sketch of a few, selected points related to the ethics of global warming and climate change. The topic is huge, and such a short presentation cannot be exhaustive. You will find a lot more information on the Internet, in popular books, and in specialist literature.*
ct19-Global-Warming.md&1.1&
ct19-Global-Warming.md&1.2&## References
ct19-Global-Warming.md&1.2&
ct19-Global-Warming.md&1.2&Please read these sources as a preparation for the session. You can find them online at Google Scholar:
ct19-Global-Warming.md&1.2&
ct19-Global-Warming.md&1.2&- Moellendorf, D. (2012). Climate change and global justice. Wiley Interdisciplinary Reviews: Climate Change, 3(2), 131-143.
ct19-Global-Warming.md&1.2&- Peter Lee: Adaptation, Not Mitigation, The Realistic Climate Debate
ct19-Global-Warming.md&1.2&Date: 27/12/14 Peter Lee, The Australian. Online: https://www.thegwpf.com/peter-lee-adaptation-not-mitigation-the-realistic-climate-debate/
ct19-Global-Warming.md&1.2&- Hayes AW (2005). The precautionary principle. Arh Hig Rada Toksikol. 2005 Jun;56(2):161-6.
ct19-Global-Warming.md&1.2&
ct19-Global-Warming.md&1.2&
ct19-Global-Warming.md&1.3&## The problem of global warming
ct19-Global-Warming.md&1.3&
ct19-Global-Warming.md&1.3&>- How does it work?
ct19-Global-Warming.md&1.3&>- Some gases (CO2, methane) trap the heat of sunlight and don’t let it leave the atmosphere.
ct19-Global-Warming.md&1.3&>- As a result, the atmosphere heats up.
ct19-Global-Warming.md&1.3&>- This often produces secondary effects (polar ice melting, release of CO2 from permafrost areas, destruction of forests, higher frequency of fires, expansion of deserts) that cause even more CO2 to be released into the atmosphere.
ct19-Global-Warming.md&1.3&>- In this way, a positive feedback loop can accelerate the effects of global warming.
ct19-Global-Warming.md&1.3&
ct19-Global-Warming.md&1.4&## Causes and effects of global warming (Wikipedia)
ct19-Global-Warming.md&1.4&
ct19-Global-Warming.md&1.4&
ct19-Global-Warming.md&1.4&![](graphics/19-global-warming-causes-effects-wikipedia-200.png)\ 
ct19-Global-Warming.md&1.4&
ct19-Global-Warming.md&1.4&
ct19-Global-Warming.md&1.4&
ct19-Global-Warming.md&1.4&
ct19-Global-Warming.md&1.5&## Sources of emissions for USA (Source: climatecentral.org)
ct19-Global-Warming.md&1.5&
ct19-Global-Warming.md&1.5&
ct19-Global-Warming.md&1.5&![](graphics/19-greenhouse-gas-sources.jpg)\ 
ct19-Global-Warming.md&1.5&
ct19-Global-Warming.md&1.5&
ct19-Global-Warming.md&1.5&
ct19-Global-Warming.md&1.5&
ct19-Global-Warming.md&1.6&## Why the phenomenon is disputed (1)
ct19-Global-Warming.md&1.6&
ct19-Global-Warming.md&1.6&>- One of the problems with the global warming debate is that the processes involved are relatively slow and hard to observe, playing out on a timescale of decades or centuries.
ct19-Global-Warming.md&1.6&>- Over such periods, we don’t have reliable climate data, and it is also difficult to predict climate changes over such long periods. No one really knows how to do that.
ct19-Global-Warming.md&1.6&>- Even where data exist, stronger short-term effects can hide the more subtle long-term effects of climate change.
ct19-Global-Warming.md&1.6&
ct19-Global-Warming.md&1.6&
ct19-Global-Warming.md&1.7&## Why the phenomenon is disputed (2)
ct19-Global-Warming.md&1.7&
ct19-Global-Warming.md&1.7&>- For example, there have always been famines, floods, hot summers, cold summers, warm winters, snowy winters. These short-term variations are much more dramatic than the long-term rises of the average temperature that are in the order of 1-3 degrees Celsius.
ct19-Global-Warming.md&1.7&>- Because of these difficulties to clearly *see* the effects of global warming, many people don’t believe that the phenomenon even exists.
ct19-Global-Warming.md&1.7&>- Wishful thinking and economic profit additionally work against the necessary realisation that climate change is both real and threatening.
ct19-Global-Warming.md&1.7&>- Here, the *precautionary principle* can help.
ct19-Global-Warming.md&1.7&
ct19-Global-Warming.md&1.7&
ct19-Global-Warming.md&1.8&## Precautionary principle
ct19-Global-Warming.md&1.8&
ct19-Global-Warming.md&1.8&> “When an activity raises threats of harm to human health or the environment, precautionary measures should be taken even if some cause and effect relationships are not fully established scientifically. In this context, the proponent of an activity, rather than the public, should bear the burden of proof. The process of applying the Precautionary Principle must be open, informed and democratic and must include potentially affected parties. It must also involve an examination of the full range of alternatives, including no action.” (Hayes 2005).
ct19-Global-Warming.md&1.8&
ct19-Global-Warming.md&1.9&## The problem (1)
ct19-Global-Warming.md&1.9&
ct19-Global-Warming.md&1.9&Several competing elements of the carbon dioxide dilemma (Lee 2014):
ct19-Global-Warming.md&1.9&
ct19-Global-Warming.md&1.9&>- Greenhouse gas emissions caused by humans are at record levels;
ct19-Global-Warming.md&1.9&>- the climate is changing;
ct19-Global-Warming.md&1.9&>- the global economy wants to keep growing;
ct19-Global-Warming.md&1.9&>- populations demand higher living standards;
ct19-Global-Warming.md&1.9&>- wealth creation emits carbon dioxide;
ct19-Global-Warming.md&1.9&>- renewable energy sources require significant financial subsidies and reduce economic competitiveness against those who do not use them.
ct19-Global-Warming.md&1.9&
ct19-Global-Warming.md&1.10&## The problem (2)
ct19-Global-Warming.md&1.10&
ct19-Global-Warming.md&1.10&Further complications (Lee 2014):
ct19-Global-Warming.md&1.10&
ct19-Global-Warming.md&1.10&>- democratically elected leaders cannot sustain unpopular climate policies in the face of opposition from voters;
ct19-Global-Warming.md&1.10&>- wealthy countries use more energy than poor ones but the growth of energy use is greatest in developing countries;
ct19-Global-Warming.md&1.10&>- in 2010 one fifth of the global population was living on less than USD 1.25 per day (down from two fifths in 1990); 
ct19-Global-Warming.md&1.10&>- fossil fuels are much cheaper to use than renewables (not necessarily true any more -- a.m.);
ct19-Global-Warming.md&1.10&>- new technologies like fracking have increased the amount of potentially available oil and gas over the next century (but they come with their own ecological problems -- a.m.);
ct19-Global-Warming.md&1.10&>- many environmentalists and environmentalism groups, especially in Europe, are vociferously opposed to fracking regardless of any potential economic or energy security benefits.
ct19-Global-Warming.md&1.10&
ct19-Global-Warming.md&1.10&
ct19-Global-Warming.md&1.11&## The difficulty of monetising climate change
ct19-Global-Warming.md&1.11&
ct19-Global-Warming.md&1.11&>- Global effects.
ct19-Global-Warming.md&1.11&>- Many regions don’t have enough data.
ct19-Global-Warming.md&1.11&>- Some areas don’t even have monetised economies (north pole, Amazon rainforest)
ct19-Global-Warming.md&1.11&>- Diversity of effects (agriculture, fishing, forestry, tourism, cities, migration, ...)
ct19-Global-Warming.md&1.11&
ct19-Global-Warming.md&1.12&## Responsibility and moral issues
ct19-Global-Warming.md&1.12&
ct19-Global-Warming.md&1.12&>- Responsibility for past
ct19-Global-Warming.md&1.12&>- Allocating future emissions
ct19-Global-Warming.md&1.12&>- Ability to pay
ct19-Global-Warming.md&1.12&>- Grandfathering (acceptance of existing situation and past practices)
ct19-Global-Warming.md&1.12&>- Emissions entitlements
ct19-Global-Warming.md&1.12&>- Right to (sustainable) development
ct19-Global-Warming.md&1.12&>- Costs of adaptation and mitigation
ct19-Global-Warming.md&1.12&
ct19-Global-Warming.md&1.12&
ct19-Global-Warming.md&1.13&## Different kinds of countries (Pollution)
ct19-Global-Warming.md&1.13&
ct19-Global-Warming.md&1.13&>- The names of the countries given here are just for the purposes of giving commonly known, illustrating examples. They have not been researched further and might not actually fall into exactly the neat categories that we’re using for this discussion.
ct19-Global-Warming.md&1.13&>- Historically polluting, presently willing to reduce emissions (early industrialised and now wealthy countries like England, Germany, Denmark, ...)
ct19-Global-Warming.md&1.13&>- Historically polluting, presently also and unwilling to reduce (USA, Australia)
ct19-Global-Warming.md&1.13&>- Historically not polluting, but presently growing and overtaking industrialised countries (China, India)
ct19-Global-Warming.md&1.13&>- Neither historically nor presently significantly polluting (some small Pacific island nations)
ct19-Global-Warming.md&1.13&
ct19-Global-Warming.md&1.14&## Different kinds of countries (Wealth)
ct19-Global-Warming.md&1.14&
ct19-Global-Warming.md&1.14&>- Early industrialised, historically and presently wealthy countries (Western Europe, USA)
ct19-Global-Warming.md&1.14&>- Late industrialised, but wealthy countries (China)
ct19-Global-Warming.md&1.14&>- Late industrialised, historically poor and presently growing countries (India)
ct19-Global-Warming.md&1.14&>- Late or barely industrialised, historically and presently poor countries
ct19-Global-Warming.md&1.14&>- Not yet industrialised
ct19-Global-Warming.md&1.14&
ct19-Global-Warming.md&1.15&## Different kinds of countries (Effects)
ct19-Global-Warming.md&1.15&
ct19-Global-Warming.md&1.15&In the short term (effects due to failing agriculture, storms, desertification, fires and droughts, or flooding):
ct19-Global-Warming.md&1.15&
ct19-Global-Warming.md&1.15&>- Strongly affected countries (Pacific islands, Middle East)
ct19-Global-Warming.md&1.15&>- Somewhat affected countries (coastal areas, Australia)
ct19-Global-Warming.md&1.15&>- Not very much affected countries (North Europe, Scandinavia, Russia, Canada)
ct19-Global-Warming.md&1.15&
ct19-Global-Warming.md&1.16&## Different kinds of countries (Mitigation)
ct19-Global-Warming.md&1.16&
ct19-Global-Warming.md&1.16&>- Able to mitigate negative effects without external help
ct19-Global-Warming.md&1.16&>- Able to mitigate negative effects with external help
ct19-Global-Warming.md&1.16&>- Not able to mitigate negative effects (with catastrophic consequences)
ct19-Global-Warming.md&1.16&
ct19-Global-Warming.md&1.16&
ct19-Global-Warming.md&1.17&## How to measure pollution/CO2 emissions?
ct19-Global-Warming.md&1.17&
ct19-Global-Warming.md&1.17&>- Per capita? (USA worst)
ct19-Global-Warming.md&1.17&>- Total for a country? (China worst)
ct19-Global-Warming.md&1.17&>- Historical total for the country?
ct19-Global-Warming.md&1.17&>- Historical total per capita?
ct19-Global-Warming.md&1.17&>- Future projection of needs until a particular stage of development reached? (“Development rights” approach)
ct19-Global-Warming.md&1.17&
ct19-Global-Warming.md&1.17&
ct19-Global-Warming.md&1.18&## Financing options
ct19-Global-Warming.md&1.18&
ct19-Global-Warming.md&1.18&>- World Climate Change Fund. Equal voice in governance for all countries. 
ct19-Global-Warming.md&1.18&>- Global carbon tax.
ct19-Global-Warming.md&1.18&>- Auctioning international emissions permits and distributing the proceeds to developing countries.
ct19-Global-Warming.md&1.18&
ct19-Global-Warming.md&1.18&
ct19-Global-Warming.md&1.19&## A right to develop?
ct19-Global-Warming.md&1.19&
ct19-Global-Warming.md&1.19&>- Since industrial development is based on CO2 emissions, the right of a country to create such emissions is, to some extent, a right to develop.
ct19-Global-Warming.md&1.19&>- Underdeveloped countries will have more difficulty developing if they are not allowed to emit CO2 freely.
ct19-Global-Warming.md&1.19&>- This creates a historical injustice.
ct19-Global-Warming.md&1.19&>- Should not all countries have the same rights to develop?
ct19-Global-Warming.md&1.19&>- Arguments?
ct19-Global-Warming.md&1.19&
ct19-Global-Warming.md&1.19&. . . 
ct19-Global-Warming.md&1.19&
ct19-Global-Warming.md&1.19&>- The same could be said of slavery: some countries (e.g. the USA and the British Empire) historically have profited from slavery. Does this mean that now all other countries have a right to also employ slavery?
ct19-Global-Warming.md&1.19&>- No. Advantages gained in criminal or immoral ways cannot be claimed by others.
ct19-Global-Warming.md&1.19&>- Compare: “My friend cheated in the exam. Now I also want the right to cheat.”
ct19-Global-Warming.md&1.19&
ct19-Global-Warming.md&1.20&## Carbon offsets (Source: unenvironment.org)
ct19-Global-Warming.md&1.20&
ct19-Global-Warming.md&1.20&![](graphics/19-carbon-offset.png)\ 
ct19-Global-Warming.md&1.20&
ct19-Global-Warming.md&1.20&
ct19-Global-Warming.md&1.20&
ct19-Global-Warming.md&1.20&
ct19-Global-Warming.md&1.20&
ct19-Global-Warming.md&1.21&## Carbon offsets
ct19-Global-Warming.md&1.21&
ct19-Global-Warming.md&1.21&>- The problems
ct19-Global-Warming.md&1.21&>     - Cheating
ct19-Global-Warming.md&1.21&>     - Counting of old infrastructure as new
ct19-Global-Warming.md&1.21&>     - Offsetting for rich countries often happens in poor countries (offsets export trade), thus increasing the burden on poor countries.
ct19-Global-Warming.md&1.21&>- Here is a short, introductory video about the problems of carbon offsets:
ct19-Global-Warming.md&1.21&<https://www.youtube.com/watch?v=xdW-6MXB0sI>
ct19-Global-Warming.md&1.21&>- Here is a somewhat longer, more critical video: <https://www.youtube.com/watch?v=WRNd6K8kS4M>
ct19-Global-Warming.md&1.21&
ct19-Global-Warming.md&1.21&
ct19-Global-Warming.md&1.21&
ct19-Global-Warming.md&1.22&## Ways of adaptation / Effects of global warming
ct19-Global-Warming.md&1.22&
ct19-Global-Warming.md&1.22&>- Migration, abandoning of flooded or desertified areas
ct19-Global-Warming.md&1.22&>- Flooding of coastal cities (Hong Kong!)
ct19-Global-Warming.md&1.22&>- Spread of tropical diseases
ct19-Global-Warming.md&1.22&>- Extinction of animals and insects
ct19-Global-Warming.md&1.22&>- Change of growing seasons and plant choices in agriculture
ct19-Global-Warming.md&1.22&>- Spread of new plant diseases
ct19-Global-Warming.md&1.22&>- Water scarcity
ct19-Global-Warming.md&1.22&>- Health (infectious diseases, cardiovascular diseases)
ct19-Global-Warming.md&1.22&>- Changes in energy availability (some populations dependent on wood, wind, water, which might change). Our society dependent on oil, which might not be usable soon.
ct19-Global-Warming.md&1.22&>- Intensive agriculture dependent on fossil fuels.
ct19-Global-Warming.md&1.22&
ct19-Global-Warming.md&1.22&
ct19-Global-Warming.md&2.0&# Moral theories
ct19-Global-Warming.md&2.0&
ct19-Global-Warming.md&2.1&## Justice and equality
ct19-Global-Warming.md&2.1&
ct19-Global-Warming.md&2.1&>- Equality: Should all nations and all men on Earth be treated equally?
ct19-Global-Warming.md&2.1&>     - What does this mean?
ct19-Global-Warming.md&2.1&>     - If people have different needs, should they be treated strictly equally, or according to their needs (and potentially unequally)?
ct19-Global-Warming.md&2.1&>     - For example, if the government has 1 million dollars to give away, should every citizen get the same amount, or should each one get an amount inversely proportional to their present wealth?
ct19-Global-Warming.md&2.1&
ct19-Global-Warming.md&2.1&
ct19-Global-Warming.md&2.2&## Backward-looking justice
ct19-Global-Warming.md&2.2&
ct19-Global-Warming.md&2.2&>- In “backward-looking” conceptions of justice, we look to the past and try to provide compensation for past wrongs.
ct19-Global-Warming.md&2.2&>- In the case of climate ethics, we would give an advantage (financial compensation, more CO2 quota, technological assistance) to countries that have polluted less in the past.
ct19-Global-Warming.md&2.2&>- But is this just?
ct19-Global-Warming.md&2.2&>- For example, why should the citizens of present-day Germany be punished for the over-use of resources by their great-grandfathers?
ct19-Global-Warming.md&2.2&>- Possible answer: Because they still enjoy the benefits of that over-use of resources (better development, more technology, wealthier lives).
ct19-Global-Warming.md&2.2&
ct19-Global-Warming.md&2.2&
ct19-Global-Warming.md&2.3&## Backward-looking justice: Problems (1)
ct19-Global-Warming.md&2.3&
ct19-Global-Warming.md&2.3&>- Backward-looking justice can sometimes feel unjust.
ct19-Global-Warming.md&2.3&>     - For example, because the past 50 years we had men as professors, now we should have 50 years of woman as professors. So no man will get a chance to become a professor for the next two generations.
ct19-Global-Warming.md&2.3&>     - This does not seem perfectly just. Why should the next generation’s men, who are innocent and have not participated in the initial injustice, be punished in this way?
ct19-Global-Warming.md&2.3&
ct19-Global-Warming.md&2.3&
ct19-Global-Warming.md&2.4&## Backward-looking justice: Problems (2)
ct19-Global-Warming.md&2.4&
ct19-Global-Warming.md&2.4&>- Immanuel Kant (1724-1804): Every human being has the same, absolute value. Every single person should be treated as an end, and not only as the means to some other end.
ct19-Global-Warming.md&2.4&>     - End: The goal of an action. Means: the instrument used to achieve some end.
ct19-Global-Warming.md&2.4&>- In trying to “undo” past wrongs, we would be treating present human beings only as means to the end of establishing justice. This is immoral, according to Kant.
ct19-Global-Warming.md&2.4&
ct19-Global-Warming.md&2.4&
ct19-Global-Warming.md&2.5&## Forward-looking justice
ct19-Global-Warming.md&2.5&
ct19-Global-Warming.md&2.5&>- Here we look towards equal chances for the future, rather than trying to compensate for past wrongs: What can we do to eliminate the injustice from now on and in the future?
ct19-Global-Warming.md&2.5&>- For CO2 emissions, we should establish “equal” emission rights.
ct19-Global-Warming.md&2.5&>- But what is “equal”?
ct19-Global-Warming.md&2.5&>     - Absolute amounts of CO2?
ct19-Global-Warming.md&2.5&>     - Amounts relative to population size?
ct19-Global-Warming.md&2.5&>     - Amounts relative to development needs?
ct19-Global-Warming.md&2.5&>     - Amounts relative (inversely?) to present technological status?
ct19-Global-Warming.md&2.5&>     - Considering available renewable resources?
ct19-Global-Warming.md&2.5&>     - Considering a country’s wealth?
ct19-Global-Warming.md&2.5&>- It looks pretty hard to do this right, too.
ct19-Global-Warming.md&2.5&
ct19-Global-Warming.md&2.6&## Rawls’ idea of justice (1)
ct19-Global-Warming.md&2.6&
ct19-Global-Warming.md&2.6&>- John Rawls (1921-2002), American philosopher: “A Theory of Justice” (1971).
ct19-Global-Warming.md&2.6&>- Original position (explained differently from Rawls): 
ct19-Global-Warming.md&2.6&>     - Imagine you are conscious before you are actually born as a particular individual.
ct19-Global-Warming.md&2.6&>     - Now you look down on Earth and try to create rules for a just society, not knowing as who of all those people down there you will be born (man, woman, poor, rich, Chinese, American or Greek).
ct19-Global-Warming.md&2.6&>     - This is called the “veil of ignorance”. Veil: a kind of curtain that hides the (future) reality from your view.
ct19-Global-Warming.md&2.6&>- Rawls: If we think and create rules in this way, we are more likely to create a truly just society.
ct19-Global-Warming.md&2.6&
ct19-Global-Warming.md&2.6&
ct19-Global-Warming.md&2.7&## Rawls’ idea of justice (2)
ct19-Global-Warming.md&2.7&
ct19-Global-Warming.md&2.7&>- From this original position, we would arrive at the following principles of justice:
ct19-Global-Warming.md&2.7&>- The Liberty Principle: Equal basic liberties for all citizens: freedom of conscience, association, expression and personal property.
ct19-Global-Warming.md&2.7&>- Fair Equality of Opportunity: offices and positions should be open and *effectively* accessible to all.
ct19-Global-Warming.md&2.7&>- The Difference Principle: Any inequalities are only permitted if they work to the advantage of the least well-off.
ct19-Global-Warming.md&2.7&
ct19-Global-Warming.md&2.7&
ct19-Global-Warming.md&2.8&## Rawls’ idea of justice (3)
ct19-Global-Warming.md&2.8&
ct19-Global-Warming.md&2.8&>- *Think how we can apply these principles to CO2 emissions and global warming justice!*
ct19-Global-Warming.md&2.8&>- When we talk about how to regulate CO2 emissions, we should do so from “behind the veil,” that is, assuming that we don’t yet know as who we will be born on this Earth.
ct19-Global-Warming.md&2.8&>- So you wouldn’t try to regulate CO2 emissions knowing that you are a German teacher, an Indian programmer, a US farmer, or a Chinese president. 
ct19-Global-Warming.md&2.8&>- You would assume that you might be in any of these roles, having to deal with the consequences of your regulation. 
ct19-Global-Warming.md&2.8&>- How would you regulate then???
ct19-Global-Warming.md&2.8&
ct19-Global-Warming.md&2.9&## Rawls’ idea of justice (4)
ct19-Global-Warming.md&2.9&
ct19-Global-Warming.md&2.9&
ct19-Global-Warming.md&2.9&>- The Liberty Principle: Equal basic liberties for all citizens: freedom of conscience, association, expression and personal property.
ct19-Global-Warming.md&2.9&>     - Applied to environmental questions, we see that personal liberty often goes against the benefit of all.
ct19-Global-Warming.md&2.9&>     - My liberty to own and drive a car, or fly to a holiday destination by airplane, causes many environmental problems that others will have to bear the consequences of.
ct19-Global-Warming.md&2.9&>     - So the liberties must really be restricted to the most basic ones, while liberties that are more likely to cause harm to society at large should perhaps be restricted.
ct19-Global-Warming.md&2.9&
ct19-Global-Warming.md&2.10&## Rawls’ idea of justice (5)
ct19-Global-Warming.md&2.10&
ct19-Global-Warming.md&2.10&
ct19-Global-Warming.md&2.10&>- Fair Equality of Opportunity: offices and positions should be open and *effectively* accessible to all.
ct19-Global-Warming.md&2.10&>     - In terms of global warming, we might understand this to mean that access to CO2 emissions (and therefore cheap development options) should be available equally to all. It cannot only be a privilege of rich countries, or only allowed to poor countries.
ct19-Global-Warming.md&2.10&>- The Difference Principle: Any inequalities are only permitted if they work to the advantage of the least well-off.
ct19-Global-Warming.md&2.10&>     - This principle breaks the symmetry of the previous one. If we need to restrict emissions, we should always restrict in favour of the poorer, least well-off countries. That means, richer countries should contribute more towards the mitigation of global warming than poorer countries.
ct19-Global-Warming.md&2.10&
ct19-Global-Warming.md&2.10&
ct19-Global-Warming.md&2.11&## Utilitarianism (1)
ct19-Global-Warming.md&2.11&
ct19-Global-Warming.md&2.11&>- Utilitarianism would ask us to look at the benefit of the majority, and to weight benefits and harms against each other.
ct19-Global-Warming.md&2.11&>- For example: What is the harm of CO2 emissions?
ct19-Global-Warming.md&2.11&>     - Global warming leads to flooding, changing climate that affects crops and may cause famines, it will lead to people losing their homes, potentially to more international conflicts, more violent storms etc.
ct19-Global-Warming.md&2.11&
ct19-Global-Warming.md&2.11&
ct19-Global-Warming.md&2.12&## Utilitarianism (2)
ct19-Global-Warming.md&2.12&
ct19-Global-Warming.md&2.12&>- On the other hand, what is the benefit of CO2 emissions for the emitter?
ct19-Global-Warming.md&2.12&>     - To a large extent, it is luxury items and convenience: a private car, a big house, many manufactured items that are convenient or trendy but not essential for life, holidays using airplanes, a meat-based diet instead of a vegetable-based one, etc.
ct19-Global-Warming.md&2.12&>- Accordingly, utilitarianism would probably conclude that we should give up these luxuries and conveniences in order to mitigate the existential, harmful effects of global warming.
ct19-Global-Warming.md&2.12&
ct19-Global-Warming.md&2.13&## Utilitarianism (3)
ct19-Global-Warming.md&2.13&
ct19-Global-Warming.md&2.13&>- But you’d also have to look at the number of people involved. How many suffer from the effects vs how many benefit?
ct19-Global-Warming.md&2.13&>- It seems that the harms will affect many people (floods, famines, storms, diseases), while the benefits of excessive CO2 emissions are concentrated mainly on the more developed countries, which have smaller populations.
ct19-Global-Warming.md&2.13&
ct19-Global-Warming.md&2.14&## Utilitarianism (4)
ct19-Global-Warming.md&2.14&
ct19-Global-Warming.md&2.14&>- But it becomes more difficult to judge when you consider the right of less developed nations to develop. Since the less developed nations, taken together, have a higher population than the developed nations, would a cap on CO2 emissions cause more harm to more people by hindering development of these underdeveloped regions?
ct19-Global-Warming.md&2.14&
ct19-Global-Warming.md&2.14&
ct19-Global-Warming.md&2.15&## Other perspectives
ct19-Global-Warming.md&2.15&
ct19-Global-Warming.md&2.15&>- This presentation provides just some discussion-starters about the ethics of global warming.
ct19-Global-Warming.md&2.15&>- For your own debate contributions, please go out and find your own, additional materials. You are *not* supposed to just copy from this presentation.
ct19-Global-Warming.md&2.15&>- You could, for example, talk more about the science of global warming; the consequences for particular groups; the views of Social Contract or Virtue Ethics theories; about alternative proposals to deal with the problem; and so on.
ct19-Global-Warming.md&2.15&
ct19-Global-Warming.md&2.15&
ct19-Global-Warming.md&2.15&
ct19-Global-Warming.md&2.16&## Any questions?
ct19-Global-Warming.md&2.16&
ct19-Global-Warming.md&2.16&![](graphics/questions.jpg)\ 
ct19-Global-Warming.md&2.16&
ct19-Global-Warming.md&2.17&## Thank you for your attention!
ct19-Global-Warming.md&2.17&
ct19-Global-Warming.md&2.17&Email: <matthias@ln.edu.hk>
ct19-Global-Warming.md&2.17&
ct19-Global-Warming.md&2.17&If you have questions, please come to my office hours (see course outline).
ct19-Global-Warming.md&2.17&
ct19-Global-Warming.md&2.17&
ct19-Global-Warming.md&2.17&
ct19-Global-Warming.md&2.17&
ct19-Global-Warming.md&2.17&
ct20-Surveillance.md&0.1&% Critical Thinking
ct20-Surveillance.md&0.1&% 20. Debate 3: Surveillance and freedom
ct20-Surveillance.md&0.1&% A. Matthias
ct20-Surveillance.md&0.1&
ct20-Surveillance.md&1.0&# Where we are
ct20-Surveillance.md&1.0&
ct20-Surveillance.md&1.1&## What did we learn last time?
ct20-Surveillance.md&1.1&
ct20-Surveillance.md&1.1&- Climate change ethics
ct20-Surveillance.md&1.1&
ct20-Surveillance.md&1.2&## What are we going to learn today?
ct20-Surveillance.md&1.2&
ct20-Surveillance.md&1.2&- Surveillance and freedom
ct20-Surveillance.md&1.2&
ct20-Surveillance.md&2.0&# How to deal with an applied question
ct20-Surveillance.md&2.0&
ct20-Surveillance.md&2.0&*Not necessarily in that order:*
ct20-Surveillance.md&2.0&
ct20-Surveillance.md&2.0&- Understand the problem
ct20-Surveillance.md&2.0&- Identify the stakeholders
ct20-Surveillance.md&2.0&- Make relevant distinctions and clarify the issues
ct20-Surveillance.md&2.0&- Find and quote relevant theory (from research)
ct20-Surveillance.md&2.0&- Find the main arguments for each side (from research)
ct20-Surveillance.md&2.0&- Find places where more research (facts) is needed
ct20-Surveillance.md&2.0&- Identify benefits and costs and weigh them against each other
ct20-Surveillance.md&2.0&- Avoid yes/no answers. Try to see the complexities of the problem
ct20-Surveillance.md&2.0&- Try to find original alternatives that solve the problem better than just saying that one side is right (for example: meat eating might be morally right if animals are treated well, if we reduce the amount of meat consumed, if we eat only wild animals or insects etc)
ct20-Surveillance.md&2.0&
ct20-Surveillance.md&2.0&
ct20-Surveillance.md&2.0&
ct20-Surveillance.md&3.0&# Social contract
ct20-Surveillance.md&3.0&
ct20-Surveillance.md&3.1&## Hobbes, Locke, Rousseau
ct20-Surveillance.md&3.1&
ct20-Surveillance.md&3.1&Three thinkers:
ct20-Surveillance.md&3.1&
ct20-Surveillance.md&3.1&. . . 
ct20-Surveillance.md&3.1&
ct20-Surveillance.md&3.1&- Thomas Hobbes (1588-1679)
ct20-Surveillance.md&3.1&- John Locke (1632-1704)
ct20-Surveillance.md&3.1&- Jean-Jacques Rousseau (1712-1778)
ct20-Surveillance.md&3.1&
ct20-Surveillance.md&3.1&. . .
ct20-Surveillance.md&3.1&
ct20-Surveillance.md&3.1&We will mainly talk about Hobbes.
ct20-Surveillance.md&3.1&
ct20-Surveillance.md&4.0&# The State of Nature
ct20-Surveillance.md&4.0&
ct20-Surveillance.md&4.1&## The State of Nature (1)
ct20-Surveillance.md&4.1&
ct20-Surveillance.md&4.1&Imagine there is no God, no society, no government. Just a few people living each for themselves in nature.
ct20-Surveillance.md&4.1&
ct20-Surveillance.md&4.1&How would life be?
ct20-Surveillance.md&4.1&
ct20-Surveillance.md&4.1&- Hunting, gathering food
ct20-Surveillance.md&4.1&- Gathered food can be taken away by others
ct20-Surveillance.md&4.1&- No trust between people, therefore no friendships
ct20-Surveillance.md&4.1&- A state of war of all against all
ct20-Surveillance.md&4.1&
ct20-Surveillance.md&4.1&. . .
ct20-Surveillance.md&4.1&
ct20-Surveillance.md&4.1&Hobbes calls this the *"State of Nature."*
ct20-Surveillance.md&4.1&
ct20-Surveillance.md&4.2&## The State of Nature (2)
ct20-Surveillance.md&4.2&
ct20-Surveillance.md&4.2&What would *not* exist in this world?
ct20-Surveillance.md&4.2&
ct20-Surveillance.md&4.2&- Schools, Universities
ct20-Surveillance.md&4.2&- Industry
ct20-Surveillance.md&4.2&- Markets
ct20-Surveillance.md&4.2&- Division of labour
ct20-Surveillance.md&4.2&- Culture, books, art
ct20-Surveillance.md&4.2&- Air-conditioning, proper buildings, roads, electricity, water pipes, metals, iPhones
ct20-Surveillance.md&4.2&- No money and no way to accumulate wealth (nature's wealth rots away easily!)
ct20-Surveillance.md&4.2&
ct20-Surveillance.md&4.2&. . .
ct20-Surveillance.md&4.2&
ct20-Surveillance.md&4.2&... because all these things require division of labour and personal safety!
ct20-Surveillance.md&4.2&
ct20-Surveillance.md&4.3&## The State of Nature (3)
ct20-Surveillance.md&4.3&
ct20-Surveillance.md&4.3&Is it true that the stronger people would be happy with this life, while only the weak would suffer?
ct20-Surveillance.md&4.3&
ct20-Surveillance.md&4.3&. . . 
ct20-Surveillance.md&4.3&
ct20-Surveillance.md&4.3&No, the strong would suffer too:
ct20-Surveillance.md&4.3&
ct20-Surveillance.md&4.3&1. Every strong person has once been weak (as a child)
ct20-Surveillance.md&4.3&2. Every strong person will be weak again (in sleep, in illness, in old age)
ct20-Surveillance.md&4.3&3. Except for one person, every strong person has at least one who is even stronger than him. If he meets those, he will be the relatively weaker one
ct20-Surveillance.md&4.3&4. Many weak people together can always defeat one strong person
ct20-Surveillance.md&4.3&
ct20-Surveillance.md&4.4&## The State of Nature (4)
ct20-Surveillance.md&4.4&
ct20-Surveillance.md&4.4&Even if one could be undisputed and strong in one's community, would he enjoy his life?
ct20-Surveillance.md&4.4&
ct20-Surveillance.md&4.4&>- There are no luxuries, no culture, no comfortable buildings, no electricity
ct20-Surveillance.md&4.4&>- No accumulation of wealth possible. Natural products rot and go bad
ct20-Surveillance.md&4.4&>- The life of the strong is almost as bad as the life of the poor, and much worse than the poorest life in *our* society
ct20-Surveillance.md&4.4&
ct20-Surveillance.md&4.5&## The State of Nature (5)
ct20-Surveillance.md&4.5&
ct20-Surveillance.md&4.6&### Hobbes:
ct20-Surveillance.md&4.6&
ct20-Surveillance.md&4.6&The life of man in the State of Nature would be: **"solitary, poor, nasty, brutish, and short."**
ct20-Surveillance.md&4.6&
ct20-Surveillance.md&4.6&. . . 
ct20-Surveillance.md&4.6&
ct20-Surveillance.md&4.6&>- Solitary: lonely, because there is no trust between men
ct20-Surveillance.md&4.6&>- Poor: because there is no wealth to be had
ct20-Surveillance.md&4.6&>- Nasty: bad, annoying, dangerous
ct20-Surveillance.md&4.6&>- Brutish: animal-like, because there is no culture, no education
ct20-Surveillance.md&4.6&>- Short: This seems like a blessing, considering the other attributes
ct20-Surveillance.md&4.6&
ct20-Surveillance.md&4.6&
ct20-Surveillance.md&4.7&## The core idea of Hobbes' Social Contract
ct20-Surveillance.md&4.7&
ct20-Surveillance.md&4.7&We can agree to:
ct20-Surveillance.md&4.7&
ct20-Surveillance.md&4.7&>- Give power away from each one individual,
ct20-Surveillance.md&4.7&>- and to concentrate this power in one, extremely powerful entity.
ct20-Surveillance.md&4.7&>- This entity will, in return, force all the citizens to cooperate, so that they can escape the State of Nature.
ct20-Surveillance.md&4.7&
ct20-Surveillance.md&4.8&## Locke’s State of Nature
ct20-Surveillance.md&4.8&
ct20-Surveillance.md&4.8&>- For Locke, things are a bit different.
ct20-Surveillance.md&4.8&>- The State of Nature is not such a bad state.
ct20-Surveillance.md&4.8&>- Humans have share all natural resources. They own their own lives and the fruits of their own work.
ct20-Surveillance.md&4.8&>- People *naturally* have the right of life, liberty and possessions.
ct20-Surveillance.md&4.8&>- Sometimes, particular “bad” people disturb the peaceful working of primitive society and threaten the rights of others.
ct20-Surveillance.md&4.8&>- Everyone is entitled to protect their own rights, stop those who threaten the order, and restore the peace.
ct20-Surveillance.md&4.8&>     - In fact, people are not even *allowed* to voluntarily give away their rights. The rights to life and freedom are given by God and therefore cannot be given away, even with the consent of the individual. 
ct20-Surveillance.md&4.8&>     - Therefore, nobody *can* agree to a dictatorship that would take away these rights.
ct20-Surveillance.md&4.8&
ct20-Surveillance.md&4.9&## Locke’s government
ct20-Surveillance.md&4.9&
ct20-Surveillance.md&4.9&>- In time, people realise that it is easier to keep society safe if they form a special group of people (the government) whose job it becomes to protect people’s rights.
ct20-Surveillance.md&4.9&>- So for Locke, the government has a particular task to fulfil. But it is not allowed to interfere with the rights of the citizens.
ct20-Surveillance.md&4.9&>- The separation of legislative and executive power is necessary to make sure that the government does not misuse its power.
ct20-Surveillance.md&4.9&>- The invention of *money* makes it possible to store wealth and gives an incentive for people to produce more than they can consume, so that the wealth of the society increases. This is a good thing for all citizens. Eventually, the additional wealth will benefit everyone.
ct20-Surveillance.md&4.9&
ct20-Surveillance.md&4.10&## Rousseau’s State of Nature (1)
ct20-Surveillance.md&4.10&
ct20-Surveillance.md&4.10&>- Rousseau had quite the opposite idea from Hobbes.
ct20-Surveillance.md&4.10&>- He thought that the State of Nature was an environment in which people were originally free, equal and peaceful.
ct20-Surveillance.md&4.10&>- When people began to claim property and take things for themselves, the result was inequality, conflicts and war.
ct20-Surveillance.md&4.10&>- The social contract, for Rousseau, mainly has the function of protecting the original freedom of men.
ct20-Surveillance.md&4.10&
ct20-Surveillance.md&4.11&## Rousseau’s State of Nature (2)
ct20-Surveillance.md&4.11&
ct20-Surveillance.md&4.11&>- Therefore, the society should not be ruled by a king or political party, but by a system where all citizens directly form the government. There should be no separation of powers.
ct20-Surveillance.md&4.11&>- All decisions should be taken by direct vote of the citizens. So everyone would be “forced to be free.”
ct20-Surveillance.md&4.11&>- The government should be “mere officials” who administer society, but who don’t take decisions of their own. They just execute the will of the people.
ct20-Surveillance.md&4.11&>- Rousseau was very negative about the governments of his time. He thought that they did not properly implement a social contract and that they did not protect citizens’ interests.
ct20-Surveillance.md&4.11&
ct20-Surveillance.md&4.11&
ct20-Surveillance.md&4.12&## The Social Contract
ct20-Surveillance.md&4.12&
ct20-Surveillance.md&4.13&### The core idea:
ct20-Surveillance.md&4.13&
ct20-Surveillance.md&4.13&The Social Contract is a fictional contract that people create so that power is taken away from the individuals and transferred to a Third Party. In return, the Third Party will provide the guarantees that are necessary in order for the individuals to be able to escape the State of Nature.
ct20-Surveillance.md&4.13&
ct20-Surveillance.md&4.14&### A more general definition:
ct20-Surveillance.md&4.14&
ct20-Surveillance.md&4.14&"[A set of rules that] rational people will agree to accept, for their mutual benefit, on the condition that others follow those rules as well." (Rachels, 11.3)
ct20-Surveillance.md&4.14&
ct20-Surveillance.md&4.15&## The three thinkers
ct20-Surveillance.md&4.15&
ct20-Surveillance.md&4.15&>- Hobbes: Strong government keeps everyone out of the state of nature.
ct20-Surveillance.md&4.15&>- Locke: Weaker government only protects original freedoms.
ct20-Surveillance.md&4.15&>- Rousseau: Government takes no own decisions, only executes the will of the people.
ct20-Surveillance.md&4.15&
ct20-Surveillance.md&4.15&
ct20-Surveillance.md&4.16&## Government power
ct20-Surveillance.md&4.16&
ct20-Surveillance.md&4.16&When you create a powerful entity like that, it is better not to give it too much power, else it may misuse its power to suppress you and take away your freedom.
ct20-Surveillance.md&4.16&
ct20-Surveillance.md&4.16&>- So, how much power do we need to give to the government?
ct20-Surveillance.md&4.16&>- And what exactly should its functions be?
ct20-Surveillance.md&4.16&
ct20-Surveillance.md&4.17&## Government power: Hobbes
ct20-Surveillance.md&4.17&
ct20-Surveillance.md&4.17&>- The government is the only protection from the state of nature. Since the state of nature is the worst possible life, whatever a government does will be better. 
ct20-Surveillance.md&4.17&>- Therefore, governments should even have the right to take away personal liberties if this is necessary in order to secure the well-functioning of society.
ct20-Surveillance.md&4.17&>- Free speech, freedom of movement and religion, these are all less important than being out of the state of nature, and can therefore be given up to a strong government.
ct20-Surveillance.md&4.17&>- But, in the end, the purpose of the government should always be to work *for* its citizens, not against them.
ct20-Surveillance.md&4.17&>- So a government that clearly puts its citizens in a worse condition than the state of nature would not be legitimate (treatment of Jews in WW2 Germany, slavery).
ct20-Surveillance.md&4.17&
ct20-Surveillance.md&4.18&## Government power: Locke (1)
ct20-Surveillance.md&4.18&
ct20-Surveillance.md&4.18&>- “A morally legitimate government coerces [=forces people to do something] only those who freely and voluntarily consent to its authority.” (Section 95 in the Second Treatise.)
ct20-Surveillance.md&4.18&>- A government that claims absolute, unlimited authority to coerce its subjects cannot be morally legitimate.
ct20-Surveillance.md&4.18&>- No one could rationally consent to an absolutist government. 
ct20-Surveillance.md&4.18&>- The rational basis for establishing government and submitting to it is to bring it about that fundamental moral rights specified by natural law are upheld (chapter 9). 
ct20-Surveillance.md&4.18&>- Submitting to absolutist government would make it far more likely that fundamental moral rights, one’s own and those of other people, would be violated (sections 131, 139, and others). 
ct20-Surveillance.md&4.18&>- Hence, no absolutist government could arise by consent.
ct20-Surveillance.md&4.18&
ct20-Surveillance.md&4.19&## Government power: Locke (2)
ct20-Surveillance.md&4.19&
ct20-Surveillance.md&4.19&>- One does not have an absolute property right over oneself, one’s own body. Therefore, one does not have a right to commit suicide or to damage oneself.
ct20-Surveillance.md&4.19&>- Why?
ct20-Surveillance.md&4.19&>     - Because one’s rights are given by nature, or God, and thus can be limited.
ct20-Surveillance.md&4.19&>	 - We are not born free, in full possession of all possible rights over ourselves.
ct20-Surveillance.md&4.19&>- Since we don’t have unlimited rights over our own lives, we are not morally at liberty to transfer such rights to a political ruler by free and voluntary consent.
ct20-Surveillance.md&4.19&
ct20-Surveillance.md&4.20&## Government power: Locke (3)
ct20-Surveillance.md&4.20&
ct20-Surveillance.md&4.20&>- A morally legitimate government, 
ct20-Surveillance.md&4.20&>     - (=which coerces by right and which those coerced are obligated to support and obey), is one that both 
ct20-Surveillance.md&4.20&>	 - (a) coerces only those who freely and voluntarily consent to its authority and 
ct20-Surveillance.md&4.20&>	 - (b) protects their fundamental moral rights. ^[(http://philosophyfaculty.ucsd.edu/faculty/rarneson/Courses/LockeLimited.pdf)]
ct20-Surveillance.md&4.20&
ct20-Surveillance.md&4.21&## Government power: Locke (4)
ct20-Surveillance.md&4.21&
ct20-Surveillance.md&4.21&>- Locke advocates limited government, and limits to the authority of government. 
ct20-Surveillance.md&4.21&>- If the political rulers are violating people’s fundamental moral rights, it is right to rebel against them.
ct20-Surveillance.md&4.21&
ct20-Surveillance.md&4.21&
ct20-Surveillance.md&4.22&## Government power: Rousseau (1)
ct20-Surveillance.md&4.22&
ct20-Surveillance.md&4.22&>- Rousseau rejects the idea that the government should rule over the people.
ct20-Surveillance.md&4.22&>- For him, to hand over one’s general right of ruling oneself to another person or body constitutes a form a slavery. 
ct20-Surveillance.md&4.22&>- A periodically elected  government would necessarily be legislating on a range of topics on which citizens have not given their opinion. 
ct20-Surveillance.md&4.22&>- Laws passed by such assemblies would therefore bind citizens in terms that they have not themselves agreed upon.
ct20-Surveillance.md&4.22&>- Elections and government by representation is therefore a symptom of moral decline and the loss of virtue.
ct20-Surveillance.md&4.22&
ct20-Surveillance.md&4.23&## Government power: Rousseau (2)
ct20-Surveillance.md&4.23&
ct20-Surveillance.md&4.23&>- But realistically, not everyone can be directly involved in day-to-day government operations.
ct20-Surveillance.md&4.23&>- Therefore, Rousseau distinguishes between sovereign and government.
ct20-Surveillance.md&4.23&>- The sovereign, composed of the people as a whole, makes laws as an expression of its general will. 
ct20-Surveillance.md&4.23&>- The government is a more limited body that administers the state within the bounds set by the laws, and which applies the laws to particular cases.
ct20-Surveillance.md&4.23&
ct20-Surveillance.md&4.24&## How much power should the government have? (Summary)
ct20-Surveillance.md&4.24&
ct20-Surveillance.md&4.24&>- For Hobbes, the state of nature is so bad that even a bad government is better than none. So the citizens should accept is as it is, even if it is bad. But it still must be better than the state of nature.
ct20-Surveillance.md&4.24&>- For Locke, the government cannot *create* any rights, including the right to ownership of things. Everyone naturally already has rights, and the government has to respect and protect these rights.
ct20-Surveillance.md&4.24&>- For Locke, if the citizens agree that the government is not doing its job, they should always be able to choose a new government and replace the old one.
ct20-Surveillance.md&4.24&>- For Locke, dividing the government into two parts (legislative and executive) is a way of restricting and controlling the government’s power.
ct20-Surveillance.md&4.24&
ct20-Surveillance.md&4.25&## The state violating the Contract
ct20-Surveillance.md&4.25&
ct20-Surveillance.md&4.25&Can the state violate the Social Contract?
ct20-Surveillance.md&4.25&
ct20-Surveillance.md&4.25&Yes:
ct20-Surveillance.md&4.25&
ct20-Surveillance.md&4.25&>- When the state itself harms its citizens more than the state of nature would do; or
ct20-Surveillance.md&4.25&>- When it is unable to protect them; or
ct20-Surveillance.md&4.25&>- When it violates their natural rights (Locke)
ct20-Surveillance.md&4.25&
ct20-Surveillance.md&4.26&## What is the consequence of a state violating the contract?
ct20-Surveillance.md&4.26&
ct20-Surveillance.md&4.26&Different philosophers disagreed on this:
ct20-Surveillance.md&4.26&
ct20-Surveillance.md&4.26&. . . 
ct20-Surveillance.md&4.26&
ct20-Surveillance.md&4.26&>- Hobbes thought that once citizens have given their power away, they have to obey their government unconditionally.
ct20-Surveillance.md&4.26&>- Locke thought that citizens do have the right to revolt against the government if it does not protect their (naturally given) rights.
ct20-Surveillance.md&4.26&>- Rousseau did not think that a government was a good (or necessary thing) at all. Citizens should directly decide about matters together. Whether a particular state action violates the contract or not depends on the actual agreements between the contract members.
ct20-Surveillance.md&4.26&
ct20-Surveillance.md&4.26&
ct20-Surveillance.md&5.0&# Surveillance
ct20-Surveillance.md&5.0&
ct20-Surveillance.md&5.1&## Forms of surveillance
ct20-Surveillance.md&5.1&
ct20-Surveillance.md&5.1&>- When you want to talk about surveillance, you need to distinguish different types of it:
ct20-Surveillance.md&5.1&>     - companies monitoring employees’ working times and performance;
ct20-Surveillance.md&5.1&>     - parents monitoring their children (babyphones);
ct20-Surveillance.md&5.1&>     - the MTR monitoring stations and trains with security cameras; 
ct20-Surveillance.md&5.1&>     - the government monitoring streets with face recognition cameras, reading citizens’ social media posts, reading private correspondence, tapping citizens’ phones (NSA);
ct20-Surveillance.md&5.1&>- Potentially, surveillance may lead to other measures: social credit, restrictions on movement and so on.
ct20-Surveillance.md&5.1&>- All these have to be distinguished. They are different in many respects (for example, misuse potential, who profits from the surveillance, what the ultimate goal is, whether it respects individual freedom, ...)
ct20-Surveillance.md&5.1&
ct20-Surveillance.md&5.1&
ct20-Surveillance.md&6.0&# Surveillance technology
ct20-Surveillance.md&6.0&
ct20-Surveillance.md&6.1&## Different types of surveillance
ct20-Surveillance.md&6.1&
ct20-Surveillance.md&6.1&>- Research different types of surveillance and technologies used:
ct20-Surveillance.md&6.1&>     - Cameras with face recognition
ct20-Surveillance.md&6.1&>     - Work clocking systems
ct20-Surveillance.md&6.1&>     - GPS bracelets for quarantine purposes
ct20-Surveillance.md&6.1&>     - Gait recognition
ct20-Surveillance.md&6.1&>     - Phone line tapping
ct20-Surveillance.md&6.1&>     - Internet and social media observation
ct20-Surveillance.md&6.1&>     - Observation through walls
ct20-Surveillance.md&6.1&>     - Household garbage analysis
ct20-Surveillance.md&6.1&>- How are they different? Are they all equally effective? Are they equally dangerous? Are some better than others? What are their moral implications?
ct20-Surveillance.md&6.1&
ct20-Surveillance.md&6.1&
ct20-Surveillance.md&7.0&# Ethics of surveillance
ct20-Surveillance.md&7.0&
ct20-Surveillance.md&7.1&## Utilitarianism
ct20-Surveillance.md&7.1&
ct20-Surveillance.md&7.1&Different approaches. Utilitarianism: 
ct20-Surveillance.md&7.1&
ct20-Surveillance.md&7.1&>- Does surveillance improve the society’s happiness? Does it provide a benefit to most people that is higher than its drawbacks?
ct20-Surveillance.md&7.1&>- In order to judge that, you need to consider:
ct20-Surveillance.md&7.1&>     - Effectiveness of surveillance in preventing crime? (research!)
ct20-Surveillance.md&7.1&>     - How many people share the burden vs how many are benefited?
ct20-Surveillance.md&7.1&>     - Are there other ways to improve safety without the drawbacks?
ct20-Surveillance.md&7.1&>     - Other drawbacks? For example, extensive surveillance may endanger trade secrets, free speech, freedom of academic research, the ability to remove bad governments in the future and so on.
ct20-Surveillance.md&7.1&
ct20-Surveillance.md&7.1&
ct20-Surveillance.md&7.2&## Kant
ct20-Surveillance.md&7.2&
ct20-Surveillance.md&7.2&>- Kant: all humans are equal. They must be treated as ends, not only as means to some other end. Their own ends must be respected.
ct20-Surveillance.md&7.2&>- Does surveillance treat people “as ends”?
ct20-Surveillance.md&7.2&>- Or do people become just means to the end of lower crime rates?
ct20-Surveillance.md&7.2&>- Compare: forcing drivers and car passengers to wear seat belts. Is this morally right? What if the government enforces it by monitoring seat belt use with sensors inside the car? Would it then be morally right or wrong? Why exactly?
ct20-Surveillance.md&7.2&>- How far does personal freedom extend? Does it include the right to harm others? Does it include the right to harm oneself? Does the government need to protect citizens from their own mistakes or ignorance? (e.g. by certifying particular products as safe, or by outlawing unsafe products).
ct20-Surveillance.md&7.2&
ct20-Surveillance.md&7.3&## Social contract
ct20-Surveillance.md&7.3&
ct20-Surveillance.md&7.3&>- What kinds of surveillance would rational citizens consent to? Why exactly?
ct20-Surveillance.md&7.3&>- What kinds of surveillance are necessary for a society to stay out of the state of nature? What kinds promote or threaten the essential goals of the social contract? (The answer will be different for Hobbes/Locke/Rousseau!)
ct20-Surveillance.md&7.3&>- How can the government be prevented from misusing its powers? What legal and procedural safeguards need to be in place to prevent misuse and guarantee that the citizens keep their maximum freedom, while at the same time protecting society?
ct20-Surveillance.md&7.3&
ct20-Surveillance.md&7.3&
ct20-Surveillance.md&7.4&## Effect on moral standards
ct20-Surveillance.md&7.4&
ct20-Surveillance.md&7.4&>- Another argument against total surveillance is that it endangers the moral fabric of society.
ct20-Surveillance.md&7.4&>- If people are unable to do anything bad, then acting morally is not a choice anymore, but becomes inevitable and enforced. So people are not any more choosing to be good (or bad). They give up their moral freedom.
ct20-Surveillance.md&7.4&>- As a result, being “good” is not morally valuable any more, and people lose the ability to make moral choices. In the end, this leads to a population that is obedient like a flock of sheep, but that doesn’t have any understanding of moral values.
ct20-Surveillance.md&7.4&>- Such populations are dangerous, because this total obedience can be misused by bad governments for bad ends (for example, to start wars).
ct20-Surveillance.md&7.4&>- It is always better to have a population that is able to make moral decisions, the argument goes, even if this means tolerating a few crimes.
ct20-Surveillance.md&7.4&
ct20-Surveillance.md&7.4&
ct20-Surveillance.md&8.0&# Final remarks
ct20-Surveillance.md&8.0&
ct20-Surveillance.md&8.1&## Conclusion about creating your arguments / writing a paper
ct20-Surveillance.md&8.1&
ct20-Surveillance.md&8.1&- Think through the different aspects of the problem
ct20-Surveillance.md&8.1&- Show that you thought about the different aspects, but then pick a small part of the question that you can answer well for the main part of your argument or paper
ct20-Surveillance.md&8.1&- Avoid yes/no answers. Appreciate the complexity of the problem
ct20-Surveillance.md&8.1&- Do your research, both about theory and about facts
ct20-Surveillance.md&8.1&- Use your research, and quote it correctly if you write a paper
ct20-Surveillance.md&8.1&- Try to find original solutions to the problem that go beyond saying that one side is right or wrong
ct20-Surveillance.md&8.1&
ct20-Surveillance.md&8.1&
ct20-Surveillance.md&8.2&## Any questions?
ct20-Surveillance.md&8.2&
ct20-Surveillance.md&8.2&![](graphics/questions.jpg)\ 
ct20-Surveillance.md&8.2&
ct20-Surveillance.md&8.3&## Thank you for your attention!
ct20-Surveillance.md&8.3&
ct20-Surveillance.md&8.3&Email: <matthias@ln.edu.hk>
ct20-Surveillance.md&8.3&
ct20-Surveillance.md&8.3&If you have questions, please come to my office hours (see course outline).
ct26-Revision.md&0.1&% Critical Thinking
ct26-Revision.md&0.1&% 16 Analysing arguments
ct26-Revision.md&0.1&% A. Matthias
ct26-Revision.md&0.1&
ct26-Revision.md&1.0&# Where we are
ct26-Revision.md&1.0&
ct26-Revision.md&1.1&## What did we learn last time?
ct26-Revision.md&1.1&
ct26-Revision.md&1.2&## What are we going to learn today?
ct26-Revision.md&1.2&
ct26-Revision.md&2.0&# Argument mapping
ct26-Revision.md&2.0&
ct26-Revision.md&2.0&
ct26-Revision.md&2.0&
ct26-Revision.md&2.0&
ct26-Revision.md&2.1&## What did we learn today?
ct26-Revision.md&2.1&- How to express arguments in predicate logic 
ct26-Revision.md&2.1&- Validity in propositional logic
ct26-Revision.md&2.1&- Natural deduction
ct26-Revision.md&2.1&
ct26-Revision.md&2.2&## References
ct26-Revision.md&2.2&HKU OpenCourseWare on critical thinking, logic, and creativity:
ct26-Revision.md&2.2&
ct26-Revision.md&2.2&http://philosophy.hku.hk/think/pl
ct26-Revision.md&2.2&
ct26-Revision.md&2.2&(Modules Q04, Q05)
ct26-Revision.md&2.2&
ct26-Revision.md&2.3&## Any questions?
ct26-Revision.md&2.3&
ct26-Revision.md&2.3&![](graphics/questions.jpg)\ 
ct26-Revision.md&2.3&
ct26-Revision.md&2.4&## Thank you for your attention!
ct26-Revision.md&2.4&
ct26-Revision.md&2.4&Email: <matthias@ln.edu.hk>
ct26-Revision.md&2.4&
ct26-Revision.md&2.4&If you have questions, please come to my office hours (see course outline).
ct26-Revision.md&2.4&
ct26-Revision.md&2.4&
ct26-Revision.md&2.4&
ct26-Revision.md&2.4&
ct26-Revision.md&2.4&
