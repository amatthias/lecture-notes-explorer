% Critical Thinking
% 12. Argument structure -- Analogies
% A. Matthias

# Where we are

## What did we learn last time?

- A little more about the validity of arguments
- What deductive, inductive, valid, strong and weak arguments are

## What are we going to learn today?

- How to restructure and analyse arguments.
- What analogies are, and how to evaluate them.

# Argument structure

## What is an argument?

An argument is a series of statements (premises) that present *reasons* which, if true, would make it certain (deductive) or likely (inductive) that a particular conclusion is true.

## Analysing arguments

>- When presented with an argument, we must first identify what the premises are and what the conclusion is.
>- Often you recognise the conclusion by words like "so," or "therefore."
>- If unsure, see which premises/conclusion would give you a valid argument that does not beg the question and is not a tautology or contradiction.
>- Sometimes you will need to add missing (hidden) premises, or even a missing conclusion in order to make the argument valid.
>- If unsure, write the argument down as a series of logical expressions, and then look which missing premises would be needed to make it valid.

## Analysing complex arguments

>- More complex arguments have *sub-conclusions* (intermediate conclusions).
>- You then need to write down the premises, the subconclusion that follows from them, other premises and *their* subconclusion and so on.
>- In the end, the subconclusions will usually be the premises to the final conclusion.
>- It is often easier to begin with the conclusion and work backwards towards the premises, adding whatever hidden premises and intermediate conclusions you need in order to make the argument valid.

## Argument analysis (1)

Consider the following argument:

"The world would be better if there was a single world government. If there was such a government, then there would be no wars. There would also be much more economic cooperation and free trade between the different regions of the world. On the other hand, if there was a world government, there would be more administration costs. But these would be outweighed by the benefits of having a world government."

- What is the conclusion?
- Does the speaker believe that there would be more administrative costs if there was a world government?
- If so, does this belief support the conclusion?

## Argument analysis (2)

"The world would be better if there was a single world government. If there was such a government, then there would be no wars. There would also be much more economic cooperation and free trade between the different regions of the world. On the other hand, if there was a world government, there would be more administration costs. But these would be outweighed by the benefits of having a world government."

- Conclusion: The world would be better if there was a single world government.
- Does the speaker believe that there would be more administrative costs if there was a world government? -- Yes.
- Does this belief support the conclusion? -- No. This is a counter-argument, but he dismisses it, because the benefits would outweigh the additional costs.

## Argument analysis (1)

Consider the following argument:

"To punish people merely for what they have done would be unjust, because the forbidden act might have been an accident for which the person who did it cannot be held to blame."

Write down the argument in standard form (premises, conclusion), including any hidden premises.

. . .

>- You can see that the conclusion contains the word "unjust," for which there is no premise!
>- So the argument cannot be valid.
>- We need a premise to connect "cannot be blamed" with "unjust."

## Argument analysis (2)

The explicit premises:

The forbidden act might be an accident.  
Persons cannot be blamed for (some) accidents.  
 --------------------------------------------------------------  
To punish people merely for what they have done would be unjust.

You can easily see that this argument is not valid. There are no premises about "unjust," and so the conclusion does not follow from the premises. *What is the hidden premise?*

. . .

Hidden premise: it is unjust to punish people, if they cannot be blamed for an act.

## Argument analysis (1)

Consider the following argument:

"Democracy is not perfect, since it has many problems. For example, it is much more difficult for a leader of a democracy to implement unpopular policies than it is for a leader of a non-democratic government, even when those policies are the best policies. Nevertheless, democracy is the best form of government there is, since other forms of government have even bigger problems. This is why Hong Kong should have a democracy."

Present this argument in standard format. Identify the ultimate conclusion as well as the premises and sub-conclusions (intermediate conclusions). Do not include any premises or sub-conclusions that don't support the main conclusion of the argument!

## Argument analysis (2)

>- Conclusion: Hong Kong should have a democracy.
>- In the conclusion we have "Hong Kong," but not in the premises.
>- So we need to create a premise that mentions Hong Kong in a way that it can then be used in the conclusion.
>- You also see that the whole section from "For example" to "best policies" is a counter-argument. According to the instructions, we don't need to include it in our answer.

## Argument analysis (3)

"Democracy is not perfect, since it has many problems. \textcolor{red}{For example, it is much more difficult for a leader of a democracy to implement unpopular policies than it is for a leader of a non-democratic government, even when those policies are the best policies.} Nevertheless, democracy is the best form of government there is, since other forms of government have even bigger problems. This is why Hong Kong should have a democracy."

>- Premise: Other forms of government have bigger problems.
>- Sub-conclusion: Democracy is the best form of government there is.
>- Hidden premise: Hong Kong should have the best form of government.
>- Conclusion: Hong Kong should have a democracy.
>- The other premises and subconclusion do not support this conclusion, but form a counter-argument.

## Standard form and hidden premises (1)

"If we had burnt too much fossil fuel in the 20th century, there would be too much carbon dioxide in the atmosphere. If there was too much carbon dioxide in the atmosphere, global warming would be increasing. Life on earth is in great danger."

Present this argument in standard form and add hidden premises as needed to make it into a deductively valid argument!

## Standard form and hidden premises (2)

>- First, find the conclusion.
>- Conclusion: “Life on Earth is in great danger.”
>- Second, try to see the logical structure of the argument:
>     - Premise 1: If we had burnt too much fossil fuel (F) in the 20th century, there would be too much carbon dioxide in the atmosphere (C). 
>     - Premise 2: If there was too much carbon dioxide in the atmosphere (C), global warming would be increasing (W).
>     - Conclusion: Life on earth is in great danger (D).

. . . 

F$\to$C  
C$\to$W  
------  
D


## Standard form and hidden premises (3)

F$\to$C  
C$\to$W  
------  
D

>- You can see immediately that this won’t do.
>- D cannot be validly concluded from these two premises. So we need some additional (hidden) premises.
>- Just looking at the logical form, without even looking at the meaning: if we had W$\to$D, then we’d have created a connection between the premises and the conclusion:

. . . 

F$\to$C  
C$\to$W  
W$\to$D  
------  
D


## Standard form and hidden premises (4)

F$\to$C  
C$\to$W  
W$\to$D  
------  
D

>- This is a kind of hypothetical syllogism (look back to the session where we learned natural deduction!) But it’s still not complete. Can you see why?
>- The premises are all hypothetical: If F, then C; etc. But in order to conclude D, we need a premise that gives us at least an F, C, or W, so that we can affirm the antecedent and get a D as a result.
>- But if we had a premise, say, “W”, then the first two premises would be useless.
>- In order to use *all* the premises, we’d need another hidden premise: F.


## Standard form and hidden premises (5)

F$\to$C  
C$\to$W  
W$\to$D  
F   
------  
D

This is now valid! (3x affirming antecedent).



## Standard form and hidden premises (6)

>- Translated back to English:
>- Premise 1: If we had burnt too much fossil fuel in the 20th century, there would be too much carbon dioxide in the atmosphere.
>- Premise 2: If there was too much carbon dioxide in the atmosphere, global warming would be increasing.
>- Hidden premise: We burnt too much fossil fuel in the 20th century.
>- Sub-conclusion: Global warming is increasing.
>- Hidden premise: If global warming was increasing, life on earth would be in great danger.
>- Conclusion: Life on earth is in great danger.



## Present in standard form (1)

"Men can be feminists just as easily as women, since they can believe in and advance causes that help reduce unfair discrimination against women."

Present this argument in standard form and add hidden premises as needed to make it into a deductively valid argument!

## Present in standard form (2)

>- Conclusion: Men can be feminists just as easily as women.
>- The conclusion contains "feminists," but no premise contains that term.
>- So we need to add a premise that contains feminists.

. . . 

>- Premise: Men can easily believe in and advance causes that help reduce unfair discrimination against women.
>- Hidden premise: Whoever believes in and advances causes that help reduce unfair discrimination against women is a feminist.
>- Premise: Women can easily believe in and advance causes ... (this is needed, so that we can conclude that men can do this *just as easily* as women.)
>- Conclusion: Men can be feminists just as easily as women.

## Present in standard form

"No method of inference about the future will work if there is no uniformity in nature. But we can affirm that there is uniformity in nature, for induction works. This is amply confirmed by our experiences."

Write down this argument in standard form and supply any hidden premise needed for the argument to be valid.

. . .

>- Hidden premise: Induction is a method of inference about the future.
>- Premise: Induction works.
>- Sub-conclusion: A method of inference about the future works.
>- Premise: If there is no uniformity in nature, then no method of inference about the future will work.
>- Conclusion: There is uniformity in nature. (Denying the consequent!)

## Derive a valid conclusion (1)

Derive a valid conclusion using *all* the following sentences as premises. Write down the argument step by step, stating all the intermediate conclusions!

"Anything that disturbs others should not be allowed in the classroom. Babies are always noisy. All distracting things are disturbing others. Ringing mobiles are as noisy as babies. Noisy things are distracting."


## Derive a valid conclusion (2)

>- What is the ultimate conclusion? 
>- (Unstated): Babies and ringing mobiles should not be allowed in the classroom.
>- *Now try to ask a series of questions to find a valid reason to support each conclusion and each step of the reasoning process.*


## Derive a valid conclusion (3)

>- (Ultimate conclusion): Babies and ringing mobiles should not be allowed in the classroom.
>- Why should they not be allowed in the classroom?
>- Because they are disturbing others and things that disturb others should not be allowed in the classroom.
>- Why are they disturbing others?
>- Because they are distracting others and all distracting things are disturbing others.
>- Why are they distracting?
>- Because they are noisy and all noisy things are distracting.
>- How do we know that they are noisy?
>- Premise: Babies are always noisy.
>- Premise: Ringing mobiles are as noisy as babies.
>- Intermediate conclusion: Ringing mobiles are (also) always noisy.


## Derive a valid conclusion (4)

*Now put the argument together in the right order:*

Premise: Babies are always noisy.   
Premise: Ringing mobiles are as noisy as babies.   
Intermediate conclusion: Babies and ringing mobile phones are noisy.

. . .

Hidden premise: Babies and ringing mobile phones are noisy.  
Premise: Noisy things are distracting.  
Intermediate conclusion: Babies and mobile phones are distracting.

(continued on next slide)

## Derive a valid conclusion (5)

Hidden premise: Babies and mobile phones are distracting.  
Premise: All distracting things are disturbing others.  
Intermediate conclusion: Babies and mobile phones disturb others.

. . .

Hidden premise: Babies and mobile phones disturb others.  
Premise: Anything that disturbs others should not be allowed in the classroom.

. . .

Ultimate onclusion: Babies and mobile phones should not be allowed in the classroom.

(Valid; used all statements from the question).



## Hidden premises

"No one comes to our party. They must all hate us." -- Which of the following is a hidden premise of this argument?

1. All people hate us.
2. Only if people hate us they will not come to our party.
3. If people hate us they will not come to our party.
4. Unless people do not come to our party, they do not hate us.

. . .

Assume *H: All people hate us. C: People come to our party.*

\~C $\vdash$ H. Which premise is missing?

>- Answer 4 does not make sense (one "not" too much).
>- Answer 3 (H$\to$\~C) would affirm the consequent and give an invalid argument! Even if 3 was true, from the fact that they did not come to our party, we cannot conclude that they hate us.
>- Answer 1 obviously does not follow.
>- Answer 2 (H$\leftrightarrow$\~C) is correct.

## Hidden premise

"Peter is a bookworm, for he never plays with anyone." -- Which is the most appropriate hidden premise?

1. Bookworms usually do not play with anyone.
2. Only bookworms never play with anyone.
3. Peter likes to read books.
4. If Peter plays with other people, he's not a bookworm.

. . .

>- Answer 1 makes some sense, but is not conclusive. Because other people (let's say, computer geeks) also might not play with anyone. So Peter could be a computer geek instead of a bookworm if this hidden premise was true.
>- Answer 2 excludes the possibility of computer geeks fulfilling the criteria of the bookworms, and so produces a valid argument.

## Conclusion and hidden premises (1)

"The Electricity Company's business is public utility. What it does affects the lives of many people. So it does have social responsibilities. But it is not taking its social responsibilities seriously. If it did, it would not just be doing everything to maximise its own profits. The government should consider not renewing the Electricity Company's franchise."

What is the conclusion of this argument? Are there any hidden premises?

. . .

>- Conclusion: The government should consider not renewing the Electricity Company's franchise.

## Conclusion and hidden premises (2)

>- Premise: What the Electricity Company does affects the lives of many people.
>- Hidden premise: Whoever affects the lives of many people has social responsibilities.
>- Sub-conclusion: So the Electricity Company does have social responsibilities.

>- Premise: If a company takes its social responsibilities seriously, it does not do everything to maximise its own profits.
>- Hidden premise: The Electricity Company does everything just to maximise its own profits.
>- Sub-conclusion: The Electricity Company is not taking its social responsibilities seriously. (Denying the consequent.)

## Conclusion and hidden premises (3)

>- Hidden premise: The government should consider not renewing the franchise of public utilities that don't take their social responsibilities seriously.
>- Premise: The Electricity Company is a public utility.
>- Premise: The Electricity Company is not taking its social responsibilities seriously.
>- Ultimate conclusion: The government should consider not renewing the Electricity Company's franchise.


## Conclusion and hidden premises (1)

"The real justification for progressive taxation is that the wealthier receive more benefits from government and, therefore, ought to pay more. This may not be true in a strict accounting sense; the middle and upper classes cannot apply for public housing, for example. But the essence of government is to preserve the social order. The well-off benefit from this more than the poor."

(“Progressive taxation” means: The more money you have, the more taxes you pay. Wealthier people pay more taxes than poor people.)

What is the ultimate conclusion of this argument? Any hidden premises?


## Conclusion and hidden premises (2)

>- "The real justification for progressive taxation is that the wealthier receive more benefits from government and, therefore, ought to pay more. This may not be true in a strict accounting sense; the middle and upper classes cannot apply for public housing, for example. But the essence of government is to preserve the social order. The well-off benefit from this more than the poor."
>- Conclusion?
>- Conclusion: Therefore, the wealthier ought to pay more taxes.
>- Now take all the sentences of the argument apart, one by one, and see what function they have. Can or should we rearrange them?


## Conclusion and hidden premises (3)

Statements (in the original order):

- The real justification for progressive taxation is that the wealthier receive more benefits from government.
- Therefore, the wealthy ought to pay more taxes. 
- This may not be true in a strict accounting sense.
- ... because the middle and upper classes cannot apply for public housing, for example. 
- But the essence of government is to preserve the social order. 
- The well-off benefit from social order more than the poor.


## Conclusion and hidden premises (4)

Now arrange these statements in a logical order: 

>- Premise: The well-off benefit from social order more than the poor.
>- Premise: The wealthier receive more benefits from government.
>- *Hidden premise: Whoever receives more benefits ought to pay more taxes.*
>- (counter-argument) That the wealthier receive more benefits may not be true in a strict accounting sense (example: the middle and upper classes cannot apply for public housing) 
>- Premise: (counter-counter-argument): But the essence of government is to preserve the social order. 
>- Conclusion: Therefore, the wealthier ought to pay more taxes.


## Conclusion and hidden premises (5)

Another version of the same:

>- Premise: The essence of government is to preserve the social order.
>- Premise: The well-off benefit from preserving the social order more than the poor.
>- Sub-conslusion: The well-off benefit more from the government than the poor.

>- Premise: The well-off benefit more from the government than the poor.
>- Hidden premise: If a group of people receives more benefits from the government, then they ought to pay more taxes.
>- Conclusion: The well-off ought to pay more taxes.

## Conclusion?

In the following passage, what is the conclusion?

"Logic is difficult. Reasoning with abstract symbols is difficult, and logic, like mathematics, involves reasoning with abstract symbols."

1. Logic is difficult.
2. Reasoning with abstract symbols is difficult.
3. Logic is like mathematics.
4. Logic involves reasoning with abstract symbols.

. . .

Answer: 1.

## Different terms

Different teachers and books use slightly different terms. So:

- Sub-conclusion means the same as intermediate conclusion.
- Final conclusion (or just conclusion) means the same as ultimate conclusion.


# Analogies
 
## Fill in the blanks!

- Grass is to cows as _______ is to humans
- Wheels are to cars what _______ are to humans
- The sea is to a ship what _______ is to a car

## Fill in the blanks!

>- Grass is to cows as **rice** is to humans (not: “food”)
>- Wheels are to cars what **legs** are to humans
>- The sea is to a ship what **a road** is to a car

## Analogy

>- In *deduction,* we often draw a particular conclusion from a general rule.
>- In *induction,* we often draw a general conclusion from particular instances.
>- In *analogy,* we draw a particular conclusion from another, similar particular instance. Since the conclusion is not certain, analogies are a type of inductive argument.

## Analogy in science and law

>- Analogies are used in simulations. Every simulation of a process is an analogy.
>-  Sometimes analogies are used in teaching (electronic circuits are compared to water pipes).
>-  Analogies are used in law, when there is another similar case which has been decided on before.

## What do you think of this analogy?

- The universe is like a very complicated watch.
- A watch must have been designed by a watchmaker.
- Therefore, the universe must have been designed by some kind of creator.

## False analogies

>- An analogy can easily turn out to be false.
>- This happens when the things compared share only *some* features, but are *different* in others, and the differences are greater than the similarities.

## Universe and (mechanical) watch

![](graphics/15-universe-watch.png)\ 

So the real question is:  Is “having a creator” one of the *common* features or one of the *differences* between the two things? This question is not answered here!


## Form of analogical arguments

(Premise 1) Object A and object B are similar in having properties Q~1~ ... Q~n~.  
(Premise 2) Object A has property P.  
 ------------------------------------------------------------  
(Conclusion) Object B also (probably) has property P.

\vspace{3ex}

Note that P is not in Q~1~ ... Q~n~. If it was, the argument would be deductive!


## Example

(Premise 1) Object A and object B have common properties Q~1~ ... Q~n~.  
(Premise 2) Object A has property P.  
 ------------------------------------------------------------  
(Conclusion) Object B also (probably) has property P.

**Example:**

Paul is funny (Q1), likes to read Harry Potter (Q2), and he likes to listen to K-Pop music (Q3). I like Paul (P).  
Peter is funny (Q1). Peter likes to read Harry Potter (Q2). Peter likes K-Pop music (Q3).  
 ------------------------------------------------------------  
Because Paul and Peter share Q1...Q3, therefore I am justified in 
believing that they will also share property P, that means: that I
will also like Peter.  
**Conclusion:** I will (*probably*) also like Peter.


## Analogies as inductive arguments

>- This is why analogies are a kind of *inductive* argument!
>- If an analogy is weak or strong depends on the number and relevance of features that are similar in the two things that we compare to each other.

. . . 

**Example:**

>- Paul is 1.75cm in height and has black hair. He is a good friend of mine.
>- Peter is 1.75cm in height and has black hair too. He will surely also be a good friend to me.
>- Weak argument, because the similarities are not relevant to the conclusion: Height or hair colour are *not* important in friendships, therefore this argument is weak. 
>- In contrast, what one likes to read or the music one likes *is* important to friendship, therefore these features make a stronger analogical argument.








## Which analogy is better? Why?

1. Beijing is in the north of China and therefore it has cold, dry weather. Hong Kong is also in the north of China, and therefore it too has cold, dry weather.
2. Shenzhen is in the south of China, and therefore it has hot, humid weather. Hong Kong is also in the south of China, and therefore it too has hot, humid weather.

## Criterion 1: Truth

First of all we need to check that the two objects being compared are indeed similar in the way assumed.

For example, in the argument we just looked at, it is simply not true that Hong Kong is in the north of China. Therefore, the analogy number 1 is wrong.

## Which analogy is better? Why?

1. “This novel has a similar plot like the other one we have read, so probably it is also very boring.”
2. “This novel has a similar cover like the other one we have read, so probably it is also very boring.”

## Criterion 2: Relevance

Even if two objects are similar, we also need to make sure that those aspects in which they are similar are actually *relevant* to the conclusion.

## Irrelevant feature analogy

This is a common trick in politics, because it allows for easy solutions to complex problems. 

For example, the terrorists who hijacked an airplane all had long beards, and therefore now the government is increasing the security checks for people with long beards.

Generally, we need to ensure that having properties Q~1~ ... Q~n~ *actually increases the probability* of an object having property P. 

## Which analogy is better? Why?

1. “This novel is supposed to have a similar plot like the other one we have read. Additionally, it is written by the same author, and the same friend recommended them to me. So probably it is also very boring.”
2. “This novel is supposed to have a similar plot like the other one we have read and the same friend recommended them to me. So probably it is also very boring.”

## Criterion 3: Number 

If we discover a lot of shared properties between two objects, and they are all relevant to the conclusion, then the analogical argument is stronger than when we can only identify one or a few shared properties.

Suppose we find out that novel B is not just similar to another boring novel A with a similar plot. We discover that the two novels are written by the same author, and that very few of both novels have been sold. Then we can justifiably be more confident in concluding that B is likely to be boring novel. 

## Which analogy is better? Why?

1. “Italian restaurant A is very good. Restaurant B buys its ingredients from the same supplier, it uses the same brand of pasta, it uses the same olive oil, and all ingredients are of the same quality. So restaurant B will also be good.”
2. “Italian restaurant A is very good. Restaurant B buys its ingredients from the same supplier, both have lots of customers, and both restaurants have got an award from the same magazine. So restaurant B will also be good.”

## Criterion 4: Diversity

B uses the same olive oil in cooking as A, and buys meat and vegetables of the same quality from the same supplier. Such information of course increases the probability that B also serves good food. But the information we have so far are all of the same kind having to do with the quality of the raw cooking ingredients.

But if we are told that both restaurants have lots of customers, and that both restaurants have obtained awards from the same magazine, then these different aspects of similarities are going to increase our confidence in the conclusion a lot more.

## Which analogy is better? Why?

1. “Italian restaurant A is very good. Restaurant B buys its ingredients from the same supplier, both have lots of customers, and both restaurants have got an award from the same magazine. So restaurant B will also be good.”
2. “Italian restaurant A is very good. Restaurant B buys its ingredients from the same supplier, both have lots of customers, and both restaurants have got an award from the same magazine, and restaurant B has recently changed its owner. So restaurant B will also be good.”

## Criterion 4: Disanalogy

Even if two objects A and B are similar in lots of relevant respects, we should also consider whether there are dissimilarities between A and B which might cast doubt on the conclusion.

For example, returning to the restaurant example, if we find out that restaurant B now has a new owner who has just hired a team of very bad cooks, we would think that the food is probably not going to be good anymore despite being the same as A in many other ways.

## Criteria for a good analogical argument

1. Truth of similarities
2. Relevance of similarities
3. Number of similarities
4. Diversity of similarities
5. Absence of significant dissimilarities


## “Strong” and “weak” analogies

>- Analogies are a kind of inductive argument, because their conclusions are only *probable* (if the premises are true), but not *certain.*
>- The criteria above are ways in which we can increase the confidence that the conclusion will be true (given that the premises are true, that means, given that the similarities exist and are relevant).
>- So we could talk of “weak” and “strong” analogies, as we talk about weak and strong inductive arguments. (It is more common to speak of “good” and “bad” analogies, though).


## Is this analogy good? Why or why not?

We should not blame the media for having a bad influence on moral standards. Newspapers and TV are like weather reporters who report the facts. We do not blame weather reports for telling us that the weather is bad.

## Bad analogy

News don’t report the pure facts, but almost always add an attitude towards or evaluation of the facts. It is this evaluation that might be problematic. 

Weather reports do not change the weather, but newspaper reports and the public media can influence people and have an indirect effect on moral standards.

## Is this analogy good? Why or why not?

Democracy does not work in a family. Parents should have the ultimate say because they are wiser and their children do not know what is best for themselves. Similarly the best form of government for a society is not a democratic one but one where the leaders are more like parents and the citizens have to obey them.

## Bad analogy

There are many relevant ways in which a family is different from a society. 

- First, the government officials need not be wiser than the citizens.
- Also, many parents might care for their children out of love and affection but government officials might not always have the interests of the people at heart.


## What did we learn today?

- How to restructure and analyse arguments.
- What analogies are, and how to evaluate them.

## Any questions?

![](graphics/questions.jpg)\ 

## Thank you for your attention!

Email: <matthias@ln.edu.hk>

If you have questions, please come to my office hours (see course outline).
