% Critical Thinking
% 1. Introduction
% A. Matthias

# Introductions

## Introduction

My name is: **Andreas Matthias**

If this is too difficult, you can call me: “Mr. Ma (\cn{馬逸文})” or “Andy”

## Introduction

![Last I lived in Goettingen, in Germany](graphics/01-goe1.png)\ 

## Introduction

![This is how shopping streets look like there...](graphics/01-goe2.png)\ 

## Introduction

![This is the “Girl with geese”, symbol of the city](graphics/01-goe3.jpg)\ 

## Introduction

![I actually come from Greece](graphics/01-map1.png)\ 

## Introduction

![](graphics/01-greece1.png) \ 

## Introduction

![](graphics/01-greece2.png) \ 

## Introduction

![Back to work!](graphics/01-greece3.png)\ 

## Moodle

In this course, I use Moodle to give you the presentations and
other materials.

(Demonstration of Moodle)

## Course outline

(See outline in Moodle)

## Old presentations warning

Please do NOT use old materials!

This course has changed significantly since last term. Old presentations will NOT be useful.

Please download the NEW presentations from Moodle each week! Additionally, these presentations are much nicer to print and don't use up so much space on paper!

## Big List of Bad News

**1. You need to work**

Lingnan University Guidelines for Learning:

“For each hour of class contact, the expectation is that students will undertake 2 additional hours of personal study.”

We have 3 hours per week. This means that I expect you to revise
the material and do the homework for 6 full hours each week.

## Big List of Bad News

**2. You need to speak enough English**

Lingnan University and I expect you to be able to follow this course in English. You will also have English writing assignments.

If you cannot handle these, it is your responsibility to do something about it:

* Watch movies in English.
* Read English books.
* Go to the language centre (CEAL) for help!

## Big List of Bad News

**3. You need to be on time**

Attendance is mandatory. I take the attendance every time you say something in class.

* If you miss a test without excuse, you get 0 points. There are no make-up opportunities for in-class tests.
* You need to arrive here on time (max. 5 minutes late)
* If you repeatedly come later than that, your grade will go down.
* If you come more than 15 minutes late, you will be considered absent for this session.

## Big List of Bad News

**4. You need to participate in class**

* A significant percentage of your grade is participation in class!
* This means that you need to actively *say* something, not just sit there.
* I record your performance every time you say something. If you don't participate, your participation is a "C".
* I also record it in my list every time I see you play with your phone or work on materials for other classes. If you do these things, you get a participation grade “F” for this session. This will affect your final grade.

*So be careful what you do in class. Participate. Switch off your phone. Don’t read materials other than those for this class.*

## Big List of Bad News

**5. Final grade distribution (approx.):**

* \~20% A
* \~45% B
* \~40% C
* D and F as needed. Normally a little less than 5% D’s and one or two F per term (through absence or cheating).

This distribution is required by the University and will always be the same, regardless of your performance.

So make sure you are in the best 20% of the class if you want an A.

## Questions?

Any questions about the requirements and the grading?

# Groupwork

## We'll work in groups (1)

Groups stay together until the end of term.

If you help each other, you'll have better grades.

## We'll work in groups (2)

Let's organize ourselves into groups of 2 or 3.

Please not more than 3 participants in a group!

## Get to know your group

Take five minutes.

Sit together, and find out about each other's:

* Name, email address, major subjects
* Favorite way to spend a free afternoon
* Favorite place (in HK or elsewhere)
* Favorite music

# What is critical thinking?

## "Thinking" exercise (3 minutes)

Name situations and actions that have to do with "thinking."

## "Thinking" exercise

Name situations and actions that have to do with "thinking."

> - Playing chess
> - Doing mathematics
> - Reading a book
> - Writing a song
> - Thinking of my girlfriend
> - Shopping, going to the restaurant …

## "Thinking" exercise

- Playing chess
- Doing mathematics
- Reading a book
- Writing a song
- Thinking of my girlfriend
- Shopping, going to the restaurant …

*What is common in all this?*

## "Thinking"

Thinking ...

> - Is a function of the brain
> - Is something we do on purpose
> - Usually has an aim, a target

## "Critical" thinking

All this is thinking, somehow.

... but what means *critical* thinking?

## Exercise: The word "critical" (3 minutes)

What other uses of the word "critic(al)" do you know?

## “Critical”

Uses of “critic/al:”

> - To criticize someone
> - To be critical
> - To be a (cinema) critic
> - A critical condition of a hospital patient
> - A political crisis

## “Critical”: two meanings

### One:
- To criticize someone
- To be critical
- To be a (cinema) critic

### Two:
- A critical condition of a hospital patient
- A political crisis

## Ancient Greek: “κρίσις”

Ancient Greek: “κρίσις”

* means the ability to judge things (to be critical, to be a critic, to criticize)
* or: it can mean a point of decision (a critical stage, a crisis)

## To criticize = to judge

“Critical thinking” = “Judging” thinking

## Exercise (3 minutes)

What does it mean “to judge”?

Write down 5 elements of “judging” or five cases in which
someone judges something!

## Is this critical “judgment”?
“Somehow I like the right picture better than the left.”

![](graphics/01-pic12.png) \ 

## “Opinion” vs. “judgement”

### Opinion:
> - An *opinion* is personal, subjective.
> - Another person can agree or disagree equally well (“But I like the left picture better!!!”)

### Judgement:
> - A *judgement* is supported by reasons.
> - Another person cannot disagree without better reasons.

## An opinion:

> - “Orange juice is much better than coke!”
> - “I disagree! Coke is better!”

## A judgement:

> - “Orange juice is much better than coke, because it has vitamins, it is healthy, it doesn't have added sugar and preservatives and it has less calories.”
> - “ . . . ”

## Critical judgment

To *judge critically* means:

> - To decide between two or more alternatives based on reasons and logical thinking.
> - Such a judgment is not just a personal opinion, but supported by reasons.
> - It can only be *refuted* (rejected, shown to be false) by better reasons.

## Exercise (5 minutes)

Is it better to live in Central or in the New Territories?

Decide on one answer in your group, then give a list of reasons for your choice.

# A little history

## Ancient Greece

![Critical thinking, in its modern form, begins in ancient Greece (\cn{希臘})](graphics/01-map1.png)

## Athens (\cn{雅典})

![Athens is here.](graphics/01-map3b.png)

## Aristotle (\cn{亞里士多德})

![Aristotle lived in Athens in 384-322 BC (one century later than Confucius \cn{孔子}).](graphics/01-aristotle.png)

## Aristotle (\cn{亞里士多德})

![He laid the foundations of modern logic.](graphics/01-aristotle.png)

## Exercise: Why the Greeks?

Can you imagine why particularly the ancient Greeks should have advanced logic?

Think about it for a moment!

(What was the particular thing about ancient Athens other cultures of that time did not have?)

## The Athenians

The people of Athens...

> - ... had a democracy.
> - In a democracy all citizens decide together.
> - In order to influence the decision, one has to convince the others.
> - This led to the advancement of *rhetoric* (the art of speaking in public) and logic.

## Critical Thinking today

So why is Critical Thinking important for you?

> - Today you live (or will live) in democratic societies.
> - As citizens you decide about the future together.
> - In order to participate in decisions, you have to understand and use arguments and reasons.
> - You have to know how to convince others.
> - And you have to be able to spot mistakes in others' thoughts.

## Any questions?

![](graphics/questions.jpg)\ 

## Thank you for your attention!

Email: <matthias@ln.edu.hk>

If you have questions, please come to my office hours (see course outline).
