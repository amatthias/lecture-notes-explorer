% Critical Thinking
% 15. Argument mapping
% A. Matthias

# Where we are

## What did we learn last time?

- Fallacies

## What are we going to learn today?

- How to map arguments

# Argument mapping

## Why map arguments?

>- What is the purpose of argument mapping?
>     - To show the structure of an argument more clearly.
>     - To help identify premises, intermediate conclusions, and final (ultimate) conclusions.
>     - To show clearly how the premises for a conclusion are connected with each other.
>- In this way, we can understand an argument’s structure better.
>- Argument mapping has been shown in studies to improve critical thinking skills.

## Ways to map arguments

>- There are many different ways one can use to map arguments.
>- We will discuss three:
>     - The argument mapping style discussed in HKU Think
>     - The “standard diagram” style presented by Reed and Rowe (2007).
>     - A more loose, less formal, mindmap-style approach to real-world debates, which we will use in the later sessions of this course (the debate preparation sessions).

## How does it work?

>- Each proposition (premise or conclusion) is numbered.
>- The numbers are arranged in a specific way on the page, connected by arrows.
>- Each arrow is a logical inference.

## Standard diagram elements

>- A “standard diagram” contains three kinds of elements:
>     - **Dependent premises or co-premises:** Two premises that both need to be true in order to derive the conclusion.
>     - **Independent premises:** Each premise stands on its own and supports a conclusion.
>     - **Intermediate or sub-conclusions:** Conclusions that are used as premises for another conclusion further down the argument.


## Dependent premises or co-premises

Statements 1 and 2 must both be true in order to support the conclusion:

![](graphics/15-amaps1.jpg)\ 




## Independent premises

Statements 2, 3, 4 each support the conclusion independently:

![](graphics/15-amaps2.jpg)\ 




## Intermediate or sub-conclusions

Statement 5 supports statement 4, which is a sub-conclusion. 4, together with the independent premises 2 and 3, supports the ultimate conclusion 1:

![](graphics/15-amaps3.jpg)\ 





## Counter-arguments (1)^[from: By Sujai Thomman - Own work - made with The Reasoning PowerPoint App, CC BY-SA 4.0, https://commons.wikimedia.org/w/index.php?curid=54708372]

![](graphics/15-amaps4.png)\ 


## Counter-arguments (2)

>- Observe how you can mark counter-arguments by putting a minus sign (“-”) at the end of the arrow. 
>- You can do this with the other style we saw before, too. Just put a minus sign at the arrow’s tip.


## How to diagram a text

Here is one way to do it:^[Beardsley, Monroe C. (1950). Practical logic. New York: Prentice-Hall.]

>- Use brackets to separate individual statements in a text.
>- Number the statements.
>- Circle the logical connectives and other structural words indicating conclusions and so on.
>- Add any logical indicators that are left out.
>- (Add any missing or hidden premises and conclusions.)
>- Draw your diagram.


## Full example (Wikipedia)

![](graphics/15-amaps5.png)\ 


![](graphics/15-amaps6.jpg)\ 



## HKU Think diagrams

>- The HKU Think resource^[https://philosophy.hku.hk/think/arg/complex.php] uses a slightly different way of drawing argument maps.
>- The structure is the same, only the way of drawing the arrows is different (and, I think, more confusing).

## HKU Think: Single premise, single conclusion

“Death is inevitable. So life is meaningless.”

![](graphics/15-amap10.png)\ 

## Co-premises

“(1) Paris is in France, and (2) France is in Europe. So obviously (3) Paris is in Europe.”

![](graphics/15-amap11.png)\ 



## Independent premises

“(1) Smoking is unhealthy, since (2) it can cause cancer. Furthermore, (3) it also increases the chance of heart attacks and strokes.”

![](graphics/15-amap12.png)\ 


In your diagrams, better separate the premises’ lines at the end, keeping two separate arrows going into the conclusion’s circle. Otherwise it might look like these are co-premises!

## One reason, multiple conclusions

“(1) Gold is a metal. (2) So it conducts electricity. (3) It also conducts heat.”

![](graphics/15-amap13.png)\ 


## Exercise

This computer can think. So it is conscious. Since we should not kill any conscious beings, we should not switch it off.

*Do it yourself: create an argument map!*

## Solution

[1] This computer can think. So [2] it is conscious. Since [3] we should not kill any conscious beings, [4] we should not switch it off.

![](graphics/15-amap15.png)\ 



## More complex arguments (1)

“Mary cannot come to the party because her scooter is broken. Peter also cannot come because he has to pick up his new hat. I did not invite my other friends, so none of my friends will come to the party.”

*Do it yourself!*


## More complex arguments (2)

First rewrite the argument in standard form:

1. Mary cannot come to the party.
2. Mary's scooter is broken.
3. Peter cannot come to the party.
4. Peter has to pick up his new hat.
5. I did not invite my other friends.
6. [Conclusion] None of my friends will come to the party.

![](graphics/15-amap14.png)\ 


## Exercise

Many people think that having a dark tan is attractive. But the fact is that too much exposure to the sun is very unhealthy. It has been shown that sunlight can cause premature aging of the skin. Ultraviolet rays in the sun might also trigger skin cancer.
 
*Create an argument map!*

## Solution

 [1. Many people think that having a dark tan is attractive.] [2. But the fact is that too much exposure to the sun is very unhealthy.] [3. It has been shown that sunlight can cause premature aging of the skin.] [4. Ultraviolet rays in the sun might also trigger skin cancer.]

![](graphics/15-amap16.png)\ 

Sentence [1] is not a counter-argument! It is not related to the conclusion at all. This is why we leave it out in the diagram.

## Exercise

If Mary is here, then Peter should be here as well. It follows that if Peter is not here, Mary is also absent, and indeed Peter is not here. So most likely Mary is not around.

*Create an argument map!*

## Solution

[1. If Mary is here, then Peter should be here as well.] [2. It follows that if Peter is not here, Mary is also absent,] and indeed [3. Peter is not here.] So most likely [4. Mary is not around.]


![](graphics/15-amap17.png)\ 



## Exercise

Marriage is becoming unfashionable. Divorce rate is at an all time high and living together without being married is increasingly presented in a positive manner in the media. Movies are full of characters who live together and unwilling to commit to a lifelong partnership. Even newspaper columnists recommend people to live together for an extended period before marriage in order to test their compatibility.

*Create an argument map!*

## Solution

[1. Marriage is becoming unfashionable.] [2. Divorce rate is at an all time high], and [3. living together without being married is increasingly presented in a positive manner in the media]. [4. Movies are full of characters who live together and unwilling to commit to a lifelong partnership]. [5. Even newspaper columnists recommend people to live together for an extended period before marriage in order to test their compatibility.]

![](graphics/15-amap18.png)\ 



## Exercise

All university students should study critical thinking. After all, critical thinking is necessary for surviving in the new economy, as we need to adapt to rapid changes, and make critical use of information in making decisions. Also, critical thinking can help us reflect on our values and purposes in life. Finally, critical thinking helps us improve our study skills.

*Create an argument map!*

## Solution

[1. All university students should study critical thinking.] After all, [2. critical thinking is necessary for surviving in the new economy] as [3. we need to adapt to rapid changes, and make critical use of information in making decisions.] Also, [4. critical thinking can help us reflect on our values and purposes in life.] Finally, [5. critical thinking helps us improve our study skills.]

![](graphics/15-amap19.png)\ 


# More exercises (from previous lectures)

## Exercise

"The world would be better if there was a single world government. If there was such a government, then there would be no wars. There would also be much more economic cooperation and free trade between the different regions of the world. On the other hand, if there was a world government, there would be more administration costs. But these would be outweighed by the benefits of having a world government."

*Create an argument map!*

## Considerations

- Conclusion: The world would be better if there was a single world government.
- Does the speaker believe that there would be more administrative costs if there was a world government? -- Yes.
- Does this belief support the conclusion? -- No. This is a counter-argument, but he dismisses it, because the benefits would outweigh the additional costs.

## Exercise

"To punish people merely for what they have done would be unjust, because the forbidden act might have been an accident for which the person who did it cannot be held to blame."

*Create an argument map!*

. . .

>- You can see that the conclusion contains the word "unjust," for which there is no premise!
>- So the argument cannot be valid.
>- We need a premise to connect "cannot be blamed" with "unjust."

## Argument analysis

The explicit premises:

The forbidden act might be an accident.  
Persons cannot be blamed for (some) accidents.  
 --------------------------------------------------------------  
To punish people merely for what they have done would be unjust.

You can easily see that this argument is not valid. There are no premises about "unjust," and so the conclusion does not follow from the premises. *What is the hidden premise?*

. . .

Hidden premise: it is unjust to punish people, if they cannot be blamed for an act.

*Now you can draw the argument map.*


## Exercise

"Democracy is not perfect, since it has many problems. For example, it is much more difficult for a leader of a democracy to implement unpopular policies than it is for a leader of a non-democratic government, even when those policies are the best policies. Nevertheless, democracy is the best form of government there is, since other forms of government have even bigger problems. This is why Hong Kong should have a democracy."

*Create an argument map. Present this argument in standard format. Identify the final conclusion as well as the premises and sub-conclusions. Do not include any premises or sub-conclusions that don't support the main conclusion of the argument!*

## Argument analysis

>- Conclusion: Hong Kong should have a democracy.
>- In the conclusion we have "Hong Kong," but not in the premises.
>- So we need to create a premise that mentions Hong Kong in a way that it can then be used in the conclusion.
>- You also see that the whole section from "For example" to "best policies" is a counter-argument. According to the instructions, we don't need to include it in our answer.

## Argument analysis

"Democracy is not perfect, since it has many problems. \textcolor{red}{For example, it is much more difficult for a leader of a democracy to implement unpopular policies than it is for a leader of a non-democratic government, even when those policies are the best policies.} Nevertheless, democracy is the best form of government there is, since other forms of government have even bigger problems. This is why Hong Kong should have a democracy."

>- Premise: Other forms of government have bigger problems.
>- Sub-conclusion: Democracy is the best form of government there is.
>- Hidden premise: Hong Kong should have the best form of government.
>- Conclusion: Hong Kong should have a democracy.
>- The other premises and subconclusion do not support this conclusion, but form a counter-argument.


## What did we learn today?
- How to map arguments

## References
HKU OpenCourseWare on critical thinking, logic, and creativity:

http://philosophy.hku.hk/think


## Any questions?

![](graphics/questions.jpg)\ 

## Thank you for your attention!

Email: <matthias@ln.edu.hk>

If you have questions, please come to my office hours (see course outline).





