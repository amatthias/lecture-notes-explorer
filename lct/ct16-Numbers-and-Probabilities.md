% Critical Thinking
% 16. Numbers and Probabilities
% A. Matthias

# Where we are

## What did we learn last time?

- Argument maps

## What are we going to learn today?

- How to calculate basic probabilities
- Probabilities of independent events
- Conditional probabilities
- Risks, benefits, expected values
- Utility
- Conjunction fallacy

# Probabilities

## Basic probabilities

An important part of critical thinking is the ability to think in probabilities and to avoid probability errors.

Let's start with an easy one:

You have a sack filled with socks. There are 200 white socks and 100 red socks in there. If I take out one sock, with what probability will it be a white sock?

. . . 

$\frac{2}{3}$ or 0.67, if the socks are mixed well.

## Basic probabilities

>- We express probabilities usually in fractions or in numbers between 0-1.
>- 0 is the probability of an event which can not occur.
>- 1 is the probability of an event which is *certain* to occur.

. . . 

How to calculate it:

Probability = $\frac{number\ of\ ‘interesting’\ outcomes}{number\ of\ all\ outcomes}$

p = $\frac{200\ (white\ socks)}{300\ (all\ socks)}$ = $\frac{2}{3}$


## Basic probabilities

You have a sack filled with socks.  There are 200 white socks and 100 red socks in there. If I take out ten socks (putting every sock back after I took it out), how many white and how many red socks do I expect to have seen?

. . . 

We expect $\frac{2}{3}$ of the socks (around 6-7) to be white, and $\frac{1}{3}$ (around 3-4) to be red.

## One more question

I have a sack of 34 red and 122 white socks. How often do I have to draw a sock from the sack in order to be *sure* to have a pair of the same colour?

. . . 

3 times. (This has nothing to do with probabilities).


# More probabilities

## Try it yourself!^[This and following from: http://www.teachervision.fen.com/tv/printables/Math_5_CT_12.pdf]

![](graphics/17-games-2.png)\ 

At the school fair, two spinner games offer prizes you would like to win. One offers a CD if the arrow points to an R; the other offers a computer game if it points to a 2. You have money to play only one game. Which game gives you the greater chance of winning a prize?

## Solution
 
The second game.

\Large{$\frac{2}{5}>\frac{3}{8}$}

## Try it yourself:

![](graphics/17-games-2.png)\ 

There is also a dice game at the fair. Each of 2 dice are numbered 1–6. If you roll *a sum of* 3, 4, 5, or 6, you win a DVD. Is the chance of winning a DVD greater than the chance of winning a CD?

## Solution (1)

Yes, the chance for the DVD is greater:

\Large{$\frac{7}{18} > \frac{3}{8}$}

## Solution (2)

Make a table of possible outcomes. Those with a star have the right sum:

```
11		21*	    31*		41*		51*		61
12*		22*		32*		42*		52		62
13*		23*		33*		43		53		63
14*		24*		34		44		54		64
15*		25		35		45		55		65
16		26		36		46		56		66
```

\Large{$\frac{14}{36} = \frac{7}{18}$}

## Solution (3)

The easier (?) way:

>- With 2 dice we have 6x6=36 possible outcomes.
>- Of these, one half are the same like the other half: (1,5) is the same like (5,1) and so on.
>- So we have 18 unique outcomes, and of those 7 have a sum of 3, 4, 5, or 6.
>- Thus: \Large{$\frac{7}{18}$}

## Probabilities of independent events^[Source for this and the following pages: http://www.mathgoodies.com/lessons/vol6/independent_events.html]

Two events, A and B, are *independent* if the fact that A occurs does not affect the probability of B occurring.

. . . 

Examples:

>- I pick one sock out of a sack, replace it, and pick a second.
>- It rains outside, and I drop my pen.

## Probabilities of independent events

Multiplication rule:

When two events, A and B, are independent, the probability of both occurring is:

\f{P(A and B) = P(A) · P(B)}

## Probabilities of independent events

A coin is tossed and a single 6-sided die is rolled. Find the probability of landing on the head side of the coin and rolling a 3 on the die.

. . . 

P(head) = $\frac{1}{2}$

P(3)	= $\frac{1}{6}$

P(head and 3) = P(head) · P(3) = $\frac{1}{2}$ · $\frac{1}{6}$ = $\frac{1}{12}$

## Probabilities of independent events

A large basket of fruit contains 3 oranges, 2 apples, and 5 bananas. Now it is dark and you grab a piece of fruit at random. What is the probability of grabbing an orange or a banana?

. . . 

- Probability for an orange: $\frac{3}{10}$ or 0.3

- Probability for a banana: $\frac{5}{10}$ or 0.5

- Probability to get the one *or* the other:  0.3+0.5 = 0.8 = 80%

## Probabilities of independent events

>- In the previous examples we made sure that the events did not change the probability of subsequent events.
>- For example, when I picked a sock, I *replaced it first,* before picking another sock. 
>- This is not necessary, if the population is very big. Then not replacing the sample has only a very small effect which can normally be disregarded.

## Probabilities of independent events

A nationwide survey found that 72% of people in the United States like apples. If 3 people are selected at random, what is the probability that all three like apples?

. . .

Let L represent the event of randomly choosing a person who likes apples from the U.S.:

P(L) · P(L) · P(L) =  (0.72)·(0.72)·(0.72) = 0.72^3^ = 0.37 = 37%

## Probabilities

According to a survey, in 7 children only 2 like salad.

Your mother has invited 4 children to the birthday of your little brother, and is thinking of making a salad.

How high is the probability that *all four children* will actually like the salad?

. . . 

Kids who like salad = $\frac{2}{7}$ = 0.285 = 28.5%

Probability of 4 children to like salad:

0.285 · 0.285 · 0.285 · 0.285 = 0.285^4^ = 0.006 or 0.6%

(better serve cookies instead!)

## Probabilities

According to a survey, 3 out of 4 students like pop music, while only 1 out of 6 likes classic music.

You have to organize some classic music to be played at the birthday of your best friend (6 guests) and some pop music for your department's end of term celebration (60 students).
Assuming the guests are a random selection of students, which music is more probable to be liked by all invited guests?

. . . 

- Probability to like classic music = $\frac{1}{6}$ = 0.166

- Probability to like pop music = $\frac{3}{4}$ = 0.75

- Probability of 6 people to like classic: 0.166^6^ = 0.0000214
- Probability of 60 people to like pop: 0.75^60^ = 0.0000000318
- Probability for pop is around a 1000 times lower!

# Conditional probabilities

## Conditional probabilities

>- The multiplication rule applies only to *independent* events.
>- If the events are *not* independent, we need to use a way of calculating the probability of an event B happening, *given that another event A has already happened.*
>- We call P(B|A) the “probability of B given that A has happened.”
>- P(A and B) is the probability of the events A and B happening.

. . . 

Then:

\Large{\f{P(B|A) = $\frac{P(A\ and\ B)}{P(A)}$}}

## Example:

A teacher gave her class two tests. 20% of the class passed both tests and 30% of the class passed the first test. What percent of those who passed the first test also passed the second test?

. . .

![](graphics/17-class-test.jpg)\ 

P(Second\ |\ First) = $\frac{P(First\ and\ Second)}{P(First)}$ = $\frac{0.2}{0.3}$ = 0.667 = 67%

(20 is 67% of 30)

## Conditional probabilities

We can also turn this formula around, so that we can compute the combined probability of two events happening, depending on the probability of the first event and the probability of the *second given the first:*

\f{P(A and B) = P(A)·P(B|A)}

## All probability formulas

\f{Probability = $\frac{number\ of\ ‘interesting’\ outcomes}{number\ of\ all\ outcomes}$}

Independent events (multiplication rule):

\f{P(A and B) = P(A) · P(B)}

Conditional probability (B given that A):

\f{P(B|A) = $\frac{P(A\ and\ B)}{P(A)}$}

\f{P(A\ and\ B) = P(A)·P(B|A)}

## Conditional probabilities

The probability that it is Friday and that a student is absent is 0.03. Since there are 5 school days in a week, the probability that it is Friday is 0.2.

What is the probability that a student is absent *given that today is Friday?*

. . . 

P(Absent\ |\ Friday) = $\frac{P(Friday\ and\ Absent)}{P(Friday)}$ = $\frac{0.03}{0.2}$ = 0.15 = 15%

## Conditional probabilities

At Lingnan University, the probability that a student takes CCC-8001 Logic and Critical Thinking and CCC-8003 Understanding Morality both taught by Andy is 0.087. The probability that a student takes Critical Thinking taught by Andy is 0.68. What is the probability that a student takes Andy’s Understanding Morality given that the student is taking Andy’s Critical Thinking?

. . .

P(UM | LCT) = $\frac{P(LCT\ and\ UM) }{P(LCT)}$ = $\frac{0.087}{0.68}$ = 0.128 = 12.8%

## Exercise

In New York State, 48% of all teenagers own a skateboard and 39% of all teenagers own a skateboard and roller blades. What is the probability that a teenager owns roller blades given that the teenager owns a skateboard?

. . .

P(rb | s) = $\frac{P(s\ and\ rb)}{P(s)}$ = $\frac{0.39}{0.48}$ = 0.8125 = 81%

## Exercise

The government has estimated the following survival probabilities for men:

- probability that a man lives at least 70 years: 80%
- probability that a man lives at least 80 years: 50%.

What is the conditional probability that a man lives at least 80 years given that he has just celebrated his 70th birthday?

. . .

- P(e): Probability that a man lives at least eighty years
- P(s): Probability that a man lives at least seventy years

P(e | s) = $\frac{P(s\ and\ e)}{P(s)}$ = $\frac{0.5}{0.8}$ = 0.625 = 62.5%

## How to do these calculations in the final exam

>- In the final exam you are not allowed to use a calculator! (Or any other electronic device).
>- Therefore, you would answer the previous question by writing:
>- P(e | s) = $\frac{P(s\ and\ e)}{P(s)}$ = $\frac{0.5}{0.8}$ (and stop here).
>- You don't need to make the actual final calculation, but you *do* need to fill in all the values into the formulas you use, so that only that final calculation is missing.

# Probability fallacies

## Correct or incorrect?

![](graphics/17-roulette.jpg)\ 

Fred is playing roulette in a Macau casino. The roulette wheel has 36 numbers (ignoring the zero), of which half are red and half are black. In the last ten rounds, all the winning numbers have been red. So Fred thinks that now there must be more black numbers than red numbers coming up, and that he would have a better chance of winning if he bets on black.

## Correct or incorrect?

Incorrect! The outcomes of the previous ten rounds can have no effect on the motion of the wheel and the ball; past outcomes can’t affect future outcomes. In other words, the outcomes are independent. The probability of a black number is still $\frac{1}{2}$ in each round, irrespective of what has come before.

## Getting an “A”

The probability of getting an “A” is 20% (due to the prescribed distribution of grades), so the probability of the same person to get 3 “A”s in 3 different classes must be (0.2)^3^=0.008 or 0.8%. Correct?

. . . 

Not correct.

Some people are better students, and they are more likely to get an “A” than others. So if someone already has 2 “A”s, then the probability for him to get another “A” is much higher than the normal 20%.

Since the events are *not* independent, we would need to calculate the probability of getting a 3rd A *given that the same student already has 2 “A”s* (which would be a higher number than 0.2)

## Conjunction of events

Bill is 34 years old.  He is intelligent, but boring.  In school, he was strong in mathematics but weak in social studies and humanities.

How probable are the following propositions? Order them according to their probability of being true!

A:  Bill is an accountant who plays jazz for a hobby.  
B:  Bill is an accountant.  
C:  Bill plays jazz for a hobby.

. . .

>- Many students rank the propositions B>A>C.
>- That is, they say that it is more probable that Bill is an accountant who plays jazz than a jazz player.
>- But this must be wrong!
>- For any two events, P(A&B) must always be lower or equal to either P(A) or P(B)!

## Conjunction fallacy

This is called the *conjunction fallacy.*

Sometimes, a statement that contains a conjunction of propositions seems to be more probable than the same propositions by themselves. But this must be wrong.

To be an accountant who plays jazz must always be less probable than to be a jazz player, since all accountants who play jazz are included in the set of jazz players. But additionally they must also fulfil another criterion, that of being accountants. So they must be fewer than the jazz players in general!

# Risks and benefits

## Risks and benefits

When we talk about risks and benefits, it is not enough to just calculate probabilities!

For example, how would you choose in these two cases:

1. You can play a game where you have an 80% chance of winning 100 dollars, and a 20% chance of losing. If you lose, you have to pay 1 dollar. Do you play?
2. You can fly with this very old airplane or take the train. There’s a 90% chance that the plane will make it, but there’s also a 10% chance that it may crash. In this case you will be killed. The train just takes one hour longer than the flight to the same destination and is perfectly safe. Do you take the plane or the train?

## Risks

>- Most people asked would decide to play the game but refuse to fly with the plane, although the chances to win the game are less than the chances of surviving the flight!
>- This shows that in judging risks we don’t look at the probabilities alone, but also at the expected gain.
>- To lose your life is an outcome which is so bad that a 10% chance seems unacceptable. A 20% chance of losing one dollar, on the other hand, seems okay if I can win 100 dollars.

## Would you play this game?

I toss two coins, and I pay you $2 if they both show heads but you pay me $1 if one or both show tails. -- *Calculate how much you can expect to win in the long run!*

. . . 

The **expected value** of this bet is obtained by multiplying the probability of each outcome by its value *to you,* and then adding the results.

-----------   ------------------   ----------------
HH	  			   		     +\$2          Probability 1/4
HT	  				  	     -\$1           Probability 1/4
TH	  					     -\$1           Probability 1/4
TT	  						 -\$1           Probability 1/4
----------------------------------------------

. . . 

So the expected value *to you* of the bet is

(**2**∙$\frac{1}{4}$) - (**1**∙$\frac{3}{4}$) = $\frac{2}{4}$-$\frac{3}{4}$ =  -$\frac{1}{4}$ =  **-$0.25**

**Bold**: dollars won or lost; fractions: probability.

## Expected value

>- The expected value does *not* mean that you should expect to lose $0.25 on each bet, since you will either win $2 or lose $1. 
>- But it tells you something about what you should expect in the long run.
>- If you bet over and over again, you will win some and lose some, but eventually your winnings and losses will average out to a loss of $0.25 per game. 
>- In the long run, this bet will not be financially worthwhile to you (but it will be worthwhile to me!)

## Fair bet

*A bet with an expected value of zero is called a fair bet.*

For example, if the above bet is modified so that I pay you $3 if both coins show heads, then it is fair:

-----------   ------------------   ----------------
HH	  			   		     +\$3          Probability 1/4
HT	  				  	     -\$1           Probability 1/4
TH	  					     -\$1           Probability 1/4
TT	  						 -\$1           Probability 1/4
----------------------------------------------

(**3**∙$\frac{1}{4}$) - (**1**∙$\frac{3}{4}$) = $\frac{3}{4}$-$\frac{3}{4}$ = **0**

(**Bold**: dollars won or lost; fractions: probability.)

>- It is fair in the sense that there is no built-in bias regarding the expected value in favor of either one of us.
>- But “fair” does *not* mean that we have the same chances of winning (see table!) 

# Utility

## Utility

>- Similar to the concept of expected value is the concept of utility.
>- Here we don’t have a gain or loss which can be expressed in numbers, but we try to express it in numerical form anyway, in order to be able to do a calculation of expected values.
>- The utility of an outcome provides a numerical measure of how good or bad the outcome is.

## Utility

Suppose I am trying to decide whether to take my umbrella with me today.
 
There are four possible outcomes, because it may or may not rain, and I may or may not take my umbrella. 

## Utility (2)
>- If it rains, it is clearly better to take my umbrella than not, but if the weather is fine, then it is marginally better not to take my umbrella, so that I have fewer things to carry.
>- The best outcome for me is if it's fine and I don't take my umbrella, so I'll assign that outcome a utility of +10. 
>- The outcome in which it's fine and I do take my umbrella is only marginally worse than that, so I'll assign it a utility of +9. 
>- The worst outcome is if it rains and I don't take my umbrella; I'll assign it a utility of -10.
>- The outcome in which it rains and I do take my umbrella is somewhere in the middle, so I'll assign it a utility of 0.

## Utility (3)

The situation can be expressed in a table:

\begin{tabular}{|c||c|c|}
\hline
\ & Umbrella & No umbrella \\
\hline
Rain & 0 & -10 \\
\hline
Fine & +9 & +10 \\
\hline
\end{tabular}

\vspace{3ex}

Now the weather forecast tells you that there is a 20% chance of rain today. Should you take your umbrella?

## Solution

Expected utility of taking the umbrella:

![](graphics/17-utility1.png)\ 

Expected utility of not taking the umbrella:

![](graphics/17-utility2.png)\ 

Since the expected utility of taking my umbrella is greater, I should take my umbrella.

## Calculate the utility!

Fred is short-sighted, and he is considering laser surgery to correct his vision.

He does some research, and finds that the chance of a complete correction is 46%, the chance of a partial correction is 44%, the chance of no change in vision is 9% and the chance of a worsening of vision of 1%.

Fred assigns a utility of +5 to achieving a complete vision correction, +2 to a partial correction, 0 to no change in vision, and -10 to a worsening of vision. He also assigns a utility of -2 to the cost and physical discomfort of the operation.

Assuming Fred wants to maximise his expected utility, should he have the operation? 

## Solution

The expected utility of *not* having the operation is zero (no change in vision, no cost or discomfort due to the operation). 

The expected utility of having the operation is:

(5·0.46) + (2·0.44) + (0·0.09) + (-10·0.01)-2 = +1.08. 

The expected utility of having the operation is larger than the expected utility of not having the operation, so Fred should have the operation. 

## What did we learn today?

- How to calculate basic probabilities
- Probabilities of independent events
- Conditional probabilities
- Risks, benefits, expected values
- Utility
- Conjunction fallacy

## Any questions?

![](graphics/questions.jpg)\ 

## Thank you for your attention!

Email: <matthias@ln.edu.hk>

If you have questions, please come to my office hours (see course outline).
