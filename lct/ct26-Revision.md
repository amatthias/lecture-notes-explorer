% Critical Thinking
% 16 Analysing arguments
% A. Matthias

# Where we are

## What did we learn last time?

## What are we going to learn today?

# Argument mapping




## What did we learn today?
- How to express arguments in predicate logic 
- Validity in propositional logic
- Natural deduction

## References
HKU OpenCourseWare on critical thinking, logic, and creativity:

http://philosophy.hku.hk/think/pl

(Modules Q04, Q05)

## Any questions?

![](graphics/questions.jpg)\ 

## Thank you for your attention!

Email: <matthias@ln.edu.hk>

If you have questions, please come to my office hours (see course outline).





