% Critical Thinking
% 4. Propositional Logic and Truth Tables
% A. Matthias

# Where we are
## What did we learn last time?

- How to correct unclear definitions
- What is a proposition?
- What is a non-propositional utterance?
- The language we use in daily life can be represented symbolically
- One way to do this is the so-called propositional calculus
- Propositions can be combined using logical operations

## What are we going to learn today?

- Logical operations can be described by truth tables

# The five basic logical operations

## Truth values

>- In order to be able to analyze the truth of ordinary-language sentences, we must first understand truth values.
>- We already know that a proposition is a statement which can be true or false.
>- We denote true with T and false with F.
>      - Today is Monday: (if it is really Monday) T
>      - Everyday is Monday: F
>      - You are students: T
>      - It rained yesterday: F
>      - 2+2=4 : T

## True and false

>- If we apply the logical operators to propositions, we can calculate the truth of the resulting expression.
>- Let's assume p is true.
>- Then $\lnot$p is false.
>- Example: if it is true that it rains (p), then it is false that it does not rain ($\lnot$p).
>- Thus:
>      - If p is true, then $\lnot$p is false.
>      - If p is false, then $\lnot$p is true.

## True and false

We can write this in table form (“truth table”):

\begin{center}
\begin{tabular}{|c||c|}
\hline
$ p $ & $  \neg p $ \\
\hline
F & T \\
\hline
T & F \\
\hline
\end{tabular}
\end{center}

## Truth table for conjunction

We can write down truth tables for all logical operations:

E.g. conjunction (“and”):

\begin{center}
\begin{tabular}{|c|c||c|}
\hline
$ p $ & $ q $ & $ (p \wedge q) $ \\
\hline
F & F & F \\
\hline
F & T & F \\
\hline
T & F & F \\
\hline
T & T & T \\
\hline
\end{tabular}
\end{center}

. . . 

>- What does this mean?
>- p$\land$q is only true, if both p is true and q is true.
>- “I had noodles and an apple for dinner” is only true, if both parts (“I had noodles”, “I had an apple”) are true. Otherwise it is false.


## Truth table for disjunction (“or”)

Now please write down a truth table for disjunction yourself!

As an example, take the sentence:

*The Observatory has raised the black rainstorm or the typhoon number 8 signal.*

## Truth table for disjunction

\begin{center}
\begin{tabular}{|c|c||c|}
\hline
$ p $ & $ q $ & $ (p \vee q) $ \\
\hline
F & F & F \\
\hline
F & T & T \\
\hline
T & F & T \\
\hline
T & T & T \\
\hline
\end{tabular}
\end{center}

>- What does this mean?
>- p$\lor$q is only false, if both p is false and q is false.
>- “If there is a black rainstorm or a typhoon number 8 signal, classes will be cancelled.”
>- Classes will be cancelled if any of its parts is true (if there is a black rainstorm, or a number 8 typhoon, or both!) Otherwise they will not be cancelled.

## Problems with disjunction

Problem 1:

>- Why is the result true, when only one of the propositions is true?
>- Example: the University administration says: "You will have to get 3 credits from a CLB course or a CLD course."
>- Now you take a CLB course. Do you satisfy the requirement?
>- Yes.

## Problems with disjunction

Problem 2:

>- Why is the result true, when both propositions are true?
>- Example: "If you steal a car or if you steal a diamond ring, you go to prison."
>- Now if you steal a car, and then use it to drive away with somebody's diamond ring, do you go to prison?
>- Yes.

## Exclusive “or”

There is another logical operator, called the “exclusive or ($\oplus$)”, because it excludes the case where both propositions are true.

This would be a strict “either/or.”

We will not talk about this here. In this course, we deal only with the “normal” (inclusive) “or.”

## Truth table for implication

\begin{center}
\begin{tabular}{|c|c||c|}
\hline
$ p $ & $ q $ & $ (p \rightarrow q) $ \\
\hline
F & F & T \\
\hline
F & T & T \\
\hline
T & F & F \\
\hline
T & T & T \\
\hline
\end{tabular}
\end{center}

>- What does this mean?
>- p$\to$q is only false if the first proposition is true and the second false.
>- “If it rains, the street will get wet” is true in all cases, except if it rains and the street does not get wet.

## Implication truth table

The truth table for the implication does not really match the meaning of “if...then...” very well.

Why we use it to translate “if...then...” anyway is discussed in:

<http://philosophy.hku.hk/think/sl/ifthen.php>

Please have a look there if you are interested in the details.

## Truth table for co-implication or biconditional

\begin{center}
\begin{tabular}{|c|c||c|}
\hline
$ p $ & $ q $ & $ (p \leftrightarrow q) $ \\
\hline
F & F & T \\
\hline
F & T & F \\
\hline
T & F & F \\
\hline
T & T & T \\
\hline
\end{tabular}
\end{center}

>- What does this mean?
>- p$\leftrightarrow$q is only false if the two propositions have *different* truth values and true otherwise.
>- “If and only if the sun shines will the sky be blue” is true if both parts are true or if both are false.

# Calculating truth tables

## More complex expressions

>- Now we can calculate the truth tables of more complex expressions. By doing this, we can find out under which conditions they are true or false.
>- “I will have pork or beef, together with rice and no tea.”
>- Write this sentence down in symbolic form. 

. . .

### “I will have pork or beef, together with rice and no tea.”

- p: I will have pork
- b: I will have beef
- r: I will have rice
- t: I will have tea

\begin{center}
\f{(p$\lor$b)$\land$(r$\land$$\lnot$t)}
\end{center}

## Now calculate the truth table!

“I will have pork or beef, together with rice and no tea.”

\f{(p$\lor$b)$\land$(r$\land$$\lnot$t)}


![](graphics/06-tt01.png)\ 

## (p$\lor$b)$\land$(r$\land$$\lnot$t)

\begin{center}
\begin{small}
\begin{tabular}{|c|c|c|c||c|c|c|c|}
\hline
$ p $ & $ b $ & $ r $ & $ t $ & $ (p \vee b) $ & $  \neg t $ & $ (r \wedge  \neg t) $ & $ ((p \vee b) \wedge (r \wedge  \neg t)) $ \\
\hline
F & F & F & F & F & T & F & F \\
\hline
F & F & F & T & F & F & F & F \\
\hline
F & F & T & F & F & T & T & F \\
\hline
F & F & T & T & F & F & F & F \\
\hline
F & T & F & F & T & T & F & F \\
\hline
F & T & F & T & T & F & F & F \\
\hline
F & T & T & F & T & T & T & T \\
\hline
F & T & T & T & T & F & F & F \\
\hline
T & F & F & F & T & T & F & F \\
\hline
T & F & F & T & T & F & F & F \\
\hline
T & F & T & F & T & T & T & T \\
\hline
T & F & T & T & T & F & F & F \\
\hline
T & T & F & F & T & T & F & F \\
\hline
T & T & F & T & T & F & F & F \\
\hline
T & T & T & F & T & T & T & T \\
\hline
T & T & T & T & T & F & F & F \\
\hline
\end{tabular}
\end{small}
\end{center}

## Truth tables (\cn{真值表})
Let's see how to build a truth table for a sentence:

>1. Write down the propositions as a complex expression in symbolic form
>2. Write all variables down, each into one table column
>3. Do the same for the sub-expressions
>4. Start with any variable and fill in T, F, T, F, ... and so on (starting with T!)
>5. For the second variable, go in pairs of T, T, F, F, ..., then in groups of 4, 8, 16, 32 and so on.
>6. Calculate the truth values of the subexpressions and the expression.

## Calculating a truth table

![](graphics/06-tt03.png)\ 


## Now try it yourself!

Convert the following sentence into symbolic form:

“I will paint my room white, but I want those blue sheets for the bed, or red curtains. Otherwise I will just paint it yellow, but without red curtains.”


## Symbolic representation

“I will paint my room white, but I want those blue sheets for the bed, or red curtains. Otherwise I will just paint it yellow, but without red curtains.”

Variables used: (use letters that make sense!)

- w: “I will paint my room white”
- b: “I can get the blue sheets”
- r: “I can get the red curtains”
- y: “I will paint it yellow”

\center{\f{\Large{(w$\land$(b$\lor$r))$\lor$(y$\land$$\lnot$r)}}}

## Converting the little words

“I will paint my room white, but I want those blue sheets for the bed, or red curtains. Otherwise I will just paint it yellow, but without red curtains.”

\center{\f{\Large{(w$\land$(b$\lor$r))$\lor$(y$\land$$\lnot$r)}}}

>- “but” = and
>- “otherwise” = or
>- “without” = not
>- “just” = (ignored)

## “Otherwise”

>- In the last example we translated “otherwise” as “or.”
>- A more precise translation would be: “And if not ..., then ...”
>- So: “If they have coke, I will stay, otherwise I will leave (=not stay)”
>- means:
>- “If they have coke, I will stay. If they don’t have coke, I will not stay,” and this translates to: “I will stay *if and only if* they have coke”:
>- (C $\to$ S) $\land$ ($\lnot$C $\to$ $\lnot$S)
>- which is the same as:
>- C$\leftrightarrow$S

## “Otherwise”

More generally, “if A then B, otherwise C” translates as:

\f{(A$\to$B)$\land$($\lnot$A$\to$C)}

Only in the special case where C is equivalent to $\lnot$B (as in the previous example), “otherwise” becomes a biconditional. So

“If A then B, otherwise not B” becomes:

\f{(A$\to$B)$\land$($\lnot$A$\to$$\lnot$B)}

which is the same as:

\f{A$\leftrightarrow$B}

## Now try it yourself!

Calculate the truth table for the following sentence:

“I will paint my room white, but I want those blue sheets for the bed, or red curtains. Otherwise I will just paint it yellow, but without red curtains.”

*First write it down as a symbolic expression!*

## Symbolic representation

- w: “I will paint my room white”
- b: “I can get the blue sheets”
- r: “I can get the red curtains”
- y: “I will paint it yellow”

\center{\f{\Large (w$\land$(b$\lor$r))$\lor$(y$\land$$\lnot$r)}}

*Now calculate the truth table!*

## Truth table for (w$\land$(b$\lor$r))$\lor$(y$\land$$\lnot$r)

![](graphics/06-tt04.png)\ 

## Why do we do all this?

“I will paint my room white, but I want those blue sheets for the bed, or red curtains. Otherwise I will just paint it yellow, but without red curtains.”

![](graphics/06-tt04.png)\ 

>- By analyzing the truth table of complex expressions, we can say exactly under which conditions they will be true of false.
>- In the above example, we can see exactly which combination of curtains, bed sheets and wall colour would satisfy our customer.

# More exercises

## Translate into English:

Find an English sentence with the following logical structure:

\f{A$\leftrightarrow$((B$\land$C)$\lor$(D$\land$C))}

. . . 

"If and only if you give me a good grade, I will attend your class and like you, or I will be your Facebook friend and like you."

## Translate into English:

(A$\land\lnot$B)$\to$C$\land$(D$\lor$E)

. . . 

Solution: “If I get my results without A's, then my mother will kill me with a gun or a knife.”

## Which is the correct expression?

“I want a pizza, but it should have no meat or pineapple, and make sure it has some tuna and a lot of cheese or pepper!”

Which is the correct formal representation of this sentence?

P: pizza, M: meat, I: pineapple, T: tuna, C: a lot of cheese, E: a lot of pepper.

1. P$\land\lnot$(M$\land\lnot$I)$\land$T$\land$(C$\lor$E)
2. P$\land$($\lnot$M$\lor$I)$\land$(T$\land$C)$\lor$E
3. P$\land$($\lnot$M$\lor\lnot$I)$\land$T$\land$(C$\land$E)
4. P$\land\lnot$(M$\lor$I)$\land$T$\land$(C$\lor$E)
5. P$\land\lnot$(M$\land$I)$\land$T$\land$(C$\lor$E)

. . .

Solution:

4. P$\land\lnot$(M$\lor$I)$\land$T$\land$(C$\lor$E)

## Translate into English:

(P $\land$ $\lnot$Q) $\leftrightarrow$ R $\land$ S $\land$ T

. . .

“If and only if you buy me the expensive ring and not the cheap one, I will like you and marry you and we will be happy.”

## Which is the correct expression?

“If and only if I marry Paul but not Mike or John, I will have a house with cows and elephants.”

Which is the correct formal representation of this sentence?

M: Paul, H: Mike, E: John, P: house, C: cows, J: elephants.

1. M$\land\lnot$(H$\lor$E)$\leftrightarrow$P$\land$(C$\lor$J)
2. M$\lor\lnot$(H$\lor$E)$\leftrightarrow$(P$\land$C)$\land$J
3. M$\land$($\lnot$H$\lor\lnot$E)$\leftrightarrow$P$\land$(C$\land$J)
4. M$\land\lnot$(H$\lor$E)$\to$P$\land$(C$\land$J)
5. M$\land\lnot$(H$\lor$E)$\leftrightarrow$P$\land$(C$\land$J)

. . .

Solution:

5. M$\land\lnot$(H$\lor$E)$\leftrightarrow$P$\land$(C$\land$J)

## Which is the correct expression?

“If I read a book and don’t go to the cinema or to the restaurant, I will be satisfied without having spent too much money.” -- Which is the correct formal representation of this sentence?

q: I read a book, p: I go to the restaurant, d: I go to the cinema, b: I will be satisfied, m: I will have spent too much money.

1. q$\land\lnot$p$\lor$d$\to$b$\land\lnot$m
2. q$\land\lnot$(p$\lor$d)$\leftrightarrow$b$\land\lnot$m
3. q$\land\lnot$p$\lor\lnot$d$\to$b$\land\lnot$m
4. q$\land\lnot$p$\land\lnot$d$\to$b$\land\lnot$m
5. q$\land\lnot$(p$\land$d)$\to$b$\land\lnot$m

. . .

Solution:

4. q$\land\lnot$p$\land\lnot$d$\to$b$\land\lnot$m

## What did we learn today?

Logical operations can be described by truth tables

## Any questions?

![](graphics/questions.jpg)

## Thank you for your attention!

Email: <matthias@ln.edu.hk>

If you have questions, please come to my office hours (see course outline).
