% Critical Thinking
% 11. Deductive and inductive arguments. Validity and strength.
% A. Matthias

# Where we are

## What did we learn last time?

- Logic exercises

## What are we going to learn today?

- What deductive, inductive, valid, strong and weak arguments are

# Inductive and deductive arguments

## Different ways to make an argument

Arguments can be made in different ways

*What is the difference?*

1. On Fridays, the student restaurant has curry pork chop. Today is Friday. Therefore, the student restaurant will have curry pork chop.

2. In the past six weeks, the student restaurant always had curry pork chop on Fridays. Today is Friday. Therefore, I expect them to have curry pork chop.

## What is the difference?

1. On Fridays, the student restaurant has curry pork chop. Today is Friday. Therefore, the student restaurant will have curry pork chop.

2. In the past six weeks, the student restaurant always had curry pork chop on Fridays. Today is Friday. Therefore, I expect them to have curry pork chop.

Two aspects:

1. How do I know that they have curry pork chop on Fridays? (general rule or experience)
2. How sure can I be that they’ll have curry pork chop this Friday? (100% sure or less sure)

## Deductive argument

1. On Fridays, the student restaurant has curry pork chop. Today is Friday. Therefore, the student restaurant will have curry pork chop.

>- This is the argument form we already know well.
>- It is called a *deductive* argument.
>- Deductive arguments are those where the conclusion logically follows from the premises.
>- This means: *If the premises are true, the conclusion must also be true.*

## Inductive argument

2. In the past six weeks, the student restaurant always had curry pork chop on Fridays. Today is Friday. Therefore, I expect them to have curry pork chop.

>- This is an inductive argument.
>- Inductive arguments try to infer a rule from a set of observations.
>- The more observations I make, the higher the probability will be that the conclusion is true.
>- But I can never be completely sure of its truth.

## Deductive vs. inductive

**Deductive reasoning:** The conclusion is certain. It is derived logically from the premises. Often one of the premises will be a general rule (but not always).

**Inductive reasoning:** The conclusion is not certain, but merely probable. Often (but not always) a general conclusion will be derived from the observation of specific cases (from the particular to the general).

## Deductive and inductive

### Certainty criterion

A deductive argument is an argument in which it is thought that the premises provide a *guarantee of the truth of the conclusion.* In a deductive argument, the premises are intended to provide support for the conclusion that is so strong that, if the premises are true, it would be *impossible* for the conclusion to be false.

An inductive argument is an argument in which it is thought that the premises provide reasons supporting the *probable truth of the conclusion.* In an inductive argument, the premises are intended only to be so strong that, if they are true, then it is *unlikely* that the conclusion is false.

## Same conclusion proved both ways

**Deduction:**

(1) All birds fly.  
(2) Eagles are birds.  
 ---------------------------  
(3) Therefore, all eagles fly.

**Induction:**

(1) This eagle flies.  
(2) This other eagle also flies.  
(3) ... (more observations)  
 -----------------------------  
(3) Therefore, all eagles fly.

## Good inductive arguments

>- Rely on true, accurate and representative observations (don't use unusual cases!)
>- Must provide enough evidence
>- There must not be evidence pointing into another direction!

## What type of argument is this?

*What type of argument is this? Inductive? Deductive? Is it good?*

"Alice, Amy and Anna have each lost ten pounds since the holidays and they all work out for an hour every morning in the gym. So if people go to the gym for an hour every morning, they will also lose some weight."

. . . 

>- Inductive arguments must provide enough evidence!
>- It is an inductive argument, but the evidence is weak (only three cases).

## Other factors?

- You might say that there are other reasons why people could gain weight, even if they go to the gym (for example, if they eat too much).
- But when there is nothing said about other factors, we assume that they just stay the same.
(After all, we don't know how much Alice, Amy and Anna are eating. So we assume that they behave in an average way and that the other people do so, too).

## What type of argument is this?

*What type of argument is this? Inductive? Deductive? Is it good?*

"Alice, Amy and Anna have each lost ten pounds since the holidays and they all work out for an hour every morning in the gym. So if Susan goes to the gym for an hour every morning, she will also lose some weight."

. . . 

>- Still an inductive argument
>- The reason is that the conclusion is only probable, but not certain!
>- It is still weak, for the same reasons, but it is a little better in that the conclusion is not so general. By concluding something only about Susan (who is also a woman, for example), we strengthen the argument a bit.

## What type of argument is this?

*What type of argument is this? Inductive? Deductive? Is it good?*

"Alice, Amy and Anna have each lost ten pounds since the holidays and they all work out for an hour every morning in the gym. Therefore, Alice, Amy and Anna are now ten pounds lighter than before."

. . . 

>- Deductive
>- This is a deductive argument, because the conclusion is certain. It follows logically from the premises.

## What type of argument is this?

*What type of argument is this? Inductive? Deductive? Is it good?*

"Mario studies for 2 hours every night and he's got an A. Mimi studies for three hours every morning and she's close to an A too. Michael studies for an hour every Thursday after work and he has an A, too. So it looks as if studying a lot is important in order to get a good grade."

. . .

>- Inductive but bad.
>- There must not be evidence pointing into another direction!
>- Here, Michael is studying only one hour on Thursdays, but he has an A too. The argument is not convincing, because there is evidence pointing into another direction!

## What type of argument is this?

*What type of argument is this? Inductive? Deductive? Is it good?*

"Anna's father is an investment banker and he's making a lot of money. So if I want to be rich, I should consider a career in investment banking."

. . . 

>- Inductive argument, but too few observations (this is a fallacy called “hasty generalization”).

## Inductive or deductive?

“Today I wear a black T-shirt. Yesterday, and the day before, and the day before that, I have also been wearing black T-shirts. Therefore, the past three days I have always been wearing black T-shirts!”

. . . 

It looks like it proceeds from observations to a more general rule, but actually the conclusion does follow logically from the premises. Therefore, this is really a *deductive* argument! (See: certainty criterion!)

## Inductive or deductive?

"The members of the Williams family are Susan, Nathan and Alexander. Susan wears glasses. Nathan wears glasses. Alexander wears glasses. Therefore, all members of the Williams family wear glasses."

. . . 

>- Deductive. The conclusion is certain.

## Inductive or deductive?

"It has snowed in New York every December in recorded history. Therefore, it will snow in New York this coming December."

. . . 

>- Inductive. The conclusion is probable, but not certain.

## Deductive or inductive?

1. Three friends of mine have a Thinkbox 2000 computer and they all say it's slow and expensive. Therefore, the Thinkbox must be slow and expensive.
2. Thinkbox computers are slow and expensive. I don't want a slow and expensive computer, therefore I will not buy a Thinkbox.
3. All my friends who exercise daily are healthy. Therefore if I exercise daily, I'll be healthy.
4. Everybody who exercises daily is healthy. Therefore if I exercise daily, I'll be healthy.

## Deductive or inductive?

1. Three friends of mine have a Thinkbox 2000 computer and they all say it's slow and expensive. Therefore, the Thinkbox must be slow and expensive. **Inductive.**
2. Thinkbox computers are slow and expensive. I don't want a slow and expensive computer, therefore I will not buy a Thinkbox. **Deductive.**
3. All my friends who exercise daily are healthy. Therefore if I exercise daily, I'll be healthy. **Inductive.**
4. Everybody who exercises daily is healthy. Therefore if I exercise daily, I'll be healthy. **Deductive.**

## Deductive or inductive?

“All my friends who exercise daily are healthy.”

- This might *look like* a rule, but it is not.
- It is an observation.
- If it was a rule, you could assume it to be always true. But obviously your friends will not always be healthy. Therefore, this is not a general rule, but the summary of particular observations.

## Deductive or inductive?

Sometimes it is hard to decide.

Someone says: “This is a piece of Feta cheese, so it must have been produced in Greece.”

(Feta cheese is a kind of cheese made in Greece.)

*Is this inductive or deductive?*

## Deductive or inductive?

“This is a piece of Feta cheese, so it must have been produced in Greece.”

>- If the speaker believes that most Feta cheese is made in Greece, or that usually Feta cheese is made in Greece, then the argument would be inductive.
>- If (and this is true) the name “Feta cheese” is by law reserved only for cheeses made in Greece, then there can be no Feta that is not from Greece. So the argument is deductive, because the conclusion is certain.
>- But does the speaker know that?
>- So whether the argument is inductive or deductive would depend on the speaker’s or the audience’s knowledge!

# Validity, soundness, and inductive strength

## How good are these arguments?

1. If Anne watches TV instead of going to her Critical Thinking class, she'll fail Critical Thinking. Now she watches TV instead of going to her Critical Thinking class. Therefore she’s going to fail.
2. I’ve eaten three times in this restaurant, and the food was always good. Therefore, this restaurant is good.
3. No one has ever proved that aliens don't exist. So aliens probably do exist and they have visited the earth.

## Valid arguments

Remember: *If the truth of the conclusion of a (deductive) argument depends only on the truth of its premises, it is called valid. Valid arguments cannot have true premises and a false conclusion.*

. . .

Example: “If Anne watches TV instead of going to her Critical Thinking class, she'll fail Critical Thinking. Now she watches TV instead of going to her Critical Thinking class. Therefore she’s going to fail.”

This is a deductively valid argument.

## Valid arguments

"If Anne watches TV instead of going to her Critical Thinking class, she'll fail Critical Thinking. Now she watches TV instead of going to her Critical Thinking class. Therefore she’s going to fail."

>- You might think that this argument is not valid, because Anne might fail for other reasons. But this does not change the validity of this argument!
>- What we have here, is an implication: If Ann watches TV, then Ann will fail: A$\to$B.
>- Of course B may come about through other causes, but still A is a sufficient cause for B!

## Sound arguments

If a (deductive) argument is valid and its premises are true (and, consequently, its conclusion is true), then it is called a *sound* argument.

## Invalid arguments

>- (Deductively) invalid arguments don't prove their conclusions, even if their premises are true.
>- That is, they can have true premises but still a false conclusion!
>- Invalid arguments are fallacies (mistakes in argumentation)!
>- Example: “No one has ever proved that aliens don't exist. So aliens probably do exist and they have visited the earth.”
>- In this case, although the premise is true, the conclusion can be false. This is therefore not valid.

## Inductive arguments can be strong or weak

>- *Inductive* arguments can be very convincing or only a little convincing.
>- We call them strong or weak, accordingly.

## Strong arguments

>- Example:
>- “I asked 10,000 Hong Kong citizens in a survey whether they like to drink beer. Almost all said they liked beer. Therefore I conclude that Hong Kong citizens generally like to drink beer.”
>- This is a strong argument, because the conclusion follows convincingly from the premises.

## Weak arguments

>- Example:
>- “I’ve eaten three times in this restaurant, and the food was always good. Therefore, this restaurant is good.” 
>- This is a weak argument, because we can doubt if the generalization is valid. It is not a fallacy though (which would be invalid) because to eat three times at one place is not too small a sample (how many times are you supposed to eat there until you can judge the food?)

## Weak argument vs. fallacy

>- A fallacy is an error in argumentation.
>- Whether an argument is only a weak argument or a fallacy is often a matter of degree and context.
>- If I say that all English people speak good Cantonese, because I know two English who speak good Cantonese, then this is a fallacy (not enough evidence to support that conclusion!)
>- On the other hand, if I conclude that a restaurant is good (in my opinion) after eating there 3 times, then this is a weak argument, but not a fallacy. There is some support for the conclusion, but it is not very strong.

## What is a good argument?

>- Criterion #1: A good argument must have true premises (that is, if it is deductive, it must be sound).
>- This means that if we have an argument with one or more false premises, then it is not a good argument. The reason for this condition is that we want a good argument to be one that can convince us to accept the conclusion. Unless the premises of an argument are all true, we would have no reason to accept its conclusion. 

## What is a good argument?

>- Criterion #2: A good argument must be either sound or strong.
>- Not all good arguments are sound, because only deductive arguments can be valid and sound.
>- If they are inductive, they need to at least be *strong* arguments.

## What is a good argument?

- Criterion #3: The premises of a good argument must not beg the question.

. . . 

“Begging the question”:

>1. It is going to rain tomorrow. Therefore, it is going to rain tomorrow.
>2. If smoking was not bad for your health, then you could smoke without putting your health in danger. But now you are putting your health in danger by smoking. Therefore, smoking is bad for your health.
>3. Since Mary would not lie to her best friend, and Mary told me that I am indeed her best friend, I must really be Mary's best friend. 

## What is a good argument?

Criterion #4 :

The premises of a good argument must be relevant to the conclusion

## Summary: Good arguments

A good argument is an argument that

1. is either sound or strong, and
2. with premises that are true,
3. do not beg the question, and
4. are relevant to the conclusion.

## Summary: Valid, invalid, sound, strong, weak

>- *Inductive* arguments can be strong or weak or invalid (fallacies, for example hasty generalization).
>- *Deductive* arguments can be valid (conclusion true if premises are true), or sound (valid, plus premises are actually true), or invalid (fallacies, for example affirming the consequent).
>- *Good* arguments are sound deductive arguments, or strong inductive arguments.

## Can inductive arguments be invalid?

>- It is not clear whether *inductive* arguments can be said to be “invalid.” It depends on your use of “valid” and “invalid.”
>- If we understand “invalid” to mean “not valid,” then *all* inductive arguments would be “invalid” (because no inductive argument is deductively valid!)
>- But if we understand “invalid” to mean “does not support the conclusion at all,” then inductive arguments can be invalid too (e.g. the “hasty generalisation” fallacy).
>- So this is a matter of the usage of the term. Different books and teachers might have different opinions about that.
>- In exams, we will avoid questions that are unclear in this way.



## Summary: Valid, invalid, sound, strong, weak

~~~~~

<------------------------------------------------>
Fallacy     Weak      Strong    Deductively Sound
(no support inductive inductive valid      (concl.
for concl)                      (concl.    certain
=invalid                        certain)   and 
                                           true)
                      |--------            ------>
                       “Good” arguments

~~~~~




## Valid and circular arguments

Technically, an argument is also valid if the premises include the conclusion.

For example: P, Q, R $\vdash$ P

"Valid" means: "if the premises are true, the conclusion must be true." This is the case here!

## Which are true?

A deductive argument...
	
A. ... is never a sound argument  
B. ... can be inductive if the conclusion is not 100% certain  
C. ... is always also a strong inductive argument  
D. ... can be a weak argument  
E. ... can be a sound argument  

. . . 

E.

## Which are true?

Which statements are correct of deductive arguments?

A. All fallacies are weak arguments  
B. A valid argument with true premises must have a true conclusion  
C. A sound argument must have a true conclusion  
D. Sound arguments need not have true conclusions  

. . . 

B, C.


## Which are true?

A. In a valid argument, if the premises are false, the conclusion must be false.  
B. In a valid argument, if at least one premise is true, the conclusion must be true.  
C. In a valid argument, if both premises are true, the conclusion must be true.  
D. In an invalid argument, true premises can sometimes have a true conclusion.  
E. In an invalid argument, true premises can never have a true conclusion.

. . . 

C, D.


## Which are true?

A. All inductive arguments are either valid or invalid.  
B. No inductive argument can be valid.  
C. Deductive arguments can be strong or weak or invalid.  
D. Inductive arguments can be strong or weak or invalid.  
E. All deductive arguments are valid.  

. . . 

B (and perhaps D, depending on how you define “invalid”)

## Which are true?

A. All sound arguments are valid.  
B. All valid arguments are sound.  
C. No inductive argument can be sound.  
D. Some inductive arguments can be sound.  
E. Inductive arguments are sometimes valid.  

. . .

A, C.

# Old exam exercises

## Inductive strength

Consider the following argument:

Premise: John is the fastest 100m swimmer in the world.  
Premise: John is competing in the 100m swimming competition.  
Conclusion: John will win the 100m swimming competition.

Which of the following premises will render the argument inductively weak if added to the argument?

1. John is wearing a professional swim outfit.
2. John is very famous.
3. John has a serious shoulder injury.
4. John has a serious shoulder injury, but it doesn't affect his swimming ability.

. . .

>- To "make an argument inductively weaker" means to reduce the probability that the conclusion is true.
>- So here is must be answer 3. Only this premise would make the conclusion unlikely.

## Inductive strength

Consider the following argument:

Premise: 95% of people will get cancer.  
Premise: 5% of people with blood type 2 will get cancer.  
Premise: John is a person with blood type 2.  
Conclusion: John will get cancer.

Which of the following correctly describes this argument?

1. It is deductively valid.
2. It is inductively strong.
3. It is inductively weak.
4. It is inductively bad.

. . .

Answer: 3.

## Validity and soundness

Consider the following argument:

Premise: New York is in the United States.  
Premise: Hong Kong is in Russia.  
Conclusion: Hong Kong is in Russia.

Is the argument valid, sound, both, or neither?

. . .

>- The first premise is true, the second is false. So it cannot be sound.
>- Is it valid?
>- If the premises are true, the conclusion must be true. So, yes, it is valid.


## Inductive strength

Consider the following argument:

Premise: Jack fell down.  
Conclusion: Jack hurt his head.

Adding which of the following further premises will make the argument inductively weaker?

1. Jill fell down the hill and landed on Jack's head.
2. The ground around Jack is soft and there are no hard objects nearby where Jack fell down.
3. Jack went to bed and bound his head with vinegar and brown paper.
4. None of the above.

. . .

Answer: 2.

## Validity

Consider the following argument:

Premise: John fell from the top of the IFC building.  
Conclusion: John is dead.

Which of the following additional premises would make the argument deductively valid?

1. John is afraid of heights and suffered a heart attack during the fall.
2. John landed in a pool full of toxic chemicals.
3. John is dead.
4. All of the above.

. . .

Answer: Obviously, 3. It is the only premise that makes *certain* that he is dead. The others just make it more *probable* that he is dead (they make the argument inductively stronger), but they cannot make completely sure that he is dead.

## Validity

Consider the following argument:

Premise: Alan fell from the top of the Mandarin Oriental Hotel.  
Conclusion: Alan is dead.

Adding which premise would make the argument inductively *weaker?*

1. Alan landed in a pool full of toxic chemicals.
2. Alan is afraid of heights, and suffered a heart attack during the fall.
3. Alan is a bird.
4. Alan is a bird, but has never learnt how to fly.

. . .

The intented answer is 3, although one could argue that 4 also makes the argument weaker.

Still, given that only one answer can be correct, it is safe to say 3, which is stronger than 4 in its effect of weakening the argument.


## Validity

"Eagles are fish. Fish are mammals. Therefore, it follows that eagles are mammals." -- This argument is:

1. Invalid.
2. Deductively valid.
3. Deductively valid and sound.
4. Sound but not deductively valid.

. . .

2. The actual truth (or not) of the premises is not relevant. It is not sound though, because the premises are not true.


## Validity/strength

Consider the following argument:

Premise: There are 35 students in this class.  
Premise: Lisa is a student in this class.  
Premise: The teacher will give an A to one student only, who will be chosen randomly.  
Conclusion: Lisa will not get an A.  

This argument is:

1. Inductively weak
2. Inductively strong
3. Deductively valid
4. None of the above

. . .

Answer: 2. Inductively strong. Chances of 34:1 seem to justify well that she will not get an A.

## Validity

"It is obvious that, if we are good citizens, we must love our country. It is equally clear that we all love our country. Therefore, we are good citizens." -- Is this argument valid? Explain!

. . .

This is a conditional argument. It is affirming the consequent and is therefore *not* valid.

## Validity

Which of the following is a feature of validity?

1. The premises are always true
2. The conclusion is always true
3. The premises are never false when the conclusion is true
4. The conclusion is never false when the premises are true

. . .

Answer: 4.

## Validity/strength

Consider the following argument:

Premise: 20075 cats have been thrown from a balcony on the third floor of a building, and have all landed on their feet, alive and well.  
Conclusion: The next cat (the 20076th cat) to be thrown from the balcony will also land on its feet (alive and well).

This argument:

1. Is valid
2. Is inductively strong
3. Is inductively weak
4. Is a fallacy

. . .

Answer: 2.

## Inductive strength

Consider the following argument:

- Premise: The helicopter is flying 10 km above land.
- Premise: Jill accidentally falls out of the helicopter.
- Conclusion: Jill will die.

Which of the following additional premises would make the argument inductively *weak?*

1. Jill has a parachute.
2. Jill has a parachute, but it doesn't work.
3. Jill has a working parachute, but doesn't know how to use it.
4. Jill has a working parachute, and knows how to use it, but she's knocked unconscious when she leaves the helicopter.

. . .

Answer: 1. The other answers just strengthen the argument again (making the conclusion that she'll die more probable!)


## What did we learn today?

- A little more about the validity of arguments.
- What deductive, inductive, valid, strong and weak arguments are.

## References

Drew E. Hinderer: “Building Arguments.” Belmont, California: Wadsworth, 1992

Lingnan University Library:
PE 1431 .H5 1992

## Any questions?

![](graphics/questions.jpg)\ 

## Thank you for your attention!

Email: <matthias@ln.edu.hk>

If you have questions, please come to my office hours (see course outline).





