% Critical Thinking
% 3. Propositions
% A. Matthias

# Where we are

## What did we learn last time?

- Intensional and extensional definitions
- Genus and difference
- Circular definitions
- Common definition errors

## What are we going to learn today?

- How to correct unclear definitions
- What is a proposition?
- What is a non-propositional utterance?
- The language we use in daily life can be represented symbolically
- One way to do this is the so-called propositional calculus
- Propositions can be combined using logical operations

# Unclear and inaccurate definitions

## Unclear and inaccurate definitions

Look at the (bad) definitions below. Find a counter-example for each definition to show the problem (an example which fits the definition but is not the thing defined).

1. Love means sincerely caring about someone.
2. An airplane is a heavier-than-air device that flies.
3. Tea is a warm beverage people drink instead of coffee.
4. Oxygen is a gas which we need to breathe in order to stay alive.

## Unclear and inaccurate definitions

Look at the definitions below. Identify all ambiguous, persuasive or unclear points, and write a new definition to solve those problems:

1. A dog is man's best friend.
2. Poetry is the kind of thing poets write.
3. A drug is any substance that has an effect on the body or mind.

# Propositions

## How would you group the following sentences into two groups?

- Airplanes fly
- The sea is made of wine
- I was in Greece last summer
- What is your name?
- Come here!
- Oh, no!

## Propositions (\cn{命題})

Propositions we call sentences which can be true or false.

>- Airplanes fly (true)
>- The sea is made of wine (false)
>- I was in Greece last summer (true or false? But surely one of the two!)

## Propositions (\cn{命題})

I don’t need to know whether a sentence is actually true or false, in order for it to be a proposition.

“On the dark side of the moon there are 5278 craters” *is* a proposition, because it must be either true or false.

It does not matter that I don’t know which.

## Non-propositional utterances \cn{(非命題言說)}

Some other sentences can not be said to be true or false.

These are all called non-propositional (because they are not propositions) utterances (things that are said; to utter = to say)

## Non-propositional utterances \cn{(非命題言說)}

We cannot meaningfully say that the following are true of false:

- What is your name? 
- Come here!
- Oh, no!

## Which of the following are propositions?

1. Today is Monday
2. Every day is Monday
3. Help!
4. I love China!
5. Do you love China?
6. It will rain tomorrow

## Which of the following are **propositions** (P)?

- **Today is Monday** (P)
- **Every day is Monday** (P)
- Help!
- **I love China!** (P) (must be true or false, even if others don't know which)
- Do you love China?
- **It will rain tomorrow** (P)

## Test for propositions

Sometimes it is helpful to add “It is true that...” in front of a sentence, in order to see whether it is a proposition.

If it is one, the resulting sentence will be grammatically correct. Otherwise it will not make sense. (But there are exceptions!)

## Test for propositions

>- It is true that today is Monday (correct sentence)
>- It is true that every day is Monday (wrong, but grammatically correct)
>- It is true that help! (?)
>- It is true that I love China! (correct sentence)
>- It is true that do you love China? (?)
>- It is true that it will rain tomorrow. (correct sentence)

## Propositions

Propositions are statements which can be true or false.

- The cat is here.
- Today is Monday.
- I love China.

# Symbolic representation of propositions

## Symbolic representation

Sometimes it is useful to represent propositions symbolically, in order to better see their structure.

Propositions can be represented symbolically with letters of the alphabet.

Usually we start with “p” for “proposition”.

## Propositions

p: The cat is here.  
q: Today is Monday.  
r: I love China.  

## Propositions and logical glue

In sentences, propositions are often connected with words like “and”, “or”, “not”:

- I will eat **or** I will sleep: p or q
- I want to eat **and** sleep: p and q
- I will **not** eat noodles: not p

## Logical operations

*How many propositions do you see in each of the following statements? How are they connected?*

1. Yesterday I went shopping
2. Yesterday I went shopping and bought a watch
3. Yesterday I did not go shopping
4. Tomorrow I will go shopping or to the cinema

## Logical operations

Try to separate the single propositions from the logic which glues them together:

>- "Yesterday I went shopping": p
>- "Yesterday I went shopping **and** bought a watch": p **and** q
>- “Yesterday I did not go shopping”: "It is **not** the case that yesterday I went shopping": **not** r
>- “Tomorrow I will go shopping or to the cinema”: "Tomorrow I will go shopping **or** tomorrow I will go to the cinema": s **or** t

# Logical operators

## Logical operators

The operations “and”, “or” and “not” are called *logical operators.*

They are usually represented by symbols:

>- “and” (conjunction): $\land$ or \&
>- “or” (disjunction): $\lor$ or v
>- “not” (negation): $\lnot$ or ~

## Logical operators

>- Yesterday I went shopping: p
>- Yesterday I went shopping and (I) bought a watch: p$\land$q (or: p\&q)
>- Yesterday I did not go shopping: $\lnot$ p (or: ~p)
>- Tomorrow I will go shopping or to the cinema: r$\lor$s

## Now do it yourself:

Write down the symbolic representation of the following sentences:

- This is a keyboard.
- This is not a mouse.
- It will rain tomorrow
- It will not rain tomorrow
- It will rain tomorrow and I will stay at home
- It will rain tomorrow, and I will stay at home and eat a salad or a noodle soup

## Now do it yourself:

Of course, it doesn't matter which letters we use!

If the examples are not connected, you can use the same letters! 

>- This is a keyboard: A
>- This is not a mouse: $\lnot$A
>- It will rain tomorrow: A
>- It will not rain tomorrow:  $\lnot$A
>- It will rain tomorrow and I will stay at home: A$\land$B

## Now do it yourself:

“It will rain tomorrow, and I will stay at home and eat a salad or a noodle soup”:

A: It will rain tomorrow  
B: I will stay at home  
C: I will eat a salad  
D: I will eat a noodle soup  

\f{A$\land$B$\land$(C$\lor$D)}

Use parentheses to make the structure clear!

## Use parentheses to make the structure clear

It will rain tomorrow, and I will stay at home and eat (a salad or a noodle soup):

\f{A$\land$B$\land$(C$\lor$D)}

(It will rain tomorrow, and I will stay at home and eat a salad) or I will eat a noodle soup:

\f{(A$\land$B$\land$C)$\lor$D}

(this doesn't make much sense!)

## Implication and biconditional

There are two additional logical operators:

Implication: $\to$

and co-implication or biconditional: $\leftrightarrow$

## Implication

The implication “implies” that if something is the case, then something else will also be the case.

"If it rains (p), (then) the street will be wet (q)": p $\to$ q

## Implication

The implication is true only in one direction!

We cannot turn it around:

Assume that “If it rains, then the street will be wet” (p $\to$ q) is true.

Then “If the street is wet, then it has rained” (q $\to$ p) is not necessarily true (because somebody might have spilled a bucket of water)

## Logical biconditional

The logical biconditional operator expresses the fact that two propositions can only be true or false together (because they imply each other).

It amounts to saying that something is the case *if and only if* something else is the case.

## Biconditional

>- If the sun shines, the sky will be blue: p $\to$ q
>- If the sky is blue, the sun will be shining: q $\to$ p
>- “The sky is blue” co-implies “the sun is shining” (that is, the sky is blue if and only if the sun is shining): p $\leftrightarrow$ q

## Now do it yourself:

Write down the symbolic representation of the following sentences:

1. If you strike a match, it will light
2. If you strike a match, it will light or it will do nothing
3. If you strike a match and it is wet, it will do nothing
4. If it is Sunday and I'm hungry, I usually eat noodles or a pizza.

## Now do it yourself:

Write down the symbolic representation of the following sentences:

1. If you strike a match, it will light:  
\f{p$\to$q}
2. If you strike a match, it will light or it will do nothing:  
\f{p$\to$(q$\lor$r)}
3. If you strike a match and it (the match) is wet, it will do nothing:  
\f{(p$\land$q)$\to$r}
4. If it is Sunday and I'm hungry, I usually eat noodles or (I eat) a pizza:  
\f{(p$\land$q)$\to$(r$\lor$s)}

## Comments

"If you strike a match, it will light or it will do nothing": p$\to$(q$\lor$r)

This could be understood to mean:

"If you strike a match, it will light or it will not light": p$\to$(q$\lor\lnot$q)

>- But this is a different expression!
>- “Do nothing” means more than “not light”: for example it means it will not break, not fall to the floor and so on.
>- Please be precise!

## Now do it yourself backwards:

Write down example sentences for the following symbolic forms:

- (p$\land$q) $\to$ (r$\land$s)
- p $\to$ q
- q $\to$ $\lnot$p
- q $\leftrightarrow$ $\lnot$p

## Now do it yourself backwards:

>- (p$\land$q)$\to$(r$\land$s)  
If you get up early (p) and work hard (q) then you will be rich (r) and famous (s).
>- p $\to$ q  
If I do sports, I will be more healthy.
>- q$\to\lnot$p  
If I go out every night, I will not get good grades
>- q$\leftrightarrow\lnot$p
>      - If and only if I get an ice cream now, I will not complain.
>      - If and only if I'm dead, then I am not alive.

# Exercises

## From old exams

Which of the following are propositions? (check all that apply)

1. Stand over there.
2. I like the music of Mozart.
3. Do you like his music?
4. I’m sure you will like his music!
5. Don’t tell me you don’t like it.

## From old exams

Convert into a formal expression:
 
“If and only if it is Monday and I have not eaten yet, I will call my friend and we will go to have lunch together.”

## From old exams

Find an English sentence with the following logical structure: 

\f{P$\to$((Q$\land$R)$\lor$Q)}

## From old exams

“I want a soup with noodles or potatoes if and only if it is hot and not too salty and if it contains dumplings with pork or cabbage but no prawns.”

A. (A$\lor$D)$\to$(B$\land\lnot$C$\land$G$\land$(E$\lor$F$\land\lnot$I))  
B. (A$\lor$D)$\leftrightarrow$(B$\land\lnot$C$\land$G$\land$(E$\lor$F$\lor\lnot$I))  
C. (A$\lor$D)$\leftrightarrow$(B$\land\lnot$C$\lor$G$\land$(E$\lor$F$\land\lnot$I))  
D. (A$\lor$D)$\leftrightarrow$(B$\land\lnot$C$\land$G$\land$(E$\land$F$\lor\lnot$I))  
E. (A$\lor$D)$\leftrightarrow$(B$\land\lnot$C$\land$G$\land$(E$\lor$F$\land\lnot$I))  

## References

Some examples from:

- J. Fahnestock, M. Secor: A Rhetoric of Argument (2nd ed.), Lingnan Library: PE 143.F3 1990
- Hinderer, Drew E.: Building Arguments. Lingnan Library: PE 1431.H5 1992

## What did we learn today?

- How to correct unclear definitions
- What is a proposition?
- What is a non-propositional utterance?
- The language we use in daily life can be represented symbolically
- One way to do this is the so-called propositional calculus
- Propositions can be combined using logical operations

## Any questions?

![](graphics/questions.jpg)\ 

## Thank you for your attention!

Email: <matthias@ln.edu.hk>

If you have questions, please come to my office hours (see course outline).
